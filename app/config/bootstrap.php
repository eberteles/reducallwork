<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php
 *
 * This is an application wide file to load any function that is not used within a class
 * define. You can also use this to include or require any files in your application.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * The actual directory name for the "app_client".
 */
if (!defined('APP_CLIENT_DIR')) {
    define('APP_CLIENT_DIR', 'app_client');
}

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * App::build(array(
 *     'plugins' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'models' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'views' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'controllers' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'datasources' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'behaviors' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'components' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'helpers' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'vendors' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'shells' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */

 App::build(array(
    'plugins' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'shells' . DS,
        ROOT . DS . APP_DIR . DS . 'shells' . DS
    ),
    'models' =>  array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'models' . DS,
        ROOT . DS . APP_DIR . DS . 'models' . DS
    ),
    'views' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'views' . DS,
        ROOT . DS . APP_DIR . DS . 'views' . DS
    ),
    'controllers' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'controllers' . DS,
        ROOT . DS . APP_DIR . DS . 'controllers' . DS
    ),
    'components' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'controllers' . DS . 'components' . DS,
        ROOT . DS . APP_DIR . DS . 'controllers' . DS . 'components' . DS
    ),
    'helpers' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'views' . DS . 'helpers' . DS,
        ROOT . DS . APP_DIR . DS . 'views' . DS . 'helpers' . DS
    ),
    'vendors' => array(ROOT . DS . 'vendors' . DS),
    'shells' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'shells' . DS,
        ROOT . DS . APP_DIR . DS . 'shells' . DS
    ),
    'locales' => array(
        ROOT . DS . APP_CLIENT_DIR . DS . 'locale' . DS,
        ROOT . DS . APP_DIR . DS . 'locale' . DS
    )
 ));
/**
 * Carrega configurações do cliente especifico
 */
$configs        = array();
$configsClient  = array();
$configsCore    = array();

$configFileCore     = ROOT . DS . APP_DIR . DS . 'config' . DS . 'config.php';
$configFileClient   = ROOT . DS . APP_CLIENT_DIR . DS . 'config' . DS . 'config.php';

if (file_exists($configFileClient)) {
    $configsClient  = require_once($configFileClient);
}

if(file_exists($configFileCore)) {
    $configsCore    = require_once($configFileCore);
} else {
    throw new Exception('Configurações não definidas.');
}

$configs = array_replace_recursive($configsCore, $configsClient);

Configure::write('App.config', $configs);

/**
 * Configuração dinâmica do usuário para o banco de auditoria
 */
App::import('Component', 'Session');
$sessionComponent = new SessionComponent();
$usuarioSession = $sessionComponent->read('usuario');

if (Configure::read('App.config.component.auditoria.enabled') && isset($usuarioSession)) {
        $defaultKey = sha1(serialize($usuarioSession));
        $defaultConfig = $configs['database']['default'];

        $defaultConfig['login'] = str_replace(
        ' ',
        '',
        Configure::read('App.config.component.auditoria.params.prefix_login') .
        '-' .
        substr($usuarioSession['Usuario']['no_usuario'], 0, 13)
    );

    $defaultConfig['password'] =
        Configure::read('App.config.component.auditoria.params.salt')
        . '-'
        . $defaultConfig['login'];

    $configs['database'][$defaultKey] = $defaultConfig;

    Configure::write('App.config', $configs);
}

 /**
 * As of 1.3, additional rules for the inflector are added below
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

// include do autoload
include dirname(__FILE__) . DS . '..' . DS  . '..' . DS . 'vendors' . DS .  'autoload.php';

// Tradução das mensagens do core
$path_bootstrap = dirname(__FILE__) . DS . '..' . DS  . '..' . DS . APP_CLIENT_DIR . DS . 'config' . DS .  'bootstrap.php';
if (file_exists($path_bootstrap)) {
    include $path_bootstrap;
}
// Locale folder
Configure::write('Config.language', Configure::read('App.config.language.locale'));

// Alteração das regras de inflections
include dirname(__FILE__) . DS . 'inflections.php';