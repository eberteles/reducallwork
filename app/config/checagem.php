<?php

$appDirPath = ROOT . DS . APP_DIR;
$tmpDirPath = $appDirPath . DS .'tmp' . DS;
$cacheDirPath = $tmpDirPath . 'cache' . DS;
$paths = array(
    $appDirPath . DS . 'data' . DS,
    $tmpDirPath,
    $tmpDirPath . 'logs' . DS,
    $cacheDirPath,
    $cacheDirPath . 'models' . DS,
    $cacheDirPath . 'persistent' . DS,
);

foreach ($paths as $path) {
    if (!is_dir($path)) {
        if(!mkdir($path, 0777, true)){
            throw new Exception("Não tem permissão de escrita para criar a pasta $path");
        }
    }
}