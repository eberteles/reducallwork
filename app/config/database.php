<?php

App::import('Component', 'Session');

class DATABASE_CONFIG
{
    /**
     * Configurações definidas no bootstrap.
     */
    public function __construct()
    {
        $sessionComponent = new SessionComponent();
        $usuarioSession = $sessionComponent->read('usuario');
        $defaulKey = false;
        $connections = Configure::read('App.config.database');
        if ($usuarioSession) {
            $defaulKey = sha1(serialize($usuarioSession));
        }

        foreach ($connections as $key => $configs) {
            // configuração para MaLOP
            if ($key == 'default' && $defaulKey && Configure::read('App.config.component.auditoria.enabled')) {
                $this->$key = Configure::read('App.config.database.' . $defaulKey);
            } else {
                $this->$key = $configs;
            }
        }
    }
}
