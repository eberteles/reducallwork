<?php

class AcoesController extends AppController
{

    var $name = 'Acoes';

    var $uses = array(
        'Acao'
    );

    function index()
    {
        $this->Acao->recursive = 0;
        $this->set('acoes', $this->paginate());
    }

    function add()
    {
        if (! empty($this->data)) {
            $this->Acao->create();
            if ($this->Acao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $usuarios = $this->Acao->Usuario->find('list');
        $this->set(compact('usuarios'));
    }

    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid acao', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Acao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Acao->read(null, $id);
        }
        $usuarios = $this->Acao->Usuario->find('list');
        $this->set(compact('usuarios'));
    }

    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Invalid id for acao', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Acao->delete($id)) {
            $this->Session->setFlash(__('Acao deleted', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Acao was not deleted', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>