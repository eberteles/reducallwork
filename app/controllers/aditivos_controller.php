<?php
/**
 * @resource { "name" : "Aditivos", "route":"aditivos", "access": "private", "type": "module" }
 */
class AditivosController extends AppController
{

    var $name = 'Aditivos';

    var $layout = 'iframe';
    /**
     * @resource { "name" : "Listagem", "route":"aditivos\/index", "access": "private", "type": "select" }
     */
    function index($coContrato)
    {
        $this->Aditivo->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Aditivo.co_contrato' => $coContrato
            ),
            'order' => array(
                'Aditivo.dt_aditivo' => 'desc'
            )
        );

        $this->set('aditivos', $this->paginate());

        $this->set(compact('coContrato'));
    }
    
    /**
     * @resource { "name" : "iframe", "route":"aditivos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }
    
    /**
     * @resource { "name" : "Adicionar Aditivo", "route":"aditivos\/add", "access": "private", "type": "insert" }
     */
    function add( $coContrato, $tp_aditivo = 1 )
    {

        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();
        if ($this->modulo->isCampoObrigatorio('no_aditivo')) {
            $this->Aditivo->validate['no_aditivo'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Campo Descrição em branco'
                )
            );
        }

        $this->loadModel('Contrato');
        $contratoAtual = $this->Contrato->find(array('co_contrato' => $coContrato));
        $this->set('dtFimVigenciaContrato', $contratoAtual['Contrato']['dt_fim_vigencia']);
        $this->set('dtFimVigenciaProcesso', $contratoAtual['Contrato']['dt_fim_processo']);

        if (!empty($this->data)) {
            $tp_aditivo = $this->data['Aditivo']['tp_aditivo'];

            $this->verificaValidacoes($tp_aditivo);

            $this->Aditivo->create();

            if ($this->Aditivo->save($this->data)) {

                if (isset($this->data['Aditivo']['dt_prazo'])) {
                    $this->updDtFimVigencia($coContrato);
                }
                
                $this->avisarGestorGarantia($coContrato);
                $this->avisarGestor($coContrato);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        App::import('Model', 'Contrato');
        $contratoDb = new Contrato();
        $this->set( 'vl_global', $contratoDb->field('vl_global', array('Contrato.co_contrato' => $coContrato) ) );
        $this->loadModel('Apostilamento');
        $this->set('vl_total', $contratoDb->field('vl_global', array('Contrato.co_contrato' => $coContrato) ) + $this->Aditivo->getTotalComReajuste($coContrato) + $this->Apostilamento->getTotal($coContrato));
        $this->set('max_pc_aditivo', formatMoney($this->Aditivo->getTotalPcAditivo($coContrato)));
        $this->set( compact( 'tp_aditivo' ) );
        $this->set( compact( 'coContrato' ) );
    }
    /**
     * @resource { "name" : "Editar Aditivo", "route":"aditivos\/edit", "access": "private", "type": "update" }
     */
    function edit( $id = null, $coContrato )
    {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();
        if ($this->modulo->isCampoObrigatorio('no_aditivo')) {
            $this->Aditivo->validate['no_aditivo'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Campo Descrição em branco'
                )
            );
        }

            $this->loadModel('Contrato');
            $contratoAtual = $this->Contrato->find(array('co_contrato' => $coContrato));
            $this->set('dtFimVigenciaContrato', $contratoAtual['Contrato']['dt_fim_vigencia']);

            if ( !$id && empty( $this->data ) ) {
                    $this->Session->setFlash( __( 'Identificador inválido', true ) );
                    $this->redirect( array(
                            'action' => 'index', $coContrato
                    ) );
            }
            if ( !empty( $this->data ) ) {
                $tp_aditivo = $this->data['Aditivo']['tp_aditivo'];

                $this->verificaValidacoes($tp_aditivo);

                if ( $this->Aditivo->save( $this->data ) ) {

                    if ( isset( $this->data[ 'Aditivo' ][ 'dt_prazo' ] ) ) {
                        $this->updDtFimVigencia($coContrato);
                    }

                    $this->avisarGestor($coContrato);
                    $this->Session->setFlash( __( 'Registro salvo com sucesso', true ) );
                    $this->redirect(array('action' => 'index', $coContrato));
                } else {
                    $this->Session->setFlash(implode("<br />", $this->Aditivo->validationErrors));
                }
            }

            App::import('Model', 'Contrato');
            $contratoDb = new Contrato();
            $vl_global  = $contratoDb->field('vl_global', array('Contrato.co_contrato' => $coContrato) );
            if ( empty( $this->data ) ) {
                    $this->data = $this->Aditivo->read( null, $id );
                    $this->data['Aditivo']['pc_aditivo']    = $this->data['Aditivo']['vl_aditivo'] * 100 / $vl_global;
            }
            $this->set( 'vl_global', $vl_global );
            $this->set( compact( 'coContrato' ) );
            $this->set( compact( 'id' ) );
            $this->set( 'aditivo', $this->data['Aditivo'] );
    }
    /**
     * @resource { "name" : "Remover Aditivo", "route":"aditivos\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato)
    {


        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $aditivo = $this->Aditivo->find(array('co_aditivo' => $id));
        }
        if ($this->Aditivo->delete($id)) {

            $this->updDtFimVigencia($coContrato);

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_tabela'] = 'aditivos';
            $log['ds_log'] = json_encode($aditivo['Aditivo']);
            $logModel->save($log);
            
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }

    private function updDtFimVigencia($coContrato)
    {
        App::import('Model', 'Contrato');
        $contratoDb = new Contrato();
        $contratoDb->updDtFimVigencia($coContrato);
    }

    function avisarGestorGarantia($coContrato) {
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        
        $this->loadModel('Contrato');

        $contrato = $this->Contrato->read(array('nu_contrato', 'nu_processo'), $coContrato);
        $fiscais = $this->Aditivo->getFiscais($coContrato);
        $gestor = $this->Aditivo->getGestorAtivo($coContrato);
        
        $mensagem   = '<p style="color:#000">É necessário revisar a garantia do Processo: ' . $print->processo($contrato['Contrato']['nu_processo']);

        if ($contrato['Contrato']['nu_contrato'] != '') {
            $mensagem .= ' - Contrato: ' . $print->contrato($contrato['Contrato']['nu_contrato']);
        }

        $mensagem .= '. </p>';

        if ($fiscais) {
            foreach($fiscais as $fiscal) {
                if ($fiscal['Usuario']['ds_email']) {
                    $this->EmailProvider->subject   = 'Aviso'; 
                    $this->EmailProvider->to        = $fiscal['Usuario']['ds_nome'] . ' <' . $fiscal['Usuario']['ds_email'] . '>';
                    $this->EmailProvider->send($mensagem);
                }
            }
        }

        if ($gestor) {
            if ($gestor[0]['Usuario']['ds_email']) {
                $this->EmailProvider->subject   = 'Aviso'; 
                $this->EmailProvider->to        = $gestor[0]['Usuario']['ds_nome'] . ' <' . $gestor[0]['Usuario']['ds_email'] . '>';
                $this->EmailProvider->send($mensagem);
            }
        }
    
    }

    function avisarGestor($coContrato)
    {
        ob_start();
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        
        $aditivo = $this->Aditivo->getTotalGlobal($coContrato);
        $pc_aditivo = ($aditivo['vl_total_aditivo'] * 100) / $aditivo['vl_global'];
        
        if ($pc_aditivo >= 20) {
            $this->EmailProvider->subject = 'Processo/Contrato atingiu '.$print->porcentagem($pc_aditivo).' de Aditivo ';
            $mensagem = '';
            
            $remetentes = $this->Aditivo->getGestorAtivo($coContrato);
            foreach ($remetentes as $remetente) :
                if ($remetente['Usuario']['ds_email'] != '') {
                    $mensagem = $remetente['Usuario']['ds_nome'] . ', <BR><BR> ' . ' Informamos que o Processo: ' . $print->processo($aditivo['nu_processo']);
                    if ($aditivo['nu_contrato'] != '') {
                        $mensagem .= ' - Contrato: ' . $print->contrato($aditivo['nu_contrato']);
                    }
                    $mensagem .= ' atingiu um total de ' . $print->porcentagem($pc_aditivo) . ' de Aditivo.';
                    $this->EmailProvider->to = $remetente['Usuario']['ds_nome'] . ' <' . $remetente['Usuario']['ds_email'] . '>';
                    $this->EmailProvider->send($mensagem);
                }
            endforeach
            ;
        }
    }

    public function validaData(){
        $data = str_replace("/", "-", $this->data['Aditivo']['dt_aditivo']);
        $time = implode('-', array_reverse(explode('/', substr($data, 0, 10)))).substr($data, 10);
        $di = new DateTime($time);
        $prazo = $this->data['Aditivo']['dt_prazo_em_dias'];
        $di->add(new DateInterval('P'.$prazo.'D'));
        $r = $di->format('d/m/Y');
        echo $r;
        exit;
    }

    public function verificaValidacoes($tipoAditivo){

        switch ($tipoAditivo){
            case '1':
                unset(
                    $this->Aditivo->validate['dt_prazo']
                );
                $this->data['Aditivo']['dt_prazo'] = null;
                break;
            case '2':
                unset(
                    $this->Aditivo->validate['vl_aditivo'],
                    $this->Aditivo->validate['tp_aditivo_valor']
                );
                $this->data['Aditivo']['vl_aditivo'] = null;
                $this->data['Aditivo']['tp_aditivo_valor'] = null;
                $this->data['Aditivo']['pc_aditivo'] = 0; //Modelo não permite valor nulo
                break;
            case '4':
                unset(
                    $this->Aditivo->validate['vl_aditivo'],
                    $this->Aditivo->validate['tp_aditivo_valor'],
                    $this->Aditivo->validate['dt_prazo'],
                    $this->Aditivo->validate['dt_aditivo']
                );
                $this->data['Aditivo']['vl_aditivo'] = null;
                $this->data['Aditivo']['tp_aditivo_valor'] = null;
                $this->data['Aditivo']['pc_aditivo'] = 0; //Modelo não permite valor nulo
                $this->data['Aditivo']['dt_prazo'] = null;
                $this->data['Aditivo']['dt_aditivo'] = null;
                break;
            default:
                break;
        }
    }
}
?>
