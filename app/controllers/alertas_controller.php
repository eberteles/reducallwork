<?php
/**
 * @resource { "name" : "Central de Alertas", "route":"alertas", "access": "private", "type": "module" }
 */
class AlertasController extends AppController
{

    public $name = 'Alertas';
    /**
     * @resource { "name" : "Central de Alertas", "route":"alertas\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();

        if (! empty($this->data)) {
            if ($this->Alerta->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Alerta->find('first');
            // debug($this->data);exit;
        }
        $this->set('vencimentosDashboard', Configure::read('App.config.resource.dashboard.vencimentos'));
    }
}
?>