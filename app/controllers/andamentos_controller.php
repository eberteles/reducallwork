<?php
/**
 * @resource { "name" : "Andamentos", "route":"andamentos", "access": "private", "type": "module" }
 */
class AndamentosController extends AppController
{

    var $name = 'Andamentos';

    var $layout = 'iframe';
    /**
     * @resource { "name" : "Andamentos", "route":"andamentos\/index", "access": "private", "type": "select" }
     */
    function index($coContrato)
    {
        $this->Andamento->recursive = 2;
        
        $this->paginate = array(
            'conditions' => array(
                'Andamento.co_contrato' => $coContrato
            ),
            'order' => array(
                'Andamento.co_andamento' => 'desc'
            )
        );
        
        $this->set('andamentos', $this->paginate());
        
        $this->set(compact('coContrato'));
        
        App::import('Model', 'Fase');
        $fase = new Fase();
        
        $this->set('fases', $fase->find('list'));
    }
    /**
     * @resource { "name" : "Iframe andamentos", "route":"andamentos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }
}
?>