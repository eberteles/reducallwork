<?php

/**
 * @resource { "name" : "Anexos", "route":"anexos", "access": "private", "type": "module" }
 */
class AnexosController extends AppController
{

    var $name = 'Anexos';

    var $layout = 'iframe';

    var $tpAnexo = null;

    function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        $this->tpAnexo = $modulo->getTiposDeAnexos();
        parent::__construct();
    }

    /**
     * @resource { "name" : "listagem anexos", "route":"anexos\/index", "access": "private", "type": "select" }
     */
    function index($coContrato, $modulo = "padrao", $idModulo = 0, $tpDocumento = 0, $indexarArquivo = 0, $texto = '')
    {
        $this->helpers[] = 'Imprimir';

        asort($this->tpAnexo);
        $this->Anexo->recursive = 0;

        $fields = array('Anexo.co_anexo', 'Anexo.co_anexo_pasta', 'Anexo.ds_extensao', 'Anexo.tp_documento', 'Anexo.ic_atesto',
            'Anexo.ds_observacao', 'Anexo.ds_anexo', 'Anexo.dt_anexo', 'Anexo.assinatura', 'Atividade.ds_atividade');

        $this->paginate = array(
            'fields' => $fields,
            'limit' => 1000
        );
        $conditions = null;

        $conditions['Anexo.co_licitacao'] = null;

        $conditionsPasta = null;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $conditions['Anexo.co_ata'] = $coContrato;
            $conditionsPasta['AnexoPasta.co_ata'] = $coContrato;
        } elseif ($modulo == "evento") {
            $conditions['Anexo.co_evento'] = $coContrato;
        } else {
            $conditions['Anexo.co_contrato'] = $coContrato;
            $conditionsPasta['AnexoPasta.co_contrato'] = $coContrato;
        }

        if ($texto != '') {
            $conditions['Anexo.ds_anexo like'] = '%' . $texto . '%';
        }
        if ($modulo == "atividade" && $idModulo > 0) {
            $conditions['Anexo.co_atividade'] = $idModulo;
        }
        if ($modulo == "fiscal" && $idModulo > 0) {
            $conditions['Anexo.co_contrato_fiscal'] = $idModulo;
        }
        if ($modulo == "aditivo" && $idModulo > 0) {
            $conditions['Anexo.co_aditivo'] = $idModulo;
        }
        if ($modulo == "ordem_de_fornecimento" && $idModulo > 0) {
            $conditions['Anexo.co_ordem_fornecimento_servico'] = $idModulo;
        }
        if ($modulo == "apostilamento" && $idModulo > 0) {
            $conditions['Anexo.co_apostilamento'] = $idModulo;
        }
        if ($modulo == "garantia" && $idModulo > 0) {
            $conditions['Anexo.co_garantia'] = $idModulo;
        }
        if ($modulo == "garantia_suporte" && $idModulo > 0) {
            $conditions['Anexo.co_garantia_suporte'] = $idModulo;
        }
        if ($modulo == "produto" && $idModulo > 0) {
            $conditions['Anexo.co_produto'] = $idModulo;
        }
        if ($modulo == "oficio" && $idModulo > 0) {
            $conditions['Anexo.co_oficio'] = $idModulo;
        }
        if ($modulo == "empenho" && $idModulo > 0) {
            $conditions['Anexo.co_empenho'] = $idModulo;
        }
        if ($modulo == "penalidade" && $idModulo > 0) {
            $conditions['Anexo.co_penalidade'] = $idModulo;
        }
        if ($modulo == "nota" && $idModulo > 0) {
            $conditions['Anexo.co_nota'] = $idModulo;
        }
        if (($modulo == "pendencia" || $modulo == "pendencia_ata") && $idModulo > 0) {
            $conditions['Anexo.co_pendencia'] = $idModulo;
        }
        if ($modulo == "pagamento" && $idModulo > 0) {
            $conditions['Anexo.co_pagamento'] = $idModulo;
        }
        if (($modulo == "historico" || $modulo == "historico_ata") && $idModulo > 0) {
            $conditions['Anexo.co_historico'] = $idModulo;
        }

        if ($tpDocumento > 0) {
            $conditions['Anexo.tp_documento'] = $tpDocumento;
        }

        $anexos_pastas = array();
        if ($tpDocumento == 0 && $texto == '' && $idModulo == 0) {
            $conditions['Anexo.co_anexo_pasta'] = null;
            App::import('Model', 'AnexoPasta');
            $dbPasta = new AnexoPasta();

            $options['conditions'] = $conditionsPasta;
            $options['recursive'] = 2;
            $anexos_pastas = $dbPasta->find('threaded', $options);
        }

        $anexos = $this->paginate($conditions);

        $this->set('anexos', $anexos);

        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set(compact('idModulo'));
        $this->set(compact('tpDocumento'));
        $this->set(compact('indexarArquivo'));
        $this->set(compact('texto'));
        $this->set('tpAnexo', $this->tpAnexo);

        $this->set(compact('anexos_pastas'));

    }

    /**
     * @resource { "name" : "Suporte Documental", "route":"anexos\/pesquisar", "access": "private", "type": "select" }
     */
    function pesquisar()
    {
        $this->layout = 'default';
        $this->set('tpAnexo', $this->tpAnexo);
        $anexos = array();
        if (!empty($this->data)) {
            $filtroAnexo = $this->getFiltroAnexo();

            if (count($filtroAnexo) > 0 && $this->data['Anexo']['palavra_chave'] == '') { // Direto Anexo
                $anexos = $this->Anexo->find('all',
                    array(
                        'fields' => array('co_anexo', 'dt_anexo', 'tp_documento', 'ds_anexo', 'ds_extensao'),
                        'conditions' => $filtroAnexo
                    )
                );
            } elseif (count($filtroAnexo) < 1) {
                $anexos = $this->Anexo->find('all', array(
                    'fields' => array(
                        'co_anexo', 'dt_anexo', 'tp_documento', 'ds_anexo', 'ds_extensao'
                    )
                ));
            } elseif ($this->data['Anexo']['palavra_chave'] != '' && count($filtroAnexo) < 1) { // Somente Indexação
                $anexos = $this->getAnexoComIndexacao();
            } elseif ($this->data['Anexo']['palavra_chave'] != '' && count($filtroAnexo) > 0) { // Juntar Indexação com Anexo
                $anexos = $this->getAnexoComIndexacao(array_keys($this->Anexo->find('list', array('conditions' => $filtroAnexo))));
            } else {
                $this->Session->setFlash(__('Favor informar ao menos um filtro de pesquisa!', true));
            }

            if (count($anexos) < 1) {
                $this->Session->setFlash(__('Não foi encontrado Resultado para o Filtro informado!', true));
            }
        }
        $this->set(compact('anexos'));
    }

    private function getAnexoComIndexacao($coAnexos = array())
    {
        App::import('Model', 'AnexoIndexacao');
        $dbIndexacao = new AnexoIndexacao();
        $conditions = array("MATCH (ds_conteudo) AGAINST ('" . $this->data['Anexo']['palavra_chave'] . "' IN BOOLEAN MODE)");
        if (count($coAnexos) > 0) {
            $conditions['co_anexo'] = $coAnexos;
        }
        $indexacoes = $dbIndexacao->find('all', array('conditions' => $conditions));
        $anexos = array();
        $anexo = array();
        $coAnexo = 0;
        foreach ($indexacoes as $indexacao):
            if ($coAnexo != $indexacao['AnexoIndexacao']['co_anexo']) {
                if ($coAnexo > 0) {
                    $anexos[] = $anexo;
                }
                $coAnexo = $indexacao['AnexoIndexacao']['co_anexo'];

                $anexo = $this->Anexo->find('first', array('fields' => array('co_anexo', 'dt_anexo', 'tp_documento', 'ds_anexo', 'ds_extensao'),
                    'conditions' => array('co_anexo' => $coAnexo)));
            }
            $anexo['AnexoIndexacao'][] = $indexacao['AnexoIndexacao'];
        endforeach;
        if (count($anexo) > 0) {
            $anexos[] = $anexo;
        }

        $filtroAnexo['ds_anexo like'] = '%' . $this->data['Anexo']['palavra_chave'] . '%';
        if (count($coAnexos) > 0) {
            $filtroAnexo['co_anexo'] = $coAnexos;
        }
        $anexosTitulo = $this->Anexo->find('all', array('fields' => array('co_anexo', 'dt_anexo', 'tp_documento', 'ds_anexo', 'ds_extensao'),
            'conditions' => $filtroAnexo));
        $anexos = array_merge($anexos, $anexosTitulo);
        //foreach ($anexosTitulo as $anexoTitulo):

        //endforeach;

        return $anexos;
    }

    private function getFiltroAnexo()
    {
        $filtro = array();
        if (!empty($this->data['Anexo']['tp_extensao']) && $this->data['Anexo']['tp_extensao'] != '') {
            $this->getFiltroExtensao($filtro, $this->data['Anexo']['tp_extensao']);
        }
        if (!empty($this->data['Anexo']['tipo_documento']) && $this->data['Anexo']['tipo_documento'] != '') {
            $filtro['tp_documento'] = $this->data['Anexo']['tipo_documento'];
        }
        if (!empty($this->data['Anexo']['dt_inicio']) && !empty($this->data['Anexo']['dt_fim']) && $this->data['Anexo']['dt_inicio'] != '' && $this->data['Anexo']['dt_fim'] != '') {
            $filtro['dt_anexo BETWEEN ? AND ?'] = array(dtDb($this->data['Anexo']['dt_inicio']), dtDb($this->data['Anexo']['dt_fim']));
        } else if (!empty($this->data['Anexo']['dt_inicio'])) {
            $filtro['dt_anexo >='] = dtDb($this->data['Anexo']['dt_inicio']);
        } elseif (!empty($this->data['Anexo']['dt_fim'])) {
            $filtro['dt_anexo <='] = dtDb($this->data['Anexo']['dt_fim']);
        }

        return $filtro;
    }

    private function getFiltroExtensao(&$filtro, $tpExtensao)
    {
        switch ($tpExtensao) {
            case '1':
                $filtro['ds_extensao'] = 'pdf';
                break;
            case '2':
                $filtro['OR'] = array(array('ds_extensao' => 'txt'), array('ds_extensao' => 'doc'), array('ds_extensao' => 'docx'), array('ds_extensao' => 'odt'));
                break;
            case '3':
                $filtro['OR'] = array(array('ds_extensao' => 'xls'), array('ds_extensao' => 'xlsx'), array('ds_extensao' => 'ods'));
                break;
            case '4':
                $filtro['OR'] = array(array('ds_extensao' => 'ppt'), array('ds_extensao' => 'pptx'), array('ds_extensao' => 'odp'));
                break;
            case '5':
                $filtro['OR'] = array(array('ds_extensao' => 'htm'), array('ds_extensao' => 'html'));
                break;
            case '6':
                $filtro['OR'] = array(array('ds_extensao' => 'jpg'), array('ds_extensao' => 'png'), array('ds_extensao' => 'tif'), array('ds_extensao' => 'svg'), array('ds_extensao' => 'odg'));
                break;
        }
    }

    /**
     * @resource { "name" : "Iframe anexos", "route":"anexos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato, $modulo = "padrao", $idModulo = 0)
    {
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set(compact('idModulo'));
    }

    /**
     * @resource { "name" : "adicionar anexos", "route":"anexos\/add", "access": "private", "type": "insert" }
     */
    function add($coContrato, $modulo = "padrao", $idModulo = 0)
    {
        $this->helpers[] = 'Imprimir';
        asort($this->tpAnexo);
        if (!empty($this->data)) {
            $this->Anexo->create();
            if ($this->Anexo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo,
                    $idModulo, 0,
                    $this->Anexo->id
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set(compact('idModulo'));
        $this->set('tpAnexo', $this->tpAnexo);

        $criteria = null;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $criteria['AnexoPasta.co_ata'] = $coContrato;
        } else {
            $criteria['AnexoPasta.co_contrato'] = $coContrato;
        }
        App::import('Model', 'AnexoPasta');
        $dbPasta = new AnexoPasta();
        $this->set('anexos_pastas', $dbPasta->find('threaded', array('order' => 'ds_anexo_pasta ASC',
            'conditions' => $criteria, 'recursive' => -1)));
    }

    /**
     * @resource { "name" : "Editar anexos", "route":"anexos\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $coContrato, $modulo = "padrao", $idModulo = 0)
    {
        $this->helpers[] = 'Imprimir';
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo,
                $idModulo
            ));
        }
        if (!empty($this->data)) {
            //remove o validador de conteúdo pois essa informação não é postada na edição
            unset($this->Anexo->validate['conteudo']);

            if ($this->Anexo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo,
                    $idModulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Anexo->read(null, $id);
            $this->data['Anexo']['ds_anexo'] = stripslashes($this->data['Anexo']['ds_anexo']);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set(compact('idModulo'));
        $this->set(compact('id'));

        $criteria = null;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $criteria['AnexoPasta.co_ata'] = $coContrato;
        } else {
            $criteria['AnexoPasta.co_contrato'] = $coContrato;
        }
        App::import('Model', 'AnexoPasta');
        $dbPasta = new AnexoPasta();
        $this->set('anexos_pastas', $dbPasta->find('threaded', array('order' => 'ds_anexo_pasta ASC',
            'conditions' => $criteria, 'recursive' => -1)));

    }

    /**
     * @resource { "name" : "Delete anexos", "route":"anexos\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato, $modulo = "padrao", $idModulo = 0)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo,
                $idModulo
            ));
        } else {
            $anexo = $this->Anexo->find(array('co_anexo' => $id));
        }

        if ($this->Anexo->delete($id)) {
            $moduloHelper = new ModuloHelper();

            if ($moduloHelper->isDiretorioFisico()) {
                unlink($anexo['Anexo']['caminho']);
            }
            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_tabela'] = 'anexos';
            $log['ds_log'] = json_encode($anexo['Anexo']);
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo,
                $idModulo
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato,
            $modulo,
            $idModulo
        ));
    }

    /**
     * @resource { "name" : "Visualizar Documentos", "route":"anexos\/iframe_abrir", "access": "private", "type": "select" }
     */
    public function iframe_abrir($height = 530)
    {
        $this->layout = 'blank';
        $url = $this->data["url"];
        $this->set(compact('url'));
        $this->set(compact('height'));
        $this->render();
    }

    private function setCabecalhoExtensao($extensao)
    {
        switch ($extensao) {
            case 'pdf':
                header("Content-type: application/pdf");
                break;
            case 'doc':
                header("Content-type: application/msword");
                break;
            case 'docx':
                header("Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                break;
            case 'odt':
                header("Content-type: application/vnd.oasis.opendocument.text");
                break;
            case 'xls':
                header("Content-type: application/vnd.ms-excel");
                break;
            case 'xlsx':
                header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                break;
            case 'ods':
                header("Content-type: application/vnd.oasis.opendocument.spreadsheet");
                break;
            case 'ppt':
                header("Content-type: application/vnd.ms-powerpoint");
                break;
            case 'pptx':
                header("Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation");
                break;
            case 'odp':
                header("Content-type: application/application/vnd.oasis.opendocument.presentation");
                break;
            case 'htm':
            case 'html':
            case 'txt':
                header('Content-type: text/html; charset=UTF-8');
                break;
            case 'jpg':
            case 'png':
            case 'tif':
                header("Content-type: image/" . $extensao);
                break;
            case 'svg':
                header("Content-type: image/svg+xml");
                break;
            case 'odg':
                header("Content-type: application/vnd.oasis.opendocument.graphics");
                break;
        }
    }

    /**
     * @resource { "name" : "Abrir arquivo", "route":"anexos\/lerArquivo", "access": "private", "type": "select" }
     */
    public function lerArquivo($anexo, $abrir = false)
    {
        if ($anexo) {
            $arContrato = $this->Anexo->read(null, $anexo);

            App::import('Helper', 'Modulo');
            $modulo = new ModuloHelper();

            $arquivo = null;

            if ($modulo->isDiretorioFisico()) {
                // tratamento para clientes com arquivos salvos no banco
                if (empty($arContrato['Anexo']['caminho'])) {
                    $caminho = $this->Anexo->salvarArquivoFisico($arContrato['Anexo']['ds_anexo'], $arContrato['Anexo']['conteudo'], $arContrato['Anexo']['ds_extensao']);
                    $this->Anexo->id = $arContrato['Anexo']['co_anexo'];
                    $this->Anexo->saveField('caminho', $caminho);
                    // le novamente o regitros
                    $arContrato = $this->Anexo->read(null, $anexo);
                }

                $arquivo = file_get_contents($arContrato['Anexo']['caminho']);
            } else {
                $arquivo = $arContrato['Anexo']['conteudo'];
            }

            if ($abrir) {
                $this->setCabecalhoExtensao($arContrato['Anexo']['ds_extensao']);
            } else {
                header("Content-Disposition: attachment; filename=arquivo." . $arContrato['Anexo']['ds_extensao']);
                header("Content-type: application/download");
            }

            if ($arContrato['Anexo']['ds_extensao'] == 'txt') {
                echo nl2br(utf8_encode($arquivo));
            } else {
                echo $arquivo;
            }

            exit();
        } else {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
     * @resource { "name" : "Assinar", "route":"anexos\/assinar", "access": "private", "type": "update" }
     */
    public function assinar()
    {
        $id = $this->params['form']['co_anexo'];
        $assinatura = $this->params['form']['assinatura'];
        $publicKey = $this->params['form']['publicKey'];
        $dsUsuarioToken = $this->params['form']['ds_usuario_token'];
        $usuario = $this->Session->read('usuario');
        $anexo = $this->Anexo->read(null, $id);
        $this->data['assinatura'] = $assinatura;
        $this->data['chave'] = $publicKey;
        $this->data['ds_usuario_token'] = $dsUsuarioToken;
        $this->data['co_usuario_assinatura'] = $usuario['Usuario']['co_usuario'];
        if ($this->Anexo->save($this->data))
            echo '({"success":"true", "saved": "true"})';
        else
            echo '({"success":"true", "saved": "false"})';
        exit();
    }

    /**
     * @resource { "name" : "Verificar Assinatura", "route":"anexos\/verificarAssinatura", "access": "private", "type": "select" }
     */
    public function verificarAssinatura($id = null)
    {
        if ($id)
            $this->data['coAnexo'] = $id;
        $res = $this->_verificarAssinatura($this->data['coAnexo']);
        switch ($res) {
            case '1': // Assinado e o arquivo não foi modificado desde a assinatura
                echo '({"success":"true", "signature": "true", "verified": "true"})';
                break;
            case '2': // Assinado e o arquivo foi alterado desde a assinatura
                echo '({"success":"true", "signature": "true", "verified": "false"})';
                break;
            case '3': // Não assinado ainda
                echo '({"success":"true", "signature": "false", "verified": "false"})';
                break;
        }
        exit();
    }

    /**
     * @resource { "name" : "Get Document base 64", "route":"anexos\/getDocumentBase64", "access": "private", "type": "select" }
     */
    public function getDocumentBase64()
    {
        $id = $this->params['form']['co_anexo'];
        $anexo = $this->Anexo->read(null, $id);
        $usuario = $this->Session->read('usuario');
        $documentBase64 = base64_encode($anexo['Anexo']['conteudo']);
        $signature = $anexo['Anexo']['assinatura'];
        $publicKey = $anexo['Anexo']['chave'];
        $co_usuario = $usuario['Usuario']['co_usuario'];
        $ds_nome = $usuario['Usuario']['ds_nome'];
        $ds_usuario_token = $anexo['Anexo']['ds_usuario_token'];
        $data = array(
            'success' => 'true',
            'document' => $documentBase64,
            'assinatura' => $signature,
            'chave' => $publicKey,
            'co_usuario' => $co_usuario,
            'ds_nome' => $ds_nome,
            'hash' => md5($signature),
            'ds_usuario_token' => $ds_usuario_token
        );
        echo '(' . json_encode($data) . ')';
        exit();
    }

    private function _verificarAssinatura($id, $internal = false)
    {
        return 1;
        $rsa = new Crypt_RSA();
        $anexo = $this->Anexo->read(null, $id);
        echo '<pre>';
        $publicKey = base64_decode($anexo['Anexo']['chave']);
        $assinatura = base64_decode($anexo['Anexo']['assinatura']);
        $dados = base64_decode(base64_encode($anexo['Anexo']['conteudo']));

        var_dump($rsa->loadKey($publicKey, CRYPT_RSA_PUBLIC_FORMAT_PKCS1));
        $res = $rsa->verify($dados, $assinatura);
        echo var_dump($dados);
        exit();

        if (!empty($anexo['Anexo']['assinatura'])) {
            if ($rsa->verify($anexo['Anexo']['conteudo'], $anexo['Anexo']['assinatura'])) {
                if ($internal) {
                    $this->loadModel('Usuario');
                    $usuario = $this->Usuario->find(array(
                        'co_usuario' => $anexo['Anexo']['co_usuario_assinatura']
                    ));
                    return array(
                        'verificado' => true,
                        'usuario' => $usuario['Usuario']
                    );
                } else
                    return '1';
            } else
                return '2';
        } else
            return '3';
    }
}

?>
