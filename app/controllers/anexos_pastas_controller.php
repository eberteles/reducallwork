<?php
/**
 * @resource { "name" : "Cadastro de Pastas de Anexos", "route":"anexos_pastas", "access": "private", "type": "module" }
 */
class AnexosPastasController extends AppController
{

    public $name = 'AnexosPastas';

    var $layout = 'iframe';

    public $uses = array(
        'AnexoPasta'
    );

    public $helpers = array(
        'Imprimir'
    );

    /**
     * @resource { "name" : "listagem anexos pastas", "route":"anexos_pastas\/index", "access": "private", "type": "select" }
     */
   public function index($idModulo = 0, $modulo = "padrao")
    {
        $this->gerarFiltro();
        $criteria = null;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $criteria['AnexoPasta.co_ata'] = $idModulo;
        } else {
            $criteria['AnexoPasta.co_contrato'] = $idModulo;
        }
        if (! empty($this->data)) {
                $criteria['ds_anexo_pasta like'] = '%' . up($this->data['AnexoPasta']['ds_anexo_pasta']) . '%';
            }
        $this->set('anexos_pastas', $this->AnexoPasta->find('threaded', array(
                                                                'conditions' => $criteria, 'recursive' => -1) ));
        $this->set ( compact ( 'idModulo' ) );
        $this->set ( compact ( 'modulo' ) );
    }
    /**
     * @resource { "name" : "Iframe anexos pastas", "route":"anexos_pastas\/iframe", "access": "private", "type": "select" }
     */
    function iframe( $idModulo = 0, $modulo = "padrao" )
    {
        $this->layout = 'blank';
        $this->set ( compact ( 'idModulo' ) );
        $this->set ( compact ( 'modulo' ) );
    }
    /**
     * @resource { "name" : "Iframe adicionar anexos pastas", "route":"anexos_pastas\/iframe_add", "access": "private", "type": "insert" }
     */
    function iframe_add( $idModulo = 0, $modulo = "padrao", $modal = false )
    {
        $this->layout = 'blank';
        $this->set ( compact ( 'idModulo' ) );
        $this->set ( compact ( 'modulo' ) );
        $this->set ( compact ( 'modal' ) );
    }
    /**
     * @resource { "name" : "adicionar anexos pastas", "route":"anexos_pastas\/add", "access": "private", "type": "insert" }
     */
    public function add($idModulo = 0, $modulo = "padrao", $modal = false)
    {
        if($modal) {
            $this->layout = 'iframe';
        }
        if ( !empty($this->data) ) {
            $this->AnexoPasta->create();
            if ($this->AnexoPasta->save($this->data)) {
                if ($modal) {
                    $this->redirect(array('action' => 'close', $this->AnexoPasta->id));
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index', $idModulo, $modulo
                    ));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $criteria = null;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $criteria['AnexoPasta.co_ata'] = $idModulo;
        } else {
            $criteria['AnexoPasta.co_contrato'] = $idModulo;
        }
        $this->set('anexos_pastas', $this->AnexoPasta->find('threaded', array('order' => 'ds_anexo_pasta ASC',
                                                                              'conditions' => $criteria, 'recursive' => -1 )));
        $this->set ( compact ( 'idModulo' ) );
        $this->set ( compact ( 'modulo' ) );
        $this->set ( compact ( 'modal' ) );
    }

    /**
     * @resource { "name" : "Close iframe", "route":"anexos_pastas\/close", "access": "private", "type": "select" }
     */
    function close( $co_anexo_pasta )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_anexo_pasta' ) );
    }

    /**
     * @resource { "name" : "listar anexos pastas", "route":"anexos_pastas\/listar", "access": "private", "type": "select" }
     */
    function listar() {
        App::import('Helper', 'Imprimir');
        $imprimir   = new ImprimirHelper();
        echo json_encode ($imprimir->getArrayAnexosPastas($this->AnexoPasta->find ( 'threaded', array('order' => 'ds_anexo_pasta ASC', 'recursive' => -1) ), 1 ) );

        exit ();
    }
    /**
     * @resource { "name" : "Editar anexos pastas", "route":"anexos_pastas\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null, $idModulo = 0, $modulo = "padrao")
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index', $idModulo, $modulo
            ));
        }
        if (! empty($this->data)) {
            if ($this->AnexoPasta->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index', $idModulo, $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->AnexoPasta->read(null, $id);
            $this->data['AnexoPasta']['ds_anexo_pasta'] = stripslashes($this->data['AnexoPasta']['ds_anexo_pasta']);
        }

        $criteria = null;
        // critério obrigatório, não pode trazer na combo de vinculo o registro que esta sendo alterado.
        $criteria['AnexoPasta.co_anexo_pasta <> ?'] = $id;
        if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
            $criteria['AnexoPasta.co_ata'] = $idModulo;
        } else {
            $criteria['AnexoPasta.co_contrato'] = $idModulo;
        }
        $this->set('anexos_pastas', $this->AnexoPasta->find('threaded', array(
            'order' => 'ds_anexo_pasta ASC', 'conditions' => $criteria, 'recursive' => -1
        )));

        $this->set('id', $id);
        $this->set ( compact ( 'idModulo' ) );
        $this->set ( compact ( 'modulo' ) );
    }
    /**
     * @resource { "name" : "deletar anexos pastas", "route":"anexos_pastas\/delete", "access": "private", "type": "delete" }
     */
    public function delete($id = null, $idModulo = 0, $modulo = "padrao")
    {
        try {
            if ( !$id ) {
                $this->Session->setFlash(__('Identificador inválido', true));
                $this->redirect(array(
                    'action' => 'index', $idModulo, $modulo
                ));
            } else {
                App::import('Model', 'Anexo', 'AnexoPasta');
                $dbAnexo        = new Anexo();
                $dbAnexoPasta   = new AnexoPasta();
                if( $dbAnexoPasta->find('count', array(
                        'conditions'=> array('parent_id' => $id),
                        'recursive'=>-1 )) > 0 ||
                    $dbAnexo->find('count', array(
                        'conditions'=> array('co_anexo_pasta' => $id),
                        'recursive'=>-1 )) > 0 ){
                    $this->Session->setFlash(__('Não é possível excluir a Pasta pois existem Documentos e/ou Pastas dentro dela.', true));
                    $this->redirect(array(
                        'action' => 'index', $idModulo, $modulo
                    ));
                } else {
                    if ($this->AnexoPasta->delete($id)) {
                        $this->Session->setFlash(__('Registro excluído com sucesso', true));
                        $this->redirect(array(
                            'action' => 'index', $idModulo, $modulo
                        ));
                    }
                }
            }
            $this->Session->setFlash(__('Erro ao excluir registro', true));
            $this->redirect(array(
                'action' => 'index', $idModulo, $modulo
            ));
        } catch( Exception $exception ) {
            echo "<pre>"; var_dump($exception); exit;
            $this->Session->setFlash($exception->getMessage());
            $this->redirect(array(
                'action' => 'index', $idModulo, $modulo
            ));
        }
    }
}