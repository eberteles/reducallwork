<?php

/**
 * @resource { "name" : "Apostilamentos", "route":"apostilamentos", "access": "private", "type": "module" }
 */
class ApostilamentosController extends AppController
{

    var $name = 'Apostilamentos';

    var $layout = 'iframe';

    /**
     * @resource {
     * "name" : "Apostilamentos",
     * "route":"apostilamentos\/index",
     * "access": "private",
     * "type": "select"
     * }
     */
    function index($coContrato)
    {
        $this->Apostilamento->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Apostilamento.co_contrato' => $coContrato
            ),
            'order' => array(
                'Apostilamento.dt_apostilamento' => 'desc'
            )
        );

        $this->set('apostilamentos', $this->paginate());

        $this->set(compact('coContrato'));
    }

    /**
     * @resource {
     * "name" : "iframe",
     * "route":"apostilamentos\/iframe",
     * "access": "private",
     * "type": "select"
     * }
     */
    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
     * @resource {
     * "name" : "Adicionar Apostilamento",
     * "route":"apostilamentos\/add",
     * "access": "private",
     * "type": "insert"
     * }
     */
    function add($coContrato, $tp_apostilamento = Apostilamento::TP_APOSTILAMENTO_VALOR)
    {
        //Adiciona obrigatoriedade GES-59
        if ($tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_PRAZO ||
            $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO
        ) {
            $this->Apostilamento->validate['dt_prazo']['notempty'] = array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data final da vigência em branco'
            );
        }
        if (!empty($this->data)) {
            $this->Apostilamento->create();
            if ($this->Apostilamento->save($this->data)) {
                if (isset($this->data['Apostilamento']['dt_prazo'])) {
                    $this->updDtFimVigencia($coContrato);
                }

                $this->avisarGestor($coContrato);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        App::import('Model', 'Contrato');
        $contratoDb = new Contrato();
        $this->set('vl_global', $contratoDb->field('vl_global', array('Contrato.co_contrato' => $coContrato)));
        $this->set(compact('tp_apostilamento'));
        $this->set(compact('coContrato'));

        App::import('Model', 'NotasEmpenho');
        $empenhoDb = new NotasEmpenho();
        $listEmpenho = array();
        foreach ($empenhoDb->find('all') as $emp) {
            $listEmpenho[$emp['Empenho']['co_empenho']] = $emp['Empenho']['nu_empenho'];
        }

        $this->set('notasEmpenho', $listEmpenho);
    }

    /**
     * @resource {
     * "name" : "Editar Apostilamento",
     * "route":"apostilamentos\/edit",
     * "access": "private",
     * "type": "update"
     * }
     */
    function edit($id = null, $coContrato)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index', $coContrato
            ));
        }

        //Adiciona obrigatoriedade GES-59
        if (isset($this->data['Apostilamento']['tp_apostilamento']) &&
            ($this->data['Apostilamento']['tp_apostilamento'] == Apostilamento::TP_APOSTILAMENTO_PRAZO ||
                $this->data['Apostilamento']['tp_apostilamento'] == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO
            )
        ) {
            $this->Apostilamento->validate['dt_prazo']['notempty'] = array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data final da vigência em branco'
            );
        }

        if (!empty($this->data)) {
            if ($this->Apostilamento->save($this->data)) {

                if (isset($this->data['Apostilamento']['dt_prazo'])) {
                    $this->updDtFimVigencia($coContrato);
                }

                $this->avisarGestor($coContrato);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        App::import('Model', 'Contrato');
        $contratoDb = new Contrato();
        $vl_global = $contratoDb->field('vl_global', array('Contrato.co_contrato' => $coContrato));
        if (empty($this->data)) {
            $this->data = $this->Apostilamento->read(null, $id);
            if ($vl_global != 0) {
                $this->data['Apostilamento']['pc_apostilamento'] = $this->data['Apostilamento']['vl_apostilamento'] * 100 / $vl_global;
            }
        }
        $this->set('vl_global', $vl_global);
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('apostilamento', $this->data['Apostilamento']);

        App::import('Model', 'NotasEmpenho');
        $empenhoDb = new NotasEmpenho();
        $listEmpenho = array();

        foreach ($empenhoDb->find('all') as $emp) {
            $listEmpenho[$emp['Empenho']['co_empenho']] = $emp['Empenho']['nu_empenho'];
        }

        $this->set('notasEmpenho', $listEmpenho);
    }

    /**
     * @resource {
     * "name" : "Remover Apostilamento",
     * "route":"apostilamentos\/delete",
     * "access": "private",
     * "type": "delete"
     * }
     */
    function delete($id = null, $coContrato)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $apostilamento = $this->Apostilamento->find(array('co_apostilamento' => $id));
        }

        if ($this->Apostilamento->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_tabela'] = 'apostilamentos';
            $log['ds_log'] = json_encode($apostilamento['Apostilamento']);
            $logModel->save($log);

            $this->updDtFimVigencia($coContrato);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }

    private function updDtFimVigencia($coContrato)
    {
        App::import('Model', 'Contrato');
        $contratoDb = new Contrato();
        $contratoDb->updDtFimVigencia($coContrato);
    }

    function avisarGestor($coContrato)
    {
        ob_start();
        App::import('Helper', 'Print');
        $print = new PrintHelper();

        $apostilamento = $this->Apostilamento->getTotalGlobal($coContrato);
        $pc_apostilamento = ($apostilamento['vl_total_apostilamento'] * 100) / $apostilamento['vl_global'];

        if ($pc_apostilamento >= 20) {
            $this->EmailProvider->subject = 'Processo/Contrato atingiu 20% de Apostilamento ';
            $mensagem = '';

            $remetentes = $this->Apostilamento->getGestorAtivo($coContrato);
            foreach ($remetentes as $remetente) :
                if ($remetente['Usuario']['ds_email'] != '') {
                    $mensagem = $remetente['Usuario']['ds_nome'] . ', <BR><BR> ' . ' Informamos que o Processo: ' . $print->processo($apostilamento['nu_processo']);
                    if ($apostilamento['nu_contrato'] != '') {
                        $mensagem .= ' - Contrato: ' . $print->contrato($apostilamento['nu_contrato']);
                    }
                    $mensagem .= ' atingiu um total de ' . $print->porcentagem($pc_apostilamento) . ' de Apostilamento.';
                    $this->EmailProvider->to = $remetente['Usuario']['ds_nome'] . ' <' . $remetente['Usuario']['ds_email'] . '>';
                    $this->EmailProvider->send($mensagem);
                }
            endforeach;
        }
    }
}

