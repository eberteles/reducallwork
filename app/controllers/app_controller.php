<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @link http://book.cakephp.org/view/957/The-App-Controller
 */
class AppController extends Controller {

    var $helpers = array ('Session', 'Html', 'Form', 'DatePicker', 'Aba', 'Menu', 'Print', 'JqueryEngine', 'Formatacao', 'Modulo' );

    var $components = array ('Session', 'Functions', 'ReportHtml', 'RequestHandler', 'CustomAcl', 'Phpmailer');

    var $EmailProvider = null;

    function beforeFilter() {

            //App::import('Helper', 'Modulo');
            //$modulo = new ModuloHelper();

            $this->EmailProvider = $this->Phpmailer;

            $this->setConstants();

            if( Configure::read('App.customAcl') ) {
                    if( $this->params['controller'] != 'error' ) {

                            $resource  = $this->params['controller'] . '/' . $this->params['action'];
                            $usuario = $this->Session->read( 'usuario' );

                if ( !$usuario ) {
                    $isPublic  = $this->CustomAcl->isPublic($resource);
                    if( !$isPublic ){
                            $this->redirect ( array ('controller' => 'usuarios', 'action' => 'autenticar' ) );
                    }
                } else {
                    if( !isset($this->params['isAjax']) ) {
                            $isAllowed = false;
                            $isPublic  = $this->CustomAcl->isPublic($resource);

                            if( !$isPublic ) {

                                    $isAllowed = $this->CustomAcl->isAllowed($resource);

                                    if( $isAllowed ) {
                                            // Implementação para obrigar o usuário completar seus dados.
                                            $helperModulo = new ModuloHelper();
                                            if( $helperModulo->getValidaDadosUsuario()
                                            && !in_array($resource, array('usuarios/edit', 'emails/list_menu'))) {
                                                    App::import('Model', 'Usuario');
                                                    $modelUsuario = new Usuario();
                                                    if( !$modelUsuario->hasCompleteData() ) {
                                                            $this->Session->setFlash(__('Dados de cadastros incompletos, porfavor informe os campos obrigatórios.', true));
                                                            $this->redirect ( array ('controller' => 'usuarios', 'action' => 'edit', $usuario['Usuario']['co_usuario'] ) );
                                                    }
                                            }
                                    }
                            }

                                            if( !$isPublic && !$isAllowed && (isset($this->params['pass']['0']) ? $this->params['pass']['0']: null) != $usuario['Usuario']['co_usuario'] ){
                                                    if ( $resource == 'usuarios/edit' ) {
                                                            $this->Session->setFlash(__('Acesso Negado.', true));
                                                            $this->redirect( $this->referer() );
                                                    }
                            }
                    }
                }

                    }
            } else {

                    $resourcepublic = array(
                            'dsp_relatorio',
                            'contratos_relatorio',
                            'lct_relatorio',
                            'clientes',
                            'hmab_pesquisa',
                            'usuarios:autenticar',
                            'usuarios:esqueci',
                            'usuarios:checkusuario',
                            'usuarios:checkdocumento',
                            'usuarios:direciona',
                            'contratos:add_inscricao',
                            'contratos:pam_externo',
                            'contratos:add_pam_externo',
                            'error'
                    );

                    $isPublic = false;

                    if ( !$this->Session->read( 'usuario' ) ) {
                            $resource = $this->params['controller'];
                            $isPublic = in_array($resource, $resourcepublic);

                            if( !$isPublic ){
                    $resource .= ":" . $this->params['action'];
                                    $isPublic = in_array($resource, $resourcepublic);
                            }

                            if( !$isPublic ) {
                                    $this->redirect ( array ('controller' => 'usuarios', 'action' => 'autenticar' ) );
                            }
                    }
            }
    }
    /**
     * {@inheritDoc}
     * @see Controller::beforeRender()
     */
    function beforeRender() {
            if ( Configure::read('App.customAcl') && ($usuario = $this->Session->read( 'usuario' )) != false ) {
                    App::import('Model', 'Recurso');
                    $modelRecurso = new Recurso();

                    $criteria = array();
                    $recursos = $modelRecurso->find('threaded', array(
                            'conditions' => array(
                                    'Recurso.ic_menu' => true,
                            ),
                    'joins' => array(
                        array(
                            'table' => 'permissoes',
                            'alias' => 'Permissao',
                            'type' => 'inner',
                            'foreignKey' => false,
                            'conditions'=> array(
                                'Permissao.co_recurso = Recurso.co_recurso',
                                    'Permissao.co_perfil = ' . $usuario['UsuarioPerfil']['co_perfil']
                            )
                        )
                    ),
                            'order' => array(
                                    'Recurso.nu_ordem' => 'ASC'
                            )
                    ));

                    $menus = array();

                    foreach( $recursos as $recurso ){
                            if( count($recurso['Permissao']) > 0 ) {
                                    if( count($recurso['children']) > 0 ){
                                            $main = $recurso['Recurso']['no_recurso'];
                                            $menus[$main] = array('multi' => true);
                                            $menus = $this->getSubMenu($main, $menus, $recurso['children']);
                                    } else {
                                            // verifica se o link é externo
                                            if(preg_match("/(http(s)?:\/\/|www\.)[^ ]+/", $recurso['Recurso']['ds_rota'])){
                                                    $menus[__($recurso['Recurso']['no_recurso'], true)] = $recurso['Recurso']['ds_rota'];
                                            } else {

                                                    $rota = explode('/', $recurso['Recurso']['ds_rota']);
                                                    $action = 'index';
                                                    $controller = "";

                                                    if( count($rota) > 0 ){
                                                            if( count($rota) == 1 ){
                                                                    $controller = current($rota);
                                                            } else {
                                                                    $controller = current($rota);
                                                                    $action = next($rota);
                                                            }
                                                    }

                                                    $menus[__($recurso['Recurso']['no_recurso'], true)] = array('controller' => $controller, 'action' => $action);
                                            }
                                    }
                            }
                    }

            } else {
                    $menus = array(
                            'Estação de Trabalho' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            '<i class="icon-dashboard"></i> &nbsp;&nbsp; ' . __('Dashboard', true) . '' => array('controller' => 'dashboard', 'action' => 'index'),
                                                            '<i class="icon-dashboard"></i> &nbsp;&nbsp; ' . __('B.I.', true) . '' => array('controller' => 'dashboard', 'action' => 'indexBi'),
                                                            '<i class="icon-list"></i> &nbsp;&nbsp; Minhas Atas' => array('controller' => 'atas', 'action' => 'index'),
                                                            '<i class="icon-list"></i> &nbsp;&nbsp; Meus Registros' => array('controller' => 'contratos', 'action' => 'index'),
                                                            '<i class="pink icon-folder-open"></i> &nbsp;&nbsp; ' . __('Suporte Documental', true).'' => array('controller' => 'anexos', 'action' => 'pesquisar'),
                                                            '<i class="icon-edit"></i> &nbsp;&nbsp; Novo ' . __('PAM', true).'' => array('controller' => 'contratos', 'action' => 'add_pam'),
                                                            '<i class="icon-edit"></i> &nbsp;&nbsp; Novo ' . __('Processo', true).'' => array('controller' => 'contratos', 'action' => 'add_processo'),
                                                            '<i class="icon-edit"></i> &nbsp;&nbsp; Novo ' . __('Contrato', true) =>  array('controller' => 'contratos', 'action' => 'add'),
                                                            '<i class="icon-cloud-upload"></i> &nbsp;&nbsp; SIAFI' => array('controller' => 'siafi2', 'action' => 'index'),
                                                            '<i class="icon-cloud-upload"></i> &nbsp;&nbsp; ComprasNet' => array('controller' => 'siasg'),
                                                            '<i class="icon-tasks"></i> &nbsp;&nbsp; Atividades' => array('controller' => 'atividades', 'action' => 'atividades_gerais'),
                                            )
                            ),
                            'Tabelas Basicas' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            __('Fornecedores', true) 							=> array('controller' => 'fornecedores', 'action' => 'index'),
                                                            __('Unidades Administrativas', true) 				=> array('controller' => 'setores', 'action' => 'index'),
                                                            __('Tipo de Contrato', true) 						=> array('controller' => 'modalidades', 'action' => 'index'),
                                                            __('Modalidade de Contratação', true) 				=> array('controller' => 'contratacoes', 'action' => 'index'),
                                                            __('Situações do Processo / Contrato', true) 		=> array('controller' => 'situacoes', 'action' => 'index'),
                                                            __('Fases de Tramitação', true) 					=> array('controller' => 'fases', 'action' => 'index'),
                                                            __('Verificações de Fases', true) 					=> array('controller' => 'verificacoes', 'action' => 'index'),
                                                            __('Descrição do Serviço', true) 					=> array('controller' => 'servicos', 'action' => 'index'),
                                                            __('Tipos de Produtos / Produtos', true) 			=> array('controller' => 'produtosTipos', 'action' => 'index'),
                                                            __('Áreas / Sub Áreas de Fornecedores', true) 		=> array('controller' => 'areas', 'action' => 'index'),
                            __('Tipo Fiscal', true) 							=> array('controller' => 'fiscais_tipos', 'action' => 'index'),
                            __('Categorias de Contratos', true) 				=> array('controller' => 'categorias', 'action' => 'index'),
                                                            __('Sub Categorias de Contratos', true) 			=> array('controller' => 'subcategorias', 'action' => 'index'),
                                                            __('Atas - Categorias', true) 						=> array('controller' => 'atas_categorias', 'action' => 'index'),
                                                            __('Projetos', true) 								=> array('controller' => 'projetos', 'action' => 'index'),
                                            )
                            ),
                            'Administração' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            __('Instituições', true) 							=> array('controller' => 'instituicoes', 'action' => 'index'),
                                                            __('Grupo Auxiliar', true) 							=> array('controller' => 'grupo_auxiliares', 'action' => 'index'),
                                                            __('Perfis', true) 									=> array('controller' => 'perfis', 'action' => 'index'),
                                                            __('Usuários', true) 								=> array('controller' => 'usuarios', 'action' => 'index'),
                                                            __('Recursos', true) 								=> array('controller' => 'recursos', 'action' => 'index'),
                                                            __('Log de Acesso', true) 							=> array('controller' => 'logs', 'action' => 'index'),
                                                            __('Central de Alertas', true) 						=> array('controller' => 'alertas', 'action' => 'index'),
                                                            __('WorkFlow', true) 								=> array('controller' => 'fluxos', 'action' => 'index'),
                                            )
                            ),
                            'Extrator' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            '<i class="icon-dashboard"></i> &nbsp;&nbsp; ' . __('Visão de Importação', true) . '' => array('controller' => 'dashboard', 'action' => 'indexExtratorImportacao'),
                                                            '<i class="icon-dashboard"></i> &nbsp;&nbsp; ' . __('Visão de Geração', true) . '' => array('controller' => 'dashboard', 'action' => 'indexExtratorGeracao'),
                                                            '<i class="icon-dashboard"></i> &nbsp;&nbsp; ' . __('Auditoria', true) . '' => array('controller' => 'dashboard', 'action' => 'indexExtratorAuditoria')
                                            )
                            ),
                            'Relatorio' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            __('Relatórios', true) => array('controller' => 'relatorios', 'action' => 'index')
                                            )
                            ),
                            'Manual' => array(
                                            'multi' => true,
                                            'links' => array(
                                                            __('Manual', true) => array('controller' => 'manuais', 'action' => 'downloadManualGescon')
                                            )
                            )
                    );
            }
            $this->set('menus', $menus);
    }

    function getSubMenu( $main, $menus, $recursos )
    {
        $menu = array();

        if( count($recursos) > 0 ) {
            foreach( $recursos as $menu ) {
                if( count($menu['Permissao']) > 0 ){
                    $action = 'index';

                    if(preg_match("/(http(s)?:\/\/|www\.)[^ ]+/", $menu['Recurso']['ds_rota']) > 0){
                        $menus[$main]['links'][$icon . __($menu['Recurso']['no_recurso'], true)] = $menu['Recurso']['ds_rota'];
                    } else {
                        $rota = explode('/', $menu['Recurso']['ds_rota']);

                        if( count($rota) == 1 ){
                                $controller = current($rota);
                        } else {
                                $controller = current($rota);
                                $action = next($rota);
                        }
                        $icon = '';
                        if( $menu['Recurso']['ds_icon'] ){
                                $icon = '<i class="' . $menu['Recurso']['ds_icon'] . '"></i>&nbsp;&nbsp;';
                        }

                        $menus[$main]['links'][$icon . __($menu['Recurso']['no_recurso'], true)] = array('controller' => $controller, 'action' => $action);
                    }

                    if( count($menu['children']) > 0 ){
                        $menus = $this->getSubMenu($main, $menus, $menu['children']);
                    }
                }
            }
        }

        return $menus;
    }

    /**
     * Enter description here...
     *
     */
    function gerarFiltro() {
            if (! $this->data && $this->Session->read ( $this->params ['controller'] . '.' . $this->params ['action'] . '.' . 'criterio' )) {
                    $this->data = $this->Session->read ( $this->params ['controller'] . '.' . $this->params ['action'] . '.' . 'criterio' );
            }
            if ($this->data) {
                    $this->Session->write ( $this->params ['controller'] . '.' . $this->params ['action'] . '.' . 'criterio', $this->data );
            }
    }

    /**
     * Enter description here...
     *
     */
    function apagarFiltro() {
            $this->Session->delete( $this->params ['controller'] . '.' . $this->params ['action'] . '.' . 'criterio' );
    }

    /**
     *
     */
    function setConstants()
    {
            App::import('Model', 'Perfil');
            $modelPerfil = new Perfil();

            $recordsPerfis = $modelPerfil->find('list');

            foreach( $recordsPerfis as $coPerfil => $noPerfil ) {
                    $noPerfil = strtoupper($noPerfil);
                    if( !defined($noPerfil) ) {
                            define($noPerfil, $coPerfil);
                    }
            }
    }

    public function addAndamento($mensagem, $configuracao = null) {
        
        App::import('Model', 'Andamento');
        $modelAndamento = new Andamento();
        
        $usuario = $this->Session->read('usuario');
        if (isset($this->data['co_contrato']) && $this->data['co_contrato'] > 0) {
            $this->data['Andamento']['co_contrato'] = $this->data['co_contrato'];
        }
        if (isset($configuracao['co_contrato'])) {
            $this->data['Andamento']['co_contrato'] = $configuracao['co_contrato'];
        }
        if (isset($configuracao['model'])) {
            $this->data['Andamento']['co_contrato'] = $this->data[$configuracao['model']]['co_contrato'];
        }

        if (isset($usuario['Usuario']['co_fornecedor']) && $usuario['Usuario']['co_fornecedor'] > 0) {
            $this->data['Andamento']['co_fornecedor'] = $usuario['Usuario']['co_fornecedor'];
        } else {
            $this->data['Andamento']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        $this->data['Andamento']['dt_andamento'] = date('d/m/Y H:i:s');
        $this->data['Andamento']['ds_andamento'] = $mensagem;
        if (isset($configuracao['nu_sequencia'])) {
            $this->data['Andamento']['nu_sequencia'] = $configuracao['nu_sequencia'];
        }
        if (isset($configuracao['co_setor'])) {
            $this->data['Andamento']['co_setor'] = $configuracao['co_setor'];
        }
        if (isset($configuracao['co_fase'])) {
            $this->data['Andamento']['co_fase'] = $configuracao['co_fase'];
        }
        if (isset($configuracao['justificativa'])) {
            $this->data['Andamento']['ds_justificativa'] = $configuracao['justificativa'];
        }

        if (Configure::read('App.config.resource.certificadoDigital') && isset($this->params['form']['rd2signer_authenticationid0']) && $this->params['form']['rd2signer_authenticationid0'] != '') {
            $this->data['Andamento']['authentication_id'] = $this->params['form']['rd2signer_authenticationid0'];
            $this->data['Andamento']['texto'] = $this->params['form']['rd2signer_documento0'];
            $this->data['Andamento']['assinatura'] = $this->params['form']['rd2signer_assinatura0'];
            $this->data['Andamento']['usuario_token'] = $this->params['form']['usuario_token'];
        }
        
        $modelAndamento->save($this->data);
    }
    
}
