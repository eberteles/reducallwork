<?php
/**
 * @resource { "name" : "Cadastro de Áreas / Sub Áreas de Fornecedores", "route":"areas", "access": "private", "type": "module" }
 */
class AreasController extends AppController
{

    var $name = 'Areas';

    var $uses = array(
        'Area'
    );

    var $helpers = array(
        'ImprimirArea'
    );
    /**
     * @resource { "name" : "Áreas / Sub Áreas de Fornecedores", "route":"areas\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->gerarFiltro();
        $criteria = null;
        if (! empty($this->data)) {
            $criteria['ds_area like'] = '%' . up($this->data['Areas']['ds_area']) . '%';
        }
        $this->set('areas', $this->Area->find('threaded', array(
            'conditions' => $criteria
        )));
    }
    /**
     * @resource { "name" : "Nova Área", "route":"areas\/add", "access": "private", "type": "insert" }
     */
    function add()
    {
        if (!empty($this->data)) {
            $this->Area->create();
            if ($this->Area->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $this->set('areas', $this->Area->find('threaded'));
    }
    /**
     * @resource { "name" : "Editar Área", "route":"areas\/edit", "access": "private", "type": "update" }
     */

    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Area->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Area->read(null, $id);
        }
        $this->set('id', $id);
        
        $this->set('areas', $this->Area->find('threaded'));
    }
    /**
     * @resource { "name" : "Remover Área", "route":"areas\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $area = $this->Area->find(array('co_area' => $id));
        }
        if ($this->Area->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_tabela'] = 'areas';
            $log['ds_log'] = json_encode($area['Area']);
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    /**
     * @resource { "name" : "Retorno JSON das Áreas", "route":"areas\/listar", "access": "private", "type": "select" }
     */
    function listar($co_area)
    {
        $this->Area->recursive = 0;
        
        echo json_encode($this->Area->find('list', array(
            'conditions' => array(
                'parent_id' => $co_area
            )
        )));
        
        exit();
    }
}
