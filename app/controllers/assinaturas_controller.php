<?php
class AssinaturasController extends AppController
{

    var $name = 'Assinaturas';

    var $helpers = array(
        'Imprimir', 'Util'
    );

    function __construct()
    {
        App::import('Helper', 'Modulo');
        App::import('Helper', 'Util');
        $this->modulo = new ModuloHelper();
        $this->Util = new UtilHelper();

        parent::__construct();
    }

    function assinar()
    {
        $token = $this->data["Assinatura"]["token"];
        $signatureFinisher = new \Lacuna\XmlSignatureFinisher($this->Util->getRestPkiClient());
        $signatureFinisher->setToken($token);
        $signedXml = $signatureFinisher->finish();
        $signerCert = $signatureFinisher->getCertificate();

        $filename = uniqid() . ".xml";
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . "app/signatures/{$filename}", $signedXml);

        $assinatura = array();
        $assinatura['Assinatura']['authentication_id']  = $signerCert->pkiBrazil->cpf;
        $assinatura['Assinatura']['texto']              = $signerCert->subjectName->commonName;
        $assinatura['Assinatura']['assinatura']         = $signerCert->subjectName->commonName;
        $assinatura['Assinatura']['usuario_token']      = $signerCert->subjectName->commonName;

        $this->Assinatura->create();
        if ($this->Assinatura->save($assinatura)) {
            $modal  = $this->data['Assinatura']['vincula_modal'];
            $this->loadModel($modal);
            $this->$modal->id = $this->data['Assinatura']['vincula_id'];
            $this->$modal->saveField("assinatura_id", $this->Assinatura->getInsertID());
        }

        $this->Session->setFlash(__('Documento assinado com sucesso', true));
        $this->redirect(array(
            'controller' => $this->data['Assinatura']['redirect_controller'],
            'action' => $this->data['Assinatura']['redirect_action'], $this->data['Assinatura']['redirect_complemento']
        ));
    }
}
?>