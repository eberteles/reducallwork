<?php

class AssinaturasLoginsController extends AppController
{
    public $name = 'AssinaturasLogins';
    public $uses = array(
        'AssinaturasLogin'
    );
    
    function index($coContrato)
    {
        if(!is_int((int)$coContrato))
            throw new TypeError("Parâmetro referente ao contrato é inválido");
        $this->set('assinatura', $this->AssinaturasLogin->findByCoContrato($coContrato));
    }
    
    function add($modal = false)
    {
        if(!is_bool($modal))
            throw new TypeError("Parâmetro inválido!");
        elseif(is_bool($modal) && $modal == true)
            $this->layout = 'iframe';

        if(!empty($this->data))
        {
            $this->AssinaturasLogin->create();
            if($this->AssinaturasLogin->save($this->data['AssinaturasLogin'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('modal'));
    }
}