<?php

class AssinaturasRequisitantes extends AppController
{
    public $name = 'AssinaturasRequisitantes';
    public $uses = array(
        'AssinaturasRequisitante'
    );
    
    function index($coContrato)
    {
        if(!is_int((int)$coContrato))
            throw new TypeError("Parâmetro referente ao PAM/Contrato é inválido!");

        $this->set('assinatura', $this->AssinaturasRequisitante->findByCoContrato($coContrato));
    }
    
    function add($modal = false)
    {
        if(!is_bool($modal))
            throw new TypeError("Parâmetro inválido!");
        elseif(is_bool($modal) && $modal == true)
            $this->layout = 'iframe';

        if(!empty($this->data))
        {
            $this->AssinaturasRequisitante->create();
            if($this->AssinaturasRequisitante->save($this->data['AssinaturasRequisitante']))
            {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('modal'));
    }
}