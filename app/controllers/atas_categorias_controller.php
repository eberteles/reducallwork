<?php

class AtasCategoriasController extends AppController
{

    var $name = 'AtasCategorias';

    var $uses = array(
        'AtaCategoria'
    );
    
    function index()
    {
        $this->AtaCategoria->recursive = 0;
        $this->set('atascategorias', $this->paginate());
    }
    
    function add()
    {
        if (! empty($this->data)) {
            $this->AtaCategoria->create();
            if ($this->AtaCategoria->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    /**
     * @resource { "name" : "Editar Atas Categorias", "route":"atas_categorias\/edit", "access": "private", "type": "updade" }
     */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->AtaCategoria->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->AtaCategoria->read(null, $id);
        }
        $this->set('id', $id);
    }
    
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->AtaCategoria->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
