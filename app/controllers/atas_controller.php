<?php
/**
* @resource { "name" : "Atas", "route":"atas", "access": "private", "type": "module" }
*/
class AtasController extends AppController
{
    /**
     * @resource { "name" : "Atas", "route":"atas\/index\/**", "access": "private", "type": "select" }
     */
    public function index()
    {
    }

    /**
     * @resource { "name" : "Atas", "route":"atas\/print\/**", "access": "private", "type": "select" }
     */
    public function print_ata()
    {
        $this->render('index');
    }

    /**
     * @resource { "name" : "Atas", "route":"atas\/print_item\/**", "access": "private", "type": "select" }
     */
    public function print_item()
    {
        $this->render('index');
    }
}
