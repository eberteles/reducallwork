<?php

class AtasItensController extends AppController
{

    public $name = 'AtasItens';

    public $layout = 'iframe';
    // var $uses = array('AtasItem');
    
    public function index($coAta)
    {
        // $this->AtasItem->recursive = 0;
        
        // $this->virtualFields['tt_pedido'] = 'sum( Pedido.nu_quantidade ) AS tt_pedido';
        $this->paginate = array(
            'limit' => 10000,
            'conditions' => array(
                'AtasItem.co_ata' => $coAta
            ),
            'order' => array(
                'Lote.nu_lote' => 'ASC',
                'AtasItem.nu_item' => 'ASC'
            )
        );
        
        $this->set('itens', $this->paginate());
        
        $this->set(compact('coAta'));
    }
    
    public function iframe($coAta)
    {
        $this->set(compact('coAta'));
    }

    
    public function add_lote()
    {
        if (! empty($this->data)) {
            App::import('Model', 'AtasLote');
            $loteDb = new AtasLote();
            $loteDb->create();
            
            $lote = $loteDb->find('first', array(
                'conditions' => array(
                    'nu_lote' => $this->data['nu_lote']
                )
            ));
            
            if (! empty($lote)) {
                $loteDb->id = $lote['AtasLote']['co_ata_lote'];
                $loteDb->saveField("ds_objeto", up($this->data["ds_objeto"]));
            } else {
                $loteDb->save(array(
                    'co_ata' => $this->data['co_ata'],
                    'nu_lote' => up($this->data['nu_lote']),
                    'ds_objeto' => up($this->data['ds_objeto'])
                ), false);
            }
            return false;
        }
    }
    
    public function add($coAta)
    {
        if (! empty($this->data)) {
            $this->AtasItem->create();
            if ($this->AtasItem->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coAta
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coAta'));
        $this->set('lotes', $this->AtasItem->Lote->find('list', array(
            'conditions' => array(
                'co_ata' => $coAta
            )
        )));
    }
    
    public function edit($id = null, $coAta)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }
        if (! empty($this->data)) {
            if ($this->AtasItem->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coAta
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->AtasItem->read(null, $id);
        }
        $this->set(compact('coAta'));
        $this->set(compact('id'));
        $this->set('lotes', $this->AtasItem->Lote->find('list', array(
            'conditions' => array(
                'co_ata' => $coAta
            )
        )));
    }
    
    public function delete($id = null, $coAta)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }
        
        App::import('Model', 'PedidosItem');
        $pedido = new PedidosItem();
        
        $nuPedidos = $pedido->find('count', array(
            'conditions' => array(
                'PedidosItem.co_ata_item' => $id
            )
        ));
        
        if ($nuPedidos > 0) {
            $this->Session->setFlash(__('Erro ao excluir registro. Existem Pedidos vinculados a este Item.', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        } else {
            if ($this->AtasItem->delete($id)) {
                $this->Session->setFlash(__('Registro excluído com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coAta
                ));
            }
        }
        
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coAta
        ));
    }
}
?>