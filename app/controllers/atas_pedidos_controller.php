<?php

class AtasPedidosController extends AppController
{

    var $name = 'AtasPedidos';

    var $layout = 'iframe';

    var $uses = array(
        'AtasPedido',
        'AtasItem',
        'PedidosItem'
    );
    
    function index($coAta)
    {
        $this->paginate = array(
            'limit' => 10000,
            'conditions' => array(
                'AtasPedido.co_ata' => $coAta,
                'AtasPedido.tp_pedido' => 'P'
            )
        );
        
        $this->set('pedidos', $this->paginate());
        
        $this->set(compact('coAta'));
        
        $this->set('setores', $this->AtasPedido->Contrato->Setor->find('list'));
    }
    
    function iframe($coAta)
    {
        $this->set(compact('coAta'));
    }
    
    function add($coAta)
    {
        if (! empty($this->data)) {
            
            $this->data['AtasPedido']['vl_pedido'] = 0;
            $this->data['AtasPedido']['dt_pedido'] = date('Y-m-d');
            foreach ($this->data['AtasPedido']['vl_pedir'] as $valor) :
                if ($valor != '') {
                    $valor = ln($valor);
                    $this->data['AtasPedido']['vl_pedido'] += $valor;
                }
            endforeach
            ;
            
            // debug($this->data);die;
            $this->AtasPedido->create();
            if ($this->AtasPedido->save($this->data)) {
                
                foreach ($this->data['AtasPedido']['co_ata_item'] as $co_item) :
                    if ($this->data['AtasPedido']['qt_pedir'][$co_item] != '') {
                        $this->AtasItem->id = $co_item;
                        $this->AtasItem->saveField("qt_utilizado", $this->AtasItem->field('qt_utilizado') + $this->data['AtasPedido']['qt_pedir'][$co_item]);
                        $item_pedido = $this->AtasItem->find('first', array(
                            'conditions' => array(
                                'AtasItem.co_ata_item' => $co_item
                            )
                        ));
                        $this->PedidosItem->create();
                        $this->PedidosItem->save(array(
                            'co_ata_pedido' => $this->AtasPedido->id,
                            'co_ata_item' => $co_item,
                            'co_ata_lote' => $item_pedido['AtasItem']['co_ata_lote'],
                            'nu_item' => $item_pedido['AtasItem']['nu_item'],
                            'ds_marca' => $item_pedido['AtasItem']['ds_marca'],
                            'ds_descricao' => $item_pedido['AtasItem']['ds_descricao'],
                            'ds_unidade' => $item_pedido['AtasItem']['ds_unidade'],
                            'qt_pedido' => $this->data['AtasPedido']['qt_pedir'][$co_item],
                            'vl_unitario' => vlReal($item_pedido['AtasItem']['vl_unitario'])
                        ), false);
                    }
                endforeach
                ;
                
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coAta
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coAta'));
        $this->set('itens', $this->AtasItem->find('all', array(
            'conditions' => array(
                'AtasItem.co_ata' => $coAta,
                'AtasItem.nu_quantidade > AtasItem.qt_utilizado'
            ),
            'order' => array(
                'Lote.nu_lote' => 'ASC',
                'AtasItem.nu_item' => 'ASC'
            )
        )));
    }
    
    function detalha($id = null, $coAta)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }
        $this->set(compact('coAta'));
        $this->set(compact('id'));
        $this->set('pedido', $this->AtasPedido->find('first', array(
            'conditions' => array(
                'co_ata_pedido' => $id
            ),
            'recursive' => 2
        )));
    }
    
    function edit($id = null, $coAta)
    {

        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }

        if (! empty($this->data)) {

            $quantidadeAnterior = $this->data['AtasPedido']['qt_pedido'];
            unset($this->data['AtasPedido']['qt_pedido']);

            $this->data['AtasPedido']['vl_pedido'] = 0;

            $data_brasil = DateTime::createFromFormat('d/m/Y', $this->data['AtasPedido']['dt_pedido']);


            $this->data['AtasPedido']['dt_pedido'] = $data_brasil->format('Y-m-d');
            foreach ($this->data['AtasPedido']['vl_pedir'] as $valor) :
                if ($valor != '') {
                    $valor = ln($valor);
                    $this->data['AtasPedido']['vl_pedido'] += $valor;
                }
            endforeach;

            $this->AtasPedido->create();

            if ($this->AtasPedido->save($this->data)) {

                foreach ($this->data['AtasPedido']['co_ata_item'] as $co_item) :
                    if ($this->data['AtasPedido']['qt_pedir'][$co_item] != '') {




                        if( $quantidadeAnterior > abs( $this->data['AtasPedido']['qt_pedir'][$co_item] ) ){
                            $quantidadeUtilizado = abs( $this->AtasItem->field('qt_utilizado') -  abs( $quantidadeAnterior - $this->data['AtasPedido']['qt_pedir'][$co_item] ) );

                        }else if( $quantidadeAnterior < abs ($this->data['AtasPedido']['qt_pedir'][$co_item] ) ){
                            $quantidadeUtilizado = $this->AtasItem->field('qt_utilizado') +  abs( $quantidadeAnterior - $this->data['AtasPedido']['qt_pedir'][$co_item] );
                        }else{
                            $quantidadeUtilizado = $quantidadeAnterior;
                        }
                        $this->AtasItem->id = $co_item;

                        $this->AtasItem->saveField("qt_utilizado",$quantidadeUtilizado);
                        $item_pedido = $this->AtasItem->find('first', array(
                            'conditions' => array(
                                'AtasItem.co_ata_item' => $co_item
                            )
                        ));
                        $id_item_pedido = $this->PedidosItem->find('all', array(
                        'conditions' => array(
                            'PedidosItem.co_pedido_item' => $id
                        )
                        ));


                          $itemsAtualizados = array( 'PedidosItem' =>array(
                               'co_pedido_item' => $id_item_pedido[0]['PedidosItem']['co_pedido_item'],
                               'co_ata_pedido' => $this->AtasPedido->id,
                               'co_ata_item' => $co_item,
                               'co_ata_lote' => $item_pedido['AtasItem']['co_ata_lote'],
                               'nu_item' => $item_pedido['AtasItem']['nu_item'],
                               'ds_marca' => $item_pedido['AtasItem']['ds_marca'],
                               'ds_descricao' => $item_pedido['AtasItem']['ds_descricao'],
                               'ds_unidade' => $item_pedido['AtasItem']['ds_unidade'],
                               'qt_pedido' => $this->data['AtasPedido']['qt_pedir'][$co_item],
                               'vl_unitario' => vlReal($item_pedido['AtasItem']['vl_unitario'])
                           )
                       );


                        $this->PedidosItem->create();

                       $this->PedidosItem->save($itemsAtualizados, false);

                    }
                endforeach;
                $this->Session->setFlash(__('Registro Atualizado com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coAta
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser atualizadoo. Por favor, tente novamente.', true));
            }
        }

        $this->set(compact('id'));
        $this->set(compact('coAta'));
        $this->set('itens', $this->AtasItem->find('all', array(
            'conditions' => array(
                'AtasItem.co_ata' => $coAta,
                'AtasItem.nu_quantidade > AtasItem.qt_utilizado'
            ),
            'order' => array(
                'Lote.nu_lote' => 'ASC',
                'AtasItem.nu_item' => 'ASC'
            )
        )));

       $this->set('ataPedido', $this->AtasPedido->find('all', array(
            'conditions' => array(
                'co_ata_pedido' => $id
            )
        )));

    }
    
    function delete($id = null, $coAta)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }
        
        App::import('Model', 'PedidosItem');
        App::import('Model', 'AtasItem');
        $pedido = new PedidosItem();
        $ataItem = new AtasItem();
        
        $pedidos = $pedido->find('all', array(
            'conditions' => array(
                'PedidosItem.co_ata_pedido' => $id
            )
        ));
        foreach ($pedidos as $item) :
            $ataItem->id = $item['PedidosItem']['co_ata_item'];
            
            $qtUtilizadoAtual = $ataItem->field('qt_utilizado');
            
            $ataItem->saveField('qt_utilizado', $qtUtilizadoAtual - $item['PedidosItem']['qt_pedido']);
            
            $pedido->delete($item['PedidosItem']['co_pedido_item']);
        endforeach
        ;
        
        if ($this->AtasPedido->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coAta
            ));
        }
        
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coAta
        ));
    }
}
?>