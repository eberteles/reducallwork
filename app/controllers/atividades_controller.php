<?php

/**
 * @resource { "name": "Cadastro de atividades", "route":"atividades", "access": "private", "type": "module" }
 */
class AtividadesController extends AppController
{
    var $name = 'Atividades';

    private $contrato;

    var $atividade;

    /**
     * @resource { "name" : "Atividades", "route":"atividades\/index", "access": "private", "type": "select" }
     */
    function index($coContrato = null, $tpAndamento = 'N')
    {

        if ($this->params['isAjax']) {
            App::import('Helper', 'ModuloHelper');
            App::import('Helper', 'Print');
            $modulo = new ModuloHelper();
            $printHelper = new PrintHelper();

            $this->loadModel('Usuario');
            $this->loadModel('Contrato');

            // atividades
            $atividades = $this->Atividade->all($tpAndamento, $coContrato);
            array_walk($atividades, function (&$data, $index) use ($printHelper, $modulo) {
                if (!$data['Atividade']['dt_fim_execucao']) {
                    $data['Atividade']['dt_fim_execucao'] = '-';
                }

                if (!$data['Atividade']['dt_ini_execucao']) {
                    $data['Atividade']['dt_ini_execucao'] = '-';
                }

                if (!$data['Atividade']['periodicidade']) {
                    $data['Atividade']['periodicidade'] = '-';
                }

                if (!$modulo->getAtividadesAvancado()) {
                    $data['Atividade']['periodicidade'] = '';
                } else {
                    if (!$data['Atividade']['periodicidade']) {
                        $data['Atividade']['periodicidade'] = '-';
                    }
                }

                if (!$modulo->getAtividadesAvancado()) {
                    $data['Atividade']['nome'] = $data['Atividade']['ds_atividade'];
                }

                $data['Atividade']['termino'] = $printHelper->prazoAtividade($data['Atividade']['tp_andamento'], $data['Atividade']['dt_fim_planejado']);

                $data['Atividade']['alerta'] = $printHelper->alertAtividade($data);

                foreach ($data['children'] as $index => &$children) {
                    if (!$children['Atividade']['dt_fim_execucao']) {
                        $children['Atividade']['dt_fim_execucao'] = '-';
                    }

                    if (!$children['Atividade']['dt_ini_execucao']) {
                        $children['Atividade']['dt_ini_execucao'] = '-';
                    }

                    if (!$children['Atividade']['periodicidade']) {
                        $children['Atividade']['periodicidade'] = '-';
                    }

                    if (!$modulo->getAtividadesAvancado()) {
                        $children['Atividade']['periodicidade'] = '';
                    } else {
                        if (!$children['Atividade']['periodicidade']) {
                            $children['Atividade']['periodicidade'] = '-';
                        }
                    }

                    if (!$modulo->getAtividadesAvancado()) {
                        $children['Atividade']['nome'] = $children['Atividade']['ds_atividade'];
                    }

                    $children['Atividade']['termino'] = $printHelper->prazoAtividade($children['Atividade']['tp_andamento'], $children['Atividade']['dt_fim_planejado']);

                    $children['Atividade']['alerta'] = $printHelper->alertAtividade($children);
                }

            });

            echo json_encode(array(
                'data' => $atividades,
                'tpAndamento' => $tpAndamento
            ));
            exit;
        } else {
            $this->redirect(array(
                'action' => 'atividades_gerais'
            ));
        }

    }

    /**
     * @resource { "name" : "Atividades iframe", "route":"atividades\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato, $coAtividade)
    {
        $this->set(compact('coContrato'));
        $this->set(compact('coAtividade'));
    }

    /**
     * @resource { "name" : "Select Mail", "route":"atividades\/selectMail", "access": "private", "type": "select" }
     */
    function selectMail($coAtividade)
    {
        $this->layout = 'iframe';
        $this->loadModel('Usuario');
        $this->set(compact('coAtividade'));

        $this->set('emails', $this->Usuario->find('list', array(
            'fields' => array(
                'Usuario.ds_email', 'Usuario.ds_email'
            )
        )));
    }

    /**
     * @resource { "name" : "View email", "route":"atividades\/viewMail", "access": "private", "type": "select" }
     */
    function viewMail($coAtividade)
    {
        $this->layout = 'iframe';

        $this->Atividade->recursive = 2;
        $atividade = $this->Atividade->findByCoAtividade($coAtividade);

        $this->set('data', $this->data);
        $this->set('atividade', $atividade);
        $this->set(compact('coAtividade'));
    }

    /**
     * @resource { "name" : "Enviar email", "route":"atividades\/sendMail", "access": "private", "type": "select" }
     */
    function sendMail($coAtividade)
    {
        $this->layout = 'iframe';

        $this->Atividade->recursive = 2;
        $atividade = $this->Atividade->findByCoAtividade($coAtividade);

        $mensagem = "<p style='color:#000'>Olá, " . $atividade['Usuario']['ds_nome'] . ".<br><br>Você deve realizar a atividade " . $atividade['Atividade']['nome'] . ", no período de : " . $atividade['Atividade']['dt_ini_planejado'] . " à " . $atividade['Atividade']['dt_fim_planejado'];
        $mensagem .= "<br><br>{$atividade['Atividade']['ds_atividade']}<br><br><p><b>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</b></p>";

        $this->EmailProvider->subject = $atividade['Atividade']['nome'];
        $this->EmailProvider->to = $atividade['Usuario']['ds_nome'] . ' <' . $this->data['Atividade']['destinatario'] . '>';
        $this->EmailProvider->send($mensagem);
    }

    /**
     * @resource { "name" : "Aplicar pendência", "route":"atividades\/aplicarPendencia", "access": "private", "type": "update" }
     */
    function aplicarPendencia($coAtividade, $coContrato)
    {
        if (!$coAtividade) {
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        if ($coAtividade) {

            $this->Atividade->id = $coAtividade;
            $this->Atividade->saveField('ic_pendente', 1);

            $atividade = $this->Atividade->findByCoAtividade($coAtividade);

            $mensagem = "<p style='color:#000'>" . $atividade['Usuario']['ds_nome'] . ",<br><br>Foi adicionada uma pendência na Atividade: " . $atividade['Atividade']['nome'] .
                ". <br> Prazo da Atividade: " . $atividade['Atividade']['dt_ini_planejado'] . " à " . $atividade['Atividade']['dt_fim_planejado'];
            $mensagem .= "<br><br><p><b>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</b></p>";

            $this->EmailProvider->subject = $atividade['Atividade']['nome'];
            $this->EmailProvider->to = $atividade['Usuario']['ds_nome'] . ' <' . $atividade['Usuario']['ds_email'] . '>';
            $this->EmailProvider->send($mensagem);


            if (isset($atividade['Contrato']['co_gestor_atual']) && $atividade['Contrato']['co_gestor_atual'] > 0) {
                $mensagem = "<p style='color:#000'>" . $atividade['Contrato']['GestorAtual']['ds_nome'] . ",<br><br>Foi adicionada uma pendência na Atividade: " . $atividade['Atividade']['nome'] .
                    ". <br> Prazo da Atividade: " . $atividade['Atividade']['dt_ini_planejado'] . " à " . $atividade['Atividade']['dt_fim_planejado'];
                $mensagem .= "<br><br><p><b>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</b></p>";

                $this->EmailProvider->subject = $atividade['Atividade']['nome'];
                $this->EmailProvider->to = $atividade['Contrato']['GestorAtual']['ds_nome'] . ' <' . $atividade['Contrato']['GestorAtual']['ds_email'] . '>';
                $this->EmailProvider->send($mensagem);
            }

            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
    }

    /**
     * @resource { "name" : "Remover pendência", "route":"atividades\/removePendencia", "access": "private", "type": "delete" }
     */
    function removePendencia($coAtividade, $coContrato)
    {
        if (!$coAtividade) {
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        if ($coAtividade) {
            $this->Atividade->id = $coAtividade;
            $msg = null;
            $atividade['Atividade']['ic_pendente'] = 0;

            $this->Atividade->saveField('ic_pendente', 0);
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
    }

    /**
     * @resource { "name" : "Atividades Dashboard", "route":"atividades\/list_atividades", "access": "private", "type": "select" }
     */
    public function list_atividades($filtro = '')
    {
        $usuario = $this->Session->read('usuario');
        $criterio = array();

        switch ($filtro) {
            case 'td':
                $criterio['conditions'] = array(
                    'Atividade.co_responsavel' => $usuario['Usuario']['co_usuario']
                );
                break;
            case 'cc':
                $criterio['conditions'] = array(
                    'Atividade.co_responsavel' => $usuario['Usuario']['co_usuario'],
                    'Atividade.pc_executado' => 100
                );
                break;
            case 'pd':
                $criterio['conditions'] = array(
                    'Atividade.co_responsavel' => $usuario['Usuario']['co_usuario'],
                    'Atividade.pc_executado <' => 100
                );
                break;
            default:
                $criterio['conditions'] = array(
                    'Atividade.co_responsavel' => $usuario['Usuario']['co_usuario']
                );
                break;
        }

        $criterio['limit'] = 20;
        $criterio['conditions']['Atividade.parent_id'] = null;
        $this->set('atividades', $this->Atividade->find('all', $criterio));
    }

    /**
     * @resource { "name" : "Atividades Fiscalização", "route":"atividades\/atividades_fiscalizacao", "access": "private", "type": "select" }
     */
    public function atividades_fiscalizacao()
    {
        // $this->loadModel('AtividadeContrato');
        // $atividadesContratos = $this->Atividade->query('SELECT DISTINCT Atividade.nome, group_concat(Contrato.nu_contrato) as Atividade.contratos, 
        // AtividadeContrato.id_atividade, AtividadeContrato.id_contrato, 
        // AtividadeContrato.id, Atividade.dia_execucao,  Atividade.periodicidade from atividades Atividade 
        // join atividades_contratos AtividadeContrato on (Atividade.co_atividade = AtividadeContrato.id_atividade) 
        // join contratos Contrato on (AtividadeContrato.id_contrato = Contrato.co_contrato) 
        // WHERE Contrato.nu_contrato <> "" or Contrato.nu_contrato <> null 
        // GROUP BY Atividade.nome
        // ORDER BY AtividadeContrato.id');

        $atividadesContratos = $this->AtividadeContrato->find('all', array(
                'order' => 'Atividade.co_atividade',
                'conditions' => 'WHERE Contrato.nu_contrato <> "" or Contrato.nu_contrato <> null',
                'group' => 'Atividade.nome',
                'fields' =>
                    array('Atividade.nome, group_concat(Contrato.nu_contrato SEPARATOR \' ;\') as contratos, AtividadeContrato.id_atividade, AtividadeContrato.id_contrato, AtividadeContrato.id, Atividade.dia_execucao, Atividade.periodicidade, Atividade.todos_contratos')
            )
        );

        $this->set('atividadesContratos', $atividadesContratos);
    }

    /**
     * @resource { "name" : "Deletar atividade_contrato", "route":"atividades\/delete_atividadecontrato", "access": "private", "type": "delete" }
     */
    public function delete_atividadecontrato($id = 0)
    {
        $this->loadModel('Contrato');
        $this->loadModel('AtividadeContrato');

        if ($id != 0) {
            $atividade = $this->Atividade->find(array('co_atividade' => $id));
        }

        if ($this->AtividadeContrato->query('delete from atividades_contratos where id_atividade = ' . $id)) {
            if ($this->Atividade->delete($id)) {

                // gravar log
                $sessao = new SessionComponent();
                $usuario = $sessao->read('usuario');
                App::import('Model', 'Log');
                $logModel = new Log();
                $log = array();
                $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
                $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
                $log['ds_email'] = $usuario['Usuario']['ds_email'];
                $log['nu_ip'] = $usuario['IP'];
                $log['dt_log'] = date("Y-m-d H:i:s");
                $log['ds_tabela'] = 'atividades';
                $log['ds_log'] = json_encode($atividade['Atividade']);
                $logModel->save($log);

                echo json_encode(array(
                    'status' => 200,
                    'msg' => 'O registro foi excluído com sucesso'
                ));
            }
        } else {
            echo json_encode(array(
                'status' => 200,
                'msg' => 'Não foi possivel exluir o registro'
            ));
        }

        exit();
    }

    /**
     * @resource { "name" : "Detalhar Atividade", "route":"atividades\/detalhar_atividade", "access": "private", "type": "select" }
     */
    public function detalhar_atividade($coAtividade, $coContrato)
    {
        $this->loadModel('Usuario');
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        $atividades = $this->Atividade->find('first', array(
            'conditions' => array(
                'Atividade.co_atividade' => $coAtividade,
            ),
            'fields' => array('Atividade.*', 'Usuario.ds_nome')

        ));

        echo json_encode(array(
            $atividades,
        ));
        exit();
    }

    /**
     * @resource { "name" : "Atividades Gerais", "route":"atividades\/atividades_gerais", "access": "private", "type": "select" }
     */
    public function atividades_gerais()
    {
        $atividades = null;

        $periodicidadeArr = array(
            'Diariamente' => 'Diariamente',
            'Semanal' => 'Semanal',
            'Quinzenal' => 'Quinzenal',
            'Mensal' => 'Mensal',
            'Bimestral' => 'Bimestral',
            'Trimestral' => 'Trimestral',
            'Semestral' => 'Semestral',
            'Anual' => 'Anual',
            'Quando houver necessidade' => 'Quando houver necessidade',
            'Na solicitação' => 'Na solicitação'
        );

        $dia_execucaoArr = array(
            'Segunda' => 'Segunda',
            'Terça' => 'Terça',
            'Quarta' => 'Quarta',
            'Quinta' => 'Quinta',
            'Sexta' => 'Sexta',
            'Quando Possível' => 'Quando Possível'
        );

        $condition = array();

        // $condition['group'] = 'Atividade.nome';
        $condition['order'] = 'Atividade.co_atividade';
        $condition['fields'] = array(
            'Atividade.co_atividade', 'Atividade.nome', 'Atividade.periodicidade', 'Atividade.dia_execucao', 'Atividade.pc_executado',
            'Atividade.dt_fim_planejado', 'Atividade.ds_atividade', 'Atividade.pendencia', 'Contrato.co_contrato', 'Contrato.nu_contrato',
            'Usuario.ds_nome', 'Atividade.periodicidade', 'Contrato.dt_ini_vigencia', 'Contrato.dt_fim_vigencia'
        );

        $contagemPendentes = $this->Atividade->find('count', array(
            'fields' => 'Atividade.nome',
            // 'group' => 'Atividade.nome',
            'conditions' => 'Atividade.pc_executado <> 100 and CURDATE() > Atividade.dt_fim_planejado',

        ));

        $contagemAndamentos = $this->Atividade->find('count', array(
            'fields' => 'Atividade.nome',
            // 'group' => 'Atividade.nome',
            'conditions' => 'Atividade.pc_executado <> 100 and CURDATE() < Atividade.dt_fim_planejado and Atividade.tp_andamento = \'E\'',
        ));

        $contagemConcluidos = $this->Atividade->find('count', array(
            'fields' => 'Atividade.nome',
            // 'group' => 'Atividade.nome',
            'conditions' => array(
                'pc_executado' => 100
                // 'Atividade.parent_id >' => 0
            )
        ));

        $this->set('contagemPendentes', $contagemPendentes);
        $this->set('contagemAndamentos', $contagemAndamentos);
        $this->set('contagemConcluidos', $contagemConcluidos);

        // pesquisa dos botões
        if (isset($this->params['url']['filtro'])) {

            $filtro = strtolower($this->params['url']['filtro']);

            switch ($filtro) {
                case 'pendentes':
                    $condition['conditions'] = 'Atividade.pc_executado <> 100 and CURDATE() > Atividade.dt_fim_planejado';
                    $this->set('tipoPesquisa', 'pendentes');
                    break;

                case 'andamentos':
                    $condition['conditions'] = array(
                        'Atividade.pc_executado !=' => 100,
                        'CURDATE() <=' => 'Atividade.dt_fim_planejado',
                        // 'Atividade.parent_id >' => 0,
                        'Atividade.tp_andamento' => 'E'
                        // 'Atividade.co_responsavel' => 'Usuario.co_usuario' 
                    );

                    $this->set('tipoPesquisa', 'andamentos');
                    break;

                case 'concluidos':
                    $condition['conditions'] = 'pc_executado = 100';
                    $this->set('tipoPesquisa', 'concluidos');
                    break;
                default:
                    break;
            }

            $this->set('queryString', '?filtro=' . $this->params['url']['filtro']);
        }

        // pesquisar com os filtros
        if (isset($this->params['url']['filtroPesquisa'])) {
            if (!empty($this->params['url']['filtroPesquisa']['contrato'])) {
                $condition['conditions'] = array(
                    'Atividade.co_contrato' => $this->params['url']['filtroPesquisa']['contrato']
                );
                // array_pop($condition['fields']);
            }

            if (!empty($this->params['url']['filtroPesquisa']['responsavel'])) {
                $condition['conditions']['Atividade.co_responsavel'] = $this->params['url']['filtroPesquisa']['responsavel'];
            }

            if (!empty($this->params['url']['filtroPesquisa']['periodicidade'])) {
                $condition['conditions']['Atividade.periodicidade'] = $this->params['url']['filtroPesquisa']['periodicidade'];
            }

            if (!empty($this->params['url']['filtroPesquisa']['dia_execucao'])) {
                $condition['conditions']['Atividade.dia_execucao'] = $this->params['url']['filtroPesquisa']['dia_execucao'];
            }

            $queryString = '?';
            foreach ($this->params['url']['filtroPesquisa'] as $key => $value) {
                if (!empty($value)) {
                    $queryString .= "filtroPesquisa[$key]={$value}&amp";
                }
            }

            $this->set('queryString', $queryString);
        }

        // pesquisa atividades
        if (isset($this->params['url']['filtroPesquisa']) || isset($this->params['url']['filtro'])) {
            $atividades = $this->Atividade->find('all', $condition);
            $this->loadModel('Contrato');
            $contador = 0;
            foreach ($atividades as $atividade) {
                $this->Contrato->recursive = 0;
                $contrato = $this->Contrato->find('first', array(
                    'conditions' => array(
                        'Contrato.co_contrato' => $atividade['Contrato']['co_contrato'],
                    ),
                    'fields' => array(
                        'GestorAtual.ds_nome', 'Fornecedor.no_razao_social'
                    )
                ));

                $atividades[$contador]['Contrato'] = array_merge($atividade['Contrato'], $contrato);
                $contador++;
            }
        }

        if (isset($this->params['url']['imprimir']) || $this->params['isAjax']) {
            App::import('Vendor', 'pdf');
            App::import('Vendor', 'htmlpdf');
            App::import('Helper', array(
                'Print'
            ));
            $pdf = new PDF();
            $pdf->imprimir($this->tableAtvHtml($atividades));
            exit();
        }

        // contratos que tem atividades
        $contratos = $this->Atividade->find('all', array(
            'conditions' => array(
                'Contrato.ic_ativo' => 1,
            ),
            'fields' => array(
                'DISTINCT Contrato.nu_contrato, Atividade.co_contrato'
            )
        ));

        // responsáveis que tem atividades
        $responsaveis = $this->Atividade->query('SELECT DISTINCT(u.no_usuario), u.ds_nome, a.co_responsavel from usuarios u join atividades a on(a.co_responsavel = u.co_usuario)');

        $this->set('atividades', $atividades);
        $this->set('url', $this->params['url']['url']);
        $this->set('contratos', $contratos);
        $this->set('responsaveis', $responsaveis);
        $this->set('periodicidades', $periodicidadeArr);
        $this->set('dias_execucao', $dia_execucaoArr);
    }

    // procura um valor dentro de um array multidimensional
    private function in_array_multi($elem, $array, $field)
    {

        $top = sizeof($array) - 1;
        $bottom = 0;

        while ($bottom <= $top) {
            if ($array[$bottom]['AtividadeContrato'][$field] == $elem) {
                return true;
            } else {
                if (is_array($array[$bottom]['AtividadeContrato'][$field])) {
                    if (in_array_multi($elem, ($array[$bottom][$field]))) {
                        return true;
                    }
                }
            }
            $bottom++;
        }
        return false;
    }

    private function tableAtvHtml($data)
    {
        $helper = new HtmlHelper();
        $contentHtml = "";

        if (count($data)) {
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;', 'class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(
                array('Contrato', 'Atividade', 'Responsável', 'Periodicidade', 'Conclusão')

            );
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;

            $cells = array();
            foreach ($data as $atividade) {
                // $contratos = (isset($atividade['Atividade']['todos_contratos']) && $atividade['Atividade']['todos_contratos'] == 1) ?
                //     'Todos'
                //     :
                //     $atividade[0]['nu_contratos'];
                $periodicidade = $atividade['Atividade']['periodicidade'] ? $atividade['Atividade']['periodicidade'] : '-';

                $cells[] = array(
                    $atividade['Contrato']['nu_contrato'],
                    $atividade['Atividade']['nome'],
                    $atividade['Usuario']['ds_nome'],
                    $periodicidade,
                    $atividade['Atividade']['pc_executado'] . '%'
                );
            }

            $contentHtml .= $helper->tableCells($cells, array('class' => $class));

            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . count($data) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }

        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }

        return $contentHtml;
    }

    // calcular atividades filhas
    private function calcSubAtividades($atividadePai, $coContrato, $dataFimContrato)
    {
        // qtd de dias da periodicidade
        $periodicidade = 0;

        // dia de execução da atividade
        $diaExecucao = 0;

        // número de subatividades
        $numSubAtividades = 0;

        $semanaArray = array(
            1 => 'Segunda',
            2 => 'Terça',
            3 => 'Quarta',
            4 => 'Quinta',
            5 => 'Sexta',
            // 6 => 'Sabado',
            // 0 => 'Domingo'
        );

        switch (strtolower($atividadePai['Atividade']['periodicidade'])) {
            case 'diariamente':
                $periodicidade = 1;
                break;
            case 'semanal':
                $periodicidade = 7;
                break;
            case 'quinzenal':
                $periodicidade = 15;
                break;
            case 'mensal':
                $periodicidade = 30;
                break;
            case 'bimestral':
                $periodicidade = 60;
                break;
            case 'trimestral':
                $periodicidade = 90;
                break;
            case 'semestral':
                $periodicidade = 180;
                break;
            case 'anual':
                $periodicidade = 365;
                break;
        }

        $dataInicio = new Datetime($this->dateSql($atividadePai['Atividade']['dt_ini_planejado']));
        $dataFim = new Datetime($this->dateSql($atividadePai['Atividade']['dt_fim_planejado']));
        // $iniAtividadeClone = clone($dataInicio);
        $numSubAtividades = round($dataFim->diff($dataInicio)->days / $periodicidade);
        $diaExecucao = array_search($atividadePai['Atividade']['dia_execucao'], $semanaArray);


        // if (date('Y-m-d') >= $iniAtividade->format('Y-m-d')) {
        $hoje = new Datetime(date('Y-m-d'));
        // }
        $novaSubAtividade = array(
            'Atividade' => array(
                'parent_id' => $atividadePai['Atividade']['co_atividade'],
                'co_usuario' => $atividadePai['Atividade']['co_usuario'],
                'co_contrato' => $coContrato,
                'co_responsavel' => $atividadePai['Atividade']['co_responsavel'],
                'funcao_responsavel' => $atividadePai['Atividade']['funcao_responsavel'],
                'nome' => $atividadePai['Atividade']['nome'],
                'dt_fim_planejado' => null,
                'ds_atividade' => $atividadePai['Atividade']['ds_atividade'],
                'periodicidade' => $atividadePai['Atividade']['periodicidade'],
                'dia_execucao' => $atividadePai['Atividade']['dia_execucao'],
                'tp_andamento' => 'N',
                'pendencia' => null,
                'pc_executado' => 0,
                'ic_pendente' => 0,
                'atividade_atestada' => 0
            )
        );
        if (strtolower($atividadePai['Atividade']['periodicidade']) != 'diariamente') {
            $this->cadastrarSubAtividades($hoje, $periodicidade, $diaExecucao, $novaSubAtividade, $coContrato, $dataFimContrato, $numSubAtividades);
        } else {
            $this->cadastrarSubAtividadesDiaria($hoje, $novaSubAtividade, $coContrato, $dataFimContrato, $numSubAtividades);
        }

        // data fim da ultima subatividade 
        $dataFimLast = $this->Atividade->find(
            'first', array(
                'fields' => array('Atividade.dt_fim_planejado'),
                'conditions' => array(
                    'Atividade.parent_id' => $atividadePai['Atividade']['co_atividade']
                ),
                'order' => array('Atividade.co_atividade' => 'DESC')
            )
        );

        // atualizar a data fim da atividade pai
        $this->Atividade->id = $atividadePai['Atividade']['co_atividade'];
        $this->Atividade->saveField('dt_fim_planejado', $this->dateSql($dataFimLast['Atividade']['dt_fim_planejado']));

        $novosValores = $this->Atividade->query("SELECT ROUND( AVG( pc_executado ) ) media_pc, MIN(dt_ini_execucao) ini_atv, MAX(dt_fim_execucao) fim_atv FROM atividades WHERE parent_id = " . $atividadePai['Atividade']['co_atividade']);

        $media_pc = $novosValores[0][0]['media_pc'];
        $ini_atv = $novosValores[0][0]['ini_atv'];
        $fim_atv = $novosValores[0][0]['fim_atv'];

        // atualizar a porcentagem da atividade pai;
        $this->Atividade->saveField("pc_executado", $media_pc);
        if ($media_pc >= 100) {
            $this->Atividade->saveField("dt_fim_execucao", $fim_atv);
            $this->Atividade->saveField("tp_andamento", "F");
        } else {
            $this->Atividade->saveField("dt_fim_execucao", null);
            $this->Atividade->saveField("tp_andamento", "E");
        }
    }

    // cadastrar sub atividades
    private function cadastrarSubAtividades($hoje, $periodicidade, $diaExecucao, $novaSubAtividade, $coContrato, $dataFimContrato, $numSubAtividades)
    {
        $data = $hoje;
        $novaAtividade = $novaSubAtividade;
        $nomeAtividade = $novaSubAtividade['Atividade']['nome'] . ' ';
        $fimVigencia = new Datetime($this->dateSql($dataFimContrato));

        for ($i = 0; $i < $numSubAtividades; $i++) {
            // procurar uma data válida para o inicio da atividade
            for ($j = 0; $j < 7; $j++) {
                if ($this->diaUtil($data) && ($diaExecucao == $data->format('w'))) {
                    $novaAtividade['Atividade']['dt_ini_planejado'] = $data->format('Y-m-d');
                    break 1;
                } else {
                    $data->modify("+1 days");
                }
            }

            $data->modify("+ $periodicidade days");

            // buscar uma data válida para o fim da atividade
            for ($d = 0; $d < 7; $d++) {
                if ($this->diaUtil($data)) {
                    $this->Atividade->create();

                    if ($fimVigencia->diff($data)->days > $periodicidade) {
                        $novaAtividade['Atividade']['dt_fim_planejado'] = $data->format('Y-m-d');
                        $novaAtividade['Atividade']['nome'] = $nomeAtividade . ($i + 1);
                        $this->Atividade->save($novaAtividade);
                    } else {
                        if (!$novaAtividade['Atividade']['dt_fim_planejado']) {
                            $novaAtividade['Atividade']['dt_fim_planejado'] = $data->format('Y-m-d');
                            $this->Atividade->save($novaAtividade);
                        }
                        return;
                    }

                    break 1;
                } else {
                    $data->modify("+ $periodicidade days");
                }
            }
        }
    }

    // inserir sub atividade diariamente
    function cadastrarSubAtividadesDiaria($hoje, $novaSubAtividade, $coContrato, $dataFimContrato, $numSubAtividades)
    {
        $data = $hoje;
        $novaAtividade = $novaSubAtividade;
        $nomeAtividade = $novaSubAtividade['Atividade']['nome'] . ' ';
        $fimVigencia = new Datetime($this->dateSql($dataFimContrato));

        for ($i = 0; $i < $numSubAtividades; $i++) {
            // buscar uma data válida para o inicio da atividade
            for ($c = 0; $c < 7; $c++) {
                if ($this->diaUtil($data)) {
                    $novaAtividade['Atividade']['dt_ini_planejado'] = $data->format('Y-m-d');
                    break 1;
                } else {
                    $data->modify('+1 days');
                }
            }

            $data->modify('+1 days');

            // buscar uma data válida para o fim da atividade
            for ($d = 0; $d < 7; $d++) {
                if ($this->diaUtil($data)) {
                    $this->Atividade->create();
                    if ($fimVigencia->diff($data)->days >= 1) {
                        $novaAtividade['Atividade']['dt_fim_planejado'] = $data->format('Y-m-d');
                        $novaAtividade['Atividade']['nome'] = $nomeAtividade . ($i + 1);
                        $this->Atividade->save($novaAtividade);
                    } else {
                        if (!$novaAtividade['Atividade']['dt_fim_planejado']) {
                            $novaAtividade['Atividade']['dt_fim_planejado'] = $data->format('Y-m-d');
                        }
                        return;
                    }

                    break 1;
                } else {
                    $data->modify('+1 days');
                }
            }
        }
    }

    private function diaUtil($data)
    {
        $feriado = 0;
        $diaUtil = true;
        $diaSemana = $data->format('w');

        if ($diaSemana == 0 || $diaSemana == 6) {
            $diaUtil = false;

        } else {
            for ($i = 0; $i < 12; $i++) {
                if ($data->format('d/m/Y') == $this->Feriados(date("Y"), $i)) {
                    $feriado++;
                }
            }
        }
        return ($feriado == 0) && $diaUtil ? true : false;
    }

    private function dateSql($dateSql)
    {
        $ano = substr($dateSql, 6);
        $mes = substr($dateSql, 3, -5);
        $dia = substr($dateSql, 0, -8);
        return $ano . "-" . $mes . "-" . $dia;
    }

    private function Feriados($ano, $posicao)
    {
        $dia = 86400;
        $datas = array();
        $datas['pascoa'] = easter_date($ano);
        $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
        $datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
        $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
        $feriados = array(
            '01/01',
            // '02/02', // Navegantes,
            date('d/m', $datas['carnaval']),
            date('d/m', $datas['sexta_santa']),
            date('d/m', $datas['pascoa']),
            '21/04',
            '01/05',
            date('d/m', $datas['corpus_cristi']),
            // '20/09', // Revolução Farroupilha \m/ 
            '01/01',
            '12/10',
            '02/11',
            '15/11',
            '25/12',
        );

        return $feriados[$posicao] . "/" . $ano;
    }

    /**
     * @resource { "name" : "Atestar atividade", "route":"atividades\/atestarAtividade", "access": "private", "type": "update" }
     */
    public function atestarAtividade($id)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($id) {
            $this->Atividade->id = $id;
            $msg = null;
            $atividade['Atividade']['atividade_atestada'] = 1;

            if ($this->Atividade->saveField('atividade_atestada', 1)) {
                $msg = "Atividade atestada com sucesso!";
            } else {
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
     * @resource { "name" : "Responsaveis", "route":"atividades\/responsaveis", "access": "private", "type": "select" }
     */
    public function responsaveis($coContrato)
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        if ($modulo->getAtividadesAvancado()) {
            $this->loadModel('ContratoFiscal');
            $fiscais = $this->Atividade->getFiscais($coContrato);
            $gestor = $this->Atividade->getGestor($coContrato);
            if (count($gestor)) {
                $fiscais[$gestor[0]['Usuario']['co_usuario']] = $gestor[0]['Usuario']['ds_nome'];
            }
            $usuarios = $fiscais;
        } else {
            $usuarios = $this->Usuario->find('list');
        }
        echo json_encode($usuarios);
        exit();
    }

    /**
     * @resource { "name" : "Adicionar atividade", "route":"atividades\/add_atividade", "access": "private", "type": "insert" }
     */
    public function add_atividade($coContrato)
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();
        $this->loadModel('Usuario');

        if ($this->RequestHandler->isPost()) {
            $this->Atividade->create();

            // removendo validação para nome caso não atividade avançado
            if (!$modulo->getAtividadesAvancado()) {
                unset($this->Atividade->validate['nome']);
                unset($this->Atividade->validate['funcao_responsavel']);
            }

            if ($this->Atividade->save($this->data)) {

                //atualizar atividade pai
                if (isset($this->data['Atividade']['parent_id']) && $this->data['Atividade']['parent_id'] > 0) {
                    $result = $this->Atividade->query("SELECT MIN(dt_ini_execucao) ini_atv, MAX(dt_fim_planejado) fim_atv FROM atividades WHERE parent_id = " . $this->data['Atividade']['parent_id']);
                    $this->Atividade->id = $this->data['Atividade']['parent_id'];
                    $this->Atividade->saveField('dt_fim_planejado', $result[0][0]['fim_atv']);
                    $dt_fim_planejado = $result[0][0]['fim_atv'];
                }


                $responsavel = $this->Usuario->read('ds_nome', $this->data['Atividade']['co_responsavel']);

                echo json_encode(array(
                    'atividade' => $this->data,
                    'responsavel' => $responsavel['Usuario'],
                    'status' => 200
                ));
            } else {
                echo json_encode(array(
                    'status' => 400
                ));
            }

            exit;

        } else {
            // atividades pais
            if ($modulo->getAtividadesAvancado()) {
                $this->Atividade->displayField = 'nome';
            }
            $atividadesPai = $this->Atividade->find('list', array(
                'conditions' => array(
                    'Atividade.co_contrato' => $coContrato,
                    'tp_andamento !=' => 'F',
                    'parent_id' => null
                )
            ));

            $usuarios = array();

            if ($modulo->getAtividadesAvancado()) {
                $this->loadModel('ContratoFiscal');
                $fiscais = $this->Atividade->getFiscais($coContrato);
                $gestor = $this->Atividade->getGestor($coContrato);

                if (count($gestor)) {
                    $fiscais[$gestor[0]['Usuario']['co_usuario']] = $gestor[0]['Usuario']['ds_nome'];
                }

                $usuarios = $fiscais;

            } else {
                $usuarios = $this->Usuario->find('list', array(
                    'conditions' => array('Usuario.ic_ativo' => 1)
                ));
            }

            echo json_encode(array(
                'usuarios' => $usuarios,
                'atividadesPai' => $atividadesPai
            ));
            exit();
        }

    }


    /**
     * @resource { "name" : "Iniciar Atividade", "route":"atividades\/iniciar_atividade_botao", "access": "private", "type": "update" }
     */
    public function iniciar_atividade_botao()
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        // Iniciar Atividade por botão
        if ($this->RequestHandler->isPost()) {
            $this->Atividade->recursive = -1;
            $atividade = $this->Atividade->read(null, $this->data['coAtividade']);

            // atualizar a atividade
            $this->Atividade->id = $this->data["coAtividade"];
            $this->Atividade->saveField("dt_ini_execucao", date("Y-m-d"));
            $this->Atividade->saveField("tp_andamento", "E");

            if ($modulo->getAtividadesAvancado()) {
                // só cria as subAtividades se tiver a periodicidade necessária e se for uma atividade sem pai
                if (!$atividade['Atividade']['parent_id'] && ($atividade['Atividade']['periodicidade'] != 'Quando houver necessidade'
                        && $atividade['Atividade']['periodicidade'] != 'Na solicitação')
                ) {
                    $this->calcSubAtividades($atividade, $this->data['coContrato'], $this->data['fimVigenciaContrato']);
                }
            }

            echo json_encode(array('status' => 200));
            exit;
        }
    }

    // editar_atividade
    /**
     * @resource { "name" : "Editar atividades", "route":"atividades\/editar_atividade", "access": "private", "type": "update" }
     */
    public function editar_atividade($coAtividade, $coContrato)
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        if ($this->RequestHandler->isPost()) {
            // removendo validação para nome caso não atividade avançado
            if (!$modulo->getAtividadesAvancado()) {
                unset($this->Atividade->validate['nome']);
                unset($this->Atividade->validate['funcao_responsavel']);
            }
            $this->Atividade->save($this->data);

            if (!isset($this->data['Atividade']['parent_id'])) {
                $nomeAtv = $this->data['Atividade']['nome'] . ' ';
                $atvFilhas = $this->Atividade->find('all', array(
                    'fields' => array('Atividade.co_atividade'),
                    'conditions' => array(
                        'Atividade.parent_id' => $this->data['Atividade']['co_atividade'],
                        'Atividade.co_contrato' => $coContrato
                    )
                ));

                foreach ($atvFilhas as $index => $atv) {
                    $this->data['Atividade']['co_atividade'] = $atv['Atividade']['co_atividade'];
                    $this->data['Atividade']['nome'] = $nomeAtv . ($index + 1);
                    $this->Atividade->save($this->data);
                }
            }
        } else {
            $this->loadModel('Usuario');
            if ($modulo->getAtividadesAvancado()) {
                $this->Atividade->displayField = 'nome';
                $this->loadModel('ContratoFiscal');

                $fiscais = $this->Atividade->getFiscais($coContrato);
                $gestor = $this->Atividade->getGestor($coContrato);
                if ($gestor) {
                    $fiscais[$gestor[0]['Usuario']['co_usuario']] = $gestor[0]['Usuario']['ds_nome'];
                }
                $responsaveis = $fiscais;
            } else {
                $responsaveis = $this->Usuario->find('list', array(
                    'conditions' => array('Usuario.ds_nome !=' => '')
                ));
            }

            $atividadesPai = $this->Atividade->find('list', array(
                'conditions' => array(
                    'Atividade.co_contrato' => $coContrato,
                    'Atividade.tp_andamento !=' => 'F',
                    'Atividade.parent_id' => null,
                    'Atividade.co_atividade !=' => $coAtividade
                )
            ));

            $atividade = $this->Atividade->find('first', array(
                'conditions' => array(
                    'Atividade.co_atividade' => $coAtividade,
                ),
                'fields' => array('Atividade.*', 'Usuario.ds_nome')
            ));

            echo json_encode(array('atividade' => $atividade, 'atividadesPai' => $atividadesPai, 'responsaveis' => $responsaveis));
            exit();
        }

        echo json_encode(array(
            'status' => 200,
        ));
        exit;
    }

    /**
     * @resource { "name" : "Encerrar atividade", "route":"atividades\/encerrar_atividade", "access": "private", "type": "update" }
     */
    public function encerrar_atividade($coContrato)
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        // $atividadeAtual = $this->Atividade->read('Atividade.*', $this->data["co_atividade"]);
        $this->Atividade->id = $this->data["coAtividade"];
        $this->Atividade->saveField('ic_encerrada', 1);

        if ($this->data["parentId"] == 0) {
            $atvFilhas = $this->Atividade->find('all', array(
                'fields' => array(
                    'Atividade.co_atividade',
                    'Atividade.dt_fim_execucao'
                ),
                'conditions' => array(
                    'Atividade.parent_id' => $this->data['coAtividade'],
                    'Atividade.co_contrato' => $coContrato
                )
            ));

            foreach ($atvFilhas as $index => $atv) {
                $this->Atividade->query('UPDATE atividades SET ic_encerrada=1 WHERE parent_id=' . $this->data['coAtividade']);
            }
        }

        echo json_encode(array('status' => 200));
        exit();
    }

    /**
     * @resource { "name" : "Editar porcentagem", "route":"atividades\/editar_pct", "access": "private", "type": "update" }
     */
    public function editar_pct($coContrato)
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        $atividadeAtual = $this->Atividade->read('Atividade.*', $this->data["co_atividade"]);
        $this->Atividade->id = $this->data["co_atividade"];

        if (!$modulo->getAtividadesAvancado()) {
            $this->Atividade->saveField("pc_executado", $this->data["pc_executado"]);
        } else {
            $this->Atividade->save(array(
                'Atividade' => array(
                    'co_atividade' => $this->data['co_atividade'],
                    'pc_executado' => $this->data['pc_executado'],
                    'pendencia' => $this->data['pendencia'] ? $this->data['pendencia'] : null
                )
            ));
        }

        // Finaliza a atividade
        if ($this->data["pc_executado"] == 100) {
            $this->data["dt_fim_execucao"] = date("Y-m-d");
            $this->Atividade->saveField("tp_andamento", 'F');
        } else {
            $this->data["dt_fim_execucao"] = null;
        }

        $this->Atividade->saveField("dt_fim_execucao", $this->data["dt_fim_execucao"]);

        // Inicializa tarefa concluida
        if ($atividadeAtual["Atividade"]["pc_executado"] == 100 && $this->data["pc_executado"] < 100) {
            $this->Atividade->saveField("tp_andamento", "E");
            $this->Atividade->saveField("dt_fim_execucao", null);
        }

        // Atualizar Atividade Pai
        if ($atividadeAtual["Atividade"]["parent_id"] > 0) {
            $novosValores = $this->Atividade->query("SELECT ROUND( AVG( pc_executado ) ) media_pc, 
            MIN(dt_ini_execucao) ini_atv, MAX(dt_fim_execucao) fim_atv FROM atividades WHERE parent_id = " . $atividadeAtual["Atividade"]["parent_id"]);
            $media_pc = $novosValores[0][0]['media_pc'];
            $ini_atv = $novosValores[0][0]['ini_atv'];
            $fim_atv = $novosValores[0][0]['fim_atv'];
            $this->Atividade->id = $atividadeAtual["Atividade"]["parent_id"];
            $this->Atividade->saveField("pc_executado", $media_pc);
            $this->Atividade->saveField("dt_ini_execucao", $ini_atv);
            if ($media_pc >= 100) {
                $this->Atividade->saveField("dt_fim_execucao", $fim_atv);
                $this->Atividade->saveField("tp_andamento", "F");
            } else {
                $this->Atividade->saveField("dt_fim_execucao", null);
                $this->Atividade->saveField("tp_andamento", "E");
            }
        }

        echo json_encode(array(
            'status' => 200
        ));
        exit();
    }

    /**
     * @resource { "name" : "Deletar atividade", "route":"atividades\/delete_atividade", "access": "private", "type": "delete" }
     */
    public function delete_atividade()
    {
        App::import('Helper', 'ModuloHelper');
        $modulo = new ModuloHelper();

        if ($this->RequestHandler->isPost()) {
            if ($modulo->getAtividadesAvancado()) {
                $this->Atividade->query('DELETE FROM atividades where parent_id=' . $this->data['coAtividade']);
            }

            $this->Atividade->delete($this->data["coAtividade"]);
        }

        echo json_encode(array(
            'status' => 200
        ));

        exit();
    }

    // calcula o prazo de termino da atividades_fiscalizacao
    private function prazoAtividade($tp_andamento, $dtFim)
    {
        if ($tp_andamento == "F") {
            return "Concluído";
        } else
            if ($tp_andamento == "N" || ($dtFim == null || $dtFim == '')) {
                return "-";
            } else {
                $dias = $this->expira($dtFim);
                if ($dias == 0) {
                    return "Hoje";
                } else
                    if ($dias > 0) {
                        return $dias . " dias";
                    } else {
                        $dias = $dias * -1;
                        if ($dias == 1) {
                            return "1 dia atrasado";
                        } else {
                            return $dias . " dias atrasado";
                        }
                    }
            }
    }

    private function expira($dtFim, $formato = "PT")
    {
        if ($formato == "US") {
            $dtFim = FunctionsComponent::data($dtFim);
        }
        if (!empty($dtFim)) {
            // $hoje = new Datetime(date('Y-m-d'));
            // $arrDataFim = explode('/', $dtFim);
            // $fim = new Datetime("{$arrDataFim[2]}/{$arrDataFim[1]}/{$arrDataFim[0]}");
            // echo $fim->format('d-m-Y') . '<br>';
            $time_inicial = FunctionsComponent::geraTimestamp(date('d/m/Y'));
            $time_final = FunctionsComponent::geraTimestamp($dtFim);

            $diferenca = $time_final - $time_inicial; // segundos

            return (int)floor($diferenca / (60 * 60 * 24));
            // return (int) $fim->diff($hoje)->days;
        } else {
            return -1;
        }
    }

}

?>
