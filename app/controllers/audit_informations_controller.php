<?php

App::import('Model', 'AuditParsed');
App::import('Model', 'VwAuditCategory');

/**
 * @resource { "name": "Log de Auditoria", "route":"audit_informations", "access": "private", "type": "module" }
 */
class AuditInformationsController extends AppController
{
    var $uses = array();

    /**
     * @resource {
     *  "name": "Logs de Auditoria",
     *  "route":"audit_informations\/index",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function index()
    {
    }

    public function detalhar($id)
    {
        $this->layout = 'blank';

        $mdl = new AuditParsed();

        $row = $mdl->find('first', array(
            'conditions' => array(
                'AuditParsed.id_audit_parsed' => $id
            )
        ));

        $this->set(compact('row'));
    }

    /**
     * @resource {
     *  "name": "Pesquisa Log de Auditoria",
     *  "route":"audit_informations\/log_search",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_search()
    {
        $mdlVwAuditCategory = new VwAuditCategory();
        $mdlAuditParsed = new AuditParsed();

        $this->gerarFiltro();
        $this->Log->recursive = 0;
        $this->Log->validate = array();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'AuditParsed.username' => 'desc'
            )
        );

        $criteria = null;

        if (!empty($this->data)) {
            if (isset($this->data['username']) && $this->data['username'] != '') {
                $criteria['AuditParsed.username'] = $this->data['username'];
            }
            if (isset($this->data['operation']) && $this->data['operation'] != '') {
                $criteria['AuditParsed.operation'] = $this->data['operation'];
            }
            if (isset($this->data['object']) && $this->data['object'] != '') {
                $criteria['upper(AuditParsed.object) like'] = '%' . up($this->data['object']) . '%';
            }

            if (($this->data['dt_ini_log'] != '') && ($this->data['dt_fim_log'] != '')) {
                $dt_ini = DateTime::createFromFormat('d/m/Y', $this->data['dt_ini_log']);
                $dt_fim = DateTime::createFromFormat('d/m/Y', $this->data['dt_fim_log']);

                $intervalo = $dt_fim->diff($dt_ini);

                if ($intervalo->invert == 0 && $intervalo->days > 0) {
                    $this->Session->setFlash(__('A data final deve ser maior que a data inicial', true));
                    $criteria = null;
                    $criteria['AuditParsed.operation'] = '0';
                } else {
                    $criteria["DATE(timestamp) BETWEEN '" .
                    dtDb($this->data['dt_ini_log']) . "' AND"] =
                        dtDb($this->data['dt_fim_log']);
                }

            } elseif ($this->data['dt_ini_log'] && !$this->data['dt_fim_log']) {
                $criteria['DATE(timestamp) >='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['dt_ini_log'])
                        ->format('Y-m-d');
            } elseif (!$this->data['dt_ini_log'] && $this->data['dt_fim_log']) {
                $criteria['DATE(timestamp) <='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['dt_fim_log'])
                        ->format('Y-m-d');
            }
        }

        $this->set('categories', $mdlVwAuditCategory->getCombo());
        $this->set('glogs', $this->paginate($mdlAuditParsed, $criteria));
    }

    /**
     * @resource {
     *  "name": "Pesquisa Log de Auditoria",
     *  "route":"audit_informations\/log_user_history",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_history()
    {
        $mdlAuditParsed = new AuditParsed();

        $this->gerarFiltro();

        $limit = (isset($this->data['limit'])) ? $this->data['limit']:10;

        //No gescon recupera apenas as informações do seu proprio banco
        App::Import('ConnectionManager');
        $ds = ConnectionManager::getDataSource('default');

        $mdlAuditParsed->recursive = 0;
        $mdlAuditParsed->validate = array();
        $this->paginate = array(
            'limit' => $limit,
            'order' => array(
                'AuditParsed.`timestamp`' => 'asc',
                'AuditParsed.connectionid' => 'asc',
                'AuditParsed.id_audit_parsed' => 'asc'
            ),
            'fields' => array(
                'id_audit_parsed'
            , '`timestamp`'
            , 'serverhost'
            , 'username'
            , '`host`'
            , 'connectionid'
            , 'queryid'
            , 'operation'
            , '`database`'
            , 'object'
            , "CASE LOWER(SUBSTRING(object, 2, 6))
                  WHEN 'select' THEN 'SELECT'
                        WHEN 'insert' THEN 'INSERT'
                        WHEN 'delete' THEN 'DELETE'
                        WHEN 'update' THEN 'UPDATE'
                        ELSE object
                        END AS oper_type"
            ),
            'conditions' => array(
                'AuditParsed.connectionid in(select distinct(connectionid) from audit_parsed p )',
                'AuditParsed.database' => $ds->config['database']
            )
        );

        $filters = '';
        $criteria = array();
        $aux = array();
        $search = true;

        if (!empty($this->data)) {

            if (isset($this->data['username']) && $this->data['username'] != '') {
                $criteria['AuditParsed.username like '] = "%".$this->data['username']."%";
            }

            if (($this->data['dt_ini_log'] != '') && ($this->data['dt_fim_log'] != '')) {
                $dt_ini = DateTime::createFromFormat('d/m/Y', $this->data['dt_ini_log']);
                $dt_fim = DateTime::createFromFormat('d/m/Y', $this->data['dt_fim_log']);

                $intervalo = $dt_fim->diff($dt_ini);

                if ($intervalo->invert == 0 && $intervalo->days > 0) {
                    $this->Session->setFlash(__('A data final deve ser maior que a data inicial', true));
                    $criteria['AuditParsed.username'] = '000000000000000000';
                    $search = false;
                } else {
                    $criteria["DATE(timestamp) BETWEEN '" . dtDb($this->data['dt_ini_log']) . "' AND"] = dtDb($this->data['dt_fim_log']);
                }

            } elseif ($this->data['dt_ini_log'] && !$this->data['dt_fim_log']) {
                $criteria['DATE(timestamp) >='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['dt_ini_log'])
                        ->format('Y-m-d');
            } elseif (!$this->data['dt_ini_log'] && $this->data['dt_fim_log']) {
                $criteria['DATE(timestamp) <='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['dt_fim_log'])
                        ->format('Y-m-d');
            }
        }

        $result = $this->paginate($mdlAuditParsed, $criteria);

        foreach ($result as $row) {
            $case = $row[0];
            $row = $row['AuditParsed'];
            $row['oper_type'] = $case['oper_type'];

            $dqlType = strtolower(substr($row['object'], 1, 6));

            $entity = null;
            switch ($dqlType) {
                case 'select':

                    $matched = array();
                    preg_match('/from\s*`?(?<entity>[^\s]+)`?\s*/i', $row['object'], $matched);

                    if (isset($matched['entity'])) {
                        $entity = $matched['entity'];
                    }
                    break;
                case 'insert':
                    $matched = array();
                    preg_match('/into\s*`?(?<entity>[^\s]+)[\(\s]`?/i', $row['object'], $matched);

                    if (isset($matched['entity'])) {
                        $entity = $matched['entity'];
                    }
                    break;
                case 'delete':
                    $matched = array();
                    preg_match('/from\s*`?(?<entity>[^\s]+)`?\s*/i', $row['object'], $matched);

                    if (isset($matched['entity'])) {
                        $entity = $matched['entity'];
                    }
                    break;
                case 'update':
                    $matched = array();
                    preg_match('/update\s*`?(?<entity>[^\s]+)`?\s*/i', $row['object'], $matched);

                    if (isset($matched['entity'])) {
                        $entity = $matched['entity'];
                    }
                    break;
                default;
                    $entity = null;
            }

            $row['entity'] = $entity;
            $aux[$row['connectionid']][] = $row;
        }

        $this->set('glogs', $aux);
        $this->set('searched', $search);
    }


    /**
     * @resource {
     *  "name": "Logs de Acesso de Usuário por Data",
     *  "route":"audit_informations\/log_date_user_access",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_date_user_access()
    {
        App::import('Model', 'VwAuditDateAccessUser');
        $mdlVwAuditDateAccessUser = new VwAuditDateAccessUser();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditDateAccessUser.dt_occur' => 'desc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditDateAccessUser));
    }

    /**
     * @resource {
     *  "name": "Logs de operações",
     *  "route":"audit_informations\/log_operations",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_operations()
    {
        App::import('Model', 'VwAuditOperationsOccur');
        $mdlVwAuditOperationsOccur = new VwAuditOperationsOccur();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditOperationsOccur.operation' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditOperationsOccur));
    }

    /**
     * @resource {
     *  "name": "Logs de Usuário por Local de acesso",
     *  "route":"audit_informations\/log_user_access",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_access()
    {
        App::import('Model', 'VwAuditUserAccessOccur');
        $mdlVwAuditUserAccessOccur = new VwAuditUserAccessOccur();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditUserAccessOccur.username' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditUserAccessOccur));
    }

    /**
     * @resource {
     *  "name": "Logs de Exclusões por Usuário",
     *  "route":"audit_informations\/log_user_delete",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_delete()
    {
        App::import('Model', 'VwAuditUserOccurDelete');
        $mdlVwAuditUserOccurDelete = new VwAuditUserOccurDelete();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditUserOccurDelete.username' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditUserOccurDelete));
    }

    /**
     * @resource {
     *  "name": "Logs de Inclusões por Usuário",
     *  "route":"audit_informations\/log_user_insert",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_insert()
    {
        App::import('Model', 'VwAuditUserOccurInsert');
        $mdlVwAuditUserOccurInsert = new VwAuditUserOccurInsert();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditUserOccurInsert.username' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditUserOccurInsert));
    }

    /**
     * @resource {
     *  "name": "Logs de Alterações por Usuário",
     *  "route":"audit_informations\/log_user_update",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_update()
    {
        App::import('Model', 'VwAuditUserOccurUpdate');
        $mdlVwAuditUserOccurUpdate = new VwAuditUserOccurUpdate();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditUserOccurUpdate.username' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditUserOccurUpdate));
    }

    /**
     * @resource {
     *  "name": "Logs de Alterações por Usuário",
     *  "route":"audit_informations\/log_user_update",
     *  "access": "private",
     *  "type": "select"
     * }
     */
    function log_user_operation_database()
    {
        App::import('Model', 'VwAuditUserOperationOnDatabase');
        $mdlVwAuditUserOperationOnDatabase = new VwAuditUserOperationOnDatabase();
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'VwAuditUserOperationOnDatabase.username' => 'asc'
            )
        );

        $this->set('glogs', $this->paginate($mdlVwAuditUserOperationOnDatabase));
    }

}
