<?php
/**
* @resource { "name": "Cadastro de Cargos", "route":"cargos", "access": "private", "type": "module" }
*/
class CargosController extends AppController
{

    var $name = 'Cargos';
        
    /**
    * @resource { "name": "iframe", "route":"cargos\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_cargo )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_cargo' ) );
    }

    /**
    * @resource { "name": "Listar", "route":"cargos\/listar", "access": "private", "type": "select" }
    */
    function listar() {

        echo json_encode ( $this->Cargo->find( 'list' , array('order' => 'ds_cargo ASC')) ) ;

        exit ();
    }
    /**
     * @resource { "name" : "adicionar Cargos", "route":"cargos\/add", "access": "private", "type": "insert" }
     */
    function add($modal = false) 
    {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty($this->data)) {
            $this->Cargo->create();
            if ($this->Cargo->save($this->data)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Cargo->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array(
                            'action' => 'index'
                        ));
                    }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }
}
?>
