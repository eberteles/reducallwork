<?php
/**
* @resource { "name": "Cadastro de categorias de contratos", "route":"categorias", "access": "private", "type": "module" }
*/
class CategoriasController extends AppController
{

    var $name = 'Categorias';
    /**
     * @resource { "name" : "Categorias de Contratos", "route":"categorias\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                    'Categoria.ic_ativo' => 2
                )
        );

        $this->set('categorias', $this->paginate());        
    }

    /**
    * @resource { "name": "iframe", "route":"categorias\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name": "Close iframe", "route":"categorias\/close", "access": "private", "type": "select" }
     */
    function close( $co_categoria )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_categoria' ) );
    }

    /**
     * @resource { "name": "Listar categorias", "route":"categorias\/listar", "access": "private", "type": "select" }
     */
    function listar() {

        echo json_encode ( $this->Categoria->find ( 'list' ) );

        exit ();
    }
    /**
     * @resource { "name" : "Nova Categoria", "route":"categorias\/add", "access": "private", "type": "insert" }
     */
    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        
        if (!empty($this->data)) {
            $categoria = $this->Categoria->find('first', array(
                'conditions' => array(
                    'Categoria.ic_ativo' => 1,
                    'UPPER(Categoria.no_categoria)' => strtoupper($this->data['Categoria']['no_categoria'])
                )
            ));

            if (!empty($categoria)) {
                $this->Categoria->id = $categoria['Categoria']['co_categoria']; 
                $this->Categoria->query(
                    "UPDATE subcategorias SET ic_ativo=2 WHERE co_categoria = {$categoria['Categoria']['co_categoria']}"
                );
                if ($this->Categoria->saveField('ic_ativo', 2)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));   
                }

            } else {
                $this->Categoria->create();
                if ($this->Categoria->save($this->data)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Categoria->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }
        $this->set ( compact ( 'modal' ) );
    }
    /**
     * @resource { "name" : "Editar Categoria", "route":"categorias\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $categoria = $this->Categoria->find('first', array(
                'conditions' => array(
                    'Categoria.ic_ativo' => 1,
                    'UPPER(Categoria.no_categoria)' => strtoupper($this->data['Categoria']['no_categoria'])
                )
            ));

            if (!empty($categoria)) {
                $this->Categoria->id = $categoria['Categoria']['co_categoria']; 
                if ($this->Categoria->saveField('ic_ativo', 2)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));   
                }
            } else {
                if ($this->Categoria->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }

        if (empty($this->data)) {
            $this->data = $this->Categoria->read(null, $id);
        }

        $this->set('id',$id);
    }

    /**
     * @resource { "name" : "Remover Categoria", "route":"categorias\/logicDelete", "access": "private", "type": "delete" }
     */
    function logicDelete($id = null){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Categoria->read(null, $id)) {
            $categoria = $this->Categoria->read(null, $id);
            $msg = null;

            $this->Categoria->id = $categoria['Categoria']['co_categoria'];
            $this->Categoria->query(
                "UPDATE subcategorias SET ic_ativo=1 WHERE co_categoria = {$id}"
            );

            if($this->Categoria->saveField('ic_ativo', 1)) {                
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Não foi possível deletar o registro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

    }

    /**
     * @resource { "name": "Checar Nome", "route":"categorias\/checar_nome", "access": "private", "type": "select" }
     */
    function checar_nome($nome=null) {
        $count = $this->Categoria->findByName($nome, 1);

        echo json_encode(array(
            'status' => $count
        ));
        exit;
    }
    /**
     * @resource { "name" : "Remover Categoria", "route":"categorias\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action'=>'index'));
        } else {
            $categoria = $this->Categoria->find(array('co_categoria' => $id));
        }

        if ($this->Categoria->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array('action' => 'index'));
    }
}
