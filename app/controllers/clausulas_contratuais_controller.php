<?php
/**
* @resource { "name": "Cadastro de cláusulas contratuais", "route":"clausulascontratuais", "access": "private", "type": "module" }
*/
class ClausulasContratuaisController extends AppController
{

    var $name = 'ClausulasContratuais';
    
    var $uses = array('ClausulaContratual');

    var $layout = 'iframe';
    /**
     * @resource { "name" : "Cláusulas Contratuais", "route":"clausulascontratuais\/index", "access": "private", "type": "select" }
     */
    function index($coContrato, $modulo = "contrato")
    {
        $this->ClausulaContratual->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'ClausulaContratual.nu_clausula' => 'desc'
            )
        );
        $conditions = null;
        if ($modulo == "ata") {
            $conditions['ClausulaContratual.co_ata'] = $coContrato;
        }
        elseif ($modulo == "evento") {
            $conditions['ClausulaContratual.co_evento'] = $coContrato;
        } else {
            $conditions['ClausulaContratual.co_contrato'] = $coContrato;
        }
        
        $this->set('clausulas', $this->paginate($conditions));
        
        $this->set(compact('coContrato'));
        
        $this->set(compact('modulo'));
    }

    /**
    * @resource { "name": "iFrame", "route":"clausulascontratuais\/iframe", "access": "private", "type": "select" }
    */
    function iframe($coContrato, $modulo = "contrato")
    {
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
    }
    /**
     * @resource { "name" : "adcionar Clausulas contratuais", "route":"clausulascontratuais\/add", "access": "private", "type": "insert" }
     */
    function add($coContrato, $modulo = "contrato")
    {
        if (! empty($this->data)) {
            $usuario = $this->Session->read('usuario');

            $this->ClausulaContratual->create();
            $this->data['ClausulaContratual']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            if ($this->ClausulaContratual->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));

                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
    }
    /**
     * @resource { "name" : "editar Clausulas contratuais", "route":"clausulascontratuais\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $coContrato, $modulo = "contrato")
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo
            ));
        }
        if (! empty($this->data)) {
            $usuario = $this->Session->read('usuario');

            $this->data['ClausulaContratual']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            if ($this->ClausulaContratual->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->ClausulaContratual->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set(compact('modulo'));
    }
    /**
     * @resource { "name" : "remover Clausulas contratuais", "route":"clausulascontratuais\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato, $modulo = "contrato")
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->ClausulaContratual->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato,
            $modulo
        ));
    }
}
?>