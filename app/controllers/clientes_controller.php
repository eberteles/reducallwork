<?php

class ClientesController extends AppController
{
    var $name = 'Clientes';

    //var $uses = null;

    var $components = array ('Downloader');

    var $helpers = array(
        'ImprimirArea'
    );

    private $indValor = array(
        'PEQUENO' => 'PEQUENO',
        'MÉDIO'   => 'MÉDIO',
        'ALTO'    => 'ALTO'
    );

    private $indImportancia = array(
        'COMUM'          => 'COMUM',
        'NECESSÁRIO'     => 'NECESSÁRIO',
        'IMPRESCINDÍVEL' => 'IMPRESCINDÍVEL'
    );

    private $comboOcsPsa = array(
        'NÃO' => 'NÃO',
        'OCS' => 'OCS',
        'PSA' => 'PSA'
    );

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['cliente']['nu_cnpj']);
        $this->Functions->limparMascara($this->data['cliente']['nu_telefone']);
        $this->Functions->limparMascara($this->data['cliente']['nu_fax']);
        $this->Functions->limparMascara($this->data['cliente']['nu_cpf_responsavel']);
        $this->Functions->limparMascara($this->data['cliente']['nu_rg_responsavel']);
        $this->Functions->limparMascara($this->data['cliente']['nu_cep']);
    }

    /**
        *@resource { "name" : "Clientes", "route":"clientes\/index", "access": "private", "type": "select" }
    */
    function index()
    {
        //$this->gerarFiltro();
        $this->Cliente->recursive = 0;
        $this->Cliente->validate = array();
        $this->paginate = array(
            'limit' => 10
        );
        $criteria = null;

        if (! empty($this->data) && $this->RequestHandler->isPost() || $this->params['named']['page'] != null) {
            // echo '<pre>';print_r($this->data); echo '</pre>';
            $this->prepararCampos();
            if ($this->data['Cliente']['tp_cliente']) {
                $criteria['tp_cliente'] = $this->data['Cliente']['tp_cliente'];
            }

            if ($this->data['Cliente']['no_razao_social']) {
                $criteria['no_razao_social like'] = '%' . up($this->data['Cliente']['no_razao_social']) . '%';
            }

            if ($this->data['Cliente']['nu_cnpj']) {
                $this->Functions->limparMascara($this->data['Cliente']['nu_cnpj']);
                $criteria['nu_cnpj like'] = '%' . up($this->Functions->limparMascara($this->data['Cliente']['nu_cnpj'])) . '%';
            }

            if (!empty($this->data['Cliente']['co_area'])) {


                if (empty($co_area['Area']['parent_id'])) :
                    $criteria['Cliente.co_area'] = $co_area['Area']['co_area'];
                 else :
                    $criteria['Cliente.co_area'] = array(
                        $co_area['Area']['co_area']
                        //$co_area['Area']['parent_id']
                    );
                endif;
            }
        }else{
            $this->data['Cliente'] = '';
        }

        $this->set('indValores', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('areas', $this->Cliente->Area->find('threaded'));

        $this->set('clientes', $this->paginate($criteria));
    }

    /**
        *@resource { "name" : "Detalhar Clientes", "route":"clientes\/mostrar", "access": "private", "type": "select" }
    */
    function mostrar( $id = null )
    {
        $cliente = $this->Cliente->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                'Cliente.co_cliente' => $id
            )
        ));
        $this->set('cliente', $cliente);
    }

    /**
        *@resource { "name" : "Clientes iframe", "route":"clientes\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     *@resource { "name" : "Fechar iframe", "route":"clientes\/close", "access": "private", "type": "select" }
     */
    function close( $co_cliente )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_cliente' ) );
    }

    /**
     *@resource { "name" : "Listar clientes json", "route":"clientes\/listar", "access": "private", "type": "select" }
     */
    function listar() {

        echo json_encode ( $this->Cliente->find ( 'list', array(
            'order' => 'Cliente.no_razao_social ASC',
            'conditions' => array(
                'Cliente.ic_ativo' => 1
            )
        ) ) );

        exit ();
    }

    /**
     * @resource { "name" : "Novo Cliente", "route":"clientes\/add", "access": "private", "type": "insert" }
     */
    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty ( $this->data )) {
            $cliente = $this->Cliente->findByNuCnpj($this->data['Cliente']['nu_cnpj']);
            if( !empty($cliente) )
                $this->activateCliente($cliente['Cliente']['co_cliente']);
            else{
                $this->Cliente->create();
                $this->prepararCampos ();
              //  var_dump($this->data);die;
                if ($this->Cliente->save ( $this->data )) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Cliente->id ) );
                    } else {
                        $this->Session->setFlash ( __ ( 'Registro salvo com sucesso', true ) );
                        $this->redirect ( array ('action' => 'index' ) );
                    }
                } else {
                    $this->Session->setFlash ( __ ( 'O registro não pode ser salvo. Por favor, tente novamente.', true ) );
                }
            }
        }

        $estados = $this->Cliente->Municipio->Uf->find ( 'list' );
        $this->set ( compact ( 'estados' ) );

        $this->set('indValor', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('combo', $this->comboOcsPsa);
        $this->set ( 'bancos',  $this->Cliente->Banco->find ( 'list' ) );
        $this->set ( 'areas',  $this->Cliente->Area->find ( 'threaded' ) );
        $this->set ( compact ( 'modal' ) );
    }

    /**
        *@resource { "name" : "Editar Cliente", "route":"clientes\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null) {
        if (! $id && empty ( $this->data )) {
                $this->Session->setFlash ( __ ( 'Invalid cliente', true ) );
                $this->redirect ( array ('action' => 'index' ) );
        }
        if (! empty ( $this->data )) {

                $this->prepararCampos ();

                if ($this->Cliente->save ( $this->data )) {
                        $this->Session->setFlash ( __ ( 'Registro salvo com sucesso', true ) );
                        $this->redirect ( array ('action' => 'index' ) );
                } else {
                        $this->Session->setFlash ( __ ( 'O registro não pode ser salvo. Por favor, tente novamente.', true ) );
                }
        }
        if (empty ( $this->data )) {
      $this->data = $this->Cliente->read ( null, $id );
        }
        $municipios     = array();
        if ($this->data ['Cliente'] ['sg_uf'] != "" ) {
            $municipios = $this->Cliente->Municipio->find ( 'list', array ('conditions' => array ('sg_uf' => $this->data ['Cliente'] ['sg_uf'] ) ) );
        }

        $this->set('indValor', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('combo', $this->comboOcsPsa);
        $this->set ( compact ( 'municipios' ) );
        $this->set ('id',$id);
        $estados = $this->Cliente->Municipio->Uf->find ( 'list' );
        $this->set ( compact ( 'estados' ) );

        $this->set('bancos', $this->Cliente->Banco->find('list'));
        $this->set('areas', $this->Cliente->Area->find('threaded'));

    }

    /**
   * @resource { "name" : "Remover Cliente", "route":"clientes\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $cliente = $this->Cliente->find(array('co_cliente' => $id));
        }

        if ($this->Cliente->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($cliente['Cliente']);
            $log['ds_tabela'] = 'clientes';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
        *@resource { "name" : "find cliente", "route":"clientes\/find", "access": "private", "type": "select" }
    */
    function find($nu_cnpj) {
        $nu_cnpj    = $this->Functions->limparMascara( $nu_cnpj );
        echo json_encode ( $this->Cliente->find( 'first', array(
            'fields' => array(
                'co_cliente', 'no_razao_social'
            ),
            'conditions' => array(
                ' Cliente.nu_cnpj = ' . $nu_cnpj,
                ' Cliente.ic_ativo' => 1,
            )
        ) ) );
        exit();
    }
    
    /**
        *@resource { "name" : "verificar cpf cliente", "route":"clientes\/verifyCPF", "access": "private", "type": "select" }
    */
    public function verifyCPF($nuCpf){
        $this->Cliente->recursive = 0;

        if( $nuCpf <= 0 ) {
          echo json_encode(array(
              'color' => 'red',
              'bold' => 'bold',
              'message' => 'CPF inválido!.'
          ));
          exit;
        }

        $existsCPF = $this->Cliente->find('count', array(
            'conditions' => array(
                'Cliente.nu_cnpj' => $nuCpf,
                'Cliente.ic_ativo' => 1
            )
        ));
        if( $existsCPF > 0 ){
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'CNPJ/CPF já cadastrado na base de dados.'
            ));
        }else{
            if($nuCpf == '') {
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'CNPJ/CPF em branco. Campo obrigatório.'
                ));
            } else if(!FunctionsComponent::validaCPF($nuCpf)){
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CNPJ/CPF digitado é inválido! Por favor insira outro.'
                ));
                //echo json_encode("O CPF digitado é inválido! Por favor insira outro.");
            }else{
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CPF válido!'
                ));
                //echo json_encode("CPF válido!");
            }
        }

        exit();
    }

    /**
        *@resource { "name" : "verificar cnpj cliente", "route":"clientes\/verifyCNPJ", "access": "private", "type": "select" }
    */
    public function verifyCNPJ($nuCnpj){
        $this->Cliente->recursive = 0;

        if( $nuCnpj <= 0 ) {
          echo json_encode(array(
              'color' => 'red',
              'bold' => 'bold',
              'message' => 'CNPJ inválido!.'
          ));
          exit;
        }
        $existsCNPJ = $this->Cliente->find('count', array(
            'conditions' => array(
                'Cliente.nu_cnpj' => $nuCnpj,
                'Cliente.ic_ativo' => 1
            )
        ));
        if( $existsCNPJ > 0 ){
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'CNPJ/CPF já cadastrado na base de dados.'
            ));
        }else{
            if($nuCnpj == '') {
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'CNPJ/CPF em branco. Campo obrigatório.'
                ));
            } else if(!FunctionsComponent::validaCNPJ($nuCnpj)){
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CNPJ/CPF digitado é inválido! Por favor insira outro.'
                ));
            }else{
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CNPJ válido!'
                ));
            }
        }

        exit();
    }

    /**
     *@resource { "name" : "Remover Cliente", "route":"clientes\/logicDelete", "access": "private", "type": "update" }
    */
    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Cliente->recursive = -1;
        if ($this->Cliente->read(null, $id)) {
            $cliente = $this->Cliente->read(null, $id);
            $msg = null;
            $cliente['Cliente']['ic_ativo'] = 0;

            unset($this->Cliente->validate);

            if($this->Cliente->save($cliente)){
                $msg = "Registro excluído com sucesso!";
            } else {
                $msg = "Houve um erro na exclusão";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
     *@resource { "name" : "Clientes", "route":"clientes\/activateCliente", "access": "private", "type": "update" }
     */
    public function activateCliente( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Cliente->read(null, $id)) {
            $cliente = $this->Cliente->read(null, $id);
            $msg = null;
            $cliente['Cliente']['ic_ativo'] = 1;

            if($this->Cliente->save($cliente)){
                $msg = "Registro salvo com sucesso!";
            }else{
                $msg = "Houve um erro no registro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
        *@resource { "name" : "Detalhar Cliente", "route":"clientes\/detalha", "access": "private", "type": "select" }
    */
    public function detalha($id)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $this->set(compact('id'));

        $cliente = $this->Cliente->read(null, $id);

        $this->set('cliente', $cliente);
    }

    private function getClienteFromApi($id)
    {
        $url = Configure::read('App.config.api.clientes.url');
        $token = Configure::read('App.config.api.clientes.token');
        $contentUrl = $url . "/api/v1/cliente/" . $id . "?token=" . $token;
        $contents = DownloaderComponent::fetch($contentUrl);

        $jsonApiObject = json_decode($contents, true);

        $nomeCliente = $jsonApiObject['data']['attributes']['nome'];
        $tipoCliente = (strlen($nomeCliente) < 14) ? 'F' : 'J';

        return array(
            "no_razao_social" => $nomeCliente,
            "nu_cnpj"   => $jsonApiObject['data']['id'],
            "tp_cliente" => $tipoCliente
        );
    }

    /**
        *@resource { "name" : "Proxy Cliente", "route":"clientes\/proxy", "access": "private", "type": "select" }
    */
    public function proxy($id)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $result = $this->Cliente->listarClientePorCnpj($id);

        $codigo_cliente = $id;

        if (empty($result)) {
            $cliente = $this->getClienteFromApi($id);

            if ($this->Cliente->save($cliente)) {
                $codigo_cliente = $this->Cliente->getInsertID();
            }

        } else {
            $codigo_cliente = $result[0]['clientes']['co_cliente'];
        }

        $this->redirect(array('action' => 'mostrar', $codigo_cliente));
    }

    function d($cliente = '')
    {
        $this->Session->destroy();
        $this->Session->write('cliente', $cliente);
        $this->redirect(array(
            'controller' => 'pages',
            'action' => 'index'
        ));
    }
}
?>
