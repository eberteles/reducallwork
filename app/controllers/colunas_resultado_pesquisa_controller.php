<?php

class ColunasResultadoPesquisaController extends AppController
{

    var $name = 'ColunasResultadoPesquisa';

    var $uses = array('ColunaResultadoPesquisa');
    
    function listar($modulo, $tipo = 1) {
        
        $not    = false;
        $tpPesquisa = 'list';
        
        if($tipo == 1) {
            $not    = true;
        } else {
            $tpPesquisa = 'matriz';
        }
        
        $resultado  = $this->ColunaResultadoPesquisa->findUsuario ( $tpPesquisa, $modulo, $not );
                
        echo json_encode ( $resultado );

        exit ();
    }
    
    function add_campo($modulo, $coCampo) {
        
        App::import('Helper', 'Modulo');
        $moduloHlp  = new ModuloHelper();
        $camposUsu  = $moduloHlp->getCamposPesquisa($modulo);
        
        $sessao     = new SessionComponent();
        $usuario    = $sessao->read('usuario');

        $ic_campos  = explode(",", $camposUsu);

        $ic_campos[] = $coCampo;
        
        $ic_campos_pesq = '';
        foreach ($ic_campos as $co_campo):
            if($ic_campos_pesq != '') {
                $ic_campos_pesq .= ',';
            }
            $ic_campos_pesq .= $co_campo;
        endforeach;
        
        $this->atzCamposPesquisaUsuario($usuario, $modulo, $ic_campos_pesq);
        
        exit ();
    }

    function del_campo($modulo, $coCampo) {
        
        App::import('Helper', 'Modulo');
        $moduloHlp  = new ModuloHelper();
        $camposUsu  = $moduloHlp->getCamposPesquisa($modulo);
        
        $sessao     = new SessionComponent();
        $usuario    = $sessao->read('usuario');

        $ic_campos  = explode(",", $camposUsu);
        
        $ic_campos_pesq = '';
        foreach ($ic_campos as $co_campo):
            if( $coCampo != $co_campo) {
                if($ic_campos_pesq != '') {
                    $ic_campos_pesq .= ',';
                }
                $ic_campos_pesq .= $co_campo;
            }
        endforeach;
        
        $this->atzCamposPesquisaUsuario($usuario, $modulo, $ic_campos_pesq);
        
        exit ();
    }
    
    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
        }
        if (!empty($this->data)) {
            
            $this->ColunaResultadoPesquisa->id = $this->data['ColunasResultadoPesquisa']['co_coluna_resultado'];
            if ($this->ColunaResultadoPesquisa->saveField('ds_nome', $this->data['ColunasResultadoPesquisa']['ds_nome']) ) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('id'));
    }
    
    private function atzCamposPesquisaUsuario($usuario, $modulo, $campos) {
        App::import('Model', 'Usuario');
        $dbUsuario  = new Usuario();
        $dbUsuario->id = $usuario['Usuario']['co_usuario'];
        $dbUsuario->saveField('ic_campos_pesq_' . $modulo, $campos);
        
        $usuario['Usuario']['ic_campos_pesq_' . $modulo]    = $campos;
        $this->Session->write('usuario', $usuario);
    }

}
?>