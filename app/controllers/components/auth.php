<?php

/**
 * Componente de autenticação
 *
 * @package app.controllers
 * @subpackage components
 * @name AuthComponent
 * @author Juliano Buzanello <jbcisne@gmail.com>
 */
class AuthComponent extends Object
{
    private $dataPost;

    private $defaultErrorMessage = 'Erro ao efetuar a autenticação.';

    private $messages = array(
        //LDAP
        'Invalid credentials' => 'Usuário ou senha inválida.',
        'A username is required' => 'Campo Usuário é de preencimento obrigatório',
        //DB
        'Supplied credential is invalid.' => 'Senha inválida.',
        'A record with the supplied identity could not be found.' => 'Usuário não encontrado.',
        'A value for the identity was not provided prior to authentication with Zend_Auth_Adapter_DbTable.' =>
            'Campo Usuário é de preencimento obrigatório'
    );

    public function __construct()
    {}

    /**
     *
     * @access public
     * @param array $userPostData
     * @return array
     */
    public function autenticate($userPostData)
    {
        $this->dataPost = $userPostData;
        if (Configure::read('App.config.component.ldap.enabled')) {
            return $this->autenticateLdap();
        } else {
            return $this->autenticateDb();
        }
    }

    private function autenticateLdap()
    {
        try {
            $adapterLdap = new Zend_Auth_Adapter_Ldap(
                array(Configure::read('App.config.component.ldap.params.connection'))
            );
            $adapterLdap
                ->setUsername($this->dataPost['Usuario']['no_usuario'])
                ->setCredential($this->dataPost['Usuario']['ds_senha']);

            $result = $adapterLdap->authenticate();

            if ($result->isValid()) {
                $retorno = $this->getDataUser(array(
                        'Usuario.no_usuario' => $this->dataPost['Usuario']['no_usuario'],
                        'Usuario.ic_ativo' => '1'
                    )
                );
            } else {
                $retorno = array(
                    'error' => true,
                    'msg' => $this->getErrorMessage(current($result->getMessages()))
                );
            }
        } catch (\Zend_Exception $e) {
            $retorno = array(
                'error' => true,
                'msg' => $this->getErrorMessage($e->getMessage(), true)
            );
        } catch (\Exception $e) {
            $retorno = array(
                'error' => true,
                'msg' => $this->getErrorMessage($e->getMessage(), false)
            );
        }

        return $retorno;
    }

    private function autenticateDb()
    {
        try {
            App::import('Model', 'ConnectionManager', false);
            $cm = new ConnectionManager();

            $arrConfig = array(
                'dbname' => $cm->config->default['database'],
                'host' => $cm->config->default['host'],
                'password' => $cm->config->default['password'],
                'username' => $cm->config->default['login'],
                'charset' => $cm->config->default['encoding'],
                'persistent' => $cm->config->default['persistent'],
            );

            $authAdapter = new Zend_Auth_Adapter_DbTable(
                new Zend_Db_Adapter_Pdo_Mysql($arrConfig),
                'usuarios',
                'no_usuario',
                'ds_senha',
                'MD5(?) AND ic_ativo = 1'
            );

            $authAdapter
                ->setIdentity($this->dataPost['Usuario']['no_usuario'])
                ->setCredential($this->dataPost['Usuario']['ds_senha']);

            $result = $authAdapter->authenticate();

            if ($result->isValid()) {
                $arrUsuarioData = (array)$authAdapter->getResultRowObject();
                $retorno = $this->getDataUser(array('Usuario.co_usuario' => $arrUsuarioData['co_usuario']));
            } else {
                $retorno = array(
                    'error' => true,
                    'msg' => $this->getErrorMessage(current($result->getMessages()))
                );
            }
        } catch (\Zend_Exception $e) {
            $retorno = array(
                'error' => true,
                'msg' => $this->getErrorMessage($e->getMessage(), true)
            );
        } catch (\Exception $e) {
            $retorno = array(
                'error' => true,
                'msg' => $this->getErrorMessage($e->getMessage(), false)
            );
        }

        return $retorno;
    }

    private function getDataUser($conditions)
    {
        App::import('Model', 'Usuario', false);
        $mdlUsuario = ClassRegistry::init('Usuario');
        $user = $mdlUsuario->find($conditions);
        if (!$user) {
            throw new \Exception('Usuário não cadastrado no Gescon');
        }
        return $user;
    }

    /**
     * Retorna mensagem conhecida em inglês traduzida para o português
     *
     * @param $msgKey mensagem em inglês
     * @param bool $mantemMsgOriginal
     * @return string
     */
    private function getErrorMessage($msgKey, $mantemMsgOriginal = false)
    {
        if (!isset($this->messages[$msgKey])) {
            $msg = $this->defaultErrorMessage;
            if ($mantemMsgOriginal) {
                $msg = $mantemMsgOriginal . " [{$msgKey}]";
            }
            return $msg;
        }
        return $this->messages[$msgKey];
    }

}
