<?php

/**
 * Classe ACL
 *
 * @category   Models
 * @package    App/Controllers/Components
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @version    Release: @package_version@
 * @since      Class available since Release 2.16.09
 */
class CustomAclComponent extends Object
{
    //chamado antes do Controller::beforeFilter()
    function initialize($controller, $settings = array())
    {
        // salvando a referência do controller para uso posterior
        $this->controller = $controller;
    }

    //chamado depois do Controller::beforeFilter()
    function startup($controller)
    {

    }

    //chamado depois do Controller::beforeRender()
    function beforeRender($controller)
    {
    }

    //chamado depois do Controller::render()
    function shutdown($controller)
    {
    }

    //chamado antes do Controller::redirect()
    function beforeRedirect($controller, $url, $status = null, $exit = true)
    {
    }

    function redirectSomewhere($value)
    {
        // utilizando um método de controller
        $this->controller->redirect($value);
    }

    function setupPublic()
    {

        Cache::set(array('duration' => '+1 hour'));

        if (Cache::read('rspublic') === false) {
            App::import('Model', 'Recurso');
            $objRecursoModel = new Recurso();
            $conditions = array(
                'ic_publico = TRUE'
            );
            $recursos = $objRecursoModel->find('all', compact('conditions'));

            if ($recursos) {
                // configura recursos públicos.
                $recursos_publicos = array();
                foreach ($recursos as $key => $recurso) {
                    $recursos_publicos[] = $recurso['Recurso']['ds_rota'];
                }

                Cache::write('rspublic', $recursos_publicos);
            }
        }
    }

    function isPublic($resource)
    {
        $this->setupPublic();

        if (($recursos_publicos = Cache::read('rspublic')) !== false) {
            if (in_array($resource, $recursos_publicos)) {
                return true;
            }
        }

        return false;
    }

    function setupUser()
    {
        // recursos privados.
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');

        if ($usuario) {
            $isExpired = false;
            $hasFile = false;

            $filehash = "rs" . md5($usuario['UsuarioPerfil']['co_perfil']);
            if (Cache::read($filehash) === false) {
                App::import('Model', 'UsuarioPerfil');
                $objUsuarioPerfilModel = new UsuarioPerfil();
                $arUsuarioPerfil = $objUsuarioPerfilModel->find('first', array('conditions' => array('UsuarioPerfil.co_usuario' => $usuario['Usuario']['co_usuario'])));

                App::import('Model', 'Permissao');
                $objPermissao = new Permissao();
                $arPermissao = $objPermissao->getPermissaoPorPerfil($arUsuarioPerfil['UsuarioPerfil']['co_perfil']);

                $recursos_privados = array();
                foreach ($arPermissao as $permissao) {
                    $recursos_privados[] = $permissao['r']['ds_rota'];
                }

                Cache::write($filehash, $recursos_privados);
            }
        } else {
            return false;
        }
    }

    function isAllowed($resource)
    {
        $this->setupUser();
        // recursos privados.
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $filehash = "rs" . md5($usuario['UsuarioPerfil']['co_perfil']);

        if (($recursos_privados = Cache::read($filehash)) !== false) {
            if (in_array($resource, $recursos_privados)) {
                return true;
            }
        }

        return false;
    }
}