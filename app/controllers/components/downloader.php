<?php

class DownloaderComponent extends Object
{
    public static function fetch($url)
    {
        $curlHandler = curl_init($url);

        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($curlHandler);

        curl_close($curlHandler);

        return $output;
    }
}
