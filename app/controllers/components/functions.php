<?php

/**
 *
 * @author RaulM
 *
 */
class FunctionsComponent extends Object
{

    public static function validaCNPJ($cnpj)
    {
        // Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
        $j = 0;
        for ($i = 0; $i < (strlen($cnpj)); $i ++) {
            if (is_numeric($cnpj[$i])) {
                $num[$j] = $cnpj[$i];
                $j ++;
            }
        }
        // Etapa 2: Conta os dígitos, um Cnpj válido possui 14 dígitos numéricos.
        if (count($num) != 14) {
            $isCnpjValid = false;
        }
        // Etapa 3: O número 00000000000 embora não seja um cnpj real resultaria um cnpj válido após o calculo dos dígitos verificares e por isso precisa ser filtradas nesta etapa.
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            $isCnpjValid = false;
        }  // Etapa 4: Calcula e compara o primeiro dígito verificador.
else {
            $j = 5;
            for ($i = 0; $i < 4; $i ++) {
                $multiplica[$i] = $num[$i] * $j;
                $j --;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i ++) {
                $multiplica[$i] = $num[$i] * $j;
                $j --;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                $isCnpjValid = false;
            }
        }
        // Etapa 5: Calcula e compara o segundo dígito verificador.
        if (! isset($isCnpjValid)) {
            $j = 6;
            for ($i = 0; $i < 5; $i ++) {
                $multiplica[$i] = $num[$i] * $j;
                $j --;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 5; $i < 13; $i ++) {
                $multiplica[$i] = $num[$i] * $j;
                $j --;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[13]) {
                $isCnpjValid = false;
            } else {
                $isCnpjValid = true;
            }
        }
        // Trecho usado para depurar erros.
        /*
         * if($isCnpjValid==true)
         * {
         * echo "<p><font color=\"GREEN\">Cnpj é Válido</font></p>";
         * }
         * if($isCnpjValid==false)
         * {
         * echo "<p><font color=\"RED\">Cnpj Inválido</font></p>";
         * }
         */
        // Etapa 6: Retorna o Resultado em um valor booleano.
        return $isCnpjValid;
    }

    public static function validaCPF($cpf = null)
    {

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = ereg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            // debug('diferente de 11');
            return false;
        }  // Verifica se nenhuma das sequências invalidas abaixo
          // foi digitada. Caso afirmativo, retorna falso
        else
            if (
            $cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {

                return false;
                // Calcula os digitos verificadores para verificar se o
                // CPF é válido
            } else {

                for ($t = 9; $t < 11; $t ++) {

                    for ($d = 0, $c = 0; $c < $t; $c ++) {
                        $d += $cpf{$c} * (($t + 1) - $c);
                    }
                    $d = ((10 * $d) % 11) % 10;
                    if ($cpf{$c} != $d) {
                        return false;
                    }
                }

                return true;
            }
    }

    /**
     *
     * @param
     *            $string
     * @return unknown_type
     */
    public static function limparMascara(&$string)
    {
        $string = str_replace(array(
            '.',
            '-',
            '_',
            '/',
            ' ',
            '(',
            ')'
        ), '', $string);

        return $string;
    }

    /**
     *
     * @param
     *            $fileName
     * @param
     *            $conteudo
     * @return unknown_type
     */
    public static function criarArquivo($fileName, $conteudo)
    {
        $pathName = "files/";

        $arquivo = fopen($pathName . $fileName, "w+");

        $result = fwrite($arquivo, $conteudo);

        fclose($arquivo);
    }

    /**
     *
     * @param
     *            $url
     *
     * @return unknown_type
     */
    public static function carregaUrlExterna($url)
    {
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, '1');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        $dados = curl_exec($session);
        header("Content-Type: text/html; charset=iso-8859-1");
        curl_close($session);
        return $dados;
    }

    /**
     *
     * @param
     *            $string
     * @param
     *            $mascara
     * @return unknown_type
     */
    public static function mascara($string, $mascara, $tipo = 1)
    {
        if ($string == NULL) {
            return NULL;
        }

        $mascara = self::pegarFormato($mascara, $tipo);

        if ($mascara == NULL) {
            return $string;
        }

        for ($i = 0; $i < strlen($mascara); $i ++) {
            if (substr($mascara, $i, 1) != "9" && substr($mascara, $i, 1) != "*") {
                $string = substr($string, 0, $i) . substr($mascara, $i, 1) . substr($string, $i, strlen($string));
            }
        }
        return $string;
    }

    public static function getMaskSQL($coluna, $tipo = 'processo')
    {
        $mascara = self::pegarFormato($tipo, 2);
        if ($mascara == NULL) {
            return $coluna;
        } else {
            return " mask(" . $coluna . ", '" . $mascara . "') ";
        }
    }

    public static function exibeCampoSQL($sql, $funcao) {
        $modulo = new ModuloHelper();
        if($modulo->$funcao()) {
            return $sql;
        } else {
            return '';
        }
    }

    /**
     *
     * @param
     *            $mascara
     * @return unknown_type
     */
    public static function pegarFormato($mascara, $tipo = 1)
    {
        $modulo = new ModuloHelper();
        switch ($mascara) {
            case 'pam':
                if ($tipo == 2) {
                    if (strpos(__('mascara_pam2', true), '#') === false) {
                        return '#####/####';
                    } else {
                        return __('mascara_pam2', true);
                    }
                } else {
                    if (strpos(__('mascara_pam1', true), '9') === false) {
                        return '99999/9999';
                    } else {
                        return __('mascara_pam1', true);
                    }
                }
            case 'processo':
                if ($tipo == 3) {
                    $mask = __('mascara_processo3', true);
                    if ($mask == "N") {
                        return null;
                    }
                    return $mask;
                } elseif($tipo == 4){
                    $mask = __('mascara_processo4', true);
                    if ($mask == "N") {
                        return null;
                    }
                    return $mask;
                } elseif($tipo == 17){
                    $mask = __('mascara_processo17', true);
                    if ($mask == "N") {
                        return null;
                    }
                    return $mask;
                }
                if ($tipo == 2) {
                    $mask = __('mascara_processo2', true);
                    if ($mask == "N") {
                        return null;
                    }
                    return $mask;
                } else {
                    $mask = __('mascara_processo1', true);
                    if ($mask == "N") {
                        return null;
                    }
                    return $mask;
                }
            case 'contrato':
                $mask = __('mascara_contrato' . $tipo, true);
                if ($mask == "N") {
                    return null;
                }
                return $mask;
            case 'ata':
                if ($tipo == 2) {
                    if (strpos(__('mascara_ata2', true), '#') === false) {
                        return '###/####';
                    } else {
                        return __('mascara_ata2', true);
                    }
                } else {
                    if (strpos(__('mascara_ata1', true), '9') === false) {
                        return '999/9999';
                    } else {
                        return __('mascara_ata1', true);
                    }
                }
            case 'licitacao':
                return $modulo->getMaskLicitacao();
            case 'cpf':
                return '999.999.999-99';
            case 'cnpj':
                return '99.999.999/9999-99';
            case 'cep':
                return '99999-999';
            case 'telefone':
                return '(99) 9999-9999';
            case 'ordem':
                return '9999/9999';
            case 'empenho':
                if ($tipo == 2) {
                    return __('mascara_empenho2', true);
                } else {
                    if ($tipo == 3) {
                        return str_replace('AAAA', '9999', __('mascara_empenho1', true));
                    } else {
                        return str_replace('AAAA', Date('Y'), __('mascara_empenho1', true));
                    }
                }
            default:
                return $mascara;
        }
    }

    /**
     * Retorna a data no formato brasileiro
     */
    public static function data(&$sInput)
    {
        if( empty($sInput) || $sInput == null || $sInput == '' || $sInput == '0000-00-00' ) {
            return null;
        }
        $sInput = strtotime($sInput);

        $sInput = date("d/m/Y", $sInput);

        return $sInput;
    }

    /**
     * Retorna a data e hora no formato brasileiro
     */
    public static function dataHora(&$sInput)
    {
        $sInput = strtotime($sInput);

        if (empty($sInput)) {
            return null;
        }
        $sInput = date("d/m/Y H:i:s", $sInput);

        return $sInput;
    }

    /**
     * Retorna o timestamp de uma data no formato DD/MM/AAAA
     */
    public function geraTimestamp($data, $completo = false)
    {
        $partes = explode('/', $data);
        if ($completo) {
            error_reporting(0);
            $times = explode(':', substr($partes[2], 5, 8));
            return mktime($times[0], $times[1], $times[2], $partes[1], $partes[0], substr($partes[2], 0, 4));
        } else {
            error_reporting(0);
            return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
        }
    }

    /*
     * Return null if money data comes empty
     */
    public static function validaMoney($money)
    {
        if ($money == null || $money == "" || $money == 0) {
            (float) $money = 0.00;
        }
        return $money;
    }

    public static function formataData($data)
    {
        return date("d/m/Y", strtotime("$data"));
    }

    /**
     * @return string
     */
    public static function setMask($text, $mask)
    {
        $text = str_replace(" ","",$text);

        if (strlen(str_replace(array('.', '-', '/'), '', $mask)) == strlen($text)) {
            for ($i=0;$i<strlen($text);$i++) {
                $mask[strpos($mask,"#")] = $text[$i];
            }
            return $mask;

        } else {
            return $text;
        }
    }
}

?>
