<?php

/**
 * Componente de autenticação via LDAP
 *
 * @package app.controllers
 * @subpackage components
 * @name LdapAuthComponent
 * @author Diego Costa
 */
class LdapAuthComponent extends Object
{
    /**
     * @var array|string
     */
    private $ldapParams = array();

    public function __construct()
    {
        $this->ldapParams = Configure::read('App.config.component.ldap.params');
    }

    /**
     * Verifica se o usuário está no LDAP
     *
     * @access public
     * @param array $userPostData
     * @return boolean
     */
    public function isUserInLdap($userPostData)
    {
        $ldapConnection = ldap_connect(
            $this->ldapParams['connection']['host'],
            $this->ldapParams['connection']['port']
        );

        if ($ldapConnection) {
            //these next two lines are required for windows server 03
            ldap_set_option($ldapConnection, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);

            return ldap_bind(
                $ldapConnection,
                $userPostData['no_usuario'] . "@" . $this->ldapParams['connection']['accountDomainName'],
                $userPostData['ds_senha']
            );
        }
        return false;
    }

}
