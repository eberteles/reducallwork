<?php

/**
 * Componente de busca de usuário no LDAP
 *
 * @package app.controllers
 * @subpackage components
 * @name LdapSearchComponent
 * @author Juliano Buzanello <jbcisne@gmail>
 */
class LdapSearchComponent extends Object
{
    /**
     * @var Zend_Ldap
     */
    private $objLdap = null;

    /**
     * @var array|string
     */
    private $ldapParams = array();

    public function __construct()
    {
        $this->ldapParams = Configure::read('App.config.component.ldap.params');
        if(Configure::read('App.config.component.ldap.enabled')){
            $this->objLdap = new Zend_Ldap($this->ldapParams['connection']);
        }
        
    }

    /**
     * Lista usuários do LDAP. Caso um grupo específico for definido listará usuários apenas deste grupo
     *
     * @access public
     * @param string $term termo de busca
     * @return array
     */
    public function searchUserInLdap($term)
    {
        try {
            $items = $this->objLdap->search(
                sprintf($this->ldapParams['filter'], $term)
            );
            return $this->processLdapDataUsers($items);
        } catch (\Exception $e) {
            return array('error' => true, 'msg' => $e->getMessage());
        }
    }

    /**
     * @param Zend_Ldap_Collection $collection
     * @return array
     */
    private function processLdapDataUsers(Zend_Ldap_Collection $collection)
    {
        $arrUsr = array();
        if (count($collection)) {
            foreach ($collection as $arrDataUser) {
                $arrUsr[] = $this->normalizeUser($arrDataUser);
            }
        }
        return $arrUsr;
    }

    /**
     * Normaliza os dados conforme mapeamento defino nas configurações do módulo
     *
     * @param array $arrUser
     * @return array
     */
    private function normalizeUser(array $arrUser)
    {
        $newUserData = array();
        $keymap = $this->ldapParams['user_keymap'];

        foreach ($keymap as $key => $map) {
            if (!is_null($map)) {
                if (is_array($map)) {
                    $newUserData[$key] = $this->applayFilter($map, $arrUser);
                } else {
                    $newUserData[$key] = $arrUser[$map][0];
                }
            } else {
                $newUserData[$key] = null;
            }
        }
        return $newUserData;
    }

    /**
     * @param $arrMap array com os filtros a serem aplicados [strtolower, strtoupper]
     * @param $user
     * @return string
     */
    private function applayFilter($arrMap, $user)
    {
        $value = $user[$arrMap['key']][0];
        $filters = (array)$arrMap['filters'];
        foreach ($filters as $filter) {
            $value = $filter($value);
        }
        return $value;
    }

}
