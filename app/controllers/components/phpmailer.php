<?php
/**
 * Classe ACL
 *
 * @category   Models
 * @package    App/Controllers/Components
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @version    Release: @package_version@
 * @since      Class available since Release 2.16.09
 */
class PhpmailerComponent extends Object {

    var $phpmailer = null;

    var $modulo = null;

    public $subject = null;

    public $to = null;

    public $smtpError = null;

    public $attachments = null;

    public function __construct() {
        App::import('Vendor', 'phpmailer', array(
            'file' => 'phpmailer' . DS . 'class.phpmailer.php'
        ));
        $this->phpmailer = new PHPMailer();
        $this->setupConfigs();
    }

    function setupConfigs()
    {
        $configs = Configure::read('App.config.component.email');

        $this->phpmailer->SMTPDebug 	= $configs['SMTPDebug'];

        $this->phpmailer->isSMTP();                                     									// Set mailer to use SMTP
        $this->phpmailer->Host 			= $configs['host']; 												// Specify main and backup SMTP servers
        $this->phpmailer->SMTPAuth		= (isset($configs['SMTPAuth'])) ? $configs['SMTPAuth'] : false; 	// Enable SMTP authentication
        $this->phpmailer->Username 		= $configs['username'];												// SMTP username
        $this->phpmailer->Password 		= $configs['password'];												// SMTP password
        $this->phpmailer->SMTPSecure 	= (isset($configs['SMTPSecure'])) ? $configs['SMTPSecure'] : ""; 	// Enable TLS encryption, `ssl` also accepted
        $this->phpmailer->Port 			= $configs['port'];                      							// TCP port to connect to
        $this->phpmailer->AuthType 		= $configs['AuthType'];
        $this->phpmailer->CharSet		= $configs['encoding'];                 							// TCP port to connect to
        $this->phpmailer->Realm 		= (isset($configs['Realm'])) ? $configs['Realm'] : "";
        $this->phpmailer->Workstation	= (isset($configs['Workstation'])) ? $configs['Workstation'] : "";

        $this->subject = $configs['assunto'];
        $this->setFrom($configs['from'], $configs['fromName']);
    }

    //chamado antes do Controller::beforeFilter()
    function initialize($controller, $settings = array()) {
        // salvando a referência do controller para uso posterior
        $this->controller = $controller;
    }

    //chamado depois do Controller::beforeFilter()
    function startup($controller){
    }

    //chamado depois do Controller::beforeRender()
    function beforeRender($controller) {
    }

    //chamado depois do Controller::render()
    function shutdown($controller) {
    }

    //chamado antes do Controller::redirect()
    function beforeRedirect($controller, $url, $status=null, $exit=true) {

    }

    function redirectSomewhere($value) {
        // utilizando um método de controller
        $this->controller->redirect($value);
    }

    function setFrom( $address, $name = '', $auto = true)
    {
        $this->phpmailer->setFrom($address, $name, $auto);
    }

    function addAddress( $address, $name = '', $auto = true)
    {
        $this->phpmailer->addAddress($address, $name, $auto);
    }

    function send( $message )
    {
        $stringTo = htmlentities($this->to);
        preg_match('/\&lt;(.*?)\&gt;/s', $stringTo, $emailsAddress);

        $toName = trim(substr($stringTo, 0, strpos($stringTo, '&lt;')));
        $toEmail = $emailsAddress[1];

        $this->phpmailer->addAddress($toEmail, $toName);

        if( is_string($this->attachments) ){
            $this->phpmailer->addAttachment($this->attachment);
        } else if( is_array($this->attachments) ){
            foreach( $this->attachments as $attachment ){
                $this->phpmailer->addAttachment($attachment);
            }
        }

        $this->phpmailer->isHTML(true);
        $this->phpmailer->Subject = $this->subject;
        $this->phpmailer->Body = $message;

        return $this->phpmailer->send();
    }

    function reset()
    {
        $this->phpmailer->ClearAddresses();
    }
}