<?php

/**
 *
 * @author RaulM
 *
 */
App::import('Helper', 'Html');

class ReportHtmlComponent extends Object {

    function createHmlOld($arData, $agrupador = null, $qtd_registros = 28, $resultadoHtmlAnt = null, $totalizador = false) {
        $helper = new HtmlHelper( );

        $resultadoHtml = array();
        $contentHtml = "";

        if (isset($agrupador)) {
            $resultadoHtml = $resultadoHtmlAnt;
            $contentHtml .= $helper->tag('h8', $agrupador);
            $contentHtml .= $helper->tag('/h8');
        }


        $contentHtml .= $helper->tag('table', null, array('style' => 'padding: 5px; font-size: 10pt; border-bottom:1px solid #000000; width: 100%; position: relative;', 'cellspacing' => '0', 'cellpadding' => '0'));

        //$contentHtml .= $helper->tag( 'caption' , $title , array( 'style' => 'font-size: 20px') );

        $nu_resultado = count($arData);
        if (count($arData)) {

            $contentHtml .= $helper->tableHeaders(array_keys($arData[0]['RELATORIO']), null, array('style' => 'padding: 5px; font-size: 9pt; border:1px solid #000000; text-transform:uppercase; text-align: center'));

            $contador = 0;
            foreach ($arData as $line) {
                if ($contador == $qtd_registros) {
                    $contentHtml .= $helper->tag('/table');
                    $resultadoHtml[] = $contentHtml;
                    $contentHtml = "";

                    $contentHtml .= $helper->tag('table', null, array('style' => 'page-break-inside:always; font-size: 9pt; border:1px solid #000000; width: 100%; position: relative;', 'cellspacing' => '0', 'cellpadding' => '0'));
                    $contentHtml .= $helper->tableHeaders(array_keys($arData[0]['RELATORIO']), null, array('style' => 'padding: 5px; font-size: 9pt; border:1px solid #000000; text-transform:uppercase; text-align: center'));
                    $contador = 0;
                }

                $cells = array();
                foreach($line[ 'RELATORIO' ] as $key => $value){
                    
                    $style  = 'padding: 5px;';
                    $style .= 'font-size: 10pt;';
                    $style .= 'border:1px solid #000000;';
                    
                    if(strstr($value, 'R$'))
                            $style .= 'text-align: right;';

                    if(strstr($value, '---'))
                            $style .= 'text-align: center;';
                    
                    $cells[$key] = array($value, array( 'style'=> $style ) );
                }

                $contentHtml .= $helper->tableCells( $cells );
                $contador++;
            }
        } else {
            $contentHtml .= $helper->tableCells(array('Não existem resultados a serem exibidos para este relatório.'));
        }

        $contentHtml .= $helper->tag('/table');
        $total_registros = count($arData);
        if ($totalizador) {
            $total_registros--;
        }
        if ($total_registros) {
            $contentHtml .= ' <BR> Total de registros: ' . $total_registros;
        }
        $resultadoHtml[] = $contentHtml;

//        echo ($contentHtml);
//        die;
        //return $contentHtml;
        return $resultadoHtml;
    }
    
    function createHml($arData, $agrupador = null) {
        $helper = new HtmlHelper( );
        $contentHtml = "";

        if (count($arData)) {
            if (isset($agrupador)) {
                $contentHtml .= $helper->tag('h8', $agrupador);
                $contentHtml .= $helper->tag('/h8');
            }
            
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;','class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(array_keys($arData[0]['RELATORIO']), null, null);
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;
            foreach ($arData as $line) {
                $class = '';
                if ($contador++ % 2 == 0) {
                    $class = 'altrow';
                }
                
                $cells = array();
                foreach($line[ 'RELATORIO' ] as $key => $value){
                    
                    $style  = '';
                    
                    if(strstr($value, 'R$'))
                            $style .= 'text-align: left;';

                    if(strstr($value, '---'))
                            $style .= 'text-align: center;';
                    
                    $cells[$key] = array($value, array( 'style'=> $style ) );
                }

                $contentHtml .= $helper->tableCells( $cells, array('class'=> $class) );
                
            }
            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . (count($arData) < 2 ? '0' : count($arData)) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }
        
        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }
        
        return $contentHtml;
    }

}