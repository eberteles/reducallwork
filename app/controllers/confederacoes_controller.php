<?php
class ConfederacoesController extends AppController
{

    public $name = 'Confederacoes';

    public $uses = array(
        'Confederacao'
    );
        
    public $helpers = array(
        'Imprimir'
    );
    
    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['Confederacao']['nu_confederacao']);
        $this->Functions->limparMascara($this->data['Confederacao']['nu_telefone']);
        $this->Functions->limparMascara($this->data['Confederacao']['nu_celular']);
    }
    
   public function index()
    {
        $this->gerarFiltro();
        // $this->Confederacao->recursive = 0;
        // $this->Confederacao->validate = array ();
        // $this->paginate = array('limit' => 10);
        $criteria = null;
        if (! empty($this->data)) {
            $this->prepararCampos();
            if ($this->data['Confederacao']['ds_confederacao']) {
                $criteria['ds_confederacao like'] = '%' . up($this->data['Confederacao']['ds_confederacao']) . '%';
            }
            if ($this->data['Confederacao']['sg_uf']) {
                $criteria['sg_uf'] = $this->data['Confederacao']['sg_uf'];
            }
            if ($this->data['Confederacao']['ic_cob']) {
                $criteria['ic_cob'] = $this->data['Confederacao']['ic_cob'];
            }
            if ($this->data['Confederacao']['ic_cpb']) {
                $criteria['ic_cpb'] = $this->data['Confederacao']['ic_cpb'];
            }
        }
        $this->set('confederacaos', $this->Confederacao->find('threaded', array(
            'conditions' => $criteria
        )));
        
        App::import('Model', 'Uf');
        $ufDb = new Uf();
        $query = 'SELECT UPPER(sg_uf) FROM ufs';
        $this->set ( 'estados', $ufDb->query ( $query ) );
    }
    
    public function add()
    {
        if (!empty($this->data)) {
            if (FunctionsComponent::validaCNPJ($this->data['Confederacao']['nu_confederacao'])) {
                $this->Confederacao->create();
                $this->prepararCampos();
                if ($this->Confederacao->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }else{
                $this->Session->setFlash(__('O registro não pode ser salvo porque o CNPJ não é válido!', true));
            }
        }

        $this->set('confederacoes', $this->Confederacao->find('threaded', array(
            'order' => 'ds_confederacao ASC'
        )));
        
        App::import('Model', 'Uf');
        $ufDb = new Uf();
        $query = 'SELECT sg_uf,UPPER(sg_uf) FROM ufs';
        $estados = $ufDb->query($query);
        $estadosArray = array();
        for($i = 0; $i < 27; $i++){
//            array_push($estadosArray[$estados[$i]['ufs']['sg_uf']], '');
            $estadosArray[$estados[$i]['ufs']['sg_uf']] = $estados[$i][0]['UPPER(sg_uf)'];
        }
        $this->set ( 'estados', $estadosArray );
    }
    
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->prepararCampos();
            if ($this->Confederacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Confederacao->read(null, $id);
        }
        $this->set('id', $id);
        $this->set('confederacoes', $this->Confederacao->find('threaded', array(
            'order' => 'ds_confederacao ASC'
        )));
        
        App::import('Model', 'Municipio');
        $municipioDb    = new Municipio();
        $this->set ( 'estados', $municipioDb->Uf->find ( 'list' ) );
        
        $municipios     = array();
        if ($this->data ['Confederacao'] ['sg_uf'] != "" ) {
            $municipios = $municipioDb->find ( 'list', array ('conditions' => array ('sg_uf' => $this->data ['Confederacao'] ['sg_uf'] ) ) );
        }
        $this->set ( compact ( 'municipios' ) );
    }
    
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Confederacao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    public function desvincularModalidade($idModalidade = null, $idConfederacao = null)
    {
        if (! $idModalidade && ! $idConfederacao) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $sql = "DELETE FROM modalidades_confederacoes WHERE co_confederacao = {$idConfederacao} AND co_modalidade = {$idModalidade}";

        if ($this->Confederacao->query($sql)){
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    public function iframe_vincular($coConfederacao)
    {
        $this->layout = 'blank';
        $this->set(compact('coConfederacao'));
    }
    
    public function modalidades($coConfederacao)
    {
        $this->layout = 'iframe';
        $this->set(compact('coConfederacao'));
        
        App::import('Model', 'Modalidade');
        App::import('Model', 'ModalidadesConfederacao');
        
        $modalidadesConfederacaoDb   = new ModalidadesConfederacao();
        
        if (! empty($this->data) && $this->data['ModalidadesConfederacao']['co_modalidade']) {
            $modalidadesConfederacaoDb->create();
            if ($modalidadesConfederacaoDb->save($this->data)) {
                $this->Session->setFlash(__('Modalidade Vinculada com sucesso.', true));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $modalidadeDb   = new Modalidade();
        
        //$modalidadeDb->virtualFields['list_combo'] = " CONCAT( Modalidade.ds_modalidade, ' - ', if (Modalidade.tp_modalidade is null , '' ,Modalidade.tp_modalidade) ) ";
        //$modalidadeDb->displayField = 'list_combo';
        
        $this->set ( 'listModalidades', $modalidadeDb->find ( 'list', array(
            'conditions' => array(
                "co_modalidade NOT IN ( SELECT co_modalidade FROM modalidades_confederacoes WHERE co_confederacao = $coConfederacao )"
                )
            ) ) );
        
        $conditions['joins'] = array(
            array('table' => 'modalidades_confederacoes',
                'alias' => 'ModalidadesConfederacao',
                'type' => 'LEFT',
                'conditions' => array(
                    'ModalidadesConfederacao.co_modalidade = Modalidade.co_modalidade',
                )
            )
        );
        $conditions['conditions'] = array(
            array('co_confederacao' => $coConfederacao)
        );
        $this->set ( 'modalidades', $modalidadeDb->find ( 'all', $conditions ) );
        
        $this->set('tiposModalidade', $modalidadeDb->tiposModalidade);
        $this->set('tiposProva', $modalidadeDb->tiposProva);
        
        $this->set('co_confederacao', $coConfederacao);
    }
    
    
    function listar($co_confederacao)
    {
        $this->Confederacao->recursive = 0;
        
        echo json_encode($this->Confederacao->find('list', array(
            'conditions' => array(
                'parent_id' => $co_confederacao
            )
        )));
        
        exit();
    }
}
?>