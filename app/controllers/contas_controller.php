<?php
/**
* @resource { "name": "Contas", "route":"contas", "access": "private", "type": "module" }
*/
class ContasController extends AppController
{

    var $name = 'Contas';

    var $layout = 'iframe';

    /**
    * @resource { "name": "Listagem", "route":"contas\/index", "access": "private", "type": "select" }
    */
    function index($coContrato, $ajax = false)
    {
        $this->Conta->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Conta.contrato_id' => $coContrato
            )
        );
        
        $this->set('contas', $this->paginate());
        
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "iFrame", "route":"contas\/iframe", "access": "private", "type": "select" }
    */
    function iframe($coContrato)
    {
        $this->layout = 'ajax';
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Novo Conta", "route":"contas\/add", "access": "private", "type": "insert" }
    */
    function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->Conta->create();
            if ($this->Conta->saveAll($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set( 'operadoras', $this->Conta->Operadora->find('list') );
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Editar Conta", "route":"contas\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Conta->saveAll($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Conta->read(null, $id);
        }
        $this->set( 'operadoras', $this->Conta->Operadora->find('list') );
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    /**
    * @resource { "name": "Remover Conta", "route":"contas\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Conta->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
    
    function find_gestor($contrato_id, $cpf) {
        echo json_encode ( $this->Conta->Gestor->find( 'first', array(
            'fields' => array(
                'id', 'nome', 'rg', 'nascimento'
            ),
            'conditions' => array('contrato_id'=>$contrato_id, 'cpf'=>$cpf)
        ) ) );
        exit();
    }
}
?>