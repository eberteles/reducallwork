<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 17:13
 */
/**
 * @resource { "name" : "Contatos", "route":"contatos", "access": "private", "type": "module" }
 */
class ContatosController extends AppController
{
    var $name = "Contatos";
    var $layout = 'iframe';

    var $tipos = array(
        'RESPONSÁVEL ADMINISTRATIVO' => 'RESPONSÁVEL ADMINISTRATIVO',
        'RESPONSÁVEL COMERCIAL'      => 'RESPONSÁVEL COMERCIAL',
        'PRESSUPOSTO'                => 'PRESSUPOSTO',
        'RECEPCIONISTA'              => 'RECEPCIONISTA',
        'OUTROS'                     => 'OUTROS'
    );
    /**
     * @resource { "name" : "Contatos", "route":"contatos\/index", "access": "private", "type": "select" }
     */
    public function index($coFornecedor)
    {
        $this->Contato->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Contato.ic_ativo' => 1
            )
        );
        $criteria['Contato.co_fornecedor'] = $coFornecedor;

        $this->set('contatos', $this->paginate($criteria));
        $this->set('coFornecedor', $coFornecedor);
    }

    function iframe($coFornecedor)
    {
        $this->set(compact('coFornecedor'));
    }
    /**
     * @resource { "name" : "Adicionar Contato", "route":"contatos\/add", "access": "private", "type": "insert" }
     */
    public function add($coFornecedor = null)
    {
        if(!empty($this->data)) {
            $this->Contato->create();

            if($this->Contato->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coFornecedor
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('tipos', $this->tipos);
        if($coFornecedor != null)
            $this->set('coFornecedor', $coFornecedor);
    }
    /**
     * @resource { "name" : "Editar Contato", "route":"contatos\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null, $coFornecedor = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }

        if(!empty($this->data)) {
            $coFornecedor = $this->data['Contato']['co_fornecedor'];
            if($this->Contato->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coFornecedor
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Contato->read(null, $id);
        $this->set('tipos', $this->tipos);
    }
    /**
     * @resource { "name" : "Remover Contato", "route":"contatos\/logicDelete", "access": "private", "type": "delete" }
     */
    public function logicDelete($id = null, $coFornecedor)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }

        if ( $id ) {
            $this->Contato->id = $id;
            $msg = null;
            $especialidade['Contato']['ic_ativo'] = 0;

            if($this->Contato->saveField('ic_ativo',0)){
                $msg = "Contato bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }
    }
}