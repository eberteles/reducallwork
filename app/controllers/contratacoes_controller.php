<?php
/**
 * @resource { "name" : "Cadastro de modalidade de contratação", "route":"contratacoes", "access": "private", "type": "module" }
 */
class ContratacoesController extends AppController
{

    var $name = 'Contratacoes';
    /**
     * @resource { "name" : "iframe", "route":"contratacoes\/iframe", "access": "private", "type": "select" }
     */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name" : "Close iframe", "route":"contratacoes\/close", "access": "private", "type": "select" }
     */
    function close( $co_contratacao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_contratacao' ) );
    }
    
    /**
     * @resource { "name" : "Listar json", "route":"contratacoes\/listar", "access": "private", "type": "select" }
     */
    function listar() {

        $this->loadModel('Contrato');
        echo json_encode ($this->Contrato->Contratacao->find('list', array('conditions' => 'Contratacao.at_contratacao = 2'))); //( 'list' , array('conditions' => array('at_contratacao' => 2)) ) );

        exit ();
    }
    /**
     * @resource { "name" : "Modalidade de Contratação", "route":"contratacoes\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->gerarFiltro();
        $criteria = null;
        if (! empty($this->data)) {
            $dsContratacao = '';
            if (!empty($this->data['Contratacao']['ds_contratacao'])) {
                $dsContratacao = up(trim($this->data['Contratacao']['ds_contratacao']));
            }  
            
            $this->Contratacao->recursive = 0;
            
            $contratacoes = $this->paginate('Contratacao', array(
                'Contratacao.ds_contratacao LIKE' => $dsContratacao . '%',
                'Contratacao.at_contratacao' => 2
            ));

            $this->set('contratacoes', $contratacoes);
        } else{
            $x = $this->paginate('Contratacao', array('at_contratacao' => 2));
            $this->Contratacao->recursive = 0;
            $this->set('contratacoes', $x);
        }
    }
    /**
     * @resource { "name" : "Nova Modalidade de Contratação", "route":"contratacoes\/add", "access": "private", "type": "insert" }
     */
    function add($modal = false) 
    {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty($this->data)) {
            $this->Contratacao->create();

            $contratacaoInativa = $this->Contratacao->find(
                array(
                    'ds_contratacao' => $this->data['Contratacao']['ds_contratacao'],
                    'at_contratacao' => 1
                ));

            if($contratacaoInativa){
                $contratacaoInativa['Contratacao']['at_contratacao'] = 2;
                if ($this->Contratacao->save($contratacaoInativa)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Contratacao->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro reativado com sucesso', true));
                        $this->redirect(array(
                            'action' => 'index'
                        ));
                    }
                }
            } elseif ($this->Contratacao->save($this->data)) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->Contratacao->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }
    /**
     * @resource { "name" : "Editar Modalidade de Contratação", "route":"contratacoes\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Contratacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = ($this->Contratacao->findByCoContratacao($id));
        }
    }
    /**
     * @resource { "name" : "Exclusão logica de Contratação", "route":"contratacoes\/logicDelete", "access": "private", "type": "delete" }
     */
    function logicDelete($id = null){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Contratacao->read(null, $id)) {
            $contratacao = $this->Contratacao->read(null, $id);
            $msg = null;
            $contratacao['Contratacao']['at_contratacao'] = 1;

            if($this->Contratacao->save($contratacao)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção do usuário";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
    
    /**
     * @resource { "name" : "Remover Modalidade de Contratação", "route":"contratacoes\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $contratacao = $this->Contratacao->find(array('co_contratacao' => $id));
        }

        if ($this->Contratacao->delete($id)) {            
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>
