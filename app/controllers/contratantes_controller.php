<?php
/**
 * @resource { "name" : "Cadastro de Contratatantes", "route":"contratantes", "access": "private", "type": "module" }
 */
class ContratantesController extends AppController
{

    var $name = 'Contratantes';

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['Contratante']['nu_contato']);
    }
    /**
     * @resource { "name" : "Contratantes", "route":"contratantes\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->gerarFiltro();
        $this->Contratante->recursive = 0;
        $this->Contratante->validate = array();
        $this->paginate = array(
            'limit' => 10
        );
        $criteria = null;
        if (! empty($this->data)) {
            $this->prepararCampos();
            $criteria['ds_contratante like'] = '%' . $this->data['Contratante']['ds_contratante'] . '%';
        }
        $this->set('contratantes', $this->paginate($criteria));
    }
    /**
     * @resource { "name" : "Adicionar Contratante", "route":"contratantes\/add", "access": "private", "type": "insert" }
     */
    function add()
    {
        if (! empty($this->data)) {
            $this->Contratante->create();
            $this->prepararCampos();
            if ($this->Contratante->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }
    /**
     * @resource { "name" : "Editar Contratante", "route":"contratantes\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->prepararCampos();
            if ($this->Contratante->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Contratante->read(null, $id);
        }
    }
    /**
     * @resource { "name" : "Remover Contratante", "route":"contratantes\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $contratante = $this->Contratante->find(array('co_contratante' => $id));
        }

        if ($this->Contratante->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($contratante['Contratante']);
            $log['ds_tabela'] = 'contratantes';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>