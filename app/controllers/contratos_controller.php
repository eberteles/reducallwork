<?php

/**
 * @resource { "name" : "Contratos", "route":"contratos", "access": "private", "type": "module" }
 */
class ContratosController extends AppController
{

    var $name = 'Contratos';

    var $tpOperadores = null;

    var $helpers = array('Imprimir', 'Util', 'Sei');

    var $modulo = null;

    public function __construct()
    {
        App::import('Helper', 'Modulo');
        App::import('Helper', 'Util');
        $this->modulo = new ModuloHelper();
        $this->Util = new UtilHelper();

        $this->tpOperadores = $this->modulo->getOperadoresPadrao();

        parent::__construct();
    }

    private function prepararCampos()
    {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();

        $this->Functions->limparMascara($this->data['Contrato']['nu_contrato']);
        if ($this->modulo->mascaraNuProcesso) {
            $this->Functions->limparMascara($this->data['Contrato']['nu_processo']);
        }
        $this->Functions->limparMascara($this->data['Contrato']['nu_licitacao']);
        $this->Functions->limparMascara($this->data['Contrato']['nu_cnpj']);
    }

    private function prepararCamposFornecedor()
    {
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_cnpj']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_telefone']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_fax']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_cep']);
    }

    private function avisarSetorFluxo($coContrato)
    {
        App::import('Model', 'Alerta');
        $alerta = new Alerta();
        $configuracao = $alerta->find('first');
        if (isset($configuracao['Alerta']['ck_aviso_muda_fase']) && $configuracao['Alerta']['ck_aviso_muda_fase'] == '1') {
            $this->Contrato->recursive = 1;
            $contrato = $this->Contrato->read(null, $coContrato);
            if (isset($contrato['SetorAtual']['co_setor'])) {

                $this->EmailProvider->to = $contrato['SetorAtual']['ds_setor'] . ' <' . $contrato['SetorAtual']['ds_email'] . '>';
                $this->EmailProvider->subject = __('PAM', true) . ' encaminhado para o Setor ' . $contrato['SetorAtual']['ds_setor'];
                $mensagem = 'O ' . __('PAM', true) . ' abaixo foi encaminhado para o Setor ' . $contrato['SetorAtual']['ds_setor'] . '<br>' .
                    __('PAM', true) . ': ' . $contrato['Contrato']['nu_pam'] . '<br>' .
                    'Fase: ' . $contrato['Fase']['ds_fase'] . '<br>' .
                    'Data: ' . $contrato['Contrato']['dt_fase'];
                $this->EmailProvider->send($mensagem);
            }
        }
    }
    
    /**
     * @resource { "name" : "Meus Registros", "route":"contratos\/index", "access": "private", "type": "select" }
     */
    function index($papel = null)
    {
        $this->Contrato->recursive = 1;
        $this->Contrato->validate = array();

        if ($this->modulo->siasg == true) {
            if (isset($this->data['Filter']['filter_type']) && $this->data['Filter']['filter_type'] == 'PAMs') {
                $this->paginate = array(
                    'limit' => 10,
                    'order' => array(
                        'Contrato.co_contrato' => 'desc'
                    )
                );
            } else {
                $this->paginate = array(
                    'limit' => 10,
                    'order' => array(
                        'CONCAT(SUBSTRING(nu_contrato,6,4), SUBSTRING(nu_contrato,1,5))' => 'desc'
                    )
                );
            }
        } else {
            $this->paginate = array(
                'limit' => 10
            );
        }

        if ( $this->Session->read('pesquisa') != null
             && $this->params['named']['page'] != null ) {

            $criteria = $this->Session->read('pesquisa');

        }

        $usuario = $this->Session->read('usuario');

        $criteria['Contrato.ic_ativo'] = '1';

        if (!empty($this->data)) {
            $this->prepararCampos();
            if (!empty($this->data['Contrato']['ic_tipo_contrato']) && $this->data['Contrato']['ic_tipo_contrato'] != '') {
                $usuario['ic_tipo_contrato'] = $this->data['Contrato']['ic_tipo_contrato'];
                $criteria['ic_tipo_contrato'] = $this->data['Contrato']['ic_tipo_contrato'];
            }
            if (!empty($this->data['Contrato']['nu_contrato'])) {
                $criteria['nu_contrato'] = $this->data['Contrato']['nu_contrato'];
            }
            if (!empty($this->data['Contrato']['nu_processo'])) {
                $criteria['nu_processo'] = $this->data['Contrato']['nu_processo'];
            }
            if (!empty($this->data['Filter']['filter_type'])) {
                switch ($this->data['Filter']['filter_type']) {
                    case 'Contratos':
                        $criteria['and'] = array(
                            'nu_contrato !=' => ''
                        );
                        break;
                    case 'Processos':
                        $criteria['and'] = array(
                            'nu_contrato' => '',
                            'nu_processo !=' => ''
                        );
                        break;
                    case 'PAMs':
                        $criteria['and'] = array(
                            'nu_pam !=' => '',
                            'nu_processo' => '',
                            'nu_contrato' => ''
                        );
                        break;
                    default:
                        $criteria['or'] = array(
                            'nu_pam !=' => '',
                            'nu_processo !=' => '',
                            'nu_contrato !=' => ''
                        );
                        break;
                }
            }

            if (!empty($this->data['Filter']['filter_uasg'])) {
                $criteria['Contrato.uasg'] = $this->data['Filter']['filter_uasg'];
            } else {
                unset($criteria['Contrato.uasg']);
            }

            $palavraChavePost = null;

            if (!empty($this->data['Filter']['palavra_chave'])) {
                $palavraChavePost = $this->data['Filter']['palavra_chave'];
            } else if (!empty($this->data['Contrato']['palavra_chave'])) {
                $palavraChavePost = $this->data['Contrato']['palavra_chave'];
            }

            if ($palavraChavePost) {
                $acentos = array(
                    "Ã" => "A",
                    "Á" => "A",
                    "À" => "A",
                    "Â" => "A",
                    "É" => "E",
                    "Ê" => "E",
                    "Í" => "I",
                    "Ì" => "I",
                    "Ó" => "O",
                    "Ò" => "O",
                    "Ô" => "O",
                    "Ú" => "U",
                    "Ç" => "C",
                    "á" => "a",
                    "à" => "a",
                    "â" => "a",
                    "ã" => "a",
                    "é" => "e",
                    "ê" => "e",
                    "í" => "i",
                    "ì" => "i",
                    "ó" => "o",
                    "ò" => "o",
                    "ô" => "o",
                    "ú" => "u",
                    "ç" => "c"
                );

                $palavraChavePost = strtr($palavraChavePost, $acentos);
                $pesquisaUp = $palavraChavePost;
                $pesquisaUp = up($pesquisaUp);
                $pesquisaSemMascara = $palavraChavePost;
                $pesquisaSemMascara = $this->Functions->limparMascara($pesquisaSemMascara);


                $criteria['or'] = array(
                    'ds_objeto like' => '%' . $pesquisaUp . '%',
                    'nu_contrato like' => '%' . $pesquisaSemMascara . '%',
                    'nu_processo like' => '%' . $pesquisaSemMascara . '%',
                    'nu_pam like' => '%' . $pesquisaUp . '%',
                    'nu_pam like' => '%' . $pesquisaSemMascara . '%',
                    'UPPER(retira_acento(GestorAtual.ds_nome)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(GestorSuplente.ds_nome)) like' => '%' . $pesquisaUp . '%',
                    'Fornecedor.nu_cnpj like' => '%' . $pesquisaSemMascara . '%',
                    'UPPER(retira_acento(Fornecedor.no_razao_social)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Cliente.no_razao_social)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Contrato.ds_observacao)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Situacao.ds_situacao)) like' => '%' . $pesquisaUp . '%',
                    'Contrato.nu_licitacao like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Contrato.ds_observacao)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Contrato.ds_fundamento_legal)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Contratacao.ds_contratacao)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Setor.ds_setor)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Solicitante.ds_nome)) like' => '%' . $pesquisaUp . '%',
                    'UPPER(retira_acento(Modalidade.ds_modalidade)) like' => '%' . $pesquisaUp . '%',
                );
            }

            if (!empty($this->data['Contrato']['nu_cnpj'])) {
                $fornecedor = $this->Contrato->Fornecedor->find(array(
                    'nu_cnpj' => $this->data['Contrato']['nu_cnpj']
                ));
                if (!($fornecedor['Fornecedor']['co_fornecedor'] > 0)) {
                    $fornecedor['Fornecedor']['co_fornecedor'] = 0;
                }
                $criteria['Contrato.co_fornecedor'] = $fornecedor['Fornecedor']['co_fornecedor'];
            }
            $this->Session->write('pesquisa', $criteria);
        }

        $moduloConfig = 'ctr';
        if (isset($usuario['ic_tipo_contrato']) && $usuario['ic_tipo_contrato'] == 'E') {
            $moduloConfig = 'cte';
        }

        App::import('Model', 'ColunaResultadoPesquisa');
        $dbColunaResultadoPesquisa = new ColunaResultadoPesquisa();
        $this->set('colunasResultadoPesquisa', $dbColunaResultadoPesquisa->findUsuario('all', $moduloConfig));

        App::import('Model', 'Uasg');
        $uasgModel = new Uasg();
        $uasgs = $uasgModel->find('all');

        $uasgArray = array();

        foreach ($uasgs as $uasg) {
            $uasgArray[$uasg['Uasg']['uasg']] = $uasg['Uasg']['uasg'];
        }

        $this->set('uasgsArray', $uasgArray);

        if (isset($this->params['url']['papel'])) {
            $papel = $this->params['url']['papel'];
        } else {
            $papel = $papel;
        }

        App::import('Model', 'ContratoFiscal');
        $contratoFiscal = new ContratoFiscal();
        $coFiscais = $contratoFiscal->findAllByUsuario($usuario['Usuario']['co_usuario']);

        switch ($papel) {
            case 'gestor':
                $criteria['or'] = array(
                    'Contrato.co_gestor_atual' => $usuario['Usuario']['co_usuario'],
                    'Contrato.co_gestor_suplente' => $usuario['Usuario']['co_usuario']
                );
                break;
            case 'fiscal':
                foreach ($coFiscais as $fiscal) {
                    $listContratosFiscais['or'][] = array(
                        'Contrato.co_contrato' => $fiscal
                    );
                }

                $criteria['or'] = array(
                    $listContratosFiscais
                );
                break;
        }

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $this->set('papel', $papel);

        if ($modulo->getArrayUtf8Encoder()) {
            $this->set('contratos', utf8_converter_array($this->paginate($criteria)));
        } else {
            $this->set('contratos', $this->paginate($criteria));
        }

        if ($this->RequestHandler->isPost()) {
            $this->set('palavra_chave', $palavraChavePost);

            $usuario['palavra_chave'] = $palavraChavePost;

        }elseif(!$this->RequestHandler->isPost() && $this->params['named']['page'] == null){

            $criteria = '';
            $this->set('palavra_chave', '');
            unset($usuario['palavra_chave']);

        }

        $this->Session->write('usuario', $usuario);
    }

    /**
     * @resource { "name" : "Novo Contrato", "route":"contratos\/add", "access": "private", "type": "insert" }
     */
    public function add($idContratoPai = null)
    {
        $this->setValidacoesContrato();
        $usuario = $this->Session->read('usuario');

        if ($idContratoPai != null) {
            $this->set('nuContratoPai', $this->Contrato->field('nu_contrato', array('co_contrato' => $idContratoPai)));
            $this->set('temPai', true);
        } else {
            $this->set('nuContratoPai', '');
            $this->set('temPai', false);
        }
        $this->set('idContratoPai', $idContratoPai);

        if (!empty($this->data)) {
            $this->Contrato->create();

            $this->prepararCampos();
            $this->data['Contrato']['dt_cadastro'] = DboSource::expression('CURRENT_TIMESTAMP');
            if ($this->Contrato->save($this->data)) {
                $this->data['co_contrato']  = $this->Contrato->id;
                $this->addAndamento('Contrato cadastrado.', array('co_contrato'=>$this->Contrato->id));
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $this->Contrato->getInsertID()
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $conditions = array(
            'Usuario.ic_ativo' => 1
        );

        App::import('Model', 'Usuario', 'Perfil');
        // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
        $perfilModel = new Perfil();
        $gestor = $perfilModel->find('first', array(
            'conditions' => array('Perfil.no_perfil' => 'Gestor')
        ));
        $modelUsuario = new Usuario();

        $this->set('gestores', $modelUsuario->find('list', array(
            'conditions' => array(
              'Usuario.ic_ativo' => 1,
              'Usuario.ic_acesso' => 1,
              //'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'recursive' => 2,
            'order' => 'ds_nome ASC'
        )));

        $setorConditions = array(
            'Setor.ic_ativo' => 1
        );

        // Se administrador pode visualizar arvore completa de setores.
        if ($usuario['UsuarioPerfil']['co_perfil'] != $coAdministrador) {
            $setorConditions['Setor.co_setor'] = $usuario['Usuario']['co_setor'];
        }

        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => $setorConditions
        ));
        $this->set(compact('contratantes'));

        App::import('Model', 'Usuario', 'Perfil');
        // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
        $perfilModel = new Perfil();
        $gestor = $perfilModel->find('first', array(
            'conditions' => array('Perfil.no_perfil' => 'Gestor')
        ));

        $usuarioModel = new Usuario();

        $this->set('usuarios', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_acesso' => 0,
                'Usuario.ic_ativo' => 1
            ),
            'UsuarioPerfil' => array(
                'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'order' => array('ds_nome ASC')
        )));


        App::import('Model', 'Usuario', 'Perfil');
        // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
        $perfilModel = new Perfil();
        $perfilFiscal = $perfilModel->find('first', array(
            'conditions' => array('Perfil.no_perfil' => 'Fiscal')
        ));

        $usuarioModel = new Usuario();
        $this->usuarioModel->recursive = 2;

        $this->set('fiscais', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_acesso' => 1,
                'Usuario.ic_ativo' => 1,
                'UsuarioPerfil.co_perfil' => $perfilFiscal['Perfil']['co_perfil']
            ),
            'recursive' => 2,
            'order' => array('ds_nome ASC')
        )));

        /*$fornecedores = $this->Contrato->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        );*/

        $clientes = $this->Contrato->Cliente->find('list', array(
                'fields' => array(
                    'Cliente.co_cliente',
                    'Cliente.no_razao_social'
                )
            )
        );

        //$this->set(compact('fornecedores'));
        $this->set(compact('clientes'));


        $modalidades = $this->Contrato->Modalidade->find('list');
        $this->set(compact('modalidades'));

        $this->set('contratacoes', $this->Contrato->Contratacao->find('list', array('conditions' => 'Contratacao.at_contratacao = 2')));
        $servicos = $this->Contrato->Servico->find('list');

        $this->set(compact('servicos'));
        $situacaos = $this->Contrato->Situacao->find('list', array(
            'conditions' => array(
                'Situacao.tp_situacao = "C"',
                'Situacao.ic_ativo' => 1
            )
        ));

        $this->set(compact('situacaos'));
        $this->set('categorias', $this->Contrato->Categoria->find('list'));
        $this->set('responsaveis', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_ativo' => 1,
            ),
            'order' => array('ds_nome ASC')
        )));
    }

    /**
     * @resource { "name" : "Pesquisar Processo", "route":"contratos\/find_processo", "access": "private", "type": "select" }
     */
    function find_processo()
    {
        if (!empty($this->data)) {
            $co_contrato = 0;
            $nu_processo = $this->Functions->limparMascara($this->data['Contrato']['nu_processo']);
            if ($nu_processo > 0) {
                $processo = $this->Contrato->find('first', array(
                    'fields' => array(
                        'co_contrato'
                    ),
                    'conditions' => ' Contrato.nu_processo = ' . $nu_processo
                ));
                if (count($processo, COUNT_RECURSIVE) > 1) {
                    $co_contrato = $processo['Contrato']['co_contrato'];
                }
            }
        }
        echo $co_contrato;
        exit();
    }

    function find_prefeitura_militar()
    {
        $result = array(
            'processo' => $this->Functions->limparMascara($this->data['Contrato']['nu_processo']),
            'contrato' => $this->data['Contrato']['co_contratacao']
        );

        echo $this->Contrato->find('count', array(
            'conditions' => ' Contrato.nu_processo = ' . $result['processo'] . ' AND Contrato.co_contratacao = ' . $result['contrato']
        ));

        die();
    }

    function find_candidato()
    {
        if (!empty($this->data)) {
            $co_candidato = 0;
            $nu_cpf = $this->Functions->limparMascara($this->data['nu_cpf']);
            if ($nu_cpf > 0) {
                $candidato = $this->Contrato->Fornecedor->find('first', array(
                    'fields' => array(
                        'co_fornecedor'
                    ),
                    'conditions' => " Fornecedor.nu_cnpj = '" . $nu_cpf . "' "
                ));
                if (count($candidato, COUNT_RECURSIVE) > 1) {
                    $co_candidato = $candidato['Fornecedor']['co_fornecedor'];
                }
            }
        }
        echo $co_candidato;
        exit();
    }
    
    /**
     * @resource { "name" : "Pesquisar PAM", "route":"contratos\/find_pam", "access": "private", "type": "select" }
     */
    function find_pam()
    {
        if (!empty($this->data)) {
            $co_contrato = 0;
            $nu_pam = $this->data['Contrato']['nu_pam'];
            // var_dump($nu_pam > 0);exit;
            if (count($nu_pam)) {
                $pam = $this->Contrato->find('first', array(
                    'fields' => array(
                        'co_contrato'
                    ),
                    'conditions' => " Contrato.nu_pam = '" . $nu_pam . "' "
                ));
                if (count($pam, COUNT_RECURSIVE) > 1) {
                    $co_contrato = $pam['Contrato']['co_contrato'];
                }
            }
        }
        echo $co_contrato;
        exit();
    }

    /**
     * @resource { "name" : "Novo Processo", "route":"contratos\/add_processo", "access": "private", "type": "insert" }
     */
    function add_processo()
    {
        if ($this->modulo->isPam()) {
            $this->Contrato->validate['nu_pam'] = array(
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Este número de ' . __('PAM', true) . ' já encontra-se cadastrado.',
                    'allowEmpty' => true
                ),
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Campo ' . __('PAM', true) . ' em branco',
                    'allowEmpty' => true
                )
            );
            $this->Contrato->validate['dt_autorizacao_pam'] = array(
                'date' => array(
                    'rule' => array(
                        'date'
                    ),
                    'message' => 'Data de Autorização do ' . __('PAM', true) . ' inválida.',
                    'allowEmpty' => true
                )
            );
        }

        unset($this->Contrato->validate['nu_contrato']);

        if (!empty($this->data)) {
            $this->Contrato->create();
            $this->prepararCampos();
            $this->data['Contrato']['dt_cadastro_processo'] = DboSource::expression('CURRENT_TIMESTAMP');

            $nu_sequencia_and = null;
            if (!$this->modulo->isPam()) {
                $nu_sequencia_and = 1;
                $this->data['Contrato']['nu_sequencia'] = 1;
                $fluxo = $this->Contrato->Fluxo->read(null, 1);
                $this->data['Contrato']['co_fase'] = $fluxo['Fluxo']['co_fase'];
                $this->data['Contrato']['co_setor'] = $fluxo['Fluxo']['co_setor'];
                $this->data['Contrato']['dt_fase'] = date('d/m/Y');
            }

            if ($this->modulo->isAutoNumeracao()) {
                $this->data['Contrato']['nu_processo'] = $this->Contrato->getNextProcesso();
            }

            if ($this->Contrato->save($this->data)) {

                $this->addAndamento('Processo cadastrado.', array('co_contrato'=>$this->Contrato->getInsertID(),'nu_sequencia'=>$nu_sequencia_and));
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $this->Contrato->getInsertID()
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $usuario = $this->Session->read('usuario');

        $setorConditions = array(
            'Setor.ic_ativo' => 1
        );
        // Se administrador pode visualizar arvore completa de setores.
        if ($usuario['UsuarioPerfil']['co_perfil'] != $coAdministrador) {
            $setorConditions['Setor.co_setor'] = $usuario['Usuario']['co_setor'];
        }

        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => $setorConditions
        ));
        $this->set(compact('contratantes'));
        $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        );
        $this->set(compact('fornecedores'));
        $modalidades = $this->Contrato->Modalidade->find('list');
        $this->set(compact('modalidades'));
        $this->set('contratacoes', $this->Contrato->Contratacao->find('list'));
        $servicos = $this->Contrato->Servico->find('list');
        $this->set(compact('servicos'));
        $situacaos = $this->Contrato->Situacao->find('list', array(
            'conditions' => array(
                'Situacao.tp_situacao = "P"',
                'Situacao.ic_ativo' => 1
            )
        ));
        $this->set(compact('situacaos'));
        $this->set('categorias', $this->Contrato->Categoria->find('list'));
    }

    /**
     * @resource { "name" : "Novo PAM", "route":"contratos\/add_pam", "access": "private", "type": "insert" }
     */
    function add_pam()
    {
        $this->setValidacoesPam();
        if (!empty($this->data)) {
            $this->Contrato->create();
            //$nu_pam = $this->data['Contrato']['nu_pam'];
            $nu_pam = $this->Contrato->getNextPam();
            $this->data['Contrato']['nu_pam'] = $nu_pam;
            $this->prepararCampos();
            $this->data['Contrato']['dt_cadastro_pam'] = DboSource::expression('CURRENT_TIMESTAMP');
            
            $dsAndamento    = __('PAM', true) . ' cadastrada.';
            $this->data['Contrato']['nu_sequencia'] = 1;
            if (!empty($this->data['Contrato']['co_fase']) && $this->data['Contrato']['co_fase'] > 0) {
                $this->data['Contrato']['nu_sequencia'] = $this->data['Contrato']['co_fase'];
                if (!empty($this->data['Contrato']['ds_andamento']) && $this->data['Contrato']['ds_andamento'] != '') {
                    $dsAndamento    = $this->data['Contrato']['ds_andamento'];
                }
            }
            
            $fluxo = $this->Contrato->Fluxo->read(null, $this->data['Contrato']['nu_sequencia']);
            $this->data['Contrato']['co_fase'] = $fluxo['Fluxo']['co_fase'];
            $this->data['Contrato']['co_setor'] = $fluxo['Fluxo']['co_setor'];
            $this->data['Contrato']['dt_fase'] = date('d/m/Y');

            if ($this->Contrato->save($this->data)) {
                App::import('Model', 'Anexo');
                $anexoDb = new Anexo();

                if (!empty($this->data['Contrato']['anexo']) && $this->data['Contrato']['anexo']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '1º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo'];
                    $anexoDb->save($anexo);
                }
                if (!empty($this->data['Contrato']['anexo2']) && $this->data['Contrato']['anexo2']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '2º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo2'];
                    $anexoDb->save($anexo);
                }
                if (!empty($this->data['Contrato']['anexo3']) && $this->data['Contrato']['anexo3']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '3º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo3'];
                    $anexoDb->save($anexo);
                }
                
                $this->addAndamento($dsAndamento, array('co_contrato'=>$this->Contrato->getInsertID(),'nu_sequencia'=>$this->data['Contrato']['nu_sequencia'])); 

                //$this->avisarSetorFluxo($this->Contrato->getInsertID());
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $this->Contrato->getInsertID()
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $usuario = $this->Session->read('usuario');

        $setorConditions = array(
            'Setor.ic_ativo' => 1
        );
        // Se administrador pode visualizar arvore completa de setores.
        if ($usuario['UsuarioPerfil']['co_perfil'] != $coAdministrador) {
            $setorConditions['Setor.co_setor'] = $usuario['Usuario']['co_setor'];
        }

        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => $setorConditions
        ));
        $this->set(compact('contratantes'));

        $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        );
        $this->set(compact('fornecedores'));
        
        $clientes = $this->Contrato->Cliente->find('list', array(
                'fields' => array(
                    'Cliente.co_cliente',
                    'Cliente.no_razao_social'
                )
            )
        );
        $this->set(compact('clientes'));
        
        $fases  = $this->Contrato->Fase->find('list', array(
                'fields' => array(
                    'Fase.co_fase',
                    'Fase.ds_fase'
                )
            )
        );
        $this->set(compact('fases'));
        
        App::import('Model', 'Usuario');
        $usuarioModel = new Usuario();

        $this->set('usuarios', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_acesso' => 0,
                'Usuario.ic_ativo' => 1
            ),
            'UsuarioPerfil' => array(
                'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'order' => array('ds_nome ASC')
        )));
        
        $this->set('gestores', $usuarioModel->find('list', array(
            'conditions' => array(
              'Usuario.ic_ativo' => 1,
              'Usuario.ic_acesso' => 1,
              //'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'recursive' => 2,
            'order' => 'ds_nome ASC'
        )));
    }

    function add_inscricao($co_candidato = 0)
    {
        $usuario = $this->Session->read('usuario');
        if (isset($usuario['Usuario']['co_fornecedor']) && $usuario['Usuario']['co_fornecedor'] > 0) {
            $co_candidato = $usuario['Usuario']['co_fornecedor'];
        }

        if ($co_candidato > 0 && $this->Contrato->find('count', array('conditions' => array('Contrato.co_fornecedor' => $co_candidato, "DATE_FORMAT(dt_cadastro_processo, '%Y')" => date('Y')))) > 0) {
            $this->Session->setFlash(__('Já existe uma Inscrição realizada para este Atleta.', true));
            $this->redirect(array(
                'controller' => 'contratos',
                'action' => 'index'
            ));
        }

        $this->setValidacoesInscricao();
        if (!empty($this->data)) {

            $this->prepararCamposFornecedor();

            if ($this->data['Fornecedor']['co_fornecedor'] == '' || $this->data['Fornecedor']['co_fornecedor'] == '0') {
                $this->Contrato->Fornecedor->create();
                $this->Contrato->Fornecedor->save($this->data);
                $this->data['Contrato']['co_fornecedor'] = $this->Contrato->Fornecedor->getInsertID();
            } else {
                $this->Contrato->Fornecedor->save($this->data);
                $this->data['Contrato']['co_fornecedor'] = $this->data['Fornecedor']['co_fornecedor'];
            }
            //$this->data['Contrato']['co_contratacao'] = $this->data['Fornecedor']['co_contratacao'];
            $this->data['Contrato']['co_confederacao'] = $this->data['Fornecedor']['co_confederacao'];
            $this->data['Contrato']['co_federacao'] = $this->data['Fornecedor']['co_federacao'];
            $this->data['Contrato']['co_modalidade'] = $this->data['Fornecedor']['co_modalidade'];
            $this->data['Contrato']['co_prova'] = $this->data['Fornecedor']['co_prova'];
            $this->data['Contrato']['co_evento'] = $this->data['Fornecedor']['co_evento'];
            $this->data['Contrato']['co_classificacao'] = $this->data['Fornecedor']['co_classificacao'];

            $this->Contrato->create();
            $this->prepararCampos();
            $this->data['Contrato']['dt_cadastro_processo'] = DboSource::expression('CURRENT_TIMESTAMP');
            $this->data['Contrato']['nu_processo'] = $this->Contrato->getNextProcesso();

            $this->data['Contrato']['nu_sequencia'] = 1;
            $fluxo = $this->Contrato->Fluxo->read(null, 1);
            $this->data['Contrato']['co_fase'] = $fluxo['Fluxo']['co_fase'];
            $this->data['Contrato']['co_setor'] = $fluxo['Fluxo']['co_setor'];
            $this->data['Contrato']['dt_fase'] = date('d/m/Y');

            if ($this->Contrato->save($this->data)) {

                $this->addAndamento('Inscrição cadastrada.', array('co_contrato'=>$this->Contrato->getInsertID(),'nu_sequencia'=>1)); 
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $this->Contrato->getInsertID()
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        if ($co_candidato > 0) {
            $this->data = $this->Contrato->Fornecedor->read(null, $co_candidato);
            // $candidato = $this->Contrato->Fornecedor->find( 'first', array('conditions' => " Fornecedor.co_candidato = '" . $co_candidato . "' ") );
            // foreach ($candidato['Fornecedor'] as $chave => $valor) {
            // $this->set($chave, $valor);
            // }
        }

        $this->set('confederacoes', $this->Contrato->Confederacao->find('list', array(
            'conditions' => 'parent_id is null'
        )));

        //$this->set('tipos', $this->Contrato->Contratacao->find('list'));

        //$this->set('modalidades', $this->Contrato->Modalidade->find('list'));

        $this->set('classificacoes', array(
            '1' => 'Primeiro',
            '2' => 'Segundo',
            '3' => 'Terceiro'
        ));

        //$this->set('denominacoes', $this->Contrato->Categoria->find('list'));

        $this->set('racas', array(
            '1' => 'Afro-descendente',
            '2' => 'Indígena',
            '3' => 'Amarelo',
            '4' => 'Negro',
            '5' => 'Branco',
            '6' => 'Pardo'
        ));

        App::import('Model', 'Municipio');
        $municipio = new Municipio();
        $this->set('estados', $municipio->Uf->find('list'));
    }

    /**
     * @resource { "name" : "iframe", "route":"contratos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato, $cadContrato = "edit")
    {
        $this->set(compact('coContrato'));
        $this->set(compact('cadContrato'));
    }

    /**
     * @resource { "name" : "Remover Contrato", "route":"contratos\/delete", "access": "private", "type": "delete" }
     */
    function delete()
    {
        $co_conrato = $this->params['pass'][0];
        if (isset($this->params['pass'][1])) {
            $motivo = $this->params['pass'][1];
        } else {
            $motivo = null;
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);

        $this->autoRender = false;
        $usuario = $this->Session->read('usuario');
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coAdministrador) { // Administrador
            $this->Contrato->validate = array();

            $contrato = $this->Contrato->findByCoContrato($co_conrato);

            $contrato['Contrato']['ic_ativo'] = 0;
            $contrato['Contrato']['ds_motivo'] = $motivo;
            $contrato['Contrato']['dt_cadastro'] = dtDb(substr($contrato['Contrato']['dt_cadastro'], 0, 10));
            unset($contrato['Contrato']['insert_date']);
            if (!$this->Contrato->save($contrato['Contrato'])) {
                $retorno['error'] = true;
                $retorno['msg'] = __('Houve um erro ao desabilitar o contrato...', true);
            }else{
                $retorno['error'] = false;
                $retorno['msg'] = __('Registro desabilitado com sucesso', true);
            }
        } else {
            $retorno['error'] = true;
            $retorno['msg'] = __('Você não tem permissão para excluir este registro', true);
        }

        echo json_encode($retorno);
        exit;
    }

    /**
     * @resource { "name" : "Reativar Contrato", "route":"contratos\/ativar", "access": "private", "type": "update" }
     */
    function ativar()
    {
        $co_conrato = $this->params['form']['co_contrato'];

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);

        $this->autoRender = false;
        $usuario = $this->Session->read('usuario');
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coAdministrador) { // Administrador
            unset($this->Contrato->validate['co_contratante']);
            unset($this->Contrato->validate['st_repactuado']);

            $contrato = $this->Contrato->findByCoContrato($co_conrato);
            $contrato['Contrato']['ic_ativo'] = 1;

            if ($contrato['Contrato']['nu_contrato'] == "" && $contrato['Contrato']['nu_processo'] == "") {
                unset($this->Contrato->validate['nu_processo']);
                unset($this->Contrato->validate['ds_objeto']);
                unset($this->Contrato->validate['co_modalidade']);
            }

            $contrato['Contrato']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $this->Contrato->save($contrato['Contrato']);

            if (count($this->Contrato->validationErrors) == 0) {
                $this->Session->setFlash(__('Registro reabilitado com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));

                die;
            }
        } else {

            $this->Session->setFlash(__('Você não tem permissão para ativar este registro', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function countNuContrato($nuContrato, $coModalidade)
    {
        $contrato = $this->Contrato->find('all', array(
            'conditions' => array(
                'Contrato.nu_contrato' => $nuContrato,
                'Contrato.co_modalidade' => $coModalidade
            )
        ));
        return count($contrato);
    }

    /**
     * @resource { "name" : "Editar Contrato", "route":"contratos\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $cadContrato = null)
    {
        $usuario = $this->Session->read('usuario');
        if ($cadContrato == 'edit') {
            $this->layout = 'iframe';
        }
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            //contrato
            $contratoAtual = $this->Contrato->find('first', array(
                'fields' => array('Contrato.*'),
                'conditions' => array('Contrato.co_contrato' => $this->data['Contrato']['co_contrato'])
            ));
            if (isset($this->data['Contrato']['nu_contrato'])) {
                $this->data['Contrato']['nu_contrato'] = str_replace("/", "", $this->data['Contrato']['nu_contrato']);
                if ($this->data['Contrato']['nu_contrato'] != $this->data['Contrato']['nu_contrato_atual']) {
                    if ($this->countNuContrato($this->data['Contrato']['nu_contrato_atual'], $this->data['Contrato']['co_modalidade']) > 0) {
                        $this->validateNuContrato();
                    }
                } else {
                    if ($this->countNuContrato($this->data['Contrato']['nu_contrato_atual'], $this->data['Contrato']['co_modalidade']) > 1) {
                        $this->validateNuContrato();
                    }
                }
            }
            if (isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") { // Obrigar o preenchimento do Número do Contrato
                $this->setValidacoesContrato();
            }
            if (!isset($this->data['Contrato']['nu_contrato']) && empty($this->data['Contrato']['nu_processo']) && $cadContrato != "np") {
                $this->setValidacoesPam();
            }
            $this->isPermissaoRegistro($this->data['Contrato']);
            $this->prepararCampos();
            $validaValores = true;
            if (!empty($contratoAtual['Contrato']['nu_contrato'])) {
                if (empty($this->data['Contrato']['dt_fim_vigencia']) && empty($this->data['Contrato']['nu_fim_vigencia'])) {
                    $validaValidadores = false;
                    $this->Contrato->invalidate('dt_fim_vigencia_inicios', 'Ambos os campos não podem estar em branco');
                }
            }

            if (isset($this->data['Contrato']['vl_global']) && isset($this->data['Contrato']['vl_inicial']) && $this->data['Contrato']['vl_global'] != '0,00' && !empty($this->data['Contrato']['vl_global'])) {
                if (ln($this->data['Contrato']['vl_inicial']) > ln($this->data['Contrato']['vl_global'])) {
                    $this->Session->setFlash(__('Valor Inicial não pode ser maior que Valor Global.', true));
                    $validaValores = false;
                }
            }
            if (isset($this->data['Contrato']['vl_global']) && isset($this->data['Contrato']['vl_mensal'])) {
                if (ln($this->data['Contrato']['vl_mensal']) > ln($this->data['Contrato']['vl_global'])) {
                    $this->Session->setFlash(__('Valor Mensal não pode ser maior que Valor Global.', true));
                    $validaValores = false;
                }
            }
            if ($cadContrato == "nc") {
                $this->data['Contrato']['dt_cadastro'] = DboSource::expression('CURRENT_TIMESTAMP');
            }else{
                unset($this->Contrato->validate['nu_contrato']);
            }
            if ($cadContrato == "np") {
                $this->data['Contrato']['dt_cadastro_processo'] = DboSource::expression('CURRENT_TIMESTAMP');
                unset( $this->Contrato->validate['nu_contrato'] );
            }

            if($this->data['Contrato']['ic_cadastro_garantia'] != '1') {
                unset($this->Contrato->validate['pc_garantia']);
            }

            if ($this->data['Contrato']['nu_processo'] == $contratoAtual['Contrato']['nu_processo']) {
                unset($this->Contrato->validate['nu_processo']);
            }
            //debug($this->data);exit;
            if ($validaValores && $this->Contrato->save($this->data)) {
                if ($cadContrato == "nc") {
                    if ($this->modulo->isAutoNumeracao()) {
                        $this->data['Contrato']['nu_contrato'] = $this->Contrato->setNextContrato($id);
                    }
                    $this->addAndamento('Contrato cadastrado.', array('model'=>'Contrato'));
                }
                if ($cadContrato == "np") {
                    if ($this->modulo->isAutoNumeracao()) {
                        $this->data['Contrato']['nu_processo'] = $this->Contrato->getNextProcesso();
                    }
                    $this->addAndamento('Processo cadastrado.', array('model'=>'Contrato'));
                }
                if (isset($this->data['Contrato']['co_situacao']) && $this->data['Contrato']['co_situacao'] > 0) {
                    $mensagem = '';
                    $situacaoNova = $this->Contrato->Situacao->read(null, $this->data['Contrato']['co_situacao']);
                    if (isset($this->data['Contrato']['co_situacao_ant']) && $this->data['Contrato']['co_situacao_ant'] > 0) {
                        if ($this->data['Contrato']['co_situacao_ant'] != $this->data['Contrato']['co_situacao']) {
                            $situacaoAntiga = $this->Contrato->Situacao->read(null, $this->data['Contrato']['co_situacao_ant']);
                            $mensagem = __('Situação do Contrato', true) . ' foi alterado de "' . $situacaoAntiga['Situacao']['ds_situacao'] . '" para "' . $situacaoNova['Situacao']['ds_situacao'] . '".';
                        }
                    } else {
                        $mensagem = __('Situação do Contrato', true) . ' foi alterado para "' . $situacaoNova['Situacao']['ds_situacao'] . '".';
                    }

                    if ($mensagem != '') {
                        $this->addAndamento($mensagem, array('model'=>'Contrato'));
                    }
                }
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'detalha', $id, $cadContrato));
            }
        }
        if (empty($this->data)) {

            $this->data = $this->Contrato->read(null, $id);            
            // echo '<pre>';print_r($this->data);exit;
            if ($this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") { // Obrigar o preenchimento do Número do Contrato
                $this->setValidacoesContrato();
            }

            if (isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "" && $cadContrato == "nc") {
                $cadContrato = "";
            }
            if (isset($this->data['Contrato']['nu_processo']) && $this->data['Contrato']['nu_processo'] != "" && $cadContrato == "np") {
                $cadContrato = "";
            }

            if ($this->data['Contrato']['nu_contrato'] == "" && $this->data['Contrato']['nu_processo'] == "" && $cadContrato != "np") {
                $this->setValidacoesPam();
            }

            $this->isPermissaoRegistro($this->data['Contrato']);

            $this->data['Contrato']['ic_cadastro_garantia'] = (int)($this->data['Contrato']['pc_garantia'] > 0) ? "1" : "0";
        }
        $contrato_atual = $this->Contrato->find($id);
        $setor_atual = $contrato_atual['Setor']['co_setor'];
        // $contratantes = $this->Contrato->Contratante->find( 'list' );
        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'or' => array(
                    array('Setor.ic_ativo' => 1),
                    array('Setor.co_setor' => $setor_atual)
                    )
                    )
                ));
                $this->set(compact('contratantes'));
                $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                    'fields' => array(
                        'Fornecedor.co_fornecedor',
                        'Fornecedor.nome_combo'
                        )
                        )
                    );
                    $this->set(compact('fornecedores'));

                    $this->set('modalidades', $this->Contrato->Modalidade->find('list'));
        $this->set('contratacoes', $this->Contrato->Contratacao->find('list'));
        $servicos = $this->Contrato->Servico->find('list');
        $this->set(compact('servicos'));
        if ($this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") {
            $situacaos = $this->Contrato->Situacao->find('list', array(
                'conditions' => array(
                    'Situacao.tp_situacao = "C"',
                    'Situacao.ic_ativo' => 1
                    )
                ));
            } else {
                $situacaos = $this->Contrato->Situacao->find('list', array(
                    'conditions' => array(
                        'Situacao.tp_situacao = "P"',
                    'Situacao.ic_ativo' => 1
                    )
                ));
            }
            $this->set(compact('situacaos'));
            $this->set('categorias', $this->Contrato->Categoria->find('list'));
            if (isset($this->data['Contrato']['co_categoria'])) {
                $this->set('subcategorias', $this->Contrato->Subcategoria->find('list', array(
                    'conditions' => array(
                        'co_categoria' => $this->data['Contrato']['co_categoria']
                        )
                    )));
                }
                if ((isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "") || $cadContrato == "nc") {
                    App::import('Model', 'Perfil');
                    // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.

                    $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
                    $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
                    $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

                    $conditions = array(
                        'Usuario.ic_ativo' => 1
                    );

                    App::import('Model', 'Usuario');
                    $modelUsuario = new Usuario();

                    $this->set('gestores', $modelUsuario->find('list', array(
                        'conditions' => $conditions,
                        'order' => 'ds_nome ASC'
                    )));
                }

                if (isset($this->data['Contrato']['co_setor'])) {
                    $setorDoContrato = $this->Contrato->Setor->find(array('Setor.co_setor' => $this->data['Contrato']['co_setor']));
                } else {
                    $setorDoContrato['Setor'] = null;
                }

                $this->set('usuario', $usuario);
                $this->loadModel('Usuario', 'Perfil');
                $this->set(compact('id'));
                $this->set(compact('cadContrato'));
                if (!empty($this->data['Contrato']['dt_fim_vigencia']) && !empty($this->data['Contrato']['dt_fim_vigencia_inicio'])) {
            if ($this->data['Contrato']['dt_ini_vigencia'] != null && (
                $this->data['Contrato']['dt_fim_vigencia'] != null || $this->data['Contrato']['dt_fim_vigencia_inicio'])
                ) {
                    // TODO: verificar e remover!
                    $data1 = new DateTime($this->dateSql($this->data['Contrato']['dt_ini_vigencia']));
                    $data2 = new DateTime($this->dateSql($this->data['Contrato']['dt_fim_vigencia_inicio']));

                    $interval = $this->Contrato->query("select DATEDIFF(dt_fim_vigencia, dt_ini_vigencia) as diff from contratos where co_contrato ='" . $this->data['Contrato']['co_contrato'] . "'");
                    $this->data['Contrato']['nu_fim_vigencia'] = $interval[0][0]['diff'];
                }
            }

            $clientes = $this->Contrato->Cliente->find('list', array(
                    'fields' => array(
                        'Cliente.co_cliente',
                        'Cliente.no_razao_social'
                    )
                )
            );

            App::import('Model', 'Usuario', 'Perfil');
            // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
            $perfilModel = new Perfil();
            $perfilFiscal = $perfilModel->find('first', array(
                'conditions' => array('Perfil.no_perfil' => 'Fiscal')
            ));

            $usuarioModel = new Usuario();
            $this->usuarioModel->recursive = 2;

            $this->set('fiscais', $usuarioModel->find('list', array(
                'conditions' => array(
                    'Usuario.ic_acesso' => 1,
                    'Usuario.ic_ativo' => 1,
                    'UsuarioPerfil.co_perfil' => $perfilFiscal['Perfil']['co_perfil']
                ),
                'recursive' => 2,
                'order' => array('ds_nome ASC')
            )));

            $this->set(compact('clientes'));

            // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
            $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

            $usuarioModel = new Usuario();

            $this->set('contrato', $this->data['Contrato']);
            $this->set('setor', $setorDoContrato['Setor']);
            $this->set('responsaveis', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_ativo' => 1,
            ),
            'order' => array('ds_nome ASC')
            )));
            
            $this->set('usuarios', $usuarioModel->find('list', array(
                'conditions' => array(
                    'Usuario.ic_acesso' => 0,
                    'Usuario.ic_ativo' => 1
                ),
                'UsuarioPerfil' => array(
                    'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
                ),
                'order' => array('ds_nome ASC')
            )));
    }
    
    function edit_pam($id = null, $cadContrato = null)
    {
        $usuario = $this->Session->read('usuario');
        if ($cadContrato == 'edit_pam') {
            $this->layout = 'iframe';
        }
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            //contrato
            $contratoAtual = $this->Contrato->find('first', array(
                'fields' => array('Contrato.*'),
                'conditions' => array('Contrato.co_contrato' => $this->data['Contrato']['co_contrato'])
            ));
            if (isset($this->data['Contrato']['nu_contrato'])) {
                $this->data['Contrato']['nu_contrato'] = str_replace("/", "", $this->data['Contrato']['nu_contrato']);
                if ($this->data['Contrato']['nu_contrato'] != $this->data['Contrato']['nu_contrato_atual']) {
                    if ($this->countNuContrato($this->data['Contrato']['nu_contrato_atual'], $this->data['Contrato']['co_modalidade']) > 0) {
                        $this->validateNuContrato();
                    }
                } else {
                    if ($this->countNuContrato($this->data['Contrato']['nu_contrato_atual'], $this->data['Contrato']['co_modalidade']) > 1) {
                        $this->validateNuContrato();
                    }
                }
            }
            if (isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") { // Obrigar o preenchimento do Número do Contrato
                $this->setValidacoesContrato();
            }
            if (!isset($this->data['Contrato']['nu_contrato']) && empty($this->data['Contrato']['nu_processo']) && $cadContrato != "np") {
                $this->setValidacoesPam();
            }
            $this->isPermissaoRegistro($this->data['Contrato']);
            $this->prepararCampos();
            $validaValores = true;
            if (!empty($contratoAtual['Contrato']['nu_contrato'])) {
                if (empty($this->data['Contrato']['dt_fim_vigencia']) && empty($this->data['Contrato']['nu_fim_vigencia'])) {
                    $validaValidadores = false;
                    $this->Contrato->invalidate('dt_fim_vigencia_inicios', 'Ambos os campos não podem estar em branco');
                }
            }

            if (isset($this->data['Contrato']['vl_global']) && isset($this->data['Contrato']['vl_inicial']) && $this->data['Contrato']['vl_global'] != '0,00' && !empty($this->data['Contrato']['vl_global'])) {
                if (ln($this->data['Contrato']['vl_inicial']) > ln($this->data['Contrato']['vl_global'])) {
                    $this->Session->setFlash(__('Valor Inicial não pode ser maior que Valor Global.', true));
                    $validaValores = false;
                }
            }
            if (isset($this->data['Contrato']['vl_global']) && isset($this->data['Contrato']['vl_mensal'])) {
                if (ln($this->data['Contrato']['vl_mensal']) > ln($this->data['Contrato']['vl_global'])) {
                    $this->Session->setFlash(__('Valor Mensal não pode ser maior que Valor Global.', true));
                    $validaValores = false;
                }
            }
            if ($cadContrato == "nc") {
                $this->data['Contrato']['dt_cadastro'] = DboSource::expression('CURRENT_TIMESTAMP');
            }else{
                unset($this->Contrato->validate['nu_contrato']);
            }
            if ($cadContrato == "np") {
                $this->data['Contrato']['dt_cadastro_processo'] = DboSource::expression('CURRENT_TIMESTAMP');
                unset( $this->Contrato->validate['nu_contrato'] );
            }

            if($this->data['Contrato']['ic_cadastro_garantia'] != '1') {
                unset($this->Contrato->validate['pc_garantia']);
            }

            if ($this->data['Contrato']['nu_processo'] == $contratoAtual['Contrato']['nu_processo']) {
                unset($this->Contrato->validate['nu_processo']);
            }
            if ($validaValores && $this->Contrato->save($this->data)) {
                if ($cadContrato == "nc") {
                    if ($this->modulo->isAutoNumeracao()) {
                        $this->data['Contrato']['nu_contrato'] = $this->Contrato->setNextContrato($id);
                    }
                    $this->addAndamento('Contrato cadastrado.', array('model'=>'Contrato'));
                }
                if ($cadContrato == "np") {
                    if ($this->modulo->isAutoNumeracao()) {
                        $this->data['Contrato']['nu_processo'] = $this->Contrato->getNextProcesso();
                    }
                    $this->addAndamento('Processo cadastrado.', array('model'=>'Contrato'));
                }
                if (isset($this->data['Contrato']['co_situacao']) && $this->data['Contrato']['co_situacao'] > 0) {
                    $mensagem = '';
                    $situacaoNova = $this->Contrato->Situacao->read(null, $this->data['Contrato']['co_situacao']);
                    if (isset($this->data['Contrato']['co_situacao_ant']) && $this->data['Contrato']['co_situacao_ant'] > 0) {
                        if ($this->data['Contrato']['co_situacao_ant'] != $this->data['Contrato']['co_situacao']) {
                            $situacaoAntiga = $this->Contrato->Situacao->read(null, $this->data['Contrato']['co_situacao_ant']);
                            $mensagem = __('Situação do Contrato', true) . ' foi alterado de "' . $situacaoAntiga['Situacao']['ds_situacao'] . '" para "' . $situacaoNova['Situacao']['ds_situacao'] . '".';
                        }
                    } else {
                        $mensagem = __('Situação do Contrato', true) . ' foi alterado para "' . $situacaoNova['Situacao']['ds_situacao'] . '".';
                    }

                    if ($mensagem != '') {
                        $this->addAndamento($mensagem, array('model'=>'Contrato'));
                    }
                }
                
                if ($this->data['Contrato']['co_fase'] != $this->data['Contrato']['co_fase_old']) {
                    $mensagem = $this->data['Contrato']['ds_andamento'] == '' ? 'Andamento alterado.' : $this->data['Contrato']['ds_andamento'];
                    $this->addAndamento($mensagem, array('model'=>'Contrato', 'nu_sequencia'=>$this->data['Contrato']['co_fase']));
                } else {
                    if ($this->data['Contrato']['ds_andamento'] != '') {
                        $this->addAndamento($this->data['Contrato']['ds_andamento'], array('model'=>'Contrato', 'nu_sequencia'=>$this->data['Contrato']['co_fase']));
                    } else {
                        $this->addAndamento(__('PAM', true) . ' alterada.', array('model'=>'Contrato'));
                    }
                }
                
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'detalha', $id, $cadContrato));
            }
        }
        if (empty($this->data)) {

            $this->data = $this->Contrato->read(null, $id);
            // echo '<pre>';print_r($this->data);exit;
            if ($this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") { // Obrigar o preenchimento do Número do Contrato
                $this->setValidacoesContrato();
            }

            if (isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "" && $cadContrato == "nc") {
                $cadContrato = "";
            }
            if (isset($this->data['Contrato']['nu_processo']) && $this->data['Contrato']['nu_processo'] != "" && $cadContrato == "np") {
                $cadContrato = "";
            }

            if ($this->data['Contrato']['nu_contrato'] == "" && $this->data['Contrato']['nu_processo'] == "" && $cadContrato != "np") {
                $this->setValidacoesPam();
            }

            $this->isPermissaoRegistro($this->data['Contrato']);

            $this->data['Contrato']['ic_cadastro_garantia'] = (int)($this->data['Contrato']['pc_garantia'] > 0) ? "1" : "0";
        }
        $contrato_atual = $this->Contrato->find($id);
        $setor_atual = $contrato_atual['Setor']['co_setor'];
        // $contratantes = $this->Contrato->Contratante->find( 'list' );
        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'or' => array(
                    array('Setor.ic_ativo' => 1),
                    array('Setor.co_setor' => $setor_atual)
                    )
                    )
                ));
                $this->set(compact('contratantes'));
                $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                    'fields' => array(
                        'Fornecedor.co_fornecedor',
                        'Fornecedor.nome_combo'
                        )
                        )
                    );
                    $this->set(compact('fornecedores'));

                    $this->set('modalidades', $this->Contrato->Modalidade->find('list'));
        $this->set('contratacoes', $this->Contrato->Contratacao->find('list'));
        $servicos = $this->Contrato->Servico->find('list');
        $this->set(compact('servicos'));
        if ($this->data['Contrato']['nu_contrato'] != "" || $cadContrato == "nc") {
            $situacaos = $this->Contrato->Situacao->find('list', array(
                'conditions' => array(
                    'Situacao.tp_situacao = "C"',
                    'Situacao.ic_ativo' => 1
                    )
                ));
            } else {
                $situacaos = $this->Contrato->Situacao->find('list', array(
                    'conditions' => array(
                        'Situacao.tp_situacao = "P"',
                    'Situacao.ic_ativo' => 1
                    )
                ));
            }
            $this->set(compact('situacaos'));
            $this->set('categorias', $this->Contrato->Categoria->find('list'));
            if (isset($this->data['Contrato']['co_categoria'])) {
                $this->set('subcategorias', $this->Contrato->Subcategoria->find('list', array(
                    'conditions' => array(
                        'co_categoria' => $this->data['Contrato']['co_categoria']
                        )
                    )));
                }
                if ((isset($this->data['Contrato']['nu_contrato']) && $this->data['Contrato']['nu_contrato'] != "") || $cadContrato == "nc") {
                    App::import('Model', 'Perfil');
                    // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.

                    $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
                    $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
                    $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

                    $conditions = array(
                        'Usuario.ic_ativo' => 1
                    );

                    App::import('Model', 'Usuario');
                    $modelUsuario = new Usuario();

                    $this->set('gestores', $modelUsuario->find('list', array(
                        'conditions' => $conditions,
                        'order' => 'ds_nome ASC'
                    )));
                }

                if (isset($this->data['Contrato']['co_setor'])) {
                    $setorDoContrato = $this->Contrato->Setor->find(array('Setor.co_setor' => $this->data['Contrato']['co_setor']));
                } else {
                    $setorDoContrato['Setor'] = null;
                }

                $this->set('usuario', $usuario);
                $this->loadModel('Usuario', 'Perfil');
                $this->set(compact('id'));
                $this->set(compact('cadContrato'));
                if (!empty($this->data['Contrato']['dt_fim_vigencia']) && !empty($this->data['Contrato']['dt_fim_vigencia_inicio'])) {
            if ($this->data['Contrato']['dt_ini_vigencia'] != null && (
                $this->data['Contrato']['dt_fim_vigencia'] != null || $this->data['Contrato']['dt_fim_vigencia_inicio'])
                ) {
                    // TODO: verificar e remover!
                    $data1 = new DateTime($this->dateSql($this->data['Contrato']['dt_ini_vigencia']));
                    $data2 = new DateTime($this->dateSql($this->data['Contrato']['dt_fim_vigencia_inicio']));

                    $interval = $this->Contrato->query("select DATEDIFF(dt_fim_vigencia, dt_ini_vigencia) as diff from contratos where co_contrato ='" . $this->data['Contrato']['co_contrato'] . "'");
                    $this->data['Contrato']['nu_fim_vigencia'] = $interval[0][0]['diff'];
                }
            }

            $clientes = $this->Contrato->Cliente->find('list', array(
                    'fields' => array(
                        'Cliente.co_cliente',
                        'Cliente.no_razao_social'
                    )
                )
            );

            App::import('Model', 'Usuario', 'Perfil');
            // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
            $perfilModel = new Perfil();
            $perfilFiscal = $perfilModel->find('first', array(
                'conditions' => array('Perfil.no_perfil' => 'Fiscal')
            ));

            $usuarioModel = new Usuario();
            $this->usuarioModel->recursive = 2;

            $this->set('fiscais', $usuarioModel->find('list', array(
                'conditions' => array(
                    'Usuario.ic_acesso' => 1,
                    'Usuario.ic_ativo' => 1,
                    'UsuarioPerfil.co_perfil' => $perfilFiscal['Perfil']['co_perfil']
                ),
                'recursive' => 2,
                'order' => array('ds_nome ASC')
            )));

            $this->set(compact('clientes'));

            // Somente o gestor pode ser o solicitante do contrato, segundo a lei 8666.
            $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

            $usuarioModel = new Usuario();

            $this->set('contrato', $this->data['Contrato']);
            $this->set('setor', $setorDoContrato['Setor']);
            $this->set('responsaveis', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_ativo' => 1,
            ),
            'order' => array('ds_nome ASC')
        )));
            
        
        $fases  = $this->Contrato->Fase->find('list', array(
                'fields' => array(
                    'Fase.co_fase',
                    'Fase.ds_fase'
                )
            )
        );
        $this->set(compact('fases'));
        
        App::import('Model', 'Usuario');
        $usuarioModel = new Usuario();

        $this->set('usuarios', $usuarioModel->find('list', array(
            'conditions' => array(
                'Usuario.ic_acesso' => 0,
                'Usuario.ic_ativo' => 1
            ),
            'UsuarioPerfil' => array(
                'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'order' => array('ds_nome ASC')
        )));
        
        $this->set('gestores', $usuarioModel->find('list', array(
            'conditions' => array(
              'Usuario.ic_ativo' => 1,
              'Usuario.ic_acesso' => 1,
              //'UsuarioPerfil.co_perfil' => $gestor['Perfil']['co_perfil']
            ),
            'recursive' => 2,
            'order' => 'ds_nome ASC'
        )));
    }

    /**
     * @resource { "name" : "atualizar prazo do processo", "route":"contratos\/upd_prazo", "access": "private", "type": "update" }
     */
    function upd_prazo($id = null)
    {
        if (!empty($this->data["dt_prazo_processo"]) && $this->data["dt_prazo_processo"] != "") {
            $this->Contrato->id = $id;
            $this->Contrato->saveField("dt_prazo_processo", dtDb($this->data["dt_prazo_processo"]));
            return false;
        }
    }

    public function validateNuContrato()
    {
        $this->Contrato->validate['nu_contrato']['isUnique'] = array(
            'rule' => 'isUnique',
            'message' => 'Este número de Contrato já encontra-se cadastrado.'
        );
    }

    function setValidacoesContrato()
    {
        if (!$this->modulo->isCampoObrigatorio('nu_processo')) {
            unset($this->Contrato->validate['nu_processo']);
        }
        if (!$this->modulo->isCampoObrigatorio('co_contratante')) {
            unset($this->Contrato->validate['co_contratante']);
        }
        if (!$this->modulo->isCampoObrigatorio('st_repactuado')) {
            unset($this->Contrato->validate['st_repactuado']);
        }
        if ($this->modulo->isContratoUnique()) {
            $this->validateNuContrato();
        }
        $this->Contrato->validate['nu_contrato']['numeric'] = array(
            'rule' => array(
                'notEmpty'
            ),
            'message' => 'Campo Contrato em branco'
        );
        $this->Contrato->validate['co_fornecedor'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo Fornecedor em branco'
            )
        );
        if ($this->modulo->isCampoObrigatorio('ds_fundamento_legal')) {
            $this->Contrato->validate['ds_fundamento_legal'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Campo Fundamento Legal em branco'
                )
            );
        }
        // $this->Contrato->validate['co_situacao'] = array(
        // 'numeric' => array(
        // 'rule' => array(
        // 'notEmpty'
        // ),
        // 'message' => 'Campo SituaÃ§Ã£o em branco'
        // )
        // );
//        if ($this->modulo->isCampoObrigatorio('co_situacao')) :
//            $this->Contrato->validate['co_situacao'] = array(
//                'numeric' => array(
//                    'rule' => array(
//                        'notEmpty'
//                    ),
//                    'message' => 'Campo Situação em branco'
//                )
//            );
//        endif;

        /*   $this->Contrato->validate['co_gestor_atual'] = array(
               'numeric' => array(
                   'rule' => array(
                       'notEmpty'
                   ),
                   'message' => 'Campo Gestor do Contrato em branco'
               )
           );*/

//        $dt_ini_vigencia = '';
//        if ($this->data['Contrato']['dt_ini_vigencia'] == '') {
//            $dt_ini_vigencia = 'Data de ' . __('Início da Vigência', true) . ' em branco';
//        } else {
//            $dt_ini_vigencia = 'Data de ' . __('Início da Vigência', true) . ' inválida';
//        }
//
//        $this->Contrato->validate['dt_ini_vigencia'] = array(
//            'date' => array(
//                'rule' => array(
//                    'date'
//                ),
//                'message' => $dt_ini_vigencia
//            )
//        );
//
//        $dt_fim_vigencia = '';
//        if ($this->data['Contrato']['dt_fim_vigencia'] == '') {
//            $dt_fim_vigencia = __('Fim da Vigência', true) . 'em branco.';
//        } else {
//            $dt_fim_vigencia = __('Fim da Vigência', true) . ' deve ser maior que ' . __('Início da Vigência', true) . '.';
//        }
//
//        $this->Contrato->validate['dt_fim_vigencia'] = array(
//            'rule' => array(
//                'validaDtFimContrato',
//                true
//            ),
//            'message' => $dt_fim_vigencia
//        );

        $dt_assinatura = '';
        if ($this->data['Contrato']['dt_assinatura'] == '') {
            $dt_assinatura = 'Data de assinatura em branco';
        } else {
            $dt_assinatura = 'A Data de Assinatura deve ser menor que a Data de Início da Vigência.';
        }
        if ($this->modulo->isCampoObrigatorio('dt_assinatura')) {
            $this->Contrato->validate['dt_assinatura'] = array(
                'rule' => array(
                    'validaAssinaturaContrato',
                    true
                ),
                'message' => $dt_assinatura
            );
        }
        $this->Contrato->validate['vl_global'] = array(
            'money' => array(
                'rule' => array(
                    'comparison',
                    '>',
                    0
                ),
                'message' => 'Campo Valor Global em branco'
            )
        );
    }

    function setValidacoesPam()
    {
        $this->Contrato->validate = null;
        $this->Contrato->validate['nu_pam'] = array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Este número de ' . __('PAM', true) . ' já encontra-se cadastrado.'
            ),
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo ' . __('PAM', true) . ' em branco'
            ),
            'alfanum' => array(
                // 'alfanum' =>co_modalidadeco_modalidade array(
                'rule' => array('custom', '/.*[0-9].*/i'),
                'message' => 'O nº do ' . __('PAM', true) . ' precisa conter pelo menos um número'
            )
        );

        $this->Contrato->validate['co_contratante'] = array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Unidade Solicitante em branco'
            )
        );

        if ($this->modulo->isCampoObrigatorio('ic_ata_vigente')) {
            $this->Contrato->validate['ic_ata_vigente'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Campo Possui Ata Vigente em branco'
                )
            );
        }

        $this->Contrato->validate['ds_observacao'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo Observação em branco'
            )
        );
    }

    function setValidacoesInscricao()
    {
        $this->Contrato->validate = array();
        $this->Contrato->Fornecedor->validate = array();
    }

    /**
     * Função para determinar se o usuário possui permissão para acessar o registro
     */
    function isPermissaoRegistro($contrato)
    {
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        App::import('Model', 'ContratoFiscal');
        $contratoFiscal = new ContratoFiscal();
        $coFiscais = $contratoFiscal->findAllFiscaisByContrato($contrato['co_contrato']);

        $permissao = false;
        $inContrato = false;

        $usuario = $this->Session->read('usuario');

        if ($usuario['Usuario']['licenca_nominal'] == '1') {

            $permissao = true;

        } else {

            foreach ($coFiscais as $cf) {
                if ($usuario['Usuario']['co_usuario'] == $cf['ContratoFiscal']['co_usuario_fiscal']) {
                    $inContrato = true;
                    if ($cf['ContratoFiscal']['dt_fim'] != null &&
                        new DateTime($this->dateSql($cf['ContratoFiscal']['dt_fim'])) < new DateTime('now')
                    ) {
                        $inContrato = false;
                    }
                }
            }

            // SE O USUARIO FOR FISCAL OU GESTOR
            if ($inContrato || $contrato["co_fiscal_atual"] == $usuario['Usuario']['co_usuario'] || $contrato['co_gestor_atual'] == $usuario['Usuario']['co_usuario']) {
                    $permissao = true;
            }

            // SE O USUÁRIO FOR DA UNIDADE ADMINISTRATIVA E ADMINISTRADOR
            if ($usuario['Setor']['uasg'] == $contrato['uasg'] && $usuario['UsuarioPerfil']['co_perfil'] == '1') {
                $permissao = true;
            }

            // SE O USUÁRIO FOR DA UNIDADE ADMINISTRATIVA E ADMINISTRADOR
            if ($usuario['Setor']['uasg'] == $contrato['uasg'] && $usuario['UsuarioPerfil']['co_perfil'] == '1') {
                $permissao = true;
            }

        }

        /*           foreach($coFiscais as $cf){
                    if($usuario['Usuario']['co_usuario'] == $cf['ContratoFiscal']['co_usuario_fiscal']){
                        $inContrato = true;
                        if($cf['ContratoFiscal']['dt_fim'] != null &&
                            new DateTime($this->dateSql($cf['ContratoFiscal']['dt_fim'])) < new DateTime('now')){
                            $inContrato = false;
                        }
                    }
                }

                if ($usuario['UsuarioPerfil']['co_perfil'] == $coGestor ) { // Gestor
                    $isSetor = false;
                    if( $contrato['co_contratante'] == $usuario['Usuario']['co_setor'] ) {
                        $isSetor = true;
                    }
                    if( !$isSetor && (empty($contrato['co_gestor_atual']) || $contrato['co_gestor_atual'] != $usuario['Usuario']['co_usuario']) ){
                        $permissao = false;
                    }
                }
                if ($usuario['UsuarioPerfil']['co_perfil'] == $coChefeDepartamento && (empty($contrato['co_contratante']) || $contrato['co_contratante'] != $usuario['Usuario']['co_setor'])) { // Chefe de Dpto
                    $permissao = false;
                }

     if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal && !$inContrato) { // Fiscal
//        if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal && (empty($contrato['co_fiscal_atual']) || $contrato['co_fiscal_atual'] != $usuario['Usuario']['co_usuario'])) { // Fiscal
            $permissao = false;
        }*/


        if (!$permissao) {
            $this->Session->setFlash(__('Usuário sem permissão para acessar o registro solicitado.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
     * @resource { "name" : "detalha contrato", "route":"contratos\/detalha", "access": "private", "type": "select" }
     */
    public function detalha($id = null, $cadContrato = null)
    {
        if ($cadContrato == 'detalha' || $cadContrato == 'edit' || $cadContrato == 'edit_pam') {
            $this->layout = 'iframe';
        }

        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if (!empty($this->data)) {
            $this->prepararCampos();
            if ($this->Contrato->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->Contrato->recursive = 2;
            $this->data = $this->Contrato->read(null, $id);
        }

        $this->isPermissaoRegistro($this->data['Contrato']);

        $this->set(compact('id'));

        if (!empty($this->data['Contrato']['dt_fim_processo'])) {
            $dataProcesso = explode("/", $this->data['Contrato']['dt_fim_processo']);
            $dataFimProcesso = $dataProcesso[0] . '-' . $dataProcesso[1] . '-' . $dataProcesso[2];
            $this->data['Contrato']['dt_fim_garantia'] = date('d/m/Y', strtotime("+90 days", strtotime($dataFimProcesso)));
        }

        if (!empty($this->data['Contrato']['nu_sequencia'])) {
            $andamento = $this->Contrato->Andamento->find('first', array(
                'conditions' => array(
                    'Andamento.co_contrato' => $this->data['Contrato']['co_contrato'],
                    'Andamento.dt_fim is null',
                    'OR' => array(
                        'Andamento.nu_sequencia' => $this->data['Contrato']['nu_sequencia'],
                        'Andamento.co_setor' => $this->data['Contrato']['co_setor']
                    )
                )
            ));

            $this->data['Andamento']['dt_andamento'] = $andamento['Andamento']['dt_andamento'];
            $this->data['Andamento']['dt_fim'] = $andamento['Andamento']['dt_fim'];
            $this->data['Andamento']['ds_fase_atual'] = $andamento['Andamento']['ds_andamento'];
            $this->data['Andamento']['ds_justificativa_fluxo'] = $andamento['Andamento']['ds_justificativa'];
        }

        $mensagem = '';
        $opcoesFluxo = array();
        if ($this->data['Fluxo']['nu_sequencia_um'] > 0) {

            $this->set('setoresFluxo', $this->Contrato->Setor->find('list', array(
                'conditions' => 'Setor.co_setor in (select distinct co_setor from fluxos)'
            )));

            $fluxoProximo = null;
            $id_mudar = $this->data['Fluxo']['nu_sequencia_um'];
            $id_mudar2 = $this->data['Fluxo']['nu_sequencia_dois'];
            if ($this->data['Contrato']['is_desvio_fluxo'] != 1 && $this->valida_mudanca_fase($this->data['Contrato']['co_contrato'], $fluxoProximo, $id_mudar, $mensagem, $id_mudar2)) {
                $opcoesFluxo['C'] = 'Concluir Fase &nbsp;&nbsp;<i class="icon-share-alt green"></i>&nbsp; ' . $fluxoProximo['Setor']['ds_setor'] . ' / ' . $fluxoProximo['Fase']['ds_fase'];
            }

            App::import('Model', 'Fluxo');
            $fluxo = new Fluxo();

            $fluxoAnterior = $fluxo->find(array('nu_sequencia' => $this->data['FaseAnterior']['co_fase']));

            $opcoesFluxo['D'] = 'Desviar Fluxo';

            if ($this->data['Contrato']['is_desvio_fluxo'] == 1) {
                $opcoesFluxo = null;
                $opcoesFluxo = array(
                    'F' => 'Retornar ao fluxo &nbsp;<i class="icon-share-alt red"></i>&nbsp; ' . $this->Contrato->Setor->field('ds_setor', array(
                            'co_setor' => $this->data['Fluxo']['co_setor']
                        )) . ' / ' . $this->Contrato->Fase->field('ds_fase', array(
                            'co_fase' => $this->data['Fluxo']['co_fase']
                        ))
                );
            }
        }

        App::import('Model', 'ContratoFiscal');
        $contratoFiscal = new ContratoFiscal();
        $fiscais = $contratoFiscal->findAllFiscaisByContrato($id);

        if ($this->data['Contrato']['co_contrato_pai'] != null) {
            $contratoPai = $this->Contrato->find(array('co_contrato' => $this->data['Contrato']['co_contrato_pai']));
            $this->set('contratoPai', $contratoPai);
        }

        if (count($fiscais) != 0) {
            $this->set('fiscais', $fiscais);
        }

        $this->loadModel('GrupoAuxiliar');
        if (isset($this->data['Contrato']['co_responsavel'])) {
            if (!empty($this->data['Contrato']['co_responsavel']) || $this->data['Contrato']['co_responsavel'] != null) {
                $responsavel = $this->GrupoAuxiliar->findByCoUsuario($this->data['Contrato']['co_responsavel']);
            }
        }

        $this->loadModel('AssinaturasRequisitante');
        $assRequisitante = new AssinaturasRequisitante();
        $assinaturas = $assRequisitante->findByCoContrato($this->data['Contrato']['co_contrato']);
        if (isset($assinaturas['AssinaturasRequisitante'])) {
            $this->set('assRequisitante', $assinaturas);
        }

        $this->loadModel('Usuario');
        $usuario = new Usuario();
        if (!empty($this->data['Contrato']['co_assinatura_chefe'])) {
            $this->set('assChefe', $usuario->findByCoUsuario($this->data['Contrato']['co_assinatura_chefe']));
            $this->set('dtAssChefe', $this->data['Contrato']['dt_assinatura_chefe']);
        }
        if (!empty($this->data['Contrato']['co_assinatura_ordenador'])) {
            $this->set('assOrdenador', $usuario->findByCoUsuario($this->data['Contrato']['co_assinatura_ordenador']));
            $this->set('dtAssOrdenador', $this->data['Contrato']['dt_assinatura_ordenador']);
        }

        if (isset($responsavel)) {
            $this->set('responsavelSolicitante', $responsavel['GrupoAuxiliar']['ds_nome']);
        }

        if (!empty($this->data['Contrato']['co_parceiro'])) {
            $this->set('parceiro', $usuario->findByCoUsuario($this->data['Contrato']['co_parceiro']));
        }

        $this->set('alertaProximaFase', $mensagem);
        $this->set('opcoesFluxo', $opcoesFluxo);

        $this->set('fiscalAtual', $this->data['FiscalAtual']);
        $this->set('gestorAtual', $this->data['GestorAtual']);
        $this->set('contrato', $this->data['Contrato']);
        $this->set('contratacao', $this->data['Contratacao']);
        $this->set('cadContrato', $cadContrato);

        $this->Session->write('hasPenalidade',
            (isset($this->data['Penalidade'][0]['ic_situacao'])
                && $this->data['Penalidade'][0]['ic_situacao'] == 2) ? true : false);

        if ($this->modulo->isInscricao()) {
            // $this->set( 'tipo_inscricao', array('1' => 'Nacional', '2' => 'Internacional') );
            $this->set('classificacoes', array(
                '1' => 'Primeiro',
                '2' => 'Segundo',
                '3' => 'Terceiro'
            ));
        }
        
        $fases  = $this->Contrato->Fase->find('list', array(
                'fields' => array(
                    'Fase.co_fase',
                    'Fase.ds_fase'
                )
            )
        );
        $this->set(compact('fases'));
    }

    private function valida_mudanca_fase($coContrato, &$fluxoProximo, &$id_mudar, &$mensagem, $id_mudar2 = null)
    {
        App::import('Model', 'Fluxo');
        $fluxo = new Fluxo();

        $fluxoProximo = $fluxo->read(null, $id_mudar);

        if ($fluxoProximo['Fluxo']['tp_operador'] > 0) { // VERIFICA OPERADOR

            $this->Contrato->recursive = -1;
            $contrato = $this->Contrato->read($this->tpOperadores[$fluxoProximo['Fluxo']['tp_operador']]['coluna'], $coContrato);
            if ($contrato['Contrato'][$this->tpOperadores[$fluxoProximo['Fluxo']['tp_operador']]['coluna']] == "S") { // nu_sequencia_um
                $id_mudar = $id_mudar;
            } elseif ($contrato['Contrato'][$this->tpOperadores[$fluxoProximo['Fluxo']['tp_operador']]['coluna']] == "N") { // nu_sequencia_dois
                $id_mudar = $id_mudar2;
            } else {
                $mensagem = 'Para concluir a Fase clique em Editar e informe o campo ' . $this->tpOperadores[$fluxoProximo['Fluxo']['tp_operador']]['nome'] . ' !';
                return false;
            }

            $fluxoProximo = $fluxo->read(null, $id_mudar);
        }

        return true;
    }

    /**
     * @resource { "name" : "mudar fase no fluxo", "route":"contratos\/mudar_fase", "access": "private", "type": "update" }
     */
    function mudar_fase()
    {
        if (Configure::read('App.config.resource.certificadoDigital') && isset($this->data['tramitar_sem_certificado']) && $this->data['tramitar_sem_certificado'] != "1") {
            $token = $this->data['token'];
            $auth = $this->Util->getRestPkiClient()->getAuthentication();
            $vr = $auth->completeWithWebPki($token);
            if ($vr->isValid()) {
                $userCert = $auth->getCertificate();
                $validation = true;
            } else {
                $validation = false;
            }
        } else {
            $validation = true;
        }

        $id_mudar = 0;
        
        if ($validation) {
            
            switch ($this->data['tp_mudanca']) {
                case 'D':
                    $this->Contrato->updateAll( // ATUALIZA O CONTRATO
                        array(
                            'Contrato.is_desvio_fluxo' => 1,
                            'Contrato.co_fase' => $this->data['co_fase_mudar'],
                            'Contrato.dt_fase' => "'" . dtDb(date('d/m/Y')) . "'",
                            'Contrato.co_setor' => $this->data['co_setor_mudar']
                        ), array(
                        'Contrato.co_contrato' => $this->data['co_contrato']
                    ));

                    $this->Contrato->Andamento->updateAll( // ENCERRAR A FASE ANTERIOR
                        array(
                            'Andamento.dt_fim' => "'" . dtDb(date('d/m/Y H:i:s')) . "'"
                        ), array(
                        'Andamento.co_contrato' => $this->data['co_contrato'],
                        'Andamento.nu_sequencia' => $this->data['nu_sequencia'],
                        'Andamento.dt_fim is null'
                    ));

                    $this->addAndamento($this->data['obs_fase'], 
                            array(
                                'co_setor'=>$this->data['co_setor_mudar'],
                                'co_fase'=>$this->data['co_fase_mudar'],
                                'justificativa'=>$this->data['jus_fase'])); // CADASTRAR ANDAMENTO
                    break;
                
                case 'F':
                    $id_mudar = $this->data['nu_sequencia'];
                    $fluxoRetorno = null;
                    $mensagem = '';
                    $this->valida_mudanca_fase($this->data['co_contrato'], $fluxoRetorno, $id_mudar, $mensagem);
                    $this->Contrato->updateAll( // ATUALIZA O CONTRATO
                        array(
                            'Contrato.is_desvio_fluxo' => 0,
                            'Contrato.co_fase' => $fluxoRetorno['Fluxo']['co_fase'],
                            'Contrato.dt_fase' => "'" . dtDb(date('d/m/Y')) . "'",
                            'Contrato.co_setor' => $fluxoRetorno['Fluxo']['co_setor']
                        ), array(
                        'Contrato.co_contrato' => $this->data['co_contrato']
                    ));

                    $this->Contrato->Andamento->updateAll( // ENCERRAR A FASE ANTERIOR
                        array(
                            'Andamento.dt_fim' => "'" . dtDb(date('d/m/Y H:i:s')) . "'"
                        ), array(
                        'Andamento.co_contrato' => $this->data['co_contrato'],
                        'Andamento.co_setor' => $this->data['co_setor_atual'],
                        'Andamento.co_fase' => $this->data['co_fase_atual'],
                        'Andamento.dt_fim is null'
                    ));

                    $this->addAndamento($this->data['obs_fase'], array('nu_sequencia'=>$id_mudar)); // CADASTRAR ANDAMENTO
                    break;
                
                case 'R' :
                    $id_mudar = $this->data['nu_sequencia_dois'];
                case 'P' :
                    $id_mudar = $this->data['co_fase_mudar'];
                default:
                    
                    $id_mudar == 0 ? $this->data['nu_sequencia_um'] : $id_mudar;
                    
                    $fluxoProximo = null;
                    $mensagem = null;

                    if (!$this->valida_mudanca_fase($this->data['co_contrato'], $fluxoProximo, $id_mudar, $mensagem)) {
                        $this->Session->setFlash($mensagem);
                        $this->redirect(array(
                            'action' => 'detalha',
                            $this->data['co_contrato']
                        ));
                    }

                    $this->Contrato->updateAll( // ATUALIZA O CONTRATO
                        array(
                            'Contrato.nu_sequencia' => $fluxoProximo['Fluxo']['nu_sequencia'],
                            'Contrato.co_fase' => $fluxoProximo['Fluxo']['co_fase'],
                            'Contrato.dt_fase' => "'" . dtDb(date('d/m/Y')) . "'",
                            'Contrato.co_setor' => $fluxoProximo['Fluxo']['co_setor']
                        ), array(
                        'Contrato.co_contrato' => $this->data['co_contrato']
                    ));

                    $this->Contrato->Andamento->updateAll( // ENCERRAR A FASE ANTERIOR
                        array(
                            'Andamento.dt_fim' => "'" . dtDb(date('d/m/Y H:i:s')) . "'"
                        ), array(
                        'Andamento.co_contrato' => $this->data['co_contrato'],
                        'Andamento.nu_sequencia' => $this->data['nu_sequencia'],
                        'Andamento.dt_fim is null'
                    ));

                    $this->addAndamento($this->data['obs_fase'], array('nu_sequencia'=>$id_mudar)); // CADASTRAR ANDAMENTO
                    break;
            }

            //$this->avisarSetorFluxo($this->data['co_contrato']);
            $this->Session->setFlash(__('Fase alterada com sucesso.', true));
            $this->redirect(array(
                'action' => 'detalha',
                $this->data['co_contrato']
            ));
        } else {
            $this->Session->setFlash(__('Erro na validação do Certificado Digital', true));
            $this->redirect(array(
                'action' => 'detalha',
                $this->data['co_contrato']
            ));
        }
    }

    /**
     * @resource { "name" : "Contratos Dashboard", "route":"contratos\/list_contratos", "access": "private", "type": "select" }
     */
    function list_contratos($filtro = '', $papel = '')
    {
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $usuario = $this->Session->read('usuario');

        App::import('Model', 'ContratoFiscal');
        $contratoFiscal = new ContratoFiscal();
        $coFiscais = $contratoFiscal->findAllByUsuario($usuario['Usuario']['co_usuario']);

        $criterio = array();

        if ($usuario['Usuario']['licenca_nominal'] == '1') {
            $criterio['conditions'] = array(
                "Contrato.nu_contrato != ''",
                "Contrato.ic_ativo = 1"
            );
        } else {
            $criterio['conditions'] = array(
                "Contrato.nu_contrato != ''",
                "Contrato.ic_ativo = 1",
                "Contrato.uasg = " . $usuario['Setor']['uasg']
            );
        }

        if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal) { // Fiscal
            App::import('Model', 'ContratoFiscal');
            $contratoFiscal = new ContratoFiscal();
            $arrayContratos = $contratoFiscal->findAllByUsuario($usuario['Usuario']['co_usuario']);
            $criterio['conditions']['co_contrato'] = $arrayContratos; // passaar array com contratos do fiscal
        }

        if ($usuario['UsuarioPerfil']['co_perfil'] == $coGestor) { // Gestor
            $criterio['conditions']['OR'] = array(
                'Contrato.co_gestor_atual' => $usuario['Usuario']['co_usuario'],
                'Contrato.co_contratante' => $usuario['Usuario']['co_setor'],
            );
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor) { // Setor
            $criterio['conditions'] = array(
                'Contrato.co_contratante = ' . $usuario['Usuario']['co_setor']
            );
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coChefeDepartamento) { // Chefe de Departamento
            $criterio['conditions']['co_contratante'] = $usuario['Usuario']['co_setor'];
        }

        switch ($filtro) {
            case 'pa':
                $criterio['joins'] = array(
                    array(
                        'table' => 'pagamentos',
                        'alias' => 'Pagamento',
                        'conditions' => array(
                            'Pagamento.co_contrato = Contrato.co_contrato'
                        )
                    )
                );
                $criterio['group'] = array(
                    'Contrato.co_contrato'
                );
                $criterio['conditions'][] = ' Pagamento.dt_vencimento < NOW() and Pagamento.dt_pagamento is null ';
                break;
            case 'pd':
                $criterio['joins'] = array(
                    array(
                        'table' => 'pendencias',
                        'alias' => 'Pendencia',
                        'conditions' => array(
                            'Pendencia.co_contrato = Contrato.co_contrato'
                        )
                    )
                );
                $criterio['group'] = array(
                    'Contrato.co_contrato'
                );
                $criterio['conditions']['Pendencia.st_pendencia'] = 'P';
                break;
            case 'v30':
                $criterio['conditions'][] = "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) >= 0 and " . "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) <= 30 ";
                break;
            case 'v60':
                $criterio['conditions'][] = "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > 30 and " . "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) <= 60 ";
                break;
            case 'v90':
                $criterio['conditions'][] = "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > 60 and " . "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) <= 90 ";
                break;
            case 'v120':
                $criterio['conditions'][] = "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > 90 and " . "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) <= 120 ";
                break;
            case 'v180':
                $criterio['conditions'][] = "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > 120 and " . "DATEDIFF(Contrato.dt_fim_vigencia, current_date()) <= 180 ";
                break;
        }

        switch ($papel) {
            case 'gestor':
                $criterio['conditions'] = array(
                    'OR' => array(
                        'Contrato.co_gestor_atual' => $usuario['Usuario']['co_usuario'],
                        'Contrato.co_gestor_suplente' => $usuario['Usuario']['co_usuario'],
                    )
                );
                break;

            case 'fiscal':
                foreach ($coFiscais as $fiscal) {
                    $listContratosFiscais['or'][] = array(
                        'Contrato.co_contrato' => $fiscal
                    );
                }

                $criteria['or'] = array(
                    $listContratosFiscais
                );
                break;
        }

        $criterio['limit'] = 20;
        $this->set('contratos', $this->Contrato->find('all', $criterio));
    }

    /**
     * @resource { "name" : "Processos Dashboard", "route":"contratos\/list_processos", "access": "private", "type": "select" }
     */
    function list_processos($tipoExibicao = 'all')
    {
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $conditions = array(
            " (Contrato.dt_fim_processo > now() or Contrato.dt_fim_processo is null) ",
            " (Contrato.nu_contrato = '' or Contrato.nu_contrato is null) "
        );
        $usuario = $this->Session->read('usuario');

        if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor) { // Setor
            $conditions[] = 'Contrato.co_setor = ' . $usuario['Usuario']['co_setor'];
        }

        if ($tipoExibicao == 'list_fase') {
            $conditions[] = "Contrato.co_situacao > 0 and Situacao.tp_situacao = 'P' ";
            $this->set('situacoes', $this->Contrato->Situacao->find('all', array(
                'conditions' => array(
                    " Situacao.tp_situacao = 'P' ",
                    'Situacao.ic_ativo' => 1
                )
            )));
        }

        $this->Contrato->hasMany = array(
            'Andamento' => array(
                'className' => 'Andamento',
                'foreignKey' => 'co_contrato',
                'conditions' => 'Andamento.dt_fim is null'
            ),
            'Pendencia' => array(
                'className' => 'Pendencia',
                'foreignKey' => 'co_contrato'
            ),
            'Historico' => array(
                'className' => 'Historico',
                'foreignKey' => 'co_contrato'
            ),
            'Atividade' => array(
                'className' => 'Atividade',
                'foreignKey' => 'co_contrato'
            )
        );

        $this->set('contratos', $this->Contrato->find('all', array(
            'conditions' => $conditions,
            'recursive' => 1
        )));
        $this->set(compact('tipoExibicao'));

        if ($tipoExibicao == 'list_fase') { // Listar Processos por Fase
            $this->render('list_fase');
        }
    }

    /**
     * @resource { "name" : "iframe imprimir", "route":"contratos\/iframe_imprimir", "access": "private", "type": "select" }
     */
    function iframe_imprimir($coContrato, $modulos, $name)
    {
        $this->set(compact('coContrato'));
        $this->set(compact('modulos'));
        $this->set(compact('name'));
    }

    /**
     * @resource { "name" : "Imprimir", "route":"contratos\/imprimir", "access": "private", "type": "select" }
     */
    function imprimir($coContrato, $modulos, $name)
    {
        App::import('Helper', 'Print');
        
        $print = new PrintHelper();
        
        $check_modulos = str_split($modulos);

        $contrato = $this->Contrato->read(null, $coContrato);
        
        $funcao   = 'imprimirContrato';

        $titulo = '';
        if ($contrato['Contrato']['nu_contrato'] != "") {
            if ($contrato['Contrato']['ic_ativo'] == 0) {
                $titulo .= 'CONTRATO: ' . $print->contrato($contrato['Contrato']['nu_contrato']);
                if ($contrato['Contrato']['nu_processo'] != "") {
                    $titulo .= ' - ';
                }
            } else {
                $titulo .= 'CONTRATO: ' . $print->contrato($contrato['Contrato']['nu_contrato']);
                if ($contrato['Contrato']['nu_processo'] != "") {
                    $titulo .= ' - ';
                }
            }
        }
        if ($contrato['Contrato']['nu_processo'] != "") {
            $titulo .= 'PROCESSO: ' . $print->processo($contrato['Contrato']['nu_processo']);
        }
        if ($contrato['Contrato']['nu_processo'] == "" && $contrato['Contrato']['nu_contrato'] == "" && $contrato['Contrato']['nu_pam'] != "") {
            $titulo .= __('PAM', true) . ': ' . $print->pam($contrato['Contrato']['nu_pam']);
            $funcao   = 'imprimirPam';
        }

        if ($contrato['Contrato']['ic_ativo'] == 0) {
            $titulo .= '<br>CONTRATO DESATIVADO!!! MOTIVO: ' . $contrato['Contrato']['ds_motivo'];
        }
        
        $contratoHtml = $this->$funcao($check_modulos, $contrato);

        App::import('Vendor', 'pdf');
        $PDF = new PDF();
        if ($contrato['Contrato']['ic_ativo'] == 0) {
            $PDF->imprimir($contratoHtml, 'P', $titulo, true);
        } else {
            $PDF->imprimir($contratoHtml, 'P', $titulo, false);
        }
        die();
    }
    
    private function imprimirContrato($check_modulos, $contrato){
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Number');
        App::import('Helper', 'Formatacao');
        
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();
        
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('tbody');
        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>OBJETO</b>',
                $contrato['Contrato']['ds_objeto'],
                '<b>OBSERVAÇÕES</b>',
                $contrato['Contrato']['ds_observacao']
            ),
            array(
                '<b>CLIENTE</b>',
                $contrato['Cliente']['no_razao_social'],
                '<b>CLIENTE RESPONSÁVEL</b>',
                $contrato['Cliente']['no_responsavel']
            ),
            array(
                '<b>TIPO DO SERVIÇO</b>',
                $contrato['Contrato']['ds_tipo_servico'],
                '',
                ''
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        /**
         * Informações Básicas *
         */
        if ($check_modulos[0] == 1) {
            $contratoHtml .= $this->imprimirInformacaoBasica($contrato, $html, $print, $formatacao);
        }
        /**
         * Fiscalização *
         */
        if ($check_modulos[1] == 1) {
            $contratoHtml .= $this->imprimirFiscalizacao($contrato, $html, $print, $formatacao);
        }

        /**
         * Aditivos *
         */
        if ($check_modulos[2] == 1) {
            $contratoHtml .= $this->imprimirAditivo($contrato, $html, $print, $formatacao);
        }

        /**
         * Garantias *
         */
        if ($check_modulos[3] == 1) {
            $contratoHtml .= $this->imprimirGarantia($contrato, $html, $print, $formatacao);
        }

        /**
         * Empenhos *
         */
        if ($check_modulos[4] == 1) {
            $contratoHtml .= $this->imprimirEmpenho($contrato, $html, $print, $formatacao);
        }

        /**
         * Pagamentos *
         */
        if ($check_modulos[5] == 1) {
            $contratoHtml .= $this->imprimirPagamento($contrato, $html, $print, $formatacao);
        }

        /**
         * HistÃ³rico *
         */
        if ($check_modulos[6] == 1) {
            $contratoHtml .= $this->imprimirHistorico($contrato, $html, $print, $formatacao);
        }

        /**
         * PendÃªncias *
         */
        if ($check_modulos[7] == 1) {
            $contratoHtml .= $this->imprimirPendencia($contrato, $html, $print, $formatacao);
        }

        /**
         * Andamento *
         */
        if ($check_modulos[8] == 1) {
            $contratoHtml .= $this->imprimirAndamento($contrato, $html, $print, $formatacao);
        }

        /**
         * ExecuÃ§Ã£o *
         */
        if ($check_modulos[9] == 1) {
            $contratoHtml .= $this->imprimirExecucao($contrato, $html, $print, $formatacao);
        }
        // Apostilamentos
        if ($check_modulos[10] == 1) {
            $contratoHtml .= $this->imprimirApostilamento($contrato, $html, $print, $formatacao);
        }

        // Penalidade
        if ($check_modulos[11] == 1) {
            $contratoHtml .= $this->imprimirPenalidade($contrato, $html, $print, $formatacao);
        }

        // Notas Fiscais
        if ($check_modulos[12] == 1) {
            $contratoHtml .= $this->imprimirNotaFiscal($contrato, $html, $print, $formatacao);
        }

        // Assinaturas
        if (isset($check_modulos[13]) && ($check_modulos[13] == 1)) {
            $contratoHtml .= $this->imprimirAssinaturas($contrato, $html, $print, $formatacao);
        }
        return $contratoHtml;
    }
    
    private function imprimirPam($check_modulos, $contrato){

        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Number');
        App::import('Helper', 'Formatacao');
        
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();
        
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('tbody');
        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>FASE ATUAL</b>',
                $contrato['Fase']['ds_fase'],
                '<b>CLIENTE</b>',
                $contrato['Cliente']['no_razao_social'],
            ),
            array(
                '<b>RESPONSÁVEL / PARCEIRO</b>',
                $contrato['Parceiro']['ds_nome'],
                '<b>RESPONSÁVEL REDUCALL</b>',
                $contrato['GestorAtual']['ds_nome']
            ),
            array(
                '<b>VALOR ESTIMADO DAS CONTAS</b>',
                $formatacao->moeda($contrato['Contrato']['vl_global']),
                '<b>DATA INICIAL</b>',
                $contrato['Contrato']['dt_autorizacao_pam']
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        /**
         * HistÃ³rico *
         */
        if ($check_modulos[6] == 1) {
            $contratoHtml .= $this->imprimirHistorico($contrato, $html, $print, $formatacao);
        }

        /**
         * PendÃªncias *
         */
        if ($check_modulos[7] == 1) {
            $contratoHtml .= $this->imprimirPendencia($contrato, $html, $print, $formatacao);
        }

        /**
         * Andamento *
         */
        if ($check_modulos[8] == 1) {
            $contratoHtml .= $this->imprimirAndamento($contrato, $html, $print, $formatacao);
        }

        return $contratoHtml;
    }
    
    private function imprimirInformacaoBasica($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        
        /**
         * DETALHAMENTO *
         */
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'DETALHAMENTO'
        ), null, array(
            'colspan' => 8
        ));
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');
        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>RESPONSÁVEL ARARAS</b>',
                $contrato['GestorSuplente']['ds_nome'],
                '<b>PARCEIRO</b>',
                $contrato['Parceiro']['ds_nome'],
                '<b>TIPO DE CONTRATO</b>',
                $contrato['Modalidade']['ds_modalidade']
            ),
            array(
                '<b>SITUAÇÃO</b>',
                $contrato['Situacao']['ds_situacao'],
                '<b>PERÍODO</b>',
                $contrato['Contrato']['dt_ini_vigencia'] . '  à  ' . $contrato['Contrato']['dt_fim_vigencia'],
                '<b>ASSINATURA</b>',
                $contrato['Contrato']['dt_assinatura']
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        /**
         * RECURSOS FINANCEIROS *
         */
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            strtoupper(__('Redução Mensal', true))
        ), null, array(
            'colspan' => 8
        ));
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        $pc_pagamento = 0;
        $pc_aditivo = 0;
        $pc_restante = 0;
        //$pc_apostilamento = 0;
        $vl_total_pagamento = $this->Contrato->Pagamento->getTotal($coContrato);
        $vl_total_aditivo = $this->Contrato->Aditivo->getTotal($coContrato);
        //$vl_total_apostilamento = $this->Contrato->Apostilamento->getTotal($coContrato);
        $valor_total = ($contrato['Contrato']['vl_global'] + $vl_total_aditivo);
        $valor_restante = $valor_total - $vl_total_pagamento;
        if ($contrato['Contrato']['vl_global'] > 0) {
            $pc_pagamento = ($vl_total_pagamento * 100) / $valor_total;
            $pc_aditivo = ($vl_total_aditivo * 100) / $contrato['Contrato']['vl_global'];
            $pc_restante = ($valor_restante * 100) / $valor_total;
        }

        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>VALOR CONTA (Mensal)</b>',
                array(
                    $formatacao->moeda($valor_total),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>VALOR CONTA (Com Redução)</b>',
                array(
                    $formatacao->moeda($valor_total),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>PERCENTUAL DE REDUÇÃO</b>',
                array(
                    $contrato['Contrato']['pct_reducao'] . '%',
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>TOTAL HONORÁRIO RECEBIDO</b>',
                array(
                    $formatacao->moeda($vl_total_pagamento),
                    array(
                        'style' => 'text-align:right;'
                    )
                )
            ),
            array(
                '<b>VALOR REDUCALL</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_reducall']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>VALOR ARARAS</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_araras']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>VALOR PARCEIRO</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_parceiro']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '',
                ''
            ),
            array(
                '<b>VALOR RESSARCIMENTO</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_ressarcimento']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>RESSARCIMENTO REDUCALL</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_ressarcimento_reducall']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>RESSARCIMENTO ARARAS</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_ressarcimento_araras']),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>RESSARCIMENTO PARCEIRO</b>',
                array(
                    $formatacao->moeda($contrato['Contrato']['vl_ressarcimento_parceiro']),
                    array(
                        'style' => 'text-align:right;'
                    )
                )
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');
        
        /**
         * PROJEÇÕES *
         */
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'PROJEÇÕES DE RECEBIMENTO REDUCALL'
        ), null, array(
            'colspan' => 6
        ));
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>12 MESES</b>',
                array(
                    $formatacao->moeda( ($contrato['Contrato']['vl_global'] - $contrato['Contrato']['vl_inicial']) * 12 ),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>24 MESES</b>',
                array(
                    $formatacao->moeda( ($contrato['Contrato']['vl_global'] - $contrato['Contrato']['vl_inicial']) * 24 ),
                    array(
                        'style' => 'text-align:right;'
                    )
                ),
                '<b>36 MESES</b>',
                array(
                    $formatacao->moeda( ($contrato['Contrato']['vl_global'] - $contrato['Contrato']['vl_inicial']) * 36 ),
                    array(
                        'style' => 'text-align:right;'
                    )
                )
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        /**
         * PERCENTUAIS *
         */
//        $print_pc_aditivo = '';
//        if ($pc_aditivo >= 25) {
//            $print_pc_aditivo = '<font color="#FF0000"><b>' . $print->porcentagem($pc_aditivo) . '</b></font>';
//        } else {
//            $print_pc_aditivo = $print->porcentagem($pc_aditivo);
//        }
//
//        $print_pc_apostilamento = '';
//        if ($pc_apostilamento >= 25) {
//            $print_pc_apostilamento = '<font color="#FF0000"><b>' . $print->porcentagem($pc_apostilamento) . '</b></font>';
//        } else {
//            $print_pc_apostilamento = $print->porcentagem($pc_apostilamento);
//        }
//
//        $contratoHtml .= $html->tag('br');
//        $contratoHtml .= $html->tag('table', null, array(
//            'style' => 'width: 100%; position: relative;',
//            'class' => 'table table-bordered table-striped',
//            'cellspacing' => '0',
//            'cellpadding' => '0'
//        ));
//        $contratoHtml .= $html->tag('thead');
//        $contratoHtml .= $html->tableHeaders(array(
//            'PERCENTUAIS'
//        ), null, array(
//            'colspan' => 6
//        ));
//        $contratoHtml .= $html->tag('/thead');
//        $contratoHtml .= $html->tag('tbody');
//        $contratoHtml .= $html->tableCells(array(
//            array(
//                '<b>TOTAL PAGAMENTOS</b>',
//                $print->porcentagem($pc_pagamento),
//                '<b>TOTAL ADITIVOS</b>',
//                $print_pc_aditivo,
//                '<b>TOTAL APOSTILAMENTOS</b>',
//                $print_pc_apostilamento,
//                '<b>VALOR RESTANTE</b>',
//                $print->porcentagem($pc_restante)
//            )
//        ));
//        $contratoHtml .= $html->tag('/tbody');
//        $contratoHtml .= $html->tag('/table');

        /**
         * DATAS *
         */
//        $contratoHtml .= $html->tag('br');
//        $contratoHtml .= $html->tag('table', null, array(
//            'style' => 'width: 100%; position: relative;',
//            'class' => 'table table-bordered table-striped',
//            'cellspacing' => '0',
//            'cellpadding' => '0'
//        ));
//        $contratoHtml .= $html->tag('thead');
//        $contratoHtml .= $html->tableHeaders(array(
//            'DATAS'
//        ), null, array(
//            'colspan' => 4
//        ));
//        $contratoHtml .= $html->tag('/thead');
//        $contratoHtml .= $html->tag('tbody');
//
//        if ($contrato['Contrato']['nu_contrato'] != "") {
//            $contratoHtml .= $html->tableCells(array(
//                array(
//                    '<b>PERÍODO</b>',
//                    $contrato['Contrato']['dt_ini_vigencia'] . ' &nbsp;&nbsp;à &nbsp;&nbsp; ' . $contrato['Contrato']['dt_fim_vigencia'],
//                    '<b>EXPIRA EM</b>',
//                    $print->fimDaVigencia($contrato['Contrato'])
//                )
//            ));
//        } else {
//            if ($contrato['Contrato']['nu_processo'] != "") {
//                if (!empty($contrato['Contrato']['dt_fim_processo'])) {
//                    $dataProcesso = explode("/", $contrato['Contrato']['dt_fim_processo']);
//                    $dataFimProcesso = $dataProcesso[0] . '-' . $dataProcesso[1] . '-' . $dataProcesso[2];
//                    $contrato['Contrato']['dt_fim_garantia'] = date('d/m/Y', strtotime("+90 days", strtotime($dataFimProcesso)));
//                }
//                $contratoHtml .= $html->tableCells(array(
//                    array(
//                        '<b>PERÍODO DO PROCESSO</b>',
//                        $contrato['Contrato']['dt_ini_processo'] . ' &nbsp;&nbsp; à &nbsp;&nbsp; ' . $contrato['Contrato']['dt_fim_processo'],
//                        '<b>TÉRMINO DA GARANTIA</b>',
//                        $contrato['Contrato']['dt_fim_garantia']
//                    )
//                ));
//            }
//        }
//
//        $contratoHtml .= $html->tag('/tbody');
//        $contratoHtml .= $html->tag('/table');
        
        return $contratoHtml;
    }
    
    private function imprimirFiscalizacao($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $fiscalContrato = $this->Contrato->ContratoFiscal->find('all', array(
            'conditions' => array(
                'ContratoFiscal.co_contrato' => $coContrato
            ),
            'order' => array(
                'ContratoFiscal.dt_fim' => 'desc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'FISCALIZAÇÃO'
        ), null, array(
            'colspan' => 5
        ));
        if (count($fiscalContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>CPF</b>',
                    '<b>Nome</b>',
                    '<b>Email</b>',
                    '<b>Data Início</b>',
                    '<b>Data Fim</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($fiscalContrato) > 0) {
            foreach ($fiscalContrato as $fiscal) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $print->cpf($fiscal['Usuario']['nu_cpf']),
                        $fiscal['Usuario']['ds_nome'],
                        $fiscal['Usuario']['ds_email'],
                        $fiscal['ContratoFiscal']['dt_inicio'],
                        $fiscal['ContratoFiscal']['dt_fim']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="5">Não existem Fiscais cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirAditivo($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $aditivoContrato = $this->Contrato->Aditivo->find('all', array(
            'conditions' => array(
                'Aditivo.co_contrato' => $coContrato
            ),
            'order' => array(
                'Aditivo.dt_aditivo' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'ADITIVOS'
        ), null, array(
            'colspan' => 5
        ));
        if (count($aditivoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Tipo de Aditivo</b>',
                    '<b>Data</b>',
                    '<b>Valor Aditivo</b>',
                    '<b>Prazo Aditivo</b>',
                    '<b>Justificativa</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($aditivoContrato) > 0) {
            foreach ($aditivoContrato as &$aditivo) :
                // debug($aditivo);
                if (substr($aditivo['Aditivo']['dt_aditivo'], 6)) {
                    $aditivo['Aditivo']['dt_aditivo'] = ' ';
                }

                $contratoHtml .= $html->tableCells(array(
                    array(
                        $print->tpAditivo($aditivo['Aditivo']['tp_aditivo']),
                        $aditivo['Aditivo']['dt_aditivo'],
                        array(
                            $formatacao->moeda($aditivo['Aditivo']['vl_aditivo']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        $aditivo['Aditivo']['dt_prazo'],
                        $aditivo['Aditivo']['ds_aditivo']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="5">Não existem Aditivos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }

    private function imprimirGarantia($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $garantiaContrato = $this->Contrato->Garantia->find('all', array(
            'conditions' => array(
                'Garantia.co_contrato' => $coContrato
            ),
            'order' => array(
                'Garantia.dt_inicio' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'GARANTIAS'
        ), null, array(
            'colspan' => 6
        ));
        if (count($garantiaContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Modalidade</b>',
                    '<b>Valor</b>',
                    '<b>Data Início</b>',
                    '<b>Data Fim</b>',
                    '<b>Seguradora</b>',
                    '<b>Telefone</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($garantiaContrato) > 0) {
            foreach ($garantiaContrato as $garantia) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $garantia['Garantia']['ds_modalidade'],
                        array(
                            $formatacao->moeda($garantia['Garantia']['vl_garantia']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        $garantia['Garantia']['dt_inicio'],
                        $garantia['Garantia']['dt_fim'],
                        $garantia['Garantia']['no_seguradora'],
                        $garantia['Garantia']['nu_telefone']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="6">Não existem Garantias cadastradas.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }    
    
    private function imprimirEmpenho($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $empenhoContrato = $this->Contrato->Empenho->find('all', array(
            'conditions' => array(
                'Empenho.co_contrato' => $coContrato
            ),
            'order' => array(
                'Empenho.dt_empenho' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'EMPENHOS'
        ), null, array(
            'colspan' => 6
        ));
        if (count($empenhoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Empenho</b>',
                    '<b>Tipo</b>',
                    '<b>Valor</b>',
                    '<b>Data</b>',
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($empenhoContrato) > 0) {
            $arTipos = array(
                'O' => 'Original',
                'R' => 'Reforço',
                'A' => 'Anulação',
                'G' => 'Global',
                'E' => 'Estimativo',
                'S' => 'Suprimentos',
                'P' => 'Provisão'
            );
            foreach ($empenhoContrato as $empenho) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $empenho['Empenho']['nu_empenho'],
                        $print->type($empenho['Empenho']['tp_empenho'], $arTipos),
                        array(
                            $formatacao->moeda($empenho['Empenho']['vl_empenho']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        $empenho['Empenho']['dt_empenho'],
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="6">Não existem Empenhos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirPagamento($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $this->loadModel('Pagamento');
        $this->Pagamento->recursive = 2;
        $pagamentoContrato = $this->Pagamento->find('all', array(
            'conditions' => array(
                'Pagamento.co_contrato' => $coContrato
            ),
            'order' => array(
                'Pagamento.nu_ano_pagamento' => 'asc',
                'Pagamento.nu_mes_pagamento' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'PAGAMENTOS'
        ), null, array(
            'colspan' => 9
        ));
        if (count($pagamentoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Empenho</b>',
                    '<b>Mês/Ano</b>',
                    '<b>Vencimento</b>',
                    '<b>Envio ADM</b>',
                    '<b>Envio Financeiro</b>',
                    '<b>Dt. Pagamento</b>',
                    '<b>Vl. Pagamento</b>',
                    '<b>Imposto</b>',
                    '<b>Líquido</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($pagamentoContrato) > 0) {
            foreach ($pagamentoContrato as $pagamento) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $pagamento['Empenho']['nu_empenho'],
                        $pagamento['Pagamento']['nu_mes_pagamento'] . '/' . $pagamento['Pagamento']['nu_ano_pagamento'],
                        $pagamento['Pagamento']['dt_vencimento'],
                        $pagamento['Pagamento']['dt_administrativo'],
                        $pagamento['Pagamento']['dt_financeiro'],
                        $pagamento['Pagamento']['dt_pagamento'],
                        array(
                            $formatacao->moeda($pagamento['Pagamento']['vl_pagamento']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        array(
                            $formatacao->moeda($pagamento['Pagamento']['vl_imposto']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        array(
                            $formatacao->moeda($pagamento['Pagamento']['vl_liquido']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        )
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="9">Não existem Pagamentos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirHistorico($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $historicoContrato = $this->Contrato->Historico->find('all', array(
            'conditions' => array(
                'Historico.co_contrato' => $coContrato
            ),
            'order' => array(
                'Historico.dt_historico' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'HISTÓRICO'
        ), null, array(
            'colspan' => 3
        ));
        if (count($historicoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Assunto</b>',
                    '<b>Data</b>',
                    '<b>Observação</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($historicoContrato) > 0) {
            foreach ($historicoContrato as $historico) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $historico['Historico']['no_assunto'],
                        $historico['Historico']['dt_historico'],
                        $historico['Historico']['ds_observacao']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="3">Não existem Históricos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirPendencia($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $pendenciaContrato = $this->Contrato->Pendencia->find('all', array(
            'conditions' => array(
                'Pendencia.co_contrato' => $coContrato
            ),
            'order' => array(
                'Pendencia.dt_inicio' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'PENDÊNCIAS'
        ), null, array(
            'colspan' => 4
        ));
        if (count($pendenciaContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Descrição</b>',
                    '<b>Data Início</b>',
                    '<b>Data Fim</b>',
                    '<b>Situação</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($pendenciaContrato) > 0) {
            $arSituacoes = array(
                'P' => 'Pendente',
                'C' => 'Concluída'
            );
            foreach ($pendenciaContrato as $pendencia) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $pendencia['Pendencia']['ds_pendencia'],
                        $pendencia['Pendencia']['dt_inicio'],
                        $pendencia['Pendencia']['dt_fim'],
                        $print->type($pendencia['Pendencia']['st_pendencia'], $arSituacoes)
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="4">Não existem Pendências cadastradas.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirAndamento($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $andamentoContrato = $this->Contrato->Andamento->find('all', array(
            'conditions' => array(
                'Andamento.co_contrato' => $coContrato
            ),
            'order' => array(
                'Andamento.co_andamento' => 'desc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'ANDAMENTO'
        ), null, array(
            'colspan' => 3
        ));
        if (count($andamentoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Data</b>',
                    '<b>Usuário</b>',
                    '<b>Obervações</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($andamentoContrato) > 0) {

            foreach ($andamentoContrato as $andamento) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $andamento['Andamento']['dt_andamento'],
                        $andamento['Usuario']['ds_nome'],
                        $andamento['Andamento']['ds_andamento']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="3">Não existem Andamentos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirExecucao($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        App::import('Model', 'Atividade');
        $atividadesModel = new Atividade();

        $execucaoContrato = $atividadesModel->findAllByCoContrato($coContrato);

//            echo '<pre>';
//            var_dump($execucaoContrato);die;

        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'EXECUÇÃO'
        ), null, array(
            'colspan' => 6
        ));
        if (count($execucaoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Atividade</b>',
                    '<b>Responsável</b>',
                    array(
                        '<b>Período Planejado</b>',
                        array(
                            'style' => 'text-align:center;'
                        )
                    ),
                    array(
                        '<b>Período Executado</b>',
                        array(
                            'style' => 'text-align:center;'
                        )
                    ),
                    '<b>Término</b>',
                    array(
                        '<b>%</b>',
                        array(
                            'style' => 'text-align:right;'
                        )
                    )
                )
            ), array(
                'class' => 'altrow'
            ), null, false, false);
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($execucaoContrato) > 0) {

            foreach ($execucaoContrato as $atividade) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $atividade['Atividade']['ds_atividade'],
                        $atividade['Usuario']['ds_nome'],
                        array(
                            $atividade['Atividade']['dt_ini_planejado'] . ' à ' . $atividade['Atividade']['dt_fim_planejado'],
                            array(
                                'style' => 'text-align:center;'
                            )
                        ),
                        array(
                            $atividade['Atividade']['dt_ini_execucao'] . ' à ' . $atividade['Atividade']['dt_fim_execucao'],
                            array(
                                'style' => 'text-align:center;'
                            )
                        ),
                        $print->prazoAtividade($atividade['Atividade']['tp_andamento'], $atividade['Atividade']['dt_fim_planejado']),
                        array(
                            $atividade['Atividade']['pc_executado'],
                            array(
                                'style' => 'text-align:right;'
                            )
                        )
                    )
                ), array(
                    'class' => $print->alertAtividade($atividade)
                ), null, false, false);
                foreach ($atividade['children'] as $tarefa) :
                    $contratoHtml .= $html->tableCells(array(
                        array(
                            ' &nbsp; &#8227; &nbsp; ' . $tarefa['Atividade']['ds_atividade'],
                            $tarefa['Usuario']['ds_nome'],
                            array(
                                $tarefa['Atividade']['dt_ini_planejado'] . ' à ' . $tarefa['Atividade']['dt_fim_planejado'],
                                array(
                                    'style' => 'text-align:center;'
                                )
                            ),
                            array(
                                $tarefa['Atividade']['dt_ini_execucao'] . ' à ' . $tarefa['Atividade']['dt_fim_execucao'],
                                array(
                                    'style' => 'text-align:center;'
                                )
                            ),
                            $print->prazoAtividade($tarefa['Atividade']['tp_andamento'], $tarefa['Atividade']['dt_fim_planejado']),
                            array(
                                $tarefa['Atividade']['pc_executado'],
                                array(
                                    'style' => 'text-align:right;'
                                )
                            )
                        )
                    ), array(
                        'class' => $print->alertAtividade($tarefa)
                    ), null, false, false);
                endforeach;
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="6">Não existem ' . __('Atividades', true) . ' cadastradas.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');
    }
    
    private function imprimirApostilamento($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $apostilamentoContrato = $this->Contrato->Apostilamento->find('all', array(
            'conditions' => array(
                'Apostilamento.co_contrato' => $coContrato
            ),
            'order' => array(
                'Apostilamento.dt_apostilamento' => 'asc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'APOSTILAMENTOS'
        ), null, array(
            'colspan' => 5
        ));
        if (count($apostilamentoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Tipo de Apostilamento</b>',
                    '<b>Data</b>',
                    '<b>Valor Apostilamento</b>',
                    '<b>Prazo Apostilamento</b>',
                    '<b>Justificativa</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($apostilamentoContrato) > 0) {
            foreach ($apostilamentoContrato as $apostilamento) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $print->tpApostilamento($apostilamento['Apostilamento']['tp_apostilamento']),
                        $apostilamento['Apostilamento']['dt_apostilamento'],
                        array(
                            $formatacao->moeda($apostilamento['Apostilamento']['vl_apostilamento']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        $apostilamento['Apostilamento']['dt_prazo'],
                        $apostilamento['Apostilamento']['ds_apostilamento']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="5">Não existem Apostilamentos cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirPenalidade($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $this->loadModel('Penalidade');
        $andamentoContrato = $this->Penalidade->find('all', array(
            'conditions' => array(
                'Penalidade.co_contrato' => $coContrato
            ),
            'order' => array(
                'Penalidade.co_penalidade' => 'desc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'PENALIDADES'
        ), null, array(
            'colspan' => 7
        ));
        if (count($andamentoContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Data da Penalidade</b>',
                    '<b>Nota Fiscal</b>',
                    '<b>OBSERVAÇõES DE PENALIDADE</b>',
                    '<b>Tipo de Penalidade</b>',
                    '<b>Motivo da Penalidade</b>',
                    '<b>Início da Penalidade</b>',
                    '<b>Fim da Penalidade</b>'

                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if (count($andamentoContrato) > 0) {

            foreach ($andamentoContrato as $andamento) :
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $andamento['Penalidade']['dt_penalidade'],
                        $andamento['Penalidade']['co_nota'],
                        $andamento['Penalidade']['ds_observacao'],
                        $andamento['Penalidade']['ic_situacao'],
                        $andamento['Penalidade']['mt_penalidade'],
                        $andamento['Penalidade']['dit_ini_vigencia'],
                        $andamento['Penalidade']['dit_fim_vigencia']
                    ,

                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="3">Não existem Penalidades cadastrados.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');
        
        return $contratoHtml;
    }
    
    private function imprimirNotaFiscal($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $notasContrato = $this->Contrato->NotaFiscal->find('all', array(
            'conditions' => array(
                'NotaFiscal.co_contrato' => $coContrato
            ),
            'order' => array(
                'NotaFiscal.dt_recebimento' => 'desc'
            )
        ));
        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'NOTAS FISCAIS'
        ), null, array(
            'colspan' => 5
        ));
        if (count($notasContrato) > 0) {
            $contratoHtml .= $html->tableCells(array(
                array(
                    '<b>Número da Nota</b>',
                    '<b>Valor da Nota</b>',
                    '<b>Data da Nota</b>',
                    '<b>Descrição da Nota</b>'
                )
            ), array(
                'class' => 'altrow'
            ));
        }
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        $nomeEmpenhos = 'Nome dos Empenhos';
        if (count($notasContrato) > 0) {
            foreach ($notasContrato as $nota) :
                //                    $notasEmpenho = $this->Contrato->NotasEmpenho->find('all', array(
                //                        'conditions' => array('NotasEmpenho.co_nota' => $nota['NotaFiscal']['co_nota'])
                //                    ));
                //                    foreach ($notasEmpenho as $nEmpenho) :
                //                        $nuEmpenho = $this->Contrato->Empenho->find('all', array(
                //                            'conditions' => array('Empenho.co_empenho' => $nEmpenho['NotasEmpenho']['co_empenho'])
                //                        ));
                //                    $nomeEmpenhos = $nuEmpenho . '/' . $nuEmpenho['Empenho']['nu_empenho'];
                //                    endforeach;
                $contratoHtml .= $html->tableCells(array(
                    array(
                        $nota['NotaFiscal']['nu_nota'],
                        //$nota['NotaFiscal']['dt_envio'],
                        array(
                            $formatacao->moeda($nota['NotaFiscal']['vl_nota']),
                            array(
                                'style' => 'text-align:right;'
                            )
                        ),
                        $nota['NotaFiscal']['dt_nota'],
                        $nota['NotaFiscal']['ds_nota']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;
        } else {
            $contratoHtml .= '<tr><td colspan="5">Não existem Notas Fiscais cadastradas.</td></tr>';
        }
        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    private function imprimirAssinaturas($contrato, $html, $print, $formatacao) {
        $coContrato = $contrato['Contrato']['co_contrato'];
        $contratoHtml .= $this->imprimirAssinaturas($coContrato, $html);
        App::import('Model', 'Usuario');
        $usuarioModel = new Usuario();

        if ($contrato['Contrato']['co_assinatura_chefe']) {
            $usuarioChefe = $usuarioModel->findByCoUsuario($contrato['Contrato']['co_assinatura_chefe']);
        }

        if ($contrato['Contrato']['co_assinatura_ordenador']) {
            $usuarioOrdenador = $usuarioModel->findByCoUsuario($contrato['Contrato']['co_assinatura_ordenador']);
        }

        $contratoHtml .= $html->tag('br');
        $contratoHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));
        $contratoHtml .= $html->tag('thead');
        $contratoHtml .= $html->tableHeaders(array(
            'ASSINATURAS'
        ), null, array(
            'colspan' => 5
        ));
        $contratoHtml .= $html->tableCells(array(
            array(
                '<b>Tipo de assinatura</b>',
                '<b>Assinatura eletrônica</b>'
            )
        ), array(
            'class' => 'altrow'
        ));
        $contratoHtml .= $html->tag('/thead');
        $contratoHtml .= $html->tag('tbody');

        if ($contrato['Contrato']['co_assinatura_chefe'] != null) {
            $contratoHtml .= $html->tableCells(array(
                'Chefe',
                "Documento assinado eletrônicamente por " .
                $usuarioChefe['Usuario']['ds_nome'] . ", " .
                $usuarioChefe['Setor']['ds_setor'] . ", em " .
                substr($contrato['Contrato']['dt_assinatura_chefe'], 0, 10) . " às " .
                substr($contrato['Contrato']['dt_assinatura_chefe'], 11, 5) .
                ", conforme horário oficial de Brasília, com fundamento no § 1°, art. 6°,
                do Decreto n° 8.539 de 08/10/2015 da Presidência da República.",
            ), array(
                'class' => 'altrow'
            ));
        }

        if ($contrato['Contrato']['co_assinatura_ordenador'] != null) {
            $contratoHtml .= $html->tableCells(array(
                'Ordenador',
                "Documento assinado eletrônicamente por " . $usuarioOrdenador['Usuario']['ds_nome'] . ", " .
                $usuarioOrdenador['Setor']['ds_setor'] . ", em " .
                substr($contrato['Contrato']['dt_assinatura_ordenador'], 0, 10) . " às " .
                substr($contrato['Contrato']['dt_assinatura_ordenador'], 11, 5) .
                ", conforme horário oficial de Brasília, com fundamento no § 1°, art. 6°,
                do Decreto n° 8.539 de 08/10/2015 da Presidência da República.",
            ), array(
                'class' => 'altrow'
            ));
        }
        if (($contrato['Contrato']['co_assinatura_ordenador'] == null) && ($contrato['Contrato']['co_assinatura_chefe'] == null)) {
            $contratoHtml .= '<tr><td colspan="5">Não existem assinaturas.</td></tr>';
        }

        $contratoHtml .= $html->tag('/tbody');
        $contratoHtml .= $html->tag('/table');

        return $contratoHtml;
    }
    
    function homefiscal()
    {
    }

    function estacao()
    {
        $this->layout = 'estacao';
    }

    /**
     * @resource { "name" : "devolver", "route":"contratos\/devolver", "access": "private", "type": "edit" }
     */
    function devolver($id = null)
    {
        if (!empty($this->data) && isset($this->data['Contrato']['ds_justificativa'])) { // Informar Justificativa
            if ($this->data['Contrato']['ds_justificativa'] != "") {
                $this->Contrato->updFinanceiro($id, 'D');

                $this->addAndamento('Contrato Devolvido pelo Financeiro. Justificativa: ' . $this->data['Contrato']['ds_justificativa'], 
                        array('model'=>'Contrato'));
                $this->Session->setFlash('Contrato Devolvido com sucesso.');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('A Justificativa deve ser informada para Devolução.', true));
            }
        }

        if (empty($this->data['Contrato']['nu_processo'])) {
            $this->data = $this->Contrato->read(null, $id);
        }
        // $contratantes = $this->Contrato->Contratante->find( 'list' );
        $contratantes = $this->Contrato->Setor->find('list', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));
        $this->set(compact('contratantes'));
        $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        );
        $this->set(compact('fornecedores'));
        $modalidades = $this->Contrato->Modalidade->find('list');
        $this->set(compact('modalidades'));
        $situacaos = $this->Contrato->Situacao->find('list', array(
            'conditions' => array(
                'Situacao.tp_situacao = "C"',
                'Situacao.ic_ativo' => 1
            )
        ));
        $this->set(compact('situacaos'));

        $this->set(compact('id'));
        // echo $this->data[ 'Contrato' ][ 'nu_contrato' ];
        $this->set('contrato', $this->data['Contrato']);
    }

    /**
     * @resource { "name" : "Atualização do Contrato", "route":"contratos\/updflag", "access": "private", "type": "update" }
     */
    function updflag($id, $flag)
    {
        $this->Contrato->updFinanceiro($id, $flag);
        $msg = 'Contrato atualizado com sucesso.';
        $msgFn = 'Contrato ';
        if ($flag == 'R') {
            $msg = 'Contrato Recebido com sucesso.';
            $msgFn .= 'Recebido';
        }
        if ($flag == 'D') {
            $msg = 'Contrato Devolvido com sucesso.';
            $msgFn .= 'Devolvido';
        }
        $msgFn .= ' pelo Financeiro.';
        $this->addAndamento($msgFn, array('co_contrato'=>$id));
        $this->Session->setFlash($msg);
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
     * @resource { "name" : "Pesquisa de Valores", "route":"contratos\/valores", "access": "private", "type": "select" }
     */
    public function valores($coContrato = null)
    {
        $this->layout = 'blank';
        $findVlTotalEmpenhado = $this->Contrato->Empenho->find('first', array(
            'conditions' => array(
                'Empenho.co_contrato' => $coContrato
            ),
            'fields' => 'sum(Empenho.vl_empenho) as vl_total_empenhado'
        ));

        $findVlTotalLiquidacao = $this->Contrato->Liquidacao->find('first', array(
            'conditions' => array(
                'Liquidacao.co_contrato' => $coContrato
            ),
            'fields' => 'SUM(Liquidacao.vl_liquidacao) as vl_total_liquidado'
        ));

        $contrato = $this->Contrato->find('first', array(
            'recursive' => -1,
            'fields' => array(
                'nu_contrato', 'nu_processo', 'vl_inicial', 'vl_mensal', 'vl_global', 'tp_valor', 'dt_ini_vigencia',
                'dt_fim_vigencia', 'dt_ini_processo', 'dt_fim_processo', 'dt_fim_processo', 'ic_tipo_contrato', 'nu_qtd_parcelas', 'vl_reducall'
                , 'vl_araras', 'vl_parceiro', 'vl_ressarcimento_reducall', 'vl_ressarcimento_araras', 'vl_ressarcimento_parceiro', 'pct_reducao'
            ),
            'conditions' => array('co_contrato' => $coContrato)
        ));

        $this->set('vl_total_empenhado', $findVlTotalEmpenhado[0]['vl_total_empenhado']);
        $this->set('vl_total_liquidado', $findVlTotalLiquidacao[0]['vl_total_liquidado']);
        $this->set('vl_total_pagamento', $this->Contrato->Pagamento->getTotal($coContrato));
        $this->set('vl_total_aditivo', $this->Contrato->Aditivo->getTotalComReajuste($coContrato));
        $this->set('vl_total_apostilamento', $this->Contrato->Apostilamento->getTotal($coContrato));
        $this->set('valor_total', $this->Contrato->retornaValorAtualDoContrato($coContrato));
        $this->set('contrato', $contrato['Contrato']);
        $this->set(compact('coContrato'));
    }


    // percentuais
    /**
     * @resource { "name" : "Pesquisa de Percentauis", "route":"contratos\/percentuais", "access": "private", "type": "select" }
     */
    public function percentuais($coContrato = null)
    {
        $this->layout = 'blank';
        $this->set('vl_total_pagamento', $this->Contrato->Pagamento->getTotal($coContrato));
        $this->set('vl_total_aditivo', round($this->Contrato->Aditivo->getTotal($coContrato)));
        $this->set('vl_total_apostilamento', $this->Contrato->Apostilamento->getTotal($coContrato));

        $contrato = $this->Contrato->find('first', array(
            'recursive' => -1,
            'fields' => array(
                'nu_contrato', 'nu_processo', 'vl_inicial', 'vl_mensal', 'vl_global', 'tp_valor', 'dt_ini_vigencia', 'dt_fim_vigencia', 'dt_ini_processo', 'dt_fim_processo', 'dt_fim_processo', 'ic_tipo_contrato'
            ),
            'conditions' => array('co_contrato' => $coContrato)
        ));

        App::import('Model', 'Empenho');
        $empenhoModel = new Empenho();

        $totalEmpenhos = $empenhoModel->find('all', array(
                'conditions' => array(
                    'co_contrato' => $coContrato
                ),
                'fields' => array(
                    'sum(vl_empenho) as total_empenhos'
                )
            )
        );

        $this->set('total_empenhado', $totalEmpenhos[0][0]['total_empenhos']);
        $this->set('contrato', $contrato['Contrato']);
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "Adicionar Campo", "route":"contratos\/add_campo", "access": "private", "type": "insert" }
     */
    public function add_campo($coFiltro = null)
    {
        $this->layout = 'blank';

        App::import('Model', 'Filtro');
        $filtro = new Filtro();

        $this->set('tpFiltros', $filtro->find('list', array(
            'order' => 'ds_filtro ASC'
        )));
        $this->set('coFiltro', $coFiltro);

        if ($coFiltro > 0) {
            $filtro = $filtro->read(null, $coFiltro);
            if ($filtro['Filtro']['no_dominio'] != '') {
                $opcoes = array();
                App::import('Model', $filtro['Filtro']['no_dominio']);
                $dominio = new $filtro['Filtro']['no_dominio']();
                if ($filtro['Filtro']['ds_condicao'] != '') {
                    $opcoes['conditions'] = array(
                        $filtro['Filtro']['ds_condicao']
                    );
                }
                $this->set('dominio', $dominio->find('list', $opcoes));
            }
            $this->set(compact('filtro'));
        }
    }

    /**
     * @resource { "name" : "Pesquisa Avançada", "route":"contratos\/pesquisa_avancada", "access": "private", "type": "select" }
     */
    public function pesquisa_avancada()
    {
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        $this->autoRender = false;

        $this->Contrato->recursive = 1;
        $this->Contrato->validate = array();

        $criteria['limit'] = 10;

        // COLUNAS DA PESQUISA
        $criteria['fields'] = array(
            'Contrato.nu_pam',
            'Contrato.nu_processo',
            'Contrato.nu_contrato',
            'Setor.ds_setor',
            'Fase.ds_fase',
            'SetorAtual.ds_setor',
            'Situacao.ds_situacao',
            'Modalidade.nu_modalidade',
            'Contrato.nu_pendencias',
            'Contrato.ds_objeto',
            'Contrato.vl_global',
            'Contrato.dt_fim_processo',
            'Contrato.dt_fim_vigencia',
            'Contrato.dt_ini_vigencia',
            'Contrato.dt_ini_processo',
            'Contrato.ic_ativo',
            'Fornecedor.no_razao_social',
            'Contrato.ds_observacao',
            'Executante.ds_setor',
            'GestorAtual.ds_nome',
            'FiscalAtual.ds_nome'
        );
        if ($this->modulo->isInscricao()) {
            $criteria['fields'][] = 'Fornecedor.no_nome';
        }

        $usuario = $this->Session->read('usuario');

        if (!empty($usuario['ic_tipo_contrato']) && $usuario['ic_tipo_contrato'] != '') {
            $criteria['conditions']['ic_tipo_contrato'] = $usuario['ic_tipo_contrato'];
        }

        if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal) { // Fiscal
            $criteria['conditions']['co_fiscal_atual'] = $usuario['Usuario']['co_usuario'];
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor) { // Gestor
            $criteria['conditions']['co_gestor_atual'] = $usuario['Usuario']['co_usuario'];
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coChefeDepartamento) { // Chefe de Departamento
            $criteria['conditions']['co_contratante'] = $usuario['Usuario']['co_setor'];
        }

        if (! empty($this->data['Filtro'])) {

            $filtroUtilizado = '';

            App::import('Model', 'Filtro');
            $filtro = new Filtro();

            foreach (array_keys($this->data['Filtro']) as $k => $noFiltro) :
                if (isset($this->data['Filtro']['fiscal'])) {
                    App::import('Model', 'ContratoFiscal');
                    $contratoFiscal = new ContratoFiscal();
                    $contratos = $contratoFiscal->findAllByUsuario($this->data['Filtro']['fiscal'][0], 0);
                    $criteria['conditions']['co_contrato'] = $contratos;
                } elseif (isset($this->data['Filtro']['gestor'])) {
                    $contratos = $this->Contrato->findAllByGestor($this->data['Filtro']['gestor'][0]);
                    $criteria['conditions']['co_contrato'] = $contratos;

                    unset($criteria['fields']);
                } elseif ((isset($this->data['Filtro']['tp_filtro']) && $this->data['Filtro']['tp_filtro'] != '') ||
                    (isset($this->data['tp_filtro_avan']) && $this->data['tp_filtro_avan'] != '')
                ) {
                    $filtroPesq = $filtro->find('first', array(
                        'conditions' => array(
                            'Filtro.no_filtro' => $noFiltro
                        )
                    ));

                    if ($filtroUtilizado != '') {
                        $filtroUtilizado .= ' &nbsp; | &nbsp; ';
                    }

                    $filtroUtilizado .= $filtroPesq['Filtro']['ds_filtro'];

                    $valor = $this->data['Filtro'][$noFiltro]; // VALOR DIGITADO NA CONSULTA

                    if ($filtroPesq['Filtro']['tb_join'] != '') { // ACRESCENTA JOIN NA CONSULTA
                        $join = array();
                        $join['table'] = $filtroPesq['Filtro']['tb_join'];
                        $join['alias'] = $filtroPesq['Filtro']['no_dominio'];
                        if ($filtroPesq['Filtro']['tp_join'] != '') {
                            $join['type'] = $filtroPesq['Filtro']['tp_join'];
                        }
                        $join['conditions'] = array(
                            $filtroPesq['Filtro']['no_dominio'] . '.co_contrato = Contrato.co_contrato'
                        );
                        $criteria['joins'] = array(
                            $join
                        );
                    }

                    if ($filtroPesq['Filtro']['ds_group'] != '') { // ACRESCENTA GROUP NA CONSULTA
                        if (strpos($filtroPesq['Filtro']['ds_group'], '?') > 0) {
                            $criteria['group'] = array(
                                str_replace('?', $valor, $filtroPesq['Filtro']['ds_group'])
                            );
                        } else {
                            $criteria['group'] = array(
                                $filtroPesq['Filtro']['ds_group']
                            );
                        }
                        $criteria['limit'] = 10000; // NÃO É POSSÍVEL PAGINAR QUANDO TEM GROUP
                    }

                    switch ($filtroPesq['Filtro']['tp_filtro']) {
                        case 'fixo':
                            if ($filtroPesq['Filtro']['ds_formula'] != '') {
                                $criteria['conditions'][] = $filtroPesq['Filtro']['ds_formula'];
                            }
                            break;
                        case 'money':
                            $filtroUtilizado .= ': ' . $valor;
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][$filtroPesq['Filtro']['no_coluna']] = ln($valor);
                            }
                            break;
                        case 'select':
                            $filtroUtilizado .= ': ' . $this->data['Filtro'][$noFiltro][1];
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][$filtroPesq['Filtro']['no_coluna']] = $this->data['Filtro'][$noFiltro][0];
                            }
                            break;
                        case 'int':
                            $filtroUtilizado .= ': ' . $valor;
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][$filtroPesq['Filtro']['no_coluna']] = $valor;
                            }
                            if ($filtroPesq['Filtro']['ds_formula'] != '') {
                                $criteria['conditions'][] = str_replace('?', $valor, $filtroPesq['Filtro']['ds_formula']);
                            }
                            break;
                        case 'date':
                            $filtroUtilizado .= ': ' . $valor;
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][$filtroPesq['Filtro']['no_coluna']] = dtDb($valor);
                            }
                            break;
                        case 'txt':
                            $filtroUtilizado .= ': ' . $valor;
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $valor = up($this->data['Filtro'][$noFiltro]);
                                if ($filtroPesq['Filtro']['is_limpar'] == '1') {
                                    $valor = $this->Functions->limparMascara($valor);
                                }
                                $criteria['conditions'][$filtroPesq['Filtro']['no_coluna'] . ' like'] = '%' . $valor . '%';
                            }
                            break;
                        case 'daterange':
                            $filtroUtilizado .= ': ' . $this->data['Filtro'][$noFiltro][0] . ' - ' . $this->data['Filtro'][$noFiltro][1];
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][] = $filtroPesq['Filtro']['no_coluna'] . " between '" . dtDb($this->data['Filtro'][$noFiltro][0]) . "' and '" . dtDb($this->data['Filtro'][$noFiltro][1]) . "' ";
                            }
                            break;
                        case 'moneyrange':
                            $filtroUtilizado .= ': ' . $this->data['Filtro'][$noFiltro][0] . ' - ' . $this->data['Filtro'][$noFiltro][1];
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][] = $filtroPesq['Filtro']['no_coluna'] . " >= '" . ln($this->data['Filtro'][$noFiltro][0]) . "' and " . $filtroPesq['Filtro']['no_coluna'] . " <= '" . ln($this->data['Filtro'][$noFiltro][1]) . "' ";
                            }
                            break;
                        case 'intrangedate':
                            $filtroUtilizado .= ': ' . $this->data['Filtro'][$noFiltro][0] . ' - ' . $this->data['Filtro'][$noFiltro][1];
                            if ($filtroPesq['Filtro']['no_coluna'] != '') {
                                $criteria['conditions'][] = "DATEDIFF(" . $filtroPesq['Filtro']['no_coluna'] . ", current_date()) >= '" . $this->data['Filtro'][$noFiltro][0] . "' and " . "DATEDIFF(" . $filtroPesq['Filtro']['no_coluna'] . ", current_date()) <= '" . $this->data['Filtro'][$noFiltro][1] . "' ";
                            }
                            break;
                    }
                }
            endforeach;

            $this->Session->write('criteria_avancado', $criteria);
            $this->Session->write('filtro_utilizado', $filtroUtilizado);
        } else {
            $criteria = $this->Session->read('criteria_avancado');
        }

        App::import('Model', 'Uasg');
        $uasgModel = new Uasg();
        $uasgs = $uasgModel->find('all');

        $uasgArray = array();

        foreach ($uasgs as $uasg) {
            $uasgArray[$uasg['Uasg']['uasg']] = $uasg['Uasg']['uasg'];
        }

        $this->set('uasgsArray', $uasgArray);

        $this->paginate = $criteria;
        $this->set('contratos', $this->paginate());

        $moduloConfig = 'ctr';
        if (isset($usuario['ic_tipo_contrato']) && $usuario['ic_tipo_contrato'] == 'E') {
            $moduloConfig = 'cte';
        }
        App::import('Model', 'ColunaResultadoPesquisa');
        $dbColunaResultadoPesquisa = new ColunaResultadoPesquisa();
        $this->set('colunasResultadoPesquisa', $dbColunaResultadoPesquisa->findUsuario('all', $moduloConfig));

        $this->Session->setFlash($this->Session->read('filtro_utilizado'));

        $this->render('/contratos/index');
    }

    /**
     * @resource { "name" : "Pesquisa", "route":"contratos\/pesquisa", "access": "private", "type": "select" }
     */
    function pesquisa()
    {
        $this->layout = 'iframe';
        $this->set('tpFiltros', array(
            '1' => 'Número do Contrato'
        ));
    }

    /**
     * @resource { "name" : "iframe pesquisa", "route":"contratos\/iframe_pesquisa", "access": "private", "type": "select" }
     */
    function iframe_pesquisa()
    {
        $this->layout = 'iframe';
    }

    /**
     * @resource { "name" : "PAM Externo", "route":"contratos\/pam_externo", "access": "private", "type": "pam_externo" pam_externo
     */
    function pam_externo()
    {
        $this->layout = 'externo';
        $this->Contrato->recursive = 2;
        $this->set('pams',
            $this->Contrato->find('all', array(
                'order' => 'dt_cadastro_pam DESC',
                'limit' => 10,
                'fields' => array('Contrato.nu_pam', 'Contrato.dt_cadastro_pam', 'Setor.ds_setor'),
                'conditions' => array(
                    'not' => array('Contrato.dt_cadastro_pam' => null)
                )))
        );
    }

    /**
     * @resource { "name" : "Adicionar PAM Externo", "route":"contratos\/add_pam_externo", "access": "private", "type": "insert" }
     */
    function add_pam_externo($tipoPam)
    {
        $this->set(compact('tipoPam'));
        $this->layout = 'externo';
        $this->setValidacoesPamExterno($tipoPam);

        if (!empty($this->data)) {

            $this->Contrato->create();
            $this->prepararCampos();
            $this->data['Contrato']['dt_cadastro_pam'] = DboSource::expression('CURRENT_TIMESTAMP');

            $this->data['Contrato']['nu_sequencia'] = 1;
            $fluxo = $this->Contrato->Fluxo->read(null, 1);
            $this->data['Contrato']['co_fase'] = $fluxo['Fluxo']['co_fase'];
            $this->data['Contrato']['co_setor'] = $fluxo['Fluxo']['co_setor'];
            $this->data['Contrato']['dt_fase'] = date('d/m/Y');

            $this->data['Contrato']['nu_pam'] = $this->Contrato->getNextPam();
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $nu_pam = $print->pam($this->data['Contrato']['nu_pam']);

            if ($this->Contrato->saveAll($this->data)) {

                App::import('Model', 'Anexo');
                App::import('Model', 'AssinaturasRequisitante');
                $anexoDb = new Anexo();
                if (!empty($this->data['Contrato']['anexo']) && $this->data['Contrato']['anexo']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '1º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo'];
                    $anexoDb->save($anexo);
                }
                if (!empty($this->data['Contrato']['anexo2']) && $this->data['Contrato']['anexo2']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '2º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo2'];
                    $anexoDb->save($anexo);
                }
                if (!empty($this->data['Contrato']['anexo3']) && $this->data['Contrato']['anexo3']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = '3º - ANEXO DO ' . __('PAM', true) . ': ' . $nu_pam;
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo3'];
                    $anexoDb->save($anexo);
                }
                if (!empty($this->data['Contrato']['anexo4']) && $this->data['Contrato']['anexo4']['name'] != '') {
                    $anexoDb->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->Contrato->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '8';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = 'ANEXO DO PLANO DE TRABALHO';
                    $anexo['Anexo']['conteudo'] = $this->data['Contrato']['anexo4'];
                    $anexoDb->save($anexo);
                }

                $assinatura = array();
                $assinatura['AssinaturasRequisitante']['nome_completo'] = $this->data['Contrato']['ds_requisitante'];
                $assinatura['AssinaturasRequisitante']['cargo'] = $this->data['Contrato']['pt_requisitante'];
                $assinatura['AssinaturasRequisitante']['data_assinatura'] = date('Y-m-d H:i');
                $assinatura['AssinaturasRequisitante']['referencia'] = "com fundamento no § 1°, art. 6°, do Decreto n° 8.539 de 08/10/2015 da Presidência da República.";
                $assinatura['AssinaturasRequisitante']['co_contrato'] = $this->Contrato->getInsertID();
                $assinaturaRequisitante = new AssinaturasRequisitante();
                $assinaturaRequisitante->create();
                $assinaturaRequisitante->save($assinatura);

                $this->addAndamento(__('PAM', true) . ' cadastrado.', array('nu_sequencia'=>1));
                $this->Session->setFlash('Prospecção Solicitada com Sucesso!<br><b>N° do ' . __('PAM', true) . ' solicitado: ' . $nu_pam . '</b>');
                $this->redirect(array('action' => 'pam_externo'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $contratantes = $this->Contrato->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));

        $fornecedores = $this->Contrato->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        );
        $this->set(compact('fornecedores'));

        $this->set(compact('contratantes'));
    }

    function setValidacoesPamExterno($tipo)
    {
        $this->Contrato->validate = null;

        $this->Contrato->validate['co_contratante'] = array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Unidade Solicitante em branco'
            )
        );

        $this->Contrato->validate['ds_requisitante'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Nome do Requisitante em branco'
            )
        );

        $this->Contrato->validate['tl_requisitante'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Telefone do Requisitante em branco'
            )
        );

        $this->Contrato->validate['ds_assunto'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Assunto do Pedido em branco'
            )
        );

        $this->Contrato->validate['pt_requisitante'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Posto/Grad/Cat. Func. do Requisitante em branco'
            )
        );

        $this->Contrato->validate['dt_solicitacao'] = array(
            'numeric' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Data da Solicitação em branco'
            )
        );

        if ($tipo == 1) {

            $this->Contrato->validate['ds_objeto_plano'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Objeto em branco'
                )
            );

            //          Esses validates colocam/retiram os "*".. foi retirada para a demanda 819

            //            $this->Contrato->validate['ds_vantagem'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Vantagens e Economicidade para a Administração em branco'
            //                )
            //            );

            //            $this->Contrato->validate['ds_criterio'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Critérios de Controles e Registros a Serem Adotados em branco'
            //                )
            //            );

            //            $this->Contrato->validate['ds_justificativa'] = array(
            //            $this->Contrato->validate['ds_justificativa'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Justificativa para Aquisição em branco'
            //                )
            //            );

            //            $this->Contrato->validate['ds_resultado'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Verificação dos Resultados em branco'
            //                )
            //            );

            //            $this->Contrato->validate['ds_demanda'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Demanda Prevista e Quantidade Bens e Serviços a serem Contratados em branco'
            //                )
            //            );

            //            $this->Contrato->validate['ds_aproveitamento'] = array(
            //                'numeric' => array(
            //                    'rule' => array(
            //                        'notEmpty'
            //                    ),
            //                    'message' => 'Aproveitamento de Servidores do Quadro, de Bens, de Equipamentos em branco'
            //                )
            //            );

        }

        if ($tipo == 2) {

            $this->Contrato->validate['ic_ata_orgao'] = array(
                'numeric' => array(
                    'rule' => array(
                        'notEmpty'
                    ),
                    'message' => 'Ata do HFA? em branco'
                )
            );

        }

    }

    /**
     * @resource { "name" : "Assinar Eletronicamente", "route":"contratos\/assinar_eletronicamente", "access": "private", "type": "update" }
     */
    function assinar_eletronicamente()
    {
        $this->loadModel('Usuario');
        if (!empty($this->data['co_contrato']) && !empty($this->data['co_usuario'])) {
            $contrato = $this->Contrato->read(null, $this->data['co_contrato']);
            $usuario = $this->Usuario->read(null, $this->data['co_usuario']);

            if ($usuario['Setor']['ds_setor'] == 'Chefia de Divisão') {
                $contrato['Contrato']['co_assinatura_chefe'] = $usuario['Usuario']['co_usuario'];

                $this->Contrato->id = $contrato['Contrato']['co_contrato'];

                if ($this->Contrato->saveField('co_assinatura_chefe', $contrato['Contrato']['co_assinatura_chefe'])) {
                    $this->Contrato->saveField('dt_assinatura_chefe', date('Y-m-d H:i'));
                    $this->caseSuccess($contrato['Contrato']['co_contrato']);
                } else {
                    $this->caseError($contrato['Contrato']['co_contrato']);
                }
            } elseif ($usuario['Setor']['ds_setor'] == 'Ordenador de Despesas') {
                $contrato['Contrato']['co_assinatura_ordenador'] = $usuario['Usuario']['co_usuario'];

                $this->Contrato->id = $contrato['Contrato']['co_contrato'];

                if ($this->Contrato->saveField('co_assinatura_ordenador', $contrato['Contrato']['co_assinatura_ordenador'])) {
                    $this->Contrato->saveField('dt_assinatura_ordenador', date('Y-m-d H:i'));
                    $this->caseSuccess($contrato['Contrato']['co_contrato']);
                } else {
                    $this->caseError($contrato['Contrato']['co_contrato']);
                }
            }

        } else {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'contratos',
                'action' => 'index'
            ));
        }
    }

    function caseSuccess($coContrato)
    {
        $this->Session->setFlash(__('Registro salvo com sucesso', true));
        $this->redirect(array(
            'controller' => 'contratos',
            'action' => 'detalha',
            $coContrato
        ));
    }

    function caseError($coContrato)
    {
        $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
        $this->redirect(array(
            'controller' => 'contratos',
            'action' => 'detalha',
            $coContrato
        ));
    }

    public function dateSql($dateSql)
    {
        $ano = substr($dateSql, 6);
        $mes = substr($dateSql, 3, -5);
        $dia = substr($dateSql, 0, -8);
        return $ano . "-" . $mes . "-" . $dia;
    }
}

?>
