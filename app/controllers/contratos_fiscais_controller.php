<?php

/**
 * @resource { "name" : "Cadastro de fiscais de contratos", "route":"contratos_fiscais", "access": "private", "type": "module" }
 */
class ContratosFiscaisController extends AppController
{

    var $name = 'ContratosFiscais';

    var $uses = array(
        'ContratoFiscal',
        'Contrato',
        'FiscalTipo'
    );

    var $layout = 'iframe';

    /**
     * @resource { "name" : "Fiscais do Contrato", "route":"contratos_fiscais\/index", "access": "private", "type": "select" }
     */
    function index($coContrato)
    {
        $this->ContratoFiscal->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'ContratoFiscal.co_contrato' => $coContrato
            ),
            'order' => array(
                'ContratoFiscal.dt_fim' => 'desc'
            )
        );


        $this->set('contratosFiscais', $this->paginate());

        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "iframe", "route":"contratos_fiscais\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "Atualizar Fiscal", "route":"contratos_fiscais\/updFiscal", "access": "private", "type": "update" }
     */
    function updFiscal($coContrato)
    {
        $fiscalAtual = $this->ContratoFiscal->find('first', array(
            'conditions' => array(
                'co_contrato' => $coContrato,
                'dt_inicio <=' => date('Y-m-d'),
                array('OR' => array(
                    'dt_fim >' => date('Y-m-d'),
                    'dt_fim' => null
                ))
            )
        ));

        $coFiscalAtual = null;
        if ($fiscalAtual['ContratoFiscal']['co_contrato_fiscal'] > 0) {
            $coFiscalAtual = $fiscalAtual['ContratoFiscal']['co_usuario_fiscal'];
        }
        $this->Contrato->id = $coContrato;
        $this->Contrato->saveField("co_fiscal_atual", $coFiscalAtual);
    }

    /**
     * @resource { "name" : "Adicionar Fiscal", "route":"contratos_fiscais\/add", "access": "private", "type": "insert" }
     */
    function add($coContrato = null)
    {
        $usuario = $this->Session->read('usuario');

        if (!empty($this->data)) {
            // setando co_usuario para usuário logado
            $this->data['ContratoFiscal']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $resContratoFiscal = $this->ContratoFiscal->getFiscalContrato($this->data['ContratoFiscal']['co_usuario_fiscal'],
                $coContrato,
                $this->data['ContratoFiscal']['co_fiscais_tipos'],
                0);

            if (count($resContratoFiscal) > 0) {
                $this->ContratoFiscal->id = $resContratoFiscal[0]['ContratoFiscal']['co_contrato_fiscal'];
                $this->ContratoFiscal->saveField('ic_ativo', true);
                $this->Session->setFlash(__('Registro ativado com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else if ($this->ContratoFiscal->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }

        }

        $this->ContratoFiscal->Usuario->displayField = 'nome_combo';

        $this->ContratoFiscal->Usuario->virtualFields = array(
            'nome_combo' => "CONCAT(Usuario.nu_cpf, ' - ', Usuario.ds_nome)"
        );

        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);

        $condicaoFiscal = array(
            'Usuario.ic_ativo' => 1
        );

        $fiscais = $this->ContratoFiscal->Usuario->find('list', array(
            'conditions' => $condicaoFiscal,
            'order' => array(
                'ds_nome' => 'ASC'
            )
        ));
        $Tiposfiscais = $this->FiscalTipo->find('list', array('conditions' => array('FiscalTipo.ic_ativo' => 1)));
        // $fiscais = $this->ContratoFiscal->Fiscal->find( 'list' );
        $this->set(compact('Tiposfiscais'));
        $this->set(compact('fiscais'));
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "Editar Fiscal", "route":"contratos_fiscais\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $coContrato)
    {
        $usuario = $this->Session->read('usuario');
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index', $coContrato));
        }
        if (!empty($this->data)) {
            //unset($this->ContratoFiscal->validate['co_usuario_fiscal']['myUnique']);
            // Setando co_usuario para usuário logado
            $this->data['ContratoFiscal']['co_usuario'] = $usuario['Usuario']['co_usuario'];

            if ($this->ContratoFiscal->save($this->data)) {
                $this->updFiscal($coContrato);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->ContratoFiscal->find(array('co_contrato_fiscal' => $id, 'co_contrato' => $coContrato));
        }

        $this->ContratoFiscal->Usuario->displayField = 'nome_combo';

        $this->ContratoFiscal->Usuario->virtualFields = array(
            'nome_combo' => "CONCAT(Usuario.nu_cpf, ' - ', Usuario.ds_nome)"
        );

        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);

        $condicaoFiscal = array(
            'Usuario.ic_ativo' => 1
        );

        $fiscais = $this->ContratoFiscal->Usuario->find('list', array(
            'conditions' => $condicaoFiscal,
            'order' => array(
                'ds_nome' => 'ASC'
            )
        ));

        $Tiposfiscais = $this->FiscalTipo->find('list', array(
                'conditions' => array(
                    'OR' => array(
                        'FiscalTipo.ic_ativo' => 1,
                        'FiscalTipo.co_fiscais_tipos' => $this->data['ContratoFiscal']['co_fiscais_tipos']
                    )
                )
            )
        );
        $this->set(compact('Tiposfiscais'));
        $this->set(compact('fiscais'));
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    /**
     * @resource { "name" : "Remover Fiscal", "route":"contratos_fiscais\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato)
    {
        unset($this->ContratoFiscal->validate);

        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index', $coContrato));
        } else {
            $this->ContratoFiscal->recursive = -1;
            $contratoFiscal = $this->ContratoFiscal->find(array('co_contrato_fiscal' => $id, 'co_contrato' => $coContrato));
            $contratoFiscal['ContratoFiscal']['ic_ativo'] = 0;

            if ($this->ContratoFiscal->save($contratoFiscal)) {
                $this->Session->setFlash(__('Registro excluído com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato));
            } else {

                $this->Session->setFlash(__('Erro ao excluir registro', true));
                $this->redirect(array('action' => 'index', $coContrato));
            }
        }

        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array('action' => 'index', $coContrato));
    }

    /**
     * @resource { "name" : "Histórico", "route":"contratos_fiscais\/list_historico", "access": "private", "type": "select" }
     */
    function list_historico($coFiscal, $coContrato)
    {
        App::import('Model', 'Log');
        $log = new Log();

        $fiscais = $log->find('all', array(
            'conditions' => array(
                'Log.ds_log LIKE \'%"co_contrato":"' . $coContrato . '"%\'',
                'Log.co_usuario' => $coFiscal
            ),
            'order' => array(
                'Log.dt_log' => 'desc'
            )
        ));
        $this->set('tpAcao', $log->tpAcao);
        $this->set(compact('fiscais'));
    }
}

