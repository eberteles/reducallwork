<?php
/**
* @resource { "name" : "Relatórios do Contrato", "route":"contratos_relatorio", "access": "private", "type": "module" }
*/
class ContratosRelatorioController extends AppController
{

    public $name = 'ContratosRelatorio';

    public $uses = array(
        'ContratosRelatorio'
    );

    /**
    * @resource { "name" : "listar", "route":"contratos_relatorio\/index", "access": "private", "type": "select" }
    */    
    function index()
    {
        $this->layout = 'contratos_relatorio';

        $this->loadModel('Situacao');
        $situacoes    = $this->Situacao->find('list',array(
            'fields' => array(
                'Situacao.co_situacao','Situacao.ds_situacao'
            ),
            'conditions' => array(
                'Situacao.ic_ativo' => 1
            )
        ));

        $this->set('situacoes', $situacoes);
    }

    /**
    * @resource { "name" : "imprimir relatorio geral", "route":"contratos_relatorio\/imprimirRelatorioGeralHtml", "access": "private", "type": "select" }
    */  
    function imprimirRelatorioGeralHtml( ){
        $this->layout = 'contratos_relatorio_html';

        if( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' ){
            if(!empty($this->data['PreImprimir']['modalidade'])){
                $conditions = array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => '',
                    'ContratosRelatorio.dt_ini_vigencia >=' => dtDb($this->data['PreImprimir']['dt_inicio']),
                    'ContratosRelatorio.dt_fim_vigencia <=' => dtDb($this->data['PreImprimir']['dt_final']),
                    'ContratosRelatorio.co_situacao' => $this->data['PreImprimir']['modalidade']
                );
            }else{
                $conditions = array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => '',
                    'ContratosRelatorio.dt_ini_vigencia >=' => dtDb($this->data['PreImprimir']['dt_inicio']),
                    'ContratosRelatorio.dt_fim_vigencia <=' => dtDb($this->data['PreImprimir']['dt_final'])
                );
            }
            $contratos = $this->ContratosRelatorio->find('all', array(
                'conditions' => $conditions,
                'fields' => array(
                    'ContratosRelatorio.nu_contrato','ContratosRelatorio.nu_processo','ContratosRelatorio.co_fornecedor',
                    'ContratosRelatorio.co_contratacao','ContratosRelatorio.ds_objeto','ContratosRelatorio.ds_fundamento_legal',
                    'ContratosRelatorio.dt_ini_vigencia','ContratosRelatorio.dt_fim_vigencia','ContratosRelatorio.ds_objeto',
                    'ContratosRelatorio.dt_publicacao','ContratosRelatorio.vl_global','ContratosRelatorio.co_situacao'
                )
            ));
            $this->set('dt_inicio', $this->data['PreImprimir']['dt_inicio']);
            $this->set('dt_final' , $this->data['PreImprimir']['dt_final'] );
        }else{
            if(!empty($this->data['PreImprimir']['modalidade'])){
                $conditions = array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => '',
                    'ContratosRelatorio.co_situacao' => $this->data['PreImprimir']['modalidade']
                );
            }else {
                $conditions = array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => ''
                );
            }
            $contratos = $this->ContratosRelatorio->find('all', array(
                'conditions' => $conditions,
                'fields' => array(
                    'ContratosRelatorio.nu_contrato','ContratosRelatorio.nu_processo','ContratosRelatorio.co_fornecedor',
                    'ContratosRelatorio.co_contratacao','ContratosRelatorio.ds_objeto','ContratosRelatorio.ds_fundamento_legal',
                    'ContratosRelatorio.dt_ini_vigencia','ContratosRelatorio.dt_fim_vigencia','ContratosRelatorio.ds_objeto',
                    'ContratosRelatorio.dt_publicacao','ContratosRelatorio.vl_global','ContratosRelatorio.co_situacao'
                )
            ));
        }

        $this->loadModel('Fornecedor');
        $this->loadModel('Situacao');
        $this->loadModel('Contratacao');

        $fornecedores = $this->Fornecedor->find('all',array(
            'fields' => array(
                'Fornecedor.nu_cnpj','Fornecedor.no_razao_social','Fornecedor.co_fornecedor'
            )
        ));
        $situacoes    = $this->Situacao->find('all',array(
            'fields' => array(
                'Situacao.co_situacao','Situacao.ds_situacao'
            )
        ));
        $contratacoes = $this->Contratacao->find('all',array(
            'fields' => array(
                'Contratacao.co_contratacao','Contratacao.ds_contratacao'
            )
        ));

        $this->set('contratos'   , $contratos   );
        $this->set('fornecedores', $fornecedores);
        $this->set('situacoes'   , $situacoes   );
        $this->set('contratacoes', $contratacoes);
    }

    /**
    * @resource { "name" : "imprimir pdf", "route":"contratos_relatorio\/printPdf", "access": "private", "type": "select" }
    */  
    function printPdf()
    {
//        App::import( 'Vendor', 'html2pdf' );
//        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
//
//        $html2pdf->pdf->SetDisplayMode('fullpage');
//        $html2pdf->writeHTML($this->params["form"]["html"]);
//        $html2pdf->Output('relatorio.pdf');

        App::import('Vendor', 'pdf');
        $PDF = new PDF();

        $titulo = '<b>Relatório Geral de Contratos</b>';

        header("Content-type:application/pdf");
        $PDF->imprimirNovacap($_POST["html"], 'R', $titulo, false);
//        $PDF->imprimirNovacap($this->params["form"]["html"], 'R', $titulo, false);

//        echo '<pre>';
//        var_dump($this->params["form"]["html"]);die;
    }

    /**
    * @resource { "name" : "imprimir relatorio geral", "route":"contratos_relatorio\/imprimirRelatorioGeral", "access": "private", "type": "select" }
    */  
    function imprimirRelatorioGeral( $dt_inicio = null , $dt_final = null ){

        if( $dt_inicio != null && $dt_final != null ){
            $contratos = $this->ContratosRelatorio->find('all', array(
                'conditions' => array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => '',
                    'ContratosRelatorio.dt_ini_vigencia >=' => $dt_inicio,
                    'ContratosRelatorio.dt_fim_vigencia <=' => $dt_final
                ),
                'fields' => array(
                    'ContratosRelatorio.nu_contrato','ContratosRelatorio.nu_processo','ContratosRelatorio.co_fornecedor',
                    'ContratosRelatorio.co_contratacao','ContratosRelatorio.ds_objeto','ContratosRelatorio.ds_fundamento_legal',
                    'ContratosRelatorio.dt_ini_vigencia','ContratosRelatorio.dt_fim_vigencia','ContratosRelatorio.ds_objeto',
                    'ContratosRelatorio.dt_publicacao','ContratosRelatorio.vl_global','ContratosRelatorio.co_situacao'
                )
            ));
        }else{
            $contratos = $this->ContratosRelatorio->find('all', array(
                'conditions' => array(
                    'ContratosRelatorio.ic_ativo' => 1,
                    'ContratosRelatorio.nu_contrato !=' => ''
                ),
                'fields' => array(
                    'ContratosRelatorio.nu_contrato','ContratosRelatorio.nu_processo','ContratosRelatorio.co_fornecedor',
                    'ContratosRelatorio.co_contratacao','ContratosRelatorio.ds_objeto','ContratosRelatorio.ds_fundamento_legal',
                    'ContratosRelatorio.dt_ini_vigencia','ContratosRelatorio.dt_fim_vigencia','ContratosRelatorio.ds_objeto',
                    'ContratosRelatorio.dt_publicacao','ContratosRelatorio.vl_global','ContratosRelatorio.co_situacao'
                )
            ));
        }

        $this->loadModel('Fornecedor');
        $this->loadModel('Situacao');
        $this->loadModel('Contratacao');

        $fornecedores = $this->Fornecedor->find('all',array(
            'fields' => array(
                'Fornecedor.nu_cnpj','Fornecedor.no_razao_social','Fornecedor.co_fornecedor'
            )
        ));
        $situacoes    = $this->Situacao->find('all',array(
            'fields' => array(
                'Situacao.co_situacao','Situacao.ds_situacao'
            )
        ));
        $contratacoes = $this->Contratacao->find('all',array(
            'fields' => array(
                'Contratacao.co_contratacao','Contratacao.ds_contratacao'
            )
        ));

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Contratos</b>';

        if( !empty($contratos) ) {
            $viagemHtml .= $html->tag('table', null, array(
                'style' => 'width: 100%; position: relative;',
                'class' => 'table table-bordered table-striped',
                'cellspacing' => '0',
                'cellpadding' => '0'
            ));

            $viagemHtml .= $html->tag('thead');

            $viagemHtml .= $html->tableCells(array(
                array(
                    '<b>NÚMERO DO CONTRATO</b>',
                    '<b>PROCESSO</b>',
                    '<b>EMPRESA</b>',
                    '<b>CNPJ</b>',
                    '<b>MODALIDADE DE LICITAÇÃO</b>',
                    '<b>OBJETO</b>',
                    '<b>FUNDAMENTO LEGAL</b>',
                    '<b>VIGÊNCIA</b>',
                    '<b>DATA DE PUBLICAÇÃO</b>',
                    '<b>VALOR</b>',
                    '<b>SITUAÇÃO</b>'
                )
            ), array(
                'class' => 'altrow'
            ));

            $viagemHtml .= $html->tag('/thead');
            $viagemHtml .= $html->tag('tbody');

            foreach ($contratos as $cont) :
                foreach( $fornecedores as $fornecedor ){
                    if( $fornecedor['Fornecedor']['co_fornecedor'] == $cont['ContratosRelatorio']['co_fornecedor'] ){
                        $nome_fornecedor = $fornecedor['Fornecedor']['no_razao_social'];
                        $cnpj_fornecedor = $fornecedor['Fornecedor']['nu_cnpj'];
                    }
                }
                foreach( $situacoes as $situacao ){
                    if( $situacao['Situacao']['co_situacao'] == $cont['ContratosRelatorio']['co_situacao'] ){
                        $nome_situacao = $situacao['Situacao']['ds_situacao'];
                    }
                }
                foreach( $contratacoes as $contratacao ){
                    if( $contratacao['Contratacao']['co_contratacao'] == $cont['ContratosRelatorio']['co_contratacao'] ){
                        $modalidade_contratacao = $contratacao['Contratacao']['ds_contratacao'];
                    }
                }

                $data1 = strtotime(dtDb($cont['ContratosRelatorio']['dt_ini_vigencia']));
                $data2 = strtotime(dtDb($cont['ContratosRelatorio']['dt_fim_vigencia']));
                $vigencia = (int)round(($data2 - $data1)/86400);

                $viagemHtml .= $html->tableCells(array(
                    array(
                        FunctionsComponent::mascara($cont['ContratosRelatorio']['nu_contrato'],'contrato'),
                        FunctionsComponent::mascara($cont['ContratosRelatorio']['nu_processo'],'processo'),
                        $nome_fornecedor,
                        FunctionsComponent::mascara($cnpj_fornecedor,'cnpj'),
                        $modalidade_contratacao,
                        $cont['ContratosRelatorio']['ds_objeto'],
                        $cont['ContratosRelatorio']['ds_fundamento_legal'],
                        $vigencia . " dias corridos",
                        $cont['ContratosRelatorio']['dt_publicacao'],
                        vlReal($cont['ContratosRelatorio']['vl_global']),
                        $nome_situacao
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;

            $viagemHtml .= $html->tag('/tbody');
            $viagemHtml .= $html->tag('/table');

        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }
}