<?php

class CronController extends AppController
{

    var $name = 'Cron';

    var $uses = array(
        'Contrato'
    );

    var $autoRender = false;
    var $EmailProvider = null;
    
    public function __construct()
    {
        App::import('Component', 'Email');
        $this->EmailProvider = new EmailComponent();
        
        echo ("Iniciando...");
        
        parent::__construct();
    }
    
    function beforeFilter()
    {}

    function index()
    {
        App::import('Shell', 'Shell');
        App::Import('Shell', 'Aviso');

        $myShell = new AvisoShell(new Object());
        $myShell->initialize();
        $myShell->main();
    }

    function expirando()
    {
        
        $arContratosExpirando = $this->Contrato->listarContratosExpirando();
        
        if (count($arContratosExpirando)) {
            $mensagem = "";
            
            $mensagem .= "Prezado,<br><br>";
            $mensagem .= "Os seguintes contratos estão expirando:<br><br>";
            
            foreach ($arContratosExpirando as $contrato) {
                
                $mensagem .= "Contrato: " . $this->Functions->mascara($contrato['contratos']['nu_contrato'], 'contrato') . " - Expira em: " . $this->Functions->data($contrato['contratos']['dt_fim_vigencia']) . "<br>";
            }
                        
            $this->EmailProvider->to = 'Eber <eberteles@hotmail.com>';
            
            $this->EmailProvider->subject = "AVISO Contrato expirando";

            if($this->EmailProvider->send($mensagem)) {
                echo ("E-mail envidado.");
            } else {
                echo ('Erro: ' . $this->EmailProvider->smtpError);
            }
            
            echo ("Processo finalizado.");
        } else {
            echo ("Processo finalizado. Sem avisos a serem realizados.");
        }
    }
    
}
?>