<?php
/**
* @resource { "name" : "Cronograma Financeiro Desembolso", "route":"cronograma_financeiro_desembolso", "access": "private", "type": "module" }
*/  
class CronogramaFinanceiroDesembolsoController extends AppController
{

    public $name = 'CronogramaFinanceiroDesembolso';

    public $layout = 'iframe';

    /**
    * @resource { "name" : "listar cronograma financeiro", "route":"cronograma_financeiro_desembolso\/index", "access": "private", "type": "select" }
    */  
    public function index($coContrato)
    {
        
        $this->CronogramaFinanceiroDesembolso->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
		'CronogramaFinanceiroDesembolso.co_contrato' => $coContrato
            )
        );
        // 'CronogramaFinanceiroDesembolso.co_contrato' => $coContrato
        
        $this->set('cronograma_financeiro_desembolso', $this->paginate());
        $this->set('coContrato', $coContrato);
//         $this->set(compact('coContrato'));
        
    }

    /**
    * @resource { "name" : "listar", "route":"cronograma_financeiro_desembolso\/index", "access": "private", "type": "select" }
    */     
    public function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name" : "add cronograma financeiro", "route":"cronograma_financeiro_desembolso\/add", "access": "private", "type": "insert" }
    */ 
    public function add($coContrato)
    {
        if (! empty($this->data)) {

            $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'ds_valor' ] = str_replace('.', '', $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'ds_valor' ]);
            $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'ds_valor' ] = str_replace(',', '.', $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'ds_valor' ]);

            $this->CronogramaFinanceiroDesembolso->create();
            if ($this->CronogramaFinanceiroDesembolso->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $meses = array(
            1 => "Janeiro",
            2 => "Fevereiro",
            3 => "Março",
            4 => "Abril",
            5 => "Maio",
            6 => "Junho",
            7 => "Julho",
            8 => "Agosto",
            9 => "Setembro",
            10 => "Outubro",
            11 => "Novemro",
            12 => "Dezembro"
        );
        
        $this->set('meses', $meses);
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name" : "editar cronograma finaneiro", "route":"cronograma_financeiro_desembolso\/edit", "access": "private", "type": "update" }
    */ 
    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->CronogramaFinanceiroDesembolso->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->CronogramaFinanceiroDesembolso->read(null, $id);
        }
        
        $meses = array(
            1 => "Janeiro",
            2 => "Fevereiro",
            3 => "Março",
            4 => "Abril",
            5 => "Maio",
            6 => "Junho",
            7 => "Julho",
            8 => "Agosto",
            9 => "Setembro",
            10 => "Outubro",
            11 => "Novemro",
            12 => "Dezembro"
        );
        
        $this->set('meses', $meses);
        $this->set('coContrato', $coContrato);
        $this->set('id', $id);
        $this->set(compact('id'));
    }

    /**
    * @resource { "name" : "deletar cronograma financeiro", "route":"cronograma_financeiro_desembolso\/delete", "access": "private", "type": "delete" }
    */ 
    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->CronogramaFinanceiroDesembolso->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>
