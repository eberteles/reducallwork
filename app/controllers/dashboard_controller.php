<?php
App::import('Model', 'Contrato');
App::import('Model', 'Pagamento');
App::import('Component', 'Functions');

/**
 * @resource { "name" : "Dashboard", "route":"dashboard", "access": "private", "type": "module" }
 */
class DashboardController extends AppController
{

    var $name = 'Dashboard';

    /**
     * @resource { "name" : "Dashboard", "route":"dashboard\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        App::import('Helper', 'Modulo');
        App::import('Model', 'Relatorio');

        $this->modulo = new ModuloHelper();
        $usuario = $this->Session->read('usuario');
        $relatorio = new Relatorio();

        if (!empty($this->data)) {
            if ($this->data['Dashboard']['tipo_dashboard'] == 'Operacional') {
                $isGerencial = false;
            } else {
                $isGerencial = true;
            }
        } else {
            $isGerencial = true;
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        if (($usuario['UsuarioPerfil']['co_perfil'] == $coGovernador || $usuario['UsuarioPerfil']['co_perfil'] == $coAdministrador)
            && $isGerencial
        ) { // Governador

            $contratoDb = new Contrato();

            $empenhoXPagamento = array();

            $this->set('empenhoXPagamento', $empenhoXPagamento);

            $this->set('contratosAtivosPorSetor', $this->contratosAtivosPorSetor());
            $this->set('contratosAPagarValor', $this->contratos_A_Pagar(1));
            $this->set('contratosAPagarPercentual', $this->contratos_A_Pagar(2));
            $this->set('contratosPagos_total_valores', $this->contratosPagos(1));
            $this->set('contratosPagos_total_percentual', $this->contratosPagos(2));
            $this->set('contratosPagosPorTrimetre', $this->contratosTrimestre(1));
            $this->set('contratosApagarPorTrimetre', $this->contratosTrimestre(2));
            $this->set('contratosComPendencias', $this->contratosAtivosComPendencias());
            $this->set('contratosComPagamentoAtrasado', $this->contratosComPagamentosEmAtraso());
            $this->set('contratosAVencer30', $this->contratosAVencer(0, 30));
            $this->set('contratosAVencer60', $this->contratosAVencer(30, 60));
            $this->set('contratosAVencer90', $this->contratosAVencer(60, 90));
            $this->set('contratosAVencer120', $this->contratosAVencer(90, 120));
            $this->set('contratosAVencer180', $this->contratosAVencer(90, 180));

            $contratoDb->recursive = 0;

            $pctVencendo30 = $relatorio->getPercentualVencendo(0, 30);
            $this->set('pctVencendo30', $pctVencendo30['pc_prazo']);
            $this->set('ttVencendo30', $pctVencendo30['tt_prazo']);
            $pctVencendo60 = $relatorio->getPercentualVencendo(30, 60);
            $this->set('pctVencendo60', $pctVencendo60['pc_prazo']);
            $this->set('ttVencendo60', $pctVencendo60['tt_prazo']);
            $pctVencendo90 = $relatorio->getPercentualVencendo(60, 90);
            $this->set('pctVencendo90', $pctVencendo90['pc_prazo']);
            $this->set('ttVencendo90', $pctVencendo90['tt_prazo']);
            $pctVencendo120 = $relatorio->getPercentualVencendo(90, 120);
            $this->set('pctVencendo120', $pctVencendo120['pc_prazo']);
            $this->set('ttVencendo120', $pctVencendo120['tt_prazo']);
            $pctVencendo180 = $relatorio->getPercentualVencendo(120, 180);
            $this->set('pctVencendo180', $pctVencendo180['pc_prazo']);
            $this->set('ttVencendo180', $pctVencendo180['tt_prazo']);

            /**
             * Percentual de Contratos com Pagamento em Atraso
             */
            $pctPgAtraso = $relatorio->getPercentualPgAtraso();
            $this->set('pctPgAtraso', $pctPgAtraso['pc_atraso']);

            /**
             * Percentual de Contratos / Processos com Pendencia
             */
            $pctCtPendencia = $relatorio->getPercentualPendencias();
            $this->set('pctCtPendencia', $pctCtPendencia['pc_pendencia']);
            $this->set('ttGrPendencia', $pctCtPendencia['tt_geral']);
            $this->set('ttCtPendencia', $pctCtPendencia['tt_pendencia']);

        } else {

            $pctVencendo30 = $relatorio->getPercentualVencendo(0, 30);
            $this->set('pctVencendo30', $pctVencendo30['pc_prazo']);
            $this->set('ttVencendo30', $pctVencendo30['tt_prazo']);
            $pctVencendo60 = $relatorio->getPercentualVencendo(30, 60);
            $this->set('pctVencendo60', $pctVencendo60['pc_prazo']);
            $this->set('ttVencendo60', $pctVencendo60['tt_prazo']);
            $pctVencendo90 = $relatorio->getPercentualVencendo(60, 90);
            $this->set('pctVencendo90', $pctVencendo90['pc_prazo']);
            $this->set('ttVencendo90', $pctVencendo90['tt_prazo']);
            $pctVencendo120 = $relatorio->getPercentualVencendo(90, 120);
            $this->set('pctVencendo120', $pctVencendo120['pc_prazo']);
            $this->set('ttVencendo120', $pctVencendo120['tt_prazo']);
            $pctVencendo180 = $relatorio->getPercentualVencendo(120, 180);
            $this->set('pctVencendo180', $pctVencendo180['pc_prazo']);
            $this->set('ttVencendo180', $pctVencendo180['tt_prazo']);

            /**
             * Percentual de Contratos com Pagamento em Atraso
             */
            $pctPgAtraso = $relatorio->getPercentualPgAtraso();
            $this->set('pctPgAtraso', $pctPgAtraso['pc_atraso']);

            /**
             * Percentual de Contratos / Processos com Pendencia
             */
            $pctCtPendencia = $relatorio->getPercentualPendencias();
            $this->set('pctCtPendencia', $pctCtPendencia['pc_pendencia']);
            $this->set('ttGrPendencia', $pctCtPendencia['tt_geral']);
            $this->set('ttCtPendencia', $pctCtPendencia['tt_pendencia']);
        }

        $this->set('vencimentosDashboard', Configure::read('App.config.resource.dashboard.vencimentos'));
        $this->set('opcoesDashboard', array(
            'Operacional' => 'Operacional',
            'Gerencial' => 'Gerencial'
        ));

        if (!empty($this->data)) {
            if ($this->data['Dashboard']['tipo_dashboard'] == 'Gerencial') {
                $this->render('governador');
            } elseif ($this->data['Dashboard']['tipo_dashboard'] == 'Operacional') {
                $this->render('index');
            } else {
                $this->render('index');
            }
        } else {
            if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor) { // Setor
                $this->render('setor');
            } else if ($usuario['UsuarioPerfil']['co_perfil'] == $coGovernador) { // Governador
                $this->render('governador');
            } else {
                $this->render('index');
            }
        }
    }

    /**
     * @resource { "name" : "listagem contratos ativos por setor", "route":"dashboard\/contratosAtivosPorSetor", "access": "private", "type": "select" }
     */
    public function contratosAtivosPorSetor()
    {
        $this->loadModel('Contrato');
        $this->autoRender = false;

        $contratos = $this->Contrato->contratosAtivos();

        $ativos = 0;
        $naoAtivos = 0;

        if ($contratos != NULL) {
            foreach ($contratos as $contrato) {
                $ativos = (isset($contrato['ativos'])) ? $contrato['ativos'] : 0;
                $naoAtivos = (isset($contrato['inativos'])) ? $contrato['inativos'] : 0;
            }
        }

        $json = array();
        array_push($json, array(
            'name' => 'Ativos',
            'y' => (int)$ativos,
            'color' => '#3399CC'
        ));
        array_push($json, array(
            'name' => 'Inativos',
            'y' => (int)$naoAtivos,
            'color' => '#FF6600'
        ));


        return json_encode($json);
    }

    /**
     * @resource { "name" : "listagem contratos ativos por setor", "route":"dashboard\/contratosAtivosPorSetor", "access": "private", "type": "select" }
     */
    public function contratos_A_Pagar($tipo)
    {
        $this->autoRender = false;
        $valores = $this->buscaDados(2);


        $jsonPercentual = array(
            1 => (array_sum($valores) != 0) ? round($valores[1] * 100 / array_sum($valores)) : 0,
            2 => (array_sum($valores) != 0) ? round($valores[2] * 100 / array_sum($valores)) : 0,
            3 => (array_sum($valores) != 0) ? round($valores[3] * 100 / array_sum($valores)) : 0,
            4 => (array_sum($valores) != 0) ? round($valores[4] * 100 / array_sum($valores)) : 0,
            5 => (array_sum($valores) != 0) ? round($valores[5] * 100 / array_sum($valores)) : 0,
            6 => (array_sum($valores) != 0) ? round($valores[6] * 100 / array_sum($valores)) : 0,
            7 => (array_sum($valores) != 0) ? round($valores[7] * 100 / array_sum($valores)) : 0,
            8 => (array_sum($valores) != 0) ? round($valores[8] * 100 / array_sum($valores)) : 0,
            9 => (array_sum($valores) != 0) ? round($valores[9] * 100 / array_sum($valores)) : 0,
            10 => (array_sum($valores) != 0) ? round($valores[10] * 100 / array_sum($valores)) : 0,
            11 => (array_sum($valores) != 0) ? round($valores[11] * 100 / array_sum($valores)) : 0,
            12 => (array_sum($valores) != 0) ? round($valores[12] * 100 / array_sum($valores)) : 0
        );


        if ($tipo == 1) {
            return json_encode(array_values($valores));
        }
        if ($tipo == 2) {
            return json_encode(array_values($jsonPercentual));
        }
    }


    //1 = a pagar 2 = pagos 3 = trimestre.
    /**
     * @resource { "name" : "listar dados de pagamentos", "route":"dashboard\/buscaDados", "access": "private", "type": "select" }
     */
    private function buscaDados($tipo_contrato = 3)
    {
        $pagos = array();
        $a_pagar = array();

        $pg = new Pagamento();
        $sql = $pg->selectPagamentos();

        foreach ($sql as $key => $dado) {
            $dado = $dado['pagamentos'];
            @$mes_vencimento = explode('-', $dado['dt_vencimento']);
            @$mes_vencimento = (int)$mes_vencimento[1];


            if ($dado['dt_pagamento'] != NULL) {
                $mes_pagamento = explode('-', $dado['dt_pagamento']);
                $mes_pagamento = (int)$mes_pagamento[1];
                @$pagos[$mes_pagamento] += $dado['vl_pago'];
                @$a_pagar[$mes_vencimento] += ($dado['vl_pagamento'] - $dado['vl_pago']);
            } else {
                @$a_pagar[$mes_vencimento] += $dado['vl_pagamento'];
            }
        }

        switch ($tipo_contrato) {
            case 1:

                for ($i = 1; $i < 13; $i++) {
                    if (!isset($pagos[$i])) {
                        $pagos[(int)$i] = (int)0;
                    }
                }

                ksort($pagos);
                return $pagos;


                break;

            case 2:

                for ($i = 1; $i < 13; $i++) {
                    if (!isset($a_pagar[$i])) {
                        $a_pagar[(int)$i] = (int)0;
                    }
                }
                ksort($a_pagar);
                return $a_pagar;

                break;

            case 3:

                for ($i = 1; $i < 13; $i++) {
                    if (!isset($pagos[$i])) {
                        $pagos[(int)$i] = (int)0;
                    }
                }
                if (!isset($a_pagar[$i])) {
                    $a_pagar[(int)$i] = (int)0;
                }

                for ($i = 1; $i < 13; $i++) {
                    if (!isset($a_pagar[$i])) {
                        $a_pagar[(int)$i] = (int)0;
                    }
                }


                ksort($a_pagar);
                ksort($pagos);


                $trimestres = array();
                $jackpot = 0;
                foreach ($pagos as $key => $pago) {

                    $jackpot += $pago;


                    if ($key % 3 === 0) {


                        @$trimestres['pagos'][(int)$key / 3] += $jackpot;
                        $jackpot = 0;

                    }
                }

                $jackpot = 0;
                foreach ($a_pagar as $key => $pagar) {

                    $jackpot += $pagar;

                    if ($key % 3 === 0) {

                        @$trimestres['a_pagar'][(int)$key / 3] += $jackpot;
                        $jackpot = 0;

                    }
                }

                return $trimestres;

                break;

            default:

                break;

        }


    }

    /**
     * @resource { "name" : "listagem contratos pagos", "route":"dashboard\/contratosPagos", "access": "private", "type": "select" }
     */
    public function contratosPagos($tipo)
    {
        $this->autoRender = false;

        $valores = $this->buscaDados(1);


        //O programador podia ter feito um foreach aqui.
        $jsonPercentual = array(
            1 => (array_sum($valores) != 0) ? round($valores[1] * 100 / array_sum($valores)) : 0,
            2 => (array_sum($valores) != 0) ? round($valores[2] * 100 / array_sum($valores)) : 0,
            3 => (array_sum($valores) != 0) ? round($valores[3] * 100 / array_sum($valores)) : 0,
            4 => (array_sum($valores) != 0) ? round($valores[4] * 100 / array_sum($valores)) : 0,
            5 => (array_sum($valores) != 0) ? round($valores[5] * 100 / array_sum($valores)) : 0,
            6 => (array_sum($valores) != 0) ? round($valores[6] * 100 / array_sum($valores)) : 0,
            7 => (array_sum($valores) != 0) ? round($valores[7] * 100 / array_sum($valores)) : 0,
            8 => (array_sum($valores) != 0) ? round($valores[8] * 100 / array_sum($valores)) : 0,
            9 => (array_sum($valores) != 0) ? round($valores[9] * 100 / array_sum($valores)) : 0,
            10 => (array_sum($valores) != 0) ? round($valores[10] * 100 / array_sum($valores)) : 0,
            11 => (array_sum($valores) != 0) ? round($valores[11] * 100 / array_sum($valores)) : 0,
            12 => (array_sum($valores) != 0) ? round($valores[12] * 100 / array_sum($valores)) : 0
        );

        if ($tipo == 1) {
            return json_encode(array_values($valores));
        }
        if ($tipo == 2) {
            return json_encode(array_values($jsonPercentual));
        }
    }

    public function contratosTrimestre($tipo)
    {
        $this->autoRender = false;
        $pg = new Pagamento();
        $valores = $this->buscaDados(3);


        $trimestrePago = $valores['pagos'];

        $trimestreAPagar = $valores['a_pagar'];


        if ($tipo == 1) {
            return json_encode(array_values($trimestrePago));
        } else
            if ($tipo == 2) {
                return json_encode(array_values($trimestreAPagar));
            }
    }

    public function contratosAtivosComPendencias()
    {
        $this->autoRender = false;
        $this->loadModel('Contrato');
        $contratos = $this->Contrato->contratosPendentes();

        $total = 0;
        $pendentes = 0;
        $naoPendentes = 0;

        foreach ($contratos as $contrato) {
            $total = (isset($contrato['total'])) ? $contrato['total'] : 0;
            $pendentes = (isset($contrato['com_pendencia'])) ? $contrato['com_pendencia'] : 0;
            $naoPendentes = (isset($contrato['sem_pendencia'])) ? $contrato['sem_pendencia'] : 0;
        }

        if ($total != 0) {
            $porcentagem = (($pendentes / $total) * 100);
        } else {
            $porcentagem = 0;
        }

        $comPendencias = array(
            array(
                'name' => 'Com Pendencias (' . ($pendentes) . ')',
                'y' => $porcentagem,
                'color' => '#FF6600'
            ),
            array(
                'name' => 'Sem Pendencias (' . ($naoPendentes) . ')',
                'y' => (100 - $porcentagem),
                'color' => '#3399CC'
            )
        );

        return json_encode($comPendencias);
    }

    public function contratosComPagamentosEmAtraso()
    {
        $this->autoRender = false;
        $this->loadModel('Contrato');
        $contratos = $this->Contrato->contratosPagamentos();

        $total = 0;
        $atrasados = 0;
        $naoAtrasados = 0;

        foreach ($contratos as $contrato) {
            $total = (isset($contrato['total'])) ? $contrato['total'] : 0;
            $atrasados = (isset($contrato['com_atraso'])) ? $contrato['com_atraso'] : 0;
            $naoAtrasados = (isset($contrato['em_dia'])) ? $contrato['em_dia'] : 0;
            $pagamentoSemDt = $total - ($atrasados + $naoAtrasados);
            $pagamentoSemDt = ($pagamentoSemDt > 0) ? $pagamentoSemDt : 0;
        }

        $json = array();
        array_push($json, array(
            'name' => "Em Dia",
            'y' => ($pagamentoSemDt + $naoAtrasados),
            'color' => '#3399CC'
        ));
        array_push($json, array(
            'name' => "Com Atraso",
            'y' => $atrasados,
            'color' => '#FF6600'
        ));

        return json_encode($json);
    }

    public function contratosAVencer($dataIni, $dataFim)
    {
        $this->autoRender = false;
        $sql = array(
            $this->getInstanceContrato()->find('all', array(
                'fields' => array(
                    'count(Contrato.co_contrato) as total_contratos',
                    'TIMESTAMPDIFF(DAY, current_date, Contrato.dt_fim_vigencia) as dias_para_vencer',
                    '(current_date) as data_atual',

                    // DATA_ATUAL - Contrato.dt_fim_vigencia
                    'Contrato.dt_fim_vigencia as data_fim_vigencia'
                ),
                'conditions' => array(
                    array(
                        'Contrato.dt_fim_vigencia <> ' => null
                    ),
                    array(
                        'Contrato.dt_fim_vigencia > ' => date('Y-m-d')
                    ),
                    array(
                        'DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > ' => $dataIni
                    ),
                    array(
                        'DATEDIFF(Contrato.dt_fim_vigencia, current_date()) < ' => $dataFim
                    )
                )
            ))
        );

        return $sql[0][0][0]['total_contratos'];
    }

    // Methods do 2º Nível
    public function contratosAtivos()
    {

        $this->autoRender = false;
        $conditions = null;

        // Total de Contratos
        $total_de_contratos = array(
            $this->getInstanceContrato()->find('all', array(
                'fields' => array(
                    'count(Contrato.co_contrato) as total_de_contratos'
                ),
                'conditions' => array()
            ))
        );

        if ($this->data['condicao'] == "Ativos") {
            $conditions = array(
                array(
                    'dt_fim_vigencia > ' => date('Y-m-d')
                ),
                array(
                    'Sit.ic_ativo' => 1
                )
            );
        } else
            if ($this->data['condicao'] == "Inativos") {

                $conditions = array(
                    'AND' => array(
                        array(
                            'OR' => array(
                                array(
                                    'dt_fim_vigencia < ' => date('Y-m-d')
                                ),
                                array(
                                    'Sit.ic_ativo ' => 0
                                )
                            )
                        )
                    )
                );
            }
        // Contratos Ativos por Intituicao | 2º Nível
        $sql = array(
            $this->getInstanceContrato()->find('all', array(
                'fields' => array(
                    'Ins.ds_instituicao as instituicao',
                    'count(co_contrato) as quantidade_de_contratos_ativos',
                    '(count(Contrato.co_contrato) * 100) as percentual_de_contratos_ativos',
                    'dt_ini_vigencia as data_inicio_vigencia',
                    'dt_fim_vigencia as data_fim_vigencia',
                    'Ins.co_instituicao id_instituicao'
                ),
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'instituicoes',
                        'alias' => 'Ins',
                        'conditions' => array(
                            'Contrato.co_instituicao = Ins.co_instituicao'
                        )
                    ),
                    array(
                        'table' => 'situacoes',
                        'alias' => 'Sit',
                        'conditions' => array(
                            'Contrato.co_situacao = Sit.co_situacao'
                        )
                    )
                ),
                'group' => 'Ins.ds_instituicao',
                'order' => 'count(Contrato.co_contrato) DESC'
            ))
        );

        $json = array();
        foreach ($sql as $s) {
            foreach ($s as $a) {
                array_push($json, array(
                    "Contrato" => array(
                        "instituicao" => $a['Ins']['instituicao'],
                        "quantidade_de_contratos_ativos" => $a[0]['quantidade_de_contratos_ativos'],
                        "total_de_contratos" => $this->getInstituicao($a['Ins']['id_instituicao']),
                        "percentual_de_contratos_ativos" => round(($a[0]['percentual_de_contratos_ativos'] / $this->getInstituicao($a['Ins']['id_instituicao'])), 1),
                        "percentual_de_contratos_ativos_em_relacao_ao_total_de_contratos" => round(($a[0]['percentual_de_contratos_ativos'] / $total_de_contratos[0][0][0]['total_de_contratos']), 1),
                        "inicio_da_vigencia" => FunctionsComponent::formataData($a['Contrato']['data_inicio_vigencia']),
                        "fim_da_vigencia" => FunctionsComponent::formataData($a['Contrato']['data_fim_vigencia'])
                    )
                ));
            }
        }

        return json_encode($json);
    }

    /**
     * @resource { "name" : "listagem contratos por mês a pagar", "route":"dashboard\/contratos_A_PagarPorMes", "access": "private", "type": "select" }
     */
    public function contratos_A_PagarPorMes()
    {
        $this->autoRender = false;
        $pg = new Pagamento();

        // Contratos Ainda n�o pagos
        $sql = array(
            $pg->find('all', array(
                'fields' => array(
                    'Ins.ds_instituicao as instituicao',
                    '(sum(Pagamento.vl_pagamento) - sum(Pagamento.vl_pago)) as a_pagar',
                    'MONTH(Pagamento.dt_vencimento) as mes',
                    'Pagamento.dt_vencimento as data_de_vencimento'
                ),
                'conditions' => array(
                    array(
                        'YEAR(dt_vencimento)' => date('Y')
                    ),
                    array(
                        'MONTH(Pagamento.dt_vencimento) ' => $this->data['Mes']
                    )
                ),
                'joins' => array(
                    array(
                        'table' => 'contratos',
                        'alias' => 'Contrato',
                        'conditions' => array(
                            'Contrato.co_contrato = Pagamento.co_contrato'
                        )
                    ),
                    array(
                        'table' => 'instituicoes',
                        'alias' => 'Ins',
                        'conditions' => array(
                            'Contrato.co_instituicao = Ins.co_instituicao'
                        )
                    )
                ),
                'group' => 'Ins.ds_instituicao',
                'order' => '(sum(Pagamento.vl_pagamento) - sum(Pagamento.vl_pago)) DESC'
            ))
        );

        // Agrupar por setor, de acordo com o mês
        $json = array();
        foreach ($sql as $s) {
            foreach ($s as $d) {
                array_push($json, array(
                    "Contrato" => array(
                        "instituicao" => $d['Ins']['instituicao'],
                        "a_pagar" => number_format($d['0']['a_pagar'], 2, ',', '.'),
                        "percentual_em_relacao_ao_total_do_mes" => round((FunctionsComponent::validaMoney($d['0']['a_pagar']) * 100) / $this->contratos_financeiro_calcula_percentual($this->data['Mes'], 'a_pagar'), 1),
                        "data_de_vencimento" => FunctionsComponent::formataData($d['Pagamento']['data_de_vencimento'])
                    )
                ));
            }
        }

        return json_encode($json);
    }

    /**
     * @resource { "name" : "listagem contratos pagos por mês ", "route":"dashboard\/contratosPagosPorMes", "access": "private", "type": "select" }
     */
    public function contratosPagosPorMes()
    {
        $this->autoRender = false;
        $pg = new Pagamento();

        // Contratos Ainda não pagos
        $sql = array(
            $pg->find('all', array(
                'fields' => array(
                    'Ins.ds_instituicao as instituicao',
                    'sum(Pagamento.vl_pago) as pago',
                    'MONTH(Pagamento.dt_vencimento) as mes'
                ),
                'conditions' => array(
                    array(
                        'YEAR(dt_vencimento)' => date('Y')
                    ),
                    array(
                        'MONTH(Pagamento.dt_vencimento) ' => $this->data['Mes']
                    )
                ),
                'joins' => array(
                    array(
                        'table' => 'contratos',
                        'alias' => 'Contrato',
                        'conditions' => array(
                            'Contrato.co_contrato = Pagamento.co_contrato'
                        )
                    ),
                    array(
                        'table' => 'instituicoes',
                        'alias' => 'Ins',
                        'conditions' => array(
                            'Contrato.co_instituicao = Ins.co_instituicao'
                        )
                    )
                ),
                'group' => 'Ins.ds_instituicao',
                'order' => 'sum(Pagamento.vl_pago) DESC'
            ))
        );

        // Agrupar por setor, de acordo com o mês
        $json = array();
        foreach ($sql as $s) {
            foreach ($s as $d) {
                array_push($json, array(
                    "Contrato" => array(
                        "instituicao" => $d['Ins']['instituicao'],
                        "pago" => number_format((int)$d['0']['pago'], 2, ',', '.'),
                        "percentual_em_relacao_ao_total_do_mes" => ($this->contratos_financeiro_calcula_percentual($this->data['Mes'], 'pago') != 0) ? round((FunctionsComponent::validaMoney($d['0']['pago']) * 100) / $this->contratos_financeiro_calcula_percentual($this->data['Mes'], 'pago'), 1) : 0
                    )
                ));
            }
        }

        echo json_encode($json);

    }

    /**
     * @resource { "name" : "listagem contratos com pagamentos em atraso por setor", "route":"dashboard\/contratosComPagamentosEmAtrasoPorSetor", "access": "private", "type": "select" }
     */
    public function contratosComPagamentosEmAtrasoPorSetor()
    {
        $this->autoRender = false;
        $pg = new Pagamento();
        $conditions = array();

        // Em Dia
        if ($this->data["condicao"] == "Em Dia") {
            $conditions = array(
                array(
                    'Pagamento.dt_vencimento > ' => date('Y-m-d')
                )
            );
        } else

            // Ainda não pago
            if ($this->data["condicao"] == "Com Atraso") {
                $conditions = array(
                    array(
                        'Pagamento.dt_vencimento < ' => date('Y-m-d')
                    )
                );
            }

        // Número de contratos que estão em atraso
        $totalDeContratos = array(
            $pg->find('all', array(
                'fields' => array(
                    'Instituicao.ds_instituicao',
                    'Instituicao.co_instituicao',
                    'count(Pagamento.co_contrato) as total_de_contratos',
                    'sum(Pagamento.vl_pagamento) as valor_do_pagamento'
                ),
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'contratos',
                        'alias' => 'Contrato',
                        'conditions' => array(
                            'Contrato.co_contrato = Pagamento.co_contrato'
                        )
                    ),
                    array(
                        'table' => 'instituicoes',
                        'alias' => 'Instituicao',
                        'conditions' => array(
                            'Contrato.co_instituicao = Instituicao.co_instituicao'
                        )
                    )
                ),
                'group' => 'Instituicao.ds_instituicao',
                'order' => 'count(Pagamento.co_contrato) DESC'
            ))
        );

        // Número de contratos que estão em atraso
        $geral = array(
            $pg->find('all', array(
                'fields' => array(
                    'count(Contrato.co_contrato) as total_geral'
                ),
                'conditions' => array(),
                'joins' => array(
                    array(
                        'table' => 'contratos',
                        'alias' => 'Contrato',
                        'conditions' => array(
                            'Contrato.co_contrato = Pagamento.co_contrato'
                        )
                    )
                )
            ))
        );

        $json = array();
        foreach ($totalDeContratos as $sql) {
            foreach ($sql as $s) {
                array_push($json, array(
                    "Contrato" => array(
                        "instituicao" => $s['Instituicao']['ds_instituicao'],
                        "quantidade_de_contratos" => (int)$s[0]['total_de_contratos'],
                        "total_de_contratos" => $this->getInstituicao($s['Instituicao']['co_instituicao']),
                        "percentual" => round(($s[0]['total_de_contratos'] * 100) / $geral[0][0][0]['total_geral'], 1),
                        "valor" => FunctionsComponent::validaMoney(number_format($s[0]['valor_do_pagamento'], 2, ',', '.'))
                    )
                ));
            }
        }

        return json_encode($json);
    }

    /**
     * @resource { "name" : "listagem contratos a vencer", "route":"dashboard\/contratosAVencerDetalhe", "access": "private", "type": "select" }
     */
    public function contratosAVencerDetalhe($dataIni, $dataFim)
    {
        $this->autoRender = false;
        $sql = array(
            $this->getInstanceContrato()->find('all', array(
                'fields' => array(
                    'count(Contrato.co_contrato) as total_contratos',
                    'TIMESTAMPDIFF(DAY, current_date, Contrato.dt_fim_vigencia) as dias_para_vencer',
                    '(current_date) as data_atual',
                    'Setor.ds_setor as setor',

                    // DATA_ATUAL - Contrato.dt_fim_vigencia
                    'Contrato.dt_fim_vigencia as data_fim_vigencia'
                ),
                'conditions' => array(
                    array(
                        'Contrato.dt_fim_vigencia <> ' => null
                    ),
                    array(
                        'Contrato.dt_fim_vigencia > ' => date('Y-m-d')
                    ),
                    array(
                        'DATEDIFF(Contrato.dt_fim_vigencia, current_date()) > ' => $dataIni
                    ),
                    array(
                        'DATEDIFF(Contrato.dt_fim_vigencia, current_date()) < ' => $dataFim
                    )
                ),
                array(
                    'table' => 'setores',
                    'alias' => 'Setor',
                    'conditions' => array(
                        'Contrato.co_setor = Setor.co_setor'
                    )
                )
            ))
        );
    }

    // Calculo do percentual do gr�fico de financeiro
    public function contratos_financeiro_calcula_percentual($mes, $tipo)
    {
        $this->autoRender = false;
        $pg = new Pagamento();

        // Contratos Ainda não pagos
        // Conratos Ainda não pagos
        $sql = array(
            $pg->find('all', array(
                'fields' => array(
                    'sum(Pagamento.vl_pagamento) as total',
                    'sum(Pagamento.vl_pago) as pago',
                    '(sum(Pagamento.vl_pagamento) - sum(Pagamento.vl_pago)) as a_pagar',
                    'MONTH(Pagamento.dt_vencimento) as mes'
                ),
                'conditions' => array(
                    array(
                        'YEAR(dt_vencimento)' => date('Y')
                    ),
                    array(
                        'MONTH(dt_vencimento)' => $mes
                    )
                ),
                'joins' => array(
                    array(
                        'table' => 'contratos',
                        'alias' => 'Contrato',
                        'conditions' => array(
                            'Contrato.co_contrato = Pagamento.co_contrato'
                        )
                    )
                )
            ))
        );

        $valor = null;
        if ($tipo == 'pago') {
            $valor = $sql[0][0][0]['pago'];
        } else
            if ('a_pagar') {
                $valor = $sql[0][0][0]['a_pagar'];
            }

        return $valor;
    }

    public function getInstanceContrato()
    {
        return new Contrato();
    }

    public function getInstituicao($id_instituicao)
    {
        $total = array(
            $this->getInstanceContrato()->find('all', array(
                'fields' => array(
                    'count(Contrato.co_contrato) as total_geral',
                    'Ins.ds_instituicao'
                ),
                'conditions' => array(
                    'Ins.co_instituicao ' => $id_instituicao
                ),
                'joins' => array(
                    array(
                        'table' => 'instituicoes',
                        'alias' => 'Ins',
                        'conditions' => array(
                            'Contrato.co_instituicao = Ins.co_instituicao'
                        )
                    )
                )
            ))
        );

        return $total[0][0][0]['total_geral'];
    }

    /**
     * @resource { "name" : "B.I.", "route":"dashboard\/indexBi", "access": "private", "type": "select" }
     */
    public function indexBi()
    {
    }

    /**
     * @resource { "name" : "Extrator Importação", "route":"dashboard\/indexExtratorImportacao", "access": "private", "type": "select" }
     */
    public function indexExtratorImportacao()
    {
    }

    /**
     * @resource { "name" : "Extrator Geração", "route":"dashboard\/indexExtratorGeracao", "access": "private", "type": "select" }
     */
    public function indexExtratorGeracao()
    {
    }

    /**
     * @resource { "name" : "Extrator Auditoria", "route":"dashboard\/indexExtratorAuditoria", "access": "private", "type": "select" }
     */
    public function indexExtratorAuditoria()
    {
    }

}

?>
