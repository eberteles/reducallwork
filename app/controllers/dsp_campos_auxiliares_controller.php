<?php

class DspCamposAuxiliaresController extends AppController {

    public $name = 'DspCamposAuxiliares';

    public $uses = array(
        'DspCamposAuxiliares'
    );

    function index() {
        $this->loadModel('DspCargos');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');
        $banco = null;

        if( !empty($this->data) ){
            $criteria['ic_ativo'] = 2;
            if( $this->data['CamposAuxiliares']['tipo_campo'] == 1 ){
                $this->set('banco', 'unidade');
                if ($this->data['CamposAuxiliares']['descricao_campo']) {
                    $criteria['nome_unidade like'] = '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%';
                    $this->set('campos_auxiliares', $this->DspUnidadeLotacao->find('all', array(
                        'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2, 'DspUnidadeLotacao.nome_unidade like' => '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%'),
                        $this->paginate($criteria)
                    )));
                }else{
                    $this->set('campos_auxiliares', $this->DspUnidadeLotacao->find('all', array(
                        'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2),
                        $this->paginate($criteria)
                    )));
                }
            }elseif( $this->data['CamposAuxiliares']['tipo_campo'] == 2 ){
                $this->set('banco', 'cargo');
                if ($this->data['CamposAuxiliares']['descricao_campo']) {
                    $criteria['nome_cargo like'] = '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%';
                    $this->set('campos_auxiliares', $this->DspCargos->find('all', array(
                        'conditions' => array('DspCargos.ic_ativo' => 2, 'DspCargos.nome_cargo like' => '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%'),
                        $this->paginate('DspCargos', $criteria)
                    )));
                }else{
                    $this->set('campos_auxiliares', $this->DspCargos->find('all', array(
                        'conditions' => array('DspCargos.ic_ativo' => 2),
                        $this->paginate('DspCargos', $criteria)
                    )));
                }
            }elseif( $this->data['CamposAuxiliares']['tipo_campo'] == 3 ){
                $this->set('banco', 'funcao');
                if ($this->data['CamposAuxiliares']['descricao_campo']) {
                    $criteria['nome_cargo like'] = '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%';
                    $this->set('campos_auxiliares', $this->DspFuncao->find('all', array(
                        'conditions' => array('DspFuncao.ic_ativo' => 2, 'DspFuncao.nome_funcao like' => '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%'),
                        $this->paginate('DspFuncao', $criteria)
                    )));
                }else{
                    $this->set('campos_auxiliares', $this->DspFuncao->find('all', array(
                        'conditions' => array('DspFuncao.ic_ativo' => 2),
                        $this->paginate('DspFuncao', $criteria)
                    )));
                }
            }elseif( $this->data['CamposAuxiliares']['tipo_campo'] == 4 ){
                $this->set('banco', 'meio_transporte');
                if ($this->data['CamposAuxiliares']['descricao_campo']) {
                    $criteria['nome_meio_transporte like'] = '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%';
                    $this->set('campos_auxiliares', $this->DspMeioTransporte->find('all', array(
                        'conditions' => array('DspMeioTransporte.ic_ativo' => 2, 'DspMeioTransporte.nome_meio_transporte like' => '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%'),
                        $this->paginate('DspMeioTransporte', $criteria)
                    )));
                }else{
                    $this->set('campos_auxiliares', $this->DspMeioTransporte->find('all', array(
                        'conditions' => array('DspMeioTransporte.ic_ativo' => 2),
                        $this->paginate('DspMeioTransporte', $criteria)
                    )));
                }
            }elseif( $this->data['CamposAuxiliares']['tipo_campo'] == 5 ){
                $this->set('banco', 'categoria_passagem');
                if ($this->data['CamposAuxiliares']['descricao_campo']) {
                    $criteria['nome_categoria_passagem like'] = '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%';
                    $this->set('campos_auxiliares', $this->DspCategoriaPassagem->find('all', array(
                        'conditions' => array('DspCategoriaPassagem.ic_ativo' => 2, 'DspCategoriaPassagem.nome_categoria_passagem like' => '%' . up($this->data['CamposAuxiliares']['descricao_campo']) . '%'),
                        $this->paginate('DspCategoriaPassagem', $criteria)
                    )));
                }else{
                    $this->set('campos_auxiliares', $this->DspCategoriaPassagem->find('all', array(
                        'conditions' => array('DspCategoriaPassagem.ic_ativo' => 2),
                        $this->paginate('DspCategoriaPassagem', $criteria)
                    )));
                }
            }
        }else{
            $criteria['ic_ativo'] = 2;
            $this->set('banco', 'unidade');
            $this->set('campos_auxiliares', $this->DspUnidadeLotacao->find('all', array(
                'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2),
                $this->paginate($criteria)
            )));
        }
    }

    public function add(){
        $this->loadModel('DspCargos');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');
        $banco = null;

        if (!empty($this->data['DspCamposAuxiliares']['tipo_campo']) && !empty($this->data['DspCamposAuxiliares']['descricao_campo'])) {
            if( $this->data['DspCamposAuxiliares']['tipo_campo'] == 1 ){
                $this->DspUnidadeLotacao->create();
                $this->data['DspCamposAuxiliares']['nome_unidade'] = $this->data['DspCamposAuxiliares']['descricao_campo'];
                if( $this->DspUnidadeLotacao->save($this->data['DspCamposAuxiliares']) ){
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }else{
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }elseif( $this->data['DspCamposAuxiliares']['tipo_campo'] == 2 ){
                $this->DspCargos->create();
                $this->data['DspCamposAuxiliares']['nome_cargo'] = $this->data['DspCamposAuxiliares']['descricao_campo'];
                if( $this->DspCargos->save($this->data['DspCamposAuxiliares']) ){
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }else{
                    debug($this->DspCargos->ValidationErrors());die;
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }elseif( $this->data['DspCamposAuxiliares']['tipo_campo'] == 3 ){
                $this->DspFuncao->create();
                $this->data['DspCamposAuxiliares']['nome_funcao'] = $this->data['DspCamposAuxiliares']['descricao_campo'];
                if( $this->DspFuncao->save($this->data['DspCamposAuxiliares']) ){
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }else{
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }elseif( $this->data['DspCamposAuxiliares']['tipo_campo'] == 4 ){
                $this->DspMeioTransporte->create();
                $this->data['DspCamposAuxiliares']['nome_meio_transporte'] = $this->data['DspCamposAuxiliares']['descricao_campo'];
                if( $this->DspMeioTransporte->save($this->data['DspCamposAuxiliares']) ){
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }else{
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }elseif( $this->data['DspCamposAuxiliares']['tipo_campo'] == 5 ){
                $this->DspCategoriaPassagem->create();
                $this->data['DspCamposAuxiliares']['nome_categoria_passagem'] = $this->data['DspCamposAuxiliares']['descricao_campo'];
                if( $this->DspCategoriaPassagem->save($this->data['DspCamposAuxiliares']) ){
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }else{
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->DspCamposAuxiliares->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspCamposAuxiliares->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->DspCamposAuxiliares->read(null, $id)) {
            $campos_auxiliares = $this->DspCamposAuxiliares->read(null, $id);
            $msg = null;
            $campos_auxiliares['DspCamposAuxiliares']['ic_ativo'] = 1;

            if($this->DspCamposAuxiliares->save($campos_auxiliares)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspCamposAuxiliares->find ( 'list' ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $campos_auxiliares )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_campos_auxiliares' ) );
    }

}
?>
