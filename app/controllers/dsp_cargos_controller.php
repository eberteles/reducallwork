<?php

class DspCargosController extends AppController {
    
    public $name = 'DspCargos';

    public $uses = array(
        'DspCargos'
    );

    /*
     * function index() -> load all 'cargos' in database
     * set variable 'cargos' in view
     */
    function index() {
        // CARREGA TODOS OS CARGOS E COLOCA NA VIEW
        $cargos = $this->DspCargos->find('all');
        $this->set('cargos', $cargos);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->DspCargos->create();
            if ($this->DspCargos->save($this->data['DspCargos'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspCargos->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_cargo = $this->data['DspCargos']['descricao'];
            $this->data = $this->DspCargos->read(null, $id);
            $this->data['DspCargos']['nome_cargo'] = $nome_cargo;
            if ($this->DspCargos->save($this->data['DspCargos'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspCargos->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }

        if ($this->DspCargos->read(null, $id)) {
            $cargos = $this->DspCargos->read(null, $id);
            $msg = null;
            $cargos['DspCargos']['ic_ativo'] = 1;

            if($this->DspCargos->save($cargos)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspCargos->find ( 'list', array(
            'conditions' => array('DspCargos.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_cargo )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_cargo' ) );
    }

}
?>
