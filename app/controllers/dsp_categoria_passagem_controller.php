<?php

class DspCategoriaPassagemController extends AppController {
    
    public $name = 'DspCategoriaPassagem';

    public $uses = array(
        'DspCategoriaPassagem'
    );

    /*
     * function index() -> load all 'categoria_passagem' in database
     * set variable 'categoria_passagem' in view
     */
    function index() {
        // CARREGA TODOS OS CATEGORIAS DE PASSAGEM E COLOCA NA VIEW
        $categoria_passagem = $this->DspCategoriaPassagem->find('all');
        $this->set('categoria_passagem', $categoria_passagem);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->DspCategoriaPassagem->create();
            if ($this->DspCategoriaPassagem->save($this->data['DspCategoriaPassagem'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspCategoriaPassagem->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_categoria = $this->data['DspCategoriaPassagem']['descricao'];
            $this->data = $this->DspCategoriaPassagem->read(null, $id);
            $this->data['DspCategoriaPassagem']['nome_categoria_passagem'] = $nome_categoria;
            if ($this->DspCategoriaPassagem->save($this->data['DspCategoriaPassagem'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspCategoriaPassagem->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }

        if ($this->DspCategoriaPassagem->read(null, $id)) {
            $categoria_passagem = $this->DspCategoriaPassagem->read(null, $id);
            $msg = null;
            $categoria_passagem['DspCategoriaPassagem']['ic_ativo'] = 1;

            if($this->DspCategoriaPassagem->save($categoria_passagem)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspCategoriaPassagem->find ( 'list', array(
            'conditions' => array('DspCategoriaPassagem.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_categoria_passagem )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_categoria_passagem' ) );
    }

}
?>
