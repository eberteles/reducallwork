<?php

class DspDespesasDeViagemController extends AppController {

    public $name = 'DspDespesasDeViagem';

    public $uses = array(
        'DspDespesasDeViagem'
    );

    /*
     * function index() -> load all 'despesas_viagem' in database
     * set variable 'despesas_viagem' in view
     */
    function index() {

        if( !empty($this->data) ){
            if ($this->data['Despesa']['numero_processo']) {
                $criteria['numero_processo like'] = '%' . up($this->data['Despesa']['numero_processo']) . '%';
            }
            if ($this->data['Despesa']['nome_funcionario']) {
                $criteria['GrupoAuxiliar.ds_nome like'] = '%' . up($this->data['Despesa']['nome_funcionario']) . '%';
            }
            if ($this->data['Despesa']['matricula_funcionario']) {
                $criteria['GrupoAuxiliar.nu_cpf like'] = '%' . up($this->data['Despesa']['matricula_funcionario']) . '%';
            }
            $criteria['DspDespesasDeViagem.ic_ativo'] = 2;
            $this->set('despesas_viagem', $this->paginate($criteria));
        }
//        else{
//            // CARREGA TODOS OS DESPESAS DE VIAGEM E COLOCA NA VIEW
//            $criteria['ic_ativo'] = 2;
//            $this->set('despesas_viagem', $this->paginate($criteria));
//        }
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->data['DspDespesasDeViagem'] = array_map('strtoupper', $this->data['DspDespesasDeViagem']);
            if( $this->data['DspDespesasDeViagem']['dt_inicio'] == '--' || $this->data['DspDespesasDeViagem']['dt_inicio'] == '' ){
                unset($this->data['DspDespesasDeViagem']['dt_inicio']);
            }
            if( $this->data['DspDespesasDeViagem']['dt_final'] == '--' || $this->data['DspDespesasDeViagem']['dt_final'] == '' ){
                unset($this->data['DspDespesasDeViagem']['dt_final']);
            }
            $this->DspDespesasDeViagem->create();
            if ($this->DspDespesasDeViagem->save($this->data['DspDespesasDeViagem'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspDespesasDeViagem->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                //debug($this->DspDespesasDeViagem->validationErrors);die;
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        App::import('Model', 'Uf');
        $ufDb   = new Uf();
        $this->set('ufs', $ufDb->find('list'));

        $this->loadModel('DspCargos');
        $cargos = $this->DspCargos->find('list', array(
            'conditions' => array('DspCargos.ic_ativo' => 2)
        ));
        $this->set(compact('cargos'));

        $this->loadModel('DspFuncao');
        $funcao = $this->DspFuncao->find('list', array(
            'conditions' => array('DspFuncao.ic_ativo' => 2)
        ));
        $this->set(compact('funcao'));

        $this->loadModel('DspUnidadeLotacao');
        $unidade = $this->DspUnidadeLotacao->find('list', array(
            'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2)
        ));
        $this->set(compact('unidade'));

        $this->loadModel('DspMeioTransporte');
        $meio_transporte = $this->DspMeioTransporte->find('list', array(
            'conditions' => array('DspMeioTransporte.ic_ativo' => 2)
        ));
        $this->set(compact('meio_transporte'));

        $this->loadModel('DspCategoriaPassagem');
        $categoria_passagem = $this->DspCategoriaPassagem->find('list', array(
            'conditions' => array('DspCategoriaPassagem.ic_ativo' => 2)
        ));
        $this->set(compact('categoria_passagem'));
        
        $this->set( 'funcionarios', $this->DspDespesasDeViagem->GrupoAuxiliar->find('list', array('order' => 'ds_nome ASC')) );

        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->data['DspDespesasDeViagem']['co_despesas_viagem'] = $id;
            if ($this->DspDespesasDeViagem->save($this->data['DspDespesasDeViagem'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                debug($this->DspDespesasDeViagem->validationErrors);die;
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                $this->redirect(array(
                    'action' => 'edit' . '/' . $id
                ));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspDespesasDeViagem->read(null, $id);
        }
        $this->set('id', $id);

        App::import('Model', 'Uf');
        $ufDb   = new Uf();
        $this->set('ufs', $ufDb->find('list'));

        $this->loadModel('DspCargos');
        $cargos = $this->DspCargos->find('list', array(
            'conditions' => array('DspCargos.ic_ativo' => 2)
        ));
        $this->set(compact('cargos'));

        $this->loadModel('DspFuncao');
        $funcao = $this->DspFuncao->find('list', array(
            'conditions' => array('DspFuncao.ic_ativo' => 2)
        ));
        $this->set(compact('funcao'));

        $this->loadModel('DspUnidadeLotacao');
        $unidade = $this->DspUnidadeLotacao->find('list', array(
            'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2)
        ));
        $this->set(compact('unidade'));

        $this->loadModel('DspMeioTransporte');
        $meio_transporte = $this->DspMeioTransporte->find('list', array(
            'conditions' => array('DspMeioTransporte.ic_ativo' => 2)
        ));
        $this->set(compact('meio_transporte'));

        $this->loadModel('DspCategoriaPassagem');
        $categoria_passagem = $this->DspCategoriaPassagem->find('list', array(
            'conditions' => array('DspCategoriaPassagem.ic_ativo' => 2)
        ));
        $this->set(compact('categoria_passagem'));
        
        $this->set( 'funcionarios', $this->DspDespesasDeViagem->GrupoAuxiliar->find('list', array('order' => 'ds_nome ASC')) );
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->DspDespesasDeViagem->read(null, $id)) {
            $despesas_viagem = $this->DspDespesasDeViagem->read(null, $id);
            $msg = null;
            $despesas_viagem['DspDespesasDeViagem']['ic_ativo'] = 1;

            if($this->DspDespesasDeViagem->save($despesas_viagem)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    function mostrar( $id = null )
    {
        $despesas = $this->DspDespesasDeViagem->find('all', array(
            'conditions' => array('DspDespesasDeViagem.co_despesas_viagem' => $id)
        ));

        $this->set('despesa', $this->DspDespesasDeViagem->find('all', array(
            'conditions' => array('DspDespesasDeViagem.ic_ativo' => 2, 'DspDespesasDeViagem.co_despesas_viagem' => $id)
        )));

        $this->loadModel('DspCargos');;
        $this->set('cargos', $this->DspCargos->find('list', array(
            'conditions' => array('DspCargos.co_cargo' => $despesas[0]['DspDespesasDeViagem']['cod_cargo'])
        )));

        $this->loadModel('DspFuncao');
        $this->set('funcao', $this->DspFuncao->find('list', array(
            'conditions' => array('DspFuncao.co_funcao' => $despesas[0]['DspDespesasDeViagem']['cod_funcao'])
        )));

        $this->loadModel('DspUnidadeLotacao');
        $this->set('unidade', $this->DspUnidadeLotacao->find('list', array(
            'conditions' => array('DspUnidadeLotacao.co_unidade' => $despesas[0]['DspDespesasDeViagem']['cod_unidade_lotacao'])
        )));

        $this->loadModel('DspMeioTransporte');
        $this->set('meio_transporte', $this->DspMeioTransporte->find('list', array(
            'conditions' => array('DspMeioTransporte.co_meio_transporte' => $despesas[0]['DspDespesasDeViagem']['cod_meio_transporte'])
        )));

        $this->loadModel('DspCategoriaPassagem');
        $this->set('categoria_passagem', $this->DspCategoriaPassagem->find('list', array(
            'conditions' => array('DspCategoriaPassagem.co_categoria_passagem' => $despesas[0]['DspDespesasDeViagem']['cod_categoria_passagem'])
        )));
    }

    function imprimirViagem( $id = null ){
        $viagem = $this->DspDespesasDeViagem->read(null, $id);

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Despesa de Viagem</b>';

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'PROCESSO'
        ), null, array(
            'colspan' => 2
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Número do Processo</b>',
                '<b>Motivo da Viagem</b>'
            )
            ), array(
                'class' => 'altrow'
            ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                $viagem['DspDespesasDeViagem']['numero_processo'],
                $viagem['DspDespesasDeViagem']['motivo_viagem']
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'FUNCIONÁRIO'
        ), null, array(
            'colspan' => 5
        ));

        $this->loadModel('DspCargos');
        $cargos = $this->DspCargos->find('list', array(
            'conditions' => array('DspCargos.co_cargo' => $viagem['DspDespesasDeViagem']['cod_cargo'])
        ));
        $cargos = array_values($cargos);

        $this->loadModel('DspUnidadeLotacao');
        $unidade = $this->DspUnidadeLotacao->find('list', array(
            'conditions' => array('DspUnidadeLotacao.co_unidade' => $viagem['DspDespesasDeViagem']['cod_unidade_lotacao'])
        ));
        $unidade = array_values($unidade);

        $this->loadModel('DspFuncao');
        $funcao = $this->DspFuncao->find('list', array(
            'conditions' => array('DspFuncao.co_funcao' => $viagem['DspDespesasDeViagem']['cod_funcao'])
        ));
        $funcao = array_values($funcao);

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Unidade de Lotação</b>',
                '<b>Nome do Funcionário</b>',
                '<b>Matrícula do Funcionário</b>',
                '<b>Cargo</b>',
                '<b>Função</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                $unidade[0],
                $viagem['GrupoAuxiliar']['ds_nome'],
                $viagem['GrupoAuxiliar']['nu_cpf'],
                $cargos[0],
                $funcao[0]
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'DADOS DA VIAGEM'
        ), null, array(
            'colspan' => 6
        ));

        $this->loadModel('DspMeioTransporte');
        $meio_transporte = $this->DspMeioTransporte->find('list', array(
            'conditions' => array('DspMeioTransporte.co_meio_transporte' => $viagem['DspDespesasDeViagem']['cod_meio_transporte'])
        ));
        $meio_transporte = array_values($meio_transporte);

        $this->loadModel('DspCategoriaPassagem');
        $categoria_passagem = $this->DspCategoriaPassagem->find('list', array(
            'conditions' => array('DspCategoriaPassagem.co_categoria_passagem' => $viagem['DspDespesasDeViagem']['cod_categoria_passagem'])
        ));
        $categoria_passagem = array_values($categoria_passagem);

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Origem</b>',
                '<b>Destino</b>',
                '<b>Data Início</b>',
                '<b>Data Final</b>',
                '<b>Meio de Transporte</b>',
                '<b>Categoria de Passagem</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $origem  = $viagem['DspDespesasDeViagem']['uf_origem'] ?: $viagem['DspDespesasDeViagem']['pais_origem'];
        $destino = $viagem['DspDespesasDeViagem']['uf_destino'] ?: $viagem['DspDespesasDeViagem']['pais_destino'];

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                $viagem['DspDespesasDeViagem']['origem'] . ' - ' . up($origem),
                $viagem['DspDespesasDeViagem']['destino'] . ' - ' . up($destino),
                $viagem['DspDespesasDeViagem']['dt_inicio'],
                $viagem['DspDespesasDeViagem']['dt_final'],
                $meio_transporte[0],
                $categoria_passagem[0]
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'VALORES'
        ), null, array(
            'colspan' => 4
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Valor da Passagem</b>',
                '<b>Número de Diárias</b>',
                '<b>Valor Total das Diárias</b>',
                '<b>Valor Total da Viagem</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                'R$ ' . $viagem['DspDespesasDeViagem']['valor_passagem'],
                $viagem['DspDespesasDeViagem']['numero_diarias'],
                'R$ ' . $viagem['DspDespesasDeViagem']['valor_total_diarias'],
                'R$ ' . $viagem['DspDespesasDeViagem']['valor_total_viagem']
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimir($viagemHtml, 'P', $titulo, false);
    }

    function preImprimir(){
        $this->set( 'funcionarios', $this->DspDespesasDeViagem->GrupoAuxiliar->find('list', array('order' => 'ds_nome ASC')) );
    }

    function imprimirRelatorioGeralHtml( $ano = null ){
        $this->layout = 'dsp_relatorio_html';

        $this->loadModel('DspCargos');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');

        if( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' ){
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'DspDespesasDeViagem.dt_inicio >=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_inicio']))),
                    'DspDespesasDeViagem.dt_final <=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_final'])))
                )
            ));
        }elseif( $ano != null ){
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'MONTHNAME(DspDespesasDeViagem.dt_inicio)' => $ano
                )
            ));
        }else{

        }

        $unidade = $this->DspUnidadeLotacao->find('all');
        $cargo = $this->DspCargos->find('all');
        $funcao = $this->DspFuncao->find('all');
        $meio_transporte = $this->DspMeioTransporte->find('all');
        $categoria = $this->DspCategoriaPassagem->find('all');

        $this->set('unidade', $unidade);
        $this->set('cargo', $cargo);
        $this->set('funcao', $funcao);
        $this->set('meio_transporte', $meio_transporte);
        $this->set('categoria', $categoria);
        $this->set('despesas', $despesas);
    }

    function imprimirRelatorioGeral( $mes = null , $dt_inicio = null , $dt_final = null ){

        $this->loadModel('DspCargos');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');

        if( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' && $this->data['PreImprimir']['nome_funcionario'] == '' ) {
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'DspDespesasDeViagem.dt_inicio >=' => implode("-", array_reverse(explode("/", $this->data['PreImprimir']['dt_inicio']))),
                    'DspDespesasDeViagem.dt_final <=' => implode("-", array_reverse(explode("/", $this->data['PreImprimir']['dt_final'])))
                )
            ));
        }elseif( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' && $this->data['PreImprimir']['nome_funcionario'] != '' ) {
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'DspDespesasDeViagem.dt_inicio >=' => implode("-", array_reverse(explode("/", $this->data['PreImprimir']['dt_inicio']))),
                    'DspDespesasDeViagem.dt_final <=' => implode("-", array_reverse(explode("/", $this->data['PreImprimir']['dt_final']))),
                    'DspDespesasDeViagem.co_funcionario' => $this->data['PreImprimir']['nome_funcionario']
                )
            ));
        }elseif( $this->data['PreImprimir']['nome_funcionario'] != '' ){
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'DspDespesasDeViagem.co_funcionario' => $this->data['PreImprimir']['nome_funcionario']
                )
            ));
        }elseif( $mes != null && $mes != 'geral' ){
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'MONTHNAME(DspDespesasDeViagem.dt_inicio)' => $mes
                )
            ));
        }elseif( $dt_inicio != null && $dt_final != null ){
            $despesas = $this->DspDespesasDeViagem->find('all', array(
                'conditions' => array(
                    'DspDespesasDeViagem.ic_ativo' => 2,
                    'DspDespesasDeViagem.dt_inicio >=' => $dt_inicio,
                    'DspDespesasDeViagem.dt_final <=' => $dt_final
                )
            ));
        }else{

        }

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Diárias e Passagens</b>';

        if( !empty($despesas) ) {
            $viagemHtml .= $html->tag('table', null, array(
                'style' => 'width: 100%; position: relative;',
                'class' => 'table table-bordered table-striped',
                'cellspacing' => '0',
                'cellpadding' => '0'
            ));

            $viagemHtml .= $html->tag('thead');

            $viagemHtml .= $html->tableCells(array(
                array(
                    '<b>UNIDADE DE LOTAÇÃO</b>',
                    '<b>NOME / MATRÍCULA</b>',
                    '<b>CARGO / FUNÇÃO</b>',
                    '<b>ORIGEM</b>',
                    '<b>DESTINO</b>',
                    '<b>INÍCIO PERÍODO</b>',
                    '<b>FIM PERÍODO</b>',
                    '<b>MOTIVO</b>',
                    '<b>MEIO DE TRANSPORTE</b>',
                    '<b>CAT. DA PASSAGEM</b>',
                    '<b>VALOR DA PASSAGEM</b>',
                    '<b>Nº DE DIÁRIAS</b>',
                    '<b>VALOR TOTAL DAS DIÁRIAS</b>',
                    '<b>VALOR TOTAL DA VIAGEM</b>',
                    '<b>PROCESSO</b>'
                )
            ), array(
                'class' => 'altrow'
            ));

            $viagemHtml .= $html->tag('/thead');
            $viagemHtml .= $html->tag('tbody');

            foreach ($despesas as $despesa) :
                $unidade = $this->DspUnidadeLotacao->findByCoUnidade($despesa['DspDespesasDeViagem']['cod_unidade_lotacao']);
                $cargo = $this->DspCargos->findByCoCargo($despesa['DspDespesasDeViagem']['cod_cargo']);
                $funcao = $this->DspFuncao->findByCoFuncao($despesa['DspDespesasDeViagem']['cod_funcao']);
                $meio_transporte = $this->DspMeioTransporte->findByCoMeioTransporte($despesa['DspDespesasDeViagem']['cod_meio_transporte']);
                $categoria = $this->DspCategoriaPassagem->findByCoCategoriaPassagem($despesa['DspDespesasDeViagem']['cod_categoria_passagem']);

                $valor_passagem = number_format($despesa['DspDespesasDeViagem']['valor_passagem'], 2, ',', '.');
                $valor_total_diarias = number_format($despesa['DspDespesasDeViagem']['valor_total_diarias'], 2, ',', '.');
                $valor_total_viagem = number_format($despesa['DspDespesasDeViagem']['valor_total_viagem'], 2, ',', '.');
                $numero_diarias = number_format($despesa['DspDespesasDeViagem']['numero_diarias'], 1, ',', '');

                $origem  = $despesa['DspDespesasDeViagem']['uf_origem'] ?: $despesa['DspDespesasDeViagem']['pais_origem'];
                $destino = $despesa['DspDespesasDeViagem']['uf_destino'] ?: $despesa['DspDespesasDeViagem']['pais_destino'];

                $viagemHtml .= $html->tableCells(array(
                    array(
                        $unidade['DspUnidadeLotacao']['nome_unidade'],
                        $despesa['GrupoAuxiliar']['ds_nome'] . ' / ' . $despesa['GrupoAuxiliar']['nu_cpf'],
                        $cargo['DspCargos']['nome_cargo'] . ' / ' . $funcao['DspFuncao']['nome_funcao'],
                        $despesa['DspDespesasDeViagem']['origem'] . ' / ' . $origem,
                        $despesa['DspDespesasDeViagem']['destino'] . ' / ' . $destino,
                        $despesa['DspDespesasDeViagem']['dt_inicio'],
                        $despesa['DspDespesasDeViagem']['dt_final'],
                        $despesa['DspDespesasDeViagem']['motivo_viagem'],
                        $meio_transporte['DspMeioTransporte']['nome_meio_transporte'],
                        $categoria['DspCategoriaPassagem']['nome_categoria_passagem'],
                        'R$ ' . $valor_passagem,
                        $numero_diarias,
                        'R$ ' . $valor_total_diarias,
                        'R$ ' . $valor_total_viagem,
                        $despesa['DspDespesasDeViagem']['numero_processo']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;

            $viagemHtml .= $html->tag('/tbody');
            $viagemHtml .= $html->tag('/table');

        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }

    function listar() {

        echo json_encode ( $this->DspDespesasDeViagem->find ( 'list' ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $despesas_viagem )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_despesas_viagem' ) );
    }

    function formatMoney() {
        $valor = formatMoney($this->params['form']['total']);

        echo json_encode( $valor );
        exit();
    }
}
?>
