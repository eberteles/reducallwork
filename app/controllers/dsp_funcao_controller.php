<?php

class DspFuncaoController extends AppController {
    
    public $name = 'DspFuncao';

    public $uses = array(
        'DspFuncao'
    );

    /*
     * function index() -> load all 'cargos' in database
     * set variable 'cargos' in view
     */
    function index() {
        // CARREGA TODOS AS FUNÇÕES E COLOCA NA VIEW
        $funcao = $this->DspFuncao->find('all');
        $this->set('funcao', $funcao);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->DspFuncao->create();
            if ($this->DspFuncao->save($this->data['DspFuncao'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspFuncao->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_funcao = $this->data['DspFuncao']['descricao'];
            $this->data = $this->DspFuncao->read(null, $id);
            $this->data['DspFuncao']['nome_funcao'] = $nome_funcao;
            if ($this->DspFuncao->save($this->data['DspFuncao'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspFuncao->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }

        if ($this->DspFuncao->read(null, $id)) {
            $funcao = $this->DspFuncao->read(null, $id);
            $msg = null;
            $funcao['DspFuncao']['ic_ativo'] = 1;

            if($this->DspFuncao->save($funcao)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspFuncao->find ( 'list', array(
            'conditions' => array('DspFuncao.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_funcao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_funcao' ) );
    }

}
?>
