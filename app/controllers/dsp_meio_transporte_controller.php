<?php

class DspMeioTransporteController extends AppController {
    
    public $name = 'DspMeioTransporte';

    public $uses = array(
        'DspMeioTransporte'
    );

    /*
     * function index() -> load all 'meio_transporte' in database
     * set variable 'meio_transporte' in view
     */
    function index() {
        // CARREGA TODOS OS MEIOS DE TRANSPORTE E COLOCA NA VIEW
        $meio_transporte = $this->DspMeioTransporte->find('all');
        $this->set('meio_transporte', $meio_transporte);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->DspMeioTransporte->create();
            if ($this->DspMeioTransporte->save($this->data['DspMeioTransporte'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspMeioTransporte->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_transporte = $this->data['DspMeioTransporte']['descricao'];
            $this->data = $this->DspMeioTransporte->read(null, $id);
            $this->data['DspMeioTransporte']['nome_meio_transporte'] = $nome_transporte;
            if ($this->DspMeioTransporte->save($this->data['DspMeioTransporte'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspMeioTransporte->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }

        if ($this->DspMeioTransporte->read(null, $id)) {
            $meio_transporte = $this->DspMeioTransporte->read(null, $id);
            $msg = null;
            $meio_transporte['DspMeioTransporte']['ic_ativo'] = 1;

            if($this->DspMeioTransporte->save($meio_transporte)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',

                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspMeioTransporte->find ( 'list', array(
            'conditions' => array('DspMeioTransporte.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_meio_transporte )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_meio_transporte' ) );
    }

}
?>
