<?php

class DspRelatorioController extends AppController
{

    public $name = 'DspRelatorio';

    public $uses = array(
        'DspRelatorio'
    );

    function index()
    {
        $this->layout = 'dsp_relatorio';

        $anos = $this->DspRelatorio->find('all', array(
            'fields' => array(
                'YEAR(dt_inicio)', 'YEAR(dt_final)'
            ),
            'conditions' => array(
                'DspRelatorio.ic_ativo' => 2
            )
        ));

        $array_anos = array();
        foreach( $anos as $ano ){
            array_push($array_anos, $ano[0]['YEAR(dt_inicio)']);
            array_push($array_anos, $ano[0]['YEAR(dt_final)']);
        }

        if( !empty($array_anos) ){
            $array_final = array_combine($array_anos, $array_anos);
            $this->set('anos', array_unique( $array_final ) );
        }else{
            $this->set('anos', array() );
        }

        $this->loadModel('GrupoAuxiliar');
        $funcionarios = $this->GrupoAuxiliar->find('list',array(
            'conditions' => array(
                'GrupoAuxiliar.ic_ativo' => 1,
            ),
            'fields' => array(
                'GrupoAuxiliar.co_usuario','GrupoAuxiliar.ds_nome'
            )
        ));

        $this->set('funcionarios', $funcionarios );
    }

    function listar( $ano = null )
    {
        if( $ano != null ){
            $meses = $this->DspRelatorio->find ( 'all', array(
                'fields' => array(
                    'MONTHNAME(dt_inicio)', 'MONTHNAME(dt_final)'
                ),
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'YEAR(DspRelatorio.dt_inicio)' => $ano,
                    'YEAR(DspRelatorio.dt_final)' => $ano
                )
            ) );
        }else{
            $meses = $this->DspRelatorio->find ( 'all', array(
                'fields' => array(
                    'MONTHNAME(dt_inicio)', 'MONTHNAME(dt_final)'
                ),
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2
                )
            ) );
        }

        $counter = 0;
        $array_meses = array();
        while( next( $meses ) ){
            array_push($array_meses, $meses[$counter][0]['MONTHNAME(dt_inicio)']);
            array_push($array_meses, $meses[$counter][0]['MONTHNAME(dt_final)']);
            $counter++;
        }

        $array_months = $array_meses;

        $cnt = 0;
        foreach( $array_meses as $ar ){
            if( $ar == 'January' ){
                $mes = 'Janeiro';
            }elseif( $ar == 'February' ){
                $mes = 'Fevereiro';
            }elseif( $ar == 'March' ){
                $mes = 'Março';
            }elseif( $ar == 'April' ){
                $mes = 'Abril';
            }elseif( $ar == 'May' ){
                $mes = 'Maio';
            }elseif( $ar == 'June' ){
                $mes = 'Junho';
            }elseif( $ar == 'July' ){
                $mes = 'Julho';
            }elseif( $ar == 'August' ){
                $mes = 'Agosto';
            }elseif( $ar == 'September' ){
                $mes = 'Setembro';
            }elseif( $ar == 'October' ){
                $mes = 'Outubro';
            }elseif( $ar == 'November' ){
                $mes = 'Novembro';
            }elseif( $ar == 'December' ){
                $mes = 'Dezembro';
            }
            $array_meses[$cnt] = $mes;
            $cnt++;
        }

        $array_final = array_combine($array_months, $array_meses);

        echo json_encode ( array_unique( $array_final ) );

        exit ();
    }

    function imprimirRelatorioGeralHtml( $ano = null ){
        $this->layout = 'dsp_relatorio_html';

        $this->loadModel('DspCargos');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');
        $this->loadModel('GrupoAuxiliar');

        if( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'DspRelatorio.dt_inicio >=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_inicio']))),
                    'DspRelatorio.dt_final <=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_final'])))
                )
            ));
        }elseif( $ano != null ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'MONTHNAME(DspRelatorio.dt_inicio)' => $ano
                )
            ));
        }elseif( $this->data['PreImprimir']['nome_funcionario'] ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'DspRelatorio.co_funcionario' => $this->data['PreImprimir']['nome_funcionario']
                )
            ));
        }else{

        }

        $unidade = $this->DspUnidadeLotacao->find('all');
        $cargo = $this->DspCargos->find('all');
        $funcao = $this->DspFuncao->find('all');
        $meio_transporte = $this->DspMeioTransporte->find('all');
        $categoria = $this->DspCategoriaPassagem->find('all');
        $funcionarios = $this->GrupoAuxiliar->find('all');

        $this->set('unidade', $unidade);
        $this->set('cargo', $cargo);
        $this->set('funcao', $funcao);
        $this->set('meio_transporte', $meio_transporte);
        $this->set('categoria', $categoria);
        $this->set('despesas', $despesas);
        $this->set('funcionarios', $funcionarios);
    }

    function imprimirRelatorioGeralPorFuncionario( $funcionario )
    {
        $this->loadModel('DspCargos');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');
        $this->loadModel('GrupoAuxiliar');

        $despesas = $this->DspRelatorio->find('all', array(
            'conditions' => array(
                'DspRelatorio.ic_ativo' => 2,
                'DspRelatorio.co_funcionario' => $funcionario
            )
        ));

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Despesas</b>';

        if( !empty($despesas) ) {
            $viagemHtml .= $html->tag('table', null, array(
                'style' => 'width: 100%; position: relative;',
                'class' => 'table table-bordered table-striped',
                'cellspacing' => '0',
                'cellpadding' => '0'
            ));

            $viagemHtml .= $html->tag('thead');

            $viagemHtml .= $html->tableCells(array(
                array(
                    '<b>UNIDADE DE LOTAÇÃO</b>',
                    '<b>NOME / MATRÍCULA</b>',
                    '<b>CARGO / FUNÇÃO</b>',
                    '<b>ORIGEM</b>',
                    '<b>DESTINO</b>',
                    '<b>INÍCIO PERÍODO</b>',
                    '<b>FIM PERÍODO</b>',
                    '<b>MOTIVO</b>',
                    '<b>MEIO DE TRANSPORTE</b>',
                    '<b>CAT. DA PASSAGEM</b>',
                    '<b>VALOR DA PASSAGEM</b>',
                    '<b>Nº DE DIÁRIAS</b>',
                    '<b>VALOR TOTAL DAS DIÁRIAS</b>',
                    '<b>VALOR TOTAL DA VIAGEM</b>',
                    '<b>PROCESSO</b>'
                )
            ), array(
                'class' => 'altrow'
            ));

            $viagemHtml .= $html->tag('/thead');
            $viagemHtml .= $html->tag('tbody');

            foreach ($despesas as $despesa) :
                $unidade = $this->DspUnidadeLotacao->findByCoUnidade($despesa['DspRelatorio']['cod_unidade_lotacao']);
                $cargo = $this->DspCargos->findByCoCargo($despesa['DspRelatorio']['cod_cargo']);
                $funcao = $this->DspFuncao->findByCoFuncao($despesa['DspRelatorio']['cod_funcao']);
                $meio_transporte = $this->DspMeioTransporte->findByCoMeioTransporte($despesa['DspRelatorio']['cod_meio_transporte']);
                $categoria = $this->DspCategoriaPassagem->findByCoCategoriaPassagem($despesa['DspRelatorio']['cod_categoria_passagem']);
                $func = $this->GrupoAuxiliar->findByCoUsuario($despesa['DspRelatorio']['co_funcionario']);

                $valor_passagem = number_format($despesa['DspRelatorio']['valor_passagem'], 2, ',', '.');
                $valor_total_diarias = number_format($despesa['DspRelatorio']['valor_total_diarias'], 2, ',', '.');
                $valor_total_viagem = number_format($despesa['DspRelatorio']['valor_total_viagem'], 2, ',', '.');
                $numero_diarias = number_format($despesa['DspRelatorio']['numero_diarias'], 1, ',', '');

                $origem  = $despesa['DspRelatorio']['uf_origem'] ?: $despesa['DspRelatorio']['pais_origem'];
                $destino = $despesa['DspRelatorio']['uf_destino'] ?: $despesa['DspRelatorio']['pais_destino'];

                $viagemHtml .= $html->tableCells(array(
                    array(
                        $unidade['DspUnidadeLotacao']['nome_unidade'],
                        $func['GrupoAuxiliar']['ds_nome'] . ' / ' . $func['GrupoAuxiliar']['nu_cpf'],
                        $cargo['DspCargos']['nome_cargo'] . ' / ' . $funcao['DspFuncao']['nome_funcao'],
                        $despesa['DspRelatorio']['origem'] . ' / ' . $origem,
                        $despesa['DspRelatorio']['destino'] . ' / ' . $destino,
                        $despesa['DspRelatorio']['dt_inicio'],
                        $despesa['DspRelatorio']['dt_final'],
                        $despesa['DspRelatorio']['motivo_viagem'],
                        $meio_transporte['DspMeioTransporte']['nome_meio_transporte'],
                        $categoria['DspCategoriaPassagem']['nome_categoria_passagem'],
                        'R$ ' . $valor_passagem,
                        $numero_diarias,
                        'R$ ' . $valor_total_diarias,
                        'R$ ' . $valor_total_viagem,
                        $despesa['DspRelatorio']['numero_processo']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;

            $viagemHtml .= $html->tag('/tbody');
            $viagemHtml .= $html->tag('/table');

        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }

    function imprimirRelatorioGeral( $mes = null , $dt_inicio = null , $dt_final = null ){

        $this->loadModel('DspCargos');
        $this->loadModel('DspUnidadeLotacao');
        $this->loadModel('DspFuncao');
        $this->loadModel('DspMeioTransporte');
        $this->loadModel('DspCategoriaPassagem');
        $this->loadModel('GrupoAuxiliar');

        if( $this->data['PreImprimir']['dt_inicio'] != '' && $this->data['PreImprimir']['dt_final'] != '' ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'DspRelatorio.dt_inicio >=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_inicio']))),
                    'DspRelatorio.dt_final <=' => implode("-",array_reverse(explode("/",$this->data['PreImprimir']['dt_final'])))
                )
            ));
        }elseif( $mes != null && $mes != 'geral' ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'MONTHNAME(DspRelatorio.dt_inicio)' => $mes
                )
            ));
        }elseif( $dt_inicio != null && $dt_final != null ){
            $despesas = $this->DspRelatorio->find('all', array(
                'conditions' => array(
                    'DspRelatorio.ic_ativo' => 2,
                    'DspRelatorio.dt_inicio >=' => $dt_inicio,
                    'DspRelatorio.dt_final <=' => $dt_final
                )
            ));
        }else{

        }

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Despesas</b>';

        if( !empty($despesas) ) {
            $viagemHtml .= $html->tag('table', null, array(
                'style' => 'width: 100%; position: relative;',
                'class' => 'table table-bordered table-striped',
                'cellspacing' => '0',
                'cellpadding' => '0'
            ));

            $viagemHtml .= $html->tag('thead');

            $viagemHtml .= $html->tableCells(array(
                array(
                    '<b>UNIDADE DE LOTAÇÃO</b>',
                    '<b>NOME / MATRÍCULA</b>',
                    '<b>CARGO / FUNÇÃO</b>',
                    '<b>ORIGEM</b>',
                    '<b>DESTINO</b>',
                    '<b>INÍCIO PERÍODO</b>',
                    '<b>FIM PERÍODO</b>',
                    '<b>MOTIVO</b>',
                    '<b>MEIO DE TRANSPORTE</b>',
                    '<b>CAT. DA PASSAGEM</b>',
                    '<b>VALOR DA PASSAGEM</b>',
                    '<b>Nº DE DIÁRIAS</b>',
                    '<b>VALOR TOTAL DAS DIÁRIAS</b>',
                    '<b>VALOR TOTAL DA VIAGEM</b>',
                    '<b>PROCESSO</b>'
                )
            ), array(
                'class' => 'altrow'
            ));

            $viagemHtml .= $html->tag('/thead');
            $viagemHtml .= $html->tag('tbody');

            foreach ($despesas as $despesa) :
                $unidade = $this->DspUnidadeLotacao->findByCoUnidade($despesa['DspRelatorio']['cod_unidade_lotacao']);
                $cargo = $this->DspCargos->findByCoCargo($despesa['DspRelatorio']['cod_cargo']);
                $funcao = $this->DspFuncao->findByCoFuncao($despesa['DspRelatorio']['cod_funcao']);
                $meio_transporte = $this->DspMeioTransporte->findByCoMeioTransporte($despesa['DspRelatorio']['cod_meio_transporte']);
                $categoria = $this->DspCategoriaPassagem->findByCoCategoriaPassagem($despesa['DspRelatorio']['cod_categoria_passagem']);
                $funcionario = $this->GrupoAuxiliar->findByCoUsuario($despesa['DspRelatorio']['co_funcionario']);

                $valor_passagem = number_format($despesa['DspRelatorio']['valor_passagem'], 2, ',', '.');
                $valor_total_diarias = number_format($despesa['DspRelatorio']['valor_total_diarias'], 2, ',', '.');
                $valor_total_viagem = number_format($despesa['DspRelatorio']['valor_total_viagem'], 2, ',', '.');
                $numero_diarias = number_format($despesa['DspRelatorio']['numero_diarias'], 1, ',', '');

                $origem  = $despesa['DspRelatorio']['uf_origem'] ?: $despesa['DspRelatorio']['pais_origem'];
                $destino = $despesa['DspRelatorio']['uf_destino'] ?: $despesa['DspRelatorio']['pais_destino'];

                $viagemHtml .= $html->tableCells(array(
                    array(
                        $unidade['DspUnidadeLotacao']['nome_unidade'],
                        $funcionario['GrupoAuxiliar']['ds_nome'] . ' / ' . $funcionario['GrupoAuxiliar']['nu_cpf'],
                        $cargo['DspCargos']['nome_cargo'] . ' / ' . $funcao['DspFuncao']['nome_funcao'],
                        $despesa['DspRelatorio']['origem'] . ' / ' . $origem,
                        $despesa['DspRelatorio']['destino'] . ' / ' . $destino,
                        $despesa['DspRelatorio']['dt_inicio'],
                        $despesa['DspRelatorio']['dt_final'],
                        $despesa['DspRelatorio']['motivo_viagem'],
                        $meio_transporte['DspMeioTransporte']['nome_meio_transporte'],
                        $categoria['DspCategoriaPassagem']['nome_categoria_passagem'],
                        'R$ ' . $valor_passagem,
                        $numero_diarias,
                        'R$ ' . $valor_total_diarias,
                        'R$ ' . $valor_total_viagem,
                        $despesa['DspRelatorio']['numero_processo']
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;

            $viagemHtml .= $html->tag('/tbody');
            $viagemHtml .= $html->tag('/table');

        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }
}