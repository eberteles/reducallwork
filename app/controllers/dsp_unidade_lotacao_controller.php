<?php

class DspUnidadeLotacaoController extends AppController {
    
    public $name = 'DspUnidadeLotacao';

    public $uses = array(
        'DspUnidadeLotacao'
    );

    /*
     * function index() -> load all 'unidade_lotacao' in database
     * set variable 'unidade_lotacao' in view
     */
    function index() {
        // CARREGA TODOS OS UNIDADE DE LOTAÇÃO E COLOCA NA VIEW
        $unidade_lotacao = $this->DspUnidadeLotacao->find('all');
        $this->set('unidade_lotacao', $unidade_lotacao);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->DspUnidadeLotacao->create();
            if ($this->DspUnidadeLotacao->save($this->data['DspUnidadeLotacao'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->DspUnidadeLotacao->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_unidade = $this->data['DspUnidadeLotacao']['descricao'];
            $this->data = $this->DspUnidadeLotacao->read(null, $id);
            $this->data['DspUnidadeLotacao']['nome_unidade'] = $nome_unidade;
            if ($this->DspUnidadeLotacao->save($this->data['DspUnidadeLotacao'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->DspUnidadeLotacao->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }

        if ($this->DspUnidadeLotacao->read(null, $id)) {
            $unidade_lotacao = $this->DspUnidadeLotacao->read(null, $id);
            $msg = null;
            $unidade_lotacao['DspUnidadeLotacao']['ic_ativo'] = 1;

            if($this->DspUnidadeLotacao->save($unidade_lotacao)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
    }

    function listar() {

        echo json_encode ( $this->DspUnidadeLotacao->find ( 'list', array(
            'conditions' => array('DspUnidadeLotacao.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_unidade )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_unidade' ) );
    }

}
?>
