<?php
class ElogiosController extends AppController
{
    var $name = "Elogios";

    var $layout = 'default';

    public function index($coFornecedor = null)
    {
        $criteria = null;
        if($coFornecedor != null)
        {
            $this->layout = 'iframe';
            $this->set('coFornecedor', $coFornecedor);
            $criteria['Elogio.co_fornecedor'] = $coFornecedor;
        }
        $this->loadModel('Contrato');
        $this->loadModel('Fornecedor');
        $this->Elogio->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Elogio.ic_ativo' => 1
            )
        );

        if(!empty($this->data)) {
            if(!empty($this->data['Elogio']['num_elogio'])) {
                $criteria['Elogio.nu_elogio'] = $this->data['Elogio']['num_elogio'];
            }
            if(!empty($this->data['Elogio']['num_fornecedor'])) {
                $criteria['Elogio.co_fornecedor'] = $this->data['Elogio']['num_fornecedor'];
            }
            if(!empty($this->data['Elogio']['num_contrato'])) {
                $criteria['Elogio.co_contrato'] = $this->data['Elogio']['num_contrato'];
            }
        }

        $this->set('elogios', $this->paginate($criteria));

        $this->set('fornecedores', $this->Fornecedor->find('list',array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));

        $this->set('contratos', $this->Contrato->find('list',array(
            'conditions' => array(
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        )));
    }

    function iframe($coFornecedor)
    {
        $this->set(compact('coFornecedor'));
    }

    public function add($iframe = false)
    {
        $this->loadModel('Fornecedor');
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Elogio->create();

            $this->data['Elogio']['nu_year'] = date('Y');

            $elogio = $this->Elogio->find('all', array(
                'fields' => array(
                    'MAX(Elogio.nu_prefix) AS max'
                ),
                'conditions' => array(
                    'Elogio.nu_year' => $this->data['Elogio']['nu_year']
                )
            ));

            if($elogio[0][0]['max'] == null)
                $elogio[0][0]['max'] = 0;

            $this->data['Elogio']['nu_prefix'] = $elogio[0][0]['max'] + 1;

            if($this->Elogio->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('fornecedores', $this->Fornecedor->find('list',array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));
    }

    public function edit($id = null)
    {
        $this->loadModel('Contrato');
        $this->loadModel('Fornecedor');
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Elogio->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Elogio->read(null, $id);

        $this->set('fornecedores', $this->Fornecedor->find('list', array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));

        $this->set('contratos', $this->Contrato->find('list',array(
            'conditions' => array(
                'Contrato.co_contrato' => $this->data["Elogio"]["co_contrato"],
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        )));
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Elogio->id = $id;
            $msg = null;
            $elogio['Elogio']['ic_ativo'] = 0;

            if($this->Elogio->saveField('ic_ativo',0)){
                $msg = "Elogio bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Elogio->id = $id;
            $msg = null;
            $elogio['Elogio']['ic_ativo'] = 1;

            if($this->Elogio->saveField('ic_ativo',1)){
                $msg = "Elogio cadastrada com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function listContratos()
    {
        $this->loadModel('Contrato');
        $this->Contrato->recursive = 0;
        $contratos = $this->Contrato->find('all',array(
            'conditions' => array(
                'Contrato.co_fornecedor' => $this->params["form"]["co_fornecedor"],
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        ));

        echo json_encode($contratos);
        exit;
    }
}