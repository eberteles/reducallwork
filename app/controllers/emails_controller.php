<?php
/**
 * @resource { "name" : "Cadastro de Emails", "route":"emails", "access": "private", "type": "module" }
 */
class EmailsController extends AppController
{

    var $name   = 'Emails';
    //var $layout = 'blank';
    
    var $uses = array('EmailSave', 'Fornecedor', 'Usuario', 'Setor');

    /**
     *@resource { "name" : "Emails", "route":"emails\/index", "access": "private", "type": "select" }
     */
    public function index($tipo = 1) {
        $usuario = $this->Session->read('usuario');
        $filtro  = 'EmailSave.co_destinatario';
        if($tipo == 2) {
            $filtro  = 'EmailSave.co_remetente';

            $this->paginate = array(
                'conditions' => array(
                    $filtro => $usuario['Usuario']['co_usuario']
                    ),
                'order' => array(
                    'EmailSave.envio' => 'desc'
                    )
                );
        }else{
            $this->paginate = array(
                'conditions' => array(
                    $filtro => $usuario['Usuario']['co_usuario']
                    ),
                'order' => array(
                    'EmailSave.envio' => 'desc'
                    )
                );
        }
	$this->set('emails', $this->paginate());
        $this->set('tipo', $tipo);
    }
    
    function setValidacoes()
    {
        $this->EmailSave->validate = null;

        $this->EmailSave->validate['assunto'] = array(
            'notempty' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo Assunto em branco'
            )
        );
        
        $this->EmailSave->validate['nome'] = array(
            'notempty' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo Email em branco'
            )
        );
        
        $this->EmailSave->validate['email'] = array(
            'notempty' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Campo Email em branco'
            )
        );
        
        $this->EmailSave->validate['mensagem'] = array(
            'notempty' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo Email em branco'
            )
        );
    }
    
    /**
        *@resource { "name" : "Novo Email", "route":"emails\/add", "access": "private", "type": "insert" }
    */
    public function add() {
        $this->loadModel('Usuario');
        
        $this->setValidacoes();
        
        if (! empty($this->data) && $this->EmailSave->validates()) {           
        	        	
        	$this->EmailProvider->subject   = $this->data['EmailSave']['assunto']; 
        	$this->EmailProvider->to        = $this->data['EmailSave']['nome'] . ' <' . $this->data['EmailSave']['email'] . '>';
			
        	$this->data['EmailSave']['envio'] = date('Y-m-d');
        	
		if( $this->EmailSave->save($this->data)
        		&& $this->EmailProvider->send($this->data['EmailSave']['mensagem'])) {
                $this->Session->setFlash(__('Email enviado com sucesso', true));
                $this->redirect(array(
                    'action' => 'index', 2
                ));
            } else {
                $this->Session->setFlash(__('O email não pode ser enviado. Por favor, tente novamente.', true));
            }            
        }        
    }

   /**
    * @resource { "name" : "listar menu", "route":"emails\/list_menu", "access": "private", "type": "select" }
    */
    public function list_menu()
    {
        $usuario = $this->Session->read('usuario');
        $usuario['Usuario']['co_usuario'];
        
        $this->EmailSave->recursive = 0;
        
        $or = '(EmailSave.co_destinatario = ' . $usuario['Usuario']['co_usuario'];
        if($usuario['Usuario']['co_setor'] != '') {
            $or .= ' or EmailSave.co_setor = ' . $usuario['Usuario']['co_setor'];
        }
        $or .= ')';
        
        $this->paginate = array(
            'conditions' => array(
                'EmailSave.co_destinatario' => $usuario['Usuario']['co_usuario'],
                'visualizado' => '0',
            ),
            'order' => array(
                'envio' => 'asc'
            ),
            'limit' => 8
        );
        
        $this->set('emails', $this->paginate());
    }
    
    /**
        *@resource { "name" : "abrir emails", "route":"emails\/abrir", "access": "private", "type": "select" }
    */
    public function abrir($idMensagem) {
        $this->EmailSave->id = $idMensagem;
        $mensagem   = $this->EmailSave->read(null, $idMensagem);
        
        $usuario = $this->Session->read('usuario');
        
        if($usuario['Usuario']['co_usuario'] == $mensagem['EmailSave']['co_destinatario']) {
            $this->EmailSave->saveField("visualizado", 1);
        }
        
        echo $mensagem['EmailSave']['mensagem'];
        exit();
    }

    /**
        *@resource { "name" : "limpar emails", "route":"emails\/limpar", "access": "private", "type": "update" }
    */
    public function limpar() {
        if ($this->RequestHandler->isPost()) {
            $emailIDs = $this->data['Email'];

            for ($i = 0, $len = count($emailIDs); $i < $len; $i++) {
                $this->EmailSave->id = $emailIDs[$i];
                $this->EmailSave->saveField('visualizado', 1);
            }
        }

        exit;
    }
    
    /**
        *@resource { "name" : "pesquisar emails", "route":"emails\/pesquisar", "access": "private", "type": "select" }
    */
    function pesquisar() {
        
        if (! empty($this->data) && isset($this->data['nome'])) {

            $fornecedores = $this->Fornecedor->find ( 'list', array(
                'fields' => array('Fornecedor.ds_email', 'Fornecedor.no_razao_social'),
                'order' => 'Fornecedor.no_razao_social ASC',
                'conditions' => array(
                    'Fornecedor.ic_ativo' => 1,
                    'Fornecedor.no_razao_social like' => '%' . $this->data['nome'] . '%'
                )
            ) );
            
            $usuarios =  $this->Usuario->find ( 'list', array(
                'fields' => array('Usuario.ds_email', 'Usuario.ds_nome'),
                'order' => 'Usuario.ds_nome ASC',
                'conditions' => array(
                    'Usuario.ic_ativo'=> 1,
                    'Usuario.ds_nome like' => '%' . $this->data['nome'] . '%'
                )
            ) );
            
            $setores =  $this->Setor->find ( 'list', array(
                'fields' => array('Setor.ds_email', 'Setor.ds_setor'),
                'order' => 'Setor.ds_setor ASC',
                'conditions' => array(
                    'Setor.ic_ativo'=> 1,
                    'Setor.ds_setor like' => '%' . $this->data['nome'] . '%'
                )
            ) );
            
            echo json_encode ( array_merge ($usuarios, $fornecedores, $setores) );
            
        }

        exit ();
    }
}
?>