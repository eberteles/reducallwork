<?php

class EmpenhosAnulacoesController extends AppController
{

    var $name = 'EmpenhosAnulacoes';

    var $layout = 'iframe';
    var $uses = array('EmpenhoAnulacao', 'Empenho');

    function index($coEmpenho, $acao = '')
    {
        $this->EmpenhoAnulacao->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'EmpenhoAnulacao.co_empenho' => $coEmpenho
            )
        );
        
        $this->set('anulacoes', $this->paginate());
        $this->set('tipos', $this->EmpenhoAnulacao->tiposDeAnulacoes);
        
        $this->set(compact('coEmpenho'));
        $this->set(compact('acao'));
        
        $this->set('empenho', $this->Empenho->find('first', array(
            'conditions' => array(
                'Empenho.co_empenho' => $coEmpenho
            ),
            'fields' => array('vl_empenho') )) );
        
    }
    
    function iframe( $coEmpenho)
    {		
            $this->set( compact( 'coEmpenho' ) );
    }

    function add($coEmpenho)
    {

        if (! empty($this->data)) {
            $this->EmpenhoAnulacao->create();

            if ($this->EmpenhoAnulacao->save($this->data)) {
                
                $empenho['Empenho']['vl_empenho']   = vlReal( ln($this->data['EmpenhoAnulacao']['vl_empenho']) - ln($this->data['EmpenhoAnulacao']['vl_anulacao']) );
                
                $this->Empenho->id = $coEmpenho;
                $this->Empenho->save( $empenho, false, array('vl_empenho') );
                
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                
                $this->redirect(array(
                    'action' => 'index',
                    $coEmpenho, 'S'
                ));
                
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $tipos = array(
            'P' => 'Parcial',
            'T' => 'Total'
        );

        $anulacoes = $this->EmpenhoAnulacao->find('all',array(
            'conditions' => array(
                'EmpenhoAnulacao.co_empenho' => $coEmpenho
            ),
            'fields' => array(
                'EmpenhoAnulacao.tp_anulacao'
            )
        ));

        foreach($anulacoes as $anulacao) {
            if($anulacao['EmpenhoAnulacao']['tp_anulacao'] == 'P')
                unset($tipos['T']);
        }

        $this->set(compact('coEmpenho'));
        $this->set('tipos', $tipos);
        $this->set('empenho', $this->Empenho->find('first', array(
            'conditions' => array(
                'Empenho.co_empenho' => $coEmpenho
            ),
            'fields' => array('vl_empenho') )) );
    }
    
    function delete($id = null, $coEmpenho)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coEmpenho
            ));
        } else {
            
            $empenho    = $this->Empenho->find('first', array(
                                'conditions' => array('Empenho.co_empenho' => $coEmpenho),
                                'fields' => array('vl_empenho') ));
            
            $anulacao   = $this->EmpenhoAnulacao->find('first', array(
                                'conditions' => array('EmpenhoAnulacao.co_empenho_anulacao' => $id),
                                'fields' => array('vl_anulacao') ));
            
            if ($this->EmpenhoAnulacao->delete($id)) {
                
                $this->Empenho->id = $coEmpenho;
                $this->Empenho->saveField("vl_empenho", $empenho['Empenho']['vl_empenho'] + $anulacao['EmpenhoAnulacao']['vl_anulacao'] );
                
                $this->Session->setFlash(__('Registro excluído com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coEmpenho, 'S'
                ));
            }
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coEmpenho
        ));
    }
    
}
?>