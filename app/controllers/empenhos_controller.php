<?php

/**
 * @resource { "name" : "Empenhos", "route":"empenhos", "access": "private", "type": "module" }
 */
class EmpenhosController extends AppController
{

    var $name = 'Empenhos';

    var $layout = 'iframe';

    var $arTipos = null;

    function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        $this->arTipos = $modulo->getTiposDeEmpenhos();
        parent::__construct();
    }

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['Empenho']['nu_natureza_despesa']);
        $this->Functions->limparMascara($this->data['Empenho']['nu_ptres']);
        $this->Functions->limparMascara($this->data['Empenho']['sub_nu_ptres']);
    }

    /**
     * @resource { "name" : "Empenhos", "route":"empenhos\/index", "access": "private", "type": "select" }
     */
    function index($coContrato)
    {
        $this->Empenho->recursive = 1;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Empenho.co_contrato' => $coContrato
            )
        );

        $this->set('empenhos', $this->paginate());
        $this->set('coContrato', $coContrato);
        $this->set('tipos', $this->arTipos);
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "iframe", "route":"empenhos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name" : "Editar Empenho", "route":"empenhos\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $coContrato)
    {
        $this->loadModel('PreEmpenho');
        App::import('Helper', 'Modulo');

        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        App::import('Model', 'Contrato');
        $modelContrato = new Contrato();
        $modelContrato->recursive = -1;
        $contrato = $modelContrato->find('first', array(
            'conditions' => array(
                'Contrato.co_contrato' => $coContrato
            )
        ));

        $empenho = $this->readEmpenho($id);
        if (!empty($this->data)) {
            // retorna empenho anterior
            $empenhoAnterior = $this->Empenho->read(null, $id);

            // $restante = ln($this->data["Empenho"]["vl_empenho_atual"]) - ln($this->data["Empenho"]["vl_empenho"]);

            if (isset($this->data["Empenho"]["co_pre_empenho"])) {
                $this->restorePreEmpenhoValue($this->data["Empenho"]["co_pre_empenho"], $restante);
            }

            $this->prepararCampos();

            $valor_restante = 1;

            if ($this->data["Empenho"]["tp_empenho"] != 'A' && $this->data["Empenho"]["tp_empenho"] != '') {
                $valor_restante = $this->checkValorRestanteContrato($this->data['Empenho']['co_contrato'], $this->data['Empenho']['vl_empenho'], $empenhoAnterior['Empenho']['vl_empenho']);
            }

            /*
             * @todo
             * ADICIONAR COLUNAS PARA OS NOVOS TIPO DE EMPENHO [GES-570]
             */
            $empenhosVinculados = $this->Empenho->find('list', array(
                'conditions' => array(
                    'OR' => array(
                        array( 'Empenho.empenho_reforco_originario' => $id ),
                        array( 'Empenho.empenho_anulacao_originario' => $id ),
                        array( 'Empenho.empenho_cancelamento_originario' => $id ),
                        array( 'Empenho.empenho_estorno_de_anulacao_originario' => $id )
                    )
                )
            ));

            if ($this->data['Empenho']['tp_empenho'] === 'O' && empty($empenhosVinculados)){

                $this->data['Empenho']['vl_restante'] = $this->data['Empenho']['vl_empenho'];

            }else{
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique se esse empenho possui vinculos.', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            }

            if ($valor_restante > 0) {

                if ($this->data['Empenho']['tp_empenho'] === 'A') {
                    // atualiza
                    $this->somaValorRestante($this->data['Empenho']['empenho_anulacao_originario'], $empenhoAnterior['Empenho']['vl_empenho']);
                    // refaz
                    $this->reduzValorRestante($this->data['Empenho']['empenho_anulacao_originario'], $this->data['Empenho']['vl_empenho']);
                    // se for anulação de reforço
                    $empenhoAnulacaoOriginario = $this->Empenho->read(null, $this->data['Empenho']['empenho_anulacao_originario']);
                    if ($empenhoAnulacaoOriginario
                        && $empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario']
                    ) {
                        $this->reduzValorRestante($empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario'], $this->data['Empenho']['vl_empenho']);
                    }
                }

                if ($this->data['Empenho']['tp_empenho'] == 'R') {
                    // retorna empenho anterior
                    $empenhoAnterior = $this->Empenho->read(null, $id);
                    // atualiza
                    $this->reduzValorRestante($this->data['Empenho']['empenho_reforco_originario'], $empenhoAnterior['Empenho']['vl_empenho']);
                    // refaz
                    $this->somaValorRestante($this->data['Empenho']['empenho_reforco_originario'], $this->data['Empenho']['vl_empenho']);
                }

                if ($this->Empenho->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index',
                        $coContrato
                    ));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            } else {
                // $this->Empenho->invalidate('vl_empenho', 'O valor do empenho supera o valor do contrato');
                $this->Session->setFlash(__('O registro não pode ser salvo. O valor do empenho supera o valor do contrato.', true));                
            }
        } else {
            $this->data = $empenho;
        }

        if ($empenho['Empenho']['tp_empenho'] == 'A') {
            $this->set('empenho_anulado', $empenho['Empenho']['empenho_originario_anulacao']);
        }

        $this->Empenho->displayField = 'nu_empenho';

        $empenhosAnulacao = $this->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.tp_empenho' => array('O', 'R'),
                'Empenho.co_contrato' => $coContrato,
                'Empenho.co_empenho !=' => $id
            )
        ));

        $empenhosReforco = $this->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.tp_empenho' => 'O',
                'Empenho.vl_restante !=' => 0,
                'Empenho.co_contrato' => $coContrato,
                'Empenho.co_empenho !=' => $id
            )
        ));

        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('empenho', $this->data['Empenho']);
        $this->set('tipos', $this->arTipos);
        $this->set('dataFimVigenciaContrato', $contrato['Contrato']['dt_fim_vigencia']);
        $this->set('dataIniVigenciaContrato', $contrato['Contrato']['dt_ini_vigencia']);
        $this->set('empenhos_reforco', $empenhosReforco);
        $this->set('empenhos_anulacao', $empenhosAnulacao);
        $this->set('preEmpenhos', $this->PreEmpenho->find('list', array(
            'fields' => array(
                'PreEmpenho.co_pre_empenho', 'PreEmpenho.nu_pre_empenho'
            ),
            'conditions' => array(
                'PreEmpenho.ic_ativo' => 1,
                'PreEmpenho.co_contrato' => $coContrato
            )
        )));
    }

    /**
     * @resource { "name" : "Remover Empenho", "route":"empenhos\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        $empenho = $this->Empenho->read(null, $id);
        if (!empty($empenho)) {
            $msg = null;

            if ($empenho['Empenho']['co_pre_empenho'] != null) {
                $this->restorePreEmpenhoValue($empenho['Empenho']['co_pre_empenho'], $empenho['Empenho']['vl_empenho']);
            }

            $tpEmpenho = $empenho['Empenho']['tp_empenho'];

            if ($tpEmpenho == Empenho::TIPO_ANULACAO) {
                $this->somaValorRestante($empenho['Empenho']['empenho_anulacao_originario'], $empenho['Empenho']['vl_empenho']);
                // se for reforço ele atualiza o originário
                $empenhoAnulacaoOriginario = $this->Empenho->read(null, $this->data['Empenho']['empenho_anulacao_originario']);
                if ($empenhoAnulacaoOriginario
                    && $empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario']
                ) {
                    $this->somaValorRestante($empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario'], $empenho['Empenho']['vl_empenho']);
                }
            }

            if ($tpEmpenho == Empenho::TIPO_REFORCO) {
                $this->reduzValorRestante($empenho['Empenho']['empenho_reforco_originario'], $empenho['Empenho']['vl_empenho']);
            }
        }

        if ($this->Empenho->delete($id, true)) {
            if ($tpEmpenho == Empenho::TIPO_ORIGINAL) {
                $anulacoes = $this->Empenho->find('all', array(
                    'conditions' => array(
                        'Empenho.co_contrato' => $coContrato,
                        'Empenho.empenho_anulacao_originario' => $id
                    )
                ));
                $reforcos = $this->Empenho->find('all', array(
                    'conditions' => array(
                        'Empenho.co_contrato' => $coContrato,
                        'Empenho.empenho_reforco_originario' => $id
                    )
                ));
                $excluidos = array_merge($anulacoes, $reforcos);
                foreach ($excluidos as $exc) {
                    $this->Empenho->delete($exc["Empenho"]["co_empenho"]);
                }
            }

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }

    private function readEmpenho($id)
    {
        if (!is_int((int)$id))
            throw new TypeError('Valor recebido não é válido!');

        return $this->Empenho->read(null, $id);
    }

    // somar valor de reforço
    public function somaValorRestante($coEmpenhoOriginario, $valor)
    {
        if ($valor < 0) {
            $valor = (float)-1 * $valor;
        }

        $empenho = $this->readEmpenho($coEmpenhoOriginario);

        if ($empenho) {
            $empenho['Empenho']['vl_restante'] = (float)$empenho['Empenho']['vl_restante'] + (float)ln($valor);
            $empenho['Empenho']['vl_empenho'] = (float)$empenho['Empenho']['vl_empenho'];

            $this->Empenho->save($empenho);
        }
    }

    // reduzir valor de empenho
    public function reduzValorRestante($coEmpenhoOriginario, $valor)
    {
        $empenho = $this->readEmpenho($coEmpenhoOriginario);
        if ($empenho) {
            $empenho['Empenho']['vl_restante'] = (float)$empenho['Empenho']['vl_restante'] - (float)ln($valor);
            $empenho['Empenho']['vl_empenho'] = (float)$empenho['Empenho']['vl_empenho'];
            $this->Empenho->save($empenho);
        }
    }

    /**
     * @resource { "name" : "Novo Empenho", "route":"empenhos\/add", "access": "private", "type": "insert" }
     */
    function add($coContrato)
    {
        $this->loadModel('PreEmpenho');
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        App::import('Model', 'Contrato');
        $modelContrato = new Contrato();
        $modelContrato->recursive = -1;
        $contrato = $modelContrato->find('first', array(
            'conditions' => array(
                'Contrato.co_contrato' => $coContrato
            )
        ));

        if (!empty($this->data)) {
            $this->prepararCampos();
            $this->Empenho->create();

            if ($modulo->isAutoNumeracaoEmpenho()) {
                $this->data['Empenho']['nu_empenho'] = $this->Empenho->getNextEmpenho();
            }

            if (isset($this->data["Empenho"]["co_pre_empenho"])) {
                $coPreEmpenho = $this->data["Empenho"]["co_pre_empenho"];
            }

            $vlEmpenho = ln($this->data["Empenho"]["vl_empenho"]);

            $this->data["Empenho"]["vl_restante"] = $this->data["Empenho"]["vl_empenho"];

            $valor_restante = 1;

            if ($this->data["Empenho"]["tp_empenho"] != 'A' && $contrato['Contrato']['nu_contrato'] != '') {
                // verificar se o valor do empenho atingiu o valor máximo, ultrapassou o valor total do contrato
                $valor_restante = $this->checkValorRestanteContrato($this->data['Empenho']['co_contrato'], $this->data['Empenho']['vl_empenho']);
            }

            if ($valor_restante > 0) {
                $tipoEmpenho = $this->data["Empenho"]["tp_empenho"];
                if ($this->Empenho->save($this->data)) {
                    $valorDoEmpenho = $this->data["Empenho"]["vl_empenho"];
                    if (!empty($coPreEmpenho) && !empty($vlEmpenho)) {
                        if ($this->updatePreEmpenhoValue($coPreEmpenho, $vlEmpenho)) {
                            $this->Session->setFlash(__('Registro salvo com sucesso', true));
                            $this->redirect(array(
                                'action' => 'index',
                                $coContrato
                            ));
                        }
                    }

                    // reduzir valor restante
                    if ($tipoEmpenho === Empenho::TIPO_ANULACAO) {
                        $this->reduzValorRestante($this->data["Empenho"]["empenho_anulacao_originario"], $valorDoEmpenho);
                        // se for anulação de reforço
                        $empenhoAnulacaoOriginario = $this->Empenho->read(null, $this->data['Empenho']['empenho_anulacao_originario']);
                        if ($empenhoAnulacaoOriginario
                            && $empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario']
                        ) {
                            $this->reduzValorRestante($empenhoAnulacaoOriginario['Empenho']['empenho_reforco_originario'], $this->data['Empenho']['vl_empenho']);
                        }
                    }

                    // reduzir valor restante
                    if ($tipoEmpenho === Empenho::TIPO_CANCELAMENTO) {
                        $this->reduzValorRestante($this->data["Empenho"]["empenho_cancelamento_originario"], $valorDoEmpenho);
                    }

                    if ($tipoEmpenho === Empenho::TIPO_ESTORNO_DE_ANULACAO) {
                        $idDoEmpenhoDeEstornoDeAnulacao = $this->data["Empenho"]["empenho_estorno_de_anulacao_originario"];
                        $this->reduzValorRestante($idDoEmpenhoDeEstornoDeAnulacao, $valorDoEmpenho);
                        $IdDoEmpenhoOriginal = $this->Empenho->field('empenho_anulacao_originario', array('co_empenho' => $idDoEmpenhoDeEstornoDeAnulacao));
                        $this->somaValorRestante($IdDoEmpenhoOriginal, $valorDoEmpenho);
                    }

                    // somar valor restante
                    if ($tipoEmpenho === Empenho::TIPO_REFORCO) {
                        $this->somaValorRestante($this->data["Empenho"]["empenho_reforco_originario"], $valorDoEmpenho);
                    }

                    // verificar se o valor do empenho atingiu o valor máximo, ultrapassou o valor total do contrato
                    if (($this->checkValorRestanteContrato(
                                $this->data['Empenho']['co_contrato'],
                                $this->data['Empenho']['vl_empenho']) - $this->data['Empenho']['vl_empenho']) <= 0
                    ) {
                        $this->avisarEnvolvidosEmpenho100Porcento();
                    }

                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index',
                        $coContrato
                    ));
                } else {
                    foreach ($this->Empenho->validationErrors as $erro) {
                        $this->Session->setFlash($erro);
                    }
                }
            } else {
                $this->Session->setFlash(__('Não foi possível salvar o registro. O valor do empenho superou o valor do contrato', true));
            }
        }

        $this->set('preEmpenhos', $this->PreEmpenho->find('list', array(
            'fields' => array(
                'PreEmpenho.co_pre_empenho', 'PreEmpenho.nu_pre_empenho'
            ),
            'conditions' => array(
                'PreEmpenho.ic_ativo' => 1,
                'PreEmpenho.co_contrato' => $coContrato
            )
        )));

        $this->Empenho->displayField = 'nu_empenho';

        $empenhosReforco = $this->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.tp_empenho' => 'O',
                'Empenho.co_contrato' => $coContrato
            )
        ));
        /*
        if(count($empenhosReforco) >= 1){
            unset($this->arTipos['O']);
        }*/

        $empenhosAnulacao = $this->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.tp_empenho !=' => 'A',
                'Empenho.vl_restante !=' => 0,
                'Empenho.co_contrato' => $coContrato
            )
        ));

        $empenhosDeEstornoDeAnulacao = $this->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.tp_empenho' => 'A',
                'Empenho.vl_restante !=' => 0,
                'Empenho.co_contrato' => $coContrato
            )
        ));

        $this->set('dataFimVigenciaContrato', $contrato['Contrato']['dt_fim_vigencia']);
        $this->set('dataIniVigenciaContrato', $contrato['Contrato']['dt_ini_vigencia']);
        $this->set('empenhos_reforco', $empenhosReforco);
        $this->set('empenhos_anulacao', $empenhosAnulacao);
        $this->set('empenhos_de_estorno_de_anulacao', $empenhosDeEstornoDeAnulacao);
        $this->set(compact('coContrato'));
        $this->set('tipos', $this->arTipos);
    }


    // verificar valor restante do contrato
    public function checkValorRestanteContrato($co_contrato, $valor_empenho, $valor_anterior = 0)
    {
        App::import('Model', 'Contrato');
        $contratoModel = new Contrato();

        App::import('Model', 'Aditivo');
        $aditivoModel = new Aditivo();

        App::import('Model', 'Empenhos');
        $empenhoModel = new Empenho();

        $contrato = $contratoModel->find('first', array(
                'recursive' => -1,
                'conditions' => array(
                    'Contrato.co_contrato' => $co_contrato
                )
            )
        );

        // empenho que está sendo adicionado
        $valor_empenho = ln($valor_empenho);

        $vl_total_empenhos = (float)$empenhoModel->getTotal($co_contrato) - $valor_anterior;

        $vl_total_aditivo = (float)$aditivoModel->getTotal($co_contrato);

        // valor total do contrato aditivado
        $valor_total = (float)($contrato['Contrato']['vl_global'] + $vl_total_aditivo);

        $valor_restante = (float)$valor_total - (float)$vl_total_empenhos;

        if (($vl_total_empenhos + $valor_empenho) <= $valor_total) {
            return $valor_restante;
        }

        return -1;
    }

    /**
     * @resource { "name" : "Retorno JSON Empenho", "route":"empenhos\/getValorEmpenho", "access": "private", "type": "select" }
     */
    function getValorEmpenho()
    {
        $valorTotalEmpenhos = 0.00;
        $valorMin = 0.00;
        foreach ($this->params['form']['empenhos'] as $empenho) {
            $emp = $this->Empenho->findByCoEmpenho($empenho);
            if ($emp['Empenho']['tp_empenho'] == 'O') {
                $valorMin += $emp['Empenho']['vl_empenho'];
            }
            $valorTotalEmpenhos += $emp['Empenho']['vl_restante'];
        }

        $limitValues = array(
            'minValue' => $valorMin,
            'vlTotalEmpenhos' => $valorTotalEmpenhos
        );

        echo json_encode($limitValues);

        exit();
    }

    /**
     * @resource { "name" : "Atualizar Pre empenho", "route":"empenhos\/updatePreEmpenhoValue", "access": "private", "type": "update" }
     */
    public function updatePreEmpenhoValue($coPreEmpenho, $value)
    {
        $this->loadModel('PreEmpenho');
        if ($this->PreEmpenho->read(null, $coPreEmpenho)) {
            $preEmpenho = $this->PreEmpenho->read(null, $coPreEmpenho);
            $msg = null;
            $preEmpenho['PreEmpenho']['vl_restante'] -= $value;

            if ($this->PreEmpenho->save($preEmpenho)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @resource { "name" : "Restaurar Pre empenho", "route":"empenhos\/restorePreEmpenhoValue", "access": "private", "type": "update" }
     */
    public function restorePreEmpenhoValue($coPreEmpenho, $value)
    {
        $this->loadModel('PreEmpenho');
        if ($this->PreEmpenho->read(null, $coPreEmpenho)) {
            $preEmpenho = $this->PreEmpenho->read(null, $coPreEmpenho);
            $msg = null;
            $preEmpenho['PreEmpenho']['vl_restante'] += $value;

            if ($this->PreEmpenho->save($preEmpenho)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @resource { "name" : "Retorno JSON Empenho", "route":"empenhos\/comparaValoresAnulacao", "access": "private", "type": "select" }
     */
    public function comparaValoresAnulacao()
    {
        if ($this->Empenho->read(null, $this->params["form"]["empenhoOriginario"])) {
            $empenho = $this->Empenho->read(null, $this->params["form"]["empenhoOriginario"]);

            if ($empenho['Empenho']['vl_restante'] < ln($this->params["form"]["vlEmpenho"])) {
                $retorno = array(
                    'msg' => 'false'
                );
                echo json_encode($retorno);
            } else {
                $retorno = array(
                    'msg' => 'true'
                );
                echo json_encode($retorno);
            }
        }
        exit;
    }

    public function dateSql($dateSql)
    {
        $ano = substr($dateSql, 6);
        $mes = substr($dateSql, 3, -5);
        $dia = substr($dateSql, 0, -8);
        return $ano . "-" . $mes . "-" . $dia;
    }

    public function avisarEnvolvidosEmpenho100Porcento()
    {
        ob_start();
        App::import('Model', 'Contrato');
        $mdContrato = new Contrato();

        $fiscais = $mdContrato->getFiscaisNomeEmail($this->data['Empenho']['co_contrato']);
        $gestorAtivo = $mdContrato->getGestorAtivo($this->data['Empenho']['co_contrato']);

        $contrato = $mdContrato->read(array(
            'Contrato.nu_processo',
            'Contrato.nu_contrato',
            'Contrato.ds_objeto',
            'Contrato.dt_ini_vigencia',
            'Contrato.dt_fim_vigencia',
            'Contrato.vl_global'
        ), $this->data['Empenho']['co_contrato']);

        $mensagem = array(
            "O contrato " . $contrato['Contrato']['nu_contrato'] . " acabou atingir o valor máximo de empenho.
            Acompanhe o andamento deste contrato até que o mesmo seja finalizado ou renovado."
        );

        foreach ($fiscais as $fiscal) {
            if ($fiscal['Usuario']['ds_email'] != '') {
                $this->EmailProvider->subject = "O contrato " . $contrato['Contrato']['nu_contrato'] . "atingiu 100% de valor empenhado.";
                $this->EmailProvider->to = $fiscal['Usuario']['ds_email'];
                $this->EmailProvider->send($mensagem);
            }
        }
        foreach ($gestorAtivo as $gestor) {
            if ($gestor['Usuario']['ds_email'] != '') {
                $this->EmailProvider->subject = "O contrato " . $contrato['Contrato']['nu_contrato'] . "atingiu 100% de valor empenhado.";
                $this->EmailProvider->to = $gestor['Usuario']['ds_email'];
                $this->EmailProvider->send($mensagem);
            }
        }
    }
}

?>
