<?php

class EntregasController extends AppController
{

    var $name = 'Entregas';

    var $layout = 'iframe';

    function index($coContrato, $ajax = false)
    {
        $this->Entrega->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Entrega.co_contrato' => $coContrato
            ),
            'order' => array(
                'Entrega.dt_entrega' => 'asc'
            )
        );
        
        $this->set('entregas', $this->paginate());
        
        $this->set(compact('coContrato'));
    }

    function iframe($coContrato)
    {
        $this->layout = 'ajax';
        $this->set(compact('coContrato'));
    }

    function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->Entrega->create();
            if ($this->Entrega->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
    }

    function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Entrega->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Entrega->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Entrega->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>