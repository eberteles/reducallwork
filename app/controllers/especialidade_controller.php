<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 17:13
 */
class EspecialidadeController extends AppController
{
    var $name = "Especialidade";

    public function index()
    {
        $this->Especialidade->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Especialidade.ic_ativo' => 1
            )
        );
        $criteria = null;

        if(!empty($this->data)) {
            if(isset($this->data['Especialidade']['ds_especialidade'])) {
                $criteria['Especialidade.ds_especialidade'] = $this->data['Especialidade']['ds_especialidade'];
            }
        }

        $this->set('especialidades', $this->paginate($criteria));
    }

    public function add($iframe = false)
    {
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Especialidade->create();

            $especialidade = $this->Especialidade->findByDsEspecialidade($this->data['Especialidade']['ds_especialidade']);
            if(isset($especialidade['Especialidade']) && $especialidade['Especialidade']['ic_ativo'] == 0) {
                $this->reInsert($especialidade['Especialidade']['co_especialidade']);
            }
            if($this->Especialidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Especialidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Especialidade->read(null, $id);
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Especialidade->id = $id;
            $msg = null;
            $especialidade['Especialidade']['ic_ativo'] = 0;

            if($this->Especialidade->saveField('ic_ativo',0)){
                $msg = "Especialidade bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Especialidade->id = $id;
            $msg = null;
            $especialidade['Especialidade']['ic_ativo'] = 1;

            if($this->Especialidade->saveField('ic_ativo',1)){
                $msg = "Especialidade cadastrada com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}