<?php
class EventosController extends AppController
{

    var $name = 'Eventos';

    function index()
    {
        $this->gerarFiltro();
        $this->Evento->recursive = 0;
        $this->Evento->validate = array();
        $this->paginate = array(
            'limit' => 15
        );
        $criteria = null;
        
        if (!$this->data['Evento']['nu_ano']) {
            $this->data['Evento']['nu_ano'] = date('Y');
        }
        if (! empty($this->data)) {
            if( isset( $this->data['Evento']['ds_evento'] ) && $this->data['Evento']['ds_evento'] != '' ) {
                $criteria['ds_evento like'] = '%' . up($this->data['Evento']['ds_evento']) . '%';
            }
            if( isset( $this->data['Evento']['ds_local'] ) && $this->data['Evento']['ds_local'] != '') {
                $criteria['ds_local like'] = '%' . up($this->data['Evento']['ds_local']) . '%';
            }
            if( isset( $this->data['Evento']['co_confederacao'] ) && $this->data['Evento']['co_confederacao'] > 0 ) {
                $criteria['Evento.co_confederacao'] = $this->data['Evento']['co_confederacao'];
            }
            if( isset( $this->data['Evento']['co_modalidade'] ) && $this->data['Evento']['co_modalidade'] > 0 ) {
                $criteria['Evento.co_modalidade'] = $this->data['Evento']['co_modalidade'];
            }
            $criteria['nu_ano'] = $this->data['Evento']['nu_ano'];
        }
        $this->set('confederacoes', $this->Evento->Confederacao->find('list'));
        $this->set('modalidades', $this->Evento->Modalidade->find('list'));
        
        $this->set('eventos', $this->paginate($criteria));
    }

    function detalha($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        
        if (empty($this->data)) {
            $this->Evento->recursive = 2;
            $this->data = $this->Evento->read(null, $id);
        }
        
        $this->set(compact('id'));
        $this->set('evento', $this->data);
        $this->set('icEvento', $this->Evento->icEvento);
        $this->set('sexos', $this->Evento->icSexo);
        $this->set('tiposModalidade', $this->Evento->Modalidade->tiposModalidade);
    }
    
    private function prepararCampos()
    {
        if ( $this->data['Evento']['ic_evento'] == "N" ) {
            unset($this->data['Evento']['paises_evento']);
            unset($this->data['Evento']['paises_prova']);
        } else {
            unset($this->data['Evento']['ufs_evento']);
            unset($this->data['Evento']['ufs_prova']);
        }
        
        if( !empty( $this->data['Evento']['ufs_evento'] ) ) {
            foreach ($this->data['Evento']['ufs_evento'] as $ufEvento):
                $this->data['EventoCompetidor'][]   = array('sg_uf'=>$ufEvento);
            endforeach;
        }
        if( !empty( $this->data['Evento']['ufs_prova'] ) ) {
            foreach ($this->data['Evento']['ufs_prova'] as $ufProva):
                $this->data['EventoParticipante'][]   = array('sg_uf'=>$ufProva);
            endforeach;
        }
        if( !empty( $this->data['Evento']['paises_evento'] ) ) {
            foreach ($this->data['Evento']['paises_evento'] as $paisEvento):
                $this->data['EventoCompetidor'][]   = array('co_pais'=>$paisEvento);
            endforeach;
        }
        if( !empty( $this->data['Evento']['paises_prova'] ) ) {
            foreach ($this->data['Evento']['paises_prova'] as $paisProva):
                $this->data['EventoParticipante'][]   = array('co_pais'=>$paisProva);
            endforeach;
        }
    }
    
    private function prepararCamposEdit()
    {
        if ( $this->data['Evento']['ic_evento'] == "N" ) {
            foreach ($this->data['EventoCompetidor'] as $paisEvento):
                $this->data['Evento']['ufs_evento'][]   = $paisEvento['co_pais'];
            endforeach;
            foreach ($this->data['EventoParticipante'] as $paisProva):
                $this->data['Evento']['ufs_prova'][]   = $paisProva['co_pais'];
            endforeach;
        } else {
            foreach ($this->data['EventoCompetidor'] as $paisEvento):
                $this->data['Evento']['paises_evento'][]   = $paisEvento['co_pais'];
            endforeach;
            foreach ($this->data['EventoParticipante'] as $paisProva):
                $this->data['Evento']['paises_prova'][]   = $paisProva['co_pais'];
            endforeach;
        }
    }

    function add()
    {
        $modalidades    = array();
        $provas         = array();
        $categoriasProva    = array();
        $subcategoriasProva = array();
        $classificacaoProva = array();
        $sexos              = array();
        if (! empty($this->data)) {
            $this->prepararCampos();
            $this->Evento->create();
            if ($this->Evento->saveAll($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $this->Evento->getInsertID()
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }

            if ($this->data['Evento']['co_confederacao'] > 0) {
                $criterios  = array();
                $criterios['joins'] = array(
                    array('table' => 'modalidades_confederacoes',
                        'alias' => 'ModalidadesConfederacao',
                        'conditions' => array(
                            'ModalidadesConfederacao.co_confederacao = ' . $this->data['Evento']['co_confederacao'],
                            'ModalidadesConfederacao.co_modalidade = Modalidade.co_modalidade'
                        )
                    )
                );
                $modalidades    = $this->Evento->Modalidade->find('list', $criterios);
            }
            
            if ($this->data['Evento']['co_modalidade'] > 0) {
                $provas = $this->Evento->Prova->find('list', array( 'conditions' => array('co_modalidade' => $this->data['Evento']['co_modalidade']) ) );
            }
            if ($this->data['Evento']['co_prova'] > 0) {
                $categoriasProva    = $this->Evento->ProvaCategoria->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
                $subcategoriasProva = $this->Evento->ProvaSubCategoria->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
                $classificacaoProva = $this->Evento->ProvaClassificacao->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
            }
        }
        
        App::import('Model', 'Uf');
        App::import('Model', 'Pais');
        $ufDb   = new Uf();
        $paisDb = new Pais();
        
        $this->set('ufs', $ufDb->find('list'));
        $this->set('paises', $paisDb->find('list'));
        $this->set('confederacoes', $this->Evento->Confederacao->find('list', array('conditions' => 'parent_id is null') ));
        $this->set('tipos', $this->Evento->TipoEvento->find('list'));
        $this->set('icEvento', $this->Evento->icEvento);
        $this->set('modalidades', $modalidades);
        $this->set('provas', $provas);
        $this->set('categoriasProva', $categoriasProva);
        $this->set('subcategoriasProva', $subcategoriasProva);
        $this->set('classificacaoProva', $classificacaoProva);
        //$this->set('sexos', $sexos);
        
        if (!$this->data['Evento']['nu_ano']) {
            $this->data['Evento']['nu_ano'] = date('Y');
        }
        
    }

    function edit($id = null)
    {
        $modalidades    = array();
        $provas         = array();
        $categoriasProva    = array();
        $subcategoriasProva = array();
        $classificacaoProva = array();
        $sexos              = array();
        
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->prepararCampos();
            $this->Evento->EventoParticipante->deleteAll(array('co_evento'=>$id));
            $this->Evento->EventoCompetidor->deleteAll(array('co_evento'=>$id));
            if ($this->Evento->saveAll($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'detalha',
                    $id
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Evento->read(null, $id);
            $this->prepararCamposEdit();
        }
        
        if ($this->data['Evento']['co_confederacao'] > 0) {
            $criterios  = array();
            $criterios['joins'] = array(
                array('table' => 'modalidades_confederacoes',
                    'alias' => 'ModalidadesConfederacao',
                    'conditions' => array(
                        'ModalidadesConfederacao.co_confederacao = ' . $this->data['Evento']['co_confederacao'],
                        'ModalidadesConfederacao.co_modalidade = Modalidade.co_modalidade'
                    )
                )
            );
            $modalidades    = $this->Evento->Modalidade->find('list', $criterios);
        }

        if ($this->data['Evento']['co_modalidade'] > 0) {
            $provas = $this->Evento->Prova->find('list', array( 'conditions' => array('co_modalidade' => $this->data['Evento']['co_modalidade']) ) );
        }
        if ($this->data['Evento']['co_prova'] > 0) {
            $categoriasProva    = $this->Evento->ProvaCategoria->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
            $subcategoriasProva = $this->Evento->ProvaSubCategoria->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
            $classificacaoProva = $this->Evento->ProvaClassificacao->find('list', array( 'conditions' => array('co_prova' => $this->data['Evento']['co_prova']) ) );
            $is_sexo    = $this->Evento->Prova->field( 'is_sexo', array( 'Prova.co_prova' => $this->data['Evento']['co_prova']) );
            if($is_sexo == "1") {
                $sexos = $this->Evento->icSexo;
            }
        }
        
        App::import('Model', 'Uf');
        App::import('Model', 'Pais');
        $ufDb   = new Uf();
        $paisDb = new Pais();
        
        $this->set('ufs', $ufDb->find('list'));
        $this->set('paises', $paisDb->find('list'));
        $this->set('confederacoes', $this->Evento->Confederacao->find('list', array('conditions' => 'parent_id is null') ));
        $this->set('tipos', $this->Evento->TipoEvento->find('list'));
        $this->set('icEvento', $this->Evento->icEvento);
        $this->set('modalidades', $modalidades);
        $this->set('provas', $provas);
        $this->set('categoriasProva', $categoriasProva);
        $this->set('subcategoriasProva', $subcategoriasProva);
        $this->set('classificacaoProva', $classificacaoProva);
        $this->set('sexos', $sexos);
    }

    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Evento->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    function listar($co_prova, $tp_sexo)
    {
        $this->Evento->recursive = 0;
        
        echo json_encode($this->Evento->find('list', array(
            'conditions' => array(
                'co_prova' => $co_prova,
                'nu_ano' => date('Y'),
                'OR' => array( 
                            array('ic_sexo  is null'), 
                            array('ic_sexo' => ''), 
                            array('ic_sexo' => $tp_sexo) 
                    )
            )
        )));
        
        exit();
    }
}
?>