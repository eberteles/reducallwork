<?php
class EventosResultadosController extends AppController
{

    var $name = 'EventosResultados';

    var $layout = 'iframe';
    var $uses = array('EventoResultado');
    

    function index($coEvento)
    {
        // $this->EventosResultado->recursive = 0;
        
        // $this->virtualFields['tt_pedido'] = 'sum( Pedido.nu_quantidade ) AS tt_pedido';
        $this->paginate = array(
            'limit' => 10000,
            'conditions' => array(
                'EventoResultado.co_evento' => $coEvento
            ),
            'order' => array(
                'EventoResultado.nu_classificacao' => 'ASC'
            )
        );
        
        $this->set('resultados', $this->paginate());
        
        $this->set(compact('coEvento'));
    }

    function iframe($coEvento)
    {
        $this->set(compact('coEvento'));
    }

    function add($coEvento)
    {
        if (! empty($this->data)) {
            if( $this->data['EventoResultado']['co_fornecedor'] <= 0 ) {
                App::import('Model', 'Fornecedor');
                $fornecedorDb   = new Fornecedor();
                $fornecedorDb->create();
                
                $fornecedor = array('Fornecedor'=>array( 
                    'tp_fornecedor'=>'F', 
                    'nu_cnpj'=> $this->Functions->limparMascara($this->data['EventoResultado']['nu_cpf']), 
                    'no_razao_social'=> $this->data['EventoResultado']['no_atleta'] ));
                $fornecedorDb->save($fornecedor, false, array('tp_fornecedor', 'nu_cnpj', 'no_razao_social'));
                $this->data['EventoResultado']['co_fornecedor'] = $fornecedorDb->getInsertID();
            }
            
            $this->EventoResultado->create();
            if ($this->EventoResultado->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coEvento
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coEvento'));

    }

    function delete($id = null, $coEvento)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coEvento
            ));
        }
        
        if ($this->EventoResultado->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coEvento
            ));
        }
        
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coEvento
        ));
    }
}
?>