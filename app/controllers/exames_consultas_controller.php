<?php
class ExamesConsultasController extends AppController
{
    public $name = "ExamesConsultas";
    public $uses = array(
        'ExameConsulta'
    );
 
    function index()
    {
        $this->loadModel('Especialidade');
        $criteria = null;

        if(!empty($this->data)) {
            if(!empty($this->data['ExameConsulta']['nome_ex'])) {
                $criteria['nome like'] = '%' . $this->data['ExameConsulta']['nome_ex'] . '%';
            }
            if(!empty($this->data['ExameConsulta']['codigo_ex'])) {
                $criteria['codigo like'] = '%' . $this->data['ExameConsulta']['codigo_ex'] . '%';
            }
            if(!empty($this->data['ExameConsulta']['especialidade'])) {
                $criteria['ExameConsulta.co_especialidade'] = $this->data['ExameConsulta']['especialidade'];
            }
        }

        $this->set('especialidades', $this->Especialidade->find('list'));
        $this->set('exames', $this->paginate($criteria));
    }

    function add()
    {
        $this->loadModel('Especialidade');

        if(!empty($this->data)) {
            $this->ExameConsulta->create();
            if($this->ExameConsulta->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('especialidades', $this->Especialidade->find('list'));
    }

    function edit($coExame = null)
    {
        $this->loadModel('Especialidade');
        if (! $coExame && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->ExameConsulta->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('especialidades', $this->Especialidade->find('list'));
        $this->data = $this->ExameConsulta->read(null, $coExame);
    }

    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if($id) {
            $this->ExameConsulta->id = $id;
            $msg = null;

            $exame['ExameConsulta']['ic_ativo'] = 0;

            if($this->ExameConsulta->saveField('ic_ativo', 0)) {
                $msg = "Exame/Consulta bloqueada com sucesso!";
            } else {
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}