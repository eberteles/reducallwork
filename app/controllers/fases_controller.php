<?php
/**
 * @resource { "name" : "Cadastro de Fases de Tramitação", "route":"fases", "access": "private", "type": "module" }
 */
class FasesController extends AppController
{

    var $name = 'Fases';

    var $uses = array(
        'Fase'
    );

    /**
        *@resource { "name" : "Fases de Tramitação", "route":"fases\/index", "access": "private", "type": "select" }
    */
    function index()
    {
        $this->gerarFiltro();
        $criteria = null;
        if (! empty($this->data)) {
            $criteria['ds_fase like'] = '%' . up($this->data['Fases']['ds_fase']) . '%';
        }
        //$this->Fase->recursive = 0;
        $this->set('fases', $this->paginate($criteria));
    }

    /**
        *@resource { "name" : "Trocar Fase", "route":"fases\/trocar", "access": "private", "type": "update" }
    */
    function trocar($co_fase_atual, $sequencia_atual, $co_fase_troca, $sequencia_troca)
    {
        $this->Fase->create();
        $this->Fase->id = $co_fase_atual;
        $this->Fase->saveField("ic_sequencia", $sequencia_atual);
        
        $this->Fase->create();
        $this->Fase->id = $co_fase_troca;
        $this->Fase->saveField("ic_sequencia", $sequencia_troca);
        
        return false;
    }

    /**
        *@resource { "name" : "Descer Fases", "route":"fases\/descer", "access": "private", "type": "update" }
    */
    function descer($sequencia)
    {
        $sequencia_pos = $sequencia + 1;
        $fase_posterior = $this->Fase->find('first', array(
            'fields' => array(
                'co_fase',
                'ic_sequencia'
            ),
            'conditions' => ' Fase.ic_sequencia = ' . $sequencia_pos
        ));
        if (count($fase_posterior, COUNT_RECURSIVE) > 1) {
            $fase_atual = $this->Fase->find('first', array(
                'fields' => array(
                    'co_fase',
                    'ic_sequencia'
                ),
                'conditions' => ' Fase.ic_sequencia = ' . $sequencia
            ));
            
            $this->Fase->id = $fase_posterior['Fase']['co_fase'];
            $sequencia_atz = $fase_posterior['Fase']['ic_sequencia'];
            $sequencia_upd = $fase_posterior['Fase']['ic_sequencia'] - 1;
            // $this->Fase->saveField( "ic_sequencia", $sequencia_upd );
            
            $this->Fase->id = $fase_atual['Fase']['co_fase'];
            // $this->Fase->saveField( "ic_sequencia", $sequencia_atz );
            
            debug('Pos: ' . $sequencia_upd . 'Atl: ' . $sequencia_atz);
            die();
        }
        return false;
    }
    
    /**
        *@resource { "name" : "Subir Fases", "route":"fases\/subir", "access": "private", "type": "update" }
    */
    function subir($sequencia)
    {
        $sequencia_ant = $sequencia - 1;
        $fase_anterior = $this->Fase->find('first', array(
            'fields' => array(
                'co_fase',
                'ic_sequencia'
            ),
            'conditions' => ' Fase.ic_sequencia = ' . $sequencia_ant
        ));
        
        if (count($fase_anterior) > 0) {
            $fase_atual = $this->Fase->find('first', array(
                'fields' => array(
                    'co_fase',
                    'ic_sequencia'
                ),
                'conditions' => ' Fase.ic_sequencia = ' . $sequencia
            ));
            
            $this->Fase->id = $fase_anterior['Fase']['co_fase'];
            $sequencia_atz = $fase_anterior['Fase']['ic_sequencia'];
            $sequencia_upd = $fase_anterior['Fase']['ic_sequencia'] + 1;
            $this->Fase->saveField("ic_sequencia", $sequencia_upd);
            
            $this->Fase->id = $fase_atual['Fase']['co_fase'];
            $this->Fase->saveField("ic_sequencia", $sequencia_atz);
        }
        return false;
    }

    private function setMmDuracaoFase()
    {
        $this->data['Fase']['mm_duracao_fase'] = 0;
        if ($this->data['Fase']['nu_dias'] != '' && $this->data['Fase']['nu_dias'] > 0) {
            $this->data['Fase']['mm_duracao_fase'] = 1440 * $this->data['Fase']['nu_dias'];
        }
        if ($this->data['Fase']['nu_horas'] != '') {
            $horas = (int) substr($this->data['Fase']['nu_horas'], 0, 2);
            $minutos = (int) substr($this->data['Fase']['nu_horas'], 3, 2);
            if ($horas > 0) {
                $this->data['Fase']['mm_duracao_fase'] += 60 * $horas;
            }
            if ($minutos > 0) {
                $this->data['Fase']['mm_duracao_fase'] += $minutos;
            }
        }
    }

    /**
        *@resource { "name" : "Nova Fase", "route":"fases\/add", "access": "private", "type": "insert" }
    */
    function add()
    {
        if (! empty($this->data)) {
            $this->setMmDuracaoFase();
            $this->Fase->create();
            if ($this->Fase->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        // $this->set('setores', $this->Fase->Setor->find('list', array('order' => 'ds_setor ASC')));
        $this->set('fases', $this->Fase->find('list'));
    }

    /**
        *@resource { "name" : "Editar Fase", "route":"fases\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->setMmDuracaoFase();
            if ($this->Fase->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fase->read(null, $id);
            
            if ($this->data['Fase']['mm_duracao_fase'] >= 1440) {
                $this->data['Fase']['nu_dias'] = (int) ($this->data['Fase']['mm_duracao_fase'] / 1440);
                $this->data['Fase']['mm_duracao_fase'] -= $this->data['Fase']['nu_dias'] * 1440;
            }
            $horas = 0;
            $minutos = 0;
            if ($this->data['Fase']['mm_duracao_fase'] < 1400 && $this->data['Fase']['mm_duracao_fase'] >= 60) {
                $horas = (int) ($this->data['Fase']['mm_duracao_fase'] / 60);
                $this->data['Fase']['mm_duracao_fase'] -= $horas * 60;
            }
            if ($this->data['Fase']['mm_duracao_fase'] < 60 && $this->data['Fase']['mm_duracao_fase'] > 0) {
                $minutos = (int) ($this->data['Fase']['mm_duracao_fase']);
                $this->data['Fase']['mm_duracao_fase'] -= $minutos;
            }
            $this->data['Fase']['nu_horas'] = date("H:i", strtotime("$horas:$minutos"));
        }
        $this->set('id', $id);
    }

    /**
        *@resource { "name" : "Remover Fase", "route":"fases\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $fase = $this->Fase->find(array('co_fase' => $id));
        }

        $this->loadModel('Fluxo');
        $fluxosUsandoFase = $this->Fluxo->find('all', array(
            'conditions' =>
                array('Fluxo.co_fase' => $id)
        ));

        if(count($fluxosUsandoFase) > 0){
            $this->Session->setFlash('Fase não pode ser excluída pois está vinculada ao Fluxo!');
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        
        if ($this->Fase->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
        *@resource { "name" : "listar fases fluxos", "route":"fases\/listar_fases_fluxo", "access": "private", "type": "select" }
    */
    function listar_fases_fluxo($co_setor)
    {
        $this->Fase->recursive = 0;
        
        echo json_encode($this->Fase->find('list', array(
            'conditions' => 'Fase.co_fase in (select distinct co_fase from fluxos where co_setor = ' . $co_setor . ')'
        )));
        
        exit();
    }
}
?>