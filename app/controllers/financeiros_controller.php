<?php
class FinanceirosController extends AppController
{

    var $name = 'Financeiros';

    var $layout = 'iframe';

    function index($co_contrato)
    {
        App::import('Model', 'Fornecedor');
        $fornecedores = new Fornecedor();
        $fornecedor = $fornecedores->getFornecedor($co_contrato);
        bcscale(0);
        $nuCnpj = bcadd($fornecedor[0]['Fornecedor']['nu_cnpj'], 0);
        
        $this->Financeiro->recursive = 0;
        
        $conditions = array();
        if ($this->data['Financeiro']['fg_notafiscal']) {
            $conditions[] = array(
                'num_nf NOT LIKE ' => 'PRV%'
            );
        }
        if ($this->data['Financeiro']['tp_quitacao']) {
            $conditions[] = array(
                'tp_quitacao' => $this->data['Financeiro']['tp_quitacao']
            );
        }
        $conditions[] = array(
            'nr_cgc_cpf' => $nuCnpj
        );
        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array(
                'dt_vencimento' => 'desc'
            )
        );
        
        $this->set('financeiros', $this->paginate());
        
        $this->set('fornecedor', $fornecedor[0]['Fornecedor']);
        
        $this->set('co_contrato', $co_contrato);
        
        $this->set('tpSituacao', array(
            'C' => 'Comprometido',
            'Q' => 'Quitado',
            'V' => 'Vencido'
        ));        
    }

    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }
}
?>