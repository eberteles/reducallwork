<?php
/**
    *@resource { "name" : "Cadastro de Fiscais", "route":"fiscais", "access": "private", "type": "module" }
*/
class FiscaisController extends AppController
{

    var $name = 'Fiscais';

    var $uses = array(
        'Fiscal'
    );

    var $arSituacoes = array(
        'A' => 'Ativo',
        'I' => 'Inativo'
    );

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['Fiscal']['nu_cpf_fiscal']);
        $this->Functions->limparMascara($this->data['Fiscal']['nu_telefone']);
    }

    /**
        *@resource { "name" : "Fiscais", "route":"fiscais\/index", "access": "private", "type": "index" }
    */
    function index($coContrato = null)
    {
        $this->gerarFiltro();
        $this->Fiscal->recursive = 0;
        $this->Fiscal->validate = array();
        
        $this->paginate = array(
            'limit' => 10
        );
        $criteria = null;
        if (! empty($this->data)) {
            if ($this->data['Fiscal']['no_fiscal']) {
                $criteria['no_fiscal like'] = '%' . up($this->data['Fiscal']['no_fiscal']) . '%';
            }
        }
        $this->Fiscal->recursive = 0;
        
        $this->set('fiscais', $this->paginate($criteria));
        
        $this->set('situacoes', $this->arSituacoes);
    }

    /**
        *@resource { "name" : "Novo Fiscal", "route":"fiscais\/add", "access": "private", "type": "insert" }
    */
    function add()
    {
        if (! empty($this->data)) {
            $this->Fiscal->create();
            $this->prepararCampos();
            if ($this->Fiscal->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $setores = $this->Fiscal->Setor->find('list');
        $this->set(compact('setores'));
        $this->set('situacoes', $this->arSituacoes);
    }

   /**
    * @resource { "name" : "Editar Fiscal", "route":"fiscais\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->prepararCampos();
            if ($this->Fiscal->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fiscal->read(null, $id);
        }
        $setores = $this->Fiscal->Setor->find('list');
        $this->set(compact('setores'));
        $this->set('situacoes', $this->arSituacoes);
        $this->set('id', $id);
    }

   /**
    * @resource { "name" : "Remover Fiscal", "route":"fiscais\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Fiscal->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>