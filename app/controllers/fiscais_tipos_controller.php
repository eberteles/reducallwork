<?php

/**
 * @resource { "name" : "Cadastro de tipo de fiscal", "route":"fiscais_tipos", "access": "private", "type": "module" }
 */
class FiscaisTiposController extends AppController
{

    var $name = 'FiscaisTipos';

    var $uses = array('FiscalTipo');

    /**
     * @resource { "name" : "Cadastro de tipo de fiscal", "route":"fiscais_tipos\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        $this->FiscalTipo->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'FiscalTipo.ic_ativo' => 1
            )
        );

        $this->set('fiscalTipo', $this->paginate());
    }

    /**
     * @resource { "name" : "Novo Tipo Fiscal", "route":"fiscais_tipos\/add", "access": "private", "type": "insert" }
     */
    public function add()
    {

        if (!empty($this->data)) {
            $this->FiscalTipo->create();
            if ($this->FiscalTipo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

    }

    /**
     * @resource { "name" : "Editar Tipo Fiscal", "route":"fiscais_tipos\/edit", "access": "private", "type": "updade" }
     */
    function edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->FiscalTipo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->FiscalTipo->read(null, $id);
        }
        $this->set('id', $id);
    }

    /**
     * @resource { "name" : "Remover Tipo Fiscal", "route":"fiscais_tipos\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'index'));
        }

        if (!$this->FiscalTipo->canDelete($id)) {
            $tipoFiscal = $this->FiscalTipo->read(null, $id);
            $tipoFiscal['FiscalTipo']['ic_ativo'] = 0;

            if ($this->FiscalTipo->save($tipoFiscal, false, array('ic_ativo'))) {
                $this->Session->setFlash(__('Registro excluído com sucesso', true));
                $this->redirect(array('action' => 'index'));
            }
        } else if ($this->FiscalTipo->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array('action' => 'index'));
    }

}
