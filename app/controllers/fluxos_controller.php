<?php
/**
 * @resource { "name" : "WorkFlow", "route":"fluxos", "access": "private", "type": "module" }
 */
class FluxosController extends AppController
{

    var $name = 'Fluxos';
    
    //var $layout = 'default';

    var $uses = array(
        'Fluxo'
    );
    
    public $helpers = array(
        'Imprimir'
    );
    
    var $tpOperadores = null;
    
    function __construct()
    {
        App::import('Helper', 'Modulo');
        $this->Modulo = new ModuloHelper();

        parent::__construct();
    }

    /**
     * @resource { "name" : "WorkFlow", "route":"fluxos\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->set('fluxos', $this->Fluxo->find('all'));
        $this->set('tpOperadores', $this->Modulo->getOperadoresPadrao());
    }

   /**
    * @resource { "name" : "Novo Fluxo", "route":"fluxos\/add", "access": "private", "type": "insert" }
    */    
    function add()
    {
        if (! empty($this->data)) {
            $ultimoFluxo    = $this->Fluxo->find('first', array('fields'=>array('MAX(Fluxo.nu_sequencia) AS max_nu_sequencia')));
            $this->data['Fluxo']['nu_sequencia'] = $ultimoFluxo[0]['max_nu_sequencia'] + 1;
            $this->Fluxo->create();
            if ($this->Fluxo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('tpOperadores', $this->getTpOperadoresList());
        $this->set('fases', $this->Fluxo->Fase->find('list'));
        $this->set('setores', $this->Fluxo->Setor->find('threaded', array(
            'order' => 'ds_setor ASC'
        )));

        $list = array();

        foreach($this->getFluxosDsOperadores() as $row) {
            $id = $row['Fluxo']['nu_sequencia'];
            if($row['Fluxo']['ds_list'] != null && $row['Fluxo']['ds_list'] != ''){
                $name = $row['Fluxo']['nu_sequencia'] . ' - ' . $row['Fluxo']['ds_list'];
            } else {
                $name = $row['Fluxo']['nu_sequencia'] . ' - ' . $row['Fase']['ds_fase'];
            }
            $list[$id] = $name;
        }


        $this->set('fluxos', $list);
    }
    
    private function getTpOperadoresList() {
        $listOperadores = array();
        foreach($this->Modulo->getOperadoresPadrao() as $chave => $valor ) {
            $listOperadores[$chave] = $valor['nome'];
        }
        return $listOperadores;
    }

    function getFluxosDsOperadores() {
//        $this->Fluxo->recursive = -1;
        $fluxos = $this->Fluxo->find('all');
        $operadores = $this->Modulo->getOperadoresPadrao();

        foreach($fluxos as $k => $fluxo) {
            if($fluxo['Fluxo']['tp_operador'] != null && $fluxo['Fluxo']['tp_operador'] != ''){
                $fluxo['Fluxo']['ds_list'] = $operadores[$fluxo['Fluxo']['tp_operador']]['nome'];
                $fluxo['Fluxo']['ds_fase'] = $fluxo['Fase']['ds_fase'];
                unset($fluxo['Fase']);
                unset($fluxo['Setor']);
                $fluxos[$k] = $fluxo;
            }
        }

        return $fluxos;
    }

   /**
	* @resource { "name" : "Editar Fluxo", "route":"fluxos\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Fluxo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fluxo->read(null, $id);
        }
        $this->set('id', $id);
        $this->set('tpOperadores', $this->getTpOperadoresList());
        $this->set('fases', $this->Fluxo->Fase->find('list'));
        $this->set('setores', $this->Fluxo->Setor->find('threaded', array(
            'order' => 'ds_setor ASC'
        )));

        foreach($this->getFluxosDsOperadores() as $row) {
            $id = $row['Fluxo']['nu_sequencia'];
            if($row['Fluxo']['ds_list'] != null && $row['Fluxo']['ds_list'] != ''){
                $name = $row['Fluxo']['nu_sequencia'] . ' - ' . $row['Fluxo']['ds_list'];
            } else {
                $name = $row['Fluxo']['nu_sequencia'] . ' - ' . $row['Fase']['ds_fase'];
            }
            $list[$id] = $name;
        }


        $this->set('fluxos', $list);
    }
    /**
        *@resource { "name" : "Remover Fluxo", "route":"fluxos\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $fluxo = $this->Fluxo->find(array('nu_sequencia' => $id));
        }

        if ($this->Fluxo->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($fluxo['Fluxo']);
            $log['ds_tabela'] = 'fluxos';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

}
?>