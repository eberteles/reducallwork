<?php
/**
    *@resource { "name" : "Cadastro de Fornecedores", "route":"fornecedores", "access": "private", "type": "module" }
*/
class FornecedoresController extends AppController
{
    var $components = array ('Downloader');

    var $name = 'Fornecedores';
    // var $layout = 'modal';
    var $helpers = array(
        'ImprimirArea'
    );

    private $indValor = array(
        'PEQUENO' => 'PEQUENO',
        'MÉDIO'   => 'MÉDIO',
        'ALTO'    => 'ALTO'
    );

    private $indImportancia = array(
        'COMUM'          => 'COMUM',
        'NECESSÁRIO'     => 'NECESSÁRIO',
        'IMPRESCINDÍVEL' => 'IMPRESCINDÍVEL'
    );

    private $comboOcsPsa = array(
        'NÃO' => 'NÃO',
        'OCS' => 'OCS',
        'PSA' => 'PSA'
    );

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_cnpj']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_telefone']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_fax']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_cpf_responsavel']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_rg_responsavel']);
        $this->Functions->limparMascara($this->data['Fornecedor']['nu_cep']);
    }

    /**
        *@resource { "name" : "Fornecedores", "route":"fornecedores\/index", "access": "private", "type": "select" }
    */
    function index()
    {
        $this->gerarFiltro();
        $this->Fornecedor->recursive = 0;
        $this->Fornecedor->validate = array();
        $this->paginate = array(
            'limit' => 10
        );
        $criteria = null;

        if (! empty($this->data) && $this->RequestHandler->isPost() || $this->params['named']['page'] != null) {
            // echo '<pre>';print_r($this->data); echo '</pre>';
            $this->prepararCampos();
            if ($this->data['Fornecedor']['tp_fornecedor']) {
                $criteria['tp_fornecedor'] = $this->data['Fornecedor']['tp_fornecedor'];
            }

            if ($this->data['Fornecedor']['no_razao_social']) {
                $criteria['no_razao_social like'] = '%' . up($this->data['Fornecedor']['no_razao_social']) . '%';
            }

            if ($this->data['Fornecedor']['nu_cnpj']) {
                $this->Functions->limparMascara($this->data['Fornecedor']['nu_cnpj']);
                $criteria['nu_cnpj like'] = '%' . up($this->Functions->limparMascara($this->data['Fornecedor']['nu_cnpj'])) . '%';
            }

            if (!empty($this->data['Fornecedor']['co_area'])) {

                $co_area = $this->Fornecedor->Area->find('first', array(
                    'conditions' => array(
                        'co_area' => $this->data['Fornecedor']['co_area']
                    )
                ));
                if (empty($co_area['Area']['parent_id'])) :
                    $criteria['Fornecedor.co_area'] = $co_area['Area']['co_area'];
                 else :
                    $criteria['Fornecedor.co_area'] = array(
                        $co_area['Area']['co_area']
                        //$co_area['Area']['parent_id']
                    );
                endif;
            }
        }else{
            $this->data['Fornecedor'] = '';
        }

        $this->set('indValores', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('areas', $this->Fornecedor->Area->find('threaded'));

        $this->set('fornecedores', $this->paginate($criteria));
    }

    /**
        *@resource { "name" : "Detalhar Fornecedores", "route":"fornecedores\/mostrar", "access": "private", "type": "select" }
    */
    function mostrar( $id = null )
    {
        $fornecedor = $this->Fornecedor->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                'Fornecedor.co_fornecedor' => $id
            )
        ));
        $this->set('fornecedor', $fornecedor);
    }

    /**
        *@resource { "name" : "Fornecedores iframe", "route":"fornecedores\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     *@resource { "name" : "Fechar iframe", "route":"fornecedores\/close", "access": "private", "type": "select" }
     */
    function close( $co_fornecedor )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_fornecedor' ) );
    }

    /**
     *@resource { "name" : "Listar fornecedores json", "route":"fornecedores\/listar", "access": "private", "type": "select" }
     */
    function listar() {

        echo json_encode ( $this->Fornecedor->find ( 'list', array(
            'order' => 'Fornecedor.no_razao_social ASC',
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        ) ) );

        exit ();
    }

    /**
     * @resource { "name" : "Novo Fornecedor", "route":"fornecedores\/add", "access": "private", "type": "insert" }
     */
    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty ( $this->data )) {
            $fornecedor = $this->Fornecedor->findByNuCnpj($this->data['Fornecedor']['nu_cnpj']);
            if( !empty($fornecedor) )
                $this->activateFornecedor($fornecedor['Fornecedor']['co_fornecedor']);
            else{
                $this->Fornecedor->create();
                $this->prepararCampos ();
                if ($this->Fornecedor->save ( $this->data )) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Fornecedor->id ) );
                    } else {
                        $this->Session->setFlash ( __ ( 'Registro salvo com sucesso', true ) );
                        $this->redirect ( array ('action' => 'index' ) );
                    }
                } else {
                    $this->Session->setFlash ( __ ( 'O registro não pode ser salvo. Por favor, tente novamente.', true ) );
                }
            }
        }

        $estados = $this->Fornecedor->Municipio->Uf->find ( 'list' );
        $this->set ( compact ( 'estados' ) );

        $this->set('indValor', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('combo', $this->comboOcsPsa);
        $this->set ( 'bancos',  $this->Fornecedor->Banco->find ( 'list' ) );
        $this->set ( 'areas',  $this->Fornecedor->Area->find ( 'threaded' ) );
        $this->set ( compact ( 'modal' ) );
    }

    /**
        *@resource { "name" : "Editar Fornecedor", "route":"fornecedores\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null) {
        if (! $id && empty ( $this->data )) {
                $this->Session->setFlash ( __ ( 'Invalid fornecedor', true ) );
                $this->redirect ( array ('action' => 'index' ) );
        }
        if (! empty ( $this->data )) {

                $this->prepararCampos ();

                if ($this->Fornecedor->save ( $this->data )) {
                        $this->Session->setFlash ( __ ( 'Registro salvo com sucesso', true ) );
                        $this->redirect ( array ('action' => 'index' ) );
                } else {
                        $this->Session->setFlash ( __ ( 'O registro não pode ser salvo. Por favor, tente novamente.', true ) );
                }
        }
        if (empty ( $this->data )) {
			$this->data = $this->Fornecedor->read ( null, $id );
        }
        $municipios     = array();
        if ($this->data ['Fornecedor'] ['sg_uf'] != "" ) {
            $municipios = $this->Fornecedor->Municipio->find ( 'list', array ('conditions' => array ('sg_uf' => $this->data ['Fornecedor'] ['sg_uf'] ) ) );
        }

        $this->set('indValor', $this->indValor);
        $this->set('indImportancia', $this->indImportancia);
        $this->set('combo', $this->comboOcsPsa);
        $this->set ( compact ( 'municipios' ) );
        $this->set ('id',$id);
        $estados = $this->Fornecedor->Municipio->Uf->find ( 'list' );
        $this->set ( compact ( 'estados' ) );

        $this->set('bancos', $this->Fornecedor->Banco->find('list'));
        $this->set('areas', $this->Fornecedor->Area->find('threaded'));

    }

    /**
	 * @resource { "name" : "Remover Fornecedor", "route":"fornecedores\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $fornecedor = $this->Fornecedor->find(array('co_fornecedor' => $id));
        }

        if ($this->Fornecedor->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($fornecedor['Fornecedor']);
            $log['ds_tabela'] = 'fornecedores';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
        *@resource { "name" : "find fornecedor", "route":"fornecedores\/list_fornecedor", "access": "private", "type": "select" }
    */
    function find_fornecedor() {
        if ( !empty( $this->data ) ) {
            $co_fornecedor  = 0;
            $nu_cnpj    = $this->Functions->limparMascara( $this->data[ 'Fornecedor' ][ 'nu_cnpj' ] );
            if($nu_cnpj > 0) {
                $fornecedor = $this->Fornecedor->find( 'first', array('fields' => array('co_fornecedor'), 'conditions' => ' Fornecedor.nu_cnpj = ' . $nu_cnpj) );
                if(count($fornecedor, COUNT_RECURSIVE) > 1) {
                    $co_fornecedor  = $fornecedor['Fornecedor']['co_fornecedor'];
                }
            }
        }
        echo $co_fornecedor;
        exit();
    }

    /**
        *@resource { "name" : "find fornecedor", "route":"fornecedores\/find", "access": "private", "type": "select" }
    */
    function find($nu_cnpj) {
        $nu_cnpj    = $this->Functions->limparMascara( $nu_cnpj );
        echo json_encode ( $this->Fornecedor->find( 'first', array(
            'fields' => array(
                'co_fornecedor', 'no_razao_social'
            ),
            'conditions' => array(
                ' Fornecedor.nu_cnpj = ' . $nu_cnpj,
                ' Fornecedor.ic_ativo' => 1,
            )
        ) ) );
        exit();
    }

    /**
        *@resource { "name" : "verificar cpf fornecedor", "route":"fornecedores\/verifyCPF", "access": "private", "type": "select" }
    */
    public function verifyCPF($nuCpf){
        $this->Fornecedor->recursive = 0;

        if( $nuCpf <= 0 ) {
        	echo json_encode(array(
        			'color' => 'red',
        			'bold' => 'bold',
        			'message' => 'CPF inválido!.'
        	));
        	exit;
        }

        $existsCPF = $this->Fornecedor->find('count', array(
            'conditions' => array(
                'Fornecedor.nu_cnpj' => $nuCpf,
                'Fornecedor.ic_ativo' => 1
            )
        ));
        if( $existsCPF > 0 ){
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'CNPJ/CPF já cadastrado na base de dados.'
            ));
        }else{
            if($nuCpf == '') {
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'CNPJ/CPF em branco. Campo obrigatório.'
                ));
            } else if(!FunctionsComponent::validaCPF($nuCpf)){
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CNPJ/CPF digitado é inválido! Por favor insira outro.'
                ));
                //echo json_encode("O CPF digitado é inválido! Por favor insira outro.");
            }else{
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CPF válido!'
                ));
                //echo json_encode("CPF válido!");
            }
        }

        exit();
    }

    /**
        *@resource { "name" : "verificar cnpj fornecedor", "route":"fornecedores\/verifyCNPJ", "access": "private", "type": "select" }
    */
    public function verifyCNPJ($nuCnpj){
        $this->Fornecedor->recursive = 0;

        if( $nuCnpj <= 0 ) {
        	echo json_encode(array(
        			'color' => 'red',
        			'bold' => 'bold',
        			'message' => 'CNPJ inválido!.'
        	));
        	exit;
        }
        $existsCNPJ = $this->Fornecedor->find('count', array(
            'conditions' => array(
                'Fornecedor.nu_cnpj' => $nuCnpj,
                'Fornecedor.ic_ativo' => 1
            )
        ));
        if( $existsCNPJ > 0 ){
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'CNPJ/CPF já cadastrado na base de dados.'
            ));
        }else{
            if($nuCnpj == '') {
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'CNPJ/CPF em branco. Campo obrigatório.'
                ));
            } else if(!FunctionsComponent::validaCNPJ($nuCnpj)){
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CNPJ/CPF digitado é inválido! Por favor insira outro.'
                ));
            }else{
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CNPJ válido!'
                ));
            }
        }

        exit();
    }

    /**
     *@resource { "name" : "Remover Fornecedor", "route":"fornecedores\/logicDelete", "access": "private", "type": "update" }
    */
    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Fornecedor->recursive = -1;
        if ($this->Fornecedor->read(null, $id)) {
            $fornecedor = $this->Fornecedor->read(null, $id);
            $msg = null;
            $fornecedor['Fornecedor']['ic_ativo'] = 0;

            unset($this->Fornecedor->validate);

            if($this->Fornecedor->save($fornecedor)){
                $msg = "Registro excluído com sucesso!";
            } else {
                $msg = "Houve um erro na exclusão";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
     *@resource { "name" : "Fornecedores", "route":"fornecedores\/activateFornecedor", "access": "private", "type": "update" }
     */
    public function activateFornecedor( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Fornecedor->read(null, $id)) {
            $fornecedor = $this->Fornecedor->read(null, $id);
            $msg = null;
            $fornecedor['Fornecedor']['ic_ativo'] = 1;

            if($this->Fornecedor->save($fornecedor)){
                $msg = "Registro salvo com sucesso!";
            }else{
                $msg = "Houve um erro no registro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
        *@resource { "name" : "Detalhar Fornecedor", "route":"fornecedores\/detalha", "access": "private", "type": "select" }
    */
    public function detalha($id)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $this->set(compact('id'));

        $fornecedor = $this->Fornecedor->read(null, $id);

        $this->set('fornecedor', $fornecedor);
    }

    private function getFornecedorFromApi($id)
    {
        $url = Configure::read('App.config.api.fornecedores.url');
        $token = Configure::read('App.config.api.fornecedores.token');
        $contentUrl = $url . "/api/v1/fornecedor/" . $id . "?token=" . $token;
        $contents = DownloaderComponent::fetch($contentUrl);

        $jsonApiObject = json_decode($contents, true);

        $nomeFornecedor = $jsonApiObject['data']['attributes']['nome'];
        $tipoFornecedor = (strlen($nomeFornecedor) < 14) ? 'F' : 'J';

        return array(
            "no_razao_social" => $nomeFornecedor,
            "nu_cnpj"   => $jsonApiObject['data']['id'],
            "tp_fornecedor" => $tipoFornecedor
        );
    }

    /**
        *@resource { "name" : "Proxy Fornecedor", "route":"fornecedores\/proxy", "access": "private", "type": "select" }
    */
    public function proxy($id)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $result = $this->Fornecedor->listarFornecedorPorCnpj($id);

        $codigo_fornecedor = $id;

        if (empty($result)) {
            $fornecedor = $this->getFornecedorFromApi($id);

            if ($this->Fornecedor->save($fornecedor)) {
                $codigo_fornecedor = $this->Fornecedor->getInsertID();
            }

        } else {
            $codigo_fornecedor = $result[0]['fornecedores']['co_fornecedor'];
        }

        $this->redirect(array('action' => 'mostrar', $codigo_fornecedor));
    }
}
?>
