<?php
class FuncoesController extends AppController
{

    var $name = 'Funcoes';
        
    function iframe()
    {
        $this->layout = 'blank';
    }

    function close( $co_funcao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_funcao' ) );
    }

    function listar() {

        echo json_encode ( $this->Funcao->find( 'list' , array('order' => 'ds_funcao ASC')) );

        exit ();
    }

    function add($modal = false) 
    {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty($this->data)) {
            $this->Funcao->create();
            if ($this->Funcao->save($this->data)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Funcao->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array(
                            'action' => 'index'
                        ));
                    }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }
}
?>
