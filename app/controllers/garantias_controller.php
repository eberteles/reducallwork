<?php
/**
* @resource { "name": "Garantias", "route":"garantias", "access": "private", "type": "module" }
*/
class GarantiasController extends AppController
{

    var $name = 'Garantias';

    var $layout = 'iframe';

    /**
    * @resource { "name": "Garantias", "route":"garantias\/index", "access": "private", "type": "select" }
    */
    function index($coContrato, $ajax = false)
    {
        $this->Garantia->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Garantia.co_contrato' => $coContrato
            ),
            'order' => array(
                'Garantia.dt_inicio' => 'asc'
            )
        );

        $this->loadModel('Contrato');
        $pcGarantia = $this->Contrato->field('pc_garantia', array('co_contrato' => $coContrato));
        $this->set('garantias', $this->paginate());

        $this->set(compact('coContrato'));
        $this->set(compact('pcGarantia'));
    }

    /**
    * @resource { "name": "iFrame", "route":"garantias\/iframe", "access": "private", "type": "select" }
    */
    function iframe($coContrato)
    {
        $this->layout = 'ajax';
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Nova Garantia", "route":"garantias\/add", "access": "private", "type": "insert" }
    */
    function add($coContrato)
    {
        $this->loadModel('Contrato');

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        if (! empty($this->data)) {
            $this->Garantia->create();
            if ($this->Garantia->save($this->data)) {
                $this->Contrato->id = $coContrato;
                $this->Contrato->saveField('ic_cadastro_garantia', 1);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        if ($modulo->isCadastroGarantia()) {
            $contrato = $this->Contrato->find('first', array(
                'fields' => '(vl_global * (1 + (pc_garantia/100) ) - vl_global) as valor_garantia_permitido',
                'conditions' => array('co_contrato' => $coContrato)
            ));
            $this->set('contrato', $contrato);
        }

        $this->set(compact('coContrato'));

        $modalidadesDoBanco = $this->Garantia->getModalidades();
        $modalidades = array();
        foreach ($modalidadesDoBanco as $modalidade) {
            $modalidades[$modalidade['GarantiaModalidade']['co_modalidade_garantia']] = $modalidade['GarantiaModalidade']['ds_modalidade_garantia'];
        }
        $this->set('modalidades', $modalidades);
    }

    /**
    * @resource { "name": "Editar Garantia", "route":"garantias\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null, $coContrato)
    {
        $this->loadModel('Contrato');
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Garantia->save($this->data)) {
            	$coContrato = $this->data['Garantia']['co_contrato'];
                $this->Contrato->id = $this->data['Garantia']['co_contrato'];
                $this->Contrato->saveField('ic_cadastro_garantia', 1);
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Garantia->read(null, $id);
        }
		
        if ($modulo->isCadastroGarantia()) {
            $contrato = $this->Contrato->find('first', array(
                'fields' => '(vl_global * (1 + (pc_garantia/100) ) - vl_global) as valor_garantia_permitido',
                'conditions' => array('co_contrato' => $coContrato)
            ));
            $this->set('contrato', $contrato);
        }
		
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('garantia', $this->data['Garantia']);
        $modalidadesDoBanco = $this->Garantia->getModalidades();
        $modalidades = array();
        foreach ($modalidadesDoBanco as $modalidade) {
            $modalidades[$modalidade['GarantiaModalidade']['co_modalidade_garantia']] = $modalidade['GarantiaModalidade']['ds_modalidade_garantia'];
        }
        $this->set('modalidades', $modalidades);
    }

    /**
    * @resource { "name": "Remover Garantia", "route":"garantias\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Garantia->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>
