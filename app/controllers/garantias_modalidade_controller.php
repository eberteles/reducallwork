<?php

/**
 * @resource { "name": "GarantiasModalidade", "route":"garantiasModalidade", "access": "private", "type": "module" }
 */
class GarantiasModalidadeController extends AppController
{

    public $name = 'GarantiaModalidade';

    /**
     * @resource { "name": "GarantiasModalidade", "route":"garantiasModalidade\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        $this->set('modalidades', $this->paginate());
//        App::import('Model', 'GarantiaModalidade');
//        $modalidadeDeGarantiaModel = new GarantiaModalidade();
//        $modalidades = $modalidadeDeGarantiaModel->listar();
//        $this->set('modalidades', $modalidades);
    }

    /**
     * @resource { "name": "GarantiasModalidade", "route":"garantiasModalidade\/add", "access": "private", "type": "insert" }
     */
    public function add()
    {
        if (!empty($this->data)) {
            $this->GarantiaModalidade->create();
            if ($this->GarantiaModalidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    /**
     * @resource { "name": "GarantiasModalidade", "route":"garantiasModalidade\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (!empty($this->data)) {
            if ($this->GarantiaModalidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->GarantiaModalidade->read(null, $id);
        }
        $this->set('id', $id);
    }

    /**
     * @resource { "name": "GarantiasModalidade", "route":"garantiasModalidade\/delete", "access": "private", "type": "delete" }
     */
    public function delete($id = null)
    {
        if (!$id) {
            $mensagem = 'Identificador inválido';
        } else {
            $mensagem = $this->GarantiaModalidade->delete($id) ? 'Registro excluído com sucesso' : 'Erro ao excluir registro';
        }
        $this->Session->setFlash(__($mensagem, true));
        $this->redirect(array('action' => 'index'));
    }

}
