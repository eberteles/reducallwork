<?php
class GarantiasSuporteController extends AppController
{

    var $name = 'GarantiasSuporte';
    
    var $uses = array('GarantiaSuporte');

    var $layout = 'iframe';

    function index($coContrato, $ajax = false)
    {
        $this->GarantiaSuporte->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'GarantiaSuporte.co_contrato' => $coContrato
            ),
            'order' => array(
                'GarantiaSuporte.dt_inicio' => 'asc'
            )
        );
        
        $this->set('garantias_suporte', $this->paginate());
        
        $this->set(compact('coContrato'));
    }

    function iframe($coContrato)
    {
        $this->layout = 'ajax';
        $this->set(compact('coContrato'));
    }

    function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->GarantiaSuporte->create();
            if ($this->GarantiaSuporte->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
    }

    function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->GarantiaSuporte->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->GarantiaSuporte->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->GarantiaSuporte->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>