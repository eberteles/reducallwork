<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 15:15
 */

class GraduacaoController extends AppController
{
    var $name = "Graduacao";

    public function index()
    {
        $this->Graduacao->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Graduacao.ic_ativo' => 1
            )
        );
        $criteria = null;

        if(!empty($this->data)) {
            if(isset($this->data['Graduacao']['ds_graduacao'])) {
                $criteria['Graduacao.ds_graduacao'] = $this->data['Graduacao']['ds_graduacao'];
            }
        }

        $this->set('graduacoes', $this->paginate($criteria));
    }

    public function add($iframe = false)
    {
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Graduacao->create();

            $graduacao = $this->Graduacao->findByDsGraduacao($this->data['Graduacao']['ds_graduacao']);
            if(isset($graduacao['Graduacao']) && $graduacao['Graduacao']['ic_ativo'] == 0) {
                $this->reInsert($graduacao['Graduacao']['co_graduacao']);
            }
            if($this->Graduacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Graduacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Graduacao->read(null, $id);
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Graduacao->id = $id;
            $msg = null;
            $graduacao['Graduacao']['ic_ativo'] = 0;

            if($this->Graduacao->saveField('ic_ativo',0)){
                $msg = "Posto / Graduação bloqueado com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Graduacao->id = $id;
            $msg = null;
            $graduacao['Graduacao']['ic_ativo'] = 1;

            if($this->Graduacao->saveField('ic_ativo',1)){
                $msg = "Posto / Graduação cadastrado com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}