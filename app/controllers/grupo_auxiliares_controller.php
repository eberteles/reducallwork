<?php
/**
* @resource { "name": "Cadastro de Grupo Auxiliar", "route":"grupo_auxiliares", "access": "private", "type": "module" }
*/
class GrupoAuxiliaresController extends AppController
{

    public $name = 'GrupoAuxiliar';

    public $uses = array(
         'GrupoAuxiliar'
    );

    public $helpers = array(
        'Imprimir'
    );

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['GrupoAuxiliar']['nu_cpf']);
    }

    public function __construct() {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();
        
        parent::__construct();
    }

    /**
    * @resource { "name": "Grupo Auxiliar", "route":"grupo_auxiliares\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $situacao = null;
        $criteria = array();

        if ($this->RequestHandler->isGet('GET')) {
            if (isset($this->params['url']['ds_nome_auxiliar']) && $this->params['url']['ds_nome_auxiliar']) {
                $criteria['GrupoAuxiliar.ds_nome like'] = '%' . up($this->params['url']['ds_nome_auxiliar']) . '%';
            }

            if (isset($this->params['url']['ic_ativo']) && $this->params['url']['ic_ativo'] != null && $this->params['url']['ic_ativo'] != '') {
                $icAtivo = empty($this->params['url']['ic_ativo']) ? true : $this->params['url']['ic_ativo'];
                $icAtivo = $icAtivo == 'true' ? 0 : 1; 

                $criteria['GrupoAuxiliar.ic_ativo'] = $icAtivo;
                $situacao = $icAtivo;

            }else{
                unset($this->params['url']['ic_ativo']);
            }

            if (isset($this->params['url']['ds_matricula']) && $this->params['url']['ds_matricula']) {
                $criteria['GrupoAuxiliar.nu_cpf like'] = '%' . up($this->Functions->limparMascara($this->params['url']['ds_matricula'])) . '%';
            }

            if (isset($this->params['url']['co_setor_pes']) && $this->params['url']['co_setor_pes'] > 0) {
                $criteria['GrupoAuxiliar.co_setor'] = $this->params['url']['co_setor_pes'];
            }
        } 

		$criteria['GrupoAuxiliar.no_usuario'] = '';
		
        $this->paginate = array(
            'limit' => 10,
            'conditions' => $criteria
        );
        $this->set('setores', $this->GrupoAuxiliar->Setor->find('threaded', array(
            'conditions' => array(
                'Setor.ic_ativo' => 1
            ),
            'order' => 'ds_setor ASC',
        )));

        $this->set('usuarios', $this->paginate($criteria));
    }

    /**
    * @resource { "name": "Detalhar Grupo", "route":"grupo_auxiliares\/detalha", "access": "private", "type": "select" }
    */
    function detalha($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if (empty($this->data)) {
            $this->data = $this->GrupoAuxiliar->read(null, $id);
        }

        $this->set(compact('id'));
        $this->set('grupoAuxiliar', $this->data);
    }

    /**
    * @resource { "name": "Novo Grupo", "route":"grupo_auxiliares\/add", "access": "private", "type": "insert" }
    */
    public function add($cadAuxiliar = false)
    {
        if ($cadAuxiliar == true) {
            $this->layout = 'iframe';
            $this->set('iframeFromSetor', true);
        }

        // verificar se o setor já está vinculado a um usuário
        $this->loadModel('Setor');
        
        if (! empty($this->data)) {
            $this->GrupoAuxiliar->create();
            $this->prepararCampos();
            if ($this->GrupoAuxiliar->saveAll($this->data)) {
                if($cadAuxiliar == true) {
                    $this->redirect ( array ('action' => 'close', $this->GrupoAuxiliar->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $this->GrupoAuxiliar->recursive = 2;
        $this->set('setores', $this->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));
        App::import('Model', 'Perfil');
        $modelPerfil = new Perfil();
        $this->set('perfis', $modelPerfil->find('list', array('conditions' => array("Perfil.no_perfil IN ('Fiscal', 'Gestor')"))));

        $this->set('cadAuxiliar', $cadAuxiliar);
    }


    /**
    * @resource { "name": "Novo Fiscal", "route":"grupo_auxiliares\/fiscal", "access": "private", "type": "insert" }
    */
    public function fiscal($cadAuxiliar = false)
    {

        if($cadAuxiliar) {
            $this->layout = 'iframe';
        }
        if (! empty($this->data)) {
        	$coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
            $this->data['UsuarioPerfil']['co_perfil'] = $coFiscal;
            $this->GrupoAuxiliar->create();
            $this->prepararCampos();

            if ($this->GrupoAuxiliar->saveAll($this->data)) {
                if($cadAuxiliar) {
                    $this->redirect ( array ('action' => 'close', $this->GrupoAuxiliar->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            }
        }
        $this->set ( compact ( 'cadAuxiliar' ) );

        $this->GrupoAuxiliar->recursive = 2;
        $this->set('setores', $this->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));
    }

    /**
     * @resource { "name": "Close iframe", "route":"grupo_auxiliares\/close", "access": "private", "type": "select" }
     */
    public function close( $co_grupo_auxiliar )
    {
        $this->layout = 'iframe';
        $this->Session->setFlash(__('Registro salvo com sucesso', true));
        $this->set ( compact ( 'co_grupo_auxiliar' ) );
    }

    /**
    * @resource { "name": "Adicionar e-mail", "route":"grupo_auxiliares\/addEmail", "access": "private", "type": "insert" }
    */
    public function addEmail( $cpf, $nome, $email, $ck30 = 0, $ck60 = 0, $ck90 = 0, $ck120 = 0 ){
        if( $cpf != null && $nome != null && $email != null ){
            $this->data['GrupoAuxiliar']['nu_cpf']      = $cpf;
            $this->data['GrupoAuxiliar']['ds_nome']     = $nome;
            $this->data['GrupoAuxiliar']['ds_email']    = $email;
            $this->data['GrupoAuxiliar']['ck_aviso30']  = $ck30;
            $this->data['GrupoAuxiliar']['ck_aviso60']  = $ck60;
            $this->data['GrupoAuxiliar']['ck_aviso90']  = $ck90;
            $this->data['GrupoAuxiliar']['ck_aviso120'] = $ck120;
            $this->data['GrupoAuxiliar']['ic_acesso']   = 0;
            if($this->GrupoAuxiliar->save($this->data['GrupoAuxiliar'])){
                echo json_encode(array(
                    'msg' => 'true',
                    'conteudo' => $this->GrupoAuxiliar->find('all', array(
                        'conditions' => array(
                            'GrupoAuxiliar.nu_cpf' => $cpf
                        )
                    ))
                ));
            }else{
                echo json_encode(array(
                    'msg' => 'false'
                ));
            }
        }
        exit();
    }

    /**
    * @resource { "name": "Editar e-mail", "route":"grupo_auxiliares\/editEmail", "access": "private", "type": "update" }
    */
    public function editEmail( $cpf, $nome, $email, $ck30 = 0, $ck60 = 0, $ck90 = 0, $ck120 = 0, $id=null )
    {
        if( $cpf != null && $nome != null && $email != null && $id != null ){
            $this->data = $this->GrupoAuxiliar->read(null, $id);
            $this->data['GrupoAuxiliar']['co_usuario']  = $id;
            $this->data['GrupoAuxiliar']['nu_cpf']      = $cpf;
            $this->data['GrupoAuxiliar']['ds_nome']     = $nome;
            $this->data['GrupoAuxiliar']['ds_email']    = $email;
            $this->data['GrupoAuxiliar']['ck_aviso30']  = $ck30;
            $this->data['GrupoAuxiliar']['ck_aviso60']  = $ck60;
            $this->data['GrupoAuxiliar']['ck_aviso90']  = $ck90;
            $this->data['GrupoAuxiliar']['ck_aviso120'] = $ck120;
            $this->data['GrupoAuxiliar']['ic_acesso']   = 0;

            $this->data['GrupoAuxiliar']['ic_ativo']   = 1;

            unset($this->GrupoAuxiliar->validate['co_cargo']);
            unset($this->GrupoAuxiliar->validate['co_setor']);
            unset($this->GrupoAuxiliar->validate['co_funcao']);
            if( $this->GrupoAuxiliar->save($this->data['GrupoAuxiliar']) ){
                echo json_encode(array(
                    'msg' => 'true',
                    'conteudo' => $this->GrupoAuxiliar->read(null, $id)
                ));
            }else{
                echo json_encode(array(
                    'msg' => 'false',
                ));
            }
        }else{
            echo json_encode(array(
                'msg' => 'cpfnull'
            ));
        }
        exit();
    }

    /**
    * @resource { "name": "Remover e-mail", "route":"grupo_auxiliares\/deleteemail", "access": "private", "type": "delete" }
    */
    public function deleteemail($id = null)
    {
        if($id !== null)
        {
            $this->GrupoAuxiliar->id = $id;
            if($this->GrupoAuxiliar->saveField('ic_ativo', 0)) {
                echo "true";
            } else {
                echo "false";
            }
        }
        else
        {
            echo "false";
        }
        exit();
    }


    /**
    * @resource { "name": "Carregar e-mails", "route":"grupo_auxiliares\/loadEmails", "access": "private", "type": "select" }
    */
    public function loadEmails()
    {
        $emails = $this->GrupoAuxiliar->find('all', array(
            'conditions' => array(
                'GrupoAuxiliar.ic_acesso' => 0,
                'GrupoAuxiliar.ic_ativo' => 1
            )
        ));

        if(isset($emails)){
            echo json_encode(array(
                'emails' => $emails,
                'msg' => 'true'
            ));
        }else{
            echo json_encode(array(
               'msg' => 'false'
            ));
        }

        exit();
    }

    /**
    * @resource { "name": "Editar Grupo Auxiliar", "route":"grupo_auxiliares\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        
        if (! empty($this->data)) {
            $this->prepararCampos();

            if(!empty($this->data['GrupoAuxiliar']['co_usuario'])){

                App::import('Model', 'UsuarioPerfil');
                $modelUsuarioPerfil = new UsuarioPerfil();
                $modelUsuarioPerfil->recursive = -1;
                $atualizar = $modelUsuarioPerfil->find('first', array(
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario' => $this->data['GrupoAuxiliar']['co_usuario']
                        )
                    )
                );

                $this->data['UsuarioPerfil']['co_usuario_perfil'] = $atualizar['UsuarioPerfil']['co_usuario_perfil'];

                if ($this->GrupoAuxiliar->saveAll($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }

            // verificar se o setor já está vinculado a um usuário
            $this->loadModel('Setor');
            if ($this->GrupoAuxiliar->saveAll($this->data)) {                
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        if (empty($this->data)) {
            $this->data = $this->GrupoAuxiliar->read(null, $id);
        }
        
        $this->set('setores', $this->GrupoAuxiliar->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));
        App::import('Model', 'Perfil');
        $modelPerfil = new Perfil();
        $this->set('perfis', $modelPerfil->find('list', array('conditions' => array("Perfil.no_perfil IN ('Fiscal', 'Gestor')"))));
            
        
    }

    /**
    * @resource { "name": "Remover Grupo Auxiliar", "route":"grupo_auxiliares\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($id) {
            if ($this->GrupoAuxiliar->read(array('GrupoAuxiliar.ic_ativo' => 0), $id)) {
                $usuario = $this->GrupoAuxiliar->read(array('GrupoAuxiliar.ic_ativo' => 0), $id);
                // Bloqueia o usuário
                $usuario['GrupoAuxiliar']['ic_ativo'] = 0;
                
                $this->GrupoAuxiliar->save($usuario);
                $this->Session->setFlash(__('Usuário bloqueado com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            }
            
            $this->Session->setFlash(__('Erro ao excluir registro', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Remoção Lógica", "route":"grupo_auxiliares\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete( $id = null ){    	
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $msg = null;
            
            $grupoAuxiliar = $this->GrupoAuxiliar->findByCoUsuario($id);            
			
            if( $grupoAuxiliar['GrupoAuxiliar']['ic_ativo'] == 1 ) {
            	$grupoAuxiliar['GrupoAuxiliar']['ic_ativo'] = 0;
            } else {
            	$msg = "Auxiliar já bloqueado!";
            }
            
            if( $this->GrupoAuxiliar->save($grupoAuxiliar) ) {
                $msg = "Auxiliar Bloqueado com sucesso!";
            } else {
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Desbloquear Grupo Auxiliar", "route":"grupo_auxiliares\/desbloquear", "access": "private", "type": "update" }
    */
    public function desbloquear( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->GrupoAuxiliar->id = $id;
            $msg = null;

            $grupoAuxiliar = $this->GrupoAuxiliar->findByCoUsuario($id);
            	
            if( $grupoAuxiliar['GrupoAuxiliar']['ic_ativo'] == 0 ) {
            	$grupoAuxiliar['GrupoAuxiliar']['ic_ativo'] = 1;
            } else {
            	$msg = "Auxiliar já desbloqueado!";
            }
            
            if( $this->GrupoAuxiliar->save($grupoAuxiliar) ) {
            	$msg = "Auxiliar Desbloqueado com sucesso!";
            } else {
                $msg = "Houve um erro no desbloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser desbloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Contar Usuários Bloqueados", "route":"grupo_auxiliares\/addVerificaUsuarioBloqueado", "access": "private", "type": "select" }
    */
    public function addVerificaUsuarioBloqueado()
    {
        $this->autoRender = false;
        $cpf = $this->data['GrupoAuxiliar']['nu_cpf'];
        $usuario = $this->GrupoAuxiliar->find('all', array(
            'fields' => array(
                'nu_cpf'
            ),
            'conditions' => array(
                array(
                    'nu_cpf' => $cpf,
                    'GrupoAuxiliar.ic_acesso' => 0
                )
            )
        )
        );
        
        return count($usuario);
    }

    /**
    * @resource { "name": "iFrame", "route":"grupo_auxiliares\/iframe", "access": "private", "type": "select" }
    */
    public function iframe($cadAuxiliar = "add"){
        $this->set(compact('cadAuxiliar'));
        $this->set(compact('coAuxiliar'), 1);
    }

    /**
    * @resource { "name": "Valida CPF", "route":"grupo_auxiliares\/verifyCPF", "access": "private", "type": "select" }
    */
    public function verifyCPF($nuCpf)
    {
        $this->GrupoAuxiliar->recursive = 0;

        $existsCPF = $this->GrupoAuxiliar->findByNuCpf($nuCpf);
        if( !empty($existsCPF)){
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'CPF já cadastrado no sistema! Insira outro.',
                'validation' => 'existCPF',
                'cpf' => $existsCPF['GrupoAuxiliar']['nu_cpf'],
                'nome' => $existsCPF['GrupoAuxiliar']['ds_nome'],
                'email' => $existsCPF['GrupoAuxiliar']['ds_email'],
                'id' => $existsCPF['GrupoAuxiliar']['co_usuario'],
                'ck30' => $existsCPF['GrupoAuxiliar']['ck_aviso30'],
                'ck60' => $existsCPF['GrupoAuxiliar']['ck_aviso60'],
                'ck90' => $existsCPF['GrupoAuxiliar']['ck_aviso90'],
                'ck120' => $existsCPF['GrupoAuxiliar']['ck_aviso120'],

                'ativo' => $existsCPF['GrupoAuxiliar']['ic_ativo']
            ));
            //echo json_encode("CPF já cadastrado no sistema! Insira outro.");
        }else{
            // debug('aqui');exit;
            // var_dump(FunctionsComponent::validaCPF($nuCpf));exit;
            if(!FunctionsComponent::validaCPF($nuCpf)){
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CPF digitado é inválido! Por favor insira outro.',
                    'validation' => 'false'
                ));
                //echo json_encode("O CPF digitado é inválido! Por favor insira outro.");
            }else{
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CPF válido!',
                    'validation' => 'true'
                ));
                //echo json_encode("CPF válido!");
            }
        }

        exit();
    }

    /**
    * @resource { "name": "Valida Setor", "route":"grupo_auxiliares\/checksetor", "access": "private", "type": "select" }
    */
    public function checksetor($coSetor) {
        $this->loadModel('Setor');
        $this->Setor->recursive = -1;
        $countSetor = $this->Setor->find('count', array(
            'conditions' => array(
                'Setor.ic_ativo' => 1,
                'Setor.co_usuario >' => 0,
                'Setor.co_setor' => $coSetor 
            )
        ));
       $valid = true;
        
        if ($countSetor) {
            $valid = false;
        } 

        echo json_encode(array('valid' => $valid));
        exit;
    }
}