<?php

/**
 * @resource { "name": "Histórico", "route":"historicos", "access": "private", "type": "module" }
 */
class HistoricosController extends AppController
{

    var $name = 'Historicos';

    var $layout = 'iframe';

    /**
     * @resource { "name": "Históricos", "route":"historicos\/index", "access": "private", "type": "select" }
     */
    function index($coContrato, $modulo = "contrato")
    {
        $this->Historico->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'Historico.dt_historico' => 'desc'
            )
        );
        $conditions = null;
        if ($modulo == "ata") {
            $conditions['Historico.co_ata'] = $coContrato;
        } elseif ($modulo == "evento") {
            $conditions['Historico.co_evento'] = $coContrato;
        } else {
            $conditions['Historico.co_contrato'] = $coContrato;
        }

        $this->set('historicos', $this->paginate($conditions));

        $this->set(compact('coContrato'));

        $this->set(compact('modulo'));
    }

    /**
     * @resource { "name": "iFrame", "route":"historicos\/iframe", "access": "private", "type": "select" }
     */
    function iframe($coContrato, $modulo = "contrato")
    {
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
    }

    /**
     * @resource { "name": "Novo Histórico", "route":"historicos\/add", "access": "private", "type": "insert" }
     */
    function add($coContrato, $modulo = "contrato")
    {
        if (!empty($this->data)) {
            App::import('Helper', 'Modulo');
            App::import('Model', 'Contrato');
            $contrato = new Contrato();
            $moduloHelper = new ModuloHelper();

            if ($modulo == "contrato") { // Pegar a situação do contrato
                $this->data['Historico']['co_situacao'] = $contrato->field('co_situacao', "Contrato.co_contrato = " . $this->data['Historico']['co_contrato']);
            }
          
            $this->Historico->create();
            if ($this->Historico->save($this->data)) {
                $this->addAndamento('Histórico cadastrado: ' . $this->data['Historico']['ds_observacao'], array('model'=>'Historico'));
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index', $coContrato, $modulo));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $tipos = array('Ocorrências Administrativas' => 'Ocorrências Administrativas', 'Ocorrências Técnicas' => 'Ocorrências Técnicas');
        $this->set('tipos', $tipos);

        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
    }

    /**
     * @resource { "name": "Editar Histórico", "route":"historicos\/edit", "access": "private", "type": "update" }
     */
    function edit($id = null, $coContrato, $modulo = "contrato")
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->addAndamento('Histórico atualizado: ' . $this->data['Historico']['ds_observacao'], array('model'=>'Historico'));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo
            ));
        }
        if (!empty($this->data)) {
            if ($this->Historico->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Historico->read(null, $id);
        }

        $tipos = array('Ocorrências Administrativas' => 'Ocorrências Administrativas', 'Ocorrências Técnicas' => 'Ocorrências Técnicas');
        $this->set('tipos', $tipos);

        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set(compact('modulo'));
    }

    /**
     * @resource { "name": "Remover Histórico", "route":"historicos\/delete", "access": "private", "type": "delete" }
     */
    function delete($id = null, $coContrato, $modulo = "contrato")
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Historico->delete($id)) {
            $this->addAndamento('Histórico excluído.', array('model'=>'Historico'));
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato,
            $modulo
        ));
    }
}

?>
