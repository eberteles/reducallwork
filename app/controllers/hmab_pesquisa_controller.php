<?php

class HmabPesquisaController extends AppController
{
    public $name = 'HmabPesquisa';
    public $uses = array(
        'HmabPesquisa'
    );

    function index()
    {
        $this->gerarFiltro();
//        $this->HmabPesquisa->recursive = 1;
        $this->paginate = array(
            'limit' => 10
        );
        $this->loadModel('Bairro');
        $this->loadModel('ExameConsulta');
        $this->loadModel('Fornecedor');
        $this->layout = "hmab_pesquisa";

        $criteria = null;

        if (!empty($this->data)) {
            if (!empty($this->data['Busca']['exame'])) {
                $criteria['HmabPesquisa.co_exames'] = $this->data['Busca']['exame'];
            }
            if (!empty($this->data['Busca']['bairro'])) {
                $criteria['LocalAtendimento.co_bairro'] = $this->data['Busca']['bairro'];
            }
            if (!empty($this->data['Busca']['fornecedor'])) {
                $criteria['LocalAtendimento.co_fornecedor'] = $this->data['Busca']['fornecedor'];
            }
        }

        $this->set('fornecedores', $this->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        )
        );

        $this->ExameConsulta->displayField = 'nome_combo';
        $this->ExameConsulta->virtualFields = array(
            'nome_combo' => "CONCAT( ExameConsulta.codigo, ' - ', ExameConsulta.nome )"
        );
        $this->set('exames', $this->ExameConsulta->find('list'));
        $this->set('bairros', $this->Bairro->find('list'));
        $this->set('locais', $this->paginate($criteria));
    }
}