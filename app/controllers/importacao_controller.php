<?php
/**
* @resource { "name": "Importação", "route":"importacao", "access": "private", "type": "module" }
*/
class ImportacaoController extends AppController
{
	
    var $name = 'Importacao';

    var $uses = array(
            'Importacao',
            'Contrato',
            'Contratacao',
            'Aditivo',
            'Fornecedor',
            'Empenho',
            'Andamento'
    );

    var $regAdd     = 0;
    var $regEdit    = 0;

    /**
    * @resource { "name": "Listagem", "route":"importacao\/index", "access": "private", "type": "select" }
    */
    function index(  )
    {
        if (!empty($this->data)) {

            $this->Importacao->create();
            if( $this->Importacao->saveAll($this->data, array('validate'=>'only')) ) {
                set_time_limit(0);
                $file   = fopen($this->data['Importacao']['conteudo']['tmp_name'], 'r');
                echo '<BR><BR> Iniciando a Importação... <BR><BR>';
                while (($data = fgetcsv($file, 0, ";")) !== FALSE) {
                    $nu_contrato = $this->getCampoFormatado($data[0], __('mascara_contrato1', true));
                    if( is_numeric($nu_contrato) ) {
                        
                        $novoContrato   = null;
                        $andamento      = '';
                        $this->checkNovoContrato($novoContrato, $andamento, $nu_contrato, $this->getCampoFormatado($data[1], __('mascara_processo1', true)) );
                        
                        $novoContrato['ds_objeto']           = ($data[2]);
                        $novoContrato['co_contratacao']      = $this->getModalidade(($data[3]));
                        $novoContrato['nu_licitacao']        = $this->getCampoFormatado($data[4], FunctionsComponent::pegarFormato( 'licitacao' ));
                        $novoContrato['dt_publicacao']       = $data[5];
                        $novoContrato['co_fornecedor']       = $this->getFornecedor($this->Functions->limparMascara($data[6]), ($data[7]));
                        $novoContrato['ds_fundamento_legal'] = ($data[8]);
                        $novoContrato['dt_ini_vigencia']     = $data[9];
                        $novoContrato['dt_fim_vigencia']     = $data[10];
                        $novoContrato['vl_global']           = $data[11];
                        $novoContrato['dt_cadastro']         = DboSource::expression('CURRENT_TIMESTAMP');
                        
                        $this->salvarContrato($novoContrato, $andamento, array($data[12]), array( $data[13], $data[14], $data[15] ));
                    }
                }
                
                $this->Session->setFlash( __( 'Contratos Cadastrados: ' . $this->regAdd . ' Contratos Atualizados: ' . $this->regEdit, true ) );
            }
        }
    }
    
    private function salvarContrato($novoContrato, $andamento, $empenho = null, $aditivo = null) {
        $this->Contrato->create();
        if( $this->Contrato->save( $novoContrato ) ) {
            if($empenho != null) {
                $this->updEmpenho($this->Contrato->id, $empenho[0]);
            }
            if($aditivo != null) {
                $this->updAditivo($this->Contrato->id, $aditivo[0], $aditivo[1], $aditivo[2]);
            }
            $this->addAndamento($this->Contrato->id, $andamento);
            return $this->Contrato->id;
        } else {
            debug('Erro ao cadastrar o Contrato: ');
            debug($novoContrato);
            debug( $this->Contrato->invalidFields() );
            return 0;
        }
    }
    
    private function checkNovoContrato(&$novoContrato, &$andamento, $nu_contrato, $nu_processo = 0, $setor = 0) {
        $conditions = array();

        if($nu_contrato > 0) {
            $conditions['Contrato.nu_contrato'] = $nu_contrato;
            $novoContrato['nu_contrato']        = $nu_contrato;
        }
        if($nu_processo > 0) {
            $conditions['Contrato.nu_processo'] = $nu_processo;
            $novoContrato['nu_processo']        = $nu_processo;
        }
        if($setor > 0) {
            $conditions['Contrato.co_contratante'] = $setor;
            $novoContrato['co_contratante']        = $setor;
        }
        
        $contrato   = $this->Contrato->find( 'first', array('conditions' => $conditions ) );

        if ( !empty( $contrato ) ) {
            $novoContrato['co_contrato'] = $contrato['Contrato']['co_contrato'];
            $this->regEdit++;
            $andamento  = 'Contrato atualizado.';
        } else {
            $this->regAdd++;
            $andamento  = 'Contrato cadastrado.';
        }
    }
    
    private function getModalidade($dsModalidade) {
        $dsModalidade   = trim($dsModalidade);
        $modalidade = $this->Contratacao->find( 'first', array('conditions' => array('Contratacao.ds_contratacao like "%' . up($dsModalidade) . '%"' ) ) );
        if ( !empty( $modalidade ) ) {
            return $modalidade['Contratacao']['co_contratacao'];
        } else {
            $this->Contratacao->create();
            if( $this->Contratacao->save( array('ds_contratacao' => $dsModalidade) ) ) {
                return $this->Contratacao->id;
            }
            debug('Erro ao Cadastrar Modalidade: ' . $dsModalidade );
            debug( $this->Contratacao->invalidFields() );
       }
    }
                
    private function getFornecedor($nuFornecedor, $dsFornecedor) {
        if(strlen($nuFornecedor) > 11 ) {
            $nuFornecedor   = $this->getCampoFormatado($nuFornecedor, FunctionsComponent::pegarFormato( 'cnpj' ));
        }
        $fornecedor = $this->Fornecedor->find( 'first', array('conditions' => array('Fornecedor.nu_cnpj' => $nuFornecedor ) ) );
        if ( !empty( $fornecedor ) ) {
            return $fornecedor['Fornecedor']['co_fornecedor'];
        } else {
            $this->Fornecedor->create();
            $tp_fornecedor  = 'J';
            if(strlen($nuFornecedor) == 11 ) {
                $tp_fornecedor  = 'F';
            }
            if( $this->Fornecedor->save( array('nu_cnpj' => $nuFornecedor, 'tp_fornecedor' => $tp_fornecedor, 'no_razao_social' => $dsFornecedor ) ) ) {
                return $this->Fornecedor->id;
            } else {
                debug('Erro ao Cadastrar Fornecedor: ' . $nuFornecedor . ' - ' . $dsFornecedor );
                debug( $this->Fornecedor->invalidFields() );
            }
        }
    }
    
    private function updEmpenho($coContrato, $nuEmpenho) {
        $notasEmpenho   = explode(",", $nuEmpenho);
        foreach ($notasEmpenho as $nota):
            $nota   = trim($nota);
            if($nota != '') {
                $empenho    = $this->Empenho->find( 'first', array('conditions' => array('Empenho.co_contrato' => $coContrato, 'Empenho.nu_empenho' => $nota ) ) );
                if ( empty( $empenho ) ) {
                    $this->Empenho->create();
                    if( !$this->Empenho->save( array('co_contrato'=>$coContrato, 'nu_empenho' => $nota, 'tp_empenho' => 'O') ) ) {
                        debug('Erro ao Cadastrar Empenho: ' . $coContrato . ' - ' . $nota );
                        debug( $this->Empenho->invalidFields() );
                    }
                }
            }
        endforeach;
    }
    
    private function updAditivo($coContrato, $nuAditivo, $dsAditivo, $dtAditivo, $tpAditivo = 5, $vlAditivo = 0, $dt_fim_vigencia_anterior = null, $dt_prazo = null) {
        $cadastrar  = true;
        if($nuAditivo != '') {
            $aditivo    = $this->Aditivo->find( 'first', array('conditions' => array('Aditivo.co_contrato' => $coContrato, 'Aditivo.no_aditivo' => $nuAditivo ) ) );
            if ( !empty( $aditivo ) ) {
                $cadastrar  = false;
            }
        }
        if($cadastrar) {
                $this->Aditivo->create();
                $this->Aditivo->save( array('co_contrato' => $coContrato, 'tp_aditivo' => $tpAditivo, 'no_aditivo' => $nuAditivo, 
                                            'ds_aditivo' => $dsAditivo, 'dt_aditivo' => $dtAditivo, 'vl_aditivo' => $vlAditivo, 
                                            'dt_fim_vigencia_anterior' => $dt_fim_vigencia_anterior, 'dt_prazo' => $dt_prazo) );
        }
    }
    
    private function addAndamento($coContrato, $mensagem)
    {
        $usuario = $this->Session->read('usuario');
        
        $this->Andamento->create();
        $this->Andamento->save(array('co_contrato'=>$coContrato, 'co_usuario'=>$usuario['Usuario']['co_usuario'], 'dt_andamento'=>date('d/m/Y H:i:s'), 'ds_andamento'=>$mensagem ));
    }
    
    private function getCampoFormatado($campo, $formato) {
        $formato    = $this->Functions->limparMascara($formato);
        $campo      = $this->Functions->limparMascara($campo);
        for ( $i = strlen( $formato ); strlen( $campo ) < $i; ) {
            $campo  = "0" . $campo;
        }
        return $campo;
    }
    
    /**
    * @resource { "name": "Pegar Número do Contrato Formatada", "route":"importacao\/getNuContratoFormatado", "access": "private", "type": "select" }
    */
    function getNuContratoFormatado($nu_contrato) {
        if($nu_contrato != '') {
            list($parte1, $parte2) = split('[/.-]', $nu_contrato);
            $parte1 = $this->getCampoFormatado(preg_replace("/[^0-9]/", "", $parte1), '9999');
            $parte2 = preg_replace("/[^0-9]/", "", $parte2);
            if(strlen($parte2) == 2) {
                $parte2 = '20' . $parte2;
            }
            $nu_contrato = $parte1 . $parte2;
        }
        return $nu_contrato;
    }
    
    /**
    * @resource { "name": "Pegar Número da Licitação Formatado", "route":"importacao\/getNuLicitacaoFormatado", "access": "private", "type": "select" }
    */
    function getNuLicitacaoFormatado($nu_licitacao) {
        if($nu_licitacao != '') {
            list($parte1, $parte2) = split('[/.-]', $nu_licitacao);
            $parte1 = $this->getCampoFormatado(preg_replace("/[^0-9]/", "", $parte1), '99999');
            $parte2 = preg_replace("/[^0-9]/", "", $parte2);
            if(strlen($parte2) == 2) {
                $parte2 = '20' . $parte2;
            }
            return $parte1 . $parte2;
        } else {
            return '';
        }
    }
    
    private function getCoContratante($ds_setor) {
        switch ($ds_setor) {
            case 'DE':
                return 121;
            case 'DOE':
                return 220;
            case 'DU':
                return 122;
            default :
                return 0;
        }
    }
    
    /**
    * @resource { "name": "ERP", "route":"importacao\/erp", "access": "private", "type": "select" }
    */
    function erp() {
        set_time_limit(0);
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        
        App::import('Model', 'NvcpContrato');
        $dbContratoNovacap  = new NvcpContrato();
        
        $contratos  = $dbContratoNovacap->find( 'all', array( 'limit'=>15 ) );
        foreach ($contratos as $contrato) :
            $nu_contrato    = $this->getNuContratoFormatado(trim($contrato['NvcpContrato']['NrContrato']));
            $nu_processo    = $this->getCampoFormatado(trim($contrato['NvcpContrato']['Processo']), __('mascara_processo1', true));
            $co_contratante = $this->getCoContratante(trim($contrato['NvcpContrato']['Diretoria']));
            if( is_numeric($nu_contrato) || is_numeric($nu_processo) ) {

                $novoContrato   = null;
                $andamento      = '';
                $this->checkNovoContrato($novoContrato, $andamento, $nu_contrato, $nu_processo, $co_contratante);

                $novoContrato['ds_objeto']           = $contrato['NvcpContrato']['Objeto'];
                $novoContrato['co_contratacao']      = $this->getModalidade(trim($contrato['NvcpContrato']['ModalidadeContratacao']));
                $novoContrato['nu_licitacao']        = $this->getNuLicitacaoFormatado(trim($contrato['NvcpContrato']['NrLicitacao']));
                $novoContrato['dt_publicacao']       = FunctionsComponent::data( $contrato['NvcpContrato']['DtPublicacao'] );
                $novoContrato['co_fornecedor']       = $this->getFornecedor($this->Functions->limparMascara($contrato['NvcpContrato']['CNPJ']), $contrato['NvcpContrato']['Fornecedor'] );
                $novoContrato['ds_fundamento_legal'] = $contrato['NvcpContrato']['FundamentoLegal'];
                $novoContrato['dt_ini_vigencia']     = FunctionsComponent::data( $contrato['NvcpContrato']['InicioVigencia'] );
                $novoContrato['dt_fim_vigencia']     = FunctionsComponent::data( $contrato['NvcpContrato']['TerminoVigencia'] );
                $novoContrato['vl_global']           = $print->real($contrato['NvcpContrato']['ValorGlobal']);
                $novoContrato['dt_cadastro']         = DboSource::expression('CURRENT_TIMESTAMP');
                
                debug($contrato);
                //$coContrato = $this->salvarContrato($novoContrato, $andamento);
                //if( !isset( $novoContrato['co_contrato'] ) && $coContrato > 0 ) { // Novo Contrato
                if( true ) { // Novo Contrato
                    $this->erpImportarAditivos($contrato['NvcpContrato']['Id'], $coContrato);
                    $this->erpImportarEmpenhos($contrato['NvcpContrato']['Id'], $coContrato);
                    $this->Contrato->updDtFimVigencia($coContrato);
                }
            }
        endforeach;
        exit();
    }
    
    private function erpImportarAditivos($idErp, $coContrato, $dtTerminoVigencia) {
        App::import('Helper', 'Formatacao');
        $formatacao = new NumberHelper();
        
        App::import('Helper', 'Print');
        $print      = new PrintHelper();
        
        App::import('Model', 'NvcpAditivo');
        $dbAditivoNovacap   = new NvcpAditivo();
        
        $aditivos   = $dbAditivoNovacap->find( 'all', array( 
                        'conditions' => array('NvcpAditivo.Id' => $idErp, 'NvcpAditivo.Resultado'=>'AUTORIZADO', 
                                              " (NvcpAditivo.Categoria like '%VIGÊNCIA' OR NvcpAditivo.Categoria = 'VALOR' ) "), 
                        'order' => 'NvcpAditivo.Data' ) );
        foreach ($aditivos as $aditivo) :
            //Resultado: AUTORIZADO | NÃO AUTORIZADO
            //Categoria: EXECUÇÃO | VALOR | VIGÊNCIA | EXECUÇÃO/VIGÊNCIA | CONVÊNIO
            //Tipo: VALOR | PRAZO E VALOR | PRAZO
            if($aditivo['NvcpAditivo']['Data'] == null || $aditivo['NvcpAditivo']['Data'] == '' ) {
                $aditivo['NvcpAditivo']['Data'] = date("d/m/Y");
            }
            if($aditivo['NvcpAditivo']['Valor'] > 0 && $aditivo['NvcpAditivo']['Prazo'] <= 0) { // Aditivo de Valor
                $tipo_aditivo = 1;
                $this->updAditivo($coContrato, null, $aditivo['NvcpAditivo']['Resultado'], $aditivo['NvcpAditivo']['Data'], 1, $formatacao->moeda($aditivo['NvcpAditivo']['Valor']));
            }
            if($aditivo['NvcpAditivo']['Valor'] > 0 && $aditivo['NvcpAditivo']['Prazo'] > 0) { // Aditivo de Valor e Prazo
                $dtInicioVigencia   = $print->proximoDia($dtTerminoVigencia);
                $dtTerminoVigencia  = $print->proximoDia($dtInicioVigencia, $aditivo['NvcpAditivo']['Prazo']);
                $this->updAditivo($coContrato, null, $aditivo['NvcpAditivo']['Resultado'], $aditivo['NvcpAditivo']['Data'], 3, $formatacao->moeda($aditivo['NvcpAditivo']['Valor']), 
                                  $dtInicioVigencia, $dtTerminoVigencia);
            }
            if($aditivo['NvcpAditivo']['Valor'] <= 0 && $aditivo['NvcpAditivo']['Prazo'] > 0) { // Aditivo de Prazo
                $tipo_aditivo = 2;
                $dtInicioVigencia   = $print->proximoDia($dtTerminoVigencia);
                $dtTerminoVigencia  = $print->proximoDia($dtInicioVigencia, $aditivo['NvcpAditivo']['Prazo']);
                $this->updAditivo($coContrato, null, $aditivo['NvcpAditivo']['Resultado'], $aditivo['NvcpAditivo']['Data'], 2, $formatacao->moeda($aditivo['NvcpAditivo']['Valor']), 
                                  $dtInicioVigencia, $dtTerminoVigencia);
            }
            if($aditivo['NvcpAditivo']['Valor'] < 0) { // Aditivo de Supressão
                $tipo_aditivo = 4;
                $aditivo['NvcpAditivo']['Valor'] = $aditivo['NvcpAditivo']['Valor'] * -1;
                $this->updAditivo($coContrato, null, $aditivo['NvcpAditivo']['Resultado'], $aditivo['NvcpAditivo']['Data'], 4, $formatacao->moeda($aditivo['NvcpAditivo']['Valor']));
            }
            debug($aditivo);
        endforeach;
    }
    
    private function erpImportarEmpenhos($idErp, $coContrato) {
        App::import('Model', 'NvcpEmpenho');
        $dbEmpenhoNovacap   = new NvcpEmpenho();
        
        $empenhos   = $dbEmpenhoNovacap->find( 'all', array( 'conditions'=>array('NvcpEmpenho.Id'=>$idErp) ) );
        foreach ($empenhos as $empenho) :
            debug($empenho); // Verificar se tem Nota, Valor ou Data
            $this->Empenho->create();
            if( !$this->Empenho->save( array('co_contrato'=>$coContrato, 'nu_empenho' => $empenho['NvcpEmpenho']['NotaDeEmpenhoNr'], 
                                             'tp_empenho' => 'O', 'vl_empenho' => $empenho['NvcpEmpenho']['NotaDeEmpenhoValor'], 
                                             'dt_empenho' => $empenho['NvcpEmpenho']['NotaDeEmpenhoData']) ) ) {
                debug('Erro ao Cadastrar Empenho: ' . $coContrato . ' - ' . $empenho['NvcpEmpenho']['NotaDeEmpenhoNr'] );
                debug( $this->Empenho->invalidFields() );
            }
        endforeach;
    }
    
}
?>