<?php

class IndenizacoesController extends AppController
{

    var $name = 'Indenizacoes';

    var $layout = 'iframe';

    function index($coFuncionario, $ano = "")
    {
        $this->Indenizacao->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'Indenizacao.dt_indenizacao' => 'asc'
            )
        );
        $conditions = null;
        if ($ano == "") {
            $maxAno = $this->Indenizacao->find('first',array(
                'fields'=>array('max( Indenizacao.nu_ano_indenizacao ) AS nu_ano_indenizacao'), 
                'conditions' => array('Indenizacao.co_funcionario' => $coFuncionario)
                ));
            if( isset($maxAno[0]['nu_ano_indenizacao']) && $maxAno[0]['nu_ano_indenizacao'] > 0 ) {
                $conditions['Indenizacao.nu_ano_indenizacao'] = $maxAno[0]['nu_ano_indenizacao'];
                $ano = $maxAno[0]['nu_ano_indenizacao'];
            }
        } else {
            $conditions['Indenizacao.nu_ano_indenizacao'] = $ano;
        }
        $conditions['Indenizacao.co_funcionario'] = $coFuncionario;
        
        $this->set('indenizacoes', $this->paginate($conditions));
        
        $this->set(compact('coFuncionario'));
        
        $this->set(compact('ano'));
        
        $anosInd = $this->Indenizacao->find('all', array('fields' => array('DISTINCT( Indenizacao.nu_ano_indenizacao ) AS nu_ano_indenizacao'),
                'conditions' => array(
                    'Indenizacao.co_funcionario' => $coFuncionario
                )
            ));
        $anos    = array();
        foreach ($anosInd as $ano):
            $anos[$ano['Indenizacao']['nu_ano_indenizacao']] = $ano['Indenizacao']['nu_ano_indenizacao'];
        endforeach;
        
        $this->set(compact('anos'));
        
    }
    
    function listar_ano($co_funcionario) {
        
        $anosInd = $this->Indenizacao->find('all', array(
                'fields' => array('DISTINCT( Indenizacao.nu_ano_indenizacao ) AS nu_ano_indenizacao'),
                'conditions' => array(
                    'Indenizacao.co_funcionario' => $co_funcionario
                )
            ));
        $anos    = array();
        foreach ($anosInd as $ano):
            $anos[$ano['Indenizacao']['nu_ano_indenizacao']] = $ano['Indenizacao']['nu_ano_indenizacao'];
        endforeach;

        echo json_encode ( $anos ) ;

        exit ();
    }
    
    function listar_mes($co_funcionario, $nu_ano) {
        
        $mesesInd   = $this->Indenizacao->find('all', array(
                'fields' => array("DISTINCT( DATE_FORMAT(Indenizacao.dt_indenizacao, '%m') ) AS nu_mes_indenizacao"),
                'conditions' => array(
                    'Indenizacao.co_funcionario'     => $co_funcionario,
                    'Indenizacao.nu_ano_indenizacao' => $nu_ano
                ),
                'order' => array(
                    'nu_mes_indenizacao' => 'asc'
                )
            ));
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        $meses  = array();
        foreach ($mesesInd as $mes):
            $meses[$mes[0]['nu_mes_indenizacao']] = $print->mesNuExtenso[$mes[0]['nu_mes_indenizacao']];
        endforeach;

        echo json_encode ( $meses ) ;

        exit ();
    }

    function iframe($coFuncionario, $ano = "")
    {
        $this->set(compact('coFuncionario'));
        $this->set(compact('ano'));
    }

    function add($coFuncionario)
    {
        if (! empty($this->data)) {
            $this->Indenizacao->create();
            if ($this->Indenizacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));

                $this->redirect(array(
                    'action' => 'index',
                    $coFuncionario
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coFuncionario'));
    }

    function edit($id = null, $coFuncionario)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFuncionario
            ));
        }
        if (! empty($this->data)) {
            if ($this->Indenizacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coFuncionario
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Indenizacao->read(null, $id);
        }
        $this->set(compact('coFuncionario'));
        $this->set(compact('id'));
    }

    function delete($id = null, $coFuncionario)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFuncionario
            ));
        }
        if ($this->Indenizacao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coFuncionario
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coFuncionario
        ));
    }
}
?>