<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 17:13
 */

class InspecaoController extends AppController
{
    var $name = "Inspecao";

    public function index()
    {
        $this->Inspecao->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Inspecao.ic_ativo' => 1
            )
        );
        $criteria = null;

        if(!empty($this->data)) {
            if(isset($this->data['Inspecao']['ds_inspecao'])) {
                $criteria['Inspecao.ds_inspecao'] = $this->data['Inspecao']['ds_inspecao'];
            }
        }

        $this->set('inspecoes', $this->paginate($criteria));
    }

    public function add($iframe = false)
    {
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Inspecao->create();

            $inspecao = $this->Inspecao->findByDsInspecao($this->data['Inspecao']['ds_inspecao']);
            if(isset($inspecao['Inspecao']) && $inspecao['Inspecao']['ic_ativo'] == 0) {
                $this->reInsert($inspecao['Inspecao']['co_inspecao']);
            }
            if($this->Inspecao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Inspecao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Inspecao->read(null, $id);
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Inspecao->id = $id;
            $msg = null;
            $inspecao['Inspecao']['ic_ativo'] = 0;

            if($this->Inspecao->saveField('ic_ativo',0)){
                $msg = "Inspecao bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Inspecao->id = $id;
            $msg = null;
            $inspecao['Inspecao']['ic_ativo'] = 1;

            if($this->Inspecao->saveField('ic_ativo',1)){
                $msg = "Inspecao cadastrada com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}