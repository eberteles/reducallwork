<?php

class InstituicoesController extends AppController
{

    var $name = 'Instituicoes';

    function index()
    {
        $this->Instituicao->recursive = 0;
        $this->set('instituicoes', $this->paginate());
    }
    
    function add()
    {
        if (! empty($this->data)) {
            $this->Instituicao->create();
            if ($this->Instituicao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $this->set('instituicoes', $this->Instituicao->InstituicaoPai->find('list', array(
            'order' => 'ds_instituicao ASC'
        )));
    }

    function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Instituicao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Instituicao->read(null, $id);
        }
        $this->set('id', $id);
        
        $this->set('instituicoes', $this->Instituicao->InstituicaoPai->find('list', array(
            'order' => 'ds_instituicao ASC'
        )));
    }

    function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Instituicao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>