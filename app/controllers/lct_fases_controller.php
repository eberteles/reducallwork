<?php

class LctFasesController extends AppController {

    public $name = 'LctFases';

    public $uses = array(
        'LctFases'
    );

    function index() {
        $criteria['ic_ativo'] = 2;
        $this->set('fases', $this->paginate($criteria));
    }

    public function add( $modal = false , $licitacao = null ){
        if($modal) {
            $this->layout = 'iframe';
            $this->set('licitacao', $licitacao);
        }
        if (!empty($this->data)) {
//            debug($this->data);die;
            $this->LctFases->create();
            if ($this->LctFases->save($this->data['LctFases'])) {
                if($modal) {
                    $fase = $this->LctFases->getInsertID();
                    $numero_licitacao = $this->LctFases->find('all',array(
                        'conditions' => array(
                            'LctFases.id' => $fase
                        )
                    ));
                    $this->redirect ( array ('action' => 'close', $numero_licitacao[0]['LctFases']['co_licitacao'] ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    public function edit( $id = null , $modal = true ){
        if( $modal )
        {
            $this->layout = 'iframe';
        }

        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $licitacao = $this->data['LctFases']['co_licitacao'];
            if ($this->LctFases->save($this->data['LctFases'])) {
                if( $modal ) {
                    $this->redirect ( array ('action' => 'close', $licitacao ) );
                }else{
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->LctFases->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->LctFases->read(null, $id)) {
            $fase = $this->LctFases->read(null, $id);
            $msg = null;
            $fase['LctFases']['ic_ativo'] = 1;

            if($this->LctFases->save($fase)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    function listar( $id ) {

        if( $id ){
            $fases = $this->LctFases->find ( 'all', array(
                'conditions' => array(
                    'LctFases.ic_ativo' => 2,
                    'LctFases.co_licitacao' => $id
                )
            ) );
            echo json_encode ( array(
                'fases' => $fases
            ) );
        }else{
            echo json_encode ( $this->LctFases->find ( 'all', array(
                'conditions' => array('LctFases.ic_ativo' => 2)
            ) ) );
        }
        exit ();
    }

    function removeFase()
    {
        if( !empty( $this->params['form']['id'] ) ) {
            $this->logicDelete( $this->params['form']['id'] );
        }
    }

    function iframe( )
    {
        $lic = $this->params['form']['lic'];
        $this->set('lic', $lic);
        $this->layout = 'blank';
    }

    function iframe_edit( )
    {
        $this->set('id', $this->params['form']['id']);
        $this->layout = 'blank';
    }

    function close( $co_licitacao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_licitacao' ) );
    }

}
?>
