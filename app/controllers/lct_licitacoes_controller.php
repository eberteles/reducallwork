<?php

class LctLicitacoesController extends AppController {

    public $name = 'LctLicitacoes';

    public $uses = array(
        'LctLicitacoes'
    );

    /*
     * function index() -> load all 'licitacao' in database
     * set variable 'licitacao' in view
     */
    function index() {
        $this->loadModel('LctFases');
        $criteria['ic_ativo'] = 2;
        $this->LctLicitacoes->order = 'LctLicitacoes.id DESC';
        if( !empty($this->data) ){
            if ($this->data['LctLicitacoes']['id']) {
                $criteria['id'] = up($this->data['LctLicitacoes']['id']);
            }
            if ($this->data['LctLicitacoes']['dt_inicio'] && $this->data['LctLicitacoes']['dt_final']) {
                $criteria[] = "dt BETWEEN '" . dtDb($this->data['LctLicitacoes']['dt_inicio']) . "' AND '" . dtDb($this->data['LctLicitacoes']['dt_final']) . "'";
            }
            $this->set('licitacoes', $this->paginate($criteria));
        }else{
            $this->set('licitacoes', $this->paginate($criteria));
        }
        $fases = $this->LctFases->find('all');
        $this->set('fases', $fases);
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false ){
        $this->loadModel('LctLotes');
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->LctLicitacoes->create();
            if ($this->LctLicitacoes->save($this->data['LctLicitacoes'])) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->LctLicitacoes->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    function addLicitacao()
    {
        $this->loadModel('LctLotes');
        if( ! empty($this->params['form']) )
        {
            $this->params['form'] = array_map('strtoupper', $this->params['form']);
            if( $this->params['form']['dt'] == '--' || $this->params['form']['dt'] == ''){
                unset($this->params['form']['dt']);
            }
            if($this->LctLicitacoes->save($this->params['form'])){
                $lic = $this->LctLicitacoes->getInsertID();
                echo json_encode(array(
                    $lic
                ));
            }else{
                echo json_encode(array(
                    'msg' => 'false'
                ));
            }
        }else{
            echo json_encode(array(
                'msg' => 'O campo objeto está em branco'
            ));
        }
        exit();
    }

    function editLicitacao()
    {
        $this->loadModel('LctLotes');
        if( ! empty($this->params['form']) )
        {
            $lic = $this->params['form']['id'];
            if($this->LctLicitacoes->save($this->params['form'])){
                $lotes = $this->LctLotes->find('all',array(
                    'conditions' => array(
                        'LctLotes.ic_ativo' => 2,
                        'LctLotes.co_licitacao' => $lic
                    )
                ));
                echo json_encode(array(
                    'msg' => 'true',
                    'lotes' => $lotes
                ));
            }else{
                echo json_encode(array(
                    'msg' => 'false'
                ));
            }
        }else{
            echo json_encode(array(
                'msg' => 'O campo objeto está em branco'
            ));
        }
        exit();
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null ){
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'controller' => 'dsp_campos_auxiliares',
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $nome_cargo = $this->data['LctLicitacoes']['descricao'];
            $this->data = $this->LctLicitacoes->read(null, $id);
            $this->data['LctLicitacoes']['nome_cargo'] = $nome_cargo;
            if ($this->LctLicitacoes->save($this->data['LctLicitacoes'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'controller' => 'dsp_campos_auxiliares',
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->LctLicitacoes->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->LctLicitacoes->read(null, $id)) {
            $licitacao = $this->LctLicitacoes->read(null, $id);
            $msg = null;
            $licitacao['LctLicitacoes']['ic_ativo'] = 1;

            if($this->LctLicitacoes->save($licitacao)){
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    function mostrar( $id = null )
    {
        $this->loadModel('LctFases');
        $this->loadModel('LctLotes');

        $licitacao = $this->LctLicitacoes->find('all',array(
            'conditions' => array(
                'LctLicitacoes.ic_ativo' => 2,
                'LctLicitacoes.id' => $id
            )
        ));
        $this->set('licitacao', $licitacao);

        $lotes = $this->LctLotes->find('all',array(
            'conditions' => array(
                'LctLotes.ic_ativo' => 2,
                'LctLotes.co_licitacao' => $id
            )
        ));
        $this->set('lotes', $lotes);

        $fases = $this->LctFases->find('all',array(
            'conditions' => array(
                'LctFases.ic_ativo' => 2,
                'LctFases.co_licitacao' => $id
            )
        ));
        $this->set('fases', $fases);
    }

    function imprimirLicitacao( $id = null ){
        $this->loadModel('LctFases');
        $this->loadModel('LctLotes');

        $lic = $this->LctLicitacoes->read(null, $id);

        $lotes = $this->LctLotes->find('all',array(
            'conditions' => array(
                'LctLotes.ic_ativo' => 2,
                'LctLotes.co_licitacao' => $id
            )
        ));

        $fases = $this->LctFases->find('all',array(
            'conditions' => array(
                'LctFases.ic_ativo' => 2,
                'LctFases.co_licitacao' => $id
            )
        ));

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Licitação</b>';

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'DADOS DA LICITAÇÃO'
        ), null, array(
            'colspan' => 4
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Modalidade Processo</b>',
                '<b>Modalidade da Licitação</b>',
                '<b>Data</b>',
                '<b>Prazo de Execução</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                $lic['LctLicitacoes']['nu_licitacao'] . ' / ' . $lic['LctLicitacoes']['modalidade_processo'],
                $lic['LctLicitacoes']['modalidade_licitacao'],
                $lic['LctLicitacoes']['dt'] . " às " . $lic['LctLicitacoes']['hora'],
                $lic['LctLicitacoes']['prazo_execucao'] . ' DIAS',
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'OBJETO DA LICITAÇÃO'
        ), null, array(
            'colspan' => 1
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Objeto</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
               $lic['LctLicitacoes']['objeto']
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'DADOS FINANCEIROS'
        ), null, array(
            'colspan' => 3
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Valor Estimado</b>',
                '<b>Valor Contratado</b>',
                '<b>Diferença</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        $viagemHtml .= $html->tableCells(array(
            array(
                vlReal($lic['LctLicitacoes']['valor_estimado']),
                vlReal($lic['LctLicitacoes']['valor_contratado']),
                $lic['LctLicitacoes']['diferenca'],
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'LOTES'
        ), null, array(
            'colspan' => 5
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Objeto</b>',
                '<b>Valor Estimado</b>',
                '<b>Valor Contratado</b>',
                '<b>Diferença</b>',
                '<b>Empresa Vencedora</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        foreach( $lotes as $lote ) {
            $viagemHtml .= $html->tableCells(array(
                array(
                    $lote['LctLotes']['objeto'],
                    vlReal($lote['LctLotes']['valor_estimado']),
                    vlReal($lote['LctLotes']['valor_contratado']),
                    $lote['LctLotes']['diferenca'],
                    $lote['LctLotes']['empresa_vencedora']
                )
            ), array(
                'class' => 'altrow'
            ));
        }

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        $viagemHtml .= $html->tag('br');
        $viagemHtml .= $html->tag('br');

        $viagemHtml .= $html->tag('table', null, array(
            'style' => 'width: 100%; position: relative;',
            'class' => 'table table-bordered table-striped',
            'cellspacing' => '0',
            'cellpadding' => '0'
        ));

        $viagemHtml .= $html->tag('thead');
        $viagemHtml .= $html->tableHeaders(array(
            'FASES'
        ), null, array(
            'colspan' => 2
        ));

        $viagemHtml .= $html->tableCells(array(
            array(
                '<b>Data</b>',
                '<b>Fase</b>'
            )
        ), array(
            'class' => 'altrow'
        ));

        $viagemHtml .= $html->tag('/thead');
        $viagemHtml .= $html->tag('tbody');

        foreach( $fases as $fase ) {
            $viagemHtml .= $html->tableCells(array(
                array(
                    $fase['LctFases']['data'],
                    $fase['LctFases']['descricao']
                )
            ), array(
                'class' => 'altrow'
            ));
        }

        $viagemHtml .= $html->tag('/tbody');
        $viagemHtml .= $html->tag('/table');

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimir($viagemHtml, 'P', $titulo, false);
    }

    function preImprimir()
    {

    }

    function imprimirRelatorioGeralHtml( $ano = null ){
        $this->layout = 'lct_relatorio_html';

        $this->loadModel('LctFases');
//        $this->loadModel('LctLotes');

        if( $this->data['PreImprimir']['modalidade_licitacao'] != '' ){
            $licitacao = $this->LctLicitacoes->find('all', array(
                'conditions' => array(
                    'LctLicitacoes.ic_ativo' => 2,
                    'LctLicitacoes.dt_inicio' => $this->data['PreImprimir']['modalidade_licitacao']
                )
            ));
        }else{
            $licitacao = $this->LctLicitacoes->find('all', array(
                'conditions' => array(
                    'LctLicitacoes.ic_ativo' => 2
                )
            ));
        }

        $fases = $this->LctFases->find('all');
        $this->set('fases', $fases);

        $this->set('licitacoes', $licitacao);
    }

    function imprimirRelatorioGeral(  ){

        $this->loadModel('LctFases');
        $this->loadModel('LctLotes');

        if( $this->data['PreImprimir']['modalidade_licitacao'] != null ){
            $licitacao = $this->LctLicitacoes->find('all', array(
                'conditions' => array(
                    'LctLicitacoes.ic_ativo' => 2,
                    'LctLicitacoes.modalidade_licitacao' => $this->data['PreImprimir']['modalidade_licitacao']
                )
            ));
        }else{
            $licitacao = $this->LctLicitacoes->find('all', array(
                'conditions' => array(
                    'LctLicitacoes.ic_ativo' => 2
                )
            ));
        }

        $fases = $this->LctFases->find('all');

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Licitações</b>';

        if( !empty($licitacao) ) {

            $viagemHtml .= $html->tag('br');

            foreach ($licitacao as $lic) {

                $viagemHtml .= $html->tag('table', null, array(
                    'style' => 'width: 100%; position: relative;',
                    'class' => 'table table-bordered table-striped',
                    'cellspacing' => '0',
                    'cellpadding' => '0'
                ));

                $viagemHtml .= $html->tag('thead');

                $viagemHtml .= $html->tableCells(array(
                    array(
                        '<b>ITEM</b>',
                        '<b>OBJETO</b>',
                        '<b>MODALIDADE PROCESSO</b>',
                        '<b>DATA</b>',
                        '<b>VALOR ESTIMADO</b>',
                        '<b>VALOR CONTRATADO</b>',
                        '<b>DIFERENÇA</b>',
                        '<b>PRAZO DE EXECUÇÃO</b>',
                        '<b>FASE ATUAL</b>'
                    )
                ), array(
                    'class' => 'altrow'
                ));

                $viagemHtml .= $html->tag('/thead');
                $viagemHtml .= $html->tag('tbody');

                foreach ($fases as $fase) {
                    if ($fase['LctFases']['co_licitacao'] == $lic['LctLicitacoes']['id']) {
                        $fase_atual = $fase['LctFases']['descricao'];
                    }
                }

                $lotes = $this->LctLotes->find('all', array(
                    'conditions' => array(
                        'LctLotes.co_licitacao' => $lic['LctLicitacoes']['id']
                    )
                ));

                $viagemHtml .= $html->tableCells(array(
                    array(
                        $lic['LctLicitacoes']['id'],
                        $lic['LctLicitacoes']['objeto'],
                        $lic['LctLicitacoes']['nu_licitacao'] . ' / ' . substr($lic['LctLicitacoes']['modalidade_processo'], 0, 3) . '.' . substr($lic['LctLicitacoes']['modalidade_processo'], 3, 3) . '.' . substr($lic['LctLicitacoes']['modalidade_processo'], 6, 3) . '/' . substr($lic['LctLicitacoes']['modalidade_processo'], 9, 2),
                        $lic['LctLicitacoes']['dt'] . " às " . $lic['LctLicitacoes']['hora'],
                        'R$ ' . number_format($lic['LctLicitacoes']['valor_estimado'], 2, ',', '.'),
                        'R$ ' . number_format($lic['LctLicitacoes']['valor_contratado'], 2, ',', '.'),
                        $lic['LctLicitacoes']['diferenca'] . ' %',
                        $lic['LctLicitacoes']['prazo_execucao'] . ' DIAS',
                        $fase_atual,
                    )
                ), array(
                    'class' => 'altrow'
                ));

                $viagemHtml .= $html->tag('/tbody');
                $viagemHtml .= $html->tag('/table');

                if( !empty($lotes) ) {

                    $viagemHtml .= $html->tag('table', null, array(
                        'style' => 'width: 100%; position: relative;',
                        'class' => 'table table-bordered table-striped',
                        'cellspacing' => '0',
                        'cellpadding' => '0'
                    ));

                    $viagemHtml .= $html->tag('thead');

                    $viagemHtml .= $html->tableCells(array(
                        array(
                            '<b>LOTES</b>'
                        )
                    ), array(
                        'class' => 'altrow'
                    ));

                    $viagemHtml .= $html->tag('/thead');
                    $viagemHtml .= $html->tag('/table');

                    $viagemHtml .= $html->tag('table', null, array(
                        'style' => 'width: 100%; position: relative;',
                        'class' => 'table table-bordered table-striped',
                        'cellspacing' => '0',
                        'cellpadding' => '0'
                    ));

                    $viagemHtml .= $html->tag('thead');

                    $viagemHtml .= $html->tableCells(array(
                        array(
                            '<b>OBJETO</b>',
                            '<b>VALOR ESTIMADO</b>',
                            '<b>VALOR CONTRATADO</b>',
                            '<b>DIFERENÇA</b>',
                            '<b>EMPRESA VENCEDORA</b>',
                        )
                    ), array(
                        'class' => 'altrow'
                    ));

                    $viagemHtml .= $html->tag('/thead');
                    $viagemHtml .= $html->tag('tbody');

                    $counter = 1;
                    foreach ($lotes as $lote) {
                        $viagemHtml .= $html->tableCells(array(
                            array(
                                $lote['LctLotes']['objeto'],
                                vlReal($lote['LctLotes']['valor_estimado']),
                                vlReal($lote['LctLotes']['valor_contratado']),
                                $lote['LctLotes']['diferenca'] . '%',
                                $lote['LctLotes']['empresa_vencedora']
                            )
                        ), array(
                            'class' => 'altrow'
                        ));

                        $counter++;
                    }

                    $viagemHtml .= $html->tag('/tbody');
                    $viagemHtml .= $html->tag('/table');
                }

                $viagemHtml .= $html->tag('br');
                $viagemHtml .= $html->tag('br');
            }
        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }

    function listar() {

        echo json_encode ( $this->LctLicitacoes->find ( 'list', array(
            'conditions' => array('LctLicitacoes.ic_ativo' => 2)
        ) ) );

        exit ();
    }

    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_licitacao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_licitacao' ) );
    }

}
?>
