<?php

class LctLotesController extends AppController {

    public $name = 'LctLotes';

    public $uses = array(
        'LctLotes'
    );

    /*
     * function index() -> load all 'lote' in database
     * set variable 'lote' in view
     */
    function index() {
        // CARREGA TODOS OS lote E COLOCA NA VIEW
        $criteria['ic_ativo'] = 2;
        $this->set('lotes', $this->paginate($criteria));
    }

    /*
     * function add() -> receives the data from form or load the form, if data is empty
     * @parameters modal false
     * @return -> returns the add form
     */
    public function add( $modal = false , $licitacao = null ){
        if($modal) {
            $this->set('licitacao', $licitacao);
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {
            $this->LctLotes->create();
            if ($this->LctLotes->save($this->data['LctLotes'])) {
                if($modal) {
                    $lic = $this->LctLotes->getInsertID();
                    $numero_licitacao = $this->LctLotes->find('all',array(
                        'conditions' => array(
                            'LctLotes.id' => $lic
                        )
                    ));
                    $this->redirect ( array ('action' => 'close', $numero_licitacao[0]['LctLotes']['co_licitacao'] ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /*
     * function edit() -> receives the id that will update
     * @parameters id null
     */
    public function edit( $id = null , $modal = true ){
        if( $modal )
        {
            $this->layout = 'iframe';
        }

        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $licitacao = $this->data['LctLotes']['co_licitacao'];
            if ($this->LctLotes->save($this->data['LctLotes'])) {
                if( $modal ) {
                    $this->redirect ( array ('action' => 'close', $licitacao ) );
                }else{
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->LctLotes->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
        }

        if ($this->LctLotes->read(null, $id)) {
            $lote = $this->LctLotes->read(null, $id);
            $msg = null;
            $lote['LctLotes']['ic_ativo'] = 1;

            if($this->LctLotes->save($lote)){
                $msg = "Registro deletado com sucesso!";
            }else{
                debug($this->LctLotes->validationErrors);die;
                $msg = "Houve um erro na deleção";
            }

            $this->Session->setFlash(__($msg, true));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
        }
    }

    function listar( $id ) {

        if( $id ){
            $lotes = $this->LctLotes->find ( 'all', array(
                'conditions' => array(
                    'LctLotes.ic_ativo' => 2,
                    'LctLotes.co_licitacao' => $id
                )
            ) );
            echo json_encode ( array(
                'lotes' => $lotes
            ) );
        }else{
            echo json_encode ( $this->LctLotes->find ( 'all', array(
                'conditions' => array('LctLotes.ic_ativo' => 2)
            ) ) );
        }
        exit ();
    }

    function removeLote()
    {
        if( !empty( $this->params['form']['id'] ) ) {
            $this->logicDelete( $this->params['form']['id'] );
        }
    }

    function iframe( )
    {
        $lic = $this->params['form']['lic'];
        $this->set('lic', $lic);
        $this->layout = 'blank';
    }

    function iframe_edit( )
    {
        $this->set('id', $this->params['form']['id']);
        $this->layout = 'blank';
    }

    function close( $co_licitacao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_licitacao' ) );
    }

    function loadValorContratado()
    {
        if( isset( $this->params['form']['lic'] ) )
        {
            $lic = $this->params['form']['lic'];
        }
        if( isset( $lic ) ){
            $vlContratado = $this->LctLotes->find ( 'all' , array(
                'conditions' => array(
                    'LctLotes.ic_ativo' => 2,
                    'LctLotes.co_licitacao' => $lic
                ),
                'fields' => array(
                    'SUM(LctLotes.valor_contratado) as vlContratado'
                )
            ) );

            $valor = formatMoney($vlContratado[0][0]['vlContratado']);

            echo json_encode (
                $valor
            );
        }else{
            echo json_encode ( $this->LctLotes->find ( 'all', array(
                'conditions' => array('LctLotes.ic_ativo' => 2)
            ) ) );
        }
        exit ();
    }

}
?>
