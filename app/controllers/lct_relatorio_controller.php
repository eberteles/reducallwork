<?php

class LctRelatorioController extends AppController
{

    public $name = 'LctRelatorio';

    public $uses = array(
        'LctRelatorio'
    );

    function index()
    {
        $this->layout = 'lct_relatorio';

        $modalidades_licitacao = array(
            'CONCORRÊNCIA'     => 'CONCORRÊNCIA',
            'T. DE PREÇOS'     => 'T. DE PREÇOS',
            'CONVITE'          => 'CONVITE',
            'PRE-QUALIFICAÇÃO' => 'PRE-QUALIFICAÇÃO',
            'P. ELETRÔNICO'    => 'P. ELETRÔNICO',
            'P. PRESENCIAL'    => 'P. PRESENCIAL'
        );
        $this->set('modalidades_licitacao', $modalidades_licitacao );
    }

    function listar( )
    {

    }

    function imprimirRelatorioGeralHtml( ){
        $this->layout = 'lct_relatorio_html';

        $this->loadModel('LctFases');
//        $this->loadModel('LctLotes');

        if( $this->data['PreImprimir']['modalidade_licitacao'] != '' ){
            $licitacao = $this->LctRelatorio->find('all', array(
                'conditions' => array(
                    'LctRelatorio.ic_ativo' => 2,
                    'LctRelatorio.sub_judice' => 0,
                    'LctRelatorio.modalidade_licitacao' => $this->data['PreImprimir']['modalidade_licitacao']
                )
            ));
            $this->set('modalidade', $this->data['PreImprimir']['modalidade_licitacao']);
        }else{
            $licitacao = $this->LctRelatorio->find('all', array(
                'conditions' => array(
                    'LctRelatorio.ic_ativo' => 2,
                    'LctRelatorio.sub_judice' => 0
                )
            ));
            $this->set('modalidade', null);
        }

        $fases = $this->LctFases->find('all');
        $this->set('fases', $fases);

        $this->set('licitacoes', $licitacao);
    }

    function imprimirRelatorioGeral( $modalidade = null ){

        $this->loadModel('LctFases');

        if( $modalidade != null ){
            $licitacao = $this->LctRelatorio->find('all', array(
                'conditions' => array(
                    'LctRelatorio.ic_ativo' => 2,
                    'LctRelatorio.sub_judice' => 0,
                    'LctRelatorio.modalidade_licitacao' => $modalidade
                )
            ));
        }else{
            $licitacao = $this->LctRelatorio->find('all', array(
                'conditions' => array(
                    'LctRelatorio.ic_ativo' => 2,
                    'LctRelatorio.sub_judice' => 0
                )
            ));
        }

        $fases = $this->LctFases->find('all');
        $this->set('fases', $fases);

        App::import('Vendor', 'pdf');
        App::import('Helper', 'Html');
        App::import('Helper', 'Print');
        App::import('Helper', 'Formatacao');
        App::import('Helper', 'Number');
        $PDF = new PDF();
        $html = new HtmlHelper();
        $print = new PrintHelper();
        $formatacao = new NumberHelper();

        $viagemHtml = '';
        $titulo = '<b>Relatório Geral de Licitações</b>';

        if( !empty($licitacao) ) {
            $viagemHtml .= $html->tag('table', null, array(
                'style' => 'width: 100%; position: relative;',
                'class' => 'table table-bordered table-striped',
                'cellspacing' => '0',
                'cellpadding' => '0'
            ));

            $viagemHtml .= $html->tag('thead');

            $viagemHtml .= $html->tableCells(array(
                array(
                    '<b>ITEM</b>',
                    '<b>OBJETO</b>',
                    '<b>MODALIDADE PROCESSO</b>',
                    '<b>DATA</b>',
                    '<b>VALOR ESTIMADO</b>',
                    '<b>VALOR CONTRATADO</b>',
                    '<b>DIFERENÇA</b>',
                    '<b>PRAZO DE EXECUÇÃO</b>',
                    '<b>FASE ATUAL</b>'
                )
            ), array(
                'class' => 'altrow'
            ));

            $viagemHtml .= $html->tag('/thead');
            $viagemHtml .= $html->tag('tbody');

            foreach ($licitacao as $lic) :
                $fase_atual = '';
                foreach ( $fases as $fase ) {
                    if( $fase['LctFases']['co_licitacao'] == $lic['LctRelatorio']['id'] ) {
                        $fase_atual = $fase['LctFases']['descricao'];
                    }
                }

                $viagemHtml .= $html->tableCells(array(
                    array(
                        $lic['LctRelatorio']['id'],
                        $lic['LctRelatorio']['objeto'],
                        $lic['LctRelatorio']['modalidade_processo'],
                        $lic['LctRelatorio']['dt'] . " às " . $lic['LctRelatorio']['hora'],
                        $lic['LctRelatorio']['valor_estimado'],
                        $lic['LctRelatorio']['valor_contratado'],
                        $lic['LctRelatorio']['diferenca'],
                        $lic['LctRelatorio']['prazo_execucao'],
                        $fase_atual
                    )
                ), array(
                    'class' => 'altrow'
                ));
            endforeach;

            $viagemHtml .= $html->tag('/tbody');
            $viagemHtml .= $html->tag('/table');

        }else{
            $viagemHtml .= '<b>Não existem registros para os parâmetros solicitados</b>';
        }

        // A porra do quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
        $PDF->imprimirNovacap($viagemHtml, 'R', $titulo, false);
    }
}