<?php
App::import('Controller', 'Movimentos');

// Garante que o script inteiro será processado independente do tempo necessário
set_time_limit(0);

/**
 * Processa arquivos enviados pelo módulo SIAFI
 */
class LeitorArquivosController extends AppController
{

    var $name = 'LeitorArquivos';

    var $autoRender = false;

    var $uses = array(
        'Layout',
        'Fornecedor',
        'Contrato',
        'Contratacao',
        'Empenho',
        'Siafi',
        'PreEmpenho',
        'Liquidacao',
        'OrdemBancaria'
    );

    const PASTA_ARQUIVOS = '/arquivos/';
    
    // layouts dos documentos (colunas dos documentos)
    var $layouts = array();

    /**
     * Inicia o processamento dos arquivos.
     *
     * Irá buscar todos os arquivos na pasta /arquivos/.
     * Vai processar linha por linha e chamar a ação de processamento de acordo com o layout (ex.: criticaSI002, criticaSI005)
     */
    public static function processar()
    {
        // instancias
        $la = new self();
        $la->constructClasses();
        
        $mvCtrl = new MovimentosController();
        $mvCtrl->constructClasses();
        
        $reflection = new ReflectionClass(__CLASS__);
        
        // processamento
        $arquivos = $la->listarArquivos();
        if (empty($arquivos)) {
            return;
        }
        
        foreach ($arquivos as $arquivo) {
            try {
                $movimento = $mvCtrl->obterMovimento($arquivo->nome);
                if (! $mvCtrl->isValido($movimento)) {
                    continue; // TODO: fazer log?
                }
                
                // se nao existir movimento, cria um
                if (empty($movimento)) {
                    $movimento = $mvCtrl->iniciarMovimento($arquivo->nome);
                }
                
                if (filesize($arquivo->path) <= 0) {
                    throw new Exception('O arquivo ' . $arquivo->nome . ' esta vazio.');
                }
                
                // le o arquivo
                $fp = fopen($arquivo->path, 'r');
                while (! feof($fp)) {
                    $linha = fgets($fp);
                    $linhaTrim = trim($linha);
                    
                    // processa proxima linha, se estiver vazia
                    if (empty($linhaTrim)) {
                        continue;
                    }
                    
                    $colunas = $la->transformaRegistro($linha);
                    
                    // verifica se a ação está implementada
                    $layout = $colunas['COD-ARQUIVO'];
                    $metodoAcao = 'critica' . $layout;
                    if (! $reflection->hasMethod($metodoAcao)) {
                        throw new Exception('A acao para o tipo de arquivo ' . $layout . ' nao foi implementada.');
                    }
                    
                    // executa ação do tipo de documento
                    //
                    // trata as exceçoes mas continua processando as outras linhas do arquivo
                    try {
                        $retornoAcao = $la->$metodoAcao($colunas);
                        if (! $retornoAcao) {
                            throw new Exception('Erro ao salvar linha do arquivo ' . $arquivo->nome . '(layout: ' . $layout . ')');
                        }
                    } catch (Exception $e) {
                        $mvCtrl->logErro($e->getMessage(), $arquivo->nome);
                        continue;
                    }
                }
                fclose($fp);
                
                // finaliza movimento com sucesso
                $mvCtrl->finalizarMovimento($movimento);
                
                // exclui arquivo processado
                unlink($arquivo->path);
            } catch (Exception $e) {
                $mvCtrl->logErro($e->getMessage(), $arquivo->nome);
                continue;
            }
        }
    }

    /**
     * Lista arquivos
     */
    protected function listarArquivos()
    {
        $return = array();
        $path = ROOT . self::PASTA_ARQUIVOS;
        $arquivos = scandir($path);
        
        foreach ($arquivos as $arquivo) {
            if ($arquivo[0] != '.' && ! is_dir($arquivo)) {
                $obj = new stdClass();
                $obj->nome = $arquivo;
                $obj->path = $path . $obj->nome;
                
                $return[] = $obj;
            }
        }
        
        return $return;
    }

    /**
     * Relaciona o conteúdo da linha com a coluna de um layout
     * 
     * @param [type] $linha
     *            [description]
     */
    protected function transformaRegistro($linha)
    {
        $registro = array();
        
        $co_layout = substr($linha, 0, 5);
        
        // se colunas nao estiverem no cache
        if (! array_key_exists($co_layout, $this->layouts)) {
            $this->layouts[$co_layout] = array();
            
            $colunas = $this->Layout->find('all', array(
                'conditions' => array(
                    'co_layout' => $co_layout
                )
            ));
            if (! empty($colunas)) {
                foreach ($colunas as $coluna) {
                    $item = $coluna['Layout'];
                    array_push($this->layouts[$co_layout], $item);
                }
            }
        }
        
        // obtem coluna na linha a partir dos valores da coluna (registrados no db)
        $colunas = $this->layouts[$co_layout];
        foreach ($colunas as $coluna) {
            $registro[$coluna['ds_campo']] = substr($linha, $coluna['no_ini_campo'], $coluna['no_tam_campo']);
        }
        
        return $registro;
    }

    /**
     * Formata data utilizada pelo DB
     * TODO: mover para um arquivo de funcoes utilitarias
     */
    public function formatarData($data)
    {
        $dia = substr($data, 6, 2);
        $mes = substr($data, 4, 2);
        $ano = substr($data, 0, 4);
        return $ano . '-' . $mes . '-' . $dia;
    }

    /**
     * Converte a notação de colunas dos arquivos para a notação de colunas do DB.
     * - Remove a coluna COD-ARQUIVO
     * - Converte para lowercase
     * - Troca os hífens por underline
     * - Aplica trim ao valor
     *
     * @param array $registro
     *            array representando um registro
     * @return array registro formatado
     */
    protected function formatarRegistroDb($registro)
    {
        $return = array();
        
        foreach ($registro as $col => $value) {
            if ($col == 'COD-ARQUIVO') {
                continue;
            }
            $newcol = strtolower($col);
            $newcol = str_replace('-', '_', $newcol);
            
            $return[$newcol] = trim($value);
        }
        
        return $return;
    }

    /**
     * Critica do arquivo EMPENHO
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI002($linha)
    {
        $data = $this->formatarRegistroDb($linha);
        
        $contrato = $this->Contrato->find('first', array(
            'conditions' => array(
                'nu_processo' => $data['it_nu_processo']
            )
        ));
        if (empty($contrato)) {
            throw new Exception('Contrato nao encontrado. (NU_PROCESSO: ' . $data['it_nu_processo'] . ')');
        }
        
        $contrato = $contrato['Contrato'];
        $data['co_contrato'] = $contrato['co_contrato'];
        $data['dt_empenho'] = $this->formatarData($data['dt_empenho']);
        
        $this->Empenho->create($data);
        return $this->Empenho->save();
    }

    /**
     * Critica do arquivo SI003 (liquidacao)
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI003($linha)
    {
        $data = $this->formatarRegistroDb($linha);
        $this->Liquidacao->create($data);
        return $this->Liquidacao->save();
    }

    /**
     * Critica do arquivo SI004 (ordem bancaria)
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI004($linha)
    {
        $data = $this->formatarRegistroDb($linha);
        $this->OrdemBancaria->create($data);
        return $this->OrdemBancaria->save();
    }

    /**
     * Critica do arquivo SI005 (pre-empenho)
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI005($linha)
    {
        $data = $this->formatarRegistroDb($linha);
        $this->PreEmpenho->create($data);
        return $this->PreEmpenho->save();
    }

    /**
     * Empenho PMB
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI006($linha)
    {
        return $this->criticaSI002($linha);
    }

    /**
     * Liquidacao PMB
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI007($linha)
    {
        return $this->criticaSI003($linha);
    }

    /**
     * Ordem bancaria PMB
     * 
     * @param array $linha
     *            linha a ser salva
     */
    public function criticaSI008($linha)
    {
        return $this->criticaSI004($linha);
    }
}