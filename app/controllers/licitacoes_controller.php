<?php

class LicitacoesController extends AppController
{

    var $name = 'Licitacoes';

    var $uses = array('Licitacao','TipoLicitacao', 'Contratacao', 'Situacao', 'Fornecedor', 'Anexo', 'Contrato');

    function __construct() {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();
        $this->tpAnexo = $this->modulo->getTiposDeAnexos();

        return parent::__construct();
    }
    
    public function index() {    	
        unset($this->Licitacao->validate['nu_licitacao']);
        unset($this->Licitacao->validate['dt_publicacao']);
        unset($this->Licitacao->validate['co_situacao']);
        unset($this->Licitacao->validate['co_contratacao']);


        $this->paginate = array(
            'fields' => array(
                'Licitacao.co_licitacao',
                'Licitacao.nu_licitacao', 
                'Licitacao.ds_objeto',
                'Contratacao.ds_contratacao',
                'Licitacao.dt_publicacao',
                'Licitacao.vl_estimado',
                'Licitacao.vl_contratado',
                'Licitacao.ds_prazo_execucao',
                'Fornecedor.no_razao_social',
                'Licitacao.nu_diferenca',
                'Situacao.ds_situacao',
                'Licitacao.dt_abertura'
            ),
            'limit' => 100,
            'order' => array(
                'Licitacao.co_licitacao' => 'asc'
            )
        );

        $conditions = array();
        $conditions['Licitacao.ic_ativo'] = 1;

        if (!empty($this->params['url']['nu_licitacao'])) {
            $this->data['Licitacao']['nu_licitacao'] = $this->params['url']['nu_licitacao'];
            $conditions['Licitacao.nu_licitacao'] = str_replace('/', '', $this->params['url']['nu_licitacao']);
        }

        if (!empty($this->params['url']['dt_publicacao'])) {
            $this->data['Licitacao']['dt_publicacao'] = $this->params['url']['dt_publicacao'];
            $conditions['Licitacao.dt_publicacao'] = dtDb($this->params['url']['dt_publicacao']);
        }

        if (!empty($this->params['url']['co_contratacao'])) {
            $this->data['Licitacao']['co_contratacao'] = $this->params['url']['co_contratacao'];
            $conditions['Licitacao.co_contratacao'] = $this->params['url']['co_contratacao'];
        }

        if (!empty($this->params['url']['co_situacao'])) {
            $this->data['Licitacao']['co_situacao'] = $this->params['url']['co_situacao'];
            $conditions['Licitacao.co_situacao'] = $this->params['url']['co_situacao'];
        }

        $situacoes = $this->Situacao->find('list', array(
            'conditions' => array(
                'Situacao.ic_ativo' => 1
            )
        ));

        $contratacoes = $this->Contratacao->find('list', array(
            'conditions' => array(
                'Contratacao.at_contratacao' => 2
            )
        ));

        $licitacoes = $this->paginate('Licitacao', $conditions);
        $this->set('licitacoes', $licitacoes);
        $this->set('contratacoes', $contratacoes);
        $this->set('situacoes', $situacoes);
    }
        
    public function add($modal = false) {
        $this->set('modal', $modal);

        if(! empty ( $this->data)){

            unset($this->data['Licitacao']['anexo']);
            $this->data['Licitacao']['ic_ativo'] = 1;
            $this->Licitacao->create();
            $this->prepararCampos();
//            var_dump($this->data);die;
            if ($this->Licitacao->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));

            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $contratacoes = $this->Contratacao->find('list', array(
            'conditions' => array('Contratacao.ic_licitacao' => 1)
        ));

        $situacoes = $this->Situacao->find('list', array(
            'conditions' => array('Situacao.ic_licitacao' => 1)
        ));

        $this->set( 'contratacao', $contratacoes );
        $this->set( 'situacao', $situacoes );
        $this->set( 'tiposLicitacoes', $this->TipoLicitacao->find('list') );
        $this->set( 'fornecedor', $this->Fornecedor->find('list', array(
                'fields' => array(
                    'Fornecedor.co_fornecedor',
                    'Fornecedor.nome_combo'
                )
            )
        ) );
    }

    function close( $co_licitacao )
    {
        $this->layout = 'iframe';
        $this->Session->setFlash(__('Registro salvo com sucesso', true));
        $this->set ( compact ( 'co_licitacao' ) );
    }

    public function edit($id = null, $modal = false) {
        $this->set('modal', $modal);
        if(! empty ( $this->data)){
            unset($this->data['Licitacao']['anexo']);
            $this->data['Licitacao']['dt_publicacao'] = date('Y-m-d',strtotime( $this->data['Licitacao']['dt_publicacao']));

            $this->prepararCampos();
//            var_dump($this->data);die;
            if ($this->Licitacao->save($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));

            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }

        }

        if (empty($this->data)) {
            $this->data = $this->Licitacao->read(null, $id);
        }

        $icLicitacao = 0;

        $contratacoes = $this->Contratacao->find('list', array(
            'conditions' => array('Contratacao.ic_licitacao' => $icLicitacao)
        ));

        $situacoes = $this->Situacao->find('list', array(
            'conditions' => array('Situacao.ic_licitacao' => $icLicitacao)
        ));

        $this->set( 'contratacao', $contratacoes );
        $this->set( 'situacao', $situacoes );
        $this->set( 'tiposLicitacoes', $this->TipoLicitacao->find('list') );
        $this->set( 'fornecedor', $this->Fornecedor->find('list', array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            )
        )
        ) );
        $this->set(compact('id'));
    }

    public function logicDelete($id) {
        $this->render = false;
        $licitacao = $this->Licitacao->find('first', array(
            'fields' => 'Licitacao.co_licitacao',
            'conditions' => array('Licitacao.co_licitacao' => $id)
        ));

        if (!empty($licitacao)) {
            $this->Licitacao->id = $id;
            if ($this->Licitacao->saveField('ic_ativo', 0, false)) {
                $this->Session->setFlash('Registro excluído com sucesso');
                $this->redirect(array('action' => 'index'));
            } else { 
                
            }
        } else {
            $this->Session->setFlash('Identificador inválido');
            $this->redirect(array('action' => 'index'));
        }
    }   

    private function prepararCampos()
    {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();

        $this->Functions->limparMascara($this->data['Licitacao']['nu_licitacao']);
        $this->Functions->limparMascara($this->data['Licitacao']['vl_estimado']);
        $this->Functions->limparMascara($this->data['Licitacao']['vl_contratado']);
        $this->Functions->limparMascara($this->data['Licitacao']['nu_diferenca']);

    }

    public function anexos($coLicitacao=0) {
        $this->layout = 'iframe';
        $this->helpers[] = 'Imprimir';

        asort($this->tpAnexo);
        $this->Anexo->recursive = 0;

        $fields = array('Anexo.co_anexo', 'Anexo.co_anexo_pasta', 'Anexo.ds_extensao', 'Anexo.tp_documento', 'Anexo.ic_atesto', 'Anexo.ds_observacao', 'Anexo.ds_anexo', 'Anexo.dt_anexo', 'Anexo.assinatura', 'Atividade.ds_atividade', 'Anexo.co_licitacao', 'Anexo.caminho');

        $this->paginate = array(
            'fields' => $fields,
            'limit' => 1000
        );

        $anexos = $this->paginate('Anexo', array(
            'Anexo.co_licitacao' => $coLicitacao
        ));

        $this->set('coLicitacao', $coLicitacao);
        $this->set('anexos', $anexos);
        $this->set('tpAnexo', $this->tpAnexo);
    }

    public function iframe($coLicitacao=0, $modulo = "padrao", $idModulo = 0) {
        $this->set('coLicitacao', $coLicitacao);
    }

    public function delete_anexo($coAnexo=0, $coLicitacao=0) {
        if (!$coAnexo) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'anexos',
                $coLicitacao
            ));
        }

        $anexo = $this->Anexo->read(array('caminho'), $coAnexo);

        if ($this->Anexo->delete($coAnexo, true)) {
	    	App::import('Helper', 'Modulo');
	    	$modulo = new ModuloHelper();    
	    	
	    	if ( $modulo->isDiretorioFisico() ) {
	            $file = ROOT . DS . APP_DIR . DS . 'data' . DS . $anexo['Anexo']['caminho'];
	            @unlink($file);
	    	}
	    	
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array('action' => 'anexos', $coLicitacao));
        } else {
            $this->Session->setFlash(__('Não possível excluir o registro', true));
            $this->redirect(array('action' => 'anexos', $coLicitacao));
        }
    }

    public function add_anexo($coLicitacao) {
        $this->layout = 'iframe';
        $this->helpers[] = 'Imprimir';


        if (! empty($this->data)) {
            $this->data['Anexo']['co_licitacao'] = $coLicitacao;

            $this->Anexo->create();

			if (  $this->Anexo->save($this->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso', true));
				$this->redirect(array(
					'action' => 'anexos', $coLicitacao
				));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
			}
        }

        $this->set('coLicitacao', $coLicitacao);
    }

    public function download($file, $dsAnexo='') {
        $this->view = 'Media';
        $fileArr = explode('.', $file);
        
        $params = array(
              'id' => $file,
              'download' => true,
              'extension' => $fileArr[1], 
              'path' => APP . 'data' . DS  // don't forget terminal 'DS'
       );
        
       $this->set($params);
    }

    public function relatorios() {

    	$this->layout = 'externo';
    	
        $icLicitacao = 0;
        $coContratacao = null;
        $coSituacao = null;
        $palavraChave = null;

        unset($this->Licitacao->validate['co_contratacao']);
        unset($this->Licitacao->validate['co_situacao']);

        $relatorio = array();

        $this->paginate = array(
            'limit' => 100
        );

        if (isset($this->params['url']['tipo_relatorio']) && ($this->params['url']['tipo_relatorio'] != 'C')) {
            $this->data['tipo_relatorio'] = $this->params['url']['tipo_relatorio'];
            $this->set('tipoRelatorio', $this->data['tipo_relatorio']);
            $icLicitacao = 1;
        } else {
            $this->data['tipo_relatorio'] = 'C';
            $this->set('tipoRelatorio', 'C');
        }

        $contratacoes = $this->Contratacao->find('list', array(
            'conditions' => array('Contratacao.ic_licitacao' => $icLicitacao)
        ));

        $situacoes = $this->Situacao->find('list', array(
            'conditions' => array('Situacao.ic_licitacao' => $icLicitacao)
        ));

        $conditions = array();

        if (isset($this->params['url']['co_situacao']) && !empty($this->params['url']['co_situacao'])) {
            $conditions['Situacao.co_situacao'] = $coSituacao = $this->params['url']['co_situacao']; 
            $this->data['co_situacao'] = $this->params['url']['co_situacao'];
        } 

        if (isset($this->params['url']['co_contratacao']) && !empty($this->params['url']['co_contratacao'])) {
            $conditions['Contratacao.co_contratacao'] = $coContratacao = $this->params['url']['co_contratacao']; 
            $this->data['co_contratacao'] = $this->params['url']['co_contratacao'];
        }

        if (isset($this->params['url']['palavra_chave']) && !empty($this->params['url']['palavra_chave'])) {
            $palavraChave = $this->params['url']['palavra_chave'];
            $this->data['palavra_chave'] = $this->params['url']['palavra_chave'];
        }
        
        // get contratos
        if( !isset($this->params['url']['tipo_relatorio']) || $this->params['url']['tipo_relatorio'] == 'C') {
            // $conditions['OR']['Contrato.nu_processo LIKE'] = '%'.$this->params['url']['palava_chave'].'%';
            // $conditions['OR']['Contrato.nu_contrato LIKE'] = '%'.$this->params['url']['palava_chave'].'%';
            $relatorio = $this->Licitacao->getRelatorioContrato($palavraChave, $coContratacao, $coSituacao);
        }

        // get empresas vencedoras
        if (isset($this->params['url']['tipo_relatorio']) && $this->params['url']['tipo_relatorio'] == 'E') {
            $conditions['Fornecedor.ic_ativo'] = 1;
            $conditions['Fornecedor.ic_suspenso'] = 1;
            
            if ($palavraChave) {
                $conditions['OR']['Fornecedor.no_razao_social LIKE'] = '%'.$palavraChave.'%';
                $conditions['OR']['Fornecedor.ds_penalidade_aplicada LIKE'] = '%'.$palavraChave.'%';
            }
             
             $relatorio = $this->Fornecedor->find('all', array(
                 'conditions' => $conditions
            ));
        }

        // get licitacoes
        if (isset($this->params['url']['tipo_relatorio']) && $this->params['url']['tipo_relatorio'] == 'L') {
            if ($palavraChave) {
                $conditions['OR']['Licitacao.nu_licitacao LIKE'] = '%'.$palavraChave.'%';
                $conditions['OR']['Licitacao.ds_objeto LIKE'] = '%'.$palavraChave.'%';
                $conditions['OR']['Licitacao.nu_processo LIKE'] = '%'.$palavraChave.'%';
            }

            $conditions['Licitacao.ic_ativo'] = 1;
            $relatorio = $this->Licitacao->find('all', array(
                 'conditions' => $conditions
            ));
        }

        // impressão
        if (isset($this->params['url']['imprimir'])) {
                App::import('Vendor', 'pdf');
                App::import('Vendor', 'htmlpdf');
                App::import('Helper', array(
                    'Print', 'Formatacao', 'Number'
                ));

                $pdf = new PDF();

                if ($this->data['tipo_relatorio'] == 'C') {
                    $pdf->imprimir($this->tableContratoHtml($relatorio));
                } else if ($this->data['tipo_relatorio'] == 'L') {
                    $pdf->imprimir($this->tableLicitacaoHtml($relatorio));
                } if ($this->data['tipo_relatorio'] == 'E') {
                    $pdf->imprimir($this->tableEmpresaHtml($relatorio));
                }
                exit();

        }

        $this->set('contratacoes', $contratacoes);
        $this->set('situacoes', $situacoes);
        $this->set('relatorio', $relatorio);
        $this->set('url', $this->params['url']['url']);
    }

    public function arquivosDownload($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'relatorios'
            ));
        }

        $conditions = array();
        $conditions['Anexo.co_licitacao'] = $id;
        $resultadoArquivos = $this->paginate('Anexo', array($conditions));
		
        if(count($resultadoArquivos) > 0) {
            $arquivos = array();
            foreach ($resultadoArquivos as  $key => $value){
                array_push($arquivos, array(
                	'caminho' => $value['Anexo']['caminho'],
                	'ds_anexo' => $value['Anexo']['ds_anexo']
                ));
            }
            $zipfilename  	= md5(time()) . '.zip';
            $diretorio      = ROOT . DIRECTORY_SEPARATOR . APP_DIR . DIRECTORY_SEPARATOR . "data";
            $zipfilenamefullpath  = $diretorio . DIRECTORY_SEPARATOR . $zipfilename;
    		            
            $zip = new ZipArchive();    
            
            if( $zip->open($zipfilenamefullpath, ZIPARCHIVE::CREATE) ){
                // adiciona ao zip todos os arquivos contidos no diretório.
                foreach($arquivos as $file){
                    $zip->addFile($file['caminho'], $file['ds_anexo']);
                }
            	// fechar o arquivo zip após a inclusão dos arquivos desejados
                $zip->close();
            }
            
            if(file_exists($zipfilenamefullpath)){
                // Forçamos o donwload do arquivo.
                header('Content-Type: application/zip');
                header('Content-Disposition: attachment; filename="'.$zipfilename.'"');
                readfile($zipfilenamefullpath);
                //removemos o arquivo zip após download
                unlink($zipfilenamefullpath);
			}
			exit('fail');
        } else {
            $this->Session->setFlash(__('A licitação não possui arquivos', true));
            $this->redirect(array(
                'action' => 'relatorios?tipo_relatorio=L'
            ));
        }
    }

    private function tableContratoHtml($data) {
        $helper = new HtmlHelper( );
        $contentHtml = "";
        $printHelper = new PrintHelper();
        $formatacaoHelper = new FormatacaoHelper();
        $formatacaoHelper->Number = new NumberHelper;

        if (count($data)) {
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;','class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(
                array('Nº Contrato', 'Nº Processo' ,'Data de publicação', 'Modalidade de Contratação', 'Fundamento Legal', 'CNPJ', 'Fornecedor', 'Objeto', 'Período da Vigência', 'Situação', 'Valor Global', 'Nota de Empenho', 'Aditivo', 'Data do Aditivo')
            );
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;

            $cells = array();
            foreach($data as $index => $contrato) {
                $cells[] = array(
                    $printHelper->contrato($contrato['Contrato']['nu_contrato']),
                    $printHelper->processo($contrato['Contrato']['nu_processo']),
                    $contrato[0]['dt_publicacao'],
                    $contrato['Contratacao']['ds_contratacao'],
                    $contrato['Contrato']['ds_fundamento_legal'],
                    $printHelper->cnpjCpf($contrato['Fornecedor']['nu_cnpj'], $contrato['Fornecedor']['tp_fornecedor']),
                    $contrato['Fornecedor']['no_razao_social'],
                    $contrato['Contrato']['ds_objeto'],
                    "{$contrato[0]['dt_ini_vigencia']} - {$contrato[0]['dt_fim_vigencia']}",
                    $contrato['Situacao']['ds_situacao'],
                    $formatacaoHelper->moeda($contrato['Contrato']['vl_global']),
                    $contrato['NotaFiscal']['nu_nota'],
                    $contrato['Aditivo']['no_aditivo'],
                    $contrato[0]['dt_aditivo']
                );
            }

            $contentHtml .= $helper->tableCells( $cells, array('class'=> $class) );

            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . count($data) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }

        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }

        return $contentHtml;
    }

    private function tableEmpresaHtml($data) {
        $helper = new HtmlHelper( );
        $contentHtml = "";
        $printHelper = new PrintHelper();
        $formatacaoHelper = new FormatacaoHelper();
        $formatacaoHelper->Number = new NumberHelper;

        if (count($data)) {
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;','class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(
                array('Nome da Empresa', 'CNPJ' ,'Penalidade Aplicada', 'Período da Penalidade')
            );
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;
            $cells = array();
            foreach($data as $index => $empresa) {
                $cells[] = array(
                    $empresa['Fornecedor']['no_razao_social'],
                    $printHelper->cnpjCpf($empresa['Fornecedor']['nu_cnpj'], $empresa['Fornecedor']['tp_fornecedor']),
                    $empresa['Fornecedor']['ds_penalidade_aplicada'],
                    "{$empresa['Fornecedor']['dt_ini_penalidade']} - {$empresa['Fornecedor']['dt_fim_penalidade']}",
                    // $empresa['Contratacao']['ds_contratacao']
                );
            }

            $contentHtml .= $helper->tableCells( $cells, array('class'=> $class) );

            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . count($data) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }

        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }

        return $contentHtml;
    }

    private function tableLicitacaoHtml($data) {
        $helper = new HtmlHelper( );
        $contentHtml = "";
        $printHelper = new PrintHelper();
        $formatacaoHelper = new FormatacaoHelper();
        $formatacaoHelper->Number = new NumberHelper;

        if (count($data)) {
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;','class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(
                array('Data de Publicação', 'Data da Abertura' , 'Nª Licitação', 'Modalidade de Contratação', 'Nª Processo', 'Fundamento Legal', 'Objeto', 'Situação')
            );
            
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;

            $cells = array();
            foreach($data as $index => $licitacao) {
                $cells[] = array(
                    $licitacao['Licitacao']['dt_publicacao'],
                    $licitacao['Licitacao']['dt_abertura'],
                    $printHelper->licitacao($licitacao['Licitacao']['nu_licitacao']),
                    $licitacao['Contratacao']['ds_contratacao'],
                    $printHelper->processo($licitacao['Licitacao']['nu_processo']),
                    'Fundamento Legal',
                    $licitacao['Licitacao']['ds_objeto'],
                    $licitacao['Situacao']['ds_situacao']
                );
            }

            $contentHtml .= $helper->tableCells( $cells, array('class'=> $class) );

            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . count($data) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }

        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }

        return $contentHtml;
    }
}
?>
