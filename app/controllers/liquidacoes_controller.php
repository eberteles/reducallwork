<?php
/**
* @resource { "name": "Liquidações", "route":"liquidacoes", "access": "private", "type": "module" }
*/
class LiquidacoesController extends AppController
{

    var $name = 'Liquidacoes';

    var $layout = 'iframe';

    /**
    * @resource { "name": "Liquidações", "route":"liquidacoes\/index", "access": "private", "type": "select" }
    */
    public function index($coContrato)
    {
        $this->Liquidacao->recursive = 1;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Liquidacao.co_contrato' => $coContrato
            )
        );

        $this->set('liquidacoes', $this->paginate());
        $this->set('tiposDAR', $this->Liquidacao->tipoDAR);
        
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "iframe", "route":"liquidacoes\/iframe", "access": "private", "type": "select" }
    */
    public function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Nova Liquidação", "route":"liquidacoes\/add", "access": "private", "type": "insert" }
    */
    public function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->data['Liquidacao']['vl_liquidacao'] = ln($this->data['Liquidacao']['vl_liquidacao']);
            $this->data['Liquidacao']['co_contrato'] = $coContrato;
//            $this->data['Liquidacao']['vl_liquidacao'] = ln($this->data['Liquidacao']['vl_imposto']);

            $this->Liquidacao->create();
            if ($this->Liquidacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set('notasFiscais', $this->Liquidacao->NotaFiscal->find('list', array(
            'conditions' => array(
                'co_contrato' => $coContrato,
                'NotaFiscal.co_nota not in (select co_nota from liquidacao where co_nota > 0)'
            )
        )));
    }

    /**
    * @resource { "name": "Editar Liquidação", "route":"liquidacoes\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            $this->data['Liquidacao']['vl_liquidacao'] = ln($this->data['Liquidacao']['vl_liquidacao']);
            if ($this->Liquidacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Liquidacao->read(null, $id);
        }
        $this->data['Liquidacao']['vl_liquidacao'] = str_replace('.', ',', $this->data['Liquidacao']['vl_liquidacao']);
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('liquidacao', $this->data['Liquidacao']);
//        $this->set('empenhos', $this->Liquidacao->Empenho->find('list', array(
//            'conditions' => array(
//                'co_contrato' => $coContrato
//            )
//        )));
        $this->set('notasFiscais', $this->Liquidacao->NotaFiscal->find('list', array(
            'conditions' => array(
                'co_contrato' => $coContrato
            )
        )));
    }

    /**
    * @resource { "name": "Remover Liquidação", "route":"liquidacoes\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Liquidacao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}

?>