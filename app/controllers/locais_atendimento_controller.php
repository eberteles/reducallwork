<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 17:13
 */

class LocaisAtendimentoController extends AppController
{
    var $name = "LocalAtendimento";
    var $layout = 'iframe';

    public function index($coFornecedor)
    {
        $this->LocalAtendimento->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'LocalAtendimento.ic_ativo' => 1
            )
        );
        $criteria['LocalAtendimento.co_fornecedor'] = $coFornecedor;

        $this->set('locais', $this->paginate($criteria));
        // echo '<pre>';print_r($this->paginate($criteria));exit;
        $this->set('coFornecedor', $coFornecedor);
    }

    public function exames($id = null, $coFornecedor = null)
    {  //id = locais_atendimento.id
        $this->loadModel('ExameConsulta');
        $this->loadModel('LocaisExames');

        /*$this->paginate = array(
            'limit' => 8,
            'order' => array(
                'LocaisExames.id' => 'asc'
            ),
            'conditions' => array('LocaisExames.co_locais_atendimento' => $id)
        );*/

        if(!empty($this->data)) {
            $this->autoRender = false;
            // echo '<pre>';print_r($this->data);exit;
            $locais_atendimento = $this->data['ExameConsulta']['co_locais_atendimento'];
            $this->ExameConsulta->virtualFields = array(
                'nome_combo' => "CONCAT( ExameConsulta.codigo, ' - ', ExameConsulta.nome )"
            );

            /*$lex = $this->LocaisExames->find('all', array(
                'conditions' => array(
                    'LocaisExames.co_locais_atendimento' => $locais_atendimento
                )
            ));

            foreach ($lex as $le) {
                $this->LocaisExames->delete($le['LocaisExames']['id']);
            }*/

            $id_locais_exame = '';
            $msg = '';
            $nomeExame = '';
            $idexame = '';
            if($this->data['ExameConsulta']['exames']) {
                foreach ($this->data['ExameConsulta']['exames'] as $exame) {
                    $local_exames['LocaisExames']['co_locais_atendimento'] = $locais_atendimento;
                    $local_exames['LocaisExames']['co_exames'] = $idexame = $exame;

                    $this->LocaisExames->create();
                    if ($this->LocaisExames->save($local_exames)) {
                        $nomeExame = $this->ExameConsulta->read(array('nome', 'codigo'), $exame);
                        // echo '<pre>';print_r($nomeExame);exit;
                        $idLocaisExame = $this->LocaisExames->getLastInsertID();
                        // $idexame = $exame['LocaisExames']['id'];
                        $msg = 'Registro salvo com sucesso.';
                    } else {
                        $msg = 'Não foi possivel salvar.';
                    }
                }
            }
            /*$this->Session->setFlash(__('Registro salvo com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));*/
            echo json_encode(array(
                'msg' => $msg,
                'exame' => array(
                    'id' => $idLocaisExame,
                    'nome'=> $nomeExame['ExameConsulta']['codigo'] . ' - ' . $nomeExame['ExameConsulta']['nome'],
                    'idexame' => $idexame
                )
            ));
        }

        $this->set('id', $id);
        $this->set('coFornecedor', $coFornecedor);

        $this->ExameConsulta->displayField = 'nome_combo';
        $this->ExameConsulta->virtualFields = array(
            'nome_combo' => "CONCAT( ExameConsulta.codigo, ' - ', ExameConsulta.nome )"
        );

        $exames = $this->ExameConsulta->find('list');
        $this->data = $this->LocaisExames->find('all', array(
            'conditions' => array('LocaisExames.co_locais_atendimento' => $id)
        ));
        // echo '<pre>';print_r($this->data);exit;
        $idsExames = array();
        foreach ($this->data as $key => $arr) {
            array_push($idsExames, $arr['ExameConsulta']['id']);
        }
        // echo '<pre>';print_r($exames); echo '</pre>';
        // echo '<pre>';print_r($this->data);echo '</pre>';exit;
        $this->set('exames', $exames);
        $this->set('idsExames', $idsExames);
    }

    public function especialidades($id = null, $coFornecedor = null)
    {
        $this->loadModel('Especialidade');
        $this->loadModel('LocaisEspecialidades');

        if(!empty($this->data)) {
            $locais_atendimento = $this->data['Especialidade']['co_locais_atendimento'];

            $les = $this->LocaisEspecialidades->find('all', array(
                'conditions' => array(
                    'LocaisEspecialidades.co_locais_atendimento' => $locais_atendimento
                )
            ));

            foreach ($les as $le) {
                $this->LocaisEspecialidades->delete($le['LocaisEspecialidades']['id']);
            }

            if($this->data['Especialidade']['especialidades'] != "") {
                foreach ($this->data['Especialidade']['especialidades'] as $especialidade) {
                    $local_especialidades['LocaisEspecialidades']['co_locais_atendimento'] = $locais_atendimento;
                    $local_especialidades['LocaisEspecialidades']['co_especialidade'] = $especialidade;

                    $this->LocaisEspecialidades->create();
                    $this->LocaisEspecialidades->save($local_especialidades);
                }
            }
            $this->Session->setFlash(__('Registro salvo com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }

        $this->set('id', $id);
        $this->set('coFornecedor', $coFornecedor);

        $this->set('especialidades', $this->Especialidade->find('list', array(
            'conditions' => array(
                'Especialidade.ic_ativo' => 1
            ),
            'fields' => array(
                'Especialidade.co_especialidade','Especialidade.ds_especialidade'
            )
        )));

        $this->data = $this->LocaisEspecialidades->find('all', array(
            'conditions' => array(
                'LocaisEspecialidades.co_locais_atendimento' => $id
            )
        ));
    }

    function iframe($coFornecedor)
    {
        $this->set(compact('coFornecedor'));
    }

    public function add($coFornecedor = null)
    {
        $this->loadModel('Uf');
        $this->loadModel('Bairro');
        if(!empty($this->data)) {
            $this->LocalAtendimento->create();

            if($this->LocalAtendimento->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(

                    'action' => 'index',
                    $coFornecedor
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('ufs', $this->Uf->find('list'));
        $this->set('bairros', $this->Bairro->find('list'));
        if($coFornecedor != null)
            $this->set('coFornecedor', $coFornecedor);
    }

    public function edit($id = null, $coFornecedor = null)
    {
        $this->loadModel('Uf');
        $this->loadModel('Bairro');
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }

        if(!empty($this->data)) {
            $coFornecedor = $this->data['LocalAtendimento']['co_fornecedor'];
            if($this->LocalAtendimento->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coFornecedor
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('ufs', $this->Uf->find('list'));
        $this->set('bairros', $this->Bairro->find('list'));
        $this->set('coFornecedor', $coFornecedor);
        $this->data = $this->LocalAtendimento->read(null, $id);
    }

    public function logicDelete($id = null, $coFornecedor)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }

        if ( $id ) {
            $this->LocalAtendimento->id = $id;
            $msg = null;
            $especialidade['LocalAtendimento']['ic_ativo'] = 0;

            if($this->LocalAtendimento->saveField('ic_ativo',0)){
                $msg = "Local de Atendimento bloqueado com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index',
                $coFornecedor
            ));
        }
    }

    public function updateExameValor($id=0) {
        // echo 'n: ';echo doubleval($this->data);
        $this->autoRender = false;
        $this->loadModel('LocaisExames');
        $locais_exames = array('LocaisExames' => array(
            'id' => $id,
            'valor' => $this->data
        ));
        // $local_exames['LocaisExames']['id'] = $id;
        // $local_exames['LocaisExames']['valor'] = $this->data;
        // print_r($local_exames);exit;
        if ($this->LocaisExames->save($locais_exames)) {
            echo json_encode(array('msg' => 'O Registro foi salvo com successo, valor: ' . number_format($this->data, 3, '.' , ',')));
        } else {
            // debug($this->validationErrors);exit;
            echo json_encode(array('msg' => 'Não foi possivel salvar o registro'));
        }
    }

    public function deleteExameVinculado($id=0) {
        $this->loadModel('LocaisExames');
        $this->autoRender = false;
        $msg = 'O Registro foi excluído com sucesso';

        if ($this->LocaisExames->delete($id)) {
            echo json_encode(array('msg' => $msg));
        } else {
            echo json_encode(array('msg' => 'Não foi possivel excluir o registro'));
        }
    }
}
