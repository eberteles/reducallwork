<?php

/**
 * @resource { "name" : "Log Siafi", "route":"log_siafis", "access": "private", "type": "module" }
 */
class LogSiafisController extends AppController
{

    var $name = 'LogSiafi';

    /**
     * @resource { "name" : "Listar logs SIAFI", "route":"log_siafis\/index", "access": "private", "type": "select" }
     */
    public function index()
    {

        $this->gerarFiltro();
        $this->LogSiafi->recursive = 0;
        $this->LogSiafi->validate = array(
//            'dt_ini_log' => array(
//                'notempty' => array(
//                    'rule' => array('notempty'),
//                    'required' => true,
//                    'message' => 'Data de início é obrigatória'
//                ),
//            ),
        );


        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'LogSiafi.dt_operacao' => 'desc'
            ),
            'fields' => array(
                "DATE_FORMAT(dt_operacao, '%Y-%m-%d') dt_operacao",
                'COUNT(CASE WHEN tp_entidade = 1 THEN 1 END) pagamento',
                'COUNT(CASE WHEN tp_entidade = 2 THEN 1 END) liquidacao',
                'COUNT(CASE WHEN tp_entidade = 3 THEN 1 END) nota',
                'COUNT(CASE WHEN tp_entidade = 4 THEN 1 END) empenho'
            ),
            'group' => array("DATE_FORMAT(dt_operacao, '%Y-%m-%d')")
        );
        $criteria = null;

        if (!empty($this->data)) {
            if (isset($this->data['LogSiafi']['tp_entidade']) && $this->data['LogSiafi']['tp_entidade'] > 0) {
                $criteria['LogSiafi.tp_entidade'] = $this->data['LogSiafi']['tp_entidade'];
            }
            if (isset($this->data['LogSiafi']['nu_uasg']) && $this->data['LogSiafi']['nu_uasg'] != '') {
                $criteria['LogSiafi.nu_uasg'] = $this->data['LogSiafi']['nu_uasg'];
            }
            if (isset($this->data['LogSiafi']['nu_empenho']) && $this->data['LogSiafi']['nu_empenho'] != '') {
                $criteria['LogSiafi.nu_empenho'] = $this->data['LogSiafi']['nu_empenho'];
            }

            if (($this->data['LogSiafi']['dt_ini_log'] != '') && ($this->data['LogSiafi']['dt_fim_log'] != '')) {
                $dt_ini = DateTime::createFromFormat('d/m/Y', $this->data['LogSiafi']['dt_ini_log']);
                $dt_fim = DateTime::createFromFormat('d/m/Y', $this->data['LogSiafi']['dt_fim_log']);

                $intervalo = $dt_fim->diff($dt_ini);

                if ($intervalo->invert == 0 && $intervalo->days > 0) {
                    $this->Session->setFlash(__('A data final deve ser maior que a data inicial', true));
                    $criteria = null;
                    $criteria['LogSiafi.nu_empenho'] = '0';
                } else {
                    $criteria["DATE(dt_operacao) BETWEEN '" .
                    dtDb($this->data['LogSiafi']['dt_ini_log']) . "' AND"] =
                        dtDb($this->data['LogSiafi']['dt_fim_log']);
                }

            } elseif ($this->data['LogSiafi']['dt_ini_log'] && !$this->data['LogSiafi']['dt_fim_log']) {
                $criteria['DATE(dt_operacao) >='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['LogSiafi']['dt_ini_log'])
                        ->format('Y-m-d');
            } elseif (!$this->data['LogSiafi']['dt_ini_log'] && $this->data['LogSiafi']['dt_fim_log']) {
                $criteria['DATE(dt_operacao) <='] =
                    DateTime::createFromFormat('d/m/Y', $this->data['LogSiafi']['dt_fim_log'])
                        ->format('Y-m-d');
            }
        }

//        if (!$criteria) {
//            $criteria['DATE(dt_operacao) >='] = DateTime::createFromFormat('Y-m-d', date('Y-m-d'))
//                ->sub(new DateInterval('P1D'))
//                ->format('Y-m-d');
//        }

//        echo '<pre>';
//        print_r($criteria);
//        die('<b>File: </b>'.__FILE__.' <br/><b>Linha: </b>'. __LINE__);

        $this->set('arrRegistroLog', $this->paginate($criteria));
        $this->set('tiposEntidade', $this->LogSiafi->getTiposEntidade());
    }

    /**
     * @resource { "name" : "detalhar log ", "route":"log_siafis\/detalhar", "access": "private", "type": "select" }
     */
    public function detalhar($tipo, $data)
    {
        $this->layout = 'blank';

        $dados = $this->LogSiafi->find('all', array(
            'conditions' => array(
                "DATE_FORMAT(dt_operacao, '%Y-%m-%d')" => $data,
                'LogSiafi.tp_entidade' => $tipo
            ),
            'order' => array(
                'LogSiafi.nu_empenho' => 'asc'
            )
        ));

        $arrDados = array();
        foreach ($dados as $dado) {
            $arrDados[$dado['LogSiafi']['nu_empenho']][] = $dado['LogSiafi'];
        }

        $this->set(compact('arrDados'));
        $this->set('tipoEntidade', $tipo);

    }

}
