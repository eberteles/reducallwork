<?php

/**
 * @resource { "name": "Log de Sistema", "route":"logs", "access": "private", "type": "module" }
 */
class LogsController extends AppController
{

    var $name = 'Logs';

    /**
     * @resource { "name": "Log de Acesso", "route":"logs\/index", "access": "private", "type": "select" }
     */
    function index()
    {
        $this->gerarFiltro();
        $this->Log->recursive = 0;
        $this->Log->validate = array();
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Log.tp_acao' => 'L'
            ),
            'order' => array(
                'Log.dt_log' => 'desc'
            )
        );
        $criteria = null;
        if (!empty($this->data)) {
            if ($this->data['Log']['dt_ini_log'] && $this->data['Log']['dt_ini_log']) {
                $dt_ini = new DateTime(dtDb($this->data['Log']['dt_ini_log']));
                $dt_fim = new DateTime(dtDb($this->data['Log']['dt_fim_log']));

                $intervalo = $dt_fim->diff($dt_ini);
                if ($intervalo->invert == 0) {
                    $criteria['DATE(dt_log)'] = dtDb($this->data['Log']['dt_ini_log']);
                } else {
                    if (isset($this->data['Log']['co_usuario']) && $this->data['Log']['co_usuario'] > 0) {
                        $criteria['Log.co_usuario'] = $this->data['Log']['co_usuario'];
                    }
                    if (isset($this->data['Log']['dt_ini_log']) && strlen($this->data['Log']['dt_ini_log']) == 10 && $this->data['Log']['dt_fim_log'] == null) {
                        $criteria['DATE(dt_log) >'] = dtDb($this->data['Log']['dt_ini_log']);
                    }
                    if (isset($this->data['Log']['dt_fim_log']) && strlen($this->data['Log']['dt_fim_log']) == 10 && $this->data['Log']['dt_ini_log'] == null) {
                        $criteria['DATE(dt_log) <'] = dtDb($this->data['Log']['dt_fim_log']);
                    }
                    if (isset($this->data['Log']['dt_fim_log']) && strlen($this->data['Log']['dt_fim_log']) == 10 && isset($this->data['Log']['dt_ini_log']) && strlen($this->data['Log']['dt_ini_log']) == 10) {
                        $criteria["DATE(dt_log) BETWEEN '" . dtDb($this->data['Log']['dt_ini_log']) . "' AND"] = dtDb($this->data['Log']['dt_fim_log']);
                    }
                    if (isset($this->data['Log']['tp_acao']) && strlen($this->data['Log']['tp_acao']) == 1) {
                        $criteria['tp_acao'] = $this->data['Log']['tp_acao'];
                    }
                }
            }

            if ($this->data['Log']['co_usuario']) {
                $criteria['Log.co_usuario'] = $this->data['Log']['co_usuario'];
            }
            if ($this->data['Log']['nu_cpf']) {
                $criteria['Usuario.nu_cpf'] = FunctionsComponent::limparMascara($this->data['Log']['nu_cpf']);
            }
            if ($this->data['Log']['ds_email']) {
                $criteria['Usuario.ds_email'] = $this->data['Log']['ds_email'];
            }
        }

        $this->set('glogs', $this->paginate($criteria));
        $this->set('tpAcao', $this->Log->tpAcao);
        $this->set('tpModulo', $this->Log->tpModulo);
    }

    /**
     * Busca usuarios
     *
     * @param $searchTerm
     * @return json
     */
    public function getUsersAutocomplete($searchTerm)
    {
        $arrData = null;
        $usuarios = $this->Log->Usuario->find('all', array(
            'fields' => array(
                'Usuario.co_usuario',
                'Usuario.no_usuario'
            ),
            'conditions' => array(
                'Usuario.no_usuario like' => '%' . $searchTerm . '%'
            )
        ));

        foreach ($usuarios as $usuario){
            $arrData[] = $usuario['Usuario'];
        }

        echo json_encode($arrData);
        exit();
    }
}

