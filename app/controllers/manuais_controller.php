<?php
/**
* @resource { "name": "Manual", "route":"manuais", "access": "private", "type": "module" }
*/
class ManuaisController extends AppController
{

    public $name = 'Manuais';

    public $autoRender = false;

    public $uses = null;

    /**
    * @resource { "name": "Baixar", "route":"manuais\/downloadManualGescon", "access": "private", "type": "select" }
    */
    public function downloadManualGescon()
    {
        header("Content-disposition: inline; filename=manual_gescon.pdf");
        header("Content-type: application/pdf");
        readfile(APP."/fileTemplates/manual_gescon.pdf");
    }
}