<?php
/**
* @resource { "name": "Cadastro de Tipos de Contrato", "route":"modalidades", "access": "private", "type": "module" }
*/
class ModalidadesController extends AppController
{

    public $name = 'Modalidades';

    /**
    * @resource { "name": "Tipo de Contrato", "route":"modalidades\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->gerarFiltro();
        
        $this->Modalidade->recursive = 0;
        
        $criteria = null;
        if ($this->RequestHandler->isPost() || isset($this->params['url']['imprimir']) || $this->params['isAjax']) {
            if (isset($this->data['Modalidade']['ds_modalidade'])) {
                $criteria['ds_modalidade like'] = '%' . up($this->data['Modalidade']['ds_modalidade']) . '%';
            }
            if (isset($this->data['Modalidade']['tp_modalidade'])) {
                $criteria['tp_modalidade'] = $this->data['Modalidade']['tp_modalidade'];
            }
            if (isset($this->data['Modalidade']['tp_prova'])) {
                $criteria['tp_prova'] = $this->data['Modalidade']['tp_prova'];
            }
        }

        // ajax
        if (isset($this->params['url']['imprimir']) || $this->params['isAjax']) {
            $this->gerarFiltro();
            App::import('Vendor', 'pdf');
            App::import('Vendor', 'htmlpdf');
            App::import('Helper', array(
                'Print'
            ));
            $pdf = new PDF();
            $modalidades = $this->paginate($criteria);
            // debug($modalidades);exit;
            $pdf->imprimir($this->tableModlidadesHtml($modalidades));
            exit();
        }
        
        $this->set('modalidades', $this->paginate($criteria));
        
        $this->set('tiposModalidade', $this->Modalidade->tiposModalidade);
        $this->set('tiposProva', $this->Modalidade->tiposProva);
        $this->set('url', $this->params['url']['url']);
    }

    /**
    * @resource { "name": "iframe", "route":"modalidades\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name": "Close iframe", "route":"modalidades\/close", "access": "private", "type": "select" }
     */
    function close( $co_modalidade )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_modalidade' ) );
    }

    /**
    * @resource { "name": "Listar", "route":"modalidades\/listar", "access": "private", "type": "select" }
    */
    function listar( $coConfederacao = 0 ) {
        
        $criterios  = array();
        if($coConfederacao > 0) {
            $criterios['joins'] = array(
                array('table' => 'modalidades_confederacoes',
                    'alias' => 'ModalidadesConfederacao',
                    'conditions' => array(
                        'ModalidadesConfederacao.co_confederacao = ' . $coConfederacao,
                        'ModalidadesConfederacao.co_modalidade = Modalidade.co_modalidade'
                    )
                )
            );
        }

        echo json_encode ( $this->Modalidade->find ( 'list', $criterios ) );

        exit ();
    }

    /**
    * @resource { "name": "Nova Modalidade", "route":"modalidades\/add", "access": "private", "type": "insert" }
    */
    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (!empty($this->data)) {

                $this->Modalidade->create();
                if ($this->Modalidade->save($this->data)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->Modalidade->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                    }
                } else {
                        $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
        }
        $this->set('isModal', $modal);
        $this->set('tiposModalidade', $this->Modalidade->tiposModalidade);
        $this->set('tiposProva', $this->Modalidade->tiposProva);
    }

    /**
    * @resource { "name": "Editar Modalidade", "route":"modalidades\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Modalidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Modalidade->read(null, $id);
        }
        $this->set('id', $id);
        $this->set('tiposModalidade', $this->Modalidade->tiposModalidade);
        $this->set('tiposProva', $this->Modalidade->tiposProva);
    }

    /**
    * @resource { "name": "Exclusão Lógica", "route":"modalidades\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete($id = null)
    {

        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Modalidade->read(null, $id)) {
            $modalidades = $this->Modalidade->read(null, $id);
            $msg = null;
            $modalidades['Modalidade']['ic_ativo'] = 1;

            if ($this->Modalidade->save($modalidades)) {
                $msg = "Registro deletado com sucesso!";
            } else {
                $msg = "Houve um erro na deleção do usuário";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Remover Modalidade", "route":"modalidades\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $modalidade = $this->Modalidade->find(array('co_modalidade' => $id));
        }

        if ($this->Modalidade->delete($id)) {

            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($modalidade['Modalidade']);
            $log['ds_tabela'] = 'modalidades';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    /**
    * @resource { "name": "iFrame Vincular", "route":"modalidades\/iframe_vincular", "access": "private", "type": "select" }
    */
    public function iframe_vincular($coModalidade)
    {
        $this->layout = 'blank';
        $this->set(compact('coModalidade'));
    }
    
    /**
    * @resource { "name": "Vinculação de Provas", "route":"modalidades\/provas", "access": "private", "type": "insert" }
    */
    public function provas($coModalidade)
    {
        $this->layout = 'iframe';
        $this->set(compact('coModalidade'));
        
        App::import('Model', 'Prova');
        
        $provaDb   = new Prova();
        
        if (! empty($this->data) && $this->data['Prova']['ds_prova']) {
            $provaDb->create();
            if ($provaDb->save($this->data)) {
                $this->Session->setFlash(__('Prova Vinculada com sucesso. Informe os dados Adicionais, caso seja necessário.', true));
                $this->redirect(array('action' => 'edit_prova', $provaDb->id));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        
        $this->set ( 'provas', $provaDb->find ( 'all', array( 'conditions' => array('co_modalidade' => $coModalidade) ) ) );
                
        $this->set('co_modalidade', $coModalidade);
    }
    
    /**
    * @resource { "name": "Edição da Vinculação de Provas", "route":"modalidades\/edit_prova", "access": "private", "type": "update" }
    */
    public function edit_prova($coProva)
    {
        $this->layout = 'iframe';
        $this->set(compact('coProva'));
        
        App::import('Model', 'Prova');
        
        $provaDb   = new Prova();
        
        if (! empty($this->data)) {
            if ($provaDb->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'provas', $this->data['Prova']['co_modalidade']
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $provaDb->read(null, $coProva);
        }
        
    }
    
    /**
    * @resource { "name": "Listar Vinculação de Provas", "route":"modalidades\/listar_provas", "access": "private", "type": "select" }
    */
    function listar_provas($coModalidade) {
        
        App::import('Model', 'Prova');
        
        $provaDb   = new Prova();

        echo json_encode ( $provaDb->find ( 'list', array( 'conditions' => array('co_modalidade' => $coModalidade) ) ) );

        exit ();
    }
    
    /**
    * @resource { "name": "Listar Sexo", "route":"modalidades\/get_is_sexo_prova", "access": "private", "type": "select" }
    */
    function get_is_sexo_prova($coProva) {
        
        App::import('Model', 'Prova');
        
        $provaDb   = new Prova();

        echo json_encode ( $provaDb->field( 'is_sexo', array( 'Prova.co_prova' => $coProva) ) );

        exit ();
    }

    /**
    * @resource { "name": "Tabela de Modalidades (HTML)", "route":"modalidades\/tableModlidadesHtml", "access": "private", "type": "select" }
    */
    public function tableModlidadesHtml($data) {
        $helper = new HtmlHelper( );
        $contentHtml = "";

        if (count($data)) {
            $contentHtml .= $helper->tag('table', null, array('style' => 'width: 100%; position: relative;','class' => 'table table-bordered table-striped', 'cellspacing' => '0', 'cellpadding' => '0'));
            $contentHtml .= $helper->tag('thead');
            $contentHtml .= $helper->tableHeaders(
                array('Número', 'Descrição')

            );
            $contentHtml .= $helper->tag('/thead');
            $contentHtml .= $helper->tag('tbody');

            $contador = 0;

            $cells = array();
            foreach($data as $modalidade) {

                $cells[] = array(
                    $modalidade['Modalidade']['nu_modalidade'],
                    $modalidade['Modalidade']['ds_modalidade']
                );
            }

            $contentHtml .= $helper->tableCells( $cells, array('class'=> $class) );

            $contentHtml .= '<tr class="total"><td colspan="' . count(array_keys($arData[0]['RELATORIO'])) . '">TOTAL DE REGISTROS: ' . count($data) . '</td></tr>';

            $contentHtml .= $helper->tag('/tbody');
            $contentHtml .= $helper->tag('/table');
        }

        if (isset($agrupador) && $contentHtml != "") {
            $contentHtml .= "<BR>";
        }

        return $contentHtml;

    }

}
?>
