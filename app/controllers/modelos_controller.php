<?php
App::import('Vendor', 'phpword/template');

class ModelosController extends AppController
{

    public $name = 'Modelos';

    private $templatePath;

    private $tempPath;

    private $mimeType = array(
        'docx' => array(
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ),
        'pdf' => array(
            'pdf' => 'application/pdf'
        )
    );

    function __construct()
    {
        $this->templatePath = realpath('.') . DS . '..' . DS . 'fileTemplates' . DS;
        $this->tempPath = realpath('.') . DS . '..' . DS . 'tmp' . DS . 'in04' . DS;
        parent::__construct();
    }


    private function check_file($params)
    {
        if(!is_file($params['path']))
        {
            echo utf8_decode("Arquivo " . $params['path'] . $params['name'] . ".". $params['extension'] ." não encontrado no servidor. Contate o administrador do sistema");
            die();
        }
        else
        {
            $params['path'] = $params['path'].DS;
            $this->view  = 'Media';
            $this->set($params);
        }

    }

    public function index()
    {}

    public function gerarCheckList()
    {

        $word = new PHPWord_Template($this->templatePath . 'IN 04 - Check List.docx');
        for ($i = 0; $i < 8; $i ++) {
            $id = '0' . ($i + 1);
            if (isset($this->params['form']['chkTemplate' . $id])) {
                $word->setValue('${chkTemplate' . $id . '}', 'X');
                $word->setValue('${obsTemplate' . $id . '}', ($this->params['form']['obsTemplate' . $id]));
            } else {
                $word->setValue('${chkTemplate' . $id . '}', '');
                $word->setValue('${obsTemplate' . $id . '}', '');
            }
        }
        @$fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('CheckList', $fileName);
        $this->check_file($params);

    }

    public function gerarInstrucoesPreenchimento()
    {

        $this->view = 'Media';
        $params = $this->getFileParam('InstrucoesPreenchimento');
        $this->check_file($params);
    }

    public function gerarOficializacaoDemanda()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '01.modelo-documento-in04-documento-de-oficializacao-da-demanda.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('OficializacaoDemanda', $fileName);
        $this->check_file($params);
    }

    public function gerarAnaliseViabilidade()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '02.modelo-documento-in04-analise-de-viabilidade.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('AnaliseViabilidade', $fileName);
        $this->check_file($params);
    }

    public function gerarPlanoSustentacao()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '03.modelo-documento-in04-plano-de-sustentacao.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('PlanoSustentacao', $fileName);
        $this->check_file($params);
    }

    public function gerarEstrategiaContratacao()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '04.modelo-documento-in04-estrategia-da-contratacao.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('EstrategiaContratacao', $fileName);
        $this->check_file($params);
    }

    public function gerarAnaliseRisco()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '05.modelo-documento-in04-analise-de-riscos.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('AnaliseRisco', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoReferencia()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '06.modelo-documento-in04-termo-de-referencia-ou-projeto-basico.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoReferencia', $fileName);
        $this->check_file($params);
    }

    public function gerarPlanoInsercao()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '07.modelo-documento-in04-plano-de-insercao.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('PlanoInsercao', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoCiencia()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '08.modelo-documento-in04-termo-de-ciencia.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoCiencia', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoCompromisso()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '09.modelo-documento-in04-termo-de-compromisso.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoCompromisso', $fileName);
        $this->check_file($params);
    }

    public function gerarOrdemServico()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '10.modelo-documento-in04-ordem-de-servico-ou-fornecimento-de-bens.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('OrdemServico', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoRecebimentoProvisorio()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '11.modelo-documento-in04-termo-de-recebimento-provisorio.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoRecebimentoProvisorio', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoRecebimentoDefinitivo()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '12.modelo-documento-in04-termo-de-recebimento-definitivo.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoRecebimentoDefinitivo', $fileName);
        $this->check_file($params);
    }

    public function gerarTermoEncerramentoContrato()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '13.modelo-documento-in04-termo-de-encerramento-do-contrato.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('TermoEncerramentoContrato', $fileName);
        $this->check_file($params);
    }

    public function gerarMemorando()
    {
        $this->view = 'Media';
        $word = new PHPWord_Template($this->templatePath . '14.modelo-memorando.docx');
        $fileName = $this->saveTempFile($word);
        $params = $this->getFileParam('Memorando', $fileName);
        $this->check_file($params);
    }

    public function limparArquivosTemporarios()
    {
        $this->autoRender = false;
        $files = glob($this->tempPath . DS . '*');
        foreach ($files as $file) {
            if (is_file($file))
                @unlink($file);
        }
    }

    private function saveTempFile($word)
    {
        $usuario = $this->Session->read('usuario');
        $fileName = mktime() . '_' . $usuario['Usuario']['no_usuario'] . '.docx';
        $word->save($this->tempPath . $fileName);
        return $fileName;
    }

    private function getFileParam($type, $fileName = null)
    {
        $tempArray = array(
            'CheckList' => array(
                'id' => $fileName,
                'name' => 'IN 04 - Check List',
                'extension' => 'docx'
            ),
            'OficializacaoDemanda' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 01. Oficialização de demanda',
                'extension' => 'docx'
            ),
            'AnaliseViabilidade' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 02. Análise de viabilidade',
                'extension' => 'docx'
            ),
            'PlanoSustentacao' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 03. Plano de sustentação',
                'extension' => 'docx'
            ),
            'EstrategiaContratacao' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 04. Estratégia de contratação',
                'extension' => 'docx'
            ),
            'AnaliseRisco' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 05. Análise de risco',
                'extension' => 'docx'
            ),
            'TermoReferencia' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 06. Termo de referência ou projeto básico',
                'extension' => 'docx'
            ),
            'PlanoInsercao' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 07. Plano de inserção',
                'extension' => 'docx'
            ),
            'TermoCiencia' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 08. Termo de ciência',
                'extension' => 'docx'
            ),
            'TermoCompromisso' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 09. Termo de compromisso',
                'extension' => 'docx'
            ),
            'OrdemServico' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 10. Ordem de serviço ou fornecimento de bens',
                'extension' => 'docx'
            ),
            'TermoRecebimentoProvisorio' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 11. Termo de recebimento provisório',
                'extension' => 'docx'
            ),
            'TermoRecebimentoDefinitivo' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 12. Termo de recebimento definitivo',
                'extension' => 'docx'
            ),
            'TermoEncerramentoContrato' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 13. Termo de encerramento do contrato',
                'extension' => 'docx'
            ),
            'Memorando' => array(
                'id' => $fileName,
                'name' => 'IN 04 - 14. Memorando',
                'extension' => 'docx'
            )
        )
        ;

        if ($type == 'InstrucoesPreenchimento') {
            return array(
                'id' => 'IN 04 - Instruções de preenchimento.pdf',
                'name' => 'IN 04 - Instruções de preenchimento',
                'extension' => 'pdf',
                'download' => true,
                'mimeType' => $this->mimeType['pdf'],
                'path' => $this->templatePath
            );
        } else {
            return array_merge($tempArray[$type], array(
                'download' => true,
                'path' => $this->tempPath,
                'mimeType' => $this->mimeType[$tempArray[$type]['extension']]
            ));
        }
    }
}
?>