<?php

/**
 * Movimento do processamento de arquivos
 */
class MovimentosController extends AppController
{

    public $name = 'Movimentos';

    public $autoRender = false;

    public $uses = array(
        'Movimento',
        'LogCritica'
    );

    /**
     * Obtém o movimento de um arquivo
     */
    public function obterMovimento($arquivo)
    {
        $movimento = $this->Movimento->find('first', array(
            'conditions' => array(
                'ds_nome_arquivo' => $arquivo
            )
        ));
        return $movimento['Movimento'];
    }

    /**
     * Identifica se o movimento é válido
     */
    public function isValido($movimento)
    {
        if ($movimento != null && $movimento['cs_sucesso'] == 'S') {
            return false;
        }
        
        return true;
    }

    /**
     * Inicia o movimento de um arquivo
     */
    public function iniciarMovimento($arquivo)
    {
        $movimento = array(
            'Movimento' => array(
                'cs_sucesso' => 'N',
                'dt_inicio' => date("Y-m-d H:i:s"),
                'ds_nome_arquivo' => $arquivo
            )
        );
        $this->Movimento->create();
        $movimento = $this->Movimento->save($movimento);
        $movimento = $movimento['Movimento'];
        
        // inclui id gerado
        $movimento[$this->Movimento->primaryKey] = $this->Movimento->getID();
        
        return $movimento;
    }

    /**
     * Finaliza o movimento de um arquivo
     */
    public function finalizarMovimento($movimento)
    {
        $this->Movimento->create($movimento);
        $this->Movimento->set('cs_sucesso', 'S');
        $this->Movimento->set('dt_fim', date("Y-m-d H:i:s"));
        return $this->Movimento->save();
    }

    /**
     * Registra erro no movimento
     * 
     * @param [type] $mensagem
     *            mensagem de erro
     * @param [type] $arquivo
     *            nome do arquivo
     */
    public function logErro($mensagem, $arquivo)
    {
        $movimento = $this->obterMovimento($arquivo);
        if (! empty($movimento)) {
            $dados = array(
                'LogCritica' => array(
                    'ds_critica' => $mensagem,
                    'dt_ocorrencia' => date("Y-m-d H:i:s"),
                    'co_movimento' => $movimento['co_movimento']
                )
            );
            
            $this->LogCritica->create();
            $this->LogCritica->save($dados);
        }
    }
}