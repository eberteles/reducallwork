<?php
/**
* @resource { "name": "Municipios", "route":"municipios", "access": "private", "type": "module" }
*/
class MunicipiosController extends AppController
{

    public $name = 'Municipios';

    /**
    * @resource { "name": "Listagem", "route":"municipios\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->Municipio->recursive = 0;
        $this->set('municipios', $this->paginate());
    }

    /**
    * @resource { "name": "Novo Município", "route":"municipios\/add", "access": "private", "type": "insert" }
    */
    public function add()
    {
        if (! empty($this->data)) {
            $this->Municipio->create();
            if ($this->Municipio->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    /**
    * @resource { "name": "Editar Município", "route":"municipios\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Municipio->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Municipio->read(null, $id);
        }
    }

    /**
    * @resource { "name": "Remover Município", "route":"municipios\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Municipio->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
    * @resource { "name": "Listar", "route":"municipios\/listar", "access": "private", "type": "select" }
    */
    public function listar($sg_uf)
    {
        $this->Municipio->recursive = 0;
        
        echo json_encode($this->Municipio->find('list', array(
            'conditions' => array(
                'sg_uf' => $sg_uf
            )
        )));
        
        exit();
    }
}
?>