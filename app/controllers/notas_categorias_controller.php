<?php
/**
* @resource { "name": "Categorias de notas", "route":"notas_categorias", "access": "private", "type": "module" }
*/
class NotasCategoriasController extends AppController
{

    var $name = 'NotasCategorias';
    
    var $uses   = array(
        'NotaCategoria'
    );
        
    /**
    * @resource { "name": "iframe", "route":"notas_categorias\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name": "Close iframe", "route":"notas_categorias\/close", "access": "private", "type": "select" }
     */
    function close( $co_nota_categoria )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_nota_categoria' ) );
    }

    /**
    * @resource { "name": "Listar", "route":"notas_categorias\/listar", "access": "private", "type": "select" }
    */
    function listar() {

        echo json_encode ($this->NotaCategoria->find('list'));

        exit ();
    }

    /**
    * @resource { "name": "Nova Nota Categoria", "route":"notas_categorias\/add", "access": "private", "type": "insert" }
    */
    function add($modal = false) 
    {
        if($modal) {
            $this->layout = 'iframe';
        }
        if (! empty($this->data)) {
            $this->NotaCategoria->create();

            if ($this->NotaCategoria->save($this->data)) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->NotaCategoria->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

}
?>
