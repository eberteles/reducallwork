<?php

/**
 * @resource { "name": "Notas Fiscais", "route":"notas_fiscais", "access": "private", "type": "module" }
 */
class NotasFiscaisController extends AppController
{

    public $name = 'NotasFiscais';

    public $helpers = array(
        'Imprimir', 'Util'
    );

    public $uses = array(
        'NotaFiscal',
        'NotaCategoria'
    );

    public $layout = 'iframe';

    public $situacoesAtesto = array(
        1 => 'Sim',
        0 => 'Não'
    );

    /**
     * @resource { "name": "Notas Fiscais", "route":"notas_fiscais\/index", "access": "private", "type": "select" }
     */
    public function index($coContrato, $coEmpenho = 0)
    {
        $this->NotaFiscal->hasOne = null;

        $criteria = array(
            'limit' => 10,
            'order' => array(
                'NotaFiscal.dt_recebimento' => 'asc'
            ),
            'conditions' => array(
                'NotaFiscal.co_contrato' => $coContrato
            )
        );

        if ($coEmpenho > 0) {
            $criteria['joins'][] = array(
                'table' => 'notas_empenhos',
                'alias' => 'NotaEmpenho',
                'conditions' => 'NotaFiscal.co_nota = NotaEmpenho.co_nota'
            );
            $criteria['conditions']['NotaEmpenho.co_empenho'] = $coEmpenho;
        }

        $this->paginate = $criteria;
        $notasFiscais = $this->paginate();

        $this->set('notas', $notasFiscais);
        // debug($notasFiscais);exit;

        $this->set('tiposDAR', $this->NotaFiscal->tipoDAR);

        $this->set(compact('coContrato'));
        $this->set(compact('coEmpenho'));
    }

    /**
     * @resource { "name": "iframe", "route":"notas_fiscais\/iframe", "access": "private", "type": "select" }
     */
    public function iframe($coContrato, $coEmpenho = 0)
    {
        $this->set(compact('coContrato'));
        $this->set(compact('coEmpenho'));
    }

    /**
     * @resource { "name": "Vincula Empenhos", "route":"notas_fiscais\/vinculaEmpenhos", "access": "private", "type": "insert" }
     */
    public function vinculaEmpenhos($empenhos, $coNota, $coContrato)
    {
        App::import('Model', 'NotasEmpenho');
        $ne = new NotasEmpenho();
        if (is_array($empenhos)) {
//            foreach ($empenhos as $co_empenho) {
//                $notaAtual = $ne->findByCoNota($coNota);
//                if (!$notaAtual) {
//                    $this->insereEmpenhoVinculado($co_empenho, $coNota);
//                } else {
//                    foreach ($notaAtual as $k => $na) {
//                        if (($na['co_empenho'] != $co_empenho) && ($k == 'NotasEmpenho')) {
//                            $this->insereEmpenhoVinculado($co_empenho, $coNota);
//                        }
//                    }
//                }
//            }

            foreach ($empenhos as $co_empenho) {
                $notaAtual = $ne->find('all', array('conditions' => array(
                    'NotasEmpenho.co_nota' => $coNota,
                    'NotasEmpenho.co_empenho' => $co_empenho,
                )));

                if (!$notaAtual) {
                    $this->insereEmpenhoVinculado($co_empenho, $coNota);
                }
            }
        }
    }

    /**
     * @resource { "name": "Insere Empenho Vinculado", "route":"notas_fiscais\/insereEmpenhoVinculado", "access": "private", "type": "insert" }
     */
    public function insereEmpenhoVinculado($co_empenho, $coNota)
    {
        App::import('Model', 'NotasEmpenho');
        $ne = new NotasEmpenho();

        $ne->create();
        $notaEmpenho = array();
        $notaEmpenho['NotasEmpenho']['dt_nota_empenho'] = date('Y-m-d');
        $notaEmpenho['NotasEmpenho']['co_empenho'] = $co_empenho;
        $notaEmpenho['NotasEmpenho']['co_nota'] = $coNota;
        $ne->save($notaEmpenho);
    }

    /**
     * @resource { "name": "Nova Nota Fiscal", "route":"notas_fiscais\/add", "access": "private", "type": "insert" }
     */
    public function add($coContrato)
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        if (!$modulo->isCampoObrigatorio('dt_recebimento')) {
            unset($this->NotaFiscal->validate['dt_recebimento']);
        }
        if (!empty($this->data)) {
            $this->NotaFiscal->create();
            if ($this->NotaFiscal->save($this->data)) {

                if (isset($this->data['NotaFiscal']['co_empenhos'])) {
                    $this->vinculaEmpenhos($this->data['NotaFiscal']['co_empenhos'], $this->NotaFiscal->getInsertID(), $coContrato);
                }

                if (!empty($this->data['NotaFiscal']['anexo']) && $this->data['NotaFiscal']['anexo']['name'] != '') {
                    $this->NotaFiscal->Anexo->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->data['NotaFiscal']['co_contrato'];
                    $anexo['Anexo']['co_nota'] = $this->NotaFiscal->getInsertID();
                    $anexo['Anexo']['tp_documento'] = '2';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = 'Nota Fiscal Nº: ' . $this->data['NotaFiscal']['nu_nota'];
                    $anexo['Anexo']['conteudo'] = $this->data['NotaFiscal']['anexo'];
                    $this->NotaFiscal->Anexo->save($anexo);
                }

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set('situacoesAtesto', $this->situacoesAtesto);

        $this->setDisplayFieldEmpenho();
        $this->set('empenhos', $this->NotaFiscal->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.co_contrato' => $coContrato,
                'Empenho.tp_empenho ' => 'O',
                "(SELECT sum(Empenho.vl_empenho) -
                        ifnull( (SELECT sum( n.vl_nota ) vl_restante
                        FROM notas_empenhos ne
                        JOIN empenhos em on ne.co_empenho = em.co_empenho
                        join notas_fiscais n on ne.co_nota = n.co_nota
                        WHERE em.co_contrato = {$coContrato}),0) FROM empenhos Empenho WHERE Empenho.co_contrato = {$coContrato}) > 0"
            ),
            'order' => 'Empenho.dt_empenho DESC'
        )));
        $notasCategoria = $this->NotaCategoria->find('list', array(
            // 'conditions' => array(
            //     'NotaCategoria.ic_ativo' => 2
            // )
        ));
        $this->set('categorias', $notasCategoria);
    }

    public function setDisplayFieldEmpenho($coNota = null)
    {

        $withNota = '';
        if ($coNota) {
            $withNota = "+ (select vl_nota from notas_fiscais where co_nota = {$coNota})";
        }

        $field = "CONCAT( CONCAT(Empenho.nu_empenho, ' - ', DATE_FORMAT(Empenho.dt_empenho, '%d/%m/%Y')), ' - ',
                    Concat('R$ ', Replace (Replace (Replace (Format( (Empenho.vl_restante -
                      ifnull(
                        (SELECT vl_restante
                            FROM notas_empenhos ne
                                left join notas_fiscais n on ne.co_nota = n.co_nota
                         WHERE Empenho.co_empenho = ne.co_empenho)
                      , 0) {$withNota} )
                    , 2), '.', '|'), ',', '.'), '|', ',')) )";

        $this->NotaFiscal->Empenho->displayField = 'nome_combo';
        $this->NotaFiscal->Empenho->virtualFields = array(
            'nome_combo' => $field
        );

    }

    /**
     * @resource { "name": "Editar Nota Fiscal", "route":"notas_fiscais\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null, $coContrato, $readonly = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        if (!empty($this->data)) {
            if ($this->NotaFiscal->save($this->data)) {
                if (isset($this->data['NotaFiscal']['co_empenhos'])) {
                    if ($this->data['NotaFiscal']['co_empenhos'] != '') {
                        $this->vinculaEmpenhos($this->data['NotaFiscal']['co_empenhos'], $id, $coContrato);
                    }

                    $notasEmpenhosSalvas = $this->NotaFiscal->NotasEmpenho->find('all', array(
                            'conditions'=>array(
                                'NotasEmpenho.co_nota' => $id
                            ),
                            'fields' =>array(
                                'NotasEmpenho.co_nota_empenho',
                                'NotasEmpenho.co_empenho'
                            )
                        )
                    );

                    /* Verifica e apaga nota_empenho caso um dos empenho tenha sido retirado da lista
                     * de empenhos enviadas no post
                     */
                    foreach ($notasEmpenhosSalvas as $key=>$notaEmpenho){
                        if (false === array_search($notaEmpenho['NotasEmpenho']['co_empenho'], $this->data['NotaFiscal']['co_empenhos'])){
                            $this->NotaFiscal->NotasEmpenho->delete($notaEmpenho['NotasEmpenho']['co_nota_empenho']);
                        }
                    }
                }

                if (!empty($this->data['NotaFiscal']['anexo']) && $this->data['NotaFiscal']['anexo']['name'] != '') {
                    $this->NotaFiscal->Anexo->deleteAll(array(
                        'co_nota' => $id
                    ));
                    $this->NotaFiscal->Anexo->create();
                    $anexo = array();
                    $anexo['Anexo']['co_contrato'] = $this->data['NotaFiscal']['co_contrato'];
                    $anexo['Anexo']['co_nota'] = $id;
                    $anexo['Anexo']['tp_documento'] = '2';
                    $anexo['Anexo']['dt_anexo'] = date('d/m/Y');
                    $anexo['Anexo']['ds_anexo'] = 'Nota Fiscal Nº: ' . $this->data['NotaFiscal']['nu_nota'];
                    $anexo['Anexo']['conteudo'] = $this->data['NotaFiscal']['anexo'];
                    $this->NotaFiscal->Anexo->save($anexo);
                }

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $this->data['NotaFiscal']['co_contrato']
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->NotaFiscal->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set('readOnly', ($readonly) ? true : false);
        $this->set(compact('id'));
        $this->set('nota', $this->data['NotaFiscal']);
        $this->set('situacoesAtesto', $this->situacoesAtesto);

        $this->setDisplayFieldEmpenho($id);
        $this->set('empenhos', $this->NotaFiscal->Empenho->find('list', array(
            'conditions' => array(
                'Empenho.co_contrato' => $coContrato,
                'Empenho.tp_empenho' => 'O',
                'Empenho.vl_empenho -
                      ifnull(
                        (SELECT sum( n.vl_nota ) vl_restante 
                            FROM notas_empenhos ne 
                                left join notas_fiscais n on ne.co_nota = n.co_nota 
                         WHERE Empenho.co_empenho = ne.co_empenho)
                      , 0)'
            ),
            'order' => 'Empenho.dt_empenho DESC'
        )));
        $empenhosNota = array();
        if (isset($this->data['Empenho'])) {
            foreach ($this->data['Empenho'] as $notasEmpenhos) {
                $empenhosNota[] = $notasEmpenhos['co_empenho'];
            }
        } else {
            $empenhosNota = $this->data['NotaFiscal']['co_empenhos'];
        }
        $this->set(compact('empenhosNota'));

        $this->set('categorias', $this->NotaCategoria->find('list'));
    }

    /**
     * @resource { "name": "Remover Notal Fiscal", "route":"notas_fiscais\/delete", "access": "private", "type": "delete" }
     */
    public function delete($id = null, $coContrato)
    {
        $this->loadModel('NotaFiscal');
        $this->loadModel('NotasEmpenho');

        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        if ($this->NotaFiscal->delete($id, true)) {
            $this->NotaFiscal->Anexo->deleteAll(array(
                'Anexo.co_nota' => $id
            ));

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído, pois existe vínculo com outro(s).', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
    }

    /**
     * @resource { "name": "Verifica Data Empenho", "route":"notas_fiscais\/checkEmpenhoDate", "access": "private", "type": "select" }
     */
    public function checkEmpenhoDate()
    {
        $msg = '';
        if (!empty($this->params["form"]["dtNota"]) && !empty($this->params["form"]["empenhos"])) {
            foreach ($this->params["form"]["empenhos"] as $empenho) {
                $emps = $this->NotaFiscal->Empenho->find('first', array(
                    'conditions' => array(
                        'Empenho.co_empenho' => $empenho
                    ),
                    'fields' => array(
                        'Empenho.co_empenho', 'Empenho.dt_empenho'
                    )
                ));
                $dtEmpenho = new DateTime(dtDb($emps["Empenho"]["dt_empenho"]));
                $dtNota = new DateTime(dtDb($this->params["form"]["dtNota"]));
                $interval = $dtNota->diff($dtEmpenho);
                if ($interval->invert > 0 || ($interval->d == 0 && $interval->m == 0 && $interval->y == 0) && $msg != 'false')
                    $msg = 'true';
                else
                    $msg = 'false';
            }
        } else
            $msg = 'false';

        echo json_encode(array(
            'msg' => $msg
        ));
        exit();
    }

    /**
     * @resource { "name": "Assinar", "route":"notas_fiscais\/assinar", "access": "private", "type": "insert" }
     */
    public function assinar($id)
    {
        $this->data = $this->NotaFiscal->read(null, $id);
        $this->set('nota', $this->data['NotaFiscal']);
        // criando arquivo xml para assinatura
        $doc = new DOMDocument('1.0');
        // we want a nice output
        $doc->formatOutput = true;

        $root = $doc->createElement('notafiscal');
        $root = $doc->appendChild($root);

        $nuNotaIte = $doc->createElement('nu_nota');
        $nuNotaVal = $doc->createTextNode($this->data['NotaFiscal']['nu_nota']);
        $nuNotaVal = $nuNotaIte->appendChild($nuNotaVal);

        $dtRecIte = $doc->createElement('dt_recebimento');
        $dtRecVal = $doc->createTextNode($this->data['NotaFiscal']['dt_recebimento']);
        $dtRecVal = $dtRecIte->appendChild($dtRecVal);

        $vlNotaIte = $doc->createElement('vl_nota');
        $vlNotaVal = $doc->createTextNode($this->data['NotaFiscal']['vl_nota']);
        $vlNotaVal = $vlNotaIte->appendChild($vlNotaVal);

        $root->appendChild($nuNotaIte);
        $root->appendChild($dtRecIte);
        $root->appendChild($vlNotaIte);

        $xmlData = $doc->saveXML();

        $coContrato = str_pad($this->data['NotaFiscal']['co_contrato'], 2, "0", STR_PAD_LEFT);
        $coNota = str_pad($this->data['NotaFiscal']['co_nota'], 2, "0", STR_PAD_LEFT);
        $nuNotas = str_pad($this->data['NotaFiscal']['nu_nota'], 5, "0", STR_PAD_LEFT);

        $folderName = realpath(dirname(__FILE__)) . "/../xml/";
        $fileName = "nota" . $coContrato . $coNota . $nuNotas . ".xml";

        $file = $folderName . $fileName;

        file_put_contents($file, $xmlData);

        $this->set('filename', $file);
    }
}

?>
