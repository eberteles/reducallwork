<?php
class OperadorasController extends AppController
{

    public $name = 'Operadoras';

    public $uses = array(
        'Operadora'
    );

    public function index()
    {
        $this->Operadora->recursive = 0;
        $this->set('operadoras', $this->paginate());
    }
    
    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $id )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'id' ) );
    }

    function listar() {

        echo json_encode ( $this->Operadora->find ( 'list' ) );

        exit ();
    }

    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        
        if (! empty($this->data)) {
            $this->Operadora->create();
            if ($this->Operadora->save($this->data)) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->Operadora->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->data['Operadora']['nome'] = ($this->data['Operadora']['nome']);
            if ($this->Operadora->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Operadora->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Operadora->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>
