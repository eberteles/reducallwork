<?php
class OrdemBancariaController extends AppController
{

    public $name = 'OrdemBancaria';

    public $layout = 'iframe';

    public $arTipos = array(
        'O' => 'Original',
        'R' => 'Reforço',
        'A' => 'Anulação'
    );

    public function index($coContrato)
    {
        $this->OrdemBancaria->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array()
            // 'OrdemBancaria.co_contrato' => $coContrato
            
        );
        
        $this->set('ordens', $this->paginate());
        $this->set('coContrato', $coContrato);
        $this->set('tipos', $this->arTipos);
        
        $this->set(compact('coContrato'));
        
        // $this->render('index', 'blank');
    }

    public function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    public function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->OrdemBancaria->create();
            if ($this->OrdemBancaria->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set('tipos', $this->arTipos);
    }

    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->OrdemBancaria->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->OrdemBancaria->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('empenho', $this->data['OrdemBancaria']);
        $this->set('tipos', $this->arTipos);
    }

    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->OrdemBancaria->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>