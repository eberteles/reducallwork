<?php

class OrdemFornecimentoServicoController extends AppController
{

    public $name = 'OrdemFornecimentoServico';

    public $layout = 'iframe';

    public function index($coContrato)
    {
        $this->OrdemFornecimentoServico->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array('co_contrato' => $coContrato)
        );
        
        $this->set('ordens_fornecimento', $this->paginate());
        $this->set('coContrato', $coContrato);
        
        $this->set(compact('coContrato'));
    }

    public function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    public function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->prepararCampos();
            $this->OrdemFornecimentoServico->create();
            if ($this->OrdemFornecimentoServico->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
    }

    private function prepararCampos()
    {
        $this->Functions->limparMascara($this->data['OrdemFornecimentoServico']['nu_ordem']);
    }

    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            $this->prepararCampos();
            if ($this->OrdemFornecimentoServico->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->OrdemFornecimentoServico->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->OrdemFornecimentoServico->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>