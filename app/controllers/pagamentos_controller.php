<?php
/**
* @resource { "name": "Pagamentos", "route":"pagamentos", "access": "private", "type": "module" }
*/
class PagamentosController extends AppController
{

    public $name = 'Pagamentos';

    public $layout = 'iframe';

    /**
    * @resource { "name": "Pagamentos", "route":"pagamentos\/index", "access": "private", "type": "select" }
    */
    public function index($coContrato, $coAtividade = 0, $filtroAtividade = 0)
    {
        App::import('Model', 'Pendencia');
        $pendenciaDb = new Pendencia();
        
        $pendencia = $pendenciaDb->field('ds_impacto_pagamento', 'co_contrato = "' . $coContrato . '" and st_pendencia = "P"');
        
        if($pendencia == 1){
            $msg = 'EXISTE PENDÊNCIAS FINANCEIRAS REFERENTE A ESTE CONTRATO';
            $this->set('pendencias', $msg);
        }
        
        $this->Pagamento->recursive = 2;
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'Pagamento.nu_ano_pagamento' => 'asc',
                'Pagamento.nu_mes_pagamento' => 'asc'
            )
        );
        $conditions = null;
        $conditions['Pagamento.co_contrato'] = $coContrato;
        if ($coAtividade) {
            $conditions['Pagamento.co_atividade'] = $coAtividade;
        }
        if ($filtroAtividade > 0) {
            $conditions['Pagamento.co_atividade'] = $filtroAtividade;
        }

        $pagamentos = $this->paginate($conditions);

        foreach($pagamentos as $k=>$pagamento){
            $pagamento['Pagamento']['empenhos_utilizados'] = '';

            if($pagamento['Pagamento']['co_empenho'] != null){
                $empenho = $this->Pagamento->Empenho->find(array('co_empenho' => $pagamento['Pagamento']['co_empenho']));
                $pagamento['Pagamento']['empenhos_utilizados'] .= $empenho['Empenho']['nu_empenho']. ' ';

                $pagamentos[$k] = $pagamento;
            }else{
                // TODO: melhorar esta query party
                $empenhosUtilizados = $this->Pagamento->query("select * from empenhos where co_empenho in (select co_empenho from notas_empenhos where co_nota = (select co_nota from notas_fiscais where nu_nota = (select nu_nota_fiscal from pagamentos where co_pagamento = '".$pagamento['Pagamento']['co_pagamento']."')))");
                foreach($empenhosUtilizados as $empenho){
                    $pagamento['Pagamento']['empenhos_utilizados'] .= $empenho['empenhos']['nu_empenho']. ' '; // o espaço é pra seprar as strings e não ficar visível no último empenho concatenado
                }
                $pagamentos[$k] = $pagamento;
            }

        }
        $this->set('pagamentos',$pagamentos);
        $this->set(compact('coContrato'));
        $this->set(compact('coAtividade'));
        $this->set(compact('filtroAtividade'));

        $modulo = new ModuloHelper();
        if ($modulo->isExecucao() && $coAtividade == 0) {
            $this->set('listAtividades', $this->Pagamento->Atividade->find('threaded', array(
                'conditions' => array(
                    'Atividade.co_contrato' => $coContrato
                )
            )));
        }
        
        App::import('Model', 'Contrato');
        $mdContrato = new Contrato();
        $contrato   = $mdContrato->find('first', array(
            'recursive' => -1,
            'fields' => array('ic_tipo_contrato'),
            'conditions' => array('Contrato.co_contrato'=>$coContrato)
        ));
        $this->set('ic_tipo_contrato', $contrato['Contrato']['ic_tipo_contrato']);

        $this->set('tipoPgto', $this->Pagamento->tipoPgto);
    }

    /**
    * @resource { "name": "iframe", "route":"pagamentos\/iframe", "access": "private", "type": "select" }
    */
    public function iframe($coContrato, $coAtividade = 0)
    {
        $this->set(compact('coContrato'));
        $this->set(compact('coAtividade'));
    }

    public function setDisplayFieldNota()
    {
        $this->Pagamento->NotaFiscal->displayField = 'nome_combo';
        $this->Pagamento->NotaFiscal->virtualFields = array(
            'nome_combo' => "CONCAT( CONCAT(NotaFiscal.nu_nota, ' - ', DATE_FORMAT(NotaFiscal.dt_recebimento, '%d/%m/%Y')), ' - ', 
                    Concat('R$ ', Replace (Replace (Replace (Format( NotaFiscal.vl_nota , 2), '.', '|'), ',', '.'), '|', ',')) )"
        );
    }

    /**
    * @resource { "name": "Vincular Notas", "route":"pagamentos\/vinculaNotas", "access": "private", "type": "insert" }
    */
    public function vinculaNotas($notas, $coPagamento)
    {
        if (is_array($notas)) {
            foreach ($notas as $co_nota) :
                $this->Pagamento->NotasPagamento->create();
                $notaPagamento = array();
                $notaPagamento['NotasPagamento']['co_pagamento'] = $coPagamento;
                $notaPagamento['NotasPagamento']['co_nota'] = $co_nota;
                $this->Pagamento->NotasPagamento->save($notaPagamento);
            endforeach
            ;
        }
    }

    /**
    * @resource { "name": "Novo Pagamento", "route":"pagamentos\/add", "access": "private", "type": "insert" }
    */
    public function add($coContrato, $coAtividade = 0)
    {
        if (! empty($this->data)) {
            $this->Pagamento->create();
            if ($this->Pagamento->save($this->data)) {

                if(!empty($this->data['Pagamento']['co_notas'])) {
                    $this->vinculaNotas($this->data['Pagamento']['co_notas'], $this->Pagamento->getInsertID());
                }

                if ($this->data['Pagamento']['repetir_pagamento'] > 0) {
                    $this->repetirPagamento($this->data['Pagamento']['repetir_pagamento']);
                }
                $this->Session->setFlash(__('Registro salvo com sucesso', true));

                App::import('Model', 'Contrato');
                $modelContrato = new Contrato();
                $modelContrato->recursive = -1;

                $contrato = $modelContrato->find(array('co_contrato' => $coContrato));

                $vlGlobal = $contrato['Contrato']['vl_global'];
                $totalPagamentos = $this->Pagamento->getTotal($coContrato);

                if($vlGlobal <= $totalPagamentos){
                    $this->avisarEnvolvidorPagamento100Porcento();
                }

                $this->avisarFinanceiro();
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $coAtividade
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        // $this->set( 'empenhos', $this->Pagamento->Empenho->find( 'list', array('conditions' => array('co_contrato' => $coContrato )) ) );
        
        $this->setDisplayFieldNota();
        $this->Pagamento->NotaFiscal->recursive = -1;
        $notas = $this->Pagamento->NotaFiscal->find('all', array('conditions' => array('NotaFiscal.co_contrato' => $coContrato)));

        $notasValorCorrigido = $this->Pagamento->findNotasComPagamentos($coContrato);

        $toView = array();
        $glosa = array();

        foreach($notas as $k=>$nota){
            foreach($notasValorCorrigido as $corrigido){
                if($corrigido['p']['nu_nota'] == $nota['NotaFiscal']['nu_nota']){
                    $new = (float)$notas[$k]['NotaFiscal']['vl_nota'] - (float)$corrigido['p']['novo_valor'];
                    $notas[$k]['NotaFiscal']['vl_nota'] = $new;
                }
            }

            if(round($notas[$k]['NotaFiscal']['vl_nota']) > 0){
                array_push($toView, $nota['NotaFiscal']['nu_nota'] . ' ; ' . $nota['NotaFiscal']['dt_nota'] . ' ; ' . nl($notas[$k]['NotaFiscal']['vl_nota']));
            $glosa[$k]['nu_nota'] = $nota['NotaFiscal']['nu_nota'];
            $glosa[$k]['vl_glosa'] = $nota['NotaFiscal']['vl_glosa'];
            }
        }

        array_unshift($toView, 'Selecione...');

        $this->set('notas', $toView);
        $this->set('glosas', $glosa);

        $this->set('isAtividade', $coAtividade);
        $this->set('atividades', $this->Pagamento->Atividade->find('threaded', array(
                'co_contrato' => $coContrato
        )));
        $this->set(compact('coContrato'));
        $this->set(compact('coAtividade'));
        $this->set('temFornecedor', $this->Pagamento->contratoComFornecedor($coContrato));
        
        App::import('Model', 'Contrato');
        $mdContrato = new Contrato();
        $contrato   = $mdContrato->find('first', array(
            'recursive' => -1,
            'fields' => array('ic_tipo_contrato'),
            'conditions' => array('co_contrato'=>$coContrato)
        ));
        $this->set('ic_tipo_contrato', $contrato['Contrato']['ic_tipo_contrato']);
        
    }

    /**
    * @resource { "name": "Editar Pagamentos", "route":"pagamentos\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null, $coContrato, $coAtividade = 0)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Pagamento->save($this->data)) {
                
                $this->Pagamento->NotasPagamento->deleteAll(array(
                    'co_pagamento' => $id
                ));
                $this->vinculaNotas($this->data['Pagamento']['co_notas'], $id);
                
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $coAtividade
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Pagamento->read(null, $id);
        }
        // $empenhos = $this->Pagamento->Empenho->find( 'list' );
        // $this->set( compact( 'empenhos' ) );
        $this->set('atividades', $this->Pagamento->Atividade->find('threaded', array(
                'co_contrato' => $coContrato
        )));
        $this->set(compact('coContrato'));
        $this->set(compact('coAtividade'));
        $this->set(compact('id'));
        $this->set('pagamento', $this->data['Pagamento']);
        
        $this->setDisplayFieldNota();
        $this->set('notas', $this->Pagamento->NotaFiscal->find('list', array(
            'conditions' => array(
                'co_contrato' => $coContrato
            )
        )));
        
        $notasPagamento = array();
        if (isset($this->data['NotaFiscal'])) {
            foreach ($this->data['NotaFiscal'] as $notasPagamentos) {
                $notasPagamento[] = $notasPagamentos['NotasPagamento']['co_nota'];
            }
        } else {
            $notasPagamento = $this->data['Pagamento']['co_notas'];
        }
        $this->set(compact('notasPagamento'));
        
        App::import('Model', 'Contrato');
        $mdContrato = new Contrato();
        $contrato   = $mdContrato->find('first', array(
            'recursive' => -1,
            'fields' => array('ic_tipo_contrato'),
            'conditions' => array('co_contrato'=>$coContrato)
        ));
        $this->set('ic_tipo_contrato', $contrato['Contrato']['ic_tipo_contrato']);
    }

    /**
    * @resource { "name": "Remover Pagamento", "route":"pagamentos\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null, $coContrato, $coAtividade = 0)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $coAtividade
            ));
        }
        $this->Pagamento->NotasPagamento->deleteAll(array(
            'co_pagamento' => $id
        ));
        if ($this->Pagamento->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $coAtividade
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato,
            $coAtividade
        ));
    }

    /**
    * @resource { "name": "Repetir Pagamento", "route":"pagamentos\/repetirPagamento", "access": "private", "type": "insert" }
    */
    public function repetirPagamento($vezes)
    {
        $pagamento = $this->data;
        $pagamentos = array();
        for ($i = 0; $i < $vezes; $i ++) {
            if ($pagamento['Pagamento']['nu_mes_pagamento'] == 12) {
                $pagamento['Pagamento']['nu_mes_pagamento'] = 1;
                $pagamento['Pagamento']['nu_ano_pagamento'] ++;
            } else {
                $pagamento['Pagamento']['nu_mes_pagamento'] ++;
            }
            $pagamentos['Pagamento'][$i] = array(
                'co_contrato' => $pagamento['Pagamento']['co_contrato'],
                'nu_mes_pagamento' => doisDigitos($pagamento['Pagamento']['nu_mes_pagamento']),
                'nu_ano_pagamento' => $pagamento['Pagamento']['nu_ano_pagamento'],
                'nu_ano_pagamento'=> dtDb($pagamento['Pagamento.nu_ano_pagamento']),
                'vl_imposto' => ln($pagamento['Pagamento']['vl_imposto']),
                'vl_liquido' => ln($pagamento['Pagamento']['vl_liquido']),
                'vl_pagamento' => ln($pagamento['Pagamento']['vl_pagamento']),
                'ds_observacao' => up($pagamento['Pagamento']['ds_observacao']),
                'co_usuario' => $pagamento['Pagamento']['co_usuario']
            );
            if ($pagamento['Pagamento']['co_atividade'] > 0) {
                $pagamentos['Pagamento'][$i]['co_atividade'] = $pagamento['Pagamento']['co_atividade'];
            }
        }
        
        $this->Pagamento->create(false);
        $this->Pagamento->saveAll($pagamentos['Pagamento'], array(
            'validate' => false
        ));
    }

    /**
    * @resource { "name": "Avisar Envolvido Pagamento 100 Porcento", "route":"pagamentos\/avisarEnvolvidorPagamento100Porcento", "access": "private", "type": "select" }
    */
    public function avisarEnvolvidorPagamento100Porcento()
    {
        ob_start();
        App::import('Model', 'Contrato');
        $mdContrato = new Contrato();

        $fiscais = $mdContrato->getFiscaisNomeEmail($this->data['Pagamento']['co_contrato']);
        $gestorAtivo = $mdContrato->getGestorAtivo($this->data['Pagamento']['co_contrato']);

        $contrato = $mdContrato->read(array(
            'Contrato.nu_processo',
            'Contrato.nu_contrato',
            'Contrato.ds_objeto',
            'Contrato.dt_ini_vigencia',
            'Contrato.dt_fim_vigencia',
            'Contrato.vl_global'
        ), $this->data['Pagamento']['co_contrato']);

        $mensagem = array(
            "Todos os pagamentos do contrato ".$contrato['Contrato']['nu_contrato']." foram feitos, totalizando o valor de
             R$ ".ln($contrato['Contrato']['vl_global']).". Acompanhe o andamento deste contrato até que o mesmo seja finalizado ou renovado."
        );

        foreach($fiscais as $fiscal){
            if($fiscal['Usuario']['ds_email'] != ''){
                $this->EmailProvider->subject = "O contrato " . $contrato['Contrato']['nu_contrato'] . "atingiu 100% de pagamentos.";
                $this->EmailProvider->to = $fiscal['Usuario']['ds_email'];
                $this->EmailProvider->send($mensagem);
            }
        }
        foreach($gestorAtivo as $gestor){
            if($gestor['Usuario']['ds_email'] != ''){
                $this->EmailProvider->subject = "O contrato " . $contrato['Contrato']['nu_contrato'] . "atingiu 100% de pagamentos.";
                $this->EmailProvider->to = $gestor['Usuario']['ds_email'];
                $this->EmailProvider->send($mensagem);
            }
        }
    }

    /**
    * @resource { "name": "Avisar ao Financeiro", "route":"pagamentos\/avisarFinanceiro", "access": "private", "type": "select" }
    */
    public function avisarFinanceiro()
    {
        // ob_start();
        App::import('Helper', 'Print');
        App::import('Model', 'Setor');
        App::import('Model', 'Contrato');
        $print = new PrintHelper();
        $mdSetor = new Setor();
        $mdContrato = new Contrato();
        
        $setor = $mdSetor->find('first', array(
            'conditions' => array(
                'Setor.ic_setor' => 'F'
            )
        ));
        $contrato = $mdContrato->read(array(
            'Contrato.nu_processo',
            'Contrato.nu_contrato',
            'Contrato.ds_objeto',
            'Contrato.dt_ini_vigencia',
            'Contrato.dt_fim_vigencia'
        ), $this->data['Pagamento']['co_contrato']);
        
        if (isset($setor['Setor']['ds_email']) && $setor['Setor']['ds_email'] != "") {
            $this->EmailProvider->subject = __('Pagamento', true) . ' enviado para ' . $setor['Setor']['ds_setor'];
            $mensagem = array(
                'Processo: ' . $print->processo($contrato['Contrato']['nu_processo']),
                'Contrato: ' . $print->contrato($contrato['Contrato']['nu_contrato']),
                'Objeto: ' . $contrato['Contrato']['ds_objeto'],
                'Data Início: ' . $contrato['Contrato']['dt_ini_vigencia'],
                'Data Fim: ' . $contrato['Contrato']['dt_fim_vigencia'],
                ' ---------- DADOS DO PAGAMENTO ---------- ',
                'Vencimento: ' . $this->data['Pagamento']['dt_vencimento'],
                'Valor: ' . $this->data['Pagamento']['vl_pagamento'],
                'Observação: ' . $this->data['Pagamento']['ds_observacao']
            );
            
            $this->EmailProvider->to = $setor['Setor']['ds_setor'] . ' <' . $setor['Setor']['ds_email'] . '>';
            $this->EmailProvider->send($mensagem);
        }
    }
}
?>
