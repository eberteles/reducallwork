<?php
/**
 * @resource { "name": "Penalidades", "route":"penalidades", "access": "private", "type": "module" }
 */
class PenalidadesController extends AppController
{

    public $name = 'Penalidades';

    public $uses = array(
        'Penalidade',
        'TipoPenalidade',
    );

    public $layout = 'iframe';

    /**
     * @resource { "name": "Penalidades", "route":"penalidades\/index", "access": "private", "type": "select" }
     */
    public function index($coContrato)
    {
        $this->Penalidade->recursive = 0;

        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Penalidade.co_contrato' => $coContrato
            ),
            'order' => array(
                'Penalidade.dt_penalidade' => 'asc'
            )
        );

        $this->set('penalidades', $this->paginate());

        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name": "iFrame", "route":"penalidades\/iframe", "access": "private", "type": "select" }
     */
    public function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    /**
     * @resource { "name": "Nova Penalidade", "route":"penalidades\/add", "access": "private", "type": "insert" }
     */
    public function add($coContrato)
    {
        if (! empty($this->data)) {
            $usuario = $this->Session->read('usuario');
            $this->Penalidade->create();
            $this->data['Penalidade']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            if ($this->Penalidade->save($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set('situacoes', $this->TipoPenalidade->find('list'));
        $this->set('notasFiscais', $this->Penalidade->NotaFiscal->find('list', array(
            'conditions' => array(
                'co_contrato' => $coContrato
            )
        )));
    }

    /**
     * @resource { "name": "Editar Penalidade", "route":"penalidades\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            $usuario = $this->Session->read('usuario');
            $this->data['Penalidade']['co_usuario'] = $usuario['Usuario']['co_usuario'];

            if ($this->Penalidade->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Penalidade->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('penalidade', $this->data['Penalidade']);
        $this->set('situacoes', $this->TipoPenalidade->find('list'));
        $this->set('notasFiscais', $this->Penalidade->NotaFiscal->find('list', array(
            'conditions' => array(
                'co_contrato' => $coContrato
            )
        )));
    }

    /**
     * @resource { "name": "Remover Penalidade", "route":"penalidades\/delete", "access": "private", "type": "delete" }
     */
    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Penalidade->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }

    /**
     * @resource { "name": "Listar Histórico", "route":"penalidades\/list_historico", "access": "private", "type": "select" }
     */
    public function list_historico($coPenalidade)
    {
        App::import('Model', 'Log');
        $logPenalidade = new Log();

        $coPenalidade = trim(json_encode(array('$coPenalidade' => $coPenalidade)), "{}");

        $penalidades = $logPenalidade->find('all', array(
            'conditions' => array(
                'MATCH(Log.co_penalidade)' => "AGAINST('" . $coPenalidade . "')"
            ),
            'order' => array(
                'Log.dt_log' => 'desc'
            )
        ));

        $this->set(compact('penalidades'));

        $this->set('situacoes', $this->TipoPenalidade->find('list'));
    }
}
