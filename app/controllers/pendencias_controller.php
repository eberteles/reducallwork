<?php
/**
* @resource { "name": "Pendências", "route":"pendencias", "access": "private", "type": "module" }
*/
class PendenciasController extends AppController
{

    public $name = 'Pendencias';

    public $layout = 'iframe';

    public $arSituacoes = array(
        'P' => 'Pendente',
        'C' => 'Concluída'
    );

    /**
    * @resource { "name": "Pendências", "route":"pendencias\/index", "access": "private", "type": "select" }
    */
    public function index($coContrato, $modulo = "contrato")
    {
        $this->Pendencia->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'order' => array(
                'Pendencia.dt_inicio' => 'asc'
            )
        );
        $conditions = null;
        if ($modulo == "ata") {
            $conditions['Pendencia.co_ata'] = $coContrato;
        } else {
            $conditions['Pendencia.co_contrato'] = $coContrato;
        }
        
        $this->set('pendencias', $this->paginate($conditions));
        
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set('situacoes', $this->arSituacoes);
    }

    /**
    * @resource { "name": "iframe", "route":"pendencias\/iframe", "access": "private", "type": "select" }
    */
    public function iframe($coContrato, $modulo = "contrato")
    {
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
    }

    /**
    * @resource { "name": "Nova Pendência", "route":"pendencias\/add", "access": "private", "type": "insert" }
    */
    public function add($coContrato, $modulo = "contrato")
    {
        if (! empty($this->data)) {;
            if ($modulo == "contrato") { // Pegar a situação do contrato
                App::import('Model', 'Contrato');
                $contrato = new Contrato();
                $this->data['Pendencia']['co_situacao'] = $contrato->field('co_situacao', "Contrato.co_contrato = " . $this->data['Pendencia']['co_contrato']);
            }
            
            $this->Pendencia->create();
            if ($this->Pendencia->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->Pendencia->updNuPendencias($coContrato);
                $this->addAndamento('Pendência cadastrada.', array('model'=>'Pendencia'));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set('situacoes', $this->arSituacoes);
    }

    /**
    * @resource { "name": "Editar Pendência", "route":"pendencias\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null, $coContrato, $modulo = "contrato")
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Pendencia->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->Pendencia->updNuPendencias($coContrato);
                $this->addAndamento('Pendência atualizada.', array('model'=>'Pendencia'));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato,
                    $modulo
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Pendencia->read(null, $id);
        }
        $this->set('situacoes', $this->arSituacoes);
        $this->set(compact('coContrato'));
        $this->set(compact('modulo'));
        $this->set(compact('id'));
    }

    /**
    * @resource { "name": "Remover Pendência", "route":"pendencias\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null, $coContrato, $modulo = "contrato")
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Pendencia->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->Pendencia->updNuPendencias($coContrato);
            $this->addAndamento('Pendência excluída.', array('model'=>'Pendencia'));
            $this->redirect(array(
                'action' => 'index',
                $coContrato,
                $modulo
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato,
            $modulo
        ));
    }
}
?>