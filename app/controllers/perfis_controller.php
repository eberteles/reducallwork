<?php
/**
 * Classe CONTROLLER de Perfis do GESCON.
 *
 * @category   Controller
 * @package    App/Controllers
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @license    <licença-n2oti-link>
 * @version    Release: @package_version@
 * @since      Class available since Release 3.0.0
 * @resource { "name" : "Cadastro de Perfis", "route":"perfis", "access": "private", "type": "module" }
 */
class PerfisController extends AppController
{
	var $name = "Perfis";

	/**
	 * @resource { "name" : "Perfis", "route":"perfis\/index", "access": "private", "type": "select" }
	 */
	public function index()
	{
        $this->Perfil->recursive = 0;
        $criteria = array();
        $this->set('perfis', $this->paginate($criteria));
	}

	/**
	 * @resource { "name" : "Adicionar", "route":"perfis\/add", "access": "private", "type": "insert" }
	 */
	public function add( $modal = false )
	{
		if (!empty($this->data)) {
			$perfil = $this->Perfil->findByNoPerfil($this->data['Perfil']['no_perfil']);
			if( empty($perfil) ) {
				$this->Perfil->create();
				if ($this->Perfil->save($this->data)) {
					if($modal) {
						$this->redirect ( array ('action' => 'close', $this->Perfil->id ) );
					} else {
						$this->Session->setFlash(__('Registro salvo com sucesso', true));
						$this->redirect(array('action' => 'index'));
					}
				} else {
					$this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
				}
			} else {
				$this->activateSituation( $perfil );
			}
		}
	}

	/**
	 * @resource { "name" : "Editar", "route":"perfis\/edit", "access": "private", "type": "update" }
	 */
	public function edit($id = null)
	{
		if ( !$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ( !empty($this->data)) {
            if ($this->Perfil->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Perfil->read(null, $id);
        }
        $this->set('id', $id);
	}

	/**
	 * @resource { "name" : "Remover", "route":"perfis\/delete", "access": "private", "type": "delete" }
	 */
	public function delete($id = null)
	{
		if ( !$id ) {
			$this->Session->setFlash(__('Identificador inválido', true));
			$this->redirect(array(
				'action' => 'index'
			));
		}

		if ( $id ) {
			$this->Perfil->id = $id;
			$msg = null;
			$situacao['Perfil']['ic_ativo'] = 0;

			if($this->Perfil->saveField('ic_ativo', 0)){
				$msg = "Perfil excluído com sucesso!";
			}else{
				$msg = "Houve um erro na exclusão";
			}

			$this->Session->setFlash(__($msg, true));
			$this->redirect(array(
				'action' => 'index'
			));
		} else {
			$this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
			$this->redirect(array(
				'action' => 'index'
			));
		}
	}

	/**
	 * @resource { "name" : "Ativar", "route":"perfis\/activateSituation", "access": "private", "type": "update" }
	 */
	public function activateSituation( $perfil )
	{
		$perfil['Perfil']['ic_ativo'] = TRUE;

        if($this->Perfil->save($perfil)){
            $msg = "Perfil ativado sucesso!";
        }else{
            $msg = "Houve um erro na ativação";
        }

        $this->Session->setFlash(__($msg, true));
        $this->redirect(array(
            'action' => 'index'
        ));
	}

	/**
	 * @resource { "name" : "Bloquear ou Desbloquear", "route":"perfis\/bloquearOrDesbloquear", "access": "private", "type": "update" }
	 */
	public function bloquearOrDesbloquear($id = null)
	{
		if (! $id) {
			$this->Session->setFlash(__('Identificador inválido', true));
			$this->redirect(array(
					'action' => 'index'
			));
		}
		if ($id > 0) {
			$perfil = $this->Perfil->read(null, $id);
			$msg = null;

			switch ($perfil['Perfil']['ic_ativo']) {
				// Desbloqueia o Perfil
				case 1:
					$perfil['Perfil']['ic_ativo'] = 0;

					$msg = "Perfil bloqueado com sucesso";
					break;
					// Bloqueia o Perfil
				case 0:
					$perfil['Perfil']['ic_ativo'] = 1;
					$msg = "Perfil desbloqueado com sucesso";
					break;
			}

			$this->Perfil->save($perfil);

			$this->Session->setFlash(__($msg, true));
			$this->redirect(array(
				'action' => 'index'
			));

			$this->Session->setFlash(__('Erro ao excluir registro', true));
			$this->redirect(array(
				'action' => 'index'
			));
		}
	}
}