<?php

/**
 * Classe CONTROLLER de Permissões do GESCON.
 *
 * @category   Controllers
 * @package    App/Controllers
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @version    Release: @package_version@
 * @since      Class available since Release 3.0.0
 * @resource { "name" : "Permissões", "route":"permissoes", "access": "private", "type": "module" }
 */
class PermissoesController extends AppController
{
    /**
     * @var string
     */
    var $name = "Permissoes";

    var $helpers = array('Ajax');

    /**
     * @resource { "name" : "Listar permissões por perfil", "route":"permissoes\/perfil", "access": "private", "type": "select" }
     * @return void
     */
    public function perfil($coPerfil)
    {
        App::import('Model', 'Perfil');

        /* se a requisição for post armazena em params['form']
         * pois é uma requisão de add ou delete então limpa o cache
         * para gerar novo cache com novas permissões
         */
        if (isset($this->params['form']['co_perfil'])){
            $this->clearPerfilCache($this->params['form']['co_perfil']);
        }

        $mPerfil = new Perfil();
        $mPerfil->recursive = 0;
        $perfil = $mPerfil->find(array(
            'co_perfil' => $coPerfil
        ));
        $options = array(
            'conditions' => array(
                'Recurso.parent_id' => NULL,
            ),
            'recursive' => 1,
        );
        if (!empty($this->data)) {
            $options['conditions']['no_recurso like'] = '%' . up($this->data['Recurso']['no_recurso']) . '%';
        }

        $this->paginate = $options;

        $recursos = array();
        $recursos_full = $this->paginate('Recurso');


        foreach ($recursos_full as $key => $recurso) {
            $permissoes = array();
            foreach ($recurso['Permissao'] as $key => $permissao) {
                if ($permissao['co_perfil'] == $coPerfil) {
                    $permissoes[] = $permissao;
                }
                unset($recurso['Permissao'][$key]);
            }
            $recurso['Permissao'] = $permissoes;
            $recursos[] = $recurso;
        }

        $this->set('perfil', $perfil);
        $this->set('recursos', $recursos);

    }

    /**
     * @resource { "name" : "Adicionar", "route":"permissoes\/add", "access": "private", "type": "insert" }
     * @return void
     */
    public function add()
    {

        $response = array(
            'status' => '500'
        );
        if (!empty($this->params) && $this->params['isAjax'] == 'true') {
            $data = $this->params['form'];
            $this->addRecursive($data);
            $response = array(
                'status' => '200'
            );
        }
        $this->clearPerfilCache($data['co_perfil']);
        echo json_encode($response);
        exit;
    }

    public function addRecursive($data)
    {
        App::import('Model', 'Recurso');
        $modelRecurso = new Recurso();

        $criteria = array();
        $recursos = $modelRecurso->find('all', array(
            'conditions' => array(
                'Recurso.parent_id' => (int)$data['co_recurso'],
                'Recurso.ic_publico' => false
            )
        ));

        $this->Permissao->recursive = -1;
        $permissao = $this->Permissao->find('first', array('conditions' => array(
            'Permissao.co_recurso' => (int)$data['co_recurso'],
            'Permissao.co_perfil' => (int)$data['co_perfil'],
        )));

        if (!$permissao) {
            $this->Permissao->create();
            $this->Permissao->save($data);
        }

        if ($recursos) {
            foreach ($recursos as $recurso) {
                if ($recurso['Recurso']['ic_childs'] == 1) {
                    $dataRecursive['co_recurso'] = $recurso['Recurso']['co_recurso'];
                    $dataRecursive['co_perfil'] = $data['co_perfil'];
                    $this->addRecursive($dataRecursive);
                } else {
                    $this->Permissao->recursive = -1;
                    $permissao = $this->Permissao->find('first', array('conditions' => array(
                        'Permissao.co_recurso' => (int)$recurso['Recurso']['co_recurso'],
                        'Permissao.co_perfil' => (int)$data['co_perfil'],
                    )));

                    if (!$permissao) {
                        $this->Permissao->create();
                        $this->Permissao->save(array(
                            'co_perfil' => $data['co_perfil'],
                            'co_recurso' => $recurso['Recurso']['co_recurso'],
                        ));
                    }
                }
            }
        }
    }

    /**
     * @resource { "name" : "Remover", "route":"permissoes\/delete", "access": "private", "type": "delete" }
     * @return void
     */
    public function delete()
    {
        $response = array(
            'status' => '500'
        );
        if (!empty($this->params) && $this->params['isAjax'] == 'true') {

            $data = $this->params['form'];
            $this->deleteRecursive($data);
        }
        $this->clearPerfilCache($data['co_perfil']);
        echo json_encode($response);
        exit;
    }

    public function deleteRecursive($data)
    {
        App::import('Model', 'Recurso');
        $modelRecurso = new Recurso();

        $criteria = array();
        $recursos = $modelRecurso->find('all', array(
            'conditions' => array(
                'Recurso.parent_id' => $data['co_recurso'],
                'Recurso.ic_publico' => false
            )
        ));

        $this->Permissao->deleteAll(array(
            'Permissao.co_recurso' => $data['co_recurso'],
            'Permissao.co_perfil' => $data['co_perfil'],
        ));

        if ($recursos) {
            foreach ($recursos as $recurso) {
                if ($recurso['Recurso']['ic_childs'] == 1) {
                    $dataRecursive['co_recurso'] = $recurso['Recurso']['co_recurso'];
                    $dataRecursive['co_perfil'] = $data['co_perfil'];
                    $this->deleteRecursive($dataRecursive);
                } else {
                    $this->Permissao->deleteAll(array(
                        'Permissao.co_recurso' => $recurso['Recurso']['co_recurso'],
                        'Permissao.co_perfil' => $data['co_perfil'],
                    ));
                }
            }
        }
    }

    private function clearPerfilCache($coPerfil)
    {
        $filehash = "rs" . md5($coPerfil);
        if(Cache::read($filehash)){
            Cache::delete($filehash);
        }
    }
}
