<?php
class PreEmpenhosController extends AppController
{

    public $name = 'PreEmpenhos';

    public $layout = 'iframe';

    function index($coContrato)
    {
        $this->PreEmpenho->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'PreEmpenho.ic_ativo' => 1,
                'PreEmpenho.co_contrato' => $coContrato
            )
        );
        
        $this->set('empenhos', $this->paginate());
        $this->set('coContrato', $coContrato);
        
        $this->set(compact('coContrato'));
        
        // $this->render('index', 'blank');
    }

    function iframe($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->data["PreEmpenho"]["vl_restante"] = $this->data["PreEmpenho"]["vl_pre_empenho"];
            $this->PreEmpenho->create();
            if ($this->PreEmpenho->save($this->data['PreEmpenho'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set(compact('coContrato'));
    }

    public function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->PreEmpenho->save($this->data['PreEmpenho'])) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->PreEmpenho->read(null, $id);
        }
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
        $this->set('empenho', $this->data['PreEmpenho']);
    }

    public function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->PreEmpenho->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }

    public function logicDelete( $id = null , $coContrato ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }

        if ($this->PreEmpenho->read(null, $id)) {
            $preEmpenho = $this->PreEmpenho->read(null, $id);
            $msg = null;
            $preEmpenho['PreEmpenho']['ic_ativo'] = 0;

            if($this->PreEmpenho->save($preEmpenho)){
                $msg = "Registro excluído com sucesso!";
            }else{
                $msg = "Houve um erro na exclusão";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
    }

    public function comparaValoresEmpenho(){
        $vlEmpenho  = ln($this->params["form"]["vlEmpenho"]);
        $preEmpenho = $this->params["form"]["preEmpenho"];

        $vlPreEmpenho = $this->PreEmpenho->findByCoPreEmpenho($preEmpenho);
        $restante = $vlPreEmpenho["PreEmpenho"]["vl_restante"] - $vlEmpenho;
        if($restante < 0.00){
            $retorno = array(
                'msg' => 'false',
                'restante' => $restante
            );
            echo json_encode($retorno);
        }else{
            $retorno = array(
                'msg' => 'true'
            );
            echo json_encode($retorno);
        }
        exit;
    }

    public function comparaValoresEmpenho2(){
        $vlEmpenhoAtual  = ln($this->params["form"]["vlEmpenhoAtual"]);
        $vlEmpenho  = ln($this->params["form"]["vlEmpenho"]);
        $vlReal = $vlEmpenho - $vlEmpenhoAtual;
        $preEmpenho = $this->params["form"]["preEmpenho"];

        $vlPreEmpenho = $this->PreEmpenho->findByCoPreEmpenho($preEmpenho);
        $restante = $vlPreEmpenho["PreEmpenho"]["vl_restante"] - $vlReal;
        if($restante < 0.00){
            $retorno = array(
                'msg' => 'false',
                'restante' => $restante
            );
            echo json_encode($retorno);
        }else{
            $retorno = array(
                'msg' => 'true'
            );
            echo json_encode($retorno);
        }
        exit;
    }
}
?>