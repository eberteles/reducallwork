<?php
/**
* @resource { "name": "Produtos", "route":"produtos", "access": "private", "type": "module" }
*/
class ProdutosController extends AppController
{

    var $name = 'Produtos';

    var $layout = 'iframe';

    /**
    * @resource { "name": "Listagem", "route":"produtos\/index", "access": "private", "type": "select" }
    */
    function index($coContrato, $ajax = false)
    {
        $this->Produto->recursive = 0;
        
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array(
                'Produto.co_contrato' => $coContrato
            )
        );
        
        $this->set('produtos', $this->paginate());
        
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "iFrame", "route":"produtos\/iframe", "access": "private", "type": "select" }
    */
    function iframe($coContrato)
    {
        $this->layout = 'ajax';
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Novo Produto", "route":"produtos\/add", "access": "private", "type": "insert" }
    */
    function add($coContrato)
    {
        if (! empty($this->data)) {
            $this->Produto->create();
            if ($this->Produto->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set( 'tipos', $this->Produto->ProdutoTipo->find('list') );
        $this->set(compact('coContrato'));
    }

    /**
    * @resource { "name": "Editar Produto", "route":"produtos\/edit", "access": "private", "type": "update" }
    */
    function edit($id = null, $coContrato)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if (! empty($this->data)) {
            if ($this->Produto->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coContrato
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Produto->read(null, $id);
        }
        $this->set( 'tipos', $this->Produto->ProdutoTipo->find('list') );
        $this->set(compact('coContrato'));
        $this->set(compact('id'));
    }

    /**
    * @resource { "name": "Remover Produto", "route":"produtos\/delete", "access": "private", "type": "delete" }
    */
    function delete($id = null, $coContrato)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        if ($this->Produto->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index',
                $coContrato
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index',
            $coContrato
        ));
    }
}
?>