<?php
/**
* @resource { "name": "Cadastro de Tipos de Produtos / Produtos", "route":"produtosTipos", "access": "private", "type": "module" }
*/
class ProdutosTiposController extends AppController {

    public $name = 'ProdutosTipos';

    public $uses = array(
        'ProdutoTipo'
    );

    /**
    * @resource { "name": "Tipos de Produtos / Produtos", "route":"produtosTipos\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->ProdutoTipo->recursive = 0;
        $criteria = array();
        $this->set('produtosTipos', $this->paginate($criteria));
    }

    /**
    * @resource { "name": "Retorno JSON de Tipos de Produtos / Produtos", "route":"produtosTipos\/listar", "access": "private", "type": "select" }
    */
    function listar() {
        echo json_encode ( $this->ProdutoTipo->find ( 'list' ) );
        exit ();
    }

    /**
    * @resource { "name": "Novo Tipo de Produto", "route":"produtosTipos\/add", "access": "private", "type": "insert" }
    */
    function add($modal = false) {
        if (!empty($this->data)) {
            $produtoTipo = $this->ProdutoTipo->findByDsTipo($this->data['ProdutoTipo']['ds_tipo']);
            if( empty($produtoTipo) ) {
                $this->ProdutoTipo->create();
                if ($this->ProdutoTipo->save($this->data)) {
                    if($modal) {
                        $this->redirect ( array ('action' => 'close', $this->ProdutoTipo->id ) );
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            } else {
                $this->activateSituation( $produtoTipo );
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    /**
    * @resource { "name": "Editar Tipo de Produto", "route":"produtosTipos\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if ( !$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ( !empty($this->data)) {
            if ($this->ProdutoTipo->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->ProdutoTipo->read(null, $id);
        }
        $this->set('id', $id);
    }

    /**
    * @resource { "name": "Remover Tipo de Produto", "route":"produtosTipos\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $produtoTipo = $this->ProdutoTipo->find(array('co_produto_tipo' => $id));
        }

        if ($this->ProdutoTipo->delete($id)) {
            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($produtoTipo['ProdutoTipo']);
            $log['ds_tabela'] = 'produtos_tipos';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
    * @resource { "name": "Listar Tipos de Produtos", "route":"produtosTipos\/listaProdutosTipos", "access": "private", "type": "select" }
    */
    public function listaProdutosTipos()
    {
        $this->layout = 'ajax';

        $this->ProdutoTipo->recursive = 0;

        $conditions = null;
        $conditions['ProdutoTipo.ic_ativo'] = 1;

        $this->set('produtosTipos', $this->paginate($conditions));
    }

    /**
    * @resource { "name": "Exclusão Lógica", "route":"produtosTipos\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete( $id = null ){
        if ( !$id ) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->ProdutoTipo->id = $id;
            $msg = null;
            $situacao['ProdutoTipo']['ic_ativo'] = 0;

            if($this->ProdutoTipo->saveField('ic_ativo', 0)){
                $msg = "Tipo de produto excluído com sucesso!";
            }else{
                $msg = "Houve um erro na exclusão";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Ativar Situação", "route":"produtosTipos\/activateSituation", "access": "private", "type": "insert" }
    */
    public function activateSituation($produtoTipo){
        $produtoTipo['ProdutoTipo']['ic_ativo'] = 1;

        if($this->ProdutoTipo->save($produtoTipo)){
            $msg = "Tipo de produto ativado sucesso!";
        }else{
            $msg = "Houve um erro na ativação";
        }

        $this->Session->setFlash(__($msg, true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}