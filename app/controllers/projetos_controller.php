<?php
/**
 * Class ProjetosController
 * @author João Marcos Bizarro Lopes
 * @resource { "name": "Cadastro de Projetos", "route":"projetos", "access": "private", "type": "module" }
 */
class ProjetosController extends AppController
{
    var $name = "Projetos";

    var $layout = 'default';

    /**
    * @resource { "name": "Projetos", "route":"projetos\/index", "access": "private", "type": "select" }
    */
    public function index($coProjeto = null)
    {
        $criteria = null;
        if($coProjeto != null)
        {
            $this->layout = 'iframe';
            $this->set('coProjeto', $coProjeto);
            $criteria['Projeto.co_projeto'] = $coProjeto;
        }

        $this->Projeto->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Projeto.ic_ativo' => 1
            )
        );

        if(!empty($this->data)) {
            if(!empty($this->data['Projeto']['no_projeto'])) {
                $criteria['Projeto.no_projeto'] = $this->data['Projeto']['no_projeto'];
            }
        }

        $this->set('projetos', $this->paginate($criteria));
    }

    /**
    * @resource { "name": "iframe", "route":"projetos\/iframe", "access": "private", "type": "select" }
    */
    function iframe($coProjeto)
    {
        $this->set(compact('coProjeto'));
    }

    /**
    * @resource { "name": "Novo Projeto", "route":"projetos\/add", "access": "private", "type": "insert" }
    */
    public function add($iframe = false)
    {
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
        	$projeto = $this->Projeto->findByNoProjeto($this->data['Projeto']['no_projeto']);
        	if(!empty($projeto)) {
        		$this->reInsert($projeto);
        	} else {
            	$this->Projeto->create();
	            if($this->Projeto->save($this->data)) {
	                $this->Session->setFlash(__('Registro salvo com sucesso', true));
	                $this->redirect(array(
	                    'action' => 'index'
	                ));
	            } else {
	                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
	            }
        	}
        }
    }

    /**
    * @resource { "name": "Editar Projeto", "route":"projetos\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Projeto->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Projeto->read(null, $id);
        }
 }

    /**
    * @resource { "name": "Exclusão Lógica", "route":"projetos\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Projeto->id = $id;
            $msg = null;
            $projeto['Projeto']['ic_ativo'] = 0;

            if($this->Projeto->saveField('ic_ativo',0)){
                $msg = "Projeto bloqueado com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($projeto)
    {
		if( $projeto['Projeto']['ic_ativo'] == 1 ) {
			$msg = "Projeto já cadastrado!";
		} else {
            $this->Projeto->id = $projeto['Projeto']['co_projeto'];
			if($this->Projeto->saveField('ic_ativo', 1)){
				$msg = "Projeto cadastrado com sucesso!";
			} else {
				$msg = "Houve um erro no cadastro";
			}
		}
	
		$this->Session->setFlash(__($msg, true));
		$this->redirect(array(
			'action' => 'index'
		));
    }
}