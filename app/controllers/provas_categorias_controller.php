<?php

class ProvasCategoriasController extends AppController
{

    var $name   = 'ProvasCategorias';

    var $uses   = array(
        'ProvaCategoria'
    );
    
    var $layout = 'blank';

    function index( $coProva, $mensagem = '' )
    {
        $this->ProvaCategoria->recursive = 0;
        $this->set('categorias', $this->paginate( array( 'ProvaCategoria.co_prova'=>$coProva ) ));
        $this->set('mensagem', $mensagem);
        $this->set('coProva', $coProva);
    }

    function add( )
    {
        $mensagem   =   '';
        if (! empty($this->data)) {
            $this->ProvaCategoria->create();
            if ($this->ProvaCategoria->save($this->data)) {
                $mensagem   =   'Registro salvo com sucesso';
            } else {
                $mensagem   =   'Campo Nome da Categoria em branco ou já cadastrado. Por favor, tente novamente.';
            }
        }
        $this->redirect(array(
            'action' => 'index', $this->data['ProvaCategoria']['co_prova'], $mensagem
        ));
    }

    function edit($id = null)
    {        
        $this->ProvaCategoria->id = $id;
        $this->ProvaCategoria->saveField("ds_prova_categoria", up($this->data["ds_prova_categoria"]));
        return false;
    }

    function delete($idProva, $id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
        }
        if ($this->ProvaCategoria->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
        } else {
            $this->Session->setFlash(__('Erro ao excluir registro', true));
        }
        $this->redirect(array(
           'controller' => 'modalidades' ,'action' => 'edit_prova', $idProva
        ));
    }
    
    function listar( $coProva = 0 ) {
        
        $criterios  = array();
        if($coProva > 0) {
            $criterios['conditions'] = array('co_prova'=> $coProva);
        }

        echo json_encode ( $this->ProvaCategoria->find ( 'list', $criterios ) );

        exit ();
    }
}
