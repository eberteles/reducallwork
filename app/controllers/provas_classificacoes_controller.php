<?php

class ProvasClassificacoesController extends AppController
{

    var $name   = 'ProvasClassificacoes';

    var $uses   = array(
        'ProvaClassificacao'
    );
    
    var $layout = 'blank';

    function index( $coProva, $mensagem = '' )
    {
        $this->ProvaClassificacao->recursive = 0;
        $this->set('classificacoes', $this->paginate( array( 'ProvaClassificacao.co_prova'=>$coProva ) ));
        $this->set('mensagem', $mensagem);
        $this->set('coProva', $coProva);
    }

    function add( )
    {
        $mensagem   =   '';
        if (! empty($this->data)) {
            $this->ProvaClassificacao->create();
            if ($this->ProvaClassificacao->save($this->data)) {
                $mensagem   =   'Registro salvo com sucesso';
            } else {
                $mensagem   =   'Campo Nome da Classificação em branco ou já cadastrado. Por favor, tente novamente.';
            }
        }
        $this->redirect(array(
            'action' => 'index', $this->data['ProvaClassificacao']['co_prova'], $mensagem
        ));
    }

    function edit($id = null)
    {        
        $this->ProvaClassificacao->id = $id;
        $this->ProvaClassificacao->saveField("ds_prova_classificacao", up($this->data["ds_prova_classificacao"]));
        return false;
    }

    function delete($idProva, $id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
        }
        if ($this->ProvaClassificacao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
        } else {
            $this->Session->setFlash(__('Erro ao excluir registro', true));
        }
        $this->redirect(array(
           'controller' => 'modalidades' ,'action' => 'edit_prova', $idProva
        ));
    }
    
    function listar( $coProva = 0 ) {
        
        $criterios  = array();
        if($coProva > 0) {
            $criterios['conditions'] = array('co_prova'=> $coProva);
        }

        echo json_encode ( $this->ProvaClassificacao->find ( 'list', $criterios ) );

        exit ();
    }
}
