<?php

class ProvasSubCategoriasController extends AppController
{

    var $name   = 'ProvasSubCategorias';

    var $uses   = array(
        'ProvaSubCategoria'
    );
    
    var $layout = 'blank';

    function index( $coProva, $mensagem = '' )
    {
        $this->ProvaSubCategoria->recursive = 0;
        $this->set('subcategorias', $this->paginate( array( 'ProvaSubCategoria.co_prova'=>$coProva ) ));
        $this->set('mensagem', $mensagem);
        $this->set('coProva', $coProva);
    }

    function add( )
    {
        $mensagem   =   '';
        if (! empty($this->data)) {
            $this->ProvaSubCategoria->create();
            if ($this->ProvaSubCategoria->save($this->data)) {
                $mensagem   =   'Registro salvo com sucesso';
            } else {
                $mensagem   =   'Campo Nome da Sub-Categoria em branco ou já cadastrado. Por favor, tente novamente.';
            }
        }
        $this->redirect(array(
            'action' => 'index', $this->data['ProvaSubCategoria']['co_prova'], $mensagem
        ));
    }

    function edit($id = null)
    {        
        $this->ProvaSubCategoria->id = $id;
        $this->ProvaSubCategoria->saveField("ds_prova_subcategoria", up($this->data["ds_prova_subcategoria"]));
        return false;
    }

    function delete($idProva, $id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
        }
        if ($this->ProvaSubCategoria->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
        } else {
            $this->Session->setFlash(__('Erro ao excluir registro', true));
        }
        $this->redirect(array(
           'controller' => 'modalidades' ,'action' => 'edit_prova', $idProva
        ));
    }
    
    function listar( $coProva = 0 ) {
        
        $criterios  = array();
        if($coProva > 0) {
            $criterios['conditions'] = array('co_prova'=> $coProva);
        }

        echo json_encode ( $this->ProvaSubCategoria->find ( 'list', $criterios ) );

        exit ();
    }
}
