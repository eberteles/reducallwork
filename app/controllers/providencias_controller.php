<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/11/2015
 * Time: 15:19
 */
class ProvidenciasController extends AppController
{
    var $name = "Providencias";

    var $layout = 'iframe';

    public function index($coReclamacao)
    {
        $criteria = null;

        $this->Providencia->recursive = 0;
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Providencia.co_reclamacao' => $coReclamacao
            )
        );

        if(!empty($this->data)) {
            $this->Providencia->create();
            if($this->Providencia->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index',
                    $coReclamacao
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('providencias', $this->paginate($criteria));
        $this->set('coReclamacao', $coReclamacao);
    }

    function iframe($coReclamacao)
    {
        $this->set(compact('coReclamacao'));
    }

    public function add($iframe = false)
    {
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Providencia->create();

            $providencia = $this->Providencia->findByDsProvidencia($this->data['Providencia']['ds_providencia']);
            if(isset($providencia['Providencia']) && $providencia['Providencia']['ic_ativo'] == 0) {
                $this->reInsert($providencia['Providencia']['co_providencia']);
            }
            if($this->Providencia->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Providencia->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->data = $this->Providencia->read(null, $id);
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Providencia->id = $id;
            $msg = null;
            $providencia['Providencia']['ic_ativo'] = 0;

            if($this->Providencia->saveField('ic_ativo',0)){
                $msg = "Providencia bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Providencia->id = $id;
            $msg = null;
            $providencia['Providencia']['ic_ativo'] = 1;

            if($this->Providencia->saveField('ic_ativo',1)){
                $msg = "Providencia cadastrada com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}