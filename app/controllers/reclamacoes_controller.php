<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/11/2015
 * Time: 15:15
 */

class ReclamacoesController extends AppController
{
    var $name = "Reclamacoes";

    var $layout = 'default';

    var $st_reclamacao = array(
        'EM ANDAMENTO' => 'EM ANDAMENTO',
        'CONCLUÍDA'    => 'CONCLUÍDA'
    );

    public function index($coFornecedor = null)
    {
        $criteria = null;
        if($coFornecedor != null)
        {
            $this->layout = 'iframe';
            $this->set('coFornecedor', $coFornecedor);
            $criteria['Reclamacao.co_fornecedor'] = $coFornecedor;
        }
        $this->loadModel('Fornecedor');
        $this->loadModel('Contrato');
        $this->paginate = array(
            'limit' => 100,
            'conditions' => array(
                'Reclamacao.ic_ativo' => 1
            )
        );

        if(!empty($this->data)) {
            if(!empty($this->data['Reclamacao']['num_reclamacao'])) {
                $criteria['Reclamacao.nu_reclamacao'] = $this->data['Reclamacao']['num_reclamacao'];
            }
            if(!empty($this->data['Reclamacao']['stt_reclamacao'])) {
                $criteria['Reclamacao.st_reclamacao'] = $this->data['Reclamacao']['stt_reclamacao'];
            }
            if(!empty($this->data['Reclamacao']['cod_fornecedor'])) {
                $criteria['Reclamacao.co_fornecedor'] = $this->data['Reclamacao']['cod_fornecedor'];
            }
            if(!empty($this->data['Reclamacao']['cod_contrato'])) {
                $criteria['Reclamacao.co_contrato'] = $this->data['Reclamacao']['cod_contrato'];
            }
        }

        $this->set('tipos', $this->st_reclamacao);

        $this->set('fornecedores', $this->Fornecedor->find('list',array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));

        $this->set('contratos', $this->Contrato->find('list',array(
            'conditions' => array(
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        )));

        $this->set('reclamacoes', $this->paginate($criteria));
    }

    function iframe($coFornecedor)
    {
        $this->set(compact('coFornecedor'));
    }

    public function add($iframe = false)
    {
        $this->loadModel('Fornecedor');
        if ($iframe == true) {
            $this->layout = 'iframe';
        }

        if(!empty($this->data)) {
            $this->Reclamacao->create();

            $this->data['Reclamacao']['nu_year'] = date('Y');

            $reclamacao = $this->Reclamacao->find('all', array(
                'fields' => array(
                    'MAX(Reclamacao.nu_prefix) AS max'
                ),
                'conditions' => array(
                    'Reclamacao.nu_year' => $this->data['Reclamacao']['nu_year']
                )
            ));

            if($reclamacao[0][0]['max'] == null)
                $reclamacao[0][0]['max'] = 0;

            $this->data['Reclamacao']['nu_prefix'] = $reclamacao[0][0]['max'] + 1;

            if($this->Reclamacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $tp_reclamacao = array(
            'MAU ATENDIMENTO' => 'MAU ATENDIMENTO',
            'NEGATIVA DE ATENDIMENTO' => 'NEGATIVA DE ATENDIMENTO',
            'PROBLEMAS NA CIRURGIA' => 'PROBLEMAS NA CIRURGIA',
            'OUTROS' => 'OUTROS'
        );
        $this->set('tipos', $tp_reclamacao);

        $this->set('fornecedores', $this->Fornecedor->find('list',array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));
    }

    public function edit($id = null)
    {
        $this->loadModel('Fornecedor');
        $this->loadModel('Contrato');
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if(!empty($this->data)) {
            if($this->Reclamacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $tp_reclamacao = array(
            'MAU ATENDIMENTO' => 'MAU ATENDIMENTO',
            'NEGATIVA DE ATENDIMENTO' => 'NEGATIVA DE ATENDIMENTO',
            'PROBLEMAS NA CIRURGIA' => 'PROBLEMAS NA CIRURGIA',
            'OUTROS' => 'OUTROS'
        );
        $this->set('tipos', $tp_reclamacao);

        $this->set('fornecedores', $this->Fornecedor->find('list',array(
            'fields' => array(
                'Fornecedor.co_fornecedor',
                'Fornecedor.nome_combo'
            ),
            'conditions' => array(
                'Fornecedor.ic_ativo' => 1
            )
        )));

        $this->data = $this->Reclamacao->read(null, $id);

        $this->set('contratos', $this->Contrato->find('list',array(
            'conditions' => array(
                'Contrato.co_fornecedor' => $this->data["Reclamacao"]["co_fornecedor"],
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        )));
    }

    public function logicDelete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Reclamacao->id = $id;
            $msg = null;
            $reclamacao['Reclamacao']['ic_ativo'] = 0;

            if($this->Reclamacao->saveField('ic_ativo',0)){
                $msg = "Reclamacao bloqueada com sucesso!";
            }else{
                $msg = "Houve um erro no bloqueio";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Reclamacao->id = $id;
            $msg = null;
            $reclamacao['Reclamacao']['ic_ativo'] = 1;

            if($this->Reclamacao->saveField('ic_ativo',1)){
                $msg = "Reclamacao cadastrada com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser cadastrado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function listContratos()
    {
        $this->loadModel('Contrato');
        $this->Contrato->recursive = 0;
        $contratos = $this->Contrato->find('all',array(
            'conditions' => array(
                'Contrato.co_fornecedor' => $this->params["form"]["co_fornecedor"],
                'Contrato.ic_ativo' => 1
            ),
            'fields' => array(
                'Contrato.co_contrato','Contrato.nu_contrato'
            )
        ));

        echo json_encode($contratos);
        exit;
    }

    public function changeStatus($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if($id) {
            $reclamacao = $this->Reclamacao->read(null, $id);
//            $this->Reclamacao->id = $id;
            $msg = null;
            if($reclamacao['Reclamacao']['st_reclamacao'] == "EM ANDAMENTO")
                $reclamacao['Reclamacao']['st_reclamacao'] = "CONCLUÍDA";
            else
                $reclamacao['Reclamacao']['st_reclamacao'] = "EM ANDAMENTO";

            if($this->Reclamacao->saveField('st_reclamacao',$reclamacao['Reclamacao']['st_reclamacao'])){
                $msg = "Reclamacao atualizada com sucesso!";
            }else{
                $msg = "Houve um erro no atualização";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser atualizado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }
}