<?php
/**
 * Classe CONTROLLER de Recursos do GESCON.
 *
 * @category   Controllers
 * @package    App/Controllers
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @license    <licença-n2oti-link>
 * @version    Release: @package_version@
 * @since      Class available since Release 3.0.0
 * @resource { "name": "Manter Recursos", "route":"recursos", "access": "private", "type": "module" }
 */
class RecursosController extends AppController
{
	/**
	 * @var string
	 */
	var $name = "Recursos";
	
	/**
	 * @var string
	 */
	var $filename = "resources";
	
	/**
	 * @var array
	 */
	var $arNoResources = array(
		'',
		'.',
		'..',
		'helpers',
		'components',
	);
	
	var $arPublic = array(
		'dsp_relatorio',
		'contratos_relatorio',
		'lct_relatorio',
		'clientes',
		'hmab_pesquisa',
		'usuarios:autenticar',
		'usuarios:esqueci',
		'usuarios:checkusuario',
		'usuarios:checkdocumento',
		'usuarios:direciona',
		'contratos:add_inscricao',
		'contratos:pam_externo',
		'contratos:add_pam_externo',	
		'error'
	);
	
	var $arSgTipo = array(
		'add'			=> 'I', 
		'edit'			=> 'A',
		'index' 		=> 'C',
		'delete'		=> 'E',
		'logicDelete'	=> 'E'
	);
	
	/**
	 * @var string
	 */
	var $dsControllersPath = null;
	
	/**
	 * @var string
	 */
	var $dsViewsPath = null;

	/**
	 * @var array
	 */
	public $helpers = array(
		'Imprimir'
	);
	
	/**
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->dsControllersPath 	= APPLICATION_PATH . DIRECTORY_SEPARATOR . "controllers";
		$this->dsViewsPath 			= APPLICATION_PATH . DIRECTORY_SEPARATOR . "views";
	}
	
	/**
	 * @return void
	 * @resource { "name": "Listar Recursos", "route":"recursos\/index", "access": "private", "type": "select" }
	 */
	public function index()
	{		
        $this->Recurso->recursive = 0;
        $criteria = array();
        $criteria['parent_id'] 	= NULL;
        
		if (! empty($this->data)) {
			$criteria['no_recurso like'] = '%' . up($this->data['Recurso']['no_recurso']) . '%';
			$criteria['ds_rota like'] = '%' . up($this->data['Recurso']['ds_rota']) . '%';
		}
		
		$this->paginate = array(
    			'Recurso' => array(
					'limit' => 20,
					'order' => array('nu_ordem' => 'ASC'),    					
		));
		
        $this->set('arSgTipo', array(
        	'I' => 'Inserção',
        	'A' => 'Alteração',
        	'E' => 'Exclusão',
        	'C' => 'Consulta',
        	'M' => 'Módulo',
        ));
        
        $page = 1;
        if( isset($this->params['named'])
        	&& isset($this->params['named']['page']) ) {
        	$page = $this->params['named']['page'];        	
        }        
        $this->set('page', $page);
        $this->set('recursos', $this->paginate($criteria));
	}
	
	/**
	 * @resource { "name": "Adicionar Recurso", "route":"recursos\/add", "access": "private", "type": "insert" }
	 */
	public function add()
	{
		$criteria = array();
		
		if (!empty($this->data)) {
								
				$parentId = null;
				
				if( $this->data['Recurso']['parent_id'] ){
					$parentId = $this->data['Recurso']['parent_id'];
				}
				
				$ordem = $this->Recurso->find('first', array(
   				    'conditions' => array(
						'parent_id' => $parentId		
					),
				   'fields' => 'MAX(Recurso.nu_ordem) as nu_ordem',
				));
				
				$this->data['Recurso']['nu_ordem'] = ++$ordem[0]['nu_ordem'];
				
				$this->Recurso->create();
				if ($this->Recurso->save($this->data)) {
					
					$this->reordenar($this->data['Recurso']['parent_id']);
					
					$this->Session->setFlash(__('Registro salvo com sucesso', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
				}

		}
	
		$recursos = $this->Recurso->find('threaded', array(
			'fields' => array(
				'Recurso.*',
			),
			'conditions' => $criteria
		));
		
        $this->set('recursos', $recursos);
        $this->set('arSgTipo', array(
        	'I' => 'Inserção',
        	'A' => 'Alteração',
        	'E' => 'Exclusão',
        	'C' => 'Consulta',
        	'M' => 'Módulo',
        ));
	}

	/**
	 * @resource { "name": "Editar Recurso", "route":"recursos\/edit", "access": "private", "type": "update" }
	 */
	public function edit($id = null)
	{
		if ( !$id && empty($this->data)) {
			$this->Session->setFlash(__('Identificador inválido', true));
			$this->redirect(array(
					'action' => 'index'
			));
		}
		if ( !empty($this->data)) {
			if ($this->Recurso->save($this->data)) {
				
				$this->reordenar($this->data['Recurso']['parent_id']);
				
				$this->Session->setFlash(__('Registro salvo com sucesso', true));
				$this->redirect(array(
					'action' => 'index'
				));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Recurso->read(null, $id);
		}
		
		$criteria = array();
		
		$recursos = $this->Recurso->find('threaded', array(
			'fields' => array(
				'Recurso.*',
			),
			'conditions' => $criteria
		));
		
		$this->set('recursos', $recursos);		
		$this->set('id', $id);
        $this->set('arSgTipo', array(
        	'I' => 'Inserção',
        	'A' => 'Alteração',
        	'E' => 'Exclusão',
        	'C' => 'Consulta',
        	'M' => 'Módulo',
        ));
	}

	/**
	 * @resource { "name": "Listar Recursos Filhos", "route":"recursos\/childs", "access": "private", "type": "select" }
	 */
	public function childs( $coRecurso, $coPerfil = false )
	{
		$criteria = array(
			'fields' => array(
				'Recurso.*',
			),
			'conditions' => array(
				'Recurso.parent_id' => $coRecurso,
			),
			'order' => array('nu_ordem' => 'asc'),
		);
		$this->Recurso->recursive = 1;
		
		$recursos = array();
		$recursos_full = $this->Recurso->find('all', $criteria);

		foreach( $recursos_full as $key => $recurso ){
			$permissoes = array();
			if( $coPerfil ) {
				foreach( $recurso['Permissao'] as $keyp => $permissao ) {
					if( $permissao['co_perfil'] == $coPerfil ){
						$permissoes[] = $permissao;
					}
					unset($recurso['Permissao'][$keyp]);
				}
				$recurso['Permissao'] = $permissoes;
			}
			$recursos[] = $recurso;
		}
		
		echo json_encode($recursos); exit;
	}
	
	/**
	 * Método atribui todas as permissões do sistema para o perfil.
	 * @resource { "name": "Conceder acesso administrador", "route":"recursos\/acessototal", "access": "private", "type": "insert" }
	 * @return void
	 */
	public function acessototal($coPerfil)
	{
		App::import('Model', 'Recurso');
		$recurso = new Recurso();

		$conditions = array(
			'ic_publico = FALSE'
		);
		$recursos = $recurso->find('all', compact('conditions'));
		
		App::import('Model', 'Permissao');
		$permissao = new Permissao();
		foreach( $recursos as $item ) {
			$arItem = array(
				'Permissao' => array(
					'co_perfil' => $coPerfil,
					'co_recurso' => $item['Recurso']['co_recurso']
				)
			);
			$permissao->create();
			$permissao->save($arItem);
		}
		
		$this->Session->setFlash(__('Atribuido todas as permissões do sistema para o Perfil.', true));
		$this->redirect(array('controller' => 'permissoes', 'action' => 'perfil', $coPerfil));
	}

	/**
	 * @resource { "name": "Reordenar Recurso", "route":"recursos\/reordenar", "access": "private", "type": "update" }
	 * @return void
	 */
	public function reordenar( $parent_id = NULL )
	{
		$this->Recurso->recursive = 0;
		$recursos = $this->Recurso->find('all', array(
			'conditions' => array(
				'parent_id' => $parent_id
			),
			'order' => array(
				'co_recurso' => 'DESC'
			)
		));
		
		foreach( $recursos as $ordem => $recurso ) {
			$recurso['Recurso']['nu_ordem'] = ++$ordem;
			$this->Recurso->save($recurso);
		}

		$this->Session->setFlash(__('Recursos ordenados.', true));
		$this->redirect(array('controller' => 'recursos', 'action' => 'index'));
	}
	
	/**
	 * @resource { "name": "Ordenar Recurso", "route":"recursos\/ordem", "access": "private", "type": "update" }
	 * @return void
	 */
	public function ordem( $coRecurso, $direction, $page, $retorno = 'redirect' )
	{		
		$this->Recurso->recursive = 1;
		
		$recurso = $this->Recurso->findByCoRecurso($coRecurso);

		$newOrdem = $recurso['Recurso']['nu_ordem'];	
		
		$recursoant = false;
		
		if( $direction == 'up' ){
			$newOrdem--;
		} else if( $direction == 'down' ) {
			$newOrdem++;
		}

		$recursoant = $this->Recurso->find('first', array('conditions' => array(
			'Recurso.parent_id' => $recurso['Recurso']['parent_id'],
			'Recurso.nu_ordem' => $newOrdem
		)));
		
		$doOrder = true;
		
		if( !$recursoant || ($recurso['Recurso']['nu_ordem'] == 1 && $direction == 'up') ){
			$doOrder = false;
		}
		
		$response = array(
			'status' => 200
		);
				
		if( $doOrder ) {					
			$this->Recurso->id = $recurso['Recurso']['co_recurso'];
			$this->Recurso->saveField('nu_ordem', $newOrdem);
			
			$this->Recurso->id = $recursoant['Recurso']['co_recurso'];
			$this->Recurso->saveField('nu_ordem', $recurso['Recurso']['nu_ordem']);
			
		} else {
			$response = array(
				'status' => 500
			);
		}
		
		if( $retorno == 'redirect' ){
			$this->redirect(array('action' => 'index', 'page:' . $page));
		}
		
		exit(json_encode($response));
	}
					
	/**
	 * @resource { "name": "Atualizar Recursos", "route":"recursos\/atualizar", "access": "private", "type": "insert" }
	 * @return void
	 */	
	public function atualizar()
	{
		$types = array(
			'insert' => 'I',
			'select' => 'C',
			'update' => 'A',
			'delete' => 'E',
			'module' => 'M'
		);
		
		$access = array(
			0 => 'private',
			1 => 'public'
		);
		
		$resources = array(
			'route',
			'access',
			'type'
		);
		
		$controllers 	= scandir($this->dsControllersPath);
		
		$obDataSource = $this->Recurso->getDataSource();		
		$obDataSource->begin($this->Recurso);
		
		
		try {
			$nuOrdemTotal = 1;
			
			foreach( $controllers as $key => $controller ){
				$isPublicModule = null;
				$coRecursoPai = 0;
		
				$filename = $this->dsControllersPath . DIRECTORY_SEPARATOR . $controller;
		
				if( is_file($filename) ) {		
					$content = file_get_contents($filename);
					
					$arContent = token_get_all($content);
					$arResources = array();
					
					foreach( $arContent as $value ){
						$token = (int)$value[0];
						$token_name = "";
						$token_name = token_name($token);
			
						if( $token_name == 'T_DOC_COMMENT' ){
							$content = str_replace(array("/*", "*/", "*", chr(10)), "", $value[1]);
							
							$arTxProperties = explode('@', $content);
															
							foreach( $arTxProperties as $property ){
								$property = trim($property);
								if( $property && strpos($property, 'resource') === 0 ) {
									$property = str_replace(array("resource "), "", $property);
									$property = trim($property);
									$arResources[] = json_decode($property);
								}
							}
						}
					}

					$coRecursoPai = NULL;
					$nuOrdem = 1;
					
					foreach( $arResources as $resource ) {
						$recurso = $this->Recurso->findByDsRota($resource->route);
						
						if( !$recurso ) {
							$nuOrdemSav = 0;
							if( $resource->type == 'module' ){
								$nuOrdemSav = $nuOrdemTotal;
								$nuOrdemTotal++;
								$nuOrdem = 1;
							} else {
								$nuOrdemSav = $nuOrdem;
								$nuOrdem++;
							}
														
							if( $resource->type == 'module' ){
								$icChilds = true;
							} else {
								$icChilds = false;
							}
							
							$recurso['Recurso'] = array(
								'no_recurso' 		=> $resource->name,
								'parent_id'			=> $coRecursoPai,
								'ds_rota'			=> $resource->route,
								'sg_tipo'			=> $types[$resource->type],
								'ic_publico'		=> array_search($resource->access, $access),
								'ic_childs'			=> $icChilds,
								'nu_ordem'			=> $nuOrdemSav,
							);
								
							$this->Recurso->create();
							$recurso = $this->Recurso->save($recurso);
							if( $resource->type == 'module' ){
								$coRecursoPai = $this->Recurso->getLastInsertID();
							}
						}
					}
				}
			}
			$obDataSource->commit($this->Recurso);
		} catch( Exception $e ) {
			$obDataSource->rollback($this->Recurso);
			echo json_encode(array(
				'status' => 500
			));
			exit();
		}

		echo json_encode(array(
			'status' => 200
		));
		exit();
	}
}