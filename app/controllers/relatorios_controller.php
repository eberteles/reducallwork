<?php
set_time_limit(0);
ini_set("memory_limit", "1G");
ob_start();

App::import('Vendor', 'pdf');
App::import('Vendor', 'htmlpdf');
App::import('Helper', array(
    'Print'
));

/**
* @resource { "name": "Relatórios", "route":"relatorios", "access": "private", "type": "module" }
*/
class RelatoriosController extends AppController
{

    /**
     *
     * @var string
     */
    public $name = 'Relatorios';

    /**
     *
     * @var array
     */
    public $uses = array(
        'Relatorio',
        'Pagamento'
    );

    /**
     *
     * @var boolean
     */
    public $autoRender = false;

    /**
     *
     * @var PDF
     */
    public $PDF;

    /**
     *
     * @var HTMLPDF
     */
    public $HTMLPDF;

    /**
     *
     * @var array
     */
    public $helpers = array(
        'ImprimirArea'
    );

    var $components = array('Downloader');

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->PDF = new PDF();
        $this->HTMLPDF = new HTMLPDF();
    }

    /**
     *
     */
    private function isTipoRelatorioExcel()
    {
        return !!isset($this->params['url']['tipo']) && $this->params['url']['tipo'] === 'excel';
    }

    /**
    * @resource { "name": "Relatorio", "route":"relatorios\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        App::import('Model', 'Situacao');
        $mdSituacao = new Situacao();
        $situacaos = $mdSituacao->find('list', array(
            'conditions' => 'Situacao.tp_situacao = "C"'
        ));
        $this->set(compact('situacaos'));

        App::import('Model', 'Modalidade');
        $mdModalidade = new Modalidade();
        $modalidades = $mdModalidade->find('list');
        $this->set(compact('modalidades'));

        App::import('Model', 'Contratacao');
        $mdContratacao = new Contratacao();
        $this->set('contratacoes', ($mdContratacao->find('list')));

        App::import('Model', 'Categoria');
        $mdCategoria = new Categoria();
        $categorias = $mdCategoria->find('list');
        $this->set(compact('categorias'));

        App::import('Model', 'Setor');
        $mdSetor = new Setor();
        $setores = $mdSetor->find('list', array(
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));
        $this->set(compact('setores'));

        App::import('Model', 'Fase');
        $mdFase = new Fase();
        $fases = $mdFase->find('list');
        $this->set(compact('fases'));

        App::import('Model', 'Area');
        $mdArea = new Area();
        $areas = $mdArea->find('list', array(
            'conditions' => 'parent_id is null'
        ));
        $this->set(compact('areas'));

        App::import('Model', 'GrupoAuxiliar');
        $grupoAux   = new GrupoAuxiliar();
        $this->set('funcionarios', $grupoAux->find('list') );

        $this->set('meses', array(
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        ));
        $this->set('anos', $this->Relatorio->anosContratos());

        // Gráfico
        $this->helpers[] = "Number";
        $this->set("anoAtual", date("Y"));
        $this->set("pgJan", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '01',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgJan", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '01',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgFev", $this->Pagamento->find('first'), array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '02',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        ));
        $this->set("aPgFev", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '02',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgMar", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '03',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgMar", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '03',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgAbr", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '04',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgAbr", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '04',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgMai", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '05',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgMai", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '05',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgJun", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '06',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgJun", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '06',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgJul", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '07',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgJul", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '07',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgAgo", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '08',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgAgo", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '08',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgSet", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '09',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgSet", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '09',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgOut", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '10',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgOut", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '10',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgNov", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '11',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgNov", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '11',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));
        $this->set("pgDez", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '12',
                'not' => array(
                    'Pagamento.dt_pagamento' => null
                )
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as pg'
            )
        )));
        $this->set("aPgDez", $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.nu_ano_pagamento' => date('Y'),
                'Pagamento.nu_mes_pagamento' => '12',
                'Pagamento.dt_pagamento' => null
            ),
            'fields' => array(
                'sum(Pagamento.vl_pagamento) as aPg'
            )
        )));

        $this->render();
    }

    /**
    * @resource { "name": "iFrame Imprimir", "route":"relatorios\/iframe_imprimir", "access": "private", "type": "select" }
    */
    public function iframe_imprimir()
    {
        $this->layout = 'blank';
        $url = $this->data["url"];
        $this->set(compact('url'));
        $this->render();
    }

    // ***********************Relatórios Consolidados ***********************\\.

    /**
     * Relatório de Atividades com Pendências
     * @item 67
     * @resource { "name": "Relatório de Atividades com Pendências", "route":"relatorios\/listarAtividadesPendentes", "access": "private", "type": "select" }
     */
    public function listarAtividadesPendentes(){
        $contratos = $this->Relatorio->listarAtividadesPendentes();
        $this->gerarRelatorio($contratos, 'Lista Geral de Atividade com Pendência');
    }

    /**
     * Relatório de Periodicidade por Setor
     * @resource { "name": "Relatório de Periodicidade por Setor", "route":"relatorios\/listarPamPorTempoFase", "access": "private", "type": "select" }
     * Item 43
     */
    public function listarPamPorTempoFase()
    {
        $pamsFase = $this->Relatorio->listarPamsFase();
        foreach($pamsFase as $k=>$pam){
            $pamsFase[$k]['RELATORIO']['Início da fase'] = date("d-m-Y", strtotime($pam['RELATORIO']['Início da fase']));
            if($pam['RELATORIO']['Final'] == null || $pam['RELATORIO']['Final'] == ''){
                $data = new DateTime('now');
                $pamsFase[$k]['RELATORIO']['Final'] = date("d-m-Y", strtotime($data->format('d-m-Y')));
            } else {
                $pamsFase[$k]['RELATORIO']['Final'] = date("d-m-Y", strtotime($pam['RELATORIO']['Final']));
            }

            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $corAviso = null; $mensagemAviso = null; $tempoDecorrido = null;
            $dataini = date('d/m/Y H:i:s', strtotime($pamsFase[$k]['RELATORIO']['Inicio']));
            $print->setPrazoDecorridoDetalhaProcesso(
                $dataini,
                null,
                $pamsFase[$k]['RELATORIO']['Duracao'],
                $corAviso,
                $mensagemAviso,
                $tempoDecorrido
            );

            $pamsFase[$k]['RELATORIO']['Tempo parado nesta fase'] = $tempoDecorrido;

            unset($pamsFase[$k]['RELATORIO']['dt_andamento']);
            unset($pamsFase[$k]['RELATORIO']['Inicio']);
            unset($pamsFase[$k]['RELATORIO']['mm_duracao_fase']);
            unset($pamsFase[$k]['RELATORIO']['Duracao']);
        }

        $this->gerarRelatorio($pamsFase, 'Lista de PAMs por tempo em fase');
    }
    
    public function listarProspeccao()
    {
        $pamsFase = $this->Relatorio->listarProspeccao();

        $this->gerarRelatorio($pamsFase, 'Lista de Prospecções');
    }

    /**
     *
     * @param array $relatorio
     * @param string $nome
     */
    private function downloadExcel(array $relatorio, $nome)
    {
        header('Content-Type:   application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename={$nome}.xls");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);

        $agruparDadosPelaChaveDoArray = !isset($relatorio[0]['RELATORIO']);

        $dados = $this->retornaDadosTratadosParaExcel($relatorio, $agruparDadosPelaChaveDoArray);

        $primeiroElemento = current($relatorio);
        $colunas = $agruparDadosPelaChaveDoArray ?
                array_keys($primeiroElemento[0]['RELATORIO']) :
                array_keys($primeiroElemento['RELATORIO']);

        $excel = new N2oti\Components\Excel\Excel($colunas, $dados);
        $excel->setAgruparDadosPelaChave($agruparDadosPelaChaveDoArray);
        $excel->adicionarImagemNoCabecalho(APP . WEBROOT_DIR . '/img/logo_sistema.png');
        $excel->salvar('php://output');
    }

    /**
     *
     * @param array $relatorio
     * @param array $agruparDadosPelaChaveDoArray
     * @return array
     */
    private function retornaDadosTratadosParaExcel(array $relatorio)
    {
        $retiraChaveRelatorio = function ($elementos) use (&$retiraChaveRelatorio) {
            $primeiroElemento = current($elementos);
            $agruparDadosPelaChaveDoArray = isset($primeiroElemento['RELATORIO']);
            if ($agruparDadosPelaChaveDoArray) {
                foreach ($elementos as $agrupador => $elem) {
                    $elementos[$agrupador] = $retiraChaveRelatorio($elem);
                }
                return $elementos;
            }
            return $elementos['RELATORIO'];
        };
        return array_map($retiraChaveRelatorio, $relatorio);
    }

    /**
     * Relatório Lista Geral de Processos/Contratos
     * @resource { "name": "Relatório Lista Geral de Processos/Contratos", "route":"relatorios\/listarContratosGeral", "access": "private", "type": "select" }
     * @item 44, 45 e 59
     */
    public function listarContratosGeral($intervalo = null, $situacao = null, $setor = null, $ano = null)
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Fornecedor'    => array(
                "_wordwrap" => array(
                    'width' => 65,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
        );

        $contratos = $this->tratarDadosContrato($arCampos, $this->Relatorio->listarContratos(null, null, null, null, null, false, $intervalo, $situacao, $setor, $ano));

        $this->gerarRelatorio($contratos, 'Lista Geral de Contratos');
    }

    /**
     * Relatório de Empenhos por Contratos/Processos
     * @resource { "name": "Relatório de Empenhos por Contratos/Processos", "route":"relatorios\/empenhosContratosAtivos", "access": "private", "type": "select" }
     * @item 48
     */
    public function empenhosContratosAtivos()
    {
        $usuario = $this->Session->read('usuario');
        $contratos = $this->Relatorio->listarEmpenhosContratosAtivos($usuario['Usuario']['co_instituicoes']);
        $this->gerarRelatorio($contratos, 'Relatório de empenhos dos Contratos/Processos Ativos');
    }

    /**
     * Relatório Lista de Contratos com Pendência
     * @resource { "name": "Lista de Contratos com Pendência", "route":"relatorios\/contratosComPendencia", "access": "private", "type": "select" }
     * @item 51
     */
    public function contratosComPendencia()
    {
        $contratos = $this->Relatorio->listarContratosComPendencia();
        $this->gerarRelatorio($contratos, 'Relatório de Contratos com Pendências');
    }

    /**
     *
     * @param array $dados
     * @param string $nome
     */
    private function gerarRelatorio(array $dados, $nome)
    {
        $temPermissaoParaRelatorioEmExcel = Configure::read('App.config.resource.available.relatorioExcel');
        if ($this->isTipoRelatorioExcel() && !$temPermissaoParaRelatorioEmExcel) {
            $this->Session->setFlash('Não está habilitada a permissão de gerar relatórios em excel.');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->isTipoRelatorioExcel() ?
                    $this->downloadExcel($dados, $nome) :
                    $this->PDF->imprimir($this->ReportHtml->createHml($dados), 'L', $nome);
        }
    }

    // ***********************Relatórios Operacionais ***********************\\.

    /**
     * Relatório de Contratos por Categoria
     * @item 52
     * @param $coCategoria
     * @resource { "name": "Relatório de Contratos por Categoria", "route":"relatorios\/contratosPorCategoria", "access": "private", "type": "select" }
     */
    public function contratosPorCategoria($coCategoria)
    {
        App::import('Model', 'Categoria');
        $mdCategoria = new Categoria();
        $categoria = $mdCategoria->read(null, $coCategoria);
        $contratos = $this->Relatorio->listarContratosCategoria($coCategoria);
        $title = $coCategoria ? $categoria['Categoria']['no_categoria'] : 'Todas';
        $this->gerarRelatorio($contratos, 'Lista de Contratos - Categoria: ' . $title);
    }

    /**
     * Relatório de Contratos Por Tipo
     * @item 47
     * @param $coTipo
     * @param bool $exibirSoAtivos
     * @resource { "name": "Relatório de Contratos Por Tipo", "route":"relatorios\/contratosPorTipo", "access": "private", "type": "select" }
     */
    public function contratosPorTipo($coTipo, $exibirSoAtivos = false)
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Fornecedor'    => array(
                "_wordwrap" => array(
                    'width' => 50,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            )
        );

        App::import('Model', 'Modalidade');
        $mdModalidade = new Modalidade();
        $dsModalidade = $mdModalidade->field('ds_modalidade', array('co_modalidade' => $coTipo));
        $contratos = $this->Relatorio->listarContratos(null, $coTipo, null, null, null, $exibirSoAtivos);
        $contratos = $this->tratarDadosContrato($arCampos, $contratos);
        $dsModalidade = ($dsModalidade)? $dsModalidade : 'Todos';
        $this->gerarRelatorio($contratos, 'Lista de Contratos - Tipo: ' . $dsModalidade);
    }

    /**
     * Relatório de Contratos por Tipo de Modalidade
     * @item 49
     * @param $coTipoContratacao
     * @param $exibirSoAtivos
     * @resource { "name": "Relatório de Contratos por Tipo de Modalidade", "route":"relatorios\/contratosPorTipoContratacao", "access": "private", "type": "select" }
     */
    public function contratosPorTipoContratacao($coTipoContratacao, $exibirSoAtivos)
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Fornecedor'    => array(
                "_wordwrap" => array(
                    'width' => 50,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            )
        );

        App::import('Model', 'Contratacao');
        $mdContratacao = new Contratacao();
        $contratos = $this->tratarDadosContrato(
            $arCampos,
            $this->Relatorio->listarContratos(null, null, $coTipoContratacao, null, null, $exibirSoAtivos)
        );

        $ds_contratacao = $mdContratacao->field('ds_contratacao', array('co_contratacao' => $coTipoContratacao));
        $ds_contratacao = ($ds_contratacao) ? $ds_contratacao : 'Todas';

        $this->gerarRelatorio($contratos, 'Lista de Contratos - Tipo de Modalidade de Contratação: ' . $ds_contratacao);
    }

    /**
     * Relatório Lista de Fiscais por Contrato
     * @item 46
     * @resource { "name": "Relatório Lista de Fiscais por Contrato", "route":"relatorios\/fiscaisPorContrato", "access": "private", "type": "select" }
     */
    public function fiscaisPorContrato()
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Fornecedor'    => array(
                "_wordwrap" => array(
                    'width' => 50,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            )
        );

        $contratos = $this->tratarDadosContrato($arCampos, $this->Relatorio->listarFiscaisPorContrato());
        $this->gerarRelatorio($contratos, 'Lista de ' . __('Fiscal', true) . ' por contrato');
    }

    /*
     * [GES-599] Listar contratos com aditivos pendentes
     */

    public function listaContratosAditivosPendentes()
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            ),
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $contratos = $this->tratarDadosContrato($arCampos, $this->Relatorio->listaContratosAditivosPendentes());
        $this->gerarRelatorio($contratos, 'Lista de ' . __('Contratos', true) . ' com ' . __('Aditivos', true) . ' pendentes');
    }

    /**
     * Relatório de Contratos por Fiscais
     * @item 53
     * @resource { "name": "Relatório de Contratos por Fiscais", "route":"relatorios\/contratosPorFiscais", "access": "private", "type": "select" }
     */
    public function contratosPorFiscais()
    {
        $contratos = $this->Relatorio->listarContratoPorFiscais();
        $this->gerarRelatorio($contratos, 'Lista de contratos por fiscal');
    }

    /**
     * Relatório de Contratos sem Garantias
     * @item 50
     * @resource { "name": "Relatório de Contratos sem Garantias", "route":"relatorios\/contratosSemGarantias", "access": "private", "type": "select" }
     */
    public function contratosSemGarantias()
    {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
        );

        $contratos = $this->tratarDadosContrato($arCampos, $this->Relatorio->listarContratosSemGarantias());
        $this->gerarRelatorio($contratos, 'Lista de contratos Sem Garantias');
    }

    // ***********************Relatórios Estatisticos ***********************\\.

    /**
     * Relatório de Contratos com Pagamento em Atraso
     * @item 54
     * @resource { "name": "Relatório de Contratos com Pagamento em Atraso", "route":"relatorios\/quantidadeContratoPagamentoAtraso", "access": "private", "type": "select" }
     */
    public function quantidadeContratoPagamentoAtraso()
    {
        $this->gerarRelatorio($this->Relatorio->listarQuantidadePagamentoAtraso(), 'Quantidade de Contratos com pagamento em atraso');
    }

    /**
     * Relatório de Percentual de Contratos por Tipo
     * @item 58
     * @resource { "name": "Relatório de Percentual de Contratos por Tipo", "route":"relatorios\/quantidadeContratoPorTipo", "access": "private", "type": "select" }
     */
    public function quantidadeContratoPorTipo()
    {
        $this->gerarRelatorio($this->Relatorio->listarQtdContratoPorTipo(), 'Quantidade de Contratos por Tipo de Contrato');
    }

    /**
     * Relatório de Contratos Celebrados Anualmente
     * @item 57
     * @resource { "name": "Relatório de Contratos Celebrados Anualmente", "route":"relatorios\/quantidadeContratoPorStatus", "access": "private", "type": "select" }
     */
    public function quantidadeContratoPorStatus()
    {
        $nomeDoContrato = 'Quantidade de Contratos por Situação';
        $anos = $this->Relatorio->getAnosContrato();
        if (! $this->isTipoRelatorioExcel()) {
            $relatorioHtml = "";
            foreach ($anos as $ano) {
                $relatorioHtml .= $this->ReportHtml->createHml($this->Relatorio->listarQtdContratoPorStatus($ano[0]['Ano']), 'Ano: ' . $ano[0]['Ano']);
            }
            $this->PDF->imprimir($relatorioHtml, 'L', $nomeDoContrato);
        } else {
            $relatoriosPorAnos = array();
            foreach ($anos as $ano) {
                $relatorios = $this->Relatorio->listarQtdContratoPorStatus($ano[0]['Ano']);
                $relatoriosPorAnos['Ano: ' . $ano[0]['Ano']] = $relatorios;
            }
            $this->downloadExcel(array_filter($relatoriosPorAnos), $nomeDoContrato);
        }
    }

    /**
     * Relatório de Contratos Encaminhados para Pagamento por Ano
     * @item 55
     * @resource { "name": "Relatório de Contratos Encaminhados para Pagamento por Ano", "route":"relatorios\/percentualContratosPagamentoPorAno", "access": "private", "type": "select" }
     */
    public function percentualContratosPagamentoPorAno()
    {
        $this->gerarRelatorio($this->Relatorio->listarPcContratosPagamentoPorAno(), 'Percentual de Contratos encaminhados para pagamentos por Ano');
    }

    /**
     * @resource { "name": "Exportar HTML", "route":"relatorios\/html", "access": "private", "type": "select" }
     */
    public function html($export)
    {
        $html = '';
        $html .= '<table  cellspacing="0" style="width: 100%; padding: 0 0 30px 0;">';
        $html .= '<tr>';
        $html .= '<td style="width: 100%;">';
        $html .= '<img alt="Logo" style="width: 100%" src="/img/cabecalho_relatorio.png">';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="width: 100%;">';
        $html .= $export;
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';

        echo $html;
    }

    /**
     * @resource { "name": "Exportar para PDF", "route":"relatorios\/exportToPdf", "access": "private", "type": "select" }
     */
    public function exportToPdf($export)
    {
        $help = new HtmlHelper();

        $html = '';
        $html .= '<table  cellspacing="0" style="width: 100%; padding: 0 0 30px 0;">';
        $html .= '<tr>';
        $html .= '<td style="width: 100%;">';
        $html .= $help->image('cabecalho_relatorio.png', array(
            'style' => 'width: 100%',
            'alt' => 'Logo'
        ));
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= $export;

        $this->HTMLPDF->output($html);
    }

    /**
     * @resource { "name": "Exportar para Excel", "route":"relatorios\/exportToExcel", "access": "private", "type": "select" }
     */
    public function exportToExcel($html)
    {
        header('Content-Type: text/html; charset=utf-8');

        $file = 'relatorio-' . gmdate("dMY") . '.xls';

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/x-msexcel;charset=UTF-8");
        header("Content-Disposition: attachment; filename=\"{$file}\"");
        header("Content-Description: PHP Generated Data");

        echo $html;
        exit();
    }

    /**
     * Relatório de Responsáveis por Atividade
     * @item 60, 62
     * @resource { "name": "Relatório de Responsáveis por Atividade", "route":"relatorios\/responsaveisAtividades", "access": "private", "type": "select" }
     */
    public function responsaveisAtividades() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->responsaveisAtividades());
        $this->gerarRelatorio($dados, 'Relatório de Atividades por Responsáveis');
    }

    /**
     * Relatório de Quantidade de Contratos Realizados Mensalmente
     * @item 61
     * @resource { "name": "Relatório de Quantidade de Contratos Realizados Mensalmente", "route":"relatorios\/quantidadeContratosPorMes", "access": "private", "type": "select" }
     */
    public function quantidadeContratosPorMes($ano) {
        $result = $this->Relatorio->listarQuantidadeContratoMensal($ano);
        $titleSuffix= $ano ? ' | Ano: ' . $ano : ' | Ano: TODOS';
        $this->gerarRelatorio($result, "Quantidade de Contratos Realizados Por Mês {$titleSuffix}");
    }

    /**
     * Relatório de Datas das Atividades
     * @item 63
     * @resource { "name": "Relatório de Datas das Atividades", "route":"relatorios\/datasAtividades", "access": "private", "type": "select" }
     */
    public function datasAtividades() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->datasAtividades());
        $this->gerarRelatorio($dados, 'Relatório de Datas das Atividades');
    }

    /**
     * Relatório de Atividades Atrasadas
     * @item 66
     * @resource { "name": "Relatório de Atividades Atrasadas", "route":"relatorios\/atividadesAtrasadas", "access": "private", "type": "select" }
     */
    public function atividadesAtrasadas() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->atividadesAtrasadas());
        $this->gerarRelatorio($dados, 'Relatório de Atividades Atrasadas');
    }

    /**
     * Relatório contratos com pagamentos em atraso
     * @resource { "name": "Relatório de Fornecedores", "route":"relatorios\/fornecedores", "access": "private", "type": "select" }
     */
    public function contratosPagamentosAtraso() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->contratosPagamentosAtraso());

        $this->gerarRelatorio($dados, 'Lista de Contratos com pagamentos em atraso');
    }

    /**
     * Relatório Financeiro de Atividades
     * @item 64
     * @resource { "name": "Relatório Financeiro de Atividades", "route":"relatorios\/financeiroAtividades", "access": "private", "type": "select" }
     */
    public function financeiroAtividades() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            ),
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->financeiroAtividades());
        $this->gerarRelatorio($dados, 'Relatório Financeiro de Atividades');
    }

    /**
     * Relatório de Processo por Fase de Tramitação
     * @item 56
     * @param $fase
     * @resource { "name": "Relatório de Processo por Fase de Tramitação", "route":"relatorios\/processosPorFaseTramitacao", "access": "private", "type": "select" }
     */
    public function processosPorFaseTramitacao($fase) {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Número do Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            ),
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->processosPorFaseTramitacao($fase));

        $faseModel = ClassRegistry::init('Fase');
        $ds_fase = $faseModel->field('ds_fase', array('co_fase' => $fase));
        $ds_fase = ($ds_fase) ? $ds_fase : 'Todas';

        $this->gerarRelatorio($dados, 'Relatório de Processos por Fase de Tramitação - Fase: ' . $ds_fase);
    }

    /**
     * Relatório de Liquidações por Pagamentos
     * @item 68
     * @resource { "name": "Relatório de Liquidações por Pagamentos", "route":"relatorios\/liquidacoesPorPagamento", "access": "private", "type": "select" }
     */
    public function liquidacoesPorPagamento() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Nome do fornecedor'    => array(
                "_wordwrap" => array(
                    'width' => 50,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Número do processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->liquidacoesPorPagamento());
        $this->gerarRelatorio($dados, 'Relatório de Liquidações por Pagamentos');
    }

    /**
     * Relatório de Itens de Produtos Fornecidos por Contrato/Processo
     * @item 69
     * @resource { "name": "Relatório de Itens de Produtos Fornecidos por Contrato/Processo", "route":"relatorios\/produtosFornecidos", "access": "private", "type": "select" }
     */
    public function produtosFornecidos() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            ),
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->produtosFornecidos());
        $this->gerarRelatorio($dados, 'Relatório de Itens de Produtos Fornecidos');
    }

    /**
     * Relatório de Itens de Produtos que Compõem o Processo/Contrato
     * @item 70
     * @resource { "name": "Relatório de Itens de Produtos que Compõem o Processo/Contrato", "route":"relatorios\/produtosCompoem", "access": "private", "type": "select" }
     */
    public function produtosCompoem() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $arCampos = array(
            'Objeto'    => array(
                "_wordwrap" => array(
                    'width' => 75,
                    'break' => $break,
                    'cut'   => true
                ),
            ),
            'Processo'  => array(
                "_mask"  => array(
                    'mask' => 'processo',
                    'tipo' => 2
                )
            ),
            'Contrato'  => array(
                "_mask"  => array(
                    'mask' => 'contrato',
                    'tipo' => 2
                )
            )
        );

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->produtosCompoem());
        $this->gerarRelatorio($dados, 'Relatório de Itens de Produtos');
    }

    /**
     * Relatório de Itens de Ata
     * @item 70
     * @resource { "name": "Relatorio de Itens de ata", "route":"relatorios\/relatorioItensAta\/[PDF|EXCEL]", "access": "private", "type": "select" }
     */
    public function relatorioItensAta($tipo='PDF')
    {
        $array = array_slice($this->params['url'], 1);
        $filter = join("&", array_reduce(array_keys($array), function($as, $a) use ($array) {
            $as[] = sprintf('%s=%s', $a, urlencode($array[$a])); return $as;
        }, array()));

        $url = Configure::read('App.config.api.atas.url');
        $token = Configure::read('App.config.api.atas.token');
        $contentUrl = $url . "/api/v1/relatorio_itens/" . $id . "?token=" . $token . "&" . $filter;

        $contents = json_decode(DownloaderComponent::fetch($contentUrl), true);

        $registros = array_map(function ($item) {

            setlocale(LC_MONETARY, 'pt_BR');

            return array('RELATORIO' => array(
                'UASG Gerenciadora' => $item['attributes']['uasg_id'],
                'Nº da Licitação' => $item['attributes']['licitacao_formatada'],
                'Modalidade Licitação' => $item['attributes']['modalidade_nome'],
                'Nº Processo' => $item['attributes']['processo_id'],
                'Fornecedor' => $item['attributes']['fornecedor_nome'],
                'Item' => $item['attributes']['item_nome'],
                'Qtd Licitada Item' => $item['attributes']['quantidade_homologada'],
                'UND Fornecimento' => $item['attributes']['unidade_fornecimento'],
                'Valor Unitário' => money_format('%.2n', $item['attributes']['valor_unitario']),
                'Valor total' => money_format('%.2n', $item['attributes']['valor_total'])
            ));
        }, $contents['data']);

        $nome = 'Relatório de Itens de Ata';

        $tipo === 'EXCEL' ?
            $this->downloadExcel($registros, $nome) :
            $this->gerarRelatorio($registros, $nome);
    }

    public function relatorioConsumoItensAta($tipo = 'PDF')
    {
        $array = array_slice($this->params['url'], 1);
        $filter = join("&", array_reduce(array_keys($array), function($as, $a) use ($array) {
            $as[] = sprintf('%s=%s', $a, urlencode($array[$a])); return $as;
        }, array()));

        $url = Configure::read('App.config.api.atas.url');
        $token = Configure::read('App.config.api.atas.token');
        $contentUrl = $url . "/api/v1/relatorio_consumo/" . $id . "?token=" . $token . "&" . $filter;

        $contents = json_decode(DownloaderComponent::fetch($contentUrl), true);

        $registros = array_map(function ($item) {

            setlocale(LC_MONETARY, 'pt_BR');

            return array('RELATORIO' => array(
                'Nº da Licitação' => $item['attributes']['licitacao_formatada'],
                'Modalidade Licitação' => $item['attributes']['modalidade_nome'],
                'Nº Processo' => $item['attributes']['processo_id'],
                'Fornecedor' => $item['attributes']['fornecedor_nome'],
                'Item' => $item['attributes']['item_nome'],
                'Qtd Consumida' => $item['attributes']['quantidade_consumida'],
                'UND Fornecimento' => $item['attributes']['unidade_fornecimento'],
                'Saldo disponível' => $item['attributes']['quantidade_homologada'] - $item['attributes']['quantidade_consumida'],
                'UASG Consumidora' => $item['attributes']['uasg_id'],
                'Tipo da UASG' => $item['attributes']['tipo_interessado_nome']
            ));
        }, $contents['data']);

        $nome = 'Relatório de Consumo de Itens de Ata';

        $tipo === 'EXCEL' ?
            $this->downloadExcel($registros, $nome) :
            $this->gerarRelatorio($registros, $nome);
    }

    /**
     * Relatório de Fornecedores
     * @resource { "name": "Relatório de Fornecedores", "route":"relatorios\/fornecedores", "access": "private", "type": "select" }
     */
    public function fornecedores() {
        $break = '<br />';

        if ($this->isTipoRelatorioExcel()) {
            $break = '';
        }

        $dados = $this->tratarDadosContrato($arCampos, $this->Relatorio->fornecedores());
        $this->gerarRelatorio($dados, 'Relatório de Fornecedores');
    }


    /**
     * Relatório Atestos por Atividades/Tarefas dos Contratos
     * @item 65
     * @resource { "name": "Relatório Atestos por Atividades/Tarefas dos Contratos", "route":"relatorios\/atestosAtividades", "access": "private", "type": "select" }
     */
    public function atestosAtividades() {
        $this->gerarRelatorio($this->Relatorio->atestosAtividades(), 'Relatório de Atestos das Atividades/Tarefas dos Contratos');
    }

    /**
     *
     * @param  [type] $arCampos  [description]
     * @param  [type] $registros [description]
     * @return [type]            [description]
     */
    protected function tratarDadosContrato($arCampos, $registros)
    {
        foreach($registros as $key => $registro) {
            $key_dados = key($registro);
            $dados = current($registro);
            foreach ($dados as $key_item => $value) {
                // retira espaços duplos
                $dados[$key_item] = preg_replace('!\s+!', " ", $value);
                foreach($arCampos as $index => $method_prop) {
                    $method = key($method_prop);
                    $params = $method_prop[$method];
                    if (isset($dados[$index])
                        && $index == $key_item ) {
                        if (method_exists($this, $method)) {
                            $dados[$key_item] = $this->$method($value, $params);
                        }
                    }
                }
            }
            $registro[$key_dados] = $dados;
            $registros[$key] = $registro;
        }

        return $registros;
    }

    /**
     * Tratamento para aplicação da máscara
     *
     * @return string
     */
    private function _mask($text, $params)
    {
        if (empty($text)) {
            return $text;
        }

        $required = array(
            'mask',
            'tipo'
        );

        if ($this->_validCall($required, $params)) {
            $mask = FunctionsComponent::pegarFormato($params['mask'], $params['tipo']);
            $text = FunctionsComponent::setMask($text, $mask);
        }

        return $text;
    }

    /**
     * Tratamento para quebra de linha de textos.
     *
     * @return string
     */
    private function _wordwrap($text, $params)
    {
        $required = array(
            'width',
            'break',
            'cut'
        );

        if ($this->_validCall($required, $params)) {
            if (strlen($text) > $params['width']) {
                $text = wordwrap($text, $params['width'], $params['break'], $params['cut']);
            }
        }

        return $text;
    }

    /**
     * Valida parâmetros enviados
     *
     * @param  array $required
     * @return bool
     */
    private function _validCall($required, $params)
    {
        foreach ($required as $field) {
            if (!array_key_exists($field, $params)) {
                return false;
            }
        }
        return true;
    }
}

