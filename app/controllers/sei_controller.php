<?php

class SeiController extends AppController
{

    public $name = 'sei';

    public $uses = array('Contrato', 'ProcessoSei');

    public $helpers = array('Sei');

    public $layout = 'iframe';

    function iframe_processos($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    function iframe_andamentos($coContrato)
    {
        $this->set(compact('coContrato'));
    }

    public function list_processos($coContrato)
    {
        $nomeDoCache = "processos-sei-do-contrato-{$coContrato}";
        $processosDoSei = Cache::read($nomeDoCache);
        if (! $processosDoSei) {
            $numerosDosProcessosCadastradosNoGesconParaBuscarDoSei = $this->getListNrProcesso($coContrato);
            $config = Configure::read('App.config.component.sei');
            $integrasSeiClient = \N2oti\Integras\Sei\IntegrasSEI::factory(
                    new \N2oti\Integras\Sei\IntegrasConfigParamenterSEI(
                            $config['params']['integrasConfig'], $config['params']['version']
                    )
            );
            $processosDoSei = array();
            $unidadesDoSei = Cache::read('unidades-do-sei');
            if (! $unidadesDoSei) {
                $unidadesDoSei = $integrasSeiClient->listarUnidades();
                Cache::write('unidades-do-sei', $unidadesDoSei);
            }
            foreach ($numerosDosProcessosCadastradosNoGesconParaBuscarDoSei as $idDoProcessoNoGescon => $numeroDoProcessoDoSei) {
                try {
                    $mensagemDeExcecao = '';
                    foreach ($unidadesDoSei as $unidade) {
                        try {
                            $dadosSei = $integrasSeiClient->consultarProcedimento($numeroDoProcessoDoSei, $unidade->IdUnidade);
                            $dadosSei->achouNoSei = true;
                            break;
                        } catch (Exception $exception) {
                            $mensagemDeExcecao .= $exception->getMessage() . PHP_EOL;
                            if (preg_match('/Erro ao recuperar os dados \[Processo \d+ não encontrado.\]/', $mensagemDeExcecao)) {
                                break;
                            }
                        }
                    }
                    if ($mensagemDeExcecao) {
                        throw new Exception($mensagemDeExcecao);
                    }
                } catch (Exception $exception) {
                    $dadosSei = new stdClass();
                    $dadosSei->achouNoSei = false;
                    $dadosSei->ProcedimentoFormatado = FunctionsComponent::mascara($numeroDoProcessoDoSei, 'processo', 17);
                    $dadosSei->Documentos = array();
                    $dadosSei->mensagemDeErro = $exception->getMessage();
                }
                $dadosSei->id = $idDoProcessoNoGescon;
                $processosDoSei[] = $dadosSei;
            }
            Cache::write($nomeDoCache, $processosDoSei);
        }
        $this->set('processosDoSei', $processosDoSei);
        $this->set('coContrato', $coContrato);
    }

    private function getListNrProcesso($coContrato)
    {
        $contrato = $this->Contrato->findByCoContrato($coContrato);
        $processoSei = $this->ProcessoSei->find('list', array(
                'fields' => 'nu_processo_sei',
                'conditions' => array('co_contrato' => $coContrato)
            )
        );
        return array_unique(array($contrato['Contrato']['nu_processo']) + $processoSei);
    }

    public function add_processo($coContrato)
    {
        if (!empty($this->data)) {
            $this->ProcessoSei->create();
            if ($this->ProcessoSei->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                Cache::delete("processos-sei-do-contrato-{$coContrato}");
            } else {
                foreach ($this->ProcessoSei->validationErrors as $mensagemDeErro) {
                    $this->Session->setFlash($mensagemDeErro);
                }
            }
        }
        $this->redirect(array('action' => 'list_processos', $coContrato));
    }

    public function delete_processo($id, $coContrato)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array('action' => 'list_processos', $coContrato));
        }
        if ($this->ProcessoSei->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            Cache::delete("processos-sei-do-contrato-{$coContrato}");
            $this->redirect(array('action' => 'list_processos', $coContrato));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array('action' => 'list_processos', $coContrato));
    }

}
