<?php
class ServicosController extends AppController
{

    public $name = 'Servicos';

    public $uses = array(
        'Servico'
    );

    public function index()
    {
        $this->Servico->recursive = 0;
        $this->set('servicos', $this->paginate());
    }
    
    function iframe( )
    {
        $this->layout = 'blank';
    }

    function close( $co_servico )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_servico' ) );
    }

    function listar() {

        echo json_encode ( $this->Servico->find ( 'list' ) );

        exit ();
    }

    function add($modal = false) {
        if($modal) {
            $this->layout = 'iframe';
        }
        
        if (! empty($this->data)) {
            $this->Servico->create();
            if ($this->Servico->save($this->data)) {
                if($modal) {
                    $this->redirect ( array ('action' => 'close', $this->Servico->id ) );
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        $this->set ( compact ( 'modal' ) );
    }

    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            $this->data['Servico']['ds_servico'] = ($this->data['Servico']['ds_servico']);
            if ($this->Servico->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Servico->read(null, $id);
        }
        $this->set('id', $id);
    }

    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Servico->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>
