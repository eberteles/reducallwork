<?php
/**
* @resource { "name": "Cadastro de Unidades Administrativas", "route":"setores", "access": "private", "type": "module" }
*/
class SetoresController extends AppController
{

    public $name = 'Setores';

    public $uses = array(
        'Setor'
    );
    
    public $helpers = array(
        'Imprimir'
    );

    public function __construct() {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();
        
        parent::__construct();
    }
    
   /**
    * @resource { "name": "Unidades Administrativas", "route":"setores\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->gerarFiltro();
        
        $findType = ($this->data['Setor']['ds_setor'] == "") ? 'threaded' : 'all';
                
        $criteria = null;
        if (! empty($this->data)) {
                $criteria['ds_setor like'] = '%' . up($this->data['Setor']['ds_setor']) . '%';
        }
        
        $criteria['Setor.ic_ativo'] = 1;
        $setores = $this->Setor->find($findType, array(
            'fields' => array(
                'Setor.*',
                'Usuario.ds_nome',
                'Usuario.ds_email',
            ),
            'conditions' => $criteria
        ));

        $this->set('setors', $setores);
    }

    /**
    * @resource { "name": "Verificar Setor", "route":"setores\/verificaSetor", "access": "private", "type": "select" }
    */
    public function verificaSetor()
    {

        if(!empty($this->params['form']['nome']))
        {
            $nome = $this->params['form']['nome'];
            $result = $this->Setor->find('first', array(
                'conditions' => array(
                    'Setor.ds_setor' => $nome
                ),
                'fields' => array(
                    'Setor.*',
                    'Usuario.co_usuario',
                    'Usuario.ds_nome'
                ) 
            ));
            if(!empty($result))
            {
                echo json_encode($result);
            }
            else
            {
                echo 'false';
            }

        }
        else
        {
            echo "false";
        }
        exit();
    }



    /**
    * @resource { "name": "Setores iframe", "route":"setores\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
    * @resource { "name": "Novo Setor", "route":"setores\/add", "access": "private", "type": "insert" }
    */
    public function add($modal = false)
    {
        $this->set('modal', $modal);
        $setores = $this->Setor->find('threaded', array(
            'fields' => 'Setor.*',
            'order' => 'Setor.ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));
        
        $this->set('setores', $setores);

        if($modal) {
            $this->set('iframeFromSetor', true);
            $this->layout = 'iframe';
        }
        if ($this->RequestHandler->isPost()) {
            $this->Functions->limparMascara($this->data['Setor']['nu_cpf']);
            $setor = $this->Setor->find('all',array(
                'conditions' => array(
                    'Setor.ic_ativo' => 0,
                    'Setor.ds_setor' => $this->data['Setor']['ds_setor']
                ),
                'fields' => array(
                    'Setor.co_setor'
                )
            ));

            // atualizar o setor do usuário com o novo setor que está sendo inserido, caso o usuario selectionado já tenha um setor vinculado
            // $this->loadModel('Usuario');
            $countUsuario = null;
            if (!$modal) {
                $this->Setor->recursive = -1;
                $countUsuario = $this->Setor->find('count', array(
                    'conditions' => array(
                        'Setor.ic_ativo' => 1,
                        'Setor.co_usuario' => $this->data['Setor']['co_usuario']
                    )
                ));
            }

            if( !empty($setor) ){
                $this->reInsert($setor[0]['Setor']['co_setor'], $countUsuario, $this->data['Setor']['co_usuario']);
            }else {
                $this->Setor->create();
                if ($this->Setor->save($this->data)) {

                    $lastCoSetor = $this->Setor->getLastInsertID();
                    if ($countUsuario) {
                        $this->Setor->query("UPDATE setores SET co_usuario=null WHERE co_usuario={$this->data['Setor']['co_usuario']} AND co_setor !={$lastCoSetor}");
                    }

                    if ($modal) {
                        $this->redirect(array('action' => 'close', $this->Setor->id));
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array(
                            'action' => 'index'
                        ));
                    }
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }
        
		$this->loadModel('Usuario');
		$this->Usuario->recursive = -1;
		$this->Usuario->displayField = 'nome_combo';
		$this->Usuario->virtualFields = array(
			'nome_combo' => "CONCAT(Usuario.nu_cpf, ' - ', Usuario.ds_nome)"
		);

		$usuarios = $this->Usuario->find('list', array(
			'conditions' => array('Usuario.ic_ativo', 1)
		));

		$this->set('usuarios', $usuarios);
		$this->set ( compact ( 'modal' ) );
    }
    
    function close( $co_setor )
    {
        $this->layout = 'iframe';
        $this->Session->setFlash(__('Registro salvo com sucesso', true));
        $this->set ( compact ( 'co_setor' ) );
    }
    
    /**
    * @resource { "name": "Listar Setores", "route":"setores\/listar", "access": "private", "type": "select" }
    */
    function listar() {
        App::import('Helper', 'Imprimir');
        $imprimir   = new ImprimirHelper();
        echo json_encode ($imprimir->getArraySetores($this->Setor->find ( 'threaded', array('order' => 'ds_setor ASC') ), 1 ) );

        exit ();
    }

    /**
    * @resource { "name": "Editar Setor", "route":"setores\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        // atualizar o setor do usuário com o novo setor que está sento inserio, caso o usuario selectionado já tenha um setor vinculado
        // $this->loadModel('Usuario');
        $this->Setor->recursive = -1;
        $countUsuario = $this->Setor->find('count', array(
            'conditions' => array(
                'Setor.ic_ativo' => 1,
                'Setor.co_usuario' => $this->data['Setor']['co_usuario']
            )
        ));

        if (! empty($this->data)) {
            if ($this->Setor->save($this->data)) {
                if ($countUsuario) {
                    $this->Setor->query("UPDATE setores SET co_usuario=null WHERE co_usuario={$this->data['Setor']['co_usuario']} AND co_setor !={$this->data['Setor']['co_setor']}");
                }

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->Setor->recursive = 1;
            $this->data = $this->Setor->read(null, $id);
        }

        $this->loadModel('Usuario');
        $this->Usuario->recursive = -1;
        $this->Usuario->displayField = 'nome_combo';
        $this->Usuario->virtualFields = array(
            'nome_combo' => "CONCAT(Usuario.nu_cpf, ' - ', Usuario.ds_nome)"
        );

        $usuarios = $this->Usuario->find('list', array(
            'conditions' => array('Usuario.ic_ativo', 1)
        ));

        $this->set('usuarios', $usuarios);
        $this->set ( compact ( 'modal' ) );
        $this->set('id', $id);
        $setores = $this->Setor->find(
            'threaded', array(
                'fields' => array('Setor.co_setor', 'Setor.parent_id', 'Setor.ds_setor'),
                'conditions' => array(
                    'Setor.ic_ativo' => '1',
                )
        ));

        $coSetorEdit = $this->data['Setor']['co_setor'];
        $setores = array_filter($setores, function ($setor) use($coSetorEdit) {
            return $setor['Setor']['co_setor'] != $coSetorEdit;
        });

        $this->set('setores', $setores);
    }

    /**
    * @resource { "name": "Remover Setor", "route":"setores\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $setor = $this->Setor->find(array('co_setor' => $id));
        }

        if ($this->Setor->delete($id)) {
            // gravar log
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            App::import('Model', 'Log');
            $logModel = new Log();
            $log = array();
            $log['co_usuario'] = $usuario['Usuario']['co_usuario'];
            $log['ds_nome'] = $usuario['Usuario']['ds_nome'];
            $log['ds_email'] = $usuario['Usuario']['ds_email'];
            $log['nu_ip'] = $usuario['IP'];
            $log['dt_log'] = date("Y-m-d H:i:s");
            $log['ds_log'] = json_encode($setor['Setor']);
            $log['ds_tabela'] = 'setores';
            $logModel->save($log);

            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
    * @resource { "name": "Exclusão Lógica", "route":"setores\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Setor->read(null, $id)) {
            $setor = $this->Setor->read(null, $id);
            $msg = null;
            $setor['Setor']['ic_ativo'] = 0;

            $this->loadModel('Contrato');
            $contratos = $this->Contrato->find('all', array(
                'conditions' => array('Contrato.co_contratante' => $id)
            ));

            if(empty($contratos)){
                if($this->Setor->save($setor)){
                    $msg = "Registro excluído com sucesso!";
                }else{
                    $msg = "Houve um erro na exclusão";
                }
            }else{
                $msg = "O registro não pode ser excluído. A unidade está vinculada a um contrato.";
            }


            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    public function reInsert( $id = null, $countUsuario, $coUsuario){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->Setor->read(null, $id)) {
            $setor = $this->Setor->read(null, $id);

            $msg = null;
            $setor['Setor']['ic_ativo'] = 1;

            if($this->Setor->save($setor)){
                $lastCoSetor = $this->Setor->getLastInsertID();
                
                if ($countUsuario) {
                    $this->Usuario->query("UPDATE usuarios SET co_setor={$lastCoSetor} WHERE co_usuario=$coUsuario");
                }

                $msg = "Registro incluído com sucesso!";
            }else{
                $msg = "Houve um erro no cadastro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser incluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Verifica Usuário", "route":"setores\/checkusuario", "access": "private", "type": "select" }
    */
    public function checkusuario($coUsuario, $coSetor=0) {
        // $this->loadModel('Usuario');
        $this->Setor->recursive = -1;
        $countUsuario = $this->Setor->find('count', array(
            'conditions' => array(
                'Setor.ic_ativo' => 1,
                'Setor.co_usuario' => $coUsuario,
                'Setor.co_setor !=' => $coSetor 
            )
        ));
       $valid = true;
        
        if ($countUsuario) {
            $valid = false;
        } 

        echo json_encode(array('valid' => $valid));
        exit;
    }
}
?>
