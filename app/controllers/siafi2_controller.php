<?php
App::import('Vendor', 'siafi', array(
    'file' => 'Siafi.class.php'
));

class Siafi2Controller extends AppController
{

    public $name = 'Siafi2';

    public $uses = null;

    public function index()
    {
        $siafi = new Siafi();
        $this->set('arquivos', $siafi->getArquivos());
        $this->set('tabelas', $siafi->getTabelas());
        $this->set('tudo', $siafi->getEverything());
    }

    public function upload()
    {
        try {
            $arquivo = PATH_ARQUIVOS . $this->data['siafi']['arquivo']['name'];
            move_uploaded_file($this->data['siafi']['arquivo']['tmp_name'], $arquivo);
            $this->Session->setFlash(__('Upload efetuado com sucesso', true));
        } catch (Exception $e) {
            $this->Session->setFlash(__('Erro ao efetuar o upload.', true));
        }
        $this->redirect(array(
            'controller' => 'siafi2',
            'action' => 'index'
        ));
    }

    public function delete($nome, $fromDb = false)
    {
        if (file_exists(PATH_ARQUIVOS . $nome))
            unlink(PATH_ARQUIVOS . $nome);
        
        if (file_exists(PATH_ARQUIVOS . str_replace('TXT', 'REF', $nome)))
            unlink(PATH_ARQUIVOS . str_replace('TXT', 'REF', $nome));
        
        if ($fromDb) {
            $siafi = new Siafi();
            $siafi->removeOccurrences($nome);
        }
        
        $this->redirect(array(
            'controller' => 'siafi2',
            'action' => 'index'
        ));
    }

    public function import($name)
    {
        $siafi = new Siafi();
        $siafi->importArquivo($name);
        $this->redirect(array(
            'controller' => 'siafi2',
            'action' => 'index'
        ));
    }

    public function importAll()
    {
        $layout = new Layout();
        $layout->digArquivos();
        $this->redirect(array(
            'controller' => 'siafi2',
            'action' => 'index'
        ));
    }

    public function detalha($id)
    {
        $siafi = new Siafi();
        $details = $siafi->getDetails($id);
        
        $html = '<div class="acoes-formulario-top clearfix">' . '<p class="requiredLegend pull-left">Detalhes do movimento:</p>' . '<div class="pull-right btn-group">' . '<button onclick="" class="btn btn-small btn-primary">Editar</button>' . '</div>' . '</div>';
        
        $html .= '<table style="margin-left:20px;">';
        foreach ($details as $k => $v) {
            if (trim($v) != '' && $v != '0' && $k != 'id' && $k != 'id_arquivo')
                $html .= '<tr><td style="width:250px;vertical-align:top;"><li> &nbsp; ' . str_replace('_', ' ', $k) . '</td><td style="width:20px;"></td><td><b>' . $v . '</b></td></tr>';
        }
        $html .= '</table>';
        echo $html;
        die();
    }

    public function importaMovimento($tipo)
    {
        $siafi = new Siafi();
        $siafi->importToGescon($tipo);
        die();
    }

    public function deletefile($filename)
    {
        $filename = explode('xoxo', $filename);
        if($filename[0] != '' || $filename[0] != null){
            $filePath = PATH_ARQUIVOS .  $filename[0];
            @unlink($filePath . '.REF');
            @unlink($filePath . '.TXT');
            @unlink($filePath . '.ref');
            @unlink($filePath . '.txt');
        }
    }
}

?>