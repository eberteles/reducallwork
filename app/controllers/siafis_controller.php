<?php
App::import('Controller', 'LeitorArquivos');

class SiafisController extends AppController
{

    var $name = 'Siafi';

    var $uses = array(
        'Siafi',
        'Critica',
        'Empenho',
        'Arquivo',
        'OrdemBancaria'
    );

    public function index()
    {
        $empenhos = $this->Empenho->find('all', array(
            'conditions' => array(
                'ds_fonte_recurso is not null'
            )
        ));
        $obs = $this->OrdemBancaria->find('all', array(
            'limit' => 10
        ));
        
        $arquivos = $this->Arquivo->find('list', array(
            'fields' => array(
                'co_layout',
                'ds_layout'
            )
        ));
        
        $this->set("ordembancarias", $obs);
        $this->set("empenhos", $empenhos);
        $this->set("arquivos", $arquivos);
    }

    public function consulta()
    {
        $movimentos = $this->Siafi->find('all', array(
            'fields' => array(
                'co_movimento',
                'ds_nome_arquivo',
                'dt_inicio',
                'dt_fim',
                'cs_sucesso'
            )
        ));
        $criticas = $this->Critica->find('all');
        
        $this->set("movimentos", $movimentos);
        $this->set("criticas", $criticas);
    }

    public function upload()
    {
        try {
            $arquivo = ROOT . '/arquivos/' . $this->data['siafi']['arquivo']['name'];
            move_uploaded_file($this->data['siafi']['arquivo']['tmp_name'], $arquivo);
            
            $this->associaLayoutArquivo($arquivo, $this->data['siafi']['layout']);
            
            $this->Session->setFlash(__('Upload efetuado com sucesso', true));
            // TODO: verificar se é necessário utilizar o shell (OS responsável pela concorrência)
            // shell_exec("cake leitor");
            LeitorArquivosController::processar();
        } catch (Exception $e) {
            $this->Session->setFlash(__('Erro ao efetuar o upload.', true));
        }
        $this->redirect(array(
            'controller' => 'siafis',
            'action' => 'index'
        ));
    }

    /**
     * Adiciona o nome do layout na primeira coluna de cada linha do arquivo
     *
     * @param [type] $file
     *            [description]
     * @param [type] $layout
     *            [description]
     * @return [type] [description]
     */
    private function associaLayoutArquivo($file, $layout)
    {
        $arquivo = fopen($file, 'r');
        if (filesize($file) <= 0) {
            fclose($arquivo);
            return;
        }
        
        $newfilename = $file . '.tmp';
        $newfile = fopen($newfilename, 'w+');
        
        // le arquivo
        while (! feof($arquivo)) {
            $line = fgets($arquivo);
            $trimLine = trim($line);
            
            // associa layout se linha nao estiver vazia
            if (! empty($trimLine)) {
                $line = $layout . $line;
                fwrite($newfile, $line);
            }
        }
        
        fclose($arquivo);
        fclose($newfile);
        
        unlink($file);
        
        rename($newfilename, $file);
    }
}