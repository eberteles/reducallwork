<?php
App::import('Vendor', 'siasg', array(
    'file' => 'Siasg.class.php'
));

/**
 * @resource { "name" : "Comprasnet", "route":"siasg", "access": "private", "type": "module" }
 */
class SiasgController extends AppController
{

    public $name = 'Siasg';

    public $uses = array(
        'Contrato',
        'Empenho',
        'Aditivo',
        'Modalidade'
    );

    /**
     * @resource { "name" : "Comprasnet Principal", "route":"siasg\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        App::import('Model', 'Uasg');
        $uasgModel = new Uasg();
        $uasgs = $uasgModel->find('all');

        $this->set('uasgs', $uasgs);

        $contratos = $this->Contrato->getNumerosContratos();
        $js = '';
        if(count($contratos) > 0){
            foreach ($contratos as $c) {
                $y = substr($c, - 4);
                $n = substr($c, 0, - 4);
                while (strlen($n) < 5)
                    $n = '0' . $n;
                $js .= "'" . $n . "/" . $y . "',";
            }
            $aditivos = $this->Aditivo->getNumerosAditivos();
            if(count($aditivos) > 0){
                $this->set('aditivos', json_encode($aditivos));
            } else {
                $this->set('aditivos', json_encode(array()));
            }
        }

        if ($js != '')
            $js = substr($js, 0, - 1);
        $this->set('contratos', $js);

        $empenhos = $this->Empenho->getNumerosEmpenhos();
        $js = '';
        if(count($empenhos) > 0){
            foreach ($empenhos as $c)
                $js .= "'" . $c . "',";
        }
        if ($js != '')
            $js = substr($js, 0, - 1);


        $this->set('empenhos', $js);
        $this->set('countEmpenhos', count($empenhos));
    }

    /**
     * @resource { "name" : "Comprasnet Pesquisa", "route":"siasg\/pesquisa", "access": "private", "type": "select" }
     */
    public function pesquisa($uasg = null)
    {
        $params = explode('_', $uasg);

        if($params[0] == 'undefined'){
            $ano = date('Y');
        } else {
            $ano = $params[0];
        }

        $modulo = new ModuloHelper();
        $urlRobo = Configure::read('App.config.resource.urls.roboComprasnet');

        if (! $modulo->uasg && ! $params[1])
            die();

        $url = $urlRobo . '?fromdb=1&uasg=' . ($params[1]) . '&ano='. $ano;

        $b = file_get_contents($url);
		$b = preg_replace("/&#?[a-z0-9]+;/i","",$b);
        echo '[' . str_replace('}]{', '}],{', $b) . ']';
        die();
    }

    /**
     * @resource { "name" : "Comprasnet Importar", "route":"siasg\/importar", "access": "private", "type": "insert" }
     */
    public function importar()
    {
        $usuario = $this->Session->read('usuario');
        $usuarioId = $usuario['Usuario']['co_usuario'];

        $siasg = new Siasg();

        $json = json_decode($this->params['form']['json']);
        $contratos = array();
        $empenhos = array();
        $aditivos = array();

        $empenhosAdicionados = array();
        foreach ($json as $contrato) {
            // cadastra modalidade caso não tenha cadastrada
            $siasg->cadastraModalidade(array('ds_modalidade' => ($contrato->modalidade)));
            $modalidade = new Modalidade();
            $modalidadeObj = $modalidade->getByDescricao(($contrato->modalidade));

            // cadastra contratacao caso não tenha cadastrada
            $siasg->cadastraContratacao(array('ds_contratacao' => ($contrato->licitacao_modalidade)));
            $contratacao = new Contratacao();
            $contratacaoObj = $contratacao->getByDescricao($contrato->licitacao_modalidade);

            $nuContrato = str_replace("/", "", $contrato->modalidade_numero);
            $forDb = array(
                "uasg" => $contrato->uasg,
                "nu_contrato" => $nuContrato,
                "nu_processo" => preg_replace("/[a-zA-Z]/", "", str_replace("/", "", str_replace("-", "", $contrato->numero_processo))),
                "nu_licitacao" => str_replace("/", "", $contrato->licitacao_numero),
                "dt_publicacao" => $this->fixDate($contrato->data_publicacao),
                "ds_objeto" => $this->cleanStr($contrato->objeto),
                "ds_fundamento_legal" => $this->cleanStr($contrato->fundamento_legal),
                "tp_valor" => "F", // Fixo
                "vl_global" => $contrato->valor_total,
                // TODO: verificar se esse deve ser o valor inicial MESMO
                "vl_inicial" => $contrato->valor_total,
                "dt_ini_vigencia" => $this->fixDate($contrato->vigencia_de),
                "dt_fim_vigencia" => $this->fixDate($contrato->vigencia_ate),
                "dt_fim_vigencia_inicio" => $this->fixDate($contrato->vigencia_ate),
                "dt_assinatura" => $this->fixDate($contrato->data_assinatura),
                "co_usuario" => $usuarioId,
                "co_modalidade" => $modalidadeObj[0]['modalidades']['co_modalidade'],
                "co_contratacao" => $contratacaoObj[0]['contratacoes']['co_contratacao']
            );

            $cnpj = preg_replace('/\D+/', '', $contrato->contratado_cpf_cnpj);
            $data = array(
                'nu_cnpj' => $cnpj,
                'no_razao_social' => $contrato->contratado,
                'tp_fornecedor' => strlen($cnpj) > 11 ? 'j' : 'F'
            );
            // cadastra caso não esteja cadastrado
            $siasg->cadastraFornecedor($data);
            $fornecedores = $siasg->getFornecedores();
            // vincula fornecedor ao contrato
            if ($x = $this->findKeyNeedle($cnpj, $fornecedores))
                $forDb['co_fornecedor'] = $x;
            set_time_limit(30);
            $c = $this->Contrato->insertOrUpdate($forDb);
            $contratos[] = $c;

            $contratoFromDB = $this->Contrato->find(
                array(
                    'Contrato.nu_contrato' => $nuContrato,
                    'Contrato.co_modalidade' => $modalidadeObj[0]['modalidades']['co_modalidade'],
                    'Contrato.uasg' => $contrato->uasg
                )
            );

            if (isset($contrato->empenhos)) {

                foreach ($contrato->empenhos as $empenho) {
                    if(!in_array($empenho->empenho, $empenhosAdicionados)){
                        array_push($empenhosAdicionados, $empenho->empenho);

                        $criteria = array(
                        	'Empenho.nu_empenho' => $empenho->empenho,
                        	'Empenho.tp_empenho' => 'O'
                        );

                        $tpEmpenho = 'O';

                        $empenhoExists = $this->Empenho->exists($criteria);

                        if( $empenhoExists ){
                        	$tpEmpenho = 'R';
                        }

                        $forDb = array(
                            "co_contrato" => $contratoFromDB['Contrato']['co_contrato'],
                            "nu_empenho" => $empenho->empenho,
                            "nu_ptres" => $empenho->programa,
                        	"tp_empenho" => $tpEmpenho,
                            "ds_empenho" => "Empenho Originário",
                            "vl_empenho" => 0,
                            "dt_empenho" => null, // ??
                            "co_usuario" => $usuarioId
                        );
                        set_time_limit(30);
                        $e = $this->Empenho->insertOrUpdate($forDb);
                        $empenhos[] = $e;
                    }
                }
            }

            if (isset($contrato->aditivos)) {
                foreach ($contrato->aditivos as $aditivo) {
                    $modalidade = $this->Modalidade->find(
                        array(
                            'Modalidade.ds_modalidade' => $aditivo->modalidade_contrato
                        )
                    );
                    $contratoFromDB = $this->Contrato->find(
                        array(
                            'Contrato.nu_contrato' => str_replace('/', '', $aditivo->contrato_modalidade_numero),
                            'Contrato.co_modalidade' => $modalidade['Modalidade']['co_modalidade'],
                            'Contrato.uasg' => $contrato->uasg
                        )
                    );
                    if(($contrato->modalidade == $aditivo->modalidade_contrato) && (count($contratoFromDB['Contrato']) > 0) ){
                        $forDb = array(
                            "co_contrato" => $contratoFromDB['Contrato']['co_contrato'],
                            "co_usuario" => $usuarioId,
                            "no_aditivo" => $aditivo->termo_aditivo,
                            "vl_aditivo" => $aditivo->valor_total,
                            "dt_assinatura" => $aditivo->data_assinatura,
                            "dt_prazo" => $aditivo->vigencia_ate,
                            "dt_aditivo" => $aditivo->vigencia_de,
                            "dt_vigencia_de" => $aditivo->vigencia_de,
                            "dt_publicacao" => $aditivo->data_publicacao,
                            "dt_fim_vigencia" => $aditivo->vigencia_ate,
                            "dt_fim_vigencia_anterior" => $aditivo->vigencia_ate,
                            "ds_aditivo" => $this->cleanStr($aditivo->objeto),
                            "ds_fundamento_legal" => $this->cleanStr($aditivo->fundamento_legal),
                            "tp_aditivo" => 3
                        );
                        set_time_limit(30);
                        $a = $this->Aditivo->insertOrUpdate($forDb);
                        if(($aditivo->vigencia_ate != '') && ($aditivo->vigencia_ate != null) && ($contratoFromDB)){
                            $contratoFromDB['Contrato']["dt_publicacao"] = $this->fixDate($contrato->data_publicacao);
                            $contratoFromDB['Contrato']["dt_ini_vigencia"] = $this->fixDate($contrato->vigencia_de);
                            $contratoFromDB['Contrato']['dt_fim_vigencia'] = $aditivo->vigencia_ate;
                            $contratoFromDB['Contrato']["dt_fim_vigencia_inicio"] = $this->fixDate($contrato->vigencia_ate);
                            $contratoFromDB['Contrato']["dt_assinatura"] = $this->fixDate($contrato->data_assinatura);
                            $this->Contrato->insertOrUpdate(array_filter($contratoFromDB['Contrato']));
                        }
                        $aditivos[] = $a;
                    }
                }
            }
        }

        $this->redirect(array(
            'controller' => 'siasg',
            'action' => 'index'
        ));
    }

    /**
     * @resource { "name" : "Comprasnet Atualizar", "route":"siasg\/atualizar", "access": "private", "type": "insert" }
     */
    public function atualizar($retroativoOriginal = 30){
        App::import('Model', 'Uasg');
        $uasgModel  = new Uasg();
        $uasgs =  $uasgModel->find('all');

        $urlRobo = Configure::read('App.config.resource.urls.roboComprasnet'); // Url do robo configurada na classe cliente

        if(count($uasgs) == 0)
            die;

        foreach ($uasgs as $uasg) {
            try {
                $this->atualizarRobo($uasg, $urlRobo, $retroativoOriginal);
            } catch(Exception $e) {
                var_dump($e->getMessage());die;
            }
        }

        die();
    }

    public function atualizarRobo($uasg, $retroativo = 0, $sisg=null) {

    	$urlRobo = Configure::read('App.config.resource.urls.roboComprasnet');

		try {
			$url = $urlRobo . '?updatedb=1&uasg=' . $uasg['Uasg']['uasg'] . '&retroativo=' . $retroativo;
			file_get_contents($url);
		} catch(Exception $e) {}

		set_time_limit(120);

        return true;
    }

    public function fixDate($dt)
    {
        $data = explode('/', $dt);
        if(isset($data)){
            if(count($data) > 2){
                if(strlen($data[2]) == 4){
                    return $data[2] . "-" . $data[1] . "-" . $data[0];
                } else {
                    return null;
                }
            }
        }
    }

    public function fixDateForReal($dt)
    {
        $dataExploded = explode('-', $dt);
        $dataForReal = $dataExploded[2] . '-' . $dataExploded[1] . '-' . $dataExploded[0];
        return $dataForReal;
    }

    public function cleanStr($s)
    {
        return str_replace("'", "", str_replace('"', "", ($s)));
    }

    private function findKeyNeedle($value, $array)
    {
        foreach ($array as $k => $v)
            if ($value == $v)
                return $k;
        return null;
    }
}

?>
