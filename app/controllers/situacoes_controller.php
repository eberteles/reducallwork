<?php
/**
* @resource { "name": "Cadastro de Situações do Contrato", "route":"situacoes", "access": "private", "type": "module" }
*/
class SituacoesController extends AppController {
    
    public $name = 'Situacoes';

    public $uses = array(
        'Situacao'
    );

    /**
    * @resource { "name": "Situações do Contrato", "route":"situacoes\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->Situacao->recursive = 0;
        $criteria['ic_ativo'] = 1;
        $this->set('situacoes', $this->paginate($criteria));
    }

    /**
    * @resource { "name": "iframe", "route":"situacoes\/iframe", "access": "private", "type": "select" }
    */
    function iframe( )
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name": "Close iframe", "route":"situacoes\/close", "access": "private", "type": "select" }
     */
    function close( $co_situacao )
    {
        $this->layout = 'iframe';
        $this->set ( compact ( 'co_situacao' ) );
    }

    /**
    * @resource { "name": "Retorno JSON das Situações", "route":"situacoes\/listar", "access": "private", "type": "select" }
    */
    function listar() {

        echo json_encode ( $this->Situacao->find ( 'list', array(
            'conditions' => array(
                'Situacao.ic_ativo' => 1,
                'Situacao.tp_situacao = "C"'
            )
        ) ) );

        exit ();
    }

    /**
    * @resource { "name": "Nova Situação", "route":"situacoes\/add", "access": "private", "type": "insert" }
    */
    function add($modal = false) {
            if($modal) {
                $this->layout = 'iframe';
            }
            if (!empty($this->data)) {
                $situacao = $this->Situacao->findByDsSituacao($this->data['Situacao']['ds_situacao']);
                if(!empty($situacao)) {
                    $this->activateSituation($situacao);
            	} else {
                    $this->Situacao->create();
                    if ($this->Situacao->save($this->data)) {
                        if($modal) {
                            $this->redirect ( array ('action' => 'close', $this->Situacao->id ) );
                        } else {
                            $this->Session->setFlash(__('Registro salvo com sucesso', true));
                            $this->redirect(array('action' => 'index'));
                        }
                    } else {
                        $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                    }
                }
            }
            $this->set ( compact ( 'modal' ) );
    }
    
    /**
    * @resource { "name": "Editar Situação", "route":"situacoes\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Situacao->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Situacao->read(null, $id);
        }
        $this->set('id', $id);
    }

    /**
    * @resource { "name": "Remover Situação", "route":"situacoes\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $situacao = $this->Situacao->find(array('co_situacaoo' => $id));
        }

        if ($this->Situacao->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }

    /**
    * @resource { "name": "Exclusão Lógica", "route":"situacoes\/logicDelete", "access": "private", "type": "delete" }
    */
    public function logicDelete( $id = null ){
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ( $id ) {
            $this->Situacao->id = $id;
            $msg = null;
            $situacao['Situacao']['ic_ativo'] = 0;

            if($this->Situacao->saveField('ic_ativo',0)){
                $msg = "Situação Excluída com sucesso!";
            }else{
                $msg = "Houve um erro na exclusão";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser bloqueado. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    /**
    * @resource { "name": "Lista Situações", "route":"situacoes\/listaSituacoes", "access": "private", "type": "select" }
    */
    public function listaSituacoes($tpSituacao)
    {
        $this->layout = 'ajax';
        
        $this->Situacao->recursive = 0;
        
        $conditions = null;
        $conditions['Situacao.tp_situacao'] = $tpSituacao;
        $conditions['Situacao.ic_ativo'] = 1;

        $this->set('situacoes', $this->paginate($conditions));
    }

    /**
    * @resource { "name": "Ativa Situação", "route":"situacoes\/activateSituation", "access": "private", "type": "insert" }
    */
    public function activateSituation($situation){
        
    	if( $situation['Situacao']['ic_ativo'] == 1 ) {
    		$msg = "Situação já existente!";
    	} else {    	
    		$situation['Situacao']['ic_ativo'] = 1;
		
	        if($this->Situacao->save($situation)){
	            $msg = "Situação cadastrada com sucesso!";
	        }else{
	            $msg = "Houve um erro no cadastro";
	        }
    	}

        $this->Session->setFlash(__($msg, true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>
