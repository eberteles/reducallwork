<?php

/**
 * @resource { "name": "Cadastro de Sub Categorias de Contratos", "route":"subcategorias", "access": "private", "type": "module" }
 */
class SubcategoriasController extends AppController
{

    public $name = 'Subcategorias';

    /**
     * @resource {
     * "name": "Sub Categorias de Contratos",
     * "route":"subcategorias\/index",
     * "access": "private",
     * "type": "select"
     * }
     */
    public function index()
    {
        // $this->Subcategoria->recursive = 0;
//        $subcategorias = $this->paginate();
        $criteria['Subcategoria.ic_ativo'] = '2';

        $subcategorias = $this->paginate($criteria);
        $categorias = $this->Subcategoria->Categoria->find('list', array(
            'conditions' => array()
        ));


        $this->set('subcategorias', $subcategorias);
        $this->set('categorias', $categorias);
    }

    /**
     * @resource {
     * "name": "iframe",
     * "route":"subcategorias\/iframe",
     * "access": "private",
     * "type": "select"
     * }
     */
    function iframe()
    {
        $this->layout = 'blank';
    }

    /**
     * @resource {
     * "name": "Close iframe",
     * "route":"subcategorias\/close",
     * "access": "private",
     * "type": "select"
     * }
     */
    function close($co_subcategoria, $co_categoria)
    {
        $this->layout = 'iframe';
        $this->set(compact('co_subcategoria'));
        $this->set(compact('co_categoria'));
        $this->Session->setFlash(__('Registro salvo com sucesso', true)); 
        $this->redirect(array('action' => 'index')); 
    }

    /**
     * @resource {
     * "name": "Nova Subcategoria",
     * "route":"subcategorias\/add",
     * "access": "private",
     * "type": "insert"
     * }
     */
    function add($modal = false)
    {
        if ($modal) {
            $this->layout = 'iframe';
        }

        if (!empty ($this->data)) {
            $subcategoria = $this->Subcategoria->find('first', array(
                'conditions' => array(
                    'Subcategoria.ic_ativo' => 1,
                    'UPPER(Subcategoria.no_subcategoria)' => strtoupper($this->data['Subcategoria']['no_subcategoria'])
                )
            ));

            if (!empty($subcategoria)) {
                $this->data['Subcategoria']['co_subcategoria'] = $subcategoria['Subcategoria']['co_subcategoria'];
                $this->data['Subcategoria']['ic_ativo'] = 2;

                if ($this->Subcategoria->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            } else {
                $this->data['Subcategoria']['ic_ativo'] = 2;
                $this->Subcategoria->create();
                if ($this->Subcategoria->save($this->data)) {
                    if ($modal) {
                        $this->redirect(array('action' => 'close', $this->Subcategoria->id, $this->data['Subcategoria']['co_categoria']));
                    } else {
                        $this->Session->setFlash(__('Registro salvo com sucesso', true));
                        $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }
        $this->set('categorias', $this->Subcategoria->Categoria->find('list', array(
            'conditions' => array(
                'Categoria.ic_ativo' => 2
            )
        )));
        $this->set(compact('modal'));
    }

    /**
     * @resource {
     * "name": "Editar Subcategoria",
     * "route":"subcategorias\/edit",
     * "access": "private",
     * "type": "update"
     * }
     */
    public function edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (!empty($this->data)) {
            $subcategoria = $this->Subcategoria->find('first', array(
                'conditions' => array(
                    'Subcategoria.ic_ativo' => 1,
                    'UPPER(Subcategoria.no_subcategoria)' => strtoupper($this->data['Subcategoria']['no_subcategoria'])
                )
            ));

            if (!empty($subcategoria)) {
                $this->data['Subcategoria']['co_subcategoria'] = $subcategoria['Subcategoria']['co_subcategoria'];
                $this->data['Subcategoria']['ic_ativo'] = 2;

                if ($this->Subcategoria->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            } else {
                if ($this->Subcategoria->save($this->data)) {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
                }
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Subcategoria->read(null, $id);
        }
        $this->set('id', $id);
        $this->set('categorias', $this->Subcategoria->Categoria->find('list'));
    }

    /**
     * @resource { "name": "Remover Subcategoria", "route":"subcategorias\/delete", "access": "private", "type": "delete" }
     */
    public function delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido.', true));
            $this->redirect(array('action' => 'index'));
        }

        if ($this->Subcategoria->read(null, $id)) {
            $msg = null;

            $this->Subcategoria->id = $id;

            if ($this->Subcategoria->saveField('ic_ativo', 1)) {
                $msg = "Registro deletado com sucesso.";
            } else {
                $msg = "Não foi possível deletar o registro.";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash(__('Erro ao excluir registro.', true));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * @resource { "name": "Listar Subcategoria", "route":"subcategorias\/listar", "access": "private", "type": "select" }
     */
    public function listar($co_categoria)
    {
        $this->Subcategoria->recursive = 0;

        echo json_encode($this->Subcategoria->find('list', array(
            'conditions' => array(
                'co_categoria' => $co_categoria
            )
        )));

        exit();
    }

    /**
     * @resource { "name": "Verificar Nome", "route":"subcategorias\/checar_nome", "access": "private", "type": "select" }
     */
    function checar_nome($nome = null)
    {
        $count = $this->Subcategoria->findByName($nome, 1);

        echo json_encode(array(
            'status' => $count
        ));
        exit;
    }
}

?>
