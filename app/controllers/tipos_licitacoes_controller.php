<?php

class TiposLicitacoesController extends AppController
{

    var $name = 'TiposLicitacoes';
    var $uses = array('TipoLicitacao');
    function __constructor() {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();

        return parent::construct();
    }

    public function index() {
        $this->TipoLicitacao->recursive = 0;
        $tipoLicitacao = $this->TipoLicitacao->find('all', array(
            'conditions' => array('TipoLicitacao.ic_ativo' => 1 ),
            $this->paginate()
        ));

        $this->set ('tipoLicitacao', $tipoLicitacao);

    }

    public function add(){

        if(! empty ( $this->data)){
            $this->data["TipoLicitacao"]["ic_ativo"] = 1;
            $this->TipoLicitacao->create();

            if ($this->TipoLicitacao->save($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));

            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }

        }


    }

    public function edit($id = null){
        if(! empty ( $this->data)){

            $this->TipoLicitacao->create();

            if ($this->TipoLicitacao->save($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));

            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }

        }
        if (empty($this->data)) {
            $this->data = $this->TipoLicitacao->read(null, $id);
        }
        $this->set(compact('id'));
    }

    function logicDelete($id = null){

        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        if ($this->TipoLicitacao->read(null, $id)) {
            $tipoLicitacao = $this->TipoLicitacao->read(null, $id);
            $msg = null;

            $this->TipoLicitacao->id = $tipoLicitacao['TipoLicitacao']['co_tipo_licitacao'];
            $this->TipoLicitacao->query(
                "UPDATE tipos_licitacoes SET ic_ativo = 0 WHERE co_tipo_licitacao = {$id}"
            );

            if($this->TipoLicitacao->saveField('ic_ativo', 0)) {
                $msg = "Registro deletado com sucesso!";
            }else{
                $msg = "Não foi possível deletar o registro";
            }

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));
        } else {
            $this->Session->setFlash(__('O registro não pode ser excluído. Por favor, tente novamente.', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }


    }


}
?>
