<?php
/**
* @resource { "name": "UASG", "route":"uasg", "access": "private", "type": "module" }
*/
class UasgController extends AppController
{

    public $name = 'Uasg';

    public $uses = array(
        'Uasg'
    );
    
    /**
    * @resource { "name": "Listagem Padrão", "route":"uasg\/index", "access": "private", "type": "select" }
    */
    public function index() {
        $this->set('uasgs', $this->Uasg->find('all'));
    }

    /**
    * @resource { "name": "Remover Uasg", "route":"uasg\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null) {
        $this->Uasg->deleteAll(array('co_uasg' => $id));
        $this->Session->setFlash(__('Registro excluído com sucesso', true));
        $this->redirect(array('action' => 'index'));
    }

    /**
    * @resource { "name": "Nova Uasg", "route":"uasg\/add", "access": "private", "type": "insert" }
    */
    public function add() {
        if (! empty($this->data)) {
            if($this->Uasg->save($this->data)){
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash($this->Uasg->validationErrors['uasg']);
                $this->redirect(array('action' => 'add'));
            }

        }
    }
}
?>