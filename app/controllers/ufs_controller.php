<?php
/**
* @resource { "name": "UFs", "route":"ufs", "access": "private", "type": "module" }
*/
class UfsController extends AppController
{

    public $name = 'Ufs';

    /**
    * @resource { "name": "Listagem Padrão", "route":"ufs\/index", "access": "private", "type": "select" }
    */
    public function index()
    {
        $this->Uf->recursive = 0;
        $this->set('ufs', $this->paginate());
    }

    /**
    * @resource { "name": "Nova UF", "route":"ufs\/add", "access": "private", "type": "insert" }
    */
    public function add()
    {
        if (! empty($this->data)) {
            $this->Uf->create();
            if ($this->Uf->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
    }

    /**
    * @resource { "name": "Editar UF", "route":"ufs\/edit", "access": "private", "type": "update" }
    */
    public function edit($id = null)
    {
        if (! $id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if (! empty($this->data)) {
            if ($this->Uf->save($this->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso', true));
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Uf->read(null, $id);
        }
    }

    /**
    * @resource { "name": "Remover UF", "route":"ufs\/delete", "access": "private", "type": "delete" }
    */
    public function delete($id = null)
    {
        if (! $id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($this->Uf->delete($id)) {
            $this->Session->setFlash(__('Registro excluído com sucesso', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        $this->Session->setFlash(__('Erro ao excluir registro', true));
        $this->redirect(array(
            'action' => 'index'
        ));
    }
}
?>