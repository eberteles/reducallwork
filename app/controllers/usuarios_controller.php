<?php

/**
 * @resource { "name": "Cadastro de Usuários", "route":"usuarios", "access": "private", "type": "module" }
 */
class UsuariosController extends AppController
{

    public $name = 'Usuarios';

    public $helpers = array(
        'Imprimir', 'Util'
    );

    public $components = array('Auth', 'LdapAuth', 'LdapSearch');

    public function __construct()
    {
        App::import('Helper', 'Modulo');
        App::import('Helper', 'Util');
        $this->modulo = new ModuloHelper();
        $this->Util = new UtilHelper();

        parent::__construct();
    }

    private function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;
        if ($maiusculas)
            $caracteres .= $lmai;
        if ($numeros)
            $caracteres .= $num;
        if ($simbolos)
            $caracteres .= $simb;

        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }
        return $retorno;
    }

    private function getPermissao($permissao)
    {
        if (isset($this->data['Usuario'][$permissao])) {
            return $this->data['Usuario'][$permissao];
        } else {
            return "0";
        }
    }

    private function prepararCampos()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $this->Functions->limparMascara($this->data['Usuario']['nu_cpf']);
        if (!empty($this->data['Usuario']['ds_senha'])) {
            $this->data['Usuario']['ds_senha'] = md5($this->data['Usuario']['ds_senha']);
            $this->data['Usuario']['ds_confirma_senha'] = md5($this->data['Usuario']['ds_confirma_senha']);
        } elseif (Configure::read('App.config.component.ldap.enabled')) {
            $this->data['Usuario']['ds_senha'] = md5($modulo->defaultPassword);
            $this->data['Usuario']['ds_confirma_senha'] = md5($modulo->defaultPassword);
        }

    }


    /**
     * @resource { "name": "Usuários", "route":"usuarios\/index", "access": "private", "type": "select" }
     */
    public function index()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        $this->gerarFiltro();
        $this->Usuario->recursive = 2;
        $this->Usuario->validate = array();
        $criteria = null;
        $situacao = null; // Ativo

        if (!empty($this->data) && $this->RequestHandler->isPost() || $this->params['named']['page'] != null) {

            if (isset($this->data['Usuario']['co_setor']) && $this->data['Usuario']['co_setor'] > 0) {
                $criteria['Usuario.co_setor'] = $this->data['Usuario']['co_setor'];
            }
            if (isset($this->data['UsuarioPerfil']['co_perfil']) && $this->data['UsuarioPerfil']['co_perfil'] > 0) {
                $criteria['UsuarioPerfil.co_perfil'] = $this->data['UsuarioPerfil']['co_perfil'];
            }
            if ($this->data['Usuario']['no_usuario']) {
                $criteria['no_usuario like'] = '%' . up($this->data['Usuario']['no_usuario']) . '%';
            }
            if ($this->data['Usuario']['nu_cpf']) {
                $criteria['Usuario.nu_cpf'] = FunctionsComponent::limparMascara($this->data['Usuario']['nu_cpf']);
            }
            if (isset($this->data['Usuario']['ic_ativo']) && $this->data['Usuario']['ic_ativo'] != '') {
                $criteria['Usuario.ic_ativo'] = $this->data['Usuario']['ic_ativo'];
                $situacao = $this->data['Usuario']['ic_ativo'];
            }
        }else{
            $this->data['Usuario'] = array();
        }

        $conditions = array(
            array(
                'Usuario.ic_acesso' => 1
            )
        );

        if (!is_null($situacao)) {
            $conditions['Usuario.ic_ativo'] = $situacao;
        }

        $this->paginate = array(
            'limit' => 10,
            'conditions' => $conditions
        );

        $this->set('setores', $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC'
        )));

        unset($this->Usuario->UsuarioPerfil->validate['co_perfil']);


        $this->set('perfis', $this->Usuario->UsuarioPerfil->Perfil->find('list', array(
            'fields' => array(
                'Perfil.co_perfil',
                'Perfil.no_perfil'
            )
        )));
        $this->set('situacoes', array(
            0 => 'Inativo',
            1 => 'Ativo'
        ));

        $this->set('usuarios', $this->paginate($criteria));
        $this->set('total', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.ic_acesso' => 1,
                    'Usuario.licenca_nominal' => 1
                )
            )
        )));

        $this->set('totalConcorrente', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.ic_acesso' => 1,
                    'Usuario.licenca_concorrente' => 1
                )
            )
        )));

        $this->set('totalBI', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.ic_acesso' => 1,
                    'Usuario.licenca_bi' => 1
                )
            )
        )));

        $this->set('concorrentesOnline', $this->contarLicencasConcorrentesDisponiveis());

    }

    /**
     * @resource { "name": "iframe", "route":"usuarios\/iframe", "access": "private", "type": "select" }
     */
    function iframe()
    {
        $this->layout = 'blank';
    }

    /**
     * @resource { "name": "Close iframe", "route":"usuarios\/close", "access": "private", "type": "select" }
     */
    function close($co_usuario)
    {
        $this->layout = 'iframe';
        $this->set(compact('co_usuario'));
    }

    /**
     * @resource { "name": "Retorno JSON de Usuários", "route":"usuarios\/listar", "access": "private", "type": "select" }
     */
    function listar()
    {
        $this->Usuario->displayField = 'nome_combo';
        $this->Usuario->virtualFields = array(
            'nome_combo' => "CONCAT(Usuario.nu_cpf, ' - ', Usuario.ds_nome)"
        );

        echo json_encode($this->Usuario->find('list', array(
            'conditions' => array(
                'UsuarioPerfil.co_perfil' => 2
            ),
            'recursive' => 3
        )));
        exit ();
    }

    /**
     * @resource { "name": "Novo Usuário", "route":"usuarios\/add", "access": "private", "type": "insert" }
     */
    public function add($modal = false)
    {
        if ($modal) {
            $this->layout = 'iframe';
        }

        $this->loadModel('Projeto');
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (!empty($this->data)) {
            if (isset($this->data['Usuario']['tipo_licenca'])) {
                if ($this->data['Usuario']['tipo_licenca'] == 'nominal') {
                    $this->data['Usuario']['licenca_nominal'] = '1';
                    $this->data['Usuario']['licenca_concorrente'] = null;
                    unset($this->data['Usuario']['tipo_licenca']);
                } elseif ($this->data['Usuario']['tipo_licenca'] == 'concorrente') {
                    $this->data['Usuario']['licenca_nominal'] = null;
                    $this->data['Usuario']['licenca_concorrente'] = '1';
                    unset($this->data['Usuario']['tipo_licenca']);
                }
            } else {
                $this->data['Usuario']['licenca_nominal'] = '1';
                $this->data['Usuario']['licenca_concorrente'] = null;
                unset($this->data['Usuario']['tipo_licenca']);
            }
            $this->Usuario->create();
            $this->prepararCampos();

            if ($this->Usuario->saveAll($this->data)) {
                if ($modal) {
                    $this->redirect(array(
                        'action' => 'close', $this->Usuario->id
                    ));
                } else {
                    $this->Session->setFlash(__('Registro salvo com sucesso', true));
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                }
            } else {
                unset($this->data['Usuario']['ds_senha']);
                unset($this->data['Usuario']['ds_confirma_senha']);
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }

        $this->set('setores', $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));

        $this->set('projetos', $this->Projeto->find('list', array(
            'conditions' => array(
                'Projeto.ic_ativo' => 1
            )
        )));

        $perfis = $this->Usuario->UsuarioPerfil->Perfil->find('list');

        $this->set(compact('perfis'));

        $this->set('total', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_nominal' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $this->set('totalBI', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_bi' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $this->set('gestores', $this->Usuario->find('list', array(
            'conditions' => $condition,
            'joins' => array(
                array(
                    'table' => 'usuario_perfis',
                    'alias' => 'UsuarioPerfil',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario = Usuario.co_usuario',
                        'UsuarioPerfil.co_perfil = ' . $coGestor
                    )
                )
            ),
            'order' => 'ds_nome ASC'
        )));
    }

    /**
     * @resource { "name": "Editar Usuário", "route":"usuarios\/edit", "access": "private", "type": "update" }
     */
    public function edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

        if (!empty($this->data)) {
            $this->prepararCampos();

            if (empty($this->data['Usuario']['ds_senha'])) {
                unset($this->data['Usuario']['ds_senha']);
                unset($this->data['Usuario']['ds_confirma_senha']);
            }

            if (isset($this->data['Usuario']['tipo_licenca'])) {
                if ($this->data['Usuario']['tipo_licenca'] == 'nominal') {
                    $this->data['Usuario']['licenca_nominal'] = '1';
                    $this->data['Usuario']['licenca_concorrente'] = null;
                    unset($this->data['Usuario']['tipo_licenca']);
                } elseif ($this->data['Usuario']['tipo_licenca'] == 'concorrente') {
                    $this->data['Usuario']['licenca_nominal'] = null;
                    $this->data['Usuario']['licenca_concorrente'] = '1';
                    unset($this->data['Usuario']['tipo_licenca']);
                }
            } else {
                $this->data['Usuario']['licenca_nominal'] = '1';
                $this->data['Usuario']['licenca_concorrente'] = null;
                unset($this->data['Usuario']['tipo_licenca']);
            }

            $usuarioPreEdicao = $this->Usuario->findByCoUsuario($this->data['Usuario']['co_usuario']);

            if ($usuarioPreEdicao['Usuario']['nu_cpf'] == $this->data['Usuario']['nu_cpf']) {
                unset($this->Usuario->validate['nu_cpf']);
            }

            if ($usuarioPreEdicao['Usuario']['ds_email'] == $this->data['Usuario']['ds_email']) {
                unset($this->Usuario->validate['ds_email']);
            }

            App::import('Model', 'UsuarioPerfil');
            $modelUsuarioPerfil = new UsuarioPerfil();
            $modelUsuarioPerfil->recursive = -1;
            $atualizar = $modelUsuarioPerfil->find('first', array(
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario' => $this->data['Usuario']['co_usuario']
                    )
                )
            );

            $this->data['UsuarioPerfil']['co_usuario_perfil'] = $atualizar['UsuarioPerfil']['co_usuario_perfil'];

            if ($this->Usuario->saveAll($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));

                App::import('Helper', 'Print');
                $print = new PrintHelper();

                $usuario = $this->Session->read('usuario');
                if ($print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/edit')) {
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->redirect(array(
                        'action' => 'edit',
                        $id
                    ));
                }
            } else {
                unset($this->data['Usuario']['ds_senha']);
                unset($this->data['Usuario']['ds_confirma_senha']);
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Usuario->read(null, $id);
        }

        unset($this->data['Usuario']['ds_senha']);
        unset($this->data['Usuario']['ds_confirma_senha']);

        $perfis = $this->Usuario->UsuarioPerfil->Perfil->find('list');

        $usuarioPreEdicao = $this->Usuario->findByCoUsuario($this->data['Usuario']['co_usuario']);

        // valida dados para exibição
        if (!FunctionsComponent::validaCPF($usuarioPreEdicao['Usuario']['nu_cpf'])) {
            $usuarioPreEdicao['Usuario']['nu_cpf'] = null;
            unset($this->data['Usuario']['nu_cpf']);
        }

        $this->set('pre_edicao', $usuarioPreEdicao);
        $this->set('dt_liberacao', $this->data['Usuario']['dt_liberacao']);

        if (isset($this->data['Usuario']['dt_bloqueio'])) {
            $this->set('dt_bloqueio', $this->data['Usuario']['dt_bloqueio']);
        }

        $setores = $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));

        $this->set('setores', $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));

        $this->loadModel('Projeto');
        $this->set('projetos', $this->Projeto->find('list', array(
            'conditions' => array(
                'Projeto.ic_ativo' => 1
            )
        )));
        $this->set(compact('perfis'));
        $this->set(compact('id'));

        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

        $condition = array(
            'Usuario.ds_nome IS NOT' => null,
            'Usuario.ic_ativo' => 1
        );

        $this->set('gestores', $this->Usuario->find('list', array(
            'conditions' => $condition,
            'joins' => array(
                array(
                    'table' => 'usuario_perfis',
                    'alias' => 'UsuarioPerfil',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario = Usuario.co_usuario',
                        'UsuarioPerfil.co_perfil = ' . $coGestor
                    )
                )
            ),
            'order' => 'ds_nome ASC'
        )));

        $this->set('total', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_nominal' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $this->set('totalBI', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_bi' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $this->set('isOwnData', $this->Usuario->isOwnData($id));

        $this->set('coSetor', $coSetor);
        $this->set('coFiscal', $coFiscal);
        $this->set('coAdministrador', $coAdministrador);
    }
    /**
     * @resource { "name": "Editar Usuário", "route":"usuarios\/edit", "access": "private", "type": "update" }
     */
    public function profile($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }

        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

        if (!empty($this->data)) {
            $this->prepararCampos();

            if (empty($this->data['Usuario']['ds_senha'])) {
                unset($this->data['Usuario']['ds_senha']);
                unset($this->data['Usuario']['ds_confirma_senha']);
            }

            if (isset($this->data['Usuario']['tipo_licenca'])) {
                if ($this->data['Usuario']['tipo_licenca'] == 'nominal') {
                    $this->data['Usuario']['licenca_nominal'] = '1';
                    $this->data['Usuario']['licenca_concorrente'] = null;
                    unset($this->data['Usuario']['tipo_licenca']);
                } elseif ($this->data['Usuario']['tipo_licenca'] == 'concorrente') {
                    $this->data['Usuario']['licenca_nominal'] = null;
                    $this->data['Usuario']['licenca_concorrente'] = '1';
                    unset($this->data['Usuario']['tipo_licenca']);
                }
            } else {
                $this->data['Usuario']['licenca_nominal'] = '1';
                $this->data['Usuario']['licenca_concorrente'] = null;
                unset($this->data['Usuario']['tipo_licenca']);
            }

            $usuarioPreEdicao = $this->Usuario->findByCoUsuario($this->data['Usuario']['co_usuario']);

            if ($usuarioPreEdicao['Usuario']['nu_cpf'] == $this->data['Usuario']['nu_cpf']) {
                unset($this->Usuario->validate['nu_cpf']);
            }

            if ($usuarioPreEdicao['Usuario']['ds_email'] == $this->data['Usuario']['ds_email']) {
                unset($this->Usuario->validate['ds_email']);
            }

            App::import('Model', 'UsuarioPerfil');
            $modelUsuarioPerfil = new UsuarioPerfil();
            $modelUsuarioPerfil->recursive = -1;
            $atualizar = $modelUsuarioPerfil->find('first', array(
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario' => $this->data['Usuario']['co_usuario']
                    )
                )
            );

            $this->data['UsuarioPerfil']['co_usuario_perfil'] = $atualizar['UsuarioPerfil']['co_usuario_perfil'];

            if ($this->Usuario->saveAll($this->data)) {

                $this->Session->setFlash(__('Registro salvo com sucesso', true));

                App::import('Helper', 'Print');
                $print = new PrintHelper();

                $usuario = $this->Session->read('usuario');
                if ($print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/edit')) {
                    $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->redirect(array(
                        'action' => 'edit',
                        $id
                    ));
                }
            } else {
                unset($this->data['Usuario']['ds_senha']);
                unset($this->data['Usuario']['ds_confirma_senha']);
                $this->Session->setFlash(__('O registro não pode ser salvo. Por favor, tente novamente.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Usuario->read(null, $id);
        }

        unset($this->data['Usuario']['ds_senha']);
        unset($this->data['Usuario']['ds_confirma_senha']);

        $perfis = $this->Usuario->UsuarioPerfil->Perfil->find('list');

        $usuarioPreEdicao = $this->Usuario->findByCoUsuario($this->data['Usuario']['co_usuario']);

        // valida dados para exibição
        if (!FunctionsComponent::validaCPF($usuarioPreEdicao['Usuario']['nu_cpf'])) {
            $usuarioPreEdicao['Usuario']['nu_cpf'] = null;
            unset($this->data['Usuario']['nu_cpf']);
        }

        $this->set('pre_edicao', $usuarioPreEdicao);
        $this->set('dt_liberacao', $this->data['Usuario']['dt_liberacao']);

        if (isset($this->data['Usuario']['dt_bloqueio'])) {
            $this->set('dt_bloqueio', $this->data['Usuario']['dt_bloqueio']);
        }

        $setores = $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        ));

        $this->set('setores', $this->Usuario->Setor->find('threaded', array(
            'order' => 'ds_setor ASC',
            'conditions' => array(
                'Setor.ic_ativo' => 1
            )
        )));

        $this->loadModel('Projeto');
        $this->set('projetos', $this->Projeto->find('list', array(
            'conditions' => array(
                'Projeto.ic_ativo' => 1
            )
        )));
        $this->set(compact('perfis'));
        $this->set(compact('id'));

        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);

        $condition = array(
            'Usuario.ds_nome IS NOT' => null,
            'Usuario.ic_ativo' => 1
        );

        $this->set('gestores', $this->Usuario->find('list', array(
            'conditions' => $condition,
            'joins' => array(
                array(
                    'table' => 'usuario_perfis',
                    'alias' => 'UsuarioPerfil',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'UsuarioPerfil.co_usuario = Usuario.co_usuario',
                        'UsuarioPerfil.co_perfil = ' . $coGestor
                    )
                )
            ),
            'order' => 'ds_nome ASC'
        )));

        $this->set('total', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_nominal' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $this->set('totalBI', $this->Usuario->find('count', array(
            'conditions' => array(
                array(
                    'Usuario.ic_ativo' => 1,
                    'Usuario.licenca_bi' => 1,
                    'Usuario.ic_acesso' => 1
                )
            )
        )));

        $this->set('isOwnData', $this->Usuario->isOwnData($id));

        $this->set('coSetor', $coSetor);
        $this->set('coFiscal', $coFiscal);
        $this->set('coAdministrador', $coAdministrador);
    }

    /**
     * x Função para determinar se o usuário possui permissão para acessar o registro
     *
     * Funcionalidade substituida pelo controle de acesso.
     *
     * @deprecated
     */
    public function isPermissaoRegistro($id)
    {
        $usuario = $this->Session->read('usuario');

        App::import('Helper', 'Print');
        $print = new PrintHelper();

        $permissao = $print->checkPrivilegio($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/edit');

        if ($id == $usuario['Usuario']['co_usuario']) {
            $permissao = true;
        }

        if (!$permissao) {
            $this->Session->setFlash(__('Usuário sem permissão para acessar o registro solicitado.', true));
            $this->redirect(array(
                'controller' => 'pages',
                'action' => 'home'
            ));
        }
    }

    /**
     * @resource { "name": "Exclusão Lógica", "route":"usuarios\/bloquearOrDesbloquear", "access": "private", "type": "delete" }
     */
    public function bloquearOrDesbloquear($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(__('Identificador inválido', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
        if ($id > 0) {
            $usuario = $this->Usuario->read(null, $id);
            $msg = null;

            switch ($usuario['Usuario']['ic_ativo']) {
                // Desbloqueia o Usuário
                case 1:
                    $usuario['Usuario']['ic_ativo'] = 0;

                    $msg = "Usuário bloqueado com sucesso";
                    break;
                // Bloqueia o Usuário
                case 0:
                    $usuario['Usuario']['ic_ativo'] = 1;
                    $msg = "Usuário desbloqueado com sucesso";
                    break;
            }
            unset($this->Usuario->validate['nu_cpf']['isUnique']);
            unset($this->Usuario->validate['nu_cpf']['notempty']);
            unset($this->Usuario->validate['ds_email']['isUnique']);
            unset($this->Usuario->validate['ds_nome']['notempty']);

            $this->Usuario->save($usuario);

            $this->Session->setFlash(__($msg, true));
            $this->redirect(array(
                'action' => 'index'
            ));

            $this->Session->setFlash(__('Erro ao excluir registro', true));
            $this->redirect(array(
                'action' => 'index'
            ));
        }
    }

    private function logar($usuario, $senha)
    {
        clearstatcache();

        $criteria['no_usuario'] = $usuario;
        $criteria['ds_senha'] = $senha;
        $criteria['logar'] = 1;

        $usuario = null;

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        if ($modulo->isInscricao() && strlen($criteria['no_usuario']) == 11) {
            App::import('Model', 'Fornecedor');
            $dbFornecedor = new Fornecedor();
            if ($fornecedor = $dbFornecedor->find(array(
                'nu_cnpj' => $criteria['no_usuario'],
                'ds_senha' => $criteria['ds_senha']
            ))
            ) {
                $usuario['Usuario']['co_fornecedor'] = $fornecedor['Fornecedor']['co_fornecedor'];
                $usuario['UsuarioPerfil']['co_perfil'] = 6;
                $usuario['Usuario']['nu_cpf'] = $fornecedor['Fornecedor']['nu_cnpj'];
                $usuario['Usuario']['ds_nome'] = $fornecedor['Fornecedor']['no_razao_social'];
                $usuario['Usuario']['no_usuario'] = $fornecedor['Fornecedor']['nu_cnpj'];
                $usuario['Usuario']['ds_email'] = $fornecedor['Fornecedor']['ds_email'];
                $usuario['UsuarioPerfil']['co_perfil'] = '00000000000000000000000000000000100010001100100000000000000110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
            }
        }

        if ($usuario != null || $usuario = $this->Usuario->find($criteria)) {

            if ($usuario['Usuario']['ic_ativo'] == 0) {
                echo "bloqueado";
                exit();
            } else {
                App::import('Helper', 'Print');
                $print = new PrintHelper();
                $dt_atual = new DateTime();

                if ($usuario['Usuario']['dt_ult_acesso']) {
                    $dt_ult_acesso = new DateTime($print->dateSql($usuario['Usuario']['dt_ult_acesso']) . ' ' . substr($usuario['Usuario']['dt_ult_acesso'], 11, 8));
                } else {
                    $dt_ult_acesso = $dt_atual;
                }
                $diferenca = $dt_ult_acesso->diff($dt_atual);


                if ($usuario['Usuario']['licenca_nominal'] == 1) {
                    $this->verificarLicencaNominal($usuario);
                    $usuarioLogado = $this->Usuario->find(array('co_usuario' => $usuario['Usuario']['co_usuario']));

                    if ($usuarioLogado['Usuario']['is_logged'] == 0) {
                        $this->criarSession($usuario);
                        exit();
                    } else if (($usuarioLogado['Usuario']['is_logged'] == 1) && ($usuarioLogado['Usuario']['ip_ult_acesso'] == $this->get_client_ip())) {
                        $this->criarSession($usuario);
                        exit();
                    } else if (($usuarioLogado['Usuario']['is_logged'] == 1) && ($usuarioLogado['Usuario']['ip_ult_acesso'] != $this->get_client_ip()) && ($diferenca->i > Configure::read('App.config.resource.licenca.timeout.nominal'))) {
                        $this->criarSession($usuarioLogado);
                        exit();
                    } else {
                        echo "logado";
                        exit();
                    }
                } else if ($usuario['Usuario']['licenca_concorrente'] == 1) {
                    $licencasConcorrentes = $this->contarLicencasConcorrentesDisponiveis();
                    $usuarioLogado = $this->Usuario->find(array('co_usuario' => $usuario['Usuario']['co_usuario']));
                    if ($usuarioLogado['Usuario']['is_logged'] == 1) {
                        echo "logado";
                        exit();
                    } else {
                        if ($licencasConcorrentes) {
                            $this->criarSession($usuario);
                            exit();
                        } else {
                            echo "fim_concorrente";
                            exit();
                        }
                    }
                } else {
                    echo "fim_concorrente";
                    exit();
                }
            }
        } else {
            echo "false";
            exit();
        }
    }

    /**
     * @resource { "name": "Verificar Licença Nominal", "route":"usuarios\/verificarLicencaNominal", "access": "private", "type": "select" }
     */
    public function verificarLicencaNominal($usuario)
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $this->Usuario->recursive = -1;
        $usuarioNominal = $this->Usuario->find('all', array(
            'conditions' => array(
                'licenca_nominal' => 1,
                'is_logged' => 1,
                'co_usuario' => $usuario['Usuario']['co_usuario']
            )
        ));

        if (count($usuarioNominal) > 0) {
            $this->Usuario->Query("SET sql_safe_updates = 0");
            foreach ($usuarioNominal[0] as $user) {
                $dt_ult_acesso = str_replace('/', '-', $user['dt_ult_acesso']);
                $userDate = new DateTime(strftime($dt_ult_acesso));
                $this->Usuario->Query("UPDATE usuarios
                                   SET is_logged = '0'
                                   WHERE co_usuario = " . $user['co_usuario'] . "
                                   AND TIMESTAMPDIFF(MINUTE,'" . $userDate->format('Y-m-d H:i:s') . "',NOW()) > " . Configure::read('App.config.resource.licenca.timeout.nominal'));
            }
            $this->Usuario->Query("set sql_safe_updates = 1");

            // conta usuários concorrentes logados
            $verificaIsLogged = $this->Usuario->find('count', array(
                'conditions' => array(
                    'licenca_concorrente' => 1,
                    'is_logged' => 1,
                    'co_usuario' => $usuario['Usuario']['co_usuario']
                )
            ));

            if ($verificaIsLogged > 0) {
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    public function criarSession($usuario)
    {
        $this->Session->destroy();

        $usuario['IP'] = $this->get_client_ip();
        $this->Session->write('usuario', $usuario);

        $dataUsu['Usuario']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        $dataUsu['Usuario']['dt_ult_acesso'] = DboSource::expression('CURRENT_TIMESTAMP');
        $dataUsu['Usuario']['ip_ult_acesso'] = $usuario['IP'];
        $dataUsu['Usuario']['is_logged'] = 1;
        $this->Usuario->create();
        $this->Usuario->save($dataUsu, false, array('dt_ult_acesso', 'is_logged', 'ip_ult_acesso'));

        // não retire o true, senão não loga!!! ò.ó
        echo "true";
        exit();
    }

    // Function to get the client IP address
    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        if ($this->check_exec_enabled()) {
            $ipaddress = $ipaddress . ' ' . exec('getmac');
        }

        return substr($ipaddress, 0, 19);
    }

    function check_exec_enabled()
    {
        $disabled = explode(',', ini_get('disable_functions'));
        return !in_array('exec', $disabled);
    }

    public function contarLicencasConcorrentesDisponiveis()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        // meow <^_^>
        // seta para is_logged 0 usuários que não interagiram a mais tempo que o configurado na variável timeoutConcorrente
        $this->Usuario->recursive = -1;
        $usuariosConcorrentesLogados = $this->Usuario->find('all', array(
            'conditions' => array(
                'licenca_concorrente' => 1,
                'is_logged' => 1
            )
        ));
        if (count($usuariosConcorrentesLogados) > 0) {
            $this->Usuario->Query("SET sql_safe_updates = 0");
            foreach ($usuariosConcorrentesLogados[0] as $user) {
                $dt_ult_acesso = str_replace('/', '-', $user['dt_ult_acesso']);
                $userDate = new DateTime(strftime($dt_ult_acesso));

                $this->Usuario->Query("UPDATE usuarios
                                   SET is_logged = '0'
                                   WHERE co_usuario = " . $user['co_usuario'] . "
                                   AND TIMESTAMPDIFF(MINUTE,'" . $userDate->format('Y-m-d H:i:s') . "',NOW()) > ".Configure::read('App.config.resource.licenca.timeout.concorrente'));
            }
            $this->Usuario->Query("set sql_safe_updates = 1");

            // conta usuários concorrentes logados
            $contagemUsuariosConcorrentesLogados = $this->Usuario->find('count', array(
                'conditions' => array(
                    'licenca_concorrente' => 1,
                    'is_logged' => 1
                )
            ));
            return $contagemUsuariosConcorrentesLogados;
        } else {
            return 'true';
        }
    }

    /**
     * @resource { "name": "Verificar Usuário", "route":"usuarios\/checkusuario", "access": "public", "type": "select" }
     */
    public function checkusuario()
    {
        if (!empty($this->data)) {
            $this->logar($this->data['Usuario']['no_usuario'], md5($this->data['Usuario']['ds_senha']));
        } else {
            echo "false";
            exit();
        }
    }

    /**
     * @resource { "name": "Verificar Documento", "route":"usuarios\/checkdocumento", "access": "public", "type": "select" }
     */
    public function checkdocumento()
    {
        if (!empty($this->data)) {
            $criteria['Usuario.nu_cpf'] = $this->data['Usuario']['nu_documento'];
            $usuario = $this->Usuario->find('first', array('conditions' => $criteria, 'fields' => array('Usuario.no_usuario', 'Usuario.ds_senha')));
            if ($usuario) {
                $this->logar($usuario['Usuario']['no_usuario'], $usuario['Usuario']['ds_senha']);
            } else {
                echo "sem_usuario";
                exit();
            }
        } else {
            echo "false";
            exit();
        }
    }

    /**
     * @resource {
     *  "name": "Autenticar",
     *  "route":"usuarios\/autenticar",
     *  "access": "public",
     *  "type": "select"
     * }
     */
    public function autenticar($errado = null)
    {
        $this->autoRender = false;
        if ($errado == null) {
            $this->layout = "principal";
            $this->render("autenticarprincipal");
        } else {
            $this->layout = "esqueci";
            $this->render("autenticar");
        }

        if (isset($this->data)) {

            if (Configure::read('App.config.resource.certificadoDigital') &&
                ($this->data['Usuario']['login_certificado'] == 1) &&
                isset($this->data['Usuario']['token']) &&
                ($this->data['Usuario']['token'] != '')
            ) {
                // Logando com o Certificado Digital
                $token = $this->data['Usuario']['token'];
                $auth = $this->Util->getRestPkiClient()->getAuthentication();
                $vr = $auth->completeWithWebPki($token);
                $criteria = array();
                $usuario = array(
                    'error' => true,
                    'msg' =>'Usuário não encontrado.'
                );
                if ($vr->isValid()) {
                    $userCert = $auth->getCertificate();
                    $criteria['Usuario.nu_cpf'] = $userCert->pkiBrazil->cpf;
                    $criteria['Usuario.ic_ativo'] = '1';
                    $hasUsuario = $this->Usuario->find($criteria);
                    if ($hasUsuario) {
                        $usuario = $hasUsuario;
                    }
                }
            } else {
                if (empty($this->data['Usuario']['ds_senha'])) {
                    $this->Session->setFlash(__('Campo Senha vazio', true));
                    $this->redirect(array(
                        'controller' => 'usuarios',
                        'action' => 'autenticar',
                        'falha'
                    ));
                }

                $usuario = $this->Auth->autenticate($this->data);
            }

            if (!isset($usuario['error'])) {

                $usuario['IP'] = $this->RequestHandler->getClientIp();

                $this->Session->write('usuario', $usuario);

                $this->loadModel('Log');
                $log = array(
                    'Log' => array(
                        'tp_acao' => 'L',
                        'dt_log' => date('Y-m-d H:m:s')
                    )
                );

                $log['Log']['ds_tabela'] = 'logs';
                $log['Log']['ds_log'] = json_encode($usuario);
                $log['Log']['co_usuario'] = $usuario['Usuario']['co_usuario'];
                $log['Log']['ds_nome'] = $usuario['Usuario']['ds_nome'];
                $log['Log']['ds_email'] = $usuario['Usuario']['ds_email'];
                $log['Log']['nu_ip'] = $usuario['IP'];
                $log['Log']['dt_log'] = date("Y-m-d H:i:s");

                $this->Log->save($log);

                // Registra usuário como logado no banco, se for uma licença concorrente
                if ($usuario["Usuario"]["licenca_concorrente"] == 1) {
                    $configLicenca = Configure::read('App.config.resource.licenca');
                    $maxConcorrente = $configLicenca['proporcaoConcorrente'] * $configLicenca['concorrente'];
                    if ($this->contarLicencasConcorrentesDisponiveis() < $maxConcorrente) {
                        try {
                            $this->Usuario->Query("UPDATE usuarios SET is_logged = '1' WHERE co_usuario = " . $usuario['Usuario']['co_usuario']);

                        } catch (Exception $e) {
                            echo 'Exceção capturada: ', $e->getMessage(), "\n";
                        }
                        $this->direciona();
                    } else {
                        $this->Session->setFlash(__('Falha ao autenticar usuário. <br><h5>Número máximo de usuários concorrentes foi atingido. Tente mais tarde.</h5>', true));
                        $this->redirect(array(
                            'controller' => 'usuarios',
                            'action' => 'autenticar',
                            'falha'
                        ));

                        die();
                    }
                } else {
                    $this->direciona();
                }

            } else {
                if (isset($usuario['error'])) {
                    $this->Session->setFlash(__($usuario['msg'], true));
                    $this->redirect(array(
                        'controller' => 'usuarios',
                        'action' => 'autenticar',
                        'falha'
                    ));
                }

                if (empty($this->data['Usuario']['no_usuario'])) {
                    $this->Session->setFlash(__('Campo Usuário vazio', true));
                    $this->redirect(array(
                        'controller' => 'usuarios',
                        'action' => 'autenticar',
                        'falha'
                    ));
                }

                $this->Session->setFlash(__('Falha ao autenticar usuário. Por favor, tente novamente.', true));
                $this->redirect(array(
                    'controller' => 'usuarios',
                    'action' => 'autenticar',
                    'falha'
                ));
            }
        }
    }

    /**
     * Salva o usuário do LDAP no banco
     * @access public
     * @param array $userPostData
     * @return boolean
     */
    private function _salvarUsuarioDoLdapNoBanco($userPostData)
    {
        /**
         * @todo
         * DEFINIR QUAIS CAMPOS SERÃO BUSCADOS NO LDAP E INSERIDOS NO BANCO (EX.: NOME E EMAIL)
         * DEFINIR OS CAMPOS QUE SERÃO SALVOS NO BANCO (ic_acesso, nu_cpf=0, licencas...???)
         * HABILITAR O CAMPO CPF CASO O USUÁRIO SEJA DE LDAP E ESTEJA SEM ESSE DADO
         * DESABILITAR A TROCA DE SENHA PARA QUE ELA PERMANEÇA A QUE VEIO DO LDAP
         */
        $usuario = array(
            'ic_acesso' => 1,
            'nu_cpf' => 0, #NOT NULL
            //'ds_nome'             => $ldap['nome'],#NOT NULL
            //'ds_email'            => $ldap['email'],
            'no_usuario' => $userPostData['no_usuario'],
            'ds_senha' => md5($userPostData['ds_senha']),
            'ds_confirma_senha' => md5($userPostData['ds_senha']),
            //'dt_liberacao'        => date('d/m/Y H:i:s'),
            //'co_privilegio'       => 5,
            //'licenca_nominal'     => 1,
            //'licenca_concorrente' => ,
            //'ds_privilegios'      => ,
        );
        $usuario['ds_nome'] = 'Usuário do LDAP';

        $this->Usuario->save(array('Usuario' => $usuario));
        $usuarioDB = $this->Usuario->find(array('Usuario.co_usuario' => $this->Usuario->id));
        $usuarioDB['IP'] = $this->RequestHandler->getClientIp();
        $this->Session->write('usuario', $usuarioDB);

        $this->loadModel('Log');
        $log = array(
            'Log' => array(
                'tp_acao' => 'L'
            )
        );
        $this->Log->save($log);
        $this->direciona();
    }

    /**
     * @resource { "name": "Sair", "route":"usuarios\/sair", "access": "public", "type": "select" }
     */
    public function sair()
    {
        $usuario = $this->Session->read('usuario');

        $dataSair = array(
            'Usuario.is_logged' => 0
        );

        $this->Usuario->updateAll($dataSair, array('Usuario.co_usuario = ' => $usuario['Usuario']["co_usuario"]));

        $cliente = $this->Session->read('cliente');
        $this->Session->destroy();
        $this->Session->write('cliente', $cliente);

        clearstatcache();

        $this->redirect(array(
            'controller' => 'pages',
            'action' => 'index'
        ));
    }

    /**
     * @resource { "name": "Esqueci a Senha", "route":"usuarios\/esqueci", "access": "public", "type": "select" }
     */
    public function esqueci()
    {
        $this->layout = "esqueci";
        if (empty($this->data['Usuario']['ds_email'])) {
            //Alterando mensagem [GES-378]
            $this->Session->setFlash(__('Favor informar o seu e-mail cadastrado para a recuperação de sua senha de acesso.', true));
        } else {
            $criteria['Usuario.ds_email'] = $this->data['Usuario']['ds_email'];
            $usuario = $this->Usuario->find($criteria);

            if (empty($usuario)) {
                $this->Session->setFlash(__('O E-mail informado é inválido ou não está cadastrado no sistema.', true));
            } else {
                $senha = $this->geraSenha(8);

                $this->Usuario->id = $usuario['Usuario']["co_usuario"];
                $this->Usuario->saveField("ds_senha", md5($senha));

                $this->EmailProvider->subject = 'Recuperacão de Senha ';
                $this->EmailProvider->to = $usuario['Usuario']['ds_nome'] . ' <' . $usuario['Usuario']['ds_email'] . '>';

                $mensage = " Foi solicitada nova senha de acesso ao GESCON, confira seu dados:<BR>";
                $mensage .= " Usuário: " . $usuario['Usuario']['no_usuario'] . "<BR>";
                $mensage .= " Nova Senha: " . $senha;

                $this->EmailProvider->send($mensage);

                $this->Session->setFlash(__('A sua nova senha de acesso foi encaminhada para o seu E-mail cadastrado!', true));
            }
        }
    }

    public function recuperado()
    {
        $this->layout = "esqueci";
        $this->Session->setFlash(__('A sua nova senha de acesso foi encaminhada para o seu E-mail cadastrado.', true));
    }

    /**
     * @resource { "name": "Direcionar", "route":"usuarios\/direciona", "access": "public", "type": "select" }
     */
    public function direciona()
    {

        App::import('Helper', 'Print');
        $print = new PrintHelper();

        $usuario = $this->Session->read('usuario');

        $this->redirect(array(
            'controller' => 'dashboard',
            'action' => 'index'
        ));
    }

    /**
     * @resource { "name": "Verificar CPF", "route":"usuarios\/verifyCPF", "access": "private", "type": "select" }
     */
    public function verifyCPF($nuCpf)
    {
        $this->Usuario->recursive = 0;
        // pertmite inserção de varios CPFs zerados
        if ($nuCpf == '00000000000') {
            $existsCPF = 0;
        } else {
            $existsCPF = $this->Usuario->Query('SELECT COUNT(*) as cpfs from usuarios WHERE nu_cpf = ' . $nuCpf);
        }


        if ($existsCPF[0][0]['cpfs'] > 0) {
            echo json_encode(array(
                'color' => 'red',
                'bold' => 'bold',
                'message' => 'O CPF pertence a um usuário cadastrado ou bloqueado!',
                'validation' => 'false'
            ));
            //echo json_encode("CPF já cadastrado no sistema! Insira outro.");
        } else {
            if (!FunctionsComponent::validaCPF($nuCpf) || $nuCpf == '00000000000' || empty($nuCpf) || $nuCpf == '___________') {
                echo json_encode(array(
                    'color' => 'red',
                    'bold' => 'bold',
                    'message' => 'O CPF digitado é inválido! Por favor insira outro.',
                    'validation' => 'false'
                ));
                //echo json_encode("O CPF digitado é inválido! Por favor insira outro.");
            } else {
                echo json_encode(array(
                    'color' => '#009900',
                    'bold' => 'bold',
                    'message' => 'CPF válido!',
                    'validation' => 'true'
                ));
                //echo json_encode("CPF válido!");
            }
        }

        exit();
    }


    /**
     * Busca usuarios no ldap
     *
     * @param $searchTerm
     * @return json
     */
    public function getUsersLdap($searchTerm)
    {
        $usuarios = null;
        if (Configure::read('App.config.component.ldap.enabled')) {
            $usuarios = $this->LdapSearch->searchUserInLdap($searchTerm);
        }
        echo json_encode($usuarios);
        exit();
    }


    // public function checksetor($coSetor) {
    //     // $this->loadModel('Setor');
    //     $this->Usuario->recursive = -1;
    //     $countSetor = $this->Usuario->find('count', array(
    //         'conditions' => array(
    //             'Usuario.ic_ativo' => 1,
    //             'Usuario.co_setor' => $coSetor
    //         )
    //     ));
    //    $valid = true;

    //     if ($countSetor) {
    //         $valid = false;
    //     }

    //     echo json_encode(array('valid' => $valid));
    //     exit;
    // }
}
