<?php

App::import( 'Vendor', 'html2pdf' );

error_reporting( E_ALL && !E_NOTICE );

//require_once(dirname(__FILE__) . '/html2pdf/html2pdf.class.php');


/**
 * HTML2PDF Librairy
 *
 * Este arquivo tem como função converter HTML em PDF
 *
 * isset($_GET['html']) Caso este parametro esteja setado
 * será exibido apenas o HTML
 *
 * @author Antério vieira <anteriovieira@gmail.com>
 */
class HTMLPDF {

    public function __construct() {
    }

    public function output($content, $vueHtml = false) {

        $page .= '<page orientation="paysage"  backbottom="5mm">';
        $page .= '<page_footer>';
        $page .= '<table style="width: 100%;" >';
        $page .= '<tr>';
        $page .= '<td style="width: 50%; text-align: left">';
        $page .= 'Emitido em ' . date("d/m/Y \à\s H:i\h\s");
        $page .= '</td>';
        $page .= '<td style="width: 50%; text-align: right">';
        $page .= 'Pagina [[page_cu]]/[[page_nb]]';
        $page .= '</td>';
        $page .= '</tr>';
        $page .= '</table>';
        $page .= '</page_footer>';
        $page .= $content;
        $page .= '</page>';


        try {
            $html2pdf = new HTML2PDF('P', 'A4', 'fr');

            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->writeHTML($page , $vueHtml);
            $html2pdf->Output('teste.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

}
