<?php
set_time_limit(0);
ini_set("memory_limit", -1);

App::import('Vendor', 'mpdf');

class PDF extends mPDF
{
    // O quarto parâmetro define se a cor da barra do título é vermelha ou azul!!
    function imprimir($contentHtml, $orientation = 'P', $title = null, $desabilitado = false)
    {
        $this->simpleTables = true;
        $this->DeflMargin = '5';
        $this->DefrMargin = '5';
        $this->shrink_tables_to_fit = 0;
        $this->setAutoTopMargin = 'stretch';

        App::import('Helper', 'Html');
        $html = new HtmlHelper();

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $header = "<table style='font-size: 12pt; width: 100%;  max-height: 80px;'><tr><td>" . $html->image($modulo->img_cabecalho_relatorio) . "</td></tr>";

        if (isset($title)) {
            if ($desabilitado) {
                $header .= "<tr><td style='text-align: center; background-color:#FF0000; color:#FFF;'><b>" . up($title) . "</b></td></tr>";
            } else {
                $header .= "<tr><td style='text-align: center; background-color:#0099FF; color:#FFF;'><b>" . up($title) . "</b></td></tr>";
            }
        }
        $header .= "</table>";
        $this->SetHTMLHeader($header);

        $this->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
                <td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE d-m-Y}</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; "></td></tr></table>');

        $style =    '<style>
                    .table { width: 100%; }
                    .table th,
                    .table td { padding: 8px; line-height: 20px; text-align: left; font-size: 8pt; }
                    .table th { font-weight: bold; }
                    .table thead th { font-size: 10pt; background-color: #f0f0f0; }
                    .total { font-weight: bold; font-size: 10pt; background-color: #f0f0f0; }
                    .table .table { background-color: #ffffff; }
                    .table-bordered { border: 1px solid #dddddd; border-collapse: separate; border-left: 0; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; }
                    .table-bordered th,
                    .table-bordered td { border-left: 1px solid #dddddd; }
                    .altrow { background-color: #f9f9f9; }
                    .warning { background-color: #ffe949; }
                    .success { background-color: #dff0d8; }
                    .error { background-color: #ffa1a1; }
                    </style>';

        if ($contentHtml == "") {
            $contentHtml = 'Não existem resultados a serem exibidos para este relatório.';
        }

        $this->AddPage($orientation);
        $this->WriteHTML($style, 1);
        $this->WriteHTML($contentHtml, 2);


        return $this->Output();
    }

    function imprimirIndenizacoes($ano, $mes, $funcionario, $indenizacoes)
    {

        App::import('Helper', 'Html');
        $html = new HtmlHelper();

        $this->simpleTables = true;
        $this->DeflMargin = '5';
        $this->DefrMargin = '5';
        $this->margin_header = '3';
        $this->shrink_tables_to_fit = 0;

        $header = '
            <table width="100%" border="0" cellspacing="0" cellpadding="2" style="font-size:14pt">
              <tr>
                <td width="10%">' . $html->image('nova_azul.png') . '</td>
                <td width="50%" align="center"><strong>INDENIZAÇÃO DE TRANSPORTE</strong></td>
                <td width="20%"><strong>MÊS: ' . $mes . '</strong></td>
                <td width="20%"><strong>ANO: ' . $ano . '</strong></td>
              </tr>
            </table>
            <BR><BR>';

        $this->SetHTMLHeader($header);

        $this->SetHTMLFooter('
                <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 10pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
                <td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE d-m-Y}</span></td>
                <td width="60%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="7%" style="text-align: right; ">' . $html->image('logo_sistema.png') . '</td></tr></table>');

        $this->AddPage('P');
        $html = '
<table width="100%" border="1" cellspacing="0" cellpadding="2" style="font-size:10pt">
  <tr>
    <td colspan="2">NOME: ' . $funcionario['GrupoAuxiliar']['ds_nome'] . '</td>
    <td width="24%">MATRÍCULA: ' . $funcionario['GrupoAuxiliar']['nu_cpf'] . '</td>
    <td colspan="2">LOTAÇÃO: ' . $funcionario['Setor']['ds_setor'] . '</td>
  </tr>
  <tr>
    <td colspan="5">CARGO/FUNÇÃO: ' . $funcionario['Cargo']['ds_cargo'] . '/' . $funcionario['Funcao']['ds_funcao'] . '</td>
  </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="2" style="font-size:12pt">
      <tr>
        <td colspan="7" align="center"><strong>VIAGENS EFETIVAMENTE FEITAS</strong></td>
      </tr>
      <tr>
        <td align="center" width="10%"><strong>DIA</strong></td>
        <td align="center" width="60%"><strong>OBRA/SERVIÇO</strong></td>
        <td align="center" width="15%"><strong>CARRO</strong></td>
        <td align="center" width="15%"><strong>PLACA</strong></td>
        <td align="center" width="15%"><strong>ANO</strong></td>
        <td align="center" width="15%"><strong>VIAGENS</strong></td>
        <td align="center" width="15%"><strong>KM</strong></td>
      </tr>
      ';
        $nu_distancia_total = 0;
        foreach ($indenizacoes as $indenizacao):
            $nu_distancia_total += $indenizacao['Indenizacao']['nu_distancia'];
            $html .= '
      <tr>
        <td align="center">&nbsp;' . date('d', strtotime(dtDb($indenizacao['Indenizacao']['dt_indenizacao']))) . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['ds_servico'] . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['ds_carro'] . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['ds_placa'] . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['nu_ano_carro'] . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['qt_viagem'] . '</td>
        <td align="center">&nbsp;' . $indenizacao['Indenizacao']['nu_distancia'] . '</td>
      </tr>
      ';
        endforeach;
        $html .= '
      <tr>
        <td colspan="6"><strong>TOTAL DE KILOMETROS NO MÊS:</strong></td>
        <td align="center"><strong>' . $nu_distancia_total . ' KM</strong></td>
      </tr>
    </table>

<table width="100%" border="1" cellspacing="0" cellpadding="2" style="font-size:8pt">
  <tr>
    <td width="25%" align="center" valign="top">
DECLARO QUE AS INFORMAÇÕES ACIMA, POR MIM FORNECIDAS, ESTÃO CORRETAS. CIENTE DE QUE VERIFICADA, A QUALQUER TEMPO A INOBSERVÂNCIA DESTAS, SERÁ ANULADO O ATO DE CONCESSÃO DA INDENIZAÇÃO DE TRANSPORTE E PROVIDENCIADA A REPOSIÇÃO DA IMPORTÂNCIA INDEVIDAMENTE
    </td>
    <td width="25%" align="center" valign="top">
ATESTO, SOB AS PENAS DA LEI, QUE SE AS INFORMAÇÕES ACIMA ESTIVEREM EM DESACORDO COM A REALIDADE, RESPONDEREI SOLIDARIAMENTE COM O FUNCIONÁRIO, PELA REPOSIÇÃO DA IMPORTÂNCIA CORRESPONDENTE AO PAGAMENTO INDEVIDO, SEM PREJUÍZO DAS SANÇÕES QUE COUBEREM.
    </td>
    <td align="center" valign="top" width="25%">
VISTO DO DIRETOR DA ÁREA PARA ENCAMINHAMENTO
    </td>
    <td colspan="2" width="25%" >&nbsp;</td>
  </tr>
  <tr>
    <td>
        <BR />
        <br />
        <BR />
        <br />
        <BR />' . $funcionario['GrupoAuxiliar']['ds_nome'] . ' <br> ' . $funcionario['GrupoAuxiliar']['nu_cpf'] . ' - ' . $funcionario['Funcao']['ds_funcao'] . '
    </td>
    <td valign="bottom">
        CHEFE IMEDIATO
    </td>
    <td valign="bottom">
        CHEFE DE DEPARTAMENTO
    </td>
    <td colspan="2" valign="bottom">
        DIRETOR
    </td>
  </tr>
</table>
                ';
        $this->WriteHTML($html, 2);
        return $this->Output();
    }

    function imprimirNovacap($contentHtml, $orientation = 'P', $title = null, $desabilitado = false, $destiny = null)
    {
        $this->simpleTables = true;
        $this->DeflMargin = '5';
        $this->DefrMargin = '5';
        $this->margin_header = '3';
        $this->shrink_tables_to_fit = 0;

        App::import('Helper', 'Html');
        $html = new HtmlHelper();

        $header = "<table style='font-size: 12pt; width: 100%;'>
                        <tr>
                            <td style='width: 10%;'>" . $html->image('logo_novacap.png') . "</td>
                            <td style='width: 80%;font-size: 30px; font-family: Arial; text-align: center; margin-top: 50px;'>
                                $title
                                <br />
                                <p style='font-size: 12px;'>(Lei de Acesso à Informação - LAI, Lei Federal nº 12.527, de 18 de novembro de 2011)</p>
                            </td>
                            <td style='width: 10%;'>" . $html->image('logo_gdf.png') . "</td>
                        </tr>";

        $header .= "</table><br><br/>";
        $this->SetHTMLHeader($header);

        $this->SetHTMLFooter('
                <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
                <td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE d-m-Y}</span></td>
                <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right; ">' . $html->image('logo_sistema.png') . '</td></tr></table>');

        $style = '<style>
            .table { width: 100%; }
            .table th,
            .table td { padding: 8px; line-height: 20px; text-align: left; font-size: 8pt; }
            .table th { font-weight: bold; }
            .table thead th { font-size: 10pt; background-color: #f0f0f0; }
            .total { font-weight: bold; font-size: 10pt; background-color: #f0f0f0; }
            .table .table { background-color: #ffffff; }
            .table-bordered { border: 1px solid #dddddd; border-collapse: separate; border-left: 0; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; }
            .table-bordered th,
            .table-bordered td { border-left: 1px solid #dddddd; }
            .altrow { background-color: #f9f9f9; }
            .warning { background-color: #ffe949; }
            .success { background-color: #dff0d8; }
            .error { background-color: #ffa1a1; }
            </style>';

        if ($contentHtml == "") {
            $contentHtml = 'Não existem resultados a serem exibidos para este relatório.';
        }

        $this->AddPage($orientation);
        $this->WriteHTML($style, 1);
        $this->WriteHTML($contentHtml, 2);

        if ($destiny != null)
            return $this->Output('', 'D');
        else
            return $this->Output();
    }

}

