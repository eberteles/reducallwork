<?

	require_once 'lib/SiasgDAO.class.php';
	
	class Siasg
    {

        public function query($query)
        {
            $dao = new SiasgDAO();
            return $dao->query($query);
        }

        public function getFornecedores()
        {
            $dao = new SiasgDAO();
            $all = $dao->getFornecedores();
            $res = array();
            foreach ($all as $obj)
                $res[$obj->co_fornecedor] = $obj->nu_cnpj;
            return $res;
        }

        /*
         * cadastra fornecedor caso não esteja cadastrado
         */
        public function cadastraFornecedor($data)
        {
            $dao = new SiasgDAO();
            $dao->cadastraFornecedor($data);
        }

        /*
         * cadastra modalidade caso não esteja cadastrada
         */
        public function cadastraModalidade($data)
        {
            $dao = new SiasgDAO();
            $dao->cadastraModalidade($data);
        }

        /*
         * cadastra contratacao caso não esteja cadastrada
         */
        public function cadastraContratacao($data)
        {
            $dao = new SiasgDAO();
            $dao->cadastraContratacao($data);
        }

    }
	
?>