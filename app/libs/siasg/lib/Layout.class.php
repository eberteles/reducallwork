<?
	require_once 'SiafiDAO.class.php';

	class Layout {
		
		private $fileLayout;
		private $fileContent;
		private $table;
		private $dao;
		
		public function __construct() {
			$this->dao = new SiafiDAO();
		}
		
		public function digArquivos() {
			$this->decompressArquivos();
			
			$files = scandir(PATH_ARQUIVOS);
			
			foreach ($files as $file) {
				if ($file[0] == '.') continue;
				if ($file[0] == '_') continue;
				
				// extension
				if (substr($file, -3) == 'TXT') {
					$content = $file;
					$layout = substr($file, 0, -3).'REF';
					if (file_exists(PATH_ARQUIVOS.$layout)) {
						$this->fileContent = $file;
						$this->fileLayout = $layout;
						$this->setTableName();
						$this->run();
					}
				}
			}
		}
		
		private function decompressArquivos() {
			$files = scandir(PATH_ARQUIVOS);
			
			foreach ($files as $file) {
				if ($file[0] == '.') continue;
				if ($file[0] == '_') continue;
				
				// extension
				$ext = null;
				if (substr($file, -3) == 'zip')          $ext = 'zip';
				else if (substr($file, -6) == 'tar.gz')  $ext = 'tar.gz';
				else if (substr($file, -2) == 'gz')      $ext = 'gz';

				// unzip
				switch ($ext) {
					case 'zip':
						$zip = new ZipArchive();
						$res = $zip->open(PATH_ARQUIVOS.$file);
						if ($res === true) {
							$zip->extractTo(PATH_ARQUIVOS);
							$zip->close();
							unlink(PATH_ARQUIVOS.$file);
						} else
							echo 'Não foi possível descompactar o arquivo: '.$file.'<br>';
						break;
						
					case 'tar.gz':
						break;
						
					case 'gz':
						$file_name = PATH_ARQUIVOS.$file;
						$buffer_size = 4096;
						$out_file_name = str_replace('.gz', '', $file_name);
						$fileR = gzopen($file_name, 'rb');
						$out_file = fopen($out_file_name, 'wb');
						while(!gzeof($fileR))
							fwrite($out_file, gzread($fileR, $buffer_size));
						fclose($out_file);
						gzclose($fileR);
						unlink(PATH_ARQUIVOS.$file);
						break;
				}
			}
		}
		
		private function run() {
			if ($this->dao->arquivoProcessado($this->fileContent)) {
// 				echo 'O arquivo ja foi processado: '.$this->fileContent.'<br>';
				return;
			}
			
			$maps = $this->getMaps();
			$lines = $this->getLines();
			$items = array();
			
			// apply maps
			$i = 0;
			foreach ($lines as $line) {
				$pos = 0;
				foreach ($maps as $map) {
					$items[$i][$map[0]] = substr($line, $pos, $map[2]);
					$pos += $map[2];
				}
				$i++;
			}
			
			// create everything
			$this->dao->createDb();
			$this->dao->createTable($this->table, $maps);
				
			// insert arquivo
			$idArquivo = $this->dao->insertArquivo($this->fileContent);
				
			// insert items
			foreach ($items as $item)
				$this->dao->insertItem($this->table, $item, $idArquivo);
		}
		
		private function getMaps() {
			$lines = file(PATH_ARQUIVOS.$this->fileLayout);
				
			$maps = array();
				
			foreach ($lines as $line) {
				$cols = explode(' ', $line);
				$tmpCol = array();
				foreach ($cols as $col)
					if (trim($col)!='')
						$tmpCol[] = trim($col);
				$maps[] = $tmpCol;
			}
			
			for ($i=0; $i<sizeof($maps); $i++) {
				if (strrpos($maps[$i][2], ",") !== false) {
					$n = explode(',', $maps[$i][2]);
					if (sizeof($n) == 2)
						$maps[$i][2] = $n[0]+$n[1];
				} else if (is_numeric($maps[$i][2]))
					$maps[$i][2] = intval($maps[$i][2]);
			}
			
			return $maps;
		}
		
		private function getLines() {
			$lines = file(PATH_ARQUIVOS.$this->fileContent);
			for ($i=sizeof($lines)-1; $i>=0; $i--)
				if (strlen($lines[$i]) < 100)
					unset($lines[$i]);
				
			return $lines;
		}
		
		private function setTableName() {
			$tmpTable = explode('_', $this->fileContent);
			unset($tmpTable[sizeof($tmpTable)-1]);
			unset($tmpTable[0]);
			$table = '';
			foreach ($tmpTable as $b) $table .= $b.'_';
			$this->table = 'siafi_'.substr($table, 0, -1);
		}
		
	}
?>