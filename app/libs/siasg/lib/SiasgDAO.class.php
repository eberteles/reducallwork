<?

	require_once 'SiasgPDO.class.php';

	/**
	 * SiasgDAO
	 *
	 * @author leo
	 */
	class SiasgDAO {

		private $maxColumnsPerTable = 1000;
		private $pdo;

		public function __construct() {
			$this->pdo = new SiasgPDO();
			$this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		public function query($query) {
            $query = str_replace('""', 'NULL', $query);

            $stmt = $this->pdo->prepare("SET FOREIGN_KEY_CHECKS = 0;");
            $stmt->execute();

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $stmt = $this->pdo->prepare("SET FOREIGN_KEY_CHECKS = 1;");
            $stmt->execute();

            return $this->pdo->lastInsertId();
		}
		
		public function getFornecedores() {
			$stmt = $this->pdo->prepare('SELECT co_fornecedor, nu_cnpj FROM fornecedores');
            $stmt->execute();
			return $stmt->fetchAll();
		}

        /*
         * cadastra fornecedor caso não esteja cadastrado
         */
        public function cadastraFornecedor($data)
        {
            App::import('Model', 'Fornecedor');
            $fornecedorModel = new Fornecedor();
            $fornecedor = $fornecedorModel->findByNuCnpj($data['nu_cnpj']);

            if (count($fornecedor['Fornecedor']) > 0) {
                $stmt = $this->pdo->prepare("UPDATE fornecedores
                                             SET tp_fornecedor = '".$data['tp_fornecedor']."', nu_cnpj = '".$data['nu_cnpj']."', no_razao_social = '". (str_replace("'", " ", $data['no_razao_social'])) ."'
                                             WHERE co_fornecedor = '".$fornecedor['Fornecedor']['co_fornecedor']."'");
                $stmt->execute();
            } else {
                $stmt = $this->pdo->prepare("INSERT INTO fornecedores (tp_fornecedor, nu_cnpj, no_razao_social)
                                             VALUES ('".$data['tp_fornecedor']."', '".$data['nu_cnpj']."', '". (str_replace("'", " ", $data['no_razao_social'])) ."')");
                $stmt->execute();
            }
        }

        /*
         * cadastra modalidade caso não esteja cadastrado
         */
        public function cadastraModalidade($data)
        {
            App::import('Model', 'Modalidade');
            $modalidadeModel = new Modalidade();

            $stmt = $this->pdo->prepare("SELECT nu_modalidade FROM modalidades ORDER BY nu_modalidade DESC LIMIT 1;");
            $stmt->execute();
            $ultimaModalidade = $stmt->fetchAll();

            $modalidade = $modalidadeModel->findByDsModalidade($data['ds_modalidade']);
            if ($modalidade) {
                return false;
            } else {
                if(isset($ultimaModalidade[0])){
                    $stmt = $this->pdo->prepare("INSERT INTO modalidades (nu_modalidade, ds_modalidade)
                                             VALUES ('".($ultimaModalidade[0]->nu_modalidade + 1)."','".$data['ds_modalidade']."')");
                    $stmt->execute();
                } else {
                    $stmt = $this->pdo->prepare("INSERT INTO modalidades (nu_modalidade, ds_modalidade)
                                             VALUES ('1','".$data['ds_modalidade']."')");
                    $stmt->execute();
                }
            }
        }

        /*
         * cadastra contreatacao caso não esteja cadastrado
         */
        public function cadastraContratacao($data)
        {
            App::import('Model', 'Contratacao');
            $contratacaoModalidade = new Contratacao();
            $contratacao = $contratacaoModalidade->findByDsContratacao($data['ds_contratacao']);

            if ($contratacao) {
                return false;
            } else {
//                debug($this->pdo);die;

                $stmt = $this->pdo->prepare("INSERT INTO contratacoes (ds_contratacao)
                                             VALUES ('". $data['ds_contratacao'] ."')");

                $stmt->execute();
            }
        }

	}
	
?>