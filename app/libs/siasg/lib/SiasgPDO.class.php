<?php
	require_once 'PDO.class.php';

	/**
	 * SiasgPDO
	 *
	 * @author leo
	 */
	class SiasgPDO extends PDO {

	    private $host, $port, $dbName, $user, $pass;

	    private function findConnection() {
	    	$connections = Configure::read('App.config.database.default');

	    	$this->host   = $connections['host'];
	    	$this->port   = '3306';
	    	$this->dbName = $connections['database'];
	    	$this->user   = $connections['login'];
	    	$this->pass   = $connections['password'];
	    }

	    public function __construct($options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')) {
	    	$this->findConnection();
	        parent::__construct('mysql:host='.$this->host.';port='.$this->port.';dbname='.$this->dbName,
								$this->user,
								$this->pass,$options);
	    }

	    public function query($query) {
	        $args = func_get_args();
	        array_shift($args);
	        $reponse = parent::prepare($query);
	        $reponse->execute($args);
	        return $reponse;
	    }

	    public function insecureQuery($query) {
	        return parent::query($query);
	    }

	}
?>