<?php

class Acao extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'Acao';

    public $useTable = 'acoes';

    public $validate = array(
        'co_acao' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'ds_acao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        
    );
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    public $hasAndBelongsToMany = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'joinTable' => 'usuarios_acoes',
            'foreignKey' => 'acao_id',
            'associationForeignKey' => 'usuario_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

    public function beforeDelete() {
        return parent::beforeDelete();
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>