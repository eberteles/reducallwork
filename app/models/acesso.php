<?php

class Acesso extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'Acesso';

    public $useTable = 'acessos';

    public $validate = array(
        'co_acesso' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'ds_acesso' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        )
    );
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    public $hasAndBelongsToMany = array(
        'Privilegio' => array(
            'className' => 'Privilegio',
            'joinTable' => 'privilegios_acessos',
            'foreignKey' => 'acesso_id',
            'associationForeignKey' => 'privilegio_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>