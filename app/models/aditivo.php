<?php

class Aditivo extends AppModel
{
    /**
     * array(
     *    'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    const ADITIVO_VALOR = 1;
    const ADITIVO_PRAZO = 2;
    const ADITIVO_VALOR_PRAZO = 3;
    const ADITIVO_OUTROS = 4;
    const ADITIVO_VALOR_ACRESCIMO = 1;
    const ADITIVO_VALOR_SUPRESSAO = 5;
    const ADITIVO_VALOR_REEQUILIBRIO = 6;
    const ADITIVO_VALOR_REPACTUACAO = 7;
    const ADITIVO_VALOR_REAJUSTE = 8;

    /**
     * Tipos de Aditivos.
     *
     * @var array
     */
    public static function getTipos()
    {
        return array(
            self::ADITIVO_VALOR => __('Aditivo de Valor', true),
            self::ADITIVO_PRAZO => __('Aditivo de Prazo', true),
            self::ADITIVO_VALOR_PRAZO => __('Aditivo de Valor e Prazo', true),
            self::ADITIVO_OUTROS => __('Aditivo (Outros)', true),
        );
    }

    /**
     * Classificação do Aditivo de Valor.
     *
     * @var array
     */
    public static function getClassificacaoAditivoValor()
    {
        return array(
            self::ADITIVO_VALOR_ACRESCIMO => __('Acréscimo', true),
            self::ADITIVO_VALOR_SUPRESSAO => __('Supressão', true),
            self::ADITIVO_VALOR_REEQUILIBRIO => __('Reequilíbrio', true),
            self::ADITIVO_VALOR_REPACTUACAO => __('Repactuação', true),
            self::ADITIVO_VALOR_REAJUSTE => __('Reajuste', true),
        );
    }

    public $name = 'Aditivo';

    public $useTable = 'aditivos';

    public $primaryKey = 'co_aditivo';

    public $validate = array(

        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'tp_aditivo' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Tipo de Aditivo em branco'
            )
        ),
        'dt_aditivo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data em branco'
            )
        ),
        'dt_prazo' => array(
            'rule' => array(
                'validarDtPrazoMaiorQueDtPublicacaoAditivo'
            ),
            'message' => 'O Prazo do Aditivo deve ser maior que a Data de Fim da Vigência Atual.',
        )/*,
        'dt_publicacao' => array(
            'rule' => array(
                'validaDtPublicacaoAditivo',
                true
            ),
            'message' => 'A Publicação do Aditivo deve ser maior ou igual que a Data de Assinatura.',
            'allowEmpty' => true
        )*/,
        'ds_aditivo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Justificativa em branco'
            )
        ),
        'vl_aditivo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Valor Aditivo em branco',
                'allowEmpty' => false
            )
        ),
        'tp_aditivo_valor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Selecione a classificação do Aditivo'
            )
        )
    );

    function validaDtPrazoAditivo()
    {
        if (isset($this->data['Aditivo']['dt_fim_vigencia_anterior'])) {
            if (!isset($this->data['Aditivo']['dt_prazo'])) {
                return false;
            }
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias(
                $this->data['Aditivo']['dt_fim_vigencia_anterior'],
                $this->data['Aditivo']['dt_prazo'],
                "US"
            );
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }

    function validarDtPrazoMaiorQueDtPublicacaoAditivo()
    {
        $supports = array(self::ADITIVO_PRAZO, self::ADITIVO_VALOR_PRAZO);
        if (!in_array($this->data['Aditivo']['tp_aditivo'], $supports)) {
            return true;
        }

        $dataIniVigencia = null;
        if (isset($this->data['Aditivo']['dt_publicacao']) && $this->data['Aditivo']['dt_publicacao']) {
            $dataIniVigencia = new DateTime($this->data['Aditivo']['dt_publicacao']);
        }

        $dataFimVigencia = null;
        if (isset($this->data['Aditivo']['dt_prazo']) && $this->data['Aditivo']['dt_prazo']) {
            $dataFimVigencia = new DateTime($this->data['Aditivo']['dt_prazo']);
        }

        if (!$dataIniVigencia || !$dataFimVigencia) {
            return false;
        }

        $diferenca = $dataIniVigencia->diff($dataFimVigencia);
        if ($diferenca->days <= 0) {
            return false;
        }
        return true;
    }

    function validaDtPublicacaoAditivo()
    {

        if (isset($this->data['Aditivo']['dt_publicacao'])) {
            if (!isset($this->data['Aditivo']['dt_assinatura'])) {
                return false;
            }
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias(
                $this->data['Aditivo']['dt_assinatura'],
                $this->data['Aditivo']['dt_publicacao'],
                "US"
            );
            if ($diferenca < 0) {
                return false;
            }
        }
        return true;
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function beforeValidate($options = array())
    {
        if ($this->data['Aditivo']['tp_aditivo'] == 1) {
            $this->validate['dt_aditivo']['notempty']['allowEmpty'] = true;
        }

        //Somente os aditivos 5 e 6 nao possuem essa validacao
        if (in_array($this->data['Aditivo']['tp_aditivo'], array(1, 2, 3, 4, 7, 8))) {
            $this->validate['dt_assinatura'] = array(
                'notempty' => array(
                    'rule' => array(
                        'notempty'
                    ),
                    'message' => 'Campo Data de assinatura em branco'
                )
            );

        }
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        if (empty($this->data['Aditivo']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Aditivo']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        $this->data['Aditivo']['no_aditivo'] = up($this->data['Aditivo']['no_aditivo']);
        $this->data['Aditivo']['ds_aditivo'] = up($this->data['Aditivo']['ds_aditivo']);

        if (isset($this->data['Aditivo']['vl_aditivo'])) {
            $this->data['Aditivo']['vl_aditivo'] = ln($this->data['Aditivo']['vl_aditivo']);
        }

        if (isset($this->data['Aditivo']['pc_aditivo'])) {
            $this->data['Aditivo']['pc_aditivo'] = ln($this->data['Aditivo']['pc_aditivo']);
        }

        if (empty($this->data['Aditivo']['dt_prazo']) || $this->data['Aditivo']['dt_prazo'] == '') {
            $this->data['Aditivo']['dt_prazo'] = null;
        }

        if (empty($this->data['Aditivo']['dt_aditivo']) || $this->data['Aditivo']['dt_aditivo'] == '') {
            $this->data['Aditivo']['dt_aditivo'] = null;
        }

        if (empty($this->data['Aditivo']['dt_fim_vigencia_anterior'])) {
            $this->data['Aditivo']['dt_fim_vigencia_anterior'] = null;
        }

        if (empty($this->data['Aditivo']['dt_publicacao'])) {
            $this->data['Aditivo']['dt_publicacao'] = null;
        }

        if (empty($this->data['Aditivo']['dt_assinatura'])) {
            $this->data['Aditivo']['dt_assinatura'] = null;
        }

        if (empty($this->data['Aditivo']['dt_fim_autorizacao'])) {
            $this->data['Aditivo']['dt_fim_autorizacao'] = null;
        }

        if (empty($this->data['Aditivo']['co_aditivo'])) {
            unset($this->data['Aditivo']['co_aditivo']);
        }

        return parent::beforeSave($options);
    }

    public function getTotalPercentual($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
                                    WHERE co_contrato = '$co_contrato' AND tp_aditivo in (" . Aditivo::ADITIVO_VALOR_ACRESCIMO . ", " . Aditivo::ADITIVO_VALOR_PRAZO . ")
                                    AND (tp_aditivo_valor NOT IN (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ", " . Aditivo::ADITIVO_VALOR_REEQUILIBRIO . ", " . Aditivo::ADITIVO_VALOR_REPACTUACAO . ", " . Aditivo::ADITIVO_VALOR_REAJUSTE . ") OR tp_aditivo_valor IS NUll)");
        $result2 = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
                                    WHERE co_contrato = '$co_contrato' AND tp_aditivo_valor in (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ")");

        return $result[0][0]['vl_total_aditivo'] - $result2[0][0]['vl_total_aditivo'];
    }

    public function getTotal($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
                                    WHERE co_contrato = '$co_contrato' AND tp_aditivo in (" . Aditivo::ADITIVO_VALOR_ACRESCIMO . ", " . Aditivo::ADITIVO_VALOR_PRAZO . ")
                                    AND (tp_aditivo_valor <> " . Aditivo::ADITIVO_VALOR_SUPRESSAO . " OR tp_aditivo_valor IS NUll)");
        $result2 = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
                                    WHERE co_contrato = '$co_contrato' AND tp_aditivo_valor in (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ")");

        return $result[0][0]['vl_total_aditivo'] - $result2[0][0]['vl_total_aditivo'];
    }

    // Aditivo de Reajuste
    // TODO: Verificar a necessidade de tp_aditivo in***
    public function getTotalComReajuste($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
            WHERE co_contrato = '$co_contrato' AND tp_aditivo in (" . Aditivo::ADITIVO_VALOR_ACRESCIMO . ", " . Aditivo::ADITIVO_VALOR_PRAZO . ", " . Aditivo::ADITIVO_VALOR_REEQUILIBRIO . ", " . Aditivo::ADITIVO_VALOR_REPACTUACAO . ", " . Aditivo::ADITIVO_VALOR_REAJUSTE . ") AND tp_aditivo_valor <> " . Aditivo::ADITIVO_VALOR_SUPRESSAO . "");
        $result2 = $this->query("SELECT SUM( vl_aditivo ) as vl_total_aditivo FROM aditivos
            WHERE co_contrato = '$co_contrato' AND tp_aditivo_valor in (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ")");
        return $result[0][0]['vl_total_aditivo'] - $result2[0][0]['vl_total_aditivo'];
    }

    public function getTotalGlobal($co_contrato)
    {
        $result = $this->query("SELECT nu_contrato, nu_processo, vl_global, sum(vl_aditivo) as vl_total_aditivo FROM contratos
                LEFT JOIN aditivos on (aditivos .co_contrato = contratos.co_contrato)
                WHERE contratos.co_contrato = $co_contrato ");
        $aditivo['nu_contrato'] = $result[0]['contratos']['nu_contrato'];
        $aditivo['nu_processo'] = $result[0]['contratos']['nu_processo'];
        $aditivo['vl_global'] = $result[0]['contratos']['vl_global'];
        $aditivo['vl_total_aditivo'] = $result[0][0]['vl_total_aditivo'];
        return $aditivo;
    }

    public function getFiscais($coContrato)
    {
        $query = "SELECT ds_nome, ds_email FROM usuarios Usuario JOIN contratos_fiscais ContratoFiscal
        ON (ContratoFiscal.co_usuario_fiscal=Usuario.co_usuario) JOIN contratos Contrato
        ON (Contrato.co_contrato=ContratoFiscal.co_contrato)";
        $query .= " WHERE Contrato.co_contrato = $coContrato";

        return $this->query($query);
    }

    public function getGestorAtivo($co_contrato)
    {
        return $result = $this->query("SELECT ds_nome, ds_email FROM contratos Contrato
                LEFT JOIN usuarios Usuario ON ( Usuario.co_usuario = Contrato.co_fiscal_atual || Usuario.co_usuario = Contrato.co_gestor_atual )
                WHERE Contrato.co_contrato = $co_contrato ");
    }

    public function getNumerosAditivos()
    {
        $contratos = array();
        $r = $this->query("SELECT co_contrato, vl_aditivo, dt_aditivo, dt_fim_vigencia_anterior, dt_prazo FROM aditivos");
        if (count($r) > 0) {
            foreach ($r as $a) {
                $r2 = $this->query("SELECT nu_contrato FROM contratos WHERE co_contrato=" . $a['aditivos']['co_contrato']);
                if (!empty($r2)) {
                    if ($r2[0]['contratos']['nu_contrato'] != '') {
                        $contratos[$r2[0]['contratos']['nu_contrato']][] = $a['aditivos'];
                    }
                }
            }
        }
        return $contratos;
    }

    public function insertIfNotExists($aditivo)
    {
        return $this->insertOrUpdate($aditivo, false);
    }

    public function insertOrUpdate($aditivo, $update = true)
    {
        $siasg = new Siasg();
        $id = null;
        $new = false;
        $qC = 'SELECT co_aditivo FROM aditivos WHERE no_aditivo="' . $aditivo['no_aditivo'] . '" AND co_contrato = "' . $aditivo['co_contrato'] . '"';
        $r = $this->query($qC);
        if (sizeof($r) > 0) {
            $id = $r[0]['aditivos']['co_aditivo'];
        }

        if (!$id) {
            $new = true;
            $query = "INSERT INTO aditivos(";
            foreach ($aditivo as $k => $v) {
                $query .= $k . ",";
            }
            $query = substr($query, 0, -1);
            $query .= ") VALUES(";
            foreach ($aditivo as $k => $v) {
                if ($v == '---') {
                    $v = null;
                }
                $query .= '"' . $v . '",';
            }
            $query = substr($query, 0, -1);
            $query .= ")";
            $id = $siasg->query($query);
        } elseif ($update) {
            $query = "UPDATE aditivos SET ";
            foreach ($aditivo as $k => $v) {
                if (($v != null) && ($v != '') && ($k != 'dt_fim_vigencia')) {
                    $query .= $k . '="' . $v . '",';
                }
            }
            $query = substr($query, 0, -1);
            $query .= " WHERE co_aditivo=" . $id;
            $siasg->query($query);
        }

        $ret = array(
            'new' => $new,
            'id' => $id
        );

        return $ret;
    }

    public function getTotalPcAditivo($co_contrato)
    {
        $result = $this->query("SELECT SUM( pc_aditivo ) as pc_total_aditivo FROM aditivos
            WHERE co_contrato = '$co_contrato'
            AND tp_aditivo in (" . Aditivo::ADITIVO_VALOR_ACRESCIMO . ", " . Aditivo::ADITIVO_VALOR_PRAZO . ")
            AND tp_aditivo_valor NOT IN (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ", " . Aditivo::ADITIVO_VALOR_REEQUILIBRIO . ", " . Aditivo::ADITIVO_VALOR_REPACTUACAO . ", " . Aditivo::ADITIVO_VALOR_REAJUSTE . ")");
        $result2 = $this->query("SELECT SUM( pc_aditivo ) as pc_total_aditivo FROM aditivos
            WHERE co_contrato = '$co_contrato' AND tp_aditivo_valor in (" . Aditivo::ADITIVO_VALOR_SUPRESSAO . ")");
        return $result[0][0]['pc_total_aditivo'] - $result2[0][0]['pc_total_aditivo'];
    }
}

?>
