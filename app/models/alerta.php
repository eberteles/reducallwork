<?php

class Alerta extends AppModel
{

    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
	
    public $name = 'Alerta';

    public $useTable = 'alertas';

    public $primaryKey = 'co_alerta';

    public $validate = array(
        'co_alerta' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        )
        
    );

    public function afterSave($created = true)
    {
        if(!isset($this->data['co_alerta']) || $this->data['co_alerta'] == null || $this->data['co_alerta']){
            $nextId = $this->find('first', array('conditions' => array('co_alerta is not' => null),
                'order' => array('co_alerta' => 'DESC') ));

            $this->data['Alerta']['co_alerta'] = $nextId['Alerta']['co_alerta'] + 1;
        }

        return parent::afterSave($created);
    }
}
?>