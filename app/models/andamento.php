<?php

class Andamento extends AppModel
{
	protected $_logAction = array(
		'INSERT',
		'UPDATE',
		'DELETE'
	);
	
    public $name = 'Andamento';

    public $useTable = 'andamentos';

    public $primaryKey = 'co_andamento';

    public $validate = array(
        'co_andamento' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        )
        ,
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        )
        
    );

    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        ),
        'Fluxo' => array(
            'className' => 'Fluxo',
            'foreignKey' => 'nu_sequencia'
        ),
        'Fase' => array(
            'className' => 'Fase',
            'foreignKey' => 'co_fase'
        )
    );
    
    var $EmailProvider = null;
    var $print = null;
    
    function __construct()
    {
        App::import('Component', 'Email');
        App::import('Helper', 'Print');

        $this->EmailProvider = new EmailComponent();
        $this->print         = new PrintHelper();
        
        if (Configure::read('App.config.resource.certificadoDigital')) {
            $this->belongsTo['Assinatura']  = array(
                'className' => 'Assinatura',
                'foreignKey' => 'assinatura_id'
            );
        }
        parent::__construct();
    }

    function beforeValidate($options = array())
    {
        $this->data['Andamento']['ds_andamento'] = up($this->data['Andamento']['ds_andamento']);
        
        if (isset($this->data['Andamento']['ds_justificativa'])) {
            $this->data['Andamento']['ds_justificativa'] = up($this->data['Andamento']['ds_justificativa']);
        }
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        if(!isset($this->data['co_andamento']) || $this->data['co_andamento'] == null || $this->data['co_andamento']){
            $nextId = $this->find('first', array('conditions' => array('co_andamento is not' => null),
                'order' => array('co_andamento' => 'DESC') ));

            $this->data['Andamento']['co_andamento'] = $nextId['Andamento']['co_andamento'] + 1;
        }
        
        App::import('Model', 'Usuario');
        App::import('Model', 'Contrato');
        $modelUsuario   = new Usuario();
        $modelContrato  = new Contrato();
        
        $contrato       = $modelContrato->findByCoContrato($this->data['Andamento']['co_contrato']);
        
        $destinatarios  = $modelUsuario->find('all', array('conditions' => array('Usuario.ic_acesso' => 1, 'Usuario.ic_ativo' => 1 )));
        
        foreach ($destinatarios as $destinatario) {
            $this->avisarAndamento($destinatario['Usuario'], $contrato);
        }
        
        parent::afterSave($created);
    }
    
    private function getNuRegistro($contrato) {
        if($contrato['nu_contrato'] != '') {
            return array('Contrato', $this->print->contrato($contrato['nu_contrato']));
        }
        if($contrato['nu_processo'] != '') {
            return array('Processo', $this->print->processo($contrato['nu_processo']));
        }
        if($contrato['nu_pam'] != '') {
            return array('Prospecção', $this->print->pam($contrato['nu_pam']));
        }
    }

    private function avisarAndamento($usuario, $contrato){
        $this->EmailProvider->to = $usuario['ds_nome'] . ' <' . $usuario['ds_email'] . '>';
        //$this->EmailProvider->to = 'Eber <eberteles@hotmail.com>';
        
        $registro   = $this->getNuRegistro($contrato['Contrato']);

        $this->EmailProvider->subject = $registro[1] . ' - ' . $this->data['Andamento']['ds_andamento'];

        $mensagem = $usuario['ds_nome'] . ',<br><br>';
        $mensagem .= '&nbsp;&nbsp;&nbsp;' . $this->data['Andamento']['ds_andamento'] . '<br><br>';
        if ( $contrato['Contrato']['co_cliente'] > 0  ) {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>' . __('Cliente', true) . '</b>: ' . strtoupper($contrato['Cliente']['no_razao_social']);
        }
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>' . $registro[0] . '</b>: ' . $registro[1];
        if ( $contrato['contratos']['nu_contrato'] != '') {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Contrato</b>: ' . $this->contrato($contrato['Contrato']['nu_contrato']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Assinatura</b>: ' . FunctionsComponent::data($contrato['Contrato']['dt_assinatura']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Vigência</b>: ' . FunctionsComponent::data($contrato['Contrato']['dt_ini_vigencia']) . ' à ' . FunctionsComponent::data($contrato['contratos']['dt_fim_vigencia']);
        } else {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Valor Estimado das Contas</b>: ' . $contrato['Contrato']['vl_global'];
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Data Inicial</b>: ' . FunctionsComponent::data($contrato['Contrato']['dt_autorizacao_pam']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Responsável / Parceiro</b>: ' . strtoupper($contrato['Parceiro']['ds_nome']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Responsável Reducall</b>: ' . strtoupper($contrato['GestorAtual']['ds_nome']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Fase Atual</b>: ' . strtoupper($contrato['Fase']['ds_fase']);
        }
        $mensagem .= '<br><br><br> -------------------------------------------------------------------';
        $mensagem .= '<br>Este é um aviso automático, favor não responder este email.';
        
        $this->EmailProvider->send($mensagem);
    }
    
    public function listarProcessosFaseExpiranda()
    {
        return $this->query("
                select `Andamento`.co_andamento, `Contrato`.nu_pam, `Fase`.ds_fase, `Andamento`.dt_andamento, 
                        date_add(`Andamento`.dt_andamento, interval `Fase`.mm_duracao_fase minute) as pz_execucao, 
                        `Setor`.co_setor, `Setor`.ds_setor, `Setor`.ds_email from `andamentos` AS `Andamento` 
                   LEFT JOIN `fluxos` AS `Fluxo` ON (`Andamento`.`nu_sequencia` = `Fluxo`.`nu_sequencia`) 
                   LEFT JOIN `fases` AS `Fase` ON (`Fluxo`.`co_fase` = `Fase`.`co_fase`) 
                   LEFT JOIN `contratos` AS `Contrato` ON (`Andamento`.`co_contrato` = `Contrato`.`co_contrato`)
                   LEFT JOIN `setores` AS `Setor` ON (`Contrato`.`co_setor` = `Setor`.`co_setor`)
                where 
                   `Andamento`.co_contrato > 0 and `Andamento`.nu_sequencia > 0 and `Andamento`.dt_fim is null and 
                   `Fase`.mm_duracao_fase > 0 and `Andamento`.ic_aviso_expiracao = 0 and 
                    date_add(`Andamento`.dt_andamento, interval `Fase`.mm_duracao_fase minute) < now()
            ");
    }
}
?>