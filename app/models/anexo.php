<?php

class Anexo extends AppModel
{
    /**
     * array(
     *    'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'Anexo';

    public $useTable = 'anexos';

    public $primaryKey = 'co_anexo';

    private $tmp_name_arquivo = '';

    public $validate = array(

        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'tp_documento' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Tipo de Documento deve ser informado.'
            )
        ),
        'ds_anexo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        ),
        'conteudo' => array(
            'anexo' => array(
                'rule' => array('extension', array('doc', 'docx', 'odt', 'xls', 'xlsx', 'ods', 'ppt', 'pptx', 'odp', 'pdf', 'htm', 'html', 'txt', 'jpg', 'png', 'tif', 'cdr', 'svg', 'odg')),
                'message' => 'Campo Anexo não permite o tipo de arquivo selecionado.',
                'allowEmpty' => false
            ),
            'tamanho' => array(
                'rule' => 'validaTamanhoAnexo',
                'message' => 'Campo Anexo não pode ser maior que 15Mb.',
                'allowEmpty' => true
            )
        )
    );

    public $belongsTo = array(
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'co_atividade'
        ),
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_licitacao'
        )
    );

    function validaTamanhoAnexo()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (filesize($this->data['Anexo']['conteudo']['tmp_name']) > (int)$modulo->getFilemaxsize()) {
            return false;
        } else {
            return true;
        }
    }

    public function beforeValidate($options)
    {
        if (empty($this->data['Anexo']['conteudo']['name'])) {
            $this->data['Anexo']['conteudo'] = null;
        }
        $this->data['Anexo']['ds_anexo'] = up($this->data['Anexo']['ds_anexo']);
        if (!empty($this->data['Anexo']['ds_observacao'])) {
            $this->data['Anexo']['ds_observacao'] = up($this->data['Anexo']['ds_observacao']);
        }

        if (empty($this->data['Anexo']['dt_anexo'])) {
            $this->data['Anexo']['dt_anexo'] = null;
        }

        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $this->data['Anexo']['ds_anexo'] = addslashes($this->data['Anexo']['ds_anexo']);

        if ($modulo->isDiretorioFisico()) {
            if (isset($this->data['Anexo']['conteudo']) && !empty($this->data['Anexo']['conteudo'])) {
                $nomeEDiretorio = $this->gerarNome($this->data["Anexo"]["conteudo"]["name"]);

                if (!$this->moverDocumento($this->data, $nomeEDiretorio)) {
                    return false;
                }
            }

            if (isset($this->data['Anexo']['conteudo']['name']) && !empty($this->data['Anexo']['conteudo']['name'])) {
                $this->data['Anexo']['ds_extensao'] = strtolower(end(explode('.', $this->data['Anexo']['conteudo']['name'])));
            }


            unset($this->data['Anexo']['conteudo']);
        } else {
            if (isset($this->data['Anexo']['conteudo']) && $this->data['Anexo']['conteudo']) {
                $this->prepararArquivoParaBanco($this->data['Anexo']['conteudo']);
            } else {
                unset($this->data['Anexo']['conteudo']);
            }
        }
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function afterDelete($options = array())
    {
        App::import('Model', 'AnexoIndexacao');
        $dbIndexacao = new AnexoIndexacao();

        $dbIndexacao->deleteAll(array('AnexoIndexacao.co_anexo' => $this->id));

        return parent::afterDelete($options);
    }

    private function prepararArquivoParaBanco(&$campo)
    {
        if ($campo) {
            $this->tmp_name_arquivo = $campo['tmp_name'];

            $file = new File($campo['tmp_name']);

            $this->data['Anexo']['ds_extensao'] = strtolower(end(explode('.', $this->data['Anexo']['conteudo']['name'])));

            $campo = $file->read();

            $file->close();
        }
    }

    function moverDocumento($documento, $nomeEDiretorio)
    {
        $dir = $nomeEDiretorio["diretorio"];
        $nome = $nomeEDiretorio["nome"];
        $tmpNome = $documento["Anexo"]["conteudo"]["tmp_name"];
        if (move_uploaded_file($tmpNome, $dir . $nome)) {
            return true;
        } else {
            return false;
        }
    }

    function gerarNome($novoNome)
    {
        $ext_length = strlen($novoNome) - strpos('.', $novoNome);
        $extensao = substr($novoNome, -$ext_length);
        $extensao = trim($extensao, '.');
        $extensao = strtolower($extensao);
        $geraNome = md5(uniqid(rand(), true));
        $dir = ROOT . DIRECTORY_SEPARATOR . APP_DIR . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR;
        $nome = $geraNome . '.' . $extensao;
        $gerado = array("nome" => $nome, "diretorio" => $dir);
        $this->data['Anexo']['caminho'] = $dir . $nome;
        return $gerado;
    }

    /**
     * Salva arquivo binário em um diretório fisico.
     *
     * @param  mixed $conteudo
     * @return string
     */
    public function salvarArquivoFisico($nome, $conteudo, $extensao)
    {
        $geraNome = md5(uniqid(rand(), true));
        $dir = ROOT . DIRECTORY_SEPARATOR . APP_DIR . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR;
        $nome = $geraNome . '.' . $extensao;
        $path = $dir . $nome;

        file_put_contents($path, $conteudo);

        return $path;
    }
}

?>