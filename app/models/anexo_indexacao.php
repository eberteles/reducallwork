<?php

class AnexoIndexacao extends AppModel
{

    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'AnexoIndexacao';

    public $useTable = 'anexos_indexacoes';

    public $primaryKey = 'co_anexo_indexacao';


    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>