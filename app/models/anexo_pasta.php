<?php

class AnexoPasta extends AppModel
{
    /**
     * array(
     *    'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'AnexoPasta';

    public $useTable = 'anexos_pastas';

    public $primaryKey = 'co_anexo_pasta';

    public $displayField = 'ds_anexo_pasta';

    public $order = "ds_anexo_pasta ASC";

    public $validate = array(
        'ds_anexo_pasta' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em Branco.'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUniqueFolder'
                ),
                'message' => 'Pasta já existe.'
            ),
        )
    );

    public $hasMany = array(
        'Anexo' => array(
            'className' => 'Anexo',
            'foreignKey' => 'co_anexo_pasta',
            'fields' => array('Anexo.co_anexo', 'Anexo.co_anexo_pasta', 'Anexo.co_atividade', 'Anexo.ds_extensao', 'Anexo.tp_documento', 'Anexo.ic_atesto',
                'Anexo.ds_observacao', 'Anexo.ds_anexo', 'Anexo.dt_anexo', 'Anexo.assinatura'),
            'dependent' => false,
        ),
        'AnexoPasta' => array(
            'className' => 'AnexoPasta',
            'foreignKey' => 'parent_id',
            'dependent' => true,
        ),
    );

    public function beforeValidate($options = array())
    {
        $this->data['AnexoPasta']['ds_anexo_pasta'] = up(addslashes($this->data['AnexoPasta']['ds_anexo_pasta']));
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        if (empty($this->data['AnexoPasta']['parent_id'])) {
            unset($this->data['AnexoPasta']['parent_id']);
        }

        $this->data['AnexoPasta']['ds_anexo_pasta'] = up(addslashes($this->data['AnexoPasta']['ds_anexo_pasta']));
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function isUniqueFolder($fields)
    {
        $conditions = array(
            $this->alias . '.ds_anexo_pasta' => $fields['ds_anexo_pasta'],
            $this->alias . '.co_contrato' => $this->data[$this->alias]['co_contrato'],
        );

        if (isset($this->data[$this->alias]['parent_id']) && $this->data[$this->alias]['parent_id']) {
            $conditions[$this->alias . '.parent_id'] = $this->data[$this->alias]['parent_id'];
        }

        return ($this->find('count', array('conditions' => $conditions)) == 0);
    }

}
