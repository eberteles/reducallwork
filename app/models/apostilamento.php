<?php

class Apostilamento extends AppModel
{
    /**
     * array(
     *    'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    const TP_APOSTILAMENTO_VALOR = 1;
    const TP_APOSTILAMENTO_PRAZO = 2;
    const TP_APOSTILAMENTO_VALOR_PRAZO = 3;
    const TP_APOSTILAMENTO_SUPRESSAO = 4;
    const TP_APOSTILAMENTO_REPACTUACAO = 5;
    const TP_APOSTILAMENTO_OUTROS = 6;

    /**
     * @return array
     */
    public static function getTipos()
    {
        return array(
            self::TP_APOSTILAMENTO_VALOR => __('Apostilamento de Valor', true),
            self::TP_APOSTILAMENTO_PRAZO => __('Apostilamento de Prazo', true),
            self::TP_APOSTILAMENTO_VALOR_PRAZO => __('Apostilamento de Valor e Prazo', true),
            self::TP_APOSTILAMENTO_SUPRESSAO => __('Apostilamento de Supressão', true),
            self::TP_APOSTILAMENTO_REPACTUACAO => __('Apostilamento de repactuação e reajuste', true),
            self::TP_APOSTILAMENTO_OUTROS => __('Outros', true)
        );
    }

    /**
     * @param $id
     * @return string|null
     */
    public static function getTipo($id)
    {
        $tipos = self::getTipos();
        return isset($tipos[$id]) ? $tipos[$id] : null;
    }

    var $name = 'Apostilamento';

    var $useTable = 'apostilamentos';

    var $primaryKey = 'co_apostilamento';

    var $validate = array(
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'no_apostilamento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        ),
        'tp_apostilamento' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Tipo de Apostilamento em branco'
            )
        ),
        'dt_apostilamento' => array(
            'date' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data do Apostilamento em branco'
            )
        ),
        /*este item possui validação no controller metodo add */
        'dt_prazo' => array(
            'date' => array(
                'rule' => array(
                    'validaDtPrazoApostilamento',
                    true
                ),
                'message' => 'O Prazo do Apostilamento deve ser maior que a Data de Fim da Vigência Atual.',
            )
        ),
        'ds_apostilamento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Justificativa em branco'
            )
        ),
        'vl_apostilamento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Valor Apostilamento em branco',
                'allowEmpty' => true
            )
        )
    );

    function validaDtPrazoApostilamento()
    {
        if (isset($this->data['Apostilamento']['dt_fim_vigencia_anterior'])) {
            if (!isset($this->data['Apostilamento']['dt_prazo'])) {
                return false;
            }
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Apostilamento']['dt_fim_vigencia_anterior'], $this->data['Apostilamento']['dt_prazo'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }

    public function beforeSave($options = array())
    {
        if (empty($this->data['Apostilamento']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Apostilamento']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        $this->data['Apostilamento']['no_apostilamento'] = up($this->data['Apostilamento']['no_apostilamento']);
        $this->data['Apostilamento']['ds_apostilamento'] = up($this->data['Apostilamento']['ds_apostilamento']);
        if (isset($this->data['Apostilamento']['vl_apostilamento'])) {
            $this->data['Apostilamento']['vl_apostilamento'] = ln($this->data['Apostilamento']['vl_apostilamento']);
        }

        if (empty($this->data['Apostilamento']['dt_prazo'])) {
            $this->data['Apostilamento']['dt_prazo'] = null;
        }

        if (isset($this->data['Apostilamento']['vl_mensal'])) {
            $this->data['Apostilamento']['vl_mensal'] = ln($this->data['Apostilamento']['vl_mensal']);
        }

        if (isset($this->data['Apostilamento']['vl_anual'])) {
            $this->data['Apostilamento']['vl_anual'] = ln($this->data['Apostilamento']['vl_anual']);
        }

        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function getTotal($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_apostilamento ) as vl_total_apostilamento FROM apostilamentos 
                                    WHERE co_contrato = '$co_contrato' AND tp_apostilamento in (1, 3) ");
        $result2 = $this->query("SELECT SUM( vl_apostilamento ) as vl_total_apostilamento FROM apostilamentos 
                                    WHERE co_contrato = '$co_contrato' AND tp_apostilamento in (4) ");
        return $result[0][0]['vl_total_apostilamento'] - $result2[0][0]['vl_total_apostilamento'];
    }

    public function getTotalGlobal($co_contrato)
    {
        $result = $this->query("SELECT nu_contrato, nu_processo, vl_global, sum(vl_apostilamento) as vl_total_apostilamento FROM contratos 
                LEFT JOIN apostilamentos on (apostilamentos .co_contrato = contratos.co_contrato) 
                WHERE contratos.co_contrato = $co_contrato ");
        $apostilamento['nu_contrato'] = $result[0]['contratos']['nu_contrato'];
        $apostilamento['nu_processo'] = $result[0]['contratos']['nu_processo'];
        $apostilamento['vl_global'] = $result[0]['contratos']['vl_global'];
        $apostilamento['vl_total_apostilamento'] = $result[0][0]['vl_total_apostilamento'];
        return $apostilamento;
    }

    public function getGestorAtivo($co_contrato)
    {
        return $result = $this->query("SELECT ds_nome, ds_email FROM contratos Contrato  
                LEFT JOIN usuarios Usuario ON ( Usuario.co_usuario = Contrato.co_fiscal_atual || Usuario.co_usuario = Contrato.co_gestor_atual ) 
                WHERE Contrato.co_contrato = $co_contrato ");
    }
}