<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::import('Sanitize');

/**
 * Application model for Cake.
 *
 * This is a placeholder class.
 * Create the same file in app/app_model.php
 * Add your application-wide methods to the class, your models will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.model
 */

class AppModel extends Model
{
	/**
	 * array(
	 * 	'INSERT',
	 *  'UPDATE',
	 *  'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array();

	/**
	 * (non-PHPdoc)
	 * @see cake/libs/model/Model#beforeValidate($options)
	 */
	public function beforeValidate( $options )
	{
		foreach ( $this->_schema as $field => $attr ) {
			switch ( $attr[ 'type' ] ) {
				case 'date':
					$this->formateDate( $this->data[ $this->name ][ $field ] );
					break;
				case 'datetime':
					$this->formateDateTime( $this->data[ $this->name ][ $field ] );
					break;
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * @see Model::afterSave()
	 */
	public function afterSave($created)
	{
		if( in_array( 'INSERT', $this->_logAction )
			&& $created ) {

			$log = array();
			$log['tp_acao']		= 'I';
			$log['ds_log']		= json_encode($this->data);

			$this->saveLog($log);
		}
		parent::afterSave($created);
	}

	/**
	 * {@inheritDoc}
	 * @see Model::beforeDelete()
	 */
	public function beforeDelete($cascade = true)
	{
		if( in_array( 'DELETE', $this->_logAction ) ) {

			$log = array();
			$log['tp_acao']		= 'E';
			$log['ds_log']		= json_encode($this->data);

			$this->saveLog($log);
		}
		return parent::beforeDelete($cascade);
	}

	/**
	 * {@inheritDoc}
	 * @see Model::beforeSave()
	 */
	public function beforeSave($options = array())
	{
		$this->data = Sanitize::clean($this->data, array('encode' => false));

		if( in_array( 'UPDATE', $this->_logAction )
		&& isset($this->data[$this->name][$this->primaryKey]) ) {

			$log = array();
			$log['tp_acao']		= 'A';
			$log['ds_log']		= json_encode($this->data);

			$this->saveLog($log);
		}

		return parent::beforeSave($options);
	}

	/**
	 * @param array $log
	 */
	public function saveLog( $log )
	{
		App::import('Component', 'Session');
		$sessao = new SessionComponent();
		$usuario = $sessao->read('usuario');

		$log['ds_tabela'] 	= $this->useTable;
		$log['ds_log']		= json_encode($this->data);
		$log['co_usuario'] 	= $usuario['Usuario']['co_usuario'];
		$log['ds_nome'] 	= $usuario['Usuario']['ds_nome'];
		$log['ds_email'] 	= $usuario['Usuario']['ds_email'];
		$log['nu_ip'] 		= $usuario['IP'];
		$log['dt_log'] 		= 'CURRENT_TIMESTAMP';

		$query_string = "INSERT INTO logs (dt_log, nu_ip, ds_nome, ds_email, ds_tabela, co_usuario, tp_acao, ds_log)
				  VALUES(<{dt_log: }>,'<{nu_ip: }>','<{ds_nome: }>','<{ds_email: }>','<{ds_tabela: }>','<{co_usuario: }>','<{tp_acao: }>','<{ds_log: }>');";

		$query = str_replace(array(
			'<{dt_log: }>',
			'<{nu_ip: }>',
			'<{ds_nome: }>',
			'<{ds_email: }>',
			'<{ds_tabela: }>',
			'<{co_usuario: }>',
			'<{tp_acao: }>',
			'<{ds_log: }>',
		), array(
			$log['dt_log'],
			$log['nu_ip'],
			$log['ds_nome'],
			$log['ds_email'],
			$log['ds_tabela'],
			$log['co_usuario'],
			$log['tp_acao'],
			$log['ds_log'],
		), $query_string);

		return $this->query($query);
	}

    /**
	 * (non-PHPdoc)
	 * @see cake/libs/model/Model#afterFind($results, $primary)
	 */
	public function afterFind( $results, $primary = false )
	{
		// TODO: quando habilidata, a função abaixo, eventualmente, congela o sistema, verificar o por que e habilitar a função!
        // $this->regenerarSessao();

		foreach ( $this->_schema as $field => $attr ) {
			switch ( $attr[ 'type' ] ) {
				case 'date':
					$function = "data";
					break;
				case 'datetime':
				case 'timestamp':
					$function = "dataHora";
					break;
				default:
					$function = "";
					break;
			}
			if ( !empty( $function ) ) {
				if ( isset( $results[ $this->name ][ $field ] ) ) {
					self::$function( $results[ $this->name ][ $field ] );
				} else {
					foreach ( $results as $key => $out ) {
                                            if ( isset( $results[ $key ] ) && isset( $results[ $key ][ $this->name ] ) && isset( $results[ $key ][ $this->name ][ $field ] ) ) {
                                                    self::$function( $results[ $key ][ $this->name ][ $field ] );
                                                }
                                        }
                                }
                        }
                }

		return $results;
	}

    public function regenerarSessao(){

    	if(!isset($_SESSION['componenteSessao']))
		{
			$_SESSION['componenteSessao'] = new SessionComponent();
		}

        $sessao = $_SESSION['componenteSessao'];


		$usuarioLogado = $sessao->read('usuario');

        if($usuarioLogado != null){
            if(!isset($_SESSION['number_finds']) || ($_SESSION['number_finds']) < 1){
                App::import('Helper', 'Modulo');
                $modulo = new ModuloHelper();
                $dataAtual = new DateTime();
                $to_time = strtotime($dataAtual->format('d/m/Y H:i:s'));
                $from_time = strtotime($usuarioLogado['Usuario']['dt_ult_acesso']);

                $diffMinutos = round(abs($to_time - $from_time) / 60,2);

                if( ($diffMinutos > Configure::read('App.config.resource.licenca.timeout.concorrente')) && ($usuarioLogado['Usuario']['licenca_concorrente'] == 1) ){
                    $this->renovarSessao($usuarioLogado);
                } else if( ($diffMinutos > Configure::read('App.config.resource.licenca.timeout.nominal')) && ($usuarioLogado['Usuario']['licenca_nominal'] == 1) ){
                    $this->renovarSessao($usuarioLogado);
                }
                $_SESSION['number_finds'] = 10000;
            } else {
                $_SESSION['number_finds'] = $_SESSION['number_finds'] - 1;
                //var_dump($_SESSION['number_finds']);
            }
        }
    }

    public function renovarSessao($usuarioLogado){
        App::import('Model', 'Usuario');
        $modelUsuario = new Usuario();
        $usuarioLogado['Usuario']['dt_ult_acesso'] = new DateTime();
        $modelUsuario->save($usuarioLogado);
    }

	/**
	 * Retorna a data no formato brasileiro
	 */
	static function data( &$sInput )
	{
		$sInput = strtotime( $sInput );

		if ( empty( $sInput ) ) {
			return null;
		}
		$sInput = date( "d/m/Y", $sInput );

		return $sInput;
	}

	/**
	 * Retorna a data e hora no formato brasileiro
	 */
	static function dataHora( &$sInput )
	{
		$sInput = strtotime( $sInput );

		if ( empty( $sInput ) ) {
			return null;
		}
		$sInput = date( "d/m/Y H:i:s", $sInput );

		return $sInput;
	}

	/**
	 * Converte a data para o formato americano
	 *
	 * @param unknown_type $data
	 * @return unknown
	 */
	function formateDate( &$data )
	{
		if ( strstr( $data, "/" ) ) {
			$d = explode( "/", $data );
			$rstData = "$d[2]-$d[1]-$d[0]";
			$data = $rstData;
		}

		return $data;
	}

	/**
	 * Converte a data para o formato americano
	 *
	 * @param unknown_type $data
	 * @return unknown
	 */
	function formateDateTime( &$data )
	{
		if ( is_string($data) && strstr( $data, "/" ) ) {
			$time = substr( $data, 11, 8 );
			$date = substr( $data, 0, 10 );

			$data = $this->formateDate( $date );

			if ( $time ) {
				$data = $data . ' ' . $time;
			}
		}

		return $data;
	}

	/**
	 *
	 * @param $field
	 * @param $compare_field
	 * @return unknown_type
	 */
	function identicalFieldValues( $field = array(), $compare_field = null )
	{
		foreach ( $field as $key => $value ) {
			$v1 = $value;
			$v2 = $this->data[ $this->name ][ $compare_field ];
			if ( $v1 !== $v2 ) {
				return false;
			} else {
				continue;
			}
		}
		return true;
	}

}

?>
