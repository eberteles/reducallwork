<?php

class Aquisicao extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Aquisicao';

    var $useTable = 'aquisicoes';

    var $primaryKey = 'co_aquisicao';

    var $displayField = 'ds_aquisicao';

    var $validate = array(
        
        'ds_aquisicao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (empty($this->data['Aquisicao']['dt_aquisicao'])) {
            $this->data['Aquisicao']['dt_aquisicao'] = null;
        }
        if (isset($this->data['Aquisicao']['ds_aquisicao'])) {
            $this->data['Aquisicao']['ds_aquisicao'] = up($this->data['Aquisicao']['ds_aquisicao']);
        }
        if (isset($this->data['Aquisicao']['vl_aquisicao'])) {
            $this->data['Aquisicao']['vl_aquisicao'] = ln($this->data['Aquisicao']['vl_aquisicao']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}
?>