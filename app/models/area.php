<?php

class Area extends AppModel
{
    /**
    * array(
    * 	'INSERT',
    *  'UPDATE',
    *  'DELETE',
    * )
    * @var array
    */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Area';

    var $userTable = 'areas';

    var $primaryKey = 'co_area';

    var $displayField = 'ds_area';

    var $validate = array(
        'ds_area' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ,
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data['Area']['ds_area'])) {
            $this->data['Area']['ds_area'] = up($this->data['Area']['ds_area']);
        }

        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}