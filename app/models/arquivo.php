<?php

class Arquivo extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Arquivo';

    var $useTable = 'arquivos';

    var $primaryKey = 'co_layout';

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
