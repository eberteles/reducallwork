<?php

class Assinatura extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'Assinatura';

    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Assinatura']['usuario_id'] = $usuario['Usuario']['co_usuario'];

        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
    
}
?>