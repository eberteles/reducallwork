<?php

class AssinaturasLogin extends AppModel
{
    /**
     * array(
     * 	'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'AssinaturasLogin';
    var $useTable = 'assinaturas_login';
    var $primaryKey = 'id';

    var $validate = array();

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }

    function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    function afterSave($created = true)
    {
        return parent::beforeSave($created);
    }
}