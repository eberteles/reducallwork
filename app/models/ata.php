<?php

class Ata extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Ata';

    var $useTable = 'atas';

    var $primaryKey = 'co_ata';

    var $displayField = 'nu_ata';

    var $order = "dt_cadastro  DESC";

    var $validate = array(
        'nu_ata' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nº Ata em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Campo Número de Ata já cadastrado.'
            )
        ),
        'nu_processo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Processo em branco'
            )
        ),
        'co_categoria_ata' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Categoria em branco'
            )
        ),
        'co_fornecedor' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Fornecedor em branco'
            )
        ),
        
        // 'ds_publicacao' => array(
        // 'notempty' => array(
        // 'rule' => array(
        // 'notempty'
        // ),
        // 'message' => 'Campo Publicação em branco'
        // )
        // ),
        'dt_vigencia_inicio' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Início da Vigência inválido'
            )
        ),
        'dt_vigencia_fim' => array(
            'validaDtFimVigencia' => array(
                'rule' => array(
                    'validaDtFimVigencia',
                    true
                ),
                'message' => 'A Data Final da Vigência deve ser maior que a Data de Início da Vigência.'
            ),
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Fim da Vigência inválido'
            )
        ),
        
        // 'vl_mensal' => array(
        // 'money' => array(
        // 'rule' => array(
        // 'comparison', '>', 0
        // ),
        // 'message' => 'Campo Valor Mensal em branco'
        // )
        // ),
        'vl_anual' => array(
            'money' => array(
                'rule' => array(
                    'comparison',
                    '>',
                    0
                ),
                'message' => 'Campo Valor Anual em branco'
            )
        )
    );

    function validaDtFimVigencia()
    {
        if (! isset($this->data['Ata']['dt_vigencia_inicio'])) {
            return false;
        } else {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Ata']['dt_vigencia_inicio'], $this->data['Ata']['dt_vigencia_fim'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }
    
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Categoria' => array(
            'className' => 'AtaCategoria',
            'foreignKey' => 'co_categoria_ata'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        )
    );

    public function beforeSave($options = array())
    {
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        if (empty($this->data['Ata']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Ata']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        $this->data['Ata']['nu_ata'] = up($this->data['Ata']['nu_ata']);
        $this->data['Ata']['nu_processo'] = up($this->data['Ata']['nu_processo']);
        $this->data['Ata']['ds_publicacao'] = up($this->data['Ata']['ds_publicacao']);
        $this->data['Ata']['ds_especificacao'] = up($this->data['Ata']['ds_especificacao']);
        $this->data['Ata']['ds_justificativa'] = up($this->data['Ata']['ds_justificativa']);
        if (empty($this->data['Ata']['dt_vigencia_inicio'])) {
            $this->data['Ata']['dt_vigencia_inicio'] = null;
        }
        if (empty($this->data['Ata']['dt_vigencia_fim'])) {
            $this->data['Ata']['dt_vigencia_fim'] = null;
        }
        if (empty($this->data['Ata']['dt_cadastro'])) {
            $this->data['Ata']['dt_cadastro'] = null;
        }
        if ($print->difEmDias(date('Y-m-d'), $this->data['Ata']['dt_vigencia_fim'], "US") < 0 && $this->data['Ata']['tp_status'] != 'C') {
            $this->data['Ata']['tp_status'] = 'E';
        }
        
        $this->data['Ata']['vl_mensal'] = ln($this->data['Ata']['vl_mensal']);
        $this->data['Ata']['vl_anual'] = ln($this->data['Ata']['vl_anual']);
        $this->data['Ata']['vl_ata'] = ln($this->data['Ata']['vl_ata']);
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }

}
?>