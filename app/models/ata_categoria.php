<?php

class AtaCategoria extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'AtaCategoria';

    var $useTable = 'atas_categorias';

    var $primaryKey = 'co_ata_categoria';

    var $displayField = 'ds_ata_categoria';

    var $validate = array(
        'ds_ata_categoria' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );

    function beforeSave($options = array())
    {
        $this->data['AtaCategoria']['ds_ata_categoria'] = up($this->data['AtaCategoria']['ds_ata_categoria']);
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}