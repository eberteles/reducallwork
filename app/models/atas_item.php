<?php

class AtasItem extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'AtasItem';

    public $useTable = 'atas_itens';

    public $primaryKey = 'co_ata_item';

    public $displayField = 'nu_item';

    public $validate = array(
        
        'nu_item' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Item em branco'
            )
        ),
        'ds_descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Objeto em branco'
            )
        ),
        'ds_unidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Unidade em branco'
            )
        ),
        'nu_quantidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Quantidade em branco'
            )
        ),
        'vl_unitario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Valor Unitário inválido'
            )
        )
    );

    public $belongsTo = array(
        'Lote' => array(
            'className' => 'AtasLote',
            'foreignKey' => 'co_ata_lote'
        )
    );
    
    // var $hasMany = array(
    // 'Pedido' => array(
    // 'className' => 'AtasPedido',
    // 'foreignKey' => 'co_ata_item'
    // )
    // );
    public function beforeSave($options = array())
    {
        if (isset($this->data['AtasItem']['ds_marca'])) {
            $this->data['AtasItem']['ds_marca'] = up($this->data['AtasItem']['ds_marca']);
        }
        if (isset($this->data['AtasItem']['ds_descricao'])) {
            $this->data['AtasItem']['ds_descricao'] = up($this->data['AtasItem']['ds_descricao']);
        }
        if (isset($this->data['AtasItem']['ds_unidade'])) {
            $this->data['AtasItem']['ds_unidade'] = up($this->data['AtasItem']['ds_unidade']);
        }
        if (isset($this->data['AtasItem']['vl_unitario'])) {
            $this->data['AtasItem']['vl_unitario'] = ln($this->data['AtasItem']['vl_unitario']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }

}
?>