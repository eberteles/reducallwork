<?php

class AtasLote extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'AtasLote';

    var $useTable = 'atas_lotes';

    var $primaryKey = 'co_ata_lote';

    var $displayField = 'nu_lote';

    var $order = 'nu_lote';

    var $validate = array(
        
        'nu_lote' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Lote em branco'
            )
        ),
        'ds_objeto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Objeto em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data['AtaLote']['nu_lote'])) {
            $this->data['AtaLote']['nu_lote'] = up($this->data['AtaLote']['nu_lote']);
        }
        if (isset($this->data['AtaLote']['ds_objeto'])) {
            $this->data['AtaLote']['ds_objeto'] = up($this->data['AtaLote']['ds_objeto']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}
?>