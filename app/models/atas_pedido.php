<?php

class AtasPedido extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'AtasPedido';

    var $useTable = 'atas_pedidos';

    var $primaryKey = 'co_ata_pedido';

    var $order = 'AtasPedido.dt_pedido DESC';

    var $validate = array(
        
        'ds_pedido' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Pedido em branco'
            ),
            'money' => array(
                'rule' => array(
                    'validaVlPedido',
                    true
                ),
                'message' => 'Campo Qt Pedir deve conter ao menos uma quantidade'
            )
        )
    );

    function validaVlPedido()
    {
        if ($this->data['AtasPedido']['vl_pedido'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    var $belongsTo = array(
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'co_contrato'
        )
    );

    var $hasMany = array(
        'PedidosItem' => array(
            'className' => 'PedidosItem',
            'foreignKey' => 'co_ata_pedido'
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data['AtasPedido']['ds_pedido'])) {
            $this->data['AtasPedido']['ds_pedido'] = up($this->data['AtasPedido']['ds_pedido']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}
?>