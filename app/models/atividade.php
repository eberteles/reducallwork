<?php

class Atividade extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Atividade';

    var $useTable = 'atividades';

    var $primaryKey = 'co_atividade';

    var $displayField = 'ds_atividade';

    var $validate = array(
        'co_atividade' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido.',
                'on' => 'update'
            )
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Contrato não foi informado.'
            )
        ),
        'co_responsavel' => array(
            'notempty' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo nome do responsável em branco'
            )
        ),
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => 'Campo nome da atividade em branco'
            )
        ),
        'ds_atividade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Atividade deve possuir de 3 a 100 caracteres.'
            )
            /*,'desc_atividade' => array(
                'rule' => array('valDescricao', 47),
                'message' => 'Nao foi possivel adicionar uma atividade'
            ),*/
        ),
        'dt_ini_planejado' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Data de Início de Planejamento inválida'
            ),
            'notEmpty', array(
                'rule' => array(
                    'notEmpty'
                ),
                'message' => ''
            )
        )
        ,'dt_fim_planejado' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Data de Término do Planejamento inválida'
            )
        )

    );

    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_responsavel'
        ),
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'co_contrato'
        )
    );

    function validaTamanho(){
        if (strlen($this->data['Atividade']['ds_atividade']) < 3) {
            return false;
        } elseif (strlen($this->data['Atividade']['ds_atividade']) > 100) {
            return false;
        } else {
            return true;
        }
    }

    function beforeValidate($options = array())
    {

        if (! empty($this->data['Atividade']['ds_atividade'])) {
            $this->data['Atividade']['ds_atividade'] = up($this->data['Atividade']['ds_atividade']);
        }

        if ($this->id) {
            unset($this->validate['dt_ini_planejado']);
            unset($this->validate['dt_fim_planejado']);
        }

        if (!$this->id && ! empty($this->data['Atividade']['dt_ini_planejado'])) {
            $this->data['Atividade']['dt_ini_planejado'] = up($this->data['Atividade']['dt_ini_planejado']);
        }

        if (!$this->id && ! empty($this->data['Atividade']['dt_fim_planejado'])) {
            $this->data['Atividade']['dt_fim_planejado'] = up($this->data['Atividade']['dt_fim_planejado']);
        }

        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');

        unset($this->data['Atividade']['edit_responsavel']);
        unset($this->data['Atividade']['edit_atividade']);
        $this->data['Atividade']['co_usuario'] = $usuario['Usuario']['co_usuario'];


        if (isset($this->data['Atividade']['co_atividade'])) {
            unset($this->data['Atividade']['dt_ini_planejado']);
            unset($this->data['Atividade']['dt_fim_planejado']);

            if (empty($this->data['Atividade']['dt_ini_execucao'])) {
                unset($this->data['Atividade']['dt_ini_execucao']);
            }

            if (empty($this->data['Atividade']['dt_fim_execucao'])) {
                unset($this->data['Atividade']['dt_fim_execucao']);
            }

            if (isset($this->data['Atividade']["parent_id"])) {
                $this->data['Atividade']['parent_id'] = $this->data['Atividade']['parent_id'] != 0 ? $this->data['Atividade']['parent_id'] : null;
            }
        } 

        return parent::beforeSave($options);
    }

   function valDescricao($check, $limit){
       $len = strlen($this->data['ds_atividade']);
       if ($len > 46) {
           if ($this->data['ds_atividade'][$len] == ' ') {
               return false;
           }
           if (strlen($this->data['ds_atividade']) < 3) {
               return false;
           }
           if (strlen($this->data['ds_atividade']) > 100) {
               return false;
           }
        } else {
           return true;
        }
   }

   public function all($tpAndamento, $coContrato) {
       $conditions['Atividade.co_contrato'] = $coContrato;
       
       if ($tpAndamento == 'N') {
           $conditions['Atividade.pc_executado <'] = 100;
           $conditions['Atividade.ic_encerrada'] = 0;
       }
       
       $atividades = $this->find('threaded', array(
            'fields' => array('Atividade.*', 'Usuario.ds_nome'),
            'conditions' => $conditions,
            'order' => array('Atividade.co_atividade' => 'DESC')
       ));

       $f1 = array();
        // filtro de atividades(concluídas ou encerradas)
        if (count($atividades)) {
            foreach ($atividades as $index => $atividadePai) {
                if ($tpAndamento == 'F') {
                    if ($atividadePai['Atividade']['pc_executado'] == 100) {
                        $f1[] = $atividadePai;
                    } else {
                        $countFilhas = 0;
                        $f3 = array();
                        $f3['Atividade'] = $atividadePai['Atividade'];
                        $f3['Usuario']['ds_nome'] = $atividadePai['Usuario']['ds_nome'];
                        foreach ($atividadePai['children'] as $index2 => $atividadeFilha) {
                            if ($atividadeFilha['Atividade']['pc_executado'] == 100) {
                                $f3['children'][] = $atividadeFilha;
                                $countFilhas++;
                            }
                        }

                        if ($countFilhas) {
                            $f1[] = $f3;
                        }
                    }
                } else if ($tpAndamento == 'C') {
                    if ($atividadePai['Atividade']['ic_encerrada']) {
                        $f1[] = $atividadePai;
                    } else {
                        $countFilhas = 0;
                        $f3 = array();
                        $f3['Atividade'] = $atividadePai['Atividade'];
                        $f3['Usuario']['ds_nome'] = $atividadePai['Usuario']['ds_nome'];
                        foreach ($atividadePai['children'] as $index2 => $atividadeFilha) {
                            if ($atividadeFilha['Atividade']['ic_encerrada']) {
                                $f3['children'][] = $atividadeFilha;
                                $countFilhas++;
                            }
                        }

                        if ($countFilhas) {
                            $f1[] = $f3;
                        }
                    }
                }
            }
        }

        if ($tpAndamento == 'C' || $tpAndamento == 'F') {
            $atividades = $f1;
        }

        return $atividades;
   }

   public function getFiscais($coContrato) {
       $query = "SELECT Usuario.co_usuario, Usuario.ds_nome FROM usuarios Usuario 
       JOIN contratos_fiscais ContratoFiscal ON (ContratoFiscal.co_usuario_fiscal=Usuario.co_usuario) 
       WHERE ContratoFiscal.ic_ativo=1 and ContratoFiscal.co_contrato = $coContrato";

       $result = $this->query($query);
       $fiscais = array();
       if ($result) {
           foreach ($result as $index => $fiscal) {
               $fiscais[$fiscal['Usuario']['co_usuario']] = $fiscal['Usuario']['ds_nome'];
            }
       }

       return $fiscais;
   }

   public function getGestor($coContrato) {
       $query = 'SELECT Usuario.co_usuario, Usuario.ds_nome FROM contratos Contrato 
       JOIN usuarios Usuario ON (Usuario.co_usuario=Contrato.co_gestor_atual) 
       WHERE Contrato.co_contrato = ' . $coContrato . ' LIMIT 1';

       return $this->query($query);
   }

    public function afterSave($created = true) {
        return parent::afterSave($created);
    }
}
?>
