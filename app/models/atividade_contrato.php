<?php

class AtividadeContrato extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name       = "AtividadeContrato";
    var $useTable   = "atividades_contratos";
    var $primaryKey = "id";
    var $recursive = 2;
    // var $displayField = 'Atividade.nome';

    var $validate = array(
    );

    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_responsavel'
        ),
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'id_atividade'
        ),
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'id_contrato'
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
