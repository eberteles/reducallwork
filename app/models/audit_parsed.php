<?php

class AuditParsed extends AppModel
{
    var $name       = 'AuditParsed';
    var $useTable   = 'audit_parsed';
    var $primaryKey = 'id_audit_parsed';
    var $useDbConfig= 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }


}
