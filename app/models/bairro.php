<?php

class Bairro extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Bairro';
    var $useTable = 'bairros';
    var $primaryKey = 'id';
    var $displayField = 'bairro';

    var $validate = array(
        'bairro' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Bairro em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Bairro já cadastrado'
            )
        )
    );

    var $hasMany = array(
        'LocalAtendimento' => array(
            'className' => 'LocalAtendimento',
            'foreignKey' => 'co_bairro'
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}