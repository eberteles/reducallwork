<?php

class Banco extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Banco';

    var $useTable = 'bancos';

    var $primaryKey = 'co_banco';

    var $displayField = 'ds_banco';

    var $virtualFields = array(
        'bancos' => 'CONCAT(Banco.nu_banco, " - ", Banco.ds_banco)'
    );

    var $validate = array(
        'nu_banco' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Banco em branco'
            )
        ),
        'ds_banco' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Banco em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Banco']['ds_banco'] = up($this->data['Banco']['ds_banco']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>