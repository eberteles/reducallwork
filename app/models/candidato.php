<?php

class Candidato extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Candidato';

    var $useTable = 'candidatos';

    var $primaryKey = 'co_candidato';

    var $displayField = 'no_nome';

    function beforeValidate($options = array())
    {
        $this->data['Candidato']['no_nome'] = up($this->data['Candidato']['no_nome']);
        $this->data['Candidato']['ds_orgao'] = up($this->data['Candidato']['ds_orgao']);
        $this->data['Candidato']['ds_endereco'] = up($this->data['Candidato']['ds_endereco']);
        $this->data['Candidato']['ds_email'] = up($this->data['Candidato']['ds_email']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>