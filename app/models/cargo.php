<?php

class Cargo extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Cargo';

    var $useTable = 'cargos';

    var $primaryKey = 'co_cargo';

    var $displayField = 'ds_cargo';

    var $validate = array(
        
        'ds_cargo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );
    
    public function beforeSave($options = array())
    {
        if (empty($this->data['Cargo']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Cargo']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        $this->data['Cargo']['ds_cargo'] = up($this->data['Cargo']['ds_cargo']);

        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>