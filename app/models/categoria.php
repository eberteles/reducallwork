<?php

class Categoria extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Categoria';

    var $useTable = 'categorias';

    var $primaryKey = 'co_categoria';

    var $displayField = 'no_categoria';

    var $validate = array(
        'no_categoria' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'myUnique' => array(
                'rule' => array('validateUnique'),
                'message' => 'Essa categoria já está cadastrada'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        )
    );

    function validateUnique($data) {
        $count = $this->find('count', array(
            'conditions' => array(
                'UPPER(Categoria.no_categoria)' => strtoupper($data['no_categoria']),
                'Categoria.ic_ativo' => 2
            )
        ));

        return $count ? false : true;
    }

    function findByName($name, $ativo=2) {
        $count = $this->find('count', array(
            'conditions' => array(
                'UPPER(Categoria.no_categoria)' => $name,
                'Categoria.ic_ativo' => $ativo
            )
        ));

        return $count ? true : false;
    }

    function beforeValidate($options = array())
    {
        $this->data['Categoria']['no_categoria'] = up($this->data['Categoria']['no_categoria']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($options = array())
    {
        return parent::beforeSave($options);
    }
}
?>