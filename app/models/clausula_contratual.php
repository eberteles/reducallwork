<?php

class ClausulaContratual extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'ClausulaContratual';

    public $useTable = 'clausulas_contratuais';

    public $validate = array(
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'nu_clausula' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número em branco'
            )
        ),
        'ds_clausula' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        $this->data[$this->name]['nu_clausula'] = up($this->data[$this->name]['nu_clausula']);
        $this->data[$this->name]['ds_clausula'] = up($this->data[$this->name]['ds_clausula']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>