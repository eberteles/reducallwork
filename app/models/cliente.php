<?php

class Cliente extends AppModel
{

    /**
     * array(
     *     'UPDATE',
     *     'INSERT',
     *   'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Cliente';

    var $useTable = 'clientes';

    var $primaryKey = 'co_cliente';

    var $displayField = 'no_razao_social';

    var $order = "no_razao_social  ASC";

//    var $virtualFields = array(
//        'nome_combo' => " CONCAT(
//                                if( Cliente.tp_cliente = 'J', mask(Cliente.nu_cnpj, '##.###.###/####-##'),
//                                if( Cliente.tp_cliente = 'F', mask(Cliente.nu_cnpj, '###.###.###-##'), Cliente.nu_cnpj) ),
//                                ' - ', Cliente.no_razao_social ) ",
//    );

    var $validate = array(
        'nu_cnpj' => array(
//            'notempty' => array(
//                'rule' => array(
//                    'notempty'
//                ),
//                'message' => 'Campo CNPJ/CPF em branco'
//            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'allowEmpty' => true,
                'message' => 'CNPJ/CPF já cadastrado.'
            ),
            'validaCnpj' => array(
                'rule' => array(
                    'validaCnpj',
                    true
                ),
                'message' => 'Campo CNPJ/CPF inválido.'
            )
        ),
        'no_razao_social' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Razão Social/Nome em branco'
            )
        ),
        'ds_email' => array(
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'E-mail já cadastrado.',
                'allowEmpty' => true
            ),
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Informe um e-mail válido!',
                'allowEmpty' => true
            )
        ),
        'nu_cpf_responsavel' => array(
            'rule' => array(
                'validaCpf',
                true
            ),
            'message' => 'CPF do Responsável inválido.',
            'allowEmpty' => true
        )
    );

    function validaCnpj()
    {
        if (!isset($this->data['Cliente']['nu_cnpj']) || $this->data['Cliente']['nu_cnpj'] == "") {
            return false;
        } else {
            if ($this->data['Cliente']['tp_cliente'] == 'J') {
                return FunctionsComponent::validaCNPJ($this->data['Cliente']['nu_cnpj']);
            } else {
                return FunctionsComponent::validaCPF($this->data['Cliente']['nu_cnpj']);
            }
        }
    }

    function validaCpf()
    {
        if (FunctionsComponent::validaCPF($this->data['Cliente']['nu_cpf_responsavel'])) {
            return true;
        } else {
            return false;
        }
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Municipio' => array(
            'className' => 'Municipio',
            'foreignKey' => 'co_municipio'
        ),

        'Banco' => array(
            'className' => 'Banco',
            'foreignKey' => 'co_banco'
        ),
        'Area' => array(
            'className' => 'Area',
            'foreignKey' => 'co_area'
        ),
        'SubArea' => array(
            'className' => 'Area',
            'foreignKey' => 'co_sub_area'
        )
    );

    var $hasMany = array(
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_cliente',
            'dependent' => false
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data['Cliente']['nu_cnpj'])) {
          $this->data['Cliente']['nu_cnpj'] = str_replace(array('-','.','/'), '', $this->data['Cliente']['nu_cnpj']);
        }
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        $this->data['Cliente']['no_razao_social'] = up($this->data['Cliente']['no_razao_social']);
        if (isset($this->data['Cliente']['ds_endereco'])) {
            $this->data['Cliente']['ds_endereco'] = up($this->data['Cliente']['ds_endereco']);
        }
        if (isset($this->data['Cliente']['ds_email'])) {
            $this->data['Cliente']['ds_email'] = up($this->data['Cliente']['ds_email']);
        }
        if (isset($this->data['Cliente']['no_responsavel'])) {
            $this->data['Cliente']['no_responsavel'] = up($this->data['Cliente']['no_responsavel']);
        }
        if (isset($this->data['Cliente']['no_preposto'])) {
            $this->data['Cliente']['no_preposto'] = up($this->data['Cliente']['no_preposto']);
        }

        if (isset($this->data['Cliente']['ds_observacao'])) {
            $this->data['Cliente']['ds_observacao'] = up($this->data['Cliente']['ds_observacao']);
        }

        return parent::afterSave($created);
    }

    // Validar a vigência da penalidade
    function beforeValidate($options = array())
    {
        if ((isset($this->data['Cliente']['dt_ini_penalidade']) && !empty($this->data['Cliente']['dt_ini_penalidade'])) && (isset($this->data['Cliente']['dt_fim_penalidade']) && !empty($this->data['Cliente']['dt_fim_penalidade']))) {
            $fimPenalidade = new DateTime(dtDb($this->data['Cliente']['dt_fim_penalidade']));
            $iniPenalidade = new DateTime(dtDb($this->data['Cliente']['dt_ini_penalidade']));
            $interval = $iniPenalidade->diff($fimPenalidade);

            if ($interval->format("%r%a") < 0) {
                $this->invalidate('dt_fim_penalidade', 'A data fim da penalidade não pode ser menor que a data de início');
            }
        }

        return parent::beforeValidate($options);
    }

    function beforeFind($queryData)
    {
        if (!isset($queryData['conditions']['Cliente.ic_ativo']))
            $queryData['conditions']['Cliente.ic_ativo'] = 1;

        return $queryData;
    }

    public function listarClientePorCnpj($cnpj)
    {
        return $this->query("SELECT co_cliente, nu_cnpj FROM `clientes` WHERE nu_cnpj = '$cnpj'");
    }

    public function listarCNPJaAjutar()
    {
        return $this->query("SELECT co_cliente, nu_cnpj FROM `clientes` WHERE character_length(nu_cnpj) < 14");
    }

    public function ajustarCNPJ($co_cliente, $nu_cnpj)
    {
        return $this->query("UPDATE clientes SET nu_cnpj = '$nu_cnpj' WHERE co_cliente = $co_cliente");
    }

    public function getCliente($co_contrato)
    {
        return $this->query("SELECT * FROM clientes Cliente join contratos c on c.co_cliente = Cliente.co_cliente where c.co_contrato = $co_contrato");
    }
}

?>
