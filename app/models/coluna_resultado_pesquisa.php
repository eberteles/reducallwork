<?php

class ColunaResultadoPesquisa extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'ColunaResultadoPesquisa';

    var $useTable = 'colunas_resultado_pesquisa';

    var $primaryKey = 'co_coluna_resultado';

    var $displayField = 'ds_nome';
    
    var $moduloHlp  = null;
    
    function findUsuario($tipo = 'list', $modulo = 'ctr', $not = false) {
        
        App::import('Helper', 'Modulo');
        $this->moduloHlp  = new ModuloHelper();
        $camposUsu  = $this->moduloHlp->getCamposPesquisa($modulo);

        // debug($modulo);exit;
        // debug('campos:' . $camposUsu);exit;

        $conditions = array( 'ic_modulo' => $modulo );
        
        if($not) {
            $conditions['NOT'] = array( 'co_coluna_resultado' => explode(",", $camposUsu));
            $order      = 'ds_nome ASC';
        }
        else {
            $conditions['co_coluna_resultado'] = explode(",", $camposUsu);
            $order  = 'FIELD(co_coluna_resultado, ' . $camposUsu . ')';
        }
        
        $findCampos = parent::find ( 'all', array(
            'fields' => array(
                'co_coluna_resultado',
                'ic_modulo',
                'ds_nome',
                'ds_dominio',
                'ds_coluna_dominio',
                'ds_funcao_habilita',
                'ds_funcao_exibe'
            ),
            'order' => $order,
            'conditions' => $conditions ) );

        $findCamposList = array();
        foreach ($findCampos as $chave => &$registro) :
            if ($registro['ColunaResultadoPesquisa']['co_coluna_resultado'] == 9) {
                $registro['ColunaResultadoPesquisa']['ds_nome'] = 'Dias para finalizar';
            }

            if( $registro['ColunaResultadoPesquisa']['ds_funcao_habilita'] != '' && $this->verificaCampoHabilitado($registro['ColunaResultadoPesquisa']['ds_funcao_habilita']) ) {
                unset($findCampos[$chave]);
            } else {
                if($tipo == 'list') {
                    $findCamposList[$registro['ColunaResultadoPesquisa']['co_coluna_resultado']] = __($registro['ColunaResultadoPesquisa']['ds_nome'], true);
                }
                if($tipo == 'matriz') {
                    $findCamposList[]   = array('codigo' => $registro['ColunaResultadoPesquisa']['co_coluna_resultado'], 
                                                'coluna' => __($registro['ColunaResultadoPesquisa']['ds_nome'], true));
                }
            }
        endforeach;
        
        if( $tipo == 'list' || $tipo == 'matriz' ) {
            $findCampos = $findCamposList;
        } else {
            array_walk_recursive($findCampos, array($this, 'renomearNomeCampo'));
        }
        
        return $findCampos;
    }
    
    function renomearNomeCampo(&$campo, $key) {
        if( $key == 'ds_nome' && $campo != null && $campo != '' && !is_array($campo)) {
            $campo = __($campo, true);
        }
    }
    
    function verificaCampoHabilitado($funcao) {
        return (!$this->moduloHlp->$funcao());
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>