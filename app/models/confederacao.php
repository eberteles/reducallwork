<?php

class Confederacao extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Confederacao';

    var $useTable = 'confederacoes';

    var $primaryKey = 'co_confederacao';

    var $displayField = 'ds_confederacao';
    
    var $order = "ds_confederacao  ASC";

    var $validate = array(
        'ds_confederacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em Branco.'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Campo Nome já cadastrado.'
            )
        ),
        'ds_email' => array(
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Campo E-mail inválido!',
                'allowEmpty' => true
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Campo E-mail já cadastrado.',
                'allowEmpty' => true
            )
        ),
        'nu_telefone' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone em branco',
                'allowEmpty' => true
            )
        )
    )
    ;

    public function beforeSave($options = array())
    {
        $this->data['Confederacao']['ds_confederacao'] = up($this->data['Confederacao']['ds_confederacao']);
        if (isset($this->data['Confederacao']['ds_email'])) {
            $this->data['Confederacao']['ds_email'] = up($this->data['Confederacao']['ds_email']);
        }
        if (isset($this->data['Confederacao']['no_contrato'])) {
            $this->data['Confederacao']['no_contrato'] = up($this->data['Confederacao']['no_contrato']);
        }
        if (isset($this->data['Confederacao']['ds_endereco'])) {
            $this->data['Confederacao']['ds_endereco'] = up($this->data['Confederacao']['ds_endereco']);
        }
        if (isset($this->data['Confederacao']['ds_complemento'])) {
            $this->data['Confederacao']['ds_complemento'] = up($this->data['Confederacao']['ds_complemento']);
        }
        if (isset($this->data['Confederacao']['ds_bairro'])) {
            $this->data['Confederacao']['ds_bairro'] = up($this->data['Confederacao']['ds_bairro']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>