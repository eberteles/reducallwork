<?php

class Conta extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Conta';

    var $primaryKey = 'id';

    var $validate = array(
        'id' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'contrato_id' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'operadora_id' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Operadora em branco'
            )
        ),
        'conta' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Código da Conta em branco'
            )
        ),
        'telefone' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone em branco'
            )
        )
    );
    
    var $belongsTo = array(
        'Operadora' => array(
            'className' => 'Operadora',
            'foreignKey' => 'operadora_id'
        ),
        'Gestor' => array(
            'className' => 'Gestor',
            'foreignKey' => 'gestor_id'
        )
    );

    public function beforeSave($options = array())
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Conta']['usuario_id'] = $usuario['Usuario']['co_usuario'];
        
        if (isset($this->data['Conta']['conta'])) {
            $this->data['Conta']['conta'] = up($this->data['Conta']['conta']);
        }
        
        $this->data['Conta']['cnpj'] = str_replace(array('-','.','/'), '', $this->data['Conta']['cnpj']);
        
        return parent::beforeSave($options);
    }
    
}
?>