<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 15:12
 */

class Contato extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
    var $name       = "Contato";
    var $useTable   = "contatos";
    var $primaryKey = "co_contato";

    var $validate = array(
        'co_fornecedor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Fornecedor em branco'
            )
        ),
        'tipo_contato' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Tipo de Contato em branco'
            )
        ),
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em branco'
            )
        ),
        'telefone1' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone 1 em branco'
            )
        ),
        'email' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Email em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if(isset($this->data['Contato']['telefone1'])) {
            $this->data['Contato']['telefone1'] = str_replace("(", "", str_replace(")", "", str_replace("-", "", str_replace(" ", "", $this->data['Contato']['telefone1']))));
        }

        if(isset($this->data['Contato']['telefone2'])) {
            $this->data['Contato']['telefone2'] = str_replace("(", "", str_replace(")", "", str_replace("-", "", str_replace(" ", "", $this->data['Contato']['telefone2']))));
        }
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}