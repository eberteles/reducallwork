<?php

class Contratacao extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Contratacao';

    var $useTable = 'contratacoes';

    var $primaryKey = 'co_contratacao';

    var $displayField = 'ds_contratacao';

    var $validate = array(
        
        'ds_contratacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );

    var $hasMany = array(
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_contratacao',
            'dependent' => false
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Contratacao']['ds_contratacao'] = up($this->data['Contratacao']['ds_contratacao']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function getByDescricao($descricao)
    {
        return $this->query(" SELECT * FROM contratacoes WHERE ds_contratacao = '".($descricao)."'");
    }
}
?>