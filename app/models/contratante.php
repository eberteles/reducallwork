<?php

class Contratante extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Contratante';

    var $useTable = 'contratantes';

    var $primaryKey = 'co_contratante';

    var $displayField = 'ds_contratante';

    var $validate = array(
        
        'ds_contratante' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'reg_contratante' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'nu_contato' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'no_responsavel' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        
    );

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>