<?php

class Contrato extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Contrato';

    var $useTable = 'contratos';

    var $primaryKey = 'co_contrato';

    var $displayField = 'nu_contrato';

    var $order = "Contrato.co_contrato DESC";

    var $validate = array(
        'co_modalidade' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Tipo de Contrato em branco'
            )
        ),
        'co_servico' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Descrição do Serviço em branco',
                'allowEmpty' => true
            )
        ),
        'co_contratante' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Unidade Solicitante em branco'
            )
        )/*,
        'nu_processo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Processo em branco'
            ),
            'validateNumber' => array(
                'rule' => array(
                    'validNumbersProcesso',
                    true
                ),
                'message' => 'Este número de processo é inválido.'
            )
        )*/,
        'nu_contrato' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Contrato em branco'
            ),
            'validateNumber' => array(
                'rule' => array(
                    'validNumbersContrato',
                    true
                ),
                'message' => 'Este número de Contrato é inválido.'
            )
        ),
        'ds_justificativa' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Justificativa em branco'
            )
        ),
        'co_cliente' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Cliente em branco'
            )
        ),
        'ds_objeto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Objeto em branco'
            )
        ),
        'vl_inicial' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo valor inicial em branco'
            )
        ),
        'st_repactuado' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Prorrogável em branco'
            )
        ),
        'dt_fim_processo' => array(
            'rule' => array(
                'validaDtFimProcesso',
                true
            ),
            'message' => 'A Data de Fim do Processo deve ser maior que a Data de Início do Processo.',
            'allowEmpty' => true
        ),
        'pc_garantia' => array(
            'required' => array(
                'rule' => array('validaGarantiaRequired'),
                'message' => 'Campo Porcentagem da garantia em branco',
            ),
            'range' => array(
                'rule' => array('range', 0, 11),
                'message' => 'Por favor coloque uma porcentagem que seja maior que 0 e menor ou igual 10',
                'allowEmpty' => true
            ),
            'podeBaixarAPorcentagem' => array(
                'rule' => array('validaSePodeDiminuirPorcentagemDeGarantia'),
            ),
        ),
    );

    var $belongsTo = array(
        'Situacao' => array(
            'className' => 'Situacao',
            'foreignKey' => 'co_situacao'
        ),
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_contratante'
        ),
        'Executante' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_executante'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        ),
        'Cliente' => array(
            'className' => 'Cliente',
            'foreignKey' => 'co_cliente'
        ),
        'Parceiro' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_parceiro'
        ),
        'Modalidade' => array(
            'className' => 'Modalidade',
            'foreignKey' => 'co_modalidade'
        ),
        'Contratacao' => array(
            'className' => 'Contratacao',
            'foreignKey' => 'co_contratacao'
        ),
        'Servico' => array(
            'className' => 'Servico',
            'foreignKey' => 'co_servico'
        ),
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'co_categoria'
        ),
        'Subcategoria' => array(
            'className' => 'Subcategoria',
            'foreignKey' => 'co_subcategoria'
        ),
        'GestorAtual' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_gestor_atual'
        ),
        'GestorSuplente' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_gestor_suplente'
        ),
        'FiscalAtual' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_fiscal_atual'
        ),
        'Fase' => array(
            'className' => 'Fase',
            'foreignKey' => 'co_fase'
        ),
        'FaseAnterior' => array(
            'className' => 'Fase',
            'foreignKey' => 'co_fase_anterior'
        ),
        'SetorAtual' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        ),
        'Fluxo' => array(
            'className' => 'Fluxo',
            'foreignKey' => 'nu_sequencia'
        ),
        'Solicitante' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_solicitante'
        ),
    );

    var $hasMany = array(
        'Andamento' => array(
            'className' => 'Andamento',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Pagamento' => array(
            'className' => 'Pagamento',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Aditivo' => array(
            'className' => 'Aditivo',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Pendencia' => array(
            'className' => 'Pendencia',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'ContratoFiscal' => array(
            'className' => 'ContratoFiscal',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Garantia' => array(
            'className' => 'Garantia',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'GarantiaSuporte' => array(
            'className' => 'GarantiaSuporte',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Empenho' => array(
            'className' => 'Empenho',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Historico' => array(
            'className' => 'Historico',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),/*
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),*/
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Liquidacao' => array(
            'className' => 'Liquidacao',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Apostilamento' => array(
            'className' => 'Apostilamento',
            'foreignKey' => 'co_apostilamento',
            'cascadeCallbacks' => true,
            'dependent' => true
        ),
        'Penalidade' => array(
            'className' => 'Penalidade',
            'foreignKey' => 'co_contrato',
            'cascadeCallbacks' => true,
            'dependent' => true
        )
    );
    
//    var $hasAndBelongsToMany = array(
//        'Cliente' => array(
//            'className' => 'Cliente',
//            'joinTable' => 'contratos_clientes',
//            'foreignKey' => 'contrato_id',
//            'associationForeignKey' => 'cliente_id',
//            //'unique' => false
//        )
//    );

    public function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        if ($modulo->isProcessoUnique()) {
            $this->validate['nu_processo']['isUnique'] = array(
                'rule' => 'isUnique',
                'allowEmpty' => true,
                'message' => 'Este número de Processo já encontra-se cadastrado.'
            );
        }
        if ($modulo->isItemPam()) {
            $this->hasMany['ContratosItem'] = array(
                'className' => 'ContratosItem',
                'foreignKey' => 'co_contrato'
            );
        }
        parent::__construct();
    }

    /**
     *
     * @param array $data
     * @return void
     */
    protected function validaGarantiaRequired($data)
    {
        // exit('<pre>' . print_r($this->data[$this->name]['ic_cadastro_garantia'], true));
        return $this->data[$this->name]['ic_cadastro_garantia'] && empty($data['pc_garantia']) ? false : true;
    }

    protected function validaDtFimProcesso()
    {
        if (!isset($this->data['Contrato']['dt_ini_processo'])) {
            return false;
        } else {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Contrato']['dt_ini_processo'], $this->data['Contrato']['dt_fim_processo'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }

    protected function validaDtFimContrato()
    {
        if (!isset($this->data['Contrato']['dt_ini_vigencia'])) {
            return false;
        } else {
            if (isset($this->data['Contrato']['nu_fim_vigencia']) && $this->data['Contrato']['nu_fim_vigencia'] > 0) {
                $this->data['Contrato']['dt_fim_vigencia'] = date('Y-m-d', strtotime("+" . $this->data['Contrato']['nu_fim_vigencia'] . " days", strtotime($this->data['Contrato']['dt_ini_vigencia'])));
            }
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Contrato']['dt_ini_vigencia'], $this->data['Contrato']['dt_fim_vigencia'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Para a porcentage ser válida, o somatório dos valores de todas as garantias já vinculadas a este contrato
     * não pode exeder o valor atual do contrato aplicada a nova porcentagem.
     */
    protected function validaSePodeDiminuirPorcentagemDeGarantia()
    {
        $idContrato = $this->data[$this->name]['co_contrato'];
        $porcentagemDeGarantiaDoContrato = $this->data[$this->name]['pc_garantia'];
        $valorAtualDoContrato = $this->retornaValorAtualDoContrato($idContrato);
        $valorAtualDoContratoAplicadaNovaPorcentagem = $valorAtualDoContrato / 100 * $porcentagemDeGarantiaDoContrato;
        $garantiaModel = new Garantia();
        $valorDeTodasAsGarantiasJaCadastradas = array_reduce(
            $garantiaModel->find('all', array('fields' => array('vl_garantia'), 'conditions' => array('co_contrato' => $idContrato))),
            function($garantiaAnterior, $garantiaAtual) { return $garantiaAnterior + $garantiaAtual['Garantia']['vl_garantia']; }
        );
        $porcentagemMinima = 100 * $valorDeTodasAsGarantiasJaCadastradas / $valorAtualDoContrato;
        $templateDeMensagemDeErro = array(
            'O valor informado não pode ser salvo pois existem garantias',
            'cadastradas que ultrapassam o percentual informado',
            'Valor total de Garantias cadastradas: %s',
            'Percentual mínimo para as garantias já cadastradas: %s%%',
        );
        setlocale(LC_MONETARY, 'pt_BR', 'ptb');
        $mensagemDeErro = sprintf(
            implode('<br/>', $templateDeMensagemDeErro),
            money_format('%n', $valorDeTodasAsGarantiasJaCadastradas),
            $porcentagemMinima
        );
        return $valorDeTodasAsGarantiasJaCadastradas <= $valorAtualDoContratoAplicadaNovaPorcentagem ? true : $mensagemDeErro;
    }

    public function contratosPendentes()
    {
        $result = $this->query("SELECT
                                    COUNT(Contrato.co_contrato) total,
                                    SUM(Pendencia.st_pendencia = 'P') com_pendencia,
                                    SUM(Pendencia.st_pendencia <> 'P') sem_pendencia
                                FROM contratos Contrato
                                LEFT JOIN (
                                      SELECT MAX(co_aditivo) co_aditivo, dt_fim_vigencia, co_contrato
                                      FROM aditivos Aditivo
                                      WHERE Aditivo.dt_fim_vigencia IS NOT NULL
                                      GROUP BY  co_contrato
                                ) UltimoAditivo ON UltimoAditivo.co_contrato = Contrato.co_contrato
                                LEFT JOIN pendencias Pendencia ON Contrato.co_contrato = Pendencia.co_contrato
                                WHERE Contrato.ic_ativo = 1 AND (Contrato.dt_fim_vigencia > CURRENT_DATE OR UltimoAditivo.dt_fim_vigencia > CURRENT_DATE);");

        $result = (count($result) > 0) ? current($result) : false;

        return $result;

    }

    public function contratosPagamentos()
    {
        $result = $this->query("SELECT
                                    COUNT(*) total,
                                    SUM(Pagamento.dt_vencimento < CURRENT_DATE AND Pagamento.vl_pago = 0) com_atraso,
                                    SUM(Pagamento.dt_vencimento >= CURRENT_DATE OR Pagamento.vl_pago > 0) em_dia
                                FROM contratos Contrato
                                JOIN pagamentos Pagamento ON Contrato.co_contrato = Pagamento.co_contrato;");

        $result = (count($result) > 0) ? current($result) : false;

        return $result;

    }

    public function contratosAtivos()
    {
        $result = $this->query("SELECT COUNT(*) total,
                                       SUM(ic_ativo) ativos,
                                       SUM(NOT ic_ativo) inativos
                                FROM contratos;");

        $result = (count($result) > 0) ? current($result) : false;

        return $result;

    }

    function validaAssinaturaContrato()
    {
//        if (!isset($this->data['Contrato']['dt_ini_vigencia']) || !isset($this->data['Contrato']['dt_assinatura']) || $this->data['Contrato']['dt_ini_vigencia'] == '' || $this->data['Contrato']['dt_assinatura'] == '') {
//            return false;
//        } else {
//            App::import('Helper', 'Print');
//            $print = new PrintHelper();
//            $diferenca = $print->difEmDias($this->data['Contrato']['dt_assinatura'], $this->data['Contrato']['dt_ini_vigencia'], "US");
//            if ($diferenca < 0) {
//                return false;
//            }
//        }
        return true;
    }

    public function beforeSave($options = array())
    {
        if (empty($this->data['Contrato']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            if (isset($usuario['Usuario']['co_usuario'])) {
                $this->data['Contrato']['co_usuario'] = $usuario['Usuario']['co_usuario'];
            }
        }

        if (!isset($this->data['Contrato']['co_contrato']) || $this->data['Contrato']['co_contrato'] == '') {
            $this->data['Contrato']['insert_date'] = date('Y-m-d H:i:s');
        }
        if (empty($this->data['Contrato']['dt_assinatura'])) {
            $this->data['Contrato']['dt_assinatura'] = null;
        }
        if (empty($this->data['Contrato']['dt_ini_vigencia'])) {
            $this->data['Contrato']['dt_ini_vigencia'] = null;
        }
        if (isset($this->data['Contrato']['dt_fim_vigencia'])) {
            if (empty($this->data['Contrato']['dt_fim_vigencia'])) {

                if (!empty($this->data['Contrato']['nu_fim_vigencia'])) {
                    $fim_vigencia = date('Y-m-d',
                        strtotime($this->data['Contrato']['dt_ini_vigencia'] . "+{$this->data['Contrato']['nu_fim_vigencia']} days")
                    );
                }
            } elseif (!empty($this->data['Contrato']['dt_fim_vigencia'])) {
                $fim_vigencia = $this->data['Contrato']['dt_fim_vigencia'];
            }
            $this->data['Contrato']['dt_fim_vigencia_inicio'] = $this->data['Contrato']['dt_fim_vigencia'] = $fim_vigencia;

        } else {
            if (empty($this->data['Contrato']['dt_fim_vigencia_inicio'])) {
                if (isset($this->data['Contrato']['dt_fim_vigencia'])) {
                    $this->data['Contrato']['dt_fim_vigencia_inicio'] = $this->data['Contrato']['dt_fim_vigencia'];
                }
            }
        }

        if (empty($this->data['Contrato']['dt_ini_processo'])) {
            $this->data['Contrato']['dt_ini_processo'] = null;
        }
        if (empty($this->data['Contrato']['dt_fim_processo'])) {
            $this->data['Contrato']['dt_fim_processo'] = null;
        }

        if (isset($this->data['Contrato']['nu_processo'])) {
            $this->data['Contrato']['nu_processo'] = up($this->data['Contrato']['nu_processo']);
        }
        if (isset($this->data['Contrato']['vl_inicial'])) {
            $this->data['Contrato']['vl_inicial'] = ln($this->data['Contrato']['vl_inicial']);
        }
        if (isset($this->data['Contrato']['vl_mensal'])) {
            $this->data['Contrato']['vl_mensal'] = ln($this->data['Contrato']['vl_mensal']);
        }
        if (isset($this->data['Contrato']['vl_global'])) {
            $this->data['Contrato']['vl_global'] = ln($this->data['Contrato']['vl_global']);
        }

        if (isset($this->data['Contrato']['ds_objeto'])) {
            $this->data['Contrato']['ds_objeto'] = up($this->data['Contrato']['ds_objeto']);
        }
        if (isset($this->data['Contrato']['ds_fundamento_legal'])) {
            $this->data['Contrato']['ds_fundamento_legal'] = up($this->data['Contrato']['ds_fundamento_legal']);
        }
        if (isset($this->data['Contrato']['ds_observacao'])) {
            $this->data['Contrato']['ds_observacao'] = up($this->data['Contrato']['ds_observacao']);
        }

        if (isset($this->data['Contrato']['nu_pam'])) {
            $this->data['Contrato']['nu_pam'] = up($this->data['Contrato']['nu_pam']);
        }
        if (empty($this->data['Contrato']['dt_autorizacao_pam'])) {
            $this->data['Contrato']['dt_autorizacao_pam'] = null;
        }
        if (empty($this->data['Contrato']['dt_tais'])) {
            $this->data['Contrato']['dt_tais'] = null;
        }
        if (empty($this->data['Contrato']['dt_publicacao'])) {
            $this->data['Contrato']['dt_publicacao'] = null;
        }
        if (empty($this->data['Contrato']['dt_fim_suporte'])) {
            $this->data['Contrato']['dt_fim_suporte'] = null;
        }

        if (isset($this->data['Contrato']['ds_requisitante'])) {
            $this->data['Contrato']['ds_requisitante'] = up($this->data['Contrato']['ds_requisitante']);
        }
        if (isset($this->data['Contrato']['pt_requisitante'])) {
            $this->data['Contrato']['pt_requisitante'] = up($this->data['Contrato']['pt_requisitante']);
        }
        if (isset($this->data['Contrato']['ds_assunto'])) {
            $this->data['Contrato']['ds_assunto'] = up($this->data['Contrato']['ds_assunto']);
        }
        if (isset($this->data['Contrato']['ds_objeto_plano'])) {
            $this->data['Contrato']['ds_objeto_plano'] = up($this->data['Contrato']['ds_objeto_plano']);
        }
        if (isset($this->data['Contrato']['ds_vantagem'])) {
            $this->data['Contrato']['ds_vantagem'] = up($this->data['Contrato']['ds_vantagem']);
        }
        if (isset($this->data['Contrato']['ds_criterio'])) {
            $this->data['Contrato']['ds_criterio'] = up($this->data['Contrato']['ds_criterio']);
        }
        if (isset($this->data['Contrato']['ds_justificativa'])) {
            $this->data['Contrato']['ds_justificativa'] = up($this->data['Contrato']['ds_justificativa']);
        }
        if (isset($this->data['Contrato']['ds_resultado'])) {
            $this->data['Contrato']['ds_resultado'] = up($this->data['Contrato']['ds_resultado']);
        }
        if (isset($this->data['Contrato']['ds_demanda'])) {
            $this->data['Contrato']['ds_demanda'] = up($this->data['Contrato']['ds_demanda']);
        }
        if (isset($this->data['Contrato']['ds_aproveitamento'])) {
            $this->data['Contrato']['ds_aproveitamento'] = up($this->data['Contrato']['ds_aproveitamento']);
        }
        if (isset($this->data['Contrato']['ic_cadastro_garantia']) && $this->data['Contrato']['ic_cadastro_garantia'] == 0) {
            $this->data['Contrato']['pc_garantia'] = 0;
        }

        if (isset($this->data['Contrato']['vl_reducall'])) {
            $this->data['Contrato']['vl_reducall'] = ln($this->data['Contrato']['vl_reducall']);
        }
        if (isset($this->data['Contrato']['vl_araras'])) {
            $this->data['Contrato']['vl_araras'] = ln($this->data['Contrato']['vl_araras']);
        }
        if (isset($this->data['Contrato']['vl_parceiro'])) {
            $this->data['Contrato']['vl_parceiro'] = ln($this->data['Contrato']['vl_parceiro']);
        }
        if (isset($this->data['Contrato']['vl_ressarcimento'])) {
            $this->data['Contrato']['vl_ressarcimento'] = ln($this->data['Contrato']['vl_ressarcimento']);
        }
        if (isset($this->data['Contrato']['vl_ressarcimento_reducall'])) {
            $this->data['Contrato']['vl_ressarcimento_reducall'] = ln($this->data['Contrato']['vl_ressarcimento_reducall']);
        }
        if (isset($this->data['Contrato']['vl_ressarcimento_araras'])) {
            $this->data['Contrato']['vl_ressarcimento_araras'] = ln($this->data['Contrato']['vl_ressarcimento_araras']);
        }
        if (isset($this->data['Contrato']['vl_ressarcimento_parceiro'])) {
            $this->data['Contrato']['vl_ressarcimento_parceiro'] = ln($this->data['Contrato']['vl_ressarcimento_parceiro']);
        }

        if (isset($this->data['Contrato']['pct_reducao'])) {
            $this->data['Contrato']['pct_reducao'] = floatval(str_replace("%","",$this->data['Contrato']['pct_reducao']));
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }

    function updDtFimVigencia($coContrato)
    {
        $dt_fim_vigencia = null;

        App::import('Model', 'Aditivo');
        App::import('Model', 'Apostilamento');

        $aditivoDb = new Aditivo();
        $aditivo = $aditivoDb->find('first', array(
            'fields' => array(
                'MAX( Aditivo.dt_prazo ) as dt_prazo'
            ),
            'conditions' => array(
                'Aditivo.co_contrato' => $coContrato
            )
        ));
        if (isset($aditivo[0]['dt_prazo'])) {
            $dt_fim_vigencia = $aditivo[0]['dt_prazo'];
        }

        $apostilamentoDb = new Apostilamento();
        $apostilamento = $apostilamentoDb->find('first', array(
            'fields' => array(
                'MAX( Apostilamento.dt_prazo ) as dt_prazo'
            ),
            'conditions' => array(
                'Apostilamento.co_contrato' => $coContrato
            )
        ));
        if (isset($apostilamento[0]['dt_prazo'])) {
            if ($dt_fim_vigencia != null) {
                App::import('Helper', 'Print');
                $print = new PrintHelper();
                $diferenca = $print->difEmDias($dt_fim_vigencia, $apostilamento[0]['dt_prazo'], "US");
                if ($diferenca > 0) {
                    $dt_fim_vigencia = $apostilamento[0]['dt_prazo'];
                }
            } else {
                $dt_fim_vigencia = $apostilamento[0]['dt_prazo'];
            }
        }

        if ($dt_fim_vigencia == null) {
            $contrato = $this->find('first', array(
                'fields' => array(
                    'Contrato.dt_fim_vigencia_inicio'
                ),
                'conditions' => array(
                    'Contrato.co_contrato' => $coContrato
                )
            ));
            $dt_fim_vigencia = dtDb($contrato["Contrato"]["dt_fim_vigencia_inicio"]);
        }

        $this->id = $coContrato;
        $this->saveField("dt_fim_vigencia", $dt_fim_vigencia);
    }

    function beforeFind($queryData)
    {
        if (count($queryData['conditions']) == 0) {
            $queryData['conditions']['Contrato.ic_ativo'] = 1;
        }
        return $queryData;
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratosExpirando($prazo = 90)
    {
        $inicio = $prazo - 3;
        $fim    = $prazo + 3;
        return $this->query("SELECT * FROM contratos
                    WHERE dt_fim_vigencia > ADDDATE( current_date(), $inicio ) and dt_fim_vigencia < ADDDATE( current_date(), $fim ) ORDER BY nu_contrato");
    }

    public function getNextProcesso()
    {
        $nextProcesso = 0;

        $processoAtual = $this->query(" SELECT ct_processo FROM numeracao_processo
                                                    WHERE  aa_processo = NOW( ) ");
        if (count($processoAtual)) {

            foreach ($processoAtual as $processo) {
                $nextProcesso = (int)$processo['numeracao_processo']['ct_processo'] + 1;
            }
            $this->query(" UPDATE numeracao_processo
                                    SET  ct_processo =  '$nextProcesso'
                                    WHERE  aa_processo = NOW() ");
        } else {
            $nextProcesso = 1;
            $this->query(" INSERT INTO numeracao_processo
                                    ( aa_processo , ct_processo ) VALUES
                                    ( NOW(),  '$nextProcesso' ) ");
        }

        while (strlen($nextProcesso) < 3) {
            $nextProcesso = "0" . $nextProcesso;
        }

        return $nextProcesso . date("Y");
    }

    public function getNextPam()
    {
        $nextPam = 0;

        $pamAtual = $this->query(" SELECT ct_pam FROM numeracao_pam
                                                    WHERE  aa_pam = NOW( ) ");
        if (count($pamAtual)) {

            foreach ($pamAtual as $pam) {
                $nextPam = (int)$pam['numeracao_pam']['ct_pam'] + 1;
            }
            $this->query(" UPDATE numeracao_pam
                                    SET  ct_pam =  '$nextPam'
                                    WHERE  aa_pam = NOW() ");
        } else {
            $nextPam = 1;
            $this->query(" INSERT INTO numeracao_pam
                                    ( aa_pam , ct_pam ) VALUES
                                    ( NOW(),  '$nextPam' ) ");
        }

        while (strlen($nextPam) < 5) {
            $nextPam = "0" . $nextPam;
        }

        return $nextPam . date("Y");
    }

    public function setNextContrato($co_contrato)
    {
        $nextContrato = 0;

        $contratoAtual = $this->query(" SELECT ct_contrato FROM numeracao_contrato
                                                    WHERE  aa_contrato = NOW( ) ");
        if (count($contratoAtual)) {

            foreach ($contratoAtual as $contrato) {
                $nextContrato = (int)$contrato['numeracao_contrato']['ct_contrato'] + 1;
            }
            $this->query(" UPDATE numeracao_contrato
                                    SET  ct_contrato =  '$nextContrato'
                                    WHERE  aa_contrato = NOW() ");
        } else {
            $nextContrato = 1;
            $this->query(" INSERT INTO numeracao_contrato
                                    ( aa_contrato , ct_contrato ) VALUES
                                    ( NOW(),  '$nextContrato' ) ");
        }

        while (strlen($nextContrato) < 3) {
            $nextContrato = "0" . $nextContrato;
        }

        $nextContrato = $nextContrato . date("Y");

        $this->query(" UPDATE contratos SET nu_contrato =  '$nextContrato'
                            WHERE  co_contrato = $co_contrato ");

        return $nextContrato;
    }

    public function updFinanceiro($coContrato, $situacao = 'E')
    {
        $this->query("UPDATE contratos SET fg_financeiro = '$situacao' WHERE co_contrato = $coContrato");
    }

    public function getGestorAtualNomeEmail($co_contrato)
    {
        return $result = $this->query("SELECT ds_nome, ds_email FROM contratos Contrato
                LEFT JOIN usuarios Usuario ON ( Usuario.co_usuario = Contrato.co_fiscal_atual )
                WHERE Contrato.co_contrato = $co_contrato ");
    }

    public function getFiscaisNomeEmail($co_contrato)
    {
        return $this->query("select
                                Usuario.ds_nome, Usuario.ds_email
                            from
                                contratos_fiscais as cf,
                                usuarios as Usuario
                            where cf.co_contrato = $co_contrato
                            and Usuario.co_usuario = cf.co_usuario_fiscal");
    }

    public function getGestorAtivo($co_contrato)
    {
        return $result = $this->query("SELECT ds_nome, ds_email FROM contratos Contrato
                LEFT JOIN usuarios Usuario ON ( Usuario.co_usuario = Contrato.co_fiscal_atual || Usuario.co_usuario = Contrato.co_gestor_atual )
                WHERE Contrato.co_contrato = $co_contrato ");
    }

    public function getNumerosContratos()
    {
        $r = $this->query("SELECT DISTINCT nu_contrato FROM contratos WHERE nu_contrato!=''");
        $contratos = array();
        foreach ($r as $c)
            $contratos[] = $c['contratos']['nu_contrato'];
        return $contratos;
    }

    public function insertOrUpdate($contrato)
    {
        $siasg = new Siasg();
        $id = null;
        $new = false;
        $qC = $this->find(
            array(
                'Contrato.nu_contrato' => $contrato['nu_contrato'],
                'Contrato.co_modalidade' => $contrato['co_modalidade'],
                'Contrato.uasg' => $contrato['uasg']
            )
        );
        if (sizeof($qC['Contrato']) > 0)
            $id = $qC['Contrato']['co_contrato'];

        if (!$id && !isset($contrato['co_contrato'])) {
            $new = true;
            $query = "INSERT INTO contratos(";
            foreach ($contrato as $k => $v)
                $query .= $k . ",";

            if (isset($contrato['dt_cadastro'])) {
                $query .= "dt_cadastro) VALUES (";
            } else {
                $query = substr($query, 0, -1);
                $query .= ") VALUES (";
            }
            foreach ($contrato as $k => $v)
                $query .= '"' . $v . '",';
            if (isset($contrato['dt_cadastro'])) {
                $query .= "NOW())";
            } else {
                $query = substr($query, 0, -1);
                $query .= ")";
            }
            $id = $siasg->query($query);
        } else {
            $query = "UPDATE contratos SET ";
            foreach ($contrato as $k => $v)
                $query .= $k . '="' . $v . '",';
            $query = substr($query, 0, -1);
            $query .= " WHERE co_contrato=" . $id;
            $siasg->query($query);
        }

        $ret = array(
            'new' => $new,
            'id' => $id
        );

        return $ret;
    }

    public function findAllByGestor($co_usuario)
    {
        $this->recursive = -1;
        $contratos = $this->findAllByCoGestorAtual($co_usuario);
        $arrayContratos = array();
        foreach ($contratos as $c) {
            array_push($arrayContratos, $c['Contrato']['co_contrato']);
        }
        return $arrayContratos;
    }

    public function validNumbersProcesso()
    {

        $nuProcesso = $this->data['Contrato']['nu_processo'];

        if ($nuProcesso == str_repeat('0', 17) || $nuProcesso == str_repeat('1', 17) || $nuProcesso == str_repeat('2', 17)
            || $nuProcesso == str_repeat('3', 17) || $nuProcesso == str_repeat('4', 17) || $nuProcesso == str_repeat('5', 17)
            || $nuProcesso == str_repeat('6', 17) || $nuProcesso == str_repeat('7', 17) || $nuProcesso == str_repeat('8', 17)
            || $nuProcesso == str_repeat('9', 17)
        ) {
            return false;
        }

        return true;
    }

    public function validNumbersContrato()
    {
        $nuContrato = $this->data['Contrato']['nu_contrato'];

        if ($nuContrato == str_repeat('0', 9) || $nuContrato == str_repeat('1', 9) || $nuContrato == str_repeat('2', 9)
            || $nuContrato == str_repeat('3', 9) || $nuContrato == str_repeat('4', 9) || $nuContrato == str_repeat('5', 9)
            || $nuContrato == str_repeat('6', 9) || $nuContrato == str_repeat('7', 9) || $nuContrato == str_repeat('8', 9)
            || $nuContrato == str_repeat('9', 9)
        ) {
            return false;
        }

        return true;
    }

    public function getDiferencaEmDias($dtIni, $dtFim)
    {
        $result = $this->query("SELECT DATEDIFF('$dtFim', '$dtIni') as diff");

        return $result[0][0]['diff'];
    }

    /**
     * @return float O valor atual do contrato
     */
    public function retornaValorAtualDoContrato($idContrato)
    {
        $contrato = $this->find('first', array(
            'recursive' => 0,
            'fields' => array('vl_global'),
            'conditions' => array('co_contrato' => $idContrato)
        ));
        $valorGlobal = $contrato['Contrato']['vl_global'];
        $valorTotalDosAditivos = $this->Aditivo->getTotalComReajuste($idContrato);
        $valorTotalDosApostilamentos = $this->Apostilamento->getTotal($idContrato);
        return $valorGlobal + $valorTotalDosAditivos + $valorTotalDosApostilamentos;
    }
}
