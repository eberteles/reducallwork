<?php

class ContratoFiscal extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $recursive = 0;

    var $name = 'ContratoFiscal';

    var $useTable = 'contratos_fiscais';

    var $primaryKey = 'co_contrato_fiscal';

    var $displayField = 'co_usuario_fiscal';

    var $validate = array(
        'co_contrato' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Campo inválido'
            )
        ),
        'co_usuario_fiscal' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Campo fiscal em branco'
            ),
        ),
        'co_fiscais_tipos' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Campo tipo fiscal em branco'
            )
        ),

        'dt_inicio' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Campo início em branco',
                'required' => true
            ),
            'intervalo' => array(
                'rule' => 'validaPeriodoNomeacao',
                'message' => 'Já existe um fiscal com o mesmo tipo cadastrado para este período.',
                'required' => true
            ),
        ),

        'dt_fim' => array(
            'dataMaior' => array(
                'rule' => 'validaDtFim',
                'message' => 'A data final deve ser maior que a data de início.',
                'allowEmpty' => true
            )
        )
    );

    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario_fiscal'
        ),
        'FiscalTipo' => array(
            'className' => 'FiscalTipo',
            'foreignKey' => 'co_fiscais_tipos'
        )
    );

    /**
     * @param  boolean $created
     * @return boolean
     */
    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    /**
     * @param  string $queryData
     * @return string
     */
    public function beforeFind($queryData)
    {
        if (!isset($queryData['conditions']['ContratoFiscal.ic_ativo'])) {
            $queryData['conditions']['ContratoFiscal.ic_ativo'] = 1;
        }

        return $queryData;
    }

    /**
     * @param  array $options
     * @return boolean
     */
    public function beforeValidate($options = array())
    {
        if (empty($this->data['ContratoFiscal']['dt_fim'])) {
            $this->data['ContratoFiscal']['dt_fim'] = null;
        }
        if (empty($this->data['ContratoFiscal']['dt_boletim_especial'])) {
            $this->data['ContratoFiscal']['dt_boletim_especial'] = null;
        }

        if (!empty($this->data['ContratoFiscal']['co_contrato_fiscal'])) {
            $fiscal = $this->find(array('co_contrato_fiscal' => $this->data['ContratoFiscal']['co_contrato_fiscal']));
            if($fiscal['ContratoFiscal']['co_fiscais_tipos'] == $this->data['ContratoFiscal']['co_fiscais_tipos']){
                unset($this->validate['co_usuario_fiscal']['myUnique']);
            }
        }

        return parent::beforeValidate($options);
    }

    /**
     * @return boolean
     */
    public function validaDtFim()
    {
        if (!empty($this->data['ContratoFiscal']['dt_fim'])) {
            $objDtInicio    = DateTime::createFromFormat('Y-m-d', $this->data['ContratoFiscal']['dt_inicio']);
            $objDtFim       = DateTime::createFromFormat('Y-m-d', $this->data['ContratoFiscal']['dt_fim']);
            $interval       = $objDtInicio->diff($objDtFim);

            if ($interval->format("%R%a") >= 0) {
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    function validaPeriodoNomeacao($data)
    {
        $semDtFim = $this->_existeNomeacaoSemFimVigencia();

        if ($this->_validaDataNomeacao($semDtFim)) {
            return $this->_validaDataExoneracao($semDtFim);
        }

        // Data de nomeação dentro de período vigente do atual
        return false;
    }

    /**
     * Verifica se existe nomeação vigente para o contrato do mesmo tipo de fiscal.
     *
     * @return bool
     */
    private function _existeNomeacaoSemFimVigencia()
    {
        $conditions     = array();

        $conditions["ContratoFiscal.dt_fim"] = NULL;
        $conditions['ContratoFiscal.co_usuario_fiscal'] = $this->data['ContratoFiscal']['co_usuario_fiscal'];
        $conditions['ContratoFiscal.co_contrato'] = $this->data['ContratoFiscal']['co_contrato'];
        $conditions['ContratoFiscal.co_fiscais_tipos'] = $this->data['ContratoFiscal']['co_fiscais_tipos'];

        if(!empty($this->id)) {
            $conditions['ContratoFiscal.co_contrato_fiscal <>'] = $this->id;
        }

        $fiscal = $this->find('count', array('conditions' => $conditions));

        if ($fiscal > 0) {
            return true;
        }

        return false;
    }

    /**
     * Valida se a data de nomeação está entre algum período vigente.
     *
     * @return bool
     */
    private function _validaDataNomeacao( $semDtFim = false )
    {
        $conditions     = array();

        if($semDtFim) {
            if (empty($this->data['ContratoFiscal']['dt_fim'])) {
                $conditions["ContratoFiscal.dt_inicio <="] = $this->data['ContratoFiscal']['dt_inicio'];
                $conditions["ContratoFiscal.dt_fim"] = NULL;
            } else {
                $conditions["ContratoFiscal.dt_inicio >="] = $this->data['ContratoFiscal']['dt_inicio'];
            }
        } else {
            if (empty($this->data['ContratoFiscal']['dt_fim'])) {
                $conditions["ContratoFiscal.dt_inicio >="] = $this->data['ContratoFiscal']['dt_inicio'];
            } else {
                $conditions["OR"]["? BETWEEN ContratoFiscal.dt_inicio AND ContratoFiscal.dt_fim"] = $this->data['ContratoFiscal']['dt_inicio'];
                $conditions["OR"]["ContratoFiscal.dt_inicio BETWEEN ? AND '" . $this->data['ContratoFiscal']['dt_fim'] . "'"] = $this->data['ContratoFiscal']['dt_inicio'];
            }
        }

        $conditions['ContratoFiscal.co_usuario_fiscal'] = $this->data['ContratoFiscal']['co_usuario_fiscal'];
        $conditions['ContratoFiscal.co_contrato'] = $this->data['ContratoFiscal']['co_contrato'];
        $conditions['ContratoFiscal.co_fiscais_tipos'] = $this->data['ContratoFiscal']['co_fiscais_tipos'];

        if(!empty($this->id)) {
            $conditions['ContratoFiscal.co_contrato_fiscal <>'] = $this->id;
        }

        $fiscal = $this->find('count', array('conditions' => $conditions));

        if ($fiscal > 0) {
            return false;
        }

        return true;
    }

    /**
     * Valida se a data de exoneração não é nula e se esta entre um
     * intervalo existente ou é nula e a data inicio não esta entre um intervalo existente
     *
     * @return bool
     */
    private function _validaDataExoneracao($semDtFim = false)
    {
        $conditions     = array();

        if($semDtFim) {
            $conditions["ContratoFiscal.dt_inicio <="] = $this->data['ContratoFiscal']['dt_fim'];
        } else {
            $conditions["? BETWEEN ContratoFiscal.dt_inicio AND ContratoFiscal.dt_fim"] = $this->data['ContratoFiscal']['dt_fim'];
        }

        $conditions['ContratoFiscal.co_usuario_fiscal'] = $this->data['ContratoFiscal']['co_usuario_fiscal'];
        $conditions['ContratoFiscal.co_contrato'] = $this->data['ContratoFiscal']['co_contrato'];
        $conditions['ContratoFiscal.co_fiscais_tipos'] = $this->data['ContratoFiscal']['co_fiscais_tipos'];

        if(!empty($this->id)) {
            $conditions['ContratoFiscal.co_contrato_fiscal <>'] = $this->id;
        }

        $fiscal = $this->find('count', array('conditions' => $conditions));

        if ($fiscal > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return string
     */
    private function convertData($data)
    {
        $objDateTime = DateTime::createFromFormat('d/m/Y', $data);
        return $objDateTime->format('Y-m-d');

    }

    /**
     * @param $coUsuarioFiscal
     * @param $coContrato
     * @param $coFiscaisTipo
     * @param int $icAtivo
     * @return array
     */
    public function getFiscalContrato($coUsuarioFiscal, $coContrato, $coFiscaisTipo, $icAtivo=1)
    {
        return
            $this->find('all', array('conditions' => array(
                'ContratoFiscal.co_usuario_fiscal' => $coUsuarioFiscal,
                'ContratoFiscal.co_contrato' => $coContrato,
                'ContratoFiscal.co_fiscais_tipos' => $coFiscaisTipo,
                'ContratoFiscal.ic_ativo' => $icAtivo
            ),
                'order' => 'ContratoFiscal.dt_fim DESC'
            ));
    }

    public function findAllByUsuario($co_usuario, $recursive = -1)
    {
        $this->recursive = $recursive;
        $contratos = $this->find('all', array('conditions' => array('co_usuario_fiscal' => $co_usuario)));

        $arrayContratos = array();
        foreach ($contratos as $cf) {
            array_push($arrayContratos, $cf['ContratoFiscal']['co_contrato']);
        }

        return $arrayContratos;
    }

    public function findAllFiscaisByContrato($co_contrato)
    {
        $this->recursive = 0;
        $contratosFiscais = $this->find('all', array('conditions' => array('co_contrato' => $co_contrato)));
        $fiscais = array();
        foreach ($contratosFiscais as $cf) {
            array_push($fiscais, $cf['Usuario']);
        }
        return $contratosFiscais;
    }
}

?>
