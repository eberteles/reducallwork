 <?php

class ContratosContato extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'ContratosContato';

    var $validate = array(
        'id' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em branco'
            )
        )
    );
    
    public function beforeSave($options = array())
    {
        if (empty($this->data['ContratosContato']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['ContratosContato']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        $this->data['ContratosContato']['nome'] = up($this->data['ContratosContato']['nome']);

        if (isset($this->data['ContratosContato']['cargo'])) {
            $this->data['ContratosContato']['cargo'] = up($this->data['ContratosContato']['cargo']);
        }
        if (isset($this->data['ContratosContato']['departamento'])) {
            $this->data['ContratosContato']['departamento'] = up($this->data['ContratosContato']['departamento']);
        }
        if (isset($this->data['ContratosContato']['email'])) {
            $this->data['ContratosContato']['email'] = up($this->data['ContratosContato']['email']);
        }
        if (isset($this->data['ContratosContato']['endereco'])) {
            $this->data['ContratosContato']['endereco'] = up($this->data['ContratosContato']['endereco']);
        }
        
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>