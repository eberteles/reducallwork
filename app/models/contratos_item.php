<?php

class ContratosItem extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'ContratosItem';

    public $useTable = 'contratos_itens';

    public $primaryKey = 'co_contrato_item';

    public function beforeSave($options = array())
    {
        if (isset($this->data['ContratosItem']['nu_unidade'])) {
            $this->data['ContratosItem']['nu_unidade'] = up($this->data['ContratosItem']['nu_unidade']);
        }
        if (isset($this->data['ContratosItem']['ds_demanda'])) {
            $this->data['ContratosItem']['ds_demanda'] = up($this->data['ContratosItem']['ds_demanda']);
        }
        if (isset($this->data['ContratosItem']['nu_item_pregao'])) {
            $this->data['ContratosItem']['nu_item_pregao'] = ln($this->data['ContratosItem']['nu_item_pregao']);
        }
        if (isset($this->data['ContratosItem']['qt_solicitada'])) {
            $this->data['ContratosItem']['qt_solicitada'] = ln($this->data['ContratosItem']['qt_solicitada']);
        }
        if (isset($this->data['ContratosItem']['vl_unitario'])) {
            $this->data['ContratosItem']['vl_unitario'] = ln($this->data['ContratosItem']['vl_unitario']);
        }
        if (isset($this->data['ContratosItem']['co_siasg'])) {
            $this->data['ContratosItem']['co_siasg'] = ln($this->data['ContratosItem']['co_siasg']);
        }
        if (isset($this->data['ContratosItem']['qt_consumo'])) {
            $this->data['ContratosItem']['qt_consumo'] = ln($this->data['ContratosItem']['qt_consumo']);
        }
        
        return parent::beforeSave($options);
    }
    
    var $belongsTo = array(
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        )
    );

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
    
}
?>