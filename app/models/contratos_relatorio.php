<?php

class ContratosRelatorio extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'ContratosRelatorio';

    var $useTable = 'contratos';

    var $primaryKey = 'co_contrato';

    var $order = "ContratosRelatorio.co_contrato DESC";

    function __construct()
    {
        parent::__construct();
    }
    
     function beforeFind($queryData)
     {
         if(count($queryData['conditions']) == 0){
             $queryData['conditions']['ContratosRelatorio.ic_ativo'] = 1;
         }
         return $queryData;
     }

    public function getValores($coContratosRelatorio)
    {
        $result = $this->query("SELECT nu_contrato, nu_processo, vl_inicial, vl_mensal, vl_global, tp_valor,
                                            dt_ini_vigencia, dt_fim_vigencia, dt_ini_processo, dt_fim_processo FROM contratos
                                    WHERE co_contrato = '$coContratosRelatorio' ");
        $contrato['vl_inicial'] = $result[0]['contratos']['vl_inicial'];
        $contrato['vl_mensal'] = $result[0]['contratos']['vl_mensal'];
        $contrato['vl_global'] = $result[0]['contratos']['vl_global'];
        $contrato['tp_valor'] = $result[0]['contratos']['tp_valor'];
        $contrato['nu_contrato'] = $result[0]['contratos']['nu_contrato'];
        $contrato['nu_processo'] = $result[0]['contratos']['nu_processo'];
        $contrato['dt_ini_vigencia'] = $result[0]['contratos']['dt_ini_vigencia'];
        $contrato['dt_fim_vigencia'] = $result[0]['contratos']['dt_fim_vigencia'];
        $contrato['dt_ini_processo'] = $result[0]['contratos']['dt_ini_processo'];
        $contrato['dt_fim_processo'] = $result[0]['contratos']['dt_fim_processo'];
        return $contrato;
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>