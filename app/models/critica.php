<?php

class Critica extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Critica';

    var $useTable = 'log_criticas';

    var $primaryKey = 'co_critica';

    var $displayField = 'ds_critica';

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}