<?php

class CronogramaFinanceiroDesembolso extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'CronogramaFinanceiroDesembolso';

    public $useTable = 'cronograma_financeiro_desembolso';

    public $primaryKey = 'co_cronograma_financeiro_desembolso';

    public $validate = array(
        
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'O campo de descrição não pode ficar em branco'
            )
        ),
        
        'mes' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'O campo de mes não pode ficar em branco'
            )
        ),
        
        'ds_valor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'O campo de valor não pode ficar em branco'
            )
        ),
        
        'percentual' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'O campo de percentual não pode ficar em branco'
            )
        ),
        
        'dt_inicio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'A data inicial não pode fica em branco'
            )
        ),
        
        'dt_data_final' => array(
            'rule' => array(
                'validaDataFinal',
                true
            ),
            'message' => 'A data final deve ser menor que a data inicial'
        )
    );

    public function validaDataFinal()
    {
        if (! isset($this->data['CronogramaFinanceiroDesembolso']['dt_inicio'])) {
            return false;
        } else {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['CronogramaFinanceiroDesembolso']['dt_inicio'], $this->data['CronogramaFinanceiroDesembolso']['dt_data_final'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }
    
    public function beforeSave( $options = array() )
    {
        
        if ( isset( $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'descricao' ] ) ) {
            $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'descricao' ] = up( $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'descricao' ] );
        }
        if ( isset( $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'percentual' ] ) ) {
            $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'percentual' ] = str_replace(',', '.', $this->data[ 'CronogramaFinanceiroDesembolso' ][ 'percentual' ] ) ;
        }

        return parent::beforeSave( $options );
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}