<?php

class Dashboard extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Dashboard';

    var $useTable = 'dashboards';

    var $primaryKey = 'co_dashboard';

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>