<?php

class DspCamposAuxiliares extends AppModel
{

    var $name = 'DspCamposAuxiliares';

    var $useTable = 'dsp_unidade_lotacao';

    var $primaryKey = '';

    var $displayField = '';

    var $validate = array(
        'tipo_campo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao_campo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }
}
?>