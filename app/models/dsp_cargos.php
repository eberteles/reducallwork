<?php

class DspCargos extends AppModel
{

    var $name = 'DspCargos';

    var $useTable = 'dsp_cargos';

    var $primaryKey = 'co_cargo';

    var $displayField = 'nome_cargo';

    var $validate = array(
        'nome_cargo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        //$this->data['DspCargos']['nome_cargo'] = up($this->data['DspCargos']['nome_cargo']);
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {

        $this->data['DspCargos'] = array_map('strtoupper', $this->data['DspCargos']);

        return parent::beforeSave($options);
    }
}
?>