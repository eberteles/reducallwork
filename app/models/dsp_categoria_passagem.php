<?php

class DspCategoriaPassagem extends AppModel
{

    var $name = 'DspCategoriaPassagem';

    var $useTable = 'dsp_categoria_passagem';

    var $primaryKey = 'co_categoria_passagem';

    var $displayField = 'nome_categoria_passagem';

    var $validate = array(
        'nome_categoria_passagem' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        //$this->data['DspCategoriaPassagem']['nome_categoria_passagem'] = up($this->data['DspCategoriaPassagem']['nome_categoria_passagem']);
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $this->data['DspCategoriaPassagem'] = array_map('strtoupper', $this->data['DspCategoriaPassagem']);

        return parent::beforeSave($options);
    }
}
?>