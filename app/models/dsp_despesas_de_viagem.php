<?php

class DspDespesasDeViagem extends AppModel
{

    var $name = 'DspDespesasDeViagem';

    var $useTable = 'dsp_despesas_viagem';

    var $primaryKey = 'co_despesas_viagem';

    var $order = 'DspDespesasDeViagem.co_despesas_viagem DESC';

    var $validate = array(
        'co_funcionario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'numero_processo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'motivo_viagem' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'cod_unidade_lotacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'cod_cargo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'origem' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'destino' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'dt_final' => array(
            'date' => array(
                'rule' => array(
                    'validarDatas'
                ),
                'message' => 'A data final deve ser maior que a data inicial',
                'allowEmpty' => true
            )
        )
    );
    
    var $belongsTo = array(
        'GrupoAuxiliar' => array(
            'className' => 'GrupoAuxiliar',
            'foreignKey' => 'co_funcionario'
        )
    );

    public function validarDatas(){
        if(isset($this->data['DspDespesasDeViagem']['dt_inicio'])){
            $inicio = date(substr($this->data['DspDespesasDeViagem']['dt_inicio'], 6, 4) . '-' . substr($this->data['DspDespesasDeViagem']['dt_inicio'], 3, 2) . '-' . substr($this->data['DspDespesasDeViagem']['dt_inicio'], 0, 2));
            $final = date(substr($this->data['DspDespesasDeViagem']['dt_final'], 6, 4) . '-' . substr($this->data['DspDespesasDeViagem']['dt_final'], 3, 2) . '-' . substr($this->data['DspDespesasDeViagem']['dt_final'], 0, 2));
        }else{
            return true;
        }

        if( $inicio > $final ){
            return false;
        }else{
            return true;
        }
    }

    function beforeValidate($options = array())
    {
        if(isset($this->data['DspDespesasDeViagem']['dt_inicio'])){
            $this->data['DspDespesasDeViagem']['dt_inicio'] = date(substr($this->data['DspDespesasDeViagem']['dt_inicio'], 6, 4) . '-' . substr($this->data['DspDespesasDeViagem']['dt_inicio'], 3, 2) . '-' . substr($this->data['DspDespesasDeViagem']['dt_inicio'], 0, 2));
            $this->data['DspDespesasDeViagem']['dt_final'] = date(substr($this->data['DspDespesasDeViagem']['dt_final'], 6, 4) . '-' . substr($this->data['DspDespesasDeViagem']['dt_final'], 3, 2) . '-' . substr($this->data['DspDespesasDeViagem']['dt_final'], 0, 2));
        }

        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        if(isset($this->data['DspDespesasDeViagem']['valor_passagem'])){
            $this->data['DspDespesasDeViagem']['valor_passagem'] = ln($this->data['DspDespesasDeViagem']['valor_passagem']);
        }

        if(isset($this->data['DspDespesasDeViagem']['numero_diarias'])){
            $this->data['DspDespesasDeViagem']['numero_diarias'] = str_replace(",", ".", $this->data['DspDespesasDeViagem']['numero_diarias']);
        }

        if(isset($this->data['DspDespesasDeViagem']['valor_total_diarias'])){
            $this->data['DspDespesasDeViagem']['valor_total_diarias'] = ln($this->data['DspDespesasDeViagem']['valor_total_diarias']);
        }

        if(isset($this->data['DspDespesasDeViagem']['valor_total_viagem'])){
            $this->data['DspDespesasDeViagem']['valor_total_viagem'] = $this->data['DspDespesasDeViagem']['valor_passagem'] + $this->data['DspDespesasDeViagem']['valor_total_diarias'];
        }

        return parent::beforeSave($options);
    }
}
?>