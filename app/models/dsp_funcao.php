<?php

class DspFuncao extends AppModel
{

    var $name = 'DspFuncao';

    var $useTable = 'dsp_funcao';

    var $primaryKey = 'co_funcao';

    var $displayField = 'nome_funcao';

    var $validate = array(
        'nome_funcao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        //$this->data['DspFuncao']['nome_funcao'] = up($this->data['DspFuncao']['nome_funcao']);
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $this->data['DspFuncao'] = array_map('strtoupper', $this->data['DspFuncao']);

        return parent::beforeSave($options);
    }
}
?>