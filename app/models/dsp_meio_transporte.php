<?php

class DspMeioTransporte extends AppModel
{

    var $name = 'DspMeioTransporte';

    var $useTable = 'dsp_meio_transporte';

    var $primaryKey = 'co_meio_transporte';

    var $displayField = 'nome_meio_transporte';

    var $validate = array(
        'nome_meio_transporte' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        //$this->data['DspMeioTransporte']['nome_meio_transporte'] = up($this->data['DspMeioTransporte']['nome_meio_transporte']);
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $this->data['DspMeioTransporte'] = array_map('strtoupper', $this->data['DspMeioTransporte']);

        return parent::beforeSave($options);
    }
}
?>