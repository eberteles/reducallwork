<?php

class DspRelatorio extends AppModel
{

    var $name = 'DspRelatorio';

    var $useTable = 'dsp_despesas_viagem';

    var $primaryKey = 'co_despesas_viagem';

    var $order = 'DspRelatorio.co_despesas_viagem DESC';
}
?>