<?php

class DspUnidadeLotacao extends AppModel
{

    var $name = 'DspUnidadeLotacao';

    var $useTable = 'dsp_unidade_lotacao';

    var $primaryKey = 'co_unidade';

    var $displayField = 'nome_unidade';

    var $validate = array(
        'nome_unidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        //$this->data['DspUnidadeLotacao']['nome_unidade'] = up($this->data['DspUnidadeLotacao']['nome_unidade']);
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $this->data['DspUnidadeLotacao'] = array_map('strtoupper', $this->data['DspUnidadeLotacao']);

        return parent::beforeSave($options);
    }
}
?>