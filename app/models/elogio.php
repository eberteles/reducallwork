<?php
/**
 * Created by PhpStorm.
 * User: João Marcos Bizarro Lopes
 * Date: 11/11/2015
 * Time: 15:10
 */

class Elogio extends AppModel
{
    var $name       = "Elogio";
    var $useTable   = "elogios";
    var $primaryKey = "co_elogio";
    var $recursive = 1;

    var $validate = array(
        'nu_elogio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Elogio em branco'
            )
        ),
        'co_fornecedor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Fornecedor em branco'
            )
        ),
        'co_contrato' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Contrato em branco'
            )
        )
    );

    var $belongsTo = array(
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'co_contrato'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        )
    );

    public function beforeSave($options = array())
    {
        if(isset($this->data["Elogio"]["nu_elogio"]))
            $this->data["Elogio"]["nu_elogio"] = str_replace("/","",$this->data["Elogio"]["nu_elogio"]);

        if(!isset($this->data["Elogio"]["dt_elogio"])){
            $this->data["Elogio"]["dt_elogio"] = date('Y-m-d');
        }

        return parent::beforeSave($options);
    }
}