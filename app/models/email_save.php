<?php

class EmailSave extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'EmailSave';

    var $useTable = 'emails';

    var $primaryKey = 'id';
    
    public $belongsTo = array(
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        ),
        'Remetente' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_remetente'
        ),
        'Destinatario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_destinatario'
        )
    );

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>