<?php

class Empenho extends AppModel
{

    /**
     *
     */
    const TIPO_ORIGINAL = 'O';

    /**
     *
     */
    const TIPO_REFORCO = 'R';

    /**
     *
     */
    const TIPO_ANULACAO = 'A';

    /**
     *
     */
    const TIPO_CANCELAMENTO = 'C';

    /**
     *
     */
    const TIPO_ESTORNO_DE_ANULACAO = 'E';

    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Empenho';

    var $useTable = 'empenhos';

    var $primaryKey = 'co_empenho';

    var $virtualFields = array(
        'lista_empenho' => 'CONCAT(Empenho.nu_empenho, " - ", Empenho.ds_empenho)',
        'tt_pg_empenho' => '(select sum(p.vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_empenho = Empenho.co_empenho )'
    );

    var $displayField = 'lista_empenho';

    var $validate = array(
        'co_empenho' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),

        'nu_empenho' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Empenho em branco'
            ),
            'isUnique' => array(
                'rule' => array('isUniqueEmpenho'),
                'message' => 'O empenho já foi cadastrado para esta UASG!'
            )
        ),
        'tp_empenho' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Tipo em branco'
            )
        ),
        'ds_empenho' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco',
                'allowEmpty' => true
            )
        ),
        'vl_empenho' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
//                'message' => 'Esta mensagem é setada no beforeValidate',
                'allowEmpty' => false
            ),
        ),
        'dt_empenho' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data em branco',
                'allowEmpty' => false
            ),
            'dataVigenciaContrato' => array(
                'rule' => array(
                    'validateVigenciaContrato'
                ),
                'message' => 'Esta mensagem é setada no beforeValidate'
            )
        ),
        'nu_ptres' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo PTRES em branco',
                'allowEmpty' => true
            )
        ),
        'sub_nu_ptres' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo SUB_PTRES em branco',
                'allowEmpty' => true
            )
        ),
        'empenho_reforco_originario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Vinculo em branco',
                'allowEmpty' => false
            )
        ),
        'empenho_anulacao_originario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Vinculo em branco',
                'allowEmpty' => false
            )
        ),
        'empenho_cancelamento_originario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Vinculo em branco',
                'allowEmpty' => false
            )
        ),
        'empenho_estorno_de_anulacao_originario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Vinculo em branco',
                'allowEmpty' => false
            )
        ),
    );

    var $hasAndBelongsToMany = array(
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'joinTable' => 'notas_empenhos',
            'foreignKey' => 'co_empenho',
            'with' => 'NotasEmpenho',
            'associationForeignKey' => 'co_nota',
        )
    );

    var $hasMany = array();

    public function validateVigenciaContrato($data)
    {
        App::import('Model', 'Contrato');
        $contratoModel = new Contrato();
        $coContrato = $this->data['Empenho']['co_contrato'];
        $contrato = $contratoModel->find(array('co_contrato' => $coContrato));
        if ($contrato['Contrato']['nu_contrato'] != '') {
            $sql = "SELECT * FROM {$contratoModel->useTable} WHERE co_contrato = {$coContrato} AND '{$data['dt_empenho']}' <= dt_fim_vigencia";
            $countResult = count($this->query($sql));
        } else {
            //Em caso de processo, a data nao é obrigatoria.
            $countResult = 1;
        }
        return empty($data['dt_empenho']) ? true : $countResult;
    }

    public function beforeValidate($options = array())
    {
        $tipoDeEmpenho = $this->data['Empenho']['tp_empenho'];
        if (
            in_array($tipoDeEmpenho, array(self::TIPO_ORIGINAL, self::TIPO_ANULACAO))
            && empty($this->data['Empenho']['empenho_reforco_originario'])
        ) {
            unset(
                $this->validate['empenho_reforco_originario'],
                $this->validate['empenho_cancelamento_originario'],
                $this->validate['empenho_estorno_de_anulacao_originario']
            );
        }

        if (($tipoDeEmpenho === self::TIPO_ORIGINAL || $tipoDeEmpenho === self::TIPO_REFORCO)
            && empty( $this->data['Empenho']['empenho_anulacao_originario'])
        ) {
            unset(
                $this->validate['empenho_anulacao_originario'],
                $this->validate['empenho_cancelamento_originario'],
                $this->validate['empenho_estorno_de_anulacao_originario']
            );
        }

        if ($tipoDeEmpenho === self::TIPO_CANCELAMENTO) {
            unset(
                $this->validate['empenho_reforco_originario'],
                $this->validate['empenho_anulacao_originario'],
                $this->validate['empenho_estorno_de_anulacao_originario']
            );
            if (isset($this->data['Empenho']['empenho_cancelamento_originario'])) {
                $valorDeCancelamentoDeEmpenho = ln($this->data['Empenho']['vl_empenho']);
                $valorDaAnulacaoDoEmpenho = $this->field('vl_restante', array('co_empenho' => $this->data['Empenho']['empenho_cancelamento_originario']));
                if ($valorDaAnulacaoDoEmpenho - $valorDeCancelamentoDeEmpenho < 0) {
                    $this->invalidate('vl_empenho', 'O valor do empenho do cancelamento não pode ser maior que o valor do empenho original.');
                }
            }
        }
        if ($tipoDeEmpenho === self::TIPO_ESTORNO_DE_ANULACAO) {
            unset(
                $this->validate['empenho_reforco_originario'],
                $this->validate['empenho_anulacao_originario'],
                $this->validate['empenho_cancelamento_originario']
            );
            if (isset($this->data['Empenho']['empenho_estorno_de_anulacao_originario'])) {
                $valorDoEstornoDeAnulacaoDoEmpenho = ln($this->data['Empenho']['vl_empenho']);
                $valorDaAnulacaoDoEmpenho = $this->field('vl_restante', array('co_empenho' => $this->data['Empenho']['empenho_estorno_de_anulacao_originario']));
                if ($valorDaAnulacaoDoEmpenho - $valorDoEstornoDeAnulacaoDoEmpenho < 0) {
                    $this->invalidate('vl_empenho', 'O valor do estorno da anulação não pode ser maior que o valor do empenho anulado.');
                }
            }
        }

        if ($tipoDeEmpenho == self::TIPO_ORIGINAL && mb_strtoupper($this->data['Empenho']['ds_empenho']) == 'EMPENHO ORIGINÁRIO') {
            unset($this->validate['dt_empenho']);
        } else {
            $this->validate['dt_empenho']['dataVigenciaContrato']['message'] = 'A data do ' . __('empenho', true) . ' não deve ultrapassar o fim da vigência do contrato.';
        }

        // Aplicando tradução para o empenho
        $this->validate['vl_empenho']['notempty']['message'] = 'Campo valor ' . __('doEmpenho', true) . ' em branco';
        $this->validate['nu_empenho']['notempty']['message'] = 'Campo ' . __('Empenho', true) . ' em branco';
        $this->validate['nu_empenho']['isUnique']['message'] = 'O ' . __('empenho', true) . ' já foi cadastrado para esta UASG!';

        return parent::beforeValidate($options);
    }

    public function getNumerosEmpenhos()
    {
        $r = $this->query("SELECT DISTINCT nu_empenho FROM empenhos WHERE nu_empenho!='' GROUP BY nu_empenho");
        $empenhos = array();
        foreach ($r as $c)
            $empenhos[] = $c['empenhos']['nu_empenho'];
        return $empenhos;
    }

    public function beforeSave($options = array())
    {
        $tipoDoEmepenho = $this->data['Empenho']['tp_empenho'];
        if (empty($this->data['Empenho']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Empenho']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        if (isset($this->data['Empenho']['ds_empenho'])) {
            $this->data['Empenho']['ds_empenho'] =
                up($this->data['Empenho']['ds_empenho']);
        }
        if (isset($this->data['Empenho']['ds_fonte_recurso'])) {
            $this->data['Empenho']['ds_fonte_recurso'] = up($this->data['Empenho']['ds_fonte_recurso']);
        }
        if ($tipoDoEmepenho == self::TIPO_ANULACAO || $tipoDoEmepenho == self::TIPO_CANCELAMENTO) {
            $this->data['Empenho']['vl_empenho'] = ln($this->data['Empenho']['vl_empenho']);
            if ($this->data['Empenho']['vl_empenho'] > 0)
                $this->data['Empenho']['vl_empenho'] *= (-1);
        }

        if (isset($this->data['Empenho']['vl_empenho']) && $this->data['Empenho']['tp_empenho'] != 'A') {
            $this->data['Empenho']['vl_empenho'] = ln($this->data['Empenho']['vl_empenho']);
        }

        if (isset($this->data['Empenho']['vl_restante'])) {
            $this->data['Empenho']['vl_restante'] = ln($this->data['Empenho']['vl_restante']);
        }

        return parent::beforeSave($options);
    }

    public function insertOrUpdate($empenho)
    {
        $siasg = new Siasg();
        $id = null;
        $new = false;
        $qC = 'SELECT co_empenho FROM empenhos WHERE nu_empenho="' . $empenho['nu_empenho'] . '"';
        $r = $this->query($qC);
        if (sizeof($r) > 0) $id = $r[0]['empenhos']['co_empenho'];

        if (!$id) {
            $new = true;
            $query = "INSERT INTO empenhos(";
            foreach ($empenho as $k => $v) $query .= $k . ",";
            $query = substr($query, 0, -1);
            $query .= ") VALUES(";
            foreach ($empenho as $k => $v) $query .= '"' . $v . '",';
            $query = substr($query, 0, -1);
            $query .= ")";
            $id = $siasg->query($query);
        } else {
            $query = "UPDATE empenhos SET ";
            foreach ($empenho as $k => $v) $query .= $k . '="' . $v . '",';
            $query = substr($query, 0, -1);
            $query .= " WHERE co_empenho=" . $id;
            $siasg->query($query);
        }
        $ret = array(
            'new' => $new
        , 'id' => $id
        );
        return $ret;
    }

    public function getNextEmpenho()
    {
        $nextEmpenho = 0;

        $empenhoAtual = $this->query(" SELECT ct_empenho FROM numeracao_empenho
                                                    WHERE aa_empenho = NOW( ) ");
        if (count($empenhoAtual)) {

            foreach ($empenhoAtual as $empenho) {
                $nextEmpenho = (int)$empenho['numeracao_empenho']['ct_empenho'] + 1;
            }
            $this->query(" UPDATE numeracao_empenho
                                    SET  ct_empenho =  '$nextEmpenho'
                                    WHERE  aa_empenho = NOW() ");
        } else {
            $nextEmpenho = 1;
            $this->query(" INSERT INTO numeracao_empenho
                                    ( aa_empenho , ct_empenho ) VALUES
                                    ( NOW(),  '$nextEmpenho' ) ");
        }

        while (strlen($nextEmpenho) < 4) {
            $nextEmpenho = "0" . $nextEmpenho;
        }

        return $nextEmpenho . "NE" . date("Y");
    }

    public function getTotal($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_empenho ) as vl_total FROM empenhos
                                    WHERE co_contrato = '$co_contrato' AND tp_empenho not in ('A')");
        $result2 = $this->query("SELECT SUM( vl_empenho ) as vl_total FROM empenhos
                                    WHERE co_contrato = '$co_contrato' AND tp_empenho in ('A')");
        return $result[0][0]['vl_total'] + $result2[0][0]['vl_total'];
    }

    // Deletar as notas fiscais vinculadas ao empenho
    public function afterDelete()
    {
        App::import('Model', 'NotaFiscal');
        $notaFiscal = new NotaFiscal();
        if (!empty($this->data['NotaFiscal'])) {
            foreach ($this->data['NotaFiscal'] as $index => $nota) {
                $notaFiscal->delete($nota['co_nota'], true);
            }
        }
    }

    public function isUniqueEmpenho($data)
    {

        $empenho = $this->find($data);

        if ($empenho) {
            App::import('Model', 'Contrato');
            $contratoModel = new Contrato();

            $uasgEmpenho = $contratoModel->field('uasg', array('co_contrato' => $empenho['Empenho']['co_contrato']));
            $uasg = $contratoModel->field('uasg', array('co_contrato' => $this->data['Empenho']['co_contrato']));

            if ($uasgEmpenho == $uasg && empty($this->data['Empenho']['co_empenho']))
                return false;
        }

        return true;
    }
}

?>
