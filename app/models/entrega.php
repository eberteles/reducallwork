<?php

class Entrega extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Entrega';

    var $primaryKey = 'co_entrega';

    var $validate = array(
        'co_entrega' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'dt_entrega' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Entrega em branco'
            )
        ),
        'dt_entrega_oficial' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Entrega Oficial em branco'
            )
        ),
        'pz_recebimento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Prazo de Recebimento em branco'
            )
        ),
        'dt_recebimento' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Recebimento em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (empty($this->data['Entrega']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Entrega']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>