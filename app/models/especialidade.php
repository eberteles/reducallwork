<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 15:12
 */

class Especialidade extends AppModel
{
    var $name       = "Especialidade";
    var $useTable   = "especialidade";
    var $primaryKey = "co_especialidade";
    var $displayField = "ds_especialidade";

    var $validate = array(
        'ds_especialidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição da Especialidade em branco'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Esta especialidade já está cadastrada'
            )
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    function beforeFind($queryData)
    {
        if(!isset($queryData['conditions']['Especialidade.ic_ativo']))
            $queryData['conditions']['Especialidade.ic_ativo'] = 1;

        return $queryData;
    }
}