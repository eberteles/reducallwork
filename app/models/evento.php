<?php

class Evento extends AppModel
{

    var $name = 'Evento';

    var $useTable = 'eventos';

    var $primaryKey = 'co_evento';

    var $displayField = 'ds_evento';

    var $order = "nu_ano DESC, ds_evento ASC";
    
    var $icSexo     = array(
        'M' => 'Masculino',
        'F' => 'Feminino'
    );
    
    var $icEvento   = array(
        'N' => 'Evento Nacional',
        'I' => 'Evento Internacional'
    );

    var $validate = array(
        'co_confederacao' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Confederação em branco'
            )
        ),
        'co_usuario' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Usuário em branco'
            )
        ),
        'co_modalidade' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Modalidade em branco'
            )
        ),
        'co_prova' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Prova em branco'
            )
        ),
        'co_categoria' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Tipo de Evento em branco'
            )
        ),
        'nu_ano' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Ano do Evento em branco'
            )
        ),
        'ds_evento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Evento em branco'
            )
        ),
        'ic_evento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Indicação do Evento em branco'
            )
        ),
        'dt_inicio' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Início inválido.'
            )
        ),
        'dt_fim' => array(
            'validaDtFim' => array(
                'rule' => array(
                    'validaDtFim',
                    true
                ),
                'message' => 'A Data Final deve ser maior que a Data de Início.'
            ),
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Fim inválido.'
            )
        ),
        'ds_local' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Local em branco'
            )
        ),
        'ufs_evento' => array(
            'validaUfEvento' => array(
                'rule' => array(
                    'validaUfEvento',
                    true
                ),
                'message' => 'Campo Estado(s) Participante(s) do Evento em branco'
            )
        ),
        'paises_evento' => array(
            'validaPaisEvento' => array(
                'rule' => array(
                    'validaPaisEvento',
                    true
                ),
                'message' => 'Campo País(es) Participante(s) do Evento em branco'
            )
        ),
        'ufs_prova' => array(
            'validaUfProva' => array(
                'rule' => array(
                    'validaUfProva',
                    true
                ),
                'message' => 'Campo Estado(s) Participante(s) na Prova em branco'
            )
        ),
        'paises_prova' => array(
            'validaPaisProva' => array(
                'rule' => array(
                    'validaPaisProva',
                    true
                ),
                'message' => 'Campo País(es) Participante(s) na Prova em branco'
            )
        )
    );

    function validaDtFim()
    {
        if (! isset($this->data['Evento']['dt_inicio'])) {
            return false;
        } else {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Evento']['dt_inicio'], $this->data['Evento']['dt_fim'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }
    
    function validaUfEvento()
    {
        if ( $this->data['Evento']['ic_evento'] == "N" ) {
            if( !empty( $this->data['Evento']['ufs_evento'] ) ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    function validaPaisEvento()
    {
        if ( $this->data['Evento']['ic_evento'] == "I" ) {
            if( !empty( $this->data['Evento']['paises_evento'] ) ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    function validaUfProva()
    {
        if ( $this->data['Evento']['ic_evento'] == "N" ) {
            if( !empty( $this->data['Evento']['ufs_prova'] ) ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    function validaPaisProva()
    {
        if ( $this->data['Evento']['ic_evento'] == "I" ) {
            if( !empty( $this->data['Evento']['paises_prova'] ) ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Confederacao' => array(
            'className' => 'Confederacao',
            'foreignKey' => 'co_confederacao'
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario'
        ),
        'Modalidade' => array(
            'className' => 'Modalidade',
            'foreignKey' => 'co_modalidade'
        ),
        'Prova' => array(
            'className' => 'Prova',
            'foreignKey' => 'co_prova'
        ),
        'ProvaCategoria' => array(
            'className' => 'ProvaCategoria',
            'foreignKey' => 'co_prova_categoria'
        ),
        'ProvaSubCategoria' => array(
            'className' => 'ProvaSubCategoria',
            'foreignKey' => 'co_prova_subcategoria'
        ),
        'ProvaClassificacao' => array(
            'className' => 'ProvaClassificacao',
            'foreignKey' => 'co_prova_classificacao'
        ),
        'TipoEvento' => array(
            'className' => 'Categoria',
            'foreignKey' => 'co_categoria'
        )
    );
    
    var $hasMany = array(
        'EventoParticipante' => array(
            'className' => 'EventoParticipante',
            'foreignKey' => 'co_evento',
            'dependent' => true
        ),
        'EventoCompetidor' => array(
            'className' => 'EventoCompetidor',
            'foreignKey' => 'co_evento',
            'dependent' => true
        )
    );

    public function beforeSave($options = array())
    {
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        if (empty($this->data['Evento']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Evento']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        $this->data['Evento']['ds_evento'] = up($this->data['Evento']['ds_evento']);
        $this->data['Evento']['ds_local'] = up($this->data['Evento']['ds_local']);
        
        return parent::beforeSave($options);
    }
}
?>