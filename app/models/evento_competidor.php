<?php

class EventoCompetidor extends AppModel
{

    var $name = 'EventoCompetidor';

    var $useTable = 'eventos_competidores';

    var $primaryKey = 'co_evento_competidor';
    
    var $belongsTo = array(
        'Uf' => array(
            'className' => 'Uf',
            'foreignKey' => 'sg_uf'
        ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'co_pais'
        )
    );

}