<?php

class EventoParticipante extends AppModel
{

    var $name = 'EventoParticipante';

    var $useTable = 'eventos_participantes';

    var $primaryKey = 'co_evento_participante';
    
    var $belongsTo = array(
        'Uf' => array(
            'className' => 'Uf',
            'foreignKey' => 'sg_uf'
        ),
        'Pais' => array(
            'className' => 'Pais',
            'foreignKey' => 'co_pais'
        )
    );

}