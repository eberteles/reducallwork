<?php

class EventoResultado extends AppModel
{

    var $name = 'EventoResultado';

    var $useTable = 'eventos_resultados';

    var $primaryKey = 'co_evento_resultado';
    
    var $validate = array(
        'nu_classificacao' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Classificação em branco'
            )
        )
    );
    
    var $belongsTo = array(
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        )
    );

}