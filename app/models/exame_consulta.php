<?php

class ExameConsulta extends AppModel
{
    var $name = "ExameConsulta";
    var $useTable = "exames_consultas";
    var $primaryKey = "id";
    // var $displayField = "nome";

    var $validate = array(
        'codigo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Código em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Código já cadastrado'
            )
        ),
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Nome do Exame/Consulta em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Nome do Exame/Consulta já cadastrado'
            )
        )
    );

    var $belongsTo = array(
        'Especialidade' => array(
            'className' => 'Especialidade',
            'foreignKey' => 'co_especialidade'
        )
    );

    function beforeFind($queryData)
    {
        if(!isset($queryData['conditions']['ExameConsulta.ic_ativo']))
            $queryData['conditions']['ExameConsulta.ic_ativo'] = 1;

        return $queryData;
    }

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }
}
