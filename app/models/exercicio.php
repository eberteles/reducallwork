<?php

class Exercicio extends AppModel
{

    var $name = 'Exercicio';

    var $useTable = 'exercicios';

    var $validate = array(
        'co_exercicio' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'an_exercicio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'ds_natureza_despesa' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'nu_lei' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        )
    );
}
?>