<?php

class Fase extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Fase';

    var $useTable = 'fases';

    var $primaryKey = 'co_fase';

    var $displayField = 'ds_fase';

    var $validate = array(
        'ds_fase' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );
    
    function beforeValidate($options = array())
    {
        $this->data['Fase']['ds_fase'] = up($this->data['Fase']['ds_fase']);        
        return parent::beforeValidate($options);
    }
}
?>