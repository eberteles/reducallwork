<?php

class Filtro extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Filtro';

    var $useTable = 'filtros';

    var $primaryKey = 'co_filtro';

    var $displayField = 'ds_filtro';

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}