<?php

class Financeiro extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Financeiro';
    // var $useDbConfig = 'financeiro';
    var $useTable = 'ged_financeiro';

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>