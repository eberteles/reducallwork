<?php

class Fiscal extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Fiscal';

    var $useTable = 'fiscais';

    var $primaryKey = 'co_fiscal';

    var $displayField = 'lista_fiscal';

    var $virtualFields = array(
        'lista_fiscal' => '(Fiscal.no_fiscal)'
    );

    var $validate = array(
        
        'co_setor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Setor em branco'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'nu_cpf_fiscal' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo CPF em branco'
            ), // 'allowEmpty' => false,
            
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'CPF já cadastrada'
            )
        ) // 'allowEmpty' => false,
,
        
        'ds_email' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo E-mail em branco'
            ),
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Informe um e-mail válido!'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'E-mail já cadastrada'
            )
        ) // 'allowEmpty' => false,
,
        'no_fiscal' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em branco'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'nu_siape' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Matrícula em branco'
            ),
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Matrícula só aceita números'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Matrícula já cadastrada'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'nu_portaria' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'nu_telefone' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone em branco'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'st_fiscal' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Situação em branco'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Observação em branco'
            )
        ) // 'allowEmpty' => false,

    ) // 'required' => false,
; // 'last' => false, // Stop validation after this rule
      // 'on' => 'create', // Limit validation to 'create' or 'update' operations
    
    

    var $hasOne = array(
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Fiscal']['no_fiscal'] = up($this->data['Fiscal']['no_fiscal']);
        $this->data['Fiscal']['ds_observacao'] = up($this->data['Fiscal']['ds_observacao']);
        
        return parent::beforeValidate($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>