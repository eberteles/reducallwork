<?php

class FiscalTipo extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'FiscalTipo';

    var $useTable = 'fiscais_tipos';

    var $primaryKey = 'co_fiscais_tipos';

    var $displayField = 'no_fiscais_tipos';

    var $validate = array(
        'no_fiscais_tipos' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );

    function beforeSave($options = array())
    {
        $this->data['FiscalTipo']['no_fiscais_tipos'] = up($this->data['FiscalTipo']['no_fiscais_tipos']);
        return parent::beforeValidate($options);
    }

    /**
     * @param $id
     * @return bool
     */
    public function canDelete($id)
    {
        $count = $this->query("Select count(*) as count from contratos_fiscais cf where cf.co_fiscais_tipos = {$id}");
        return ($count[0][0]['count'] == 0);
    }
}
