<?php

class Fluxo extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Fluxo';

    var $useTable = 'fluxos';

    var $primaryKey = 'nu_sequencia';

    var $validate = array(
        'nu_sequencia' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Sequencia em branco'
            )
        ),
    	'co_fase' => array(
			'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Fase em branco'
    		)
		),
    	'co_setor' => array(
			'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Setor em branco'
    		)
		),
    	'tp_operador' => array(
			'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Verificação em branco'
    		)
		)
    );

    var $belongsTo = array(
        'Fase' => array(
            'className' => 'Fase',
            'foreignKey' => 'co_fase'
        ),
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }
}
?>