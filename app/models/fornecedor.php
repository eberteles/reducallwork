<?php

class Fornecedor extends AppModel
{

    /**
     * array(
     *     'UPDATE',
     *     'INSERT',
     *   'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Fornecedor';

    var $useTable = 'fornecedores';

    var $primaryKey = 'co_fornecedor';

    var $displayField = 'no_razao_social';

    var $order = "no_razao_social  ASC";

    var $virtualFields = array(
        'nome_combo' => " CONCAT(
                                if( Fornecedor.tp_fornecedor = 'J', mask(Fornecedor.nu_cnpj, '##.###.###/####-##'),
                                if( Fornecedor.tp_fornecedor = 'F', mask(Fornecedor.nu_cnpj, '###.###.###-##'), Fornecedor.nu_cnpj) ),
                                ' - ', Fornecedor.no_razao_social ) ",
    );

    var $validate = array(
        'nu_cnpj' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo CNPJ/CPF em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'allowEmpty' => true,
                'message' => 'CNPJ/CPF já cadastrado.'
            ),
            'validaCnpj' => array(
                'rule' => array(
                    'validaCnpj',
                    true
                ),
                'message' => 'Campo CNPJ/CPF inválido.'
            )
        ),
        'no_razao_social' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Razão Social/Nome em branco'
            )
        ),
        'ds_email' => array(
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'E-mail já cadastrado.',
                'allowEmpty' => true
            ),
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Informe um e-mail válido!',
                'allowEmpty' => true
            )
        ),
        'nu_cpf_responsavel' => array(
            'rule' => array(
                'validaCpf',
                true
            ),
            'message' => 'CPF do Responsável inválido.',
            'allowEmpty' => true
        )
    );

    function validaCnpj()
    {
        if (!isset($this->data['Fornecedor']['nu_cnpj']) || $this->data['Fornecedor']['nu_cnpj'] == "") {
            return false;
        } else {
            if ($this->data['Fornecedor']['tp_fornecedor'] == 'J') {
                return FunctionsComponent::validaCNPJ($this->data['Fornecedor']['nu_cnpj']);
            } else {
                return FunctionsComponent::validaCPF($this->data['Fornecedor']['nu_cnpj']);
            }
        }
    }

    function validaCpf()
    {
        if (FunctionsComponent::validaCPF($this->data['Fornecedor']['nu_cpf_responsavel'])) {
            return true;
        } else {
            return false;
        }
    }

    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Municipio' => array(
            'className' => 'Municipio',
            'foreignKey' => 'co_municipio'
        ),

        'Banco' => array(
            'className' => 'Banco',
            'foreignKey' => 'co_banco'
        ),
        'Area' => array(
            'className' => 'Area',
            'foreignKey' => 'co_area'
        ),
        'SubArea' => array(
            'className' => 'Area',
            'foreignKey' => 'co_sub_area'
        )
    );

    var $hasMany = array(
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_fornecedor',
            'dependent' => false
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        $this->data['Fornecedor']['no_razao_social'] = up($this->data['Fornecedor']['no_razao_social']);
        if (isset($this->data['Fornecedor']['ds_endereco'])) {
            $this->data['Fornecedor']['ds_endereco'] = up($this->data['Fornecedor']['ds_endereco']);
        }
        if (isset($this->data['Fornecedor']['ds_email'])) {
            $this->data['Fornecedor']['ds_email'] = up($this->data['Fornecedor']['ds_email']);
        }
        if (isset($this->data['Fornecedor']['no_responsavel'])) {
            $this->data['Fornecedor']['no_responsavel'] = up($this->data['Fornecedor']['no_responsavel']);
        }
        if (isset($this->data['Fornecedor']['no_preposto'])) {
            $this->data['Fornecedor']['no_preposto'] = up($this->data['Fornecedor']['no_preposto']);
        }

        if (isset($this->data['Fornecedor']['ds_observacao'])) {
            $this->data['Fornecedor']['ds_observacao'] = up($this->data['Fornecedor']['ds_observacao']);
        }

        return parent::afterSave($created);
    }

    // Validar a vigência da penalidade
    function beforeValidate($options = array())
    {
        if ((isset($this->data['Fornecedor']['dt_ini_penalidade']) && !empty($this->data['Fornecedor']['dt_ini_penalidade'])) && (isset($this->data['Fornecedor']['dt_fim_penalidade']) && !empty($this->data['Fornecedor']['dt_fim_penalidade']))) {
            $fimPenalidade = new DateTime(dtDb($this->data['Fornecedor']['dt_fim_penalidade']));
            $iniPenalidade = new DateTime(dtDb($this->data['Fornecedor']['dt_ini_penalidade']));
            $interval = $iniPenalidade->diff($fimPenalidade);

            if ($interval->format("%r%a") < 0) {
                $this->invalidate('dt_fim_penalidade', 'A data fim da penalidade não pode ser menor que a data de início');
            }
        }

        return parent::beforeValidate($options);
    }

    function beforeFind($queryData)
    {
        if (!isset($queryData['conditions']['Fornecedor.ic_ativo']))
            $queryData['conditions']['Fornecedor.ic_ativo'] = 1;

        return $queryData;
    }

    public function listarFornecedorPorCnpj($cnpj)
    {
        return $this->query("SELECT co_fornecedor, nu_cnpj FROM `fornecedores` WHERE nu_cnpj = '$cnpj'");
    }

    public function listarCNPJaAjutar()
    {
        return $this->query("SELECT co_fornecedor, nu_cnpj FROM `fornecedores` WHERE character_length(nu_cnpj) < 14");
    }

    public function ajustarCNPJ($co_fornecedor, $nu_cnpj)
    {
        return $this->query("UPDATE fornecedores SET nu_cnpj = '$nu_cnpj' WHERE co_fornecedor = $co_fornecedor");
    }

    public function getFornecedor($co_contrato)
    {
        return $this->query("SELECT * FROM fornecedores Fornecedor join contratos c on c.co_fornecedor = Fornecedor.co_fornecedor where c.co_contrato = $co_contrato");
    }
}

?>
