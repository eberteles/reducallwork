<?php

class Funcao extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Funcao';

    var $useTable = 'funcoes';

    var $primaryKey = 'co_funcao';

    var $displayField = 'ds_funcao';

    var $validate = array(
        
        'ds_funcao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );
    
    public function beforeSave($options = array())
    {
        if (empty($this->data['Funcao']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Funcao']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        $this->data['Funcao']['ds_funcao'] = up($this->data['Funcao']['ds_funcao']);

        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

}
?>