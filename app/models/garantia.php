<?php

class Garantia extends AppModel
{
	/**
	 * array(
	 *     'INSERT',
	 *     'UPDATE',
	 *     'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array(
			'INSERT',
			'UPDATE',
			'DELETE'
	);
	
    var $name = 'Garantia';

    var $useTable = 'garantias';

    var $primaryKey = 'co_garantia';

    var $validate = array(
        'co_garantia' => array(
            'numeric' => array('rule' => array('numeric'), 'message' => 'Campo inválido', 'on' => 'update')
        ),
        'co_contrato' => array(
            'numeric' => array('rule' => array('numeric'), 'message' => 'Campo contrato está inválido')
        ),
        'ds_modalidade' => array(
            'notempty' => array('rule' => array('notempty'), 'message' => 'Campo Tipo Garantia em branco')
        ),
        'vl_garantia' => array(
            'porcentagemValida' => array('rule' => array('validarPorcentagemDoValor')),
            // 'money' => array('rule' => array('money'), 'message' => 'Campo Valor em branco'),
        ),
        'dt_inicio' => array(
            'date' => array('rule' => array('date'), 'message' => 'Campo Início em branco', 'allowEmpty' => true)
        ),
        'dt_fim' => array(
            'rule' => array('validaDtFim', true), 'message' => 'A Data Fim deve ser maior que a Data Início.', 'allowEmpty' => true
        ),
    );

     public $belongsTo = array(
        'GarantiaModalidade' => array(
            'className' => 'GarantiaModalidade',
            'foreignKey' => 'co_modalidade_garantia',
        )
     );

    public function validaDtFim()
    {
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        $diferenca = $print->difEmDias($this->data['Garantia']['dt_inicio'], $this->data['Garantia']['dt_fim'], "US");
        return $diferenca > 0;
    }

    /**
     * O valor da garantia submetida no formulário deve seguir a segunte lógica:
     * $valorDaGarantiaSubmetidaNoFormulario + $todasAsGarantiasaCadastradasAnteriormente <= ($ValorAtualDoContrato / 100 * $porcentagemDeGarantiaDoContrato
     * Pega as garantias anteriores, soma elas, soma com a garantia atual que deseja salvar, ver se este valor total está dentro da 
     * porcentagem atual de garantia do contrato
     * Para calcular o valor da porcentagem pega o valor atual do contrato, calcula quanto é o X porcento do valor atual do contrato, e vê se 
     * o total das garantias é menor ou igual a este valor
     * @return boolean
     */
    protected function validarPorcentagemDoValor()
    {
        $idContrato = $this->data[$this->name]['co_contrato'];
        # se for  uma edição desconsidera o valor antigo cadastrado no banco desta garantia
        $idGarantia = isset($this->data[$this->name]['co_garantia']) ? $this->data[$this->name]['co_garantia'] : null;
        $contratoModel = new Contrato();
        $contratoData = $contratoModel->find('first', array('fields' => array(), 'conditions' => array('co_contrato' => $idContrato)));
        $porcentagemDeGarantiaDoContrato = $contratoData[$contratoModel->name]['pc_garantia'];
        $valorAtualDoContrato = $contratoModel->retornaValorAtualDoContrato($idContrato);
        $tetoDoValorPermitidoParaAdicionarNovaGarantiaAoContrato = $valorAtualDoContrato / 100 * $porcentagemDeGarantiaDoContrato;
        $valorDasGarantiasArray = $this->find('all', array(
            'fields' => array('SUM(vl_garantia) as vl_total'),
            'conditions' => array('co_contrato' => $idContrato, 'NOT' => array('co_garantia' => array($idGarantia))))
        );
        $valorDeTodasAsGarantiasJaCadastradas = $valorDasGarantiasArray[0][0]['vl_total'];
        $valorDaGarantiaAtual = ln($this->data[$this->name]['vl_garantia']);
        $valorTotaldasGarantias = $valorDeTodasAsGarantiasJaCadastradas + $valorDaGarantiaAtual;
        $valorTotalAceito = $tetoDoValorPermitidoParaAdicionarNovaGarantiaAoContrato - $valorDeTodasAsGarantiasJaCadastradas;

        $templateMensagemDeErro = "O valor da garantia deve ser de até %s%% do valor atual do contrato<br/>";
        $templateMensagemDeErro .= "Valor atual do contrato: %s<br/>Valor de todas as garantias já cadastradas: %s<br/>";
        $templateMensagemDeErro .= "Valor desta garantia deve ser de até %s";
        setlocale(LC_MONETARY, 'pt_BR', 'ptb');
        $mensagemDeErro = sprintf(
            $templateMensagemDeErro,
            $porcentagemDeGarantiaDoContrato,
            money_format('%n', $valorAtualDoContrato),
            money_format('%n', $valorDeTodasAsGarantiasJaCadastradas),
            money_format('%n', $valorTotalAceito)
        );
        return $valorTotaldasGarantias <= $tetoDoValorPermitidoParaAdicionarNovaGarantiaAoContrato ? true : $mensagemDeErro;
    }
    
    public function afterSave($options = array())
    {
        if(!isset($this->data['Garantia']['co_garantia']) || $this->data['Garantia']['co_garantia'] == null || $this->data['Garantia']['co_garantia']){
            $nextId = $this->find('first', array('conditions' => array('co_garantia is not' => null),
                'order' => array('co_garantia' => 'DESC') ));

            $this->data['Garantia']['co_garantia'] = $nextId['Garantia']['co_garantia'] + 1;
        }
    }

    public function beforeSave($options = array())
    {
        if (empty($this->data['Garantia']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Garantia']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        $this->data['Garantia']['ds_modalidade'] = up($this->data['Garantia']['ds_modalidade']);
        if (isset($this->data['Garantia']['no_seguradora'])) {
            $this->data['Garantia']['no_seguradora'] = up($this->data['Garantia']['no_seguradora']);
        }
        if (isset($this->data['Garantia']['ds_endereco'])) {
            $this->data['Garantia']['ds_endereco'] = up($this->data['Garantia']['ds_endereco']);
        }
        if (isset($this->data['Garantia']['ds_observacao'])) {
            $this->data['Garantia']['ds_observacao'] = up($this->data['Garantia']['ds_observacao']);
        }
        if (isset($this->data['Garantia']['vl_garantia'])) {
            $this->data['Garantia']['vl_garantia'] = ln($this->data['Garantia']['vl_garantia']);
        }
        if (!isset($this->data['Garantia']['dt_fim']) || ($this->data['Garantia']['dt_fim'] == '')){
            $this->data['Garantia']['dt_fim'] = null;
        }
        if (!isset($this->data['Garantia']['dt_inicio']) || ($this->data['Garantia']['dt_inicio'] == '')){
            $this->data['Garantia']['dt_inicio'] = null;
        }

        return parent::beforeSave($options);
    }

    public function getModalidades()
    {
        if (null === $this->modalidades) {
            App::import('Model', 'GarantiaModalidade');
            $modalidadeDeGarantiaModel = new GarantiaModalidade();
            $this->modalidades = $modalidadeDeGarantiaModel->listar();
        }
        return $this->modalidades;
    }
}