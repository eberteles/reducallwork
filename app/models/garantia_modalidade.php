<?php

class GarantiaModalidade extends AppModel
{

    /**
     *
     * @var string
     */
    public $useTable = 'modalidade_garantia';

    /**
     *
     * @var string
     */
    public $primaryKey = 'co_modalidade_garantia';

    /**
     *
     * @var array
     */
    public $validate = array(
        'ds_modalidade_garantia' => array(
            'notempty' => array('rule' => array('notempty'), 'message' => 'Campo descrição em branco'),
            'unique' => array('rule' => 'isUnique', 'required' => 'create', 'message' => 'Descrição já cadastrada'),
        )
    );

    public function listar()
    {
        return $this->find('all');
    }

}
