<?php

class GarantiaSuporte extends AppModel
{

    var $name = 'GarantiaSuporte';

    var $useTable = 'garantias_suporte';

    var $primaryKey = 'co_garantia_suporte';

    var $validate = array(
        'co_garantia_suporte' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'nu_serie' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nº de Série em branco'
            )
        ),
        'dt_inicio' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Início em branco',
                'allowEmpty' => true
            )
        ),
        'dt_fim' => array(
            'rule' => array(
                'validaDtFim',
                true
            ),
            'message' => 'A Data Fim deve ser maior que a Data Início.',
            'allowEmpty' => true
        ),
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Observação em branco',
                'allowEmpty' => true
            )
        )
    );

    function validaDtFim()
    {
        App::import('Helper', 'Print');
        $print = new PrintHelper();
        $diferenca = $print->difEmDias($this->data['GarantiaSuporte']['dt_inicio'], $this->data['GarantiaSuporte']['dt_fim'], "US");
        if ($diferenca <= 0) {
            return false;
        }
        return true;
    }

    public function afterSave($options = array())
    {
        if (empty($this->data['GarantiaSuporte']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['GarantiaSuporte']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        $this->data['GarantiaSuporte']['nu_serie'] = up($this->data['GarantiaSuporte']['nu_serie']);

        if (isset($this->data['GarantiaSuporte']['ds_observacao'])) {
            $this->data['GarantiaSuporte']['ds_observacao'] = up($this->data['GarantiaSuporte']['ds_observacao']);
        }
        if (isset($this->data['GarantiaSuporte']['ds_garantia_suporte'])) {
            $this->data['GarantiaSuporte']['ds_garantia_suporte'] = up($this->data['GarantiaSuporte']['ds_garantia_suporte']);
        }
        
        if (empty($this->data['GarantiaSuporte']['dt_inicio'])) {
            $this->data['GarantiaSuporte']['dt_inicio'] = null;
        }
        
        if (empty($this->data['GarantiaSuporte']['dt_fim'])) {
            $this->data['GarantiaSuporte']['dt_fim'] = null;
        }
        
        return parent::beforeSave($options);
    }
}
?>