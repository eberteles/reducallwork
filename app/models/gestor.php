<?php

class Gestor extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
	

    var $name = 'Gestor';

    var $useTable = 'gestores';

    var $primaryKey = 'id';

    var $displayField = 'nome';

    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em branco.'
            )            
        ),
        'cpf' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo CPF em branco'
            )            
        ),
        'rg' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo RG em branco'
            )
        )
    );

    function beforeSave($options = array())
    {
        $this->data['Gestor']['nome'] = up($this->data['Gestor']['nome']);
        
        $this->data['Gestor']['cpf'] = str_replace(array('-','.','/'), '', $this->data['Gestor']['cpf']);
        
        return parent::beforeSave($options);
    }
}
?>