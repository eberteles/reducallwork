<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 15:01
 */

class Graduacao extends AppModel
{
    var $name       = "Graduacao";
    var $useTable   = "graduacao";
    var $primaryKey = "co_graduacao";

    var $validate = array(
        'ds_graduacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Posto / Gradução em branco'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Este Posto / Graduação já está cadastrado'
            )
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }
}