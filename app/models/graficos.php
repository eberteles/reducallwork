<?php

class Graficos extends AppModel
{

    var $name = 'Graficos';

    var $useTable = 'graficos';

    var $primaryKey = 'co_graficos';

    var $displayField = 'no_graficos';
}
