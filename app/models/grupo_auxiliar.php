<?php

class GrupoAuxiliar extends AppModel
{

	/**
	 * array(
	 *     'INSERT',
	 *     'UPDATE',
	 *     'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array(
			'INSERT',
			'UPDATE',
			'DELETE'
	);

    public $name = 'GrupoAuxiliar';

    public $useTable = 'usuarios';

    public $primaryKey = 'co_usuario';

    public $displayField  = 'nome_combo';

    public $virtualFields = array(
            'nome_combo' => "CONCAT(GrupoAuxiliar.nu_cpf, ' - ', GrupoAuxiliar.ds_nome)"
        );

    public $validate = array(
        /*'co_perfil' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo Perfil é requerido.'
            )
        ),
        'nu_cpf' => array(
            'isValid' => array(
                'rule' => array('validaCpf',true),
                'message' => 'Campo CPF em branco'
            ),
            'isUnique' => array(
                'rule' => array('isUniqueCpf',true),
                'message' => 'Este CPF já está em uso.'
            )
        ),*/

        'ds_nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo Nome em branco'
            )
        ),

        'ds_email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'Campo E-mail inválido!',
                'allowEmpty' => true
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'E-mail já cadastrado.',
                'allowEmpty' => true
            )
        ),
        /*
        'co_perfil' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo Perfil em branco'
            )
        ),*/
    ); // 'allowEmpty' => false,

    public $belongsTo = array(
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        )
    );

    public $hasOne = array(
        'UsuarioPerfil' => array(
            'className' => 'UsuarioPerfil',
            'foreignKey' => 'co_usuario',
            'dependent' => true
        )
    );


    function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (!$modulo->isCamposGrupoAuxiliar('nu_cpf')) {
            unset($this->validate['nu_cpf']);
            $this->validate['nu_cpf']['isUnique'] = array(
                'rule' => 'isUnique',
                'message' => 'Matrícula já cadastrada.'
            );
        }
        if ($modulo->isCampoObrigatorioGrupoAuxiliar('co_setor')) {
            $this->validate['co_setor']['numeric'] = array(
                'rule' => 'numeric',
                'message' => 'Campo ' . __('Unidade Administrativa', true) . ' em branco'
            );
        }
        if ($modulo->isCampoObrigatorioGrupoAuxiliar('co_cargo')) {
            $this->validate['co_cargo']['numeric'] = array(
                'rule' => 'numeric',
                'message' => 'Campo Cargo em branco'
            );
        }
        if ($modulo->isCampoObrigatorioGrupoAuxiliar('co_funcao')) {
            $this->validate['co_funcao']['numeric'] = array(
                'rule' => 'numeric',
                'message' => 'Campo Função em branco'
            );
        }

        parent::__construct();
    }

    public function validaCpf()
    {
        if (FunctionsComponent::validaCPF($this->data['GrupoAuxiliar']['nu_cpf'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns false if any fields passed match any (by default, all if $or = false) of their matching values.
     *
     * @param array $fields Field/value pairs to search (if no values specified, they are pulled from $this->data)
     * @param boolean $or If false, all fields specified must match in order for a false return value
     * @return boolean False if any records matching any fields are found
     * @access public
     */
    function isUniqueCpf($fields, $or = true) {
        if (!is_array($fields)) {
            $fields = func_get_args();
            if (is_bool($fields[count($fields) - 1])) {
                $or = $fields[count($fields) - 1];
                unset($fields[count($fields) - 1]);
            }
        }

        foreach ($fields as $field => $value) {
            if($value == '00000000000'){
                return true;
            }
            if (is_numeric($field)) {
                unset($fields[$field]);

                $field = $value;
                if (isset($this->data[$this->alias][$field])) {
                    $value = $this->data[$this->alias][$field];
                } else {
                    $value = null;
                }
            }

            if (strpos($field, '.') === false) {
                unset($fields[$field]);
                $fields[$this->alias . '.' . $field] = $value;
            }
        }
        if ($or) {
            $fields = array('or' => $fields);
        }
        if (!empty($this->id)) {
            $fields[$this->alias . '.' . $this->primaryKey . ' !='] =  $this->id;
        }
        return ($this->find('count', array('conditions' => $fields, 'recursive' => -1)) == 0);
    }

    public function beforeFind($queryData)
    {
        // $queryData['conditions']['GrupoAuxiliar.ic_acesso'] = 0;
//        $queryData['conditions']['GrupoAuxiliar.ic_ativo'] = 1;
        return $queryData;
    }

    public function afterSave($options = array())
    {
        if(!isset($this->data['GrupoAuxiliar']['co_usuario']) || $this->data['GrupoAuxiliar']['co_usuario'] == null ||
        $this->data['GrupoAuxiliar']['co_usuario']){

            App::import('Model', 'Usuario');
            $modelUsuario = new Usuario();

            $nextId = $modelUsuario->find('first', array('conditions' => array('Usuario.co_usuario is not' => null),
                'order' => array('Usuario.co_usuario' => 'DESC') ));

            $this->data['GrupoAuxiliar']['co_usuario'] = $nextId['Usuario']['co_usuario'] + 1;
        }

        if (empty($this->data['GrupoAuxiliar']['co_usuario_cad'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['GrupoAuxiliar']['co_usuario_cad'] = $usuario['Usuario']['co_usuario'];
        }

        if (isset($this->data['GrupoAuxiliar']['ds_nome'])) {
            $this->data['GrupoAuxiliar']['ds_nome'] = up($this->data['GrupoAuxiliar']['ds_nome']);
        }
        return parent::beforeSave($options);
    }
}
?>
