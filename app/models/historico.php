<?php

class Historico extends AppModel
{
	/**
	 * array(
	 *     'INSERT',
	 *     'UPDATE',
	 *     'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array(
			'INSERT',
			'UPDATE',
			'DELETE'
	);

    var $name = 'Historico';

    var $useTable = 'historicos';

    var $primaryKey = 'co_historico';

    var $validate = array(
        'co_historico' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations

        ,

        // 'co_contrato' => array(
        // 'numeric' => array(
        // 'rule' => array(
        // 'numeric'
        // ),
        // 'message' => 'Campo inválido'
        // )//'allowEmpty' => false,
        // //'required' => false,
        // //'last' => false, // Stop validation after this rule
        // //'on' => 'create', // Limit validation to 'create' or 'update' operations
        //
        // ),
        'no_assunto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Assunto em branco'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations

        ,

        // 'ds_historico' => array(
        // 'notempty' => array(
        // 'rule' => array(
        // 'notempty'
        // ),
        // 'message' => 'Campo inválido'
        // )//'allowEmpty' => false,
        // //'required' => false,
        // //'last' => false, // Stop validation after this rule
        // //'on' => 'create', // Limit validation to 'create' or 'update' operations
        //
        // ),
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        ),
        'dt_historico' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data em branco'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations


    );

    function beforeValidate($options = array())
    {
        $this->data['Historico']['no_assunto'] = up($this->data['Historico']['no_assunto']);
        // $this->data[ 'Historico' ][ 'ds_historico' ] = up( $this->data[ 'Historico' ][ 'ds_historico' ] );
        $this->data['Historico']['ds_observacao'] = up($this->data['Historico']['ds_observacao']);

        return parent::beforeValidate($options);
    }

		public function beforeSave($options = array())
		{
				//$this->data['Historico']['dt_historico'] = $this->data['Historico']['dt_historico'] . ' ' . date('H:i:s');
			  //var_dump($this->data['Historico']);die;

				return true;
		}

    public function afterSave($options = array())
    {
        if(!isset($this->data['co_historico']) || $this->data['co_historico'] == null || $this->data['co_historico']){
            $nextId = $this->find('first', array('conditions' => array('co_historico is not' => null),
                'order' => array('co_historico' => 'DESC') ));

            $this->data['Historico']['co_historico'] = $nextId['Historico']['co_historico'] + 1;
        }

        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Historico']['co_usuario'] = $usuario['Usuario']['co_usuario'];

        return parent::beforeSave($options);
    }
}
?>
