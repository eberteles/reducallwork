<?php

class HmabPesquisa extends AppModel
{
    var $name = "HmabPesquisa";
    var $useTable = "locais_exames";
    var $primaryKey = "id";
    var $order = "HmabPesquisa.id DESC";
    public $recursive = 2;

    var $belongsTo = array(
        'ExameConsulta' => array(
            'className' => 'ExameConsulta',
            'foreignKey' => 'co_exames'
        ),
        'LocalAtendimento' => array(
            'className' => 'LocalAtendimento',
            'foreignKey' => 'co_locais_atendimento'
        )
    );

//    var $belongsTo = array(
//        'Bairro' => array(
//            'className' => 'Bairro',
//            'foreignKey' => 'co_bairro'
//        ),
//        'Fornecedor' => array(
//            'className' => 'Fornecedor',
//            'foreignKey' => 'co_fornecedor'
//        )
//    );
//
//    var $hasAndBelongsToMany = array(
//        'ExameConsulta' => array(
//            'className' => 'ExameConsulta',
//            'joinTable' => 'locais_exames',
//            'foreignKey' => 'co_locais_atendimento',
//            'associationForeignKey' => 'co_exames',
//            'unique' => false
//        )
//    );
}