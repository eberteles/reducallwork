<?php

class Importacao extends AppModel
{

    var $name = 'Importacao';

    var $useTable = false;
    
    public $validate = array(
        'conteudo' => array(
            'anexo' => array(
                'rule' => array('extension', array('csv')),
                'message' => 'Favor selecionar um arquivo .CSV',
                'allowEmpty' => true
            )
        )
    );
    
//    public function beforeValidate($options)
//    {
//        if ($this->data['Importacao']['conteudo']) {
//            $this->prepararArquivo($this->data['Importacao']['conteudo']);
//        } else {
//            unset($this->data['Importacao']['conteudo']);
//        }
//    }
//    
//    private function prepararArquivo(&$campo)
//    {
//        if ($campo) {
//            
//            $file = new File($campo['tmp_name']);
//            
//            $campo = $file->read();
//            
//            $file->close();
//        }
//    }
}
?>