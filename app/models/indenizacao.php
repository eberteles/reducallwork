<?php

class Indenizacao extends AppModel
{

    var $name = 'Indenizacao';

    var $useTable = 'indenizacoes';

    var $primaryKey = 'co_indenizacao';

    var $validate = array(
        'dt_indenizacao' => array(
            'date' => array(
                'rule' => array(
                    'date'
                ),
                'message' => 'Campo Data de Indenização inválido.'
            )
        ),

        'qt_viagem' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo N° de Viagens em branco'
            )
        ),

        'nu_distancia' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo KM da Viagem em branco'
            )
        ),
        
        'ds_carro' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Carro em branco'
            )
        ),
        
        'ds_placa' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Placa em branco'
            )
        ),

        'nu_ano_carro' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Ano do automóvel em branco'
            )
        ),

        'ds_servico' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Obra/Serviço em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Indenizacao']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        
        $this->data['Indenizacao']['nu_ano_indenizacao']    = date("Y", strtotime($this->data['Indenizacao']['dt_indenizacao']) );
        $this->data['Indenizacao']['ds_carro']              = up($this->data['Indenizacao']['ds_carro']);
        $this->data['Indenizacao']['ds_placa']              = up($this->data['Indenizacao']['ds_placa']);
        $this->data['Indenizacao']['ds_servico']            = up($this->data['Indenizacao']['ds_servico']);
        
        if (isset($this->data['Indenizacao']['nu_distancia'])) {
            $this->data['Indenizacao']['nu_distancia']      = ln($this->data['Indenizacao']['nu_distancia']);
        }
        
        return parent::beforeSave($options);
    }
}
?>