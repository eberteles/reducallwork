<?php
/**
 * Created by PhpStorm.
 * User: João Marcos Bizarro Lopes
 * Date: 10/11/2015
 * Time: 15:12
 */

class Inspecao extends AppModel
{
    var $name       = "Inspecao";
    var $useTable   = "inspecao";
    var $primaryKey = "co_inspecao";

    var $validate = array(
        'ds_inspecao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição da Inspeção em branco'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Esta inspeção já está cadastrada'
            )
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }
}