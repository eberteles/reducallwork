<?php

class Instituicao extends AppModel
{

    var $name = 'Instituicao';

    var $useTable = 'instituicoes';

    var $primaryKey = 'co_instituicao';

    var $displayField = 'ds_instituicao';

    var $validate = array(
        
        'nu_instituicao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo UASG em branco'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ,
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'UASG já cadastrada'
            )
        ),
        'ds_modalidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ,
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );

    var $belongsTo = array(
        'InstituicaoPai' => array(
            'className' => 'Instituicao',
            'foreignKey' => 'co_instituicao_pai'
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Instituicao']['ds_instituicao'] = up($this->data['Instituicao']['ds_instituicao']);
        
        return parent::beforeValidate($options);
    }
}
?>