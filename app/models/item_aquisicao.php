<?php

class ItemAquisicao extends AppModel
{

    var $name = 'ItemAquisicao';

    var $useTable = 'itens_aquisicoes';

    var $primaryKey = 'co_item_aquisicao';

    var $displayField = 'ds_item_aquisicao';

    var $validate = array(
        
        'ds_item_aquisicao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data['ItemAquisicao']['ds_item_aquisicao'])) {
            $this->data['ItemAquisicao']['ds_item_aquisicao'] = up($this->data['ItemAquisicao']['ds_item_aquisicao']);
        }
        
        return parent::beforeSave($options);
    }
}
?>