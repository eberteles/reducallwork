<?php

class LctFases extends AppModel
{

    var $name = 'LctFases';

    var $useTable = 'lct_fases';

    var $primaryKey = 'id';

    var $displayField = 'descricao';

    var $validate = array(
        'descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'data' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {

        $this->data['LctFases'] = array_map('strtoupper', $this->data['LctFases']);

        return parent::beforeSave($options);
    }
}
?>