<?php

class LctLicitacoes extends AppModel
{

    var $name = 'LctLicitacoes';

    var $useTable = 'lct_licitacoes';

    var $primaryKey = 'id';

    var $displayField = 'objeto';

    var $validate = array(
        'objeto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'modalidade_processo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        ),
        'nu_licitacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo em branco'
            )
        )
    );

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $this->data['LctLicitacoes']['valor_estimado']   = ln($this->data['LctLicitacoes']['valor_estimado']);
        $this->data['LctLicitacoes']['valor_contratado'] = ln($this->data['LctLicitacoes']['valor_contratado']);

        return parent::beforeSave($options);
    }
}
?>