<?php

class LctLotes extends AppModel
{

    var $name = 'LctLotes';

    var $useTable = 'lct_lotes';

    var $primaryKey = 'id';

    var $displayField = 'objeto';

    var $validate = array();

    function beforeValidate($options = array())
    {
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {

        $this->data['LctLotes']['valor_estimado'] = str_replace(',','.',str_replace('.','',$this->data['LctLotes']['valor_estimado']));
        $this->data['LctLotes']['valor_contratado'] = str_replace(',','.',str_replace('.','',$this->data['LctLotes']['valor_contratado']));
        $this->data['LctLotes'] = array_map('strtoupper', $this->data['LctLotes']);

        return parent::beforeSave($options);
    }
}
?>