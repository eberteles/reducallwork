<?php

class LctRelatorio extends AppModel
{

    var $name = 'LctRelatorio';

    var $useTable = 'lct_licitacoes';

    var $primaryKey = 'id';

    var $order = 'LctRelatorio.id DESC';
}
?>