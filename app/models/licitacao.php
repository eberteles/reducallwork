<?php

class Licitacao extends AppModel
{

    public $name = 'Licitacao';

    public $useTable = 'licitacoes';

    public $primaryKey = 'co_licitacao';

    var $displayField = 'no_tipo_licitacao';

    public $validate = array(
        'nu_licitacao' => array(
            'notempty' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo Nº Licitação em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'allowEmpty' => true,
                'message' => 'Nº Licitação já cadastrado.'
            )
        ),
        'dt_publicacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Publicação em branco'
            )
        ),
        'dt_abertura' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Abertura em branco'
            )
        ),
        'co_contratacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Abertura em branco'
            )
        ),
        'co_tipo_licitacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Abertura em branco'
            )
        ),
        'co_situacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Abertura em branco'
            )
        ),
        'co_fornecedor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Empresa vencedora em branco'
            )
        )

    );


    public $belongsTo = array(
        'Contratacao' => array(
            'className' => 'Contratacao',
            'foreignKey' => 'co_contratacao'
        ),  
        'Situacao' => array(
            'className' => 'Situacao',
            'foreignKey' => 'co_situacao'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        ),
        'TipoLicitacao' => array(
            'className' => 'TipoLicitacao',
            'foreignKey' => 'co_tipo_licitacao'
        )   
    );

    public $hasMany = array(
        'Anexo' => array(
            'className' => 'Anexo',
            'foreignKey' => 'co_licitacao',
            // 'cascadeCallbacks' => true,
            'dependent' => true
        )
    );

    public function beforeValidate($options = array()) {

        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array()) {
        // if (!empty($this->data['Anexo']['co_'])) {
        //     $file = ROOT . DS . APP_DIR . DS . 'data' . DS . $anexo['Anexo']['caminho'];
        //     @unlink($file);
        // }
        return parent::beforeSave($options);
    }

    public function afterSave() {

    }

    public function getRelatorioContrato($palavaChave=null, $coContratacao=null, $coSituacao=null) {
        $query = 'SELECT Contrato.nu_contrato, Fornecedor.tp_fornecedor,
            Contrato.nu_processo, DATE_FORMAT(Contrato.dt_publicacao, \'%m/%d/%Y\') as dt_publicacao, 
            Contrato.ds_objeto, DATE_FORMAT(Contrato.dt_ini_vigencia, \'%m/%d/%Y\') as dt_ini_vigencia,
            DATE_FORMAT(Contrato.dt_fim_vigencia, \'%d/%m/%Y\') as dt_fim_vigencia, Contrato.vl_global,
            Contrato.ds_fundamento_legal, Situacao.ds_situacao,
            Contratacao.ds_contratacao,
            Fornecedor.no_razao_social, Fornecedor.nu_cnpj,
            Aditivo.no_aditivo, DATE_FORMAT(Aditivo.dt_aditivo, \'%m/%d/%Y\') as dt_aditivo,
            NotaFiscal.nu_nota
            FROM contratos Contrato
            LEFT JOIN contratacoes Contratacao ON(Contrato.co_contratacao = Contratacao.co_contratacao) 
            LEFT JOIN situacoes Situacao ON(Situacao.co_situacao = Contrato.co_situacao)
            LEFT JOIN fornecedores Fornecedor ON(Fornecedor.co_fornecedor = Contrato.co_fornecedor)
            LEFT JOIN aditivos Aditivo ON(Aditivo.co_contrato = Contrato.co_contrato)
            LEFT JOIN empenhos Empenho ON(Empenho.co_contrato = Contrato.co_contrato)
            LEFT JOIN notas_fiscais NotaFiscal ON(NotaFiscal.co_contrato = Contrato.co_contrato)
            LEFT JOIN notas_empenhos NotaEmpenho ON(NotaEmpenho.co_nota = NotaFiscal.co_nota AND NotaEmpenho.co_empenho = Empenho.co_empenho)';
        if($palavaChave){
            $query .= 'WHERE ';
        }else{
            $query .= ' WHERE Contrato.ic_ativo = 1 AND Contrato.nu_contrato IS NOT NULL ';
        }

        if($palavaChave){
            $query .= "   Contrato.nu_contrato LIKE '%{$palavaChave}%'
                            OR Fornecedor.tp_fornecedor LIKE '%{$palavaChave}%'
                             OR  Contrato.nu_processo LIKE '%{$palavaChave}%'
                            OR Contrato.ds_objeto LIKE '%{$palavaChave}%'
                            OR  Aditivo.no_aditivo LIKE '%{$palavaChave}%'
                             OR  NotaFiscal.nu_nota LIKE '%{$palavaChave}%' ";
        }
        if ($coSituacao) {
            $query .= " AND Situacao.co_situacao = {$coSituacao}";
        }

        if ($coContratacao) {
            $query .= " AND Contratacao.co_contratacao = {$coContratacao}";
        }

        $query .= ' AND Contrato.ic_ativo = 1 AND Contrato.nu_contrato IS NOT NULL ';
        // $query .= ' group by';

        return $this->query($query);
    }       

//    public function beforeValidate() {
//
//    }
//
//    public function beforeSave( ) {
//
//    }
//
//    public function afterSave() {
//
//    }


}
?>
