<?php

class Liquidacao extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $tipoDAR = array(
        'O' => 'Original',
        'C' => 'Cancelamento'
    );

    var $name = 'Liquidacao';

    var $useTable = 'liquidacao';

    var $primaryKey = 'co_liquidacao';

    var $belongsTo = array(
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'foreignKey' => 'co_nota'
        )
    );

    var $validate = array(
        'nu_liquidacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número da Liquidação em branco'
            )
        ),
        'dt_liquidacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número da Liquidação em branco'
            )
        ),
    );

    public function beforeSave($options = array())
    {
        if (empty($this->data['Liquidacao']['dt_envio_sefin']))
            $this->data['Liquidacao']['dt_envio_sefin'] = null;

        if (empty($this->data['Liquidacao']['dt_recebimento_sefin']))
            $this->data['Liquidacao']['dt_recebimento_sefin'] = null;

        if (empty($this->data['Liquidacao']['dt_liquidacao']))
            $this->data['Liquidacao']['dt_liquidacao'] = null;

        return parent::beforeSave($options);

    }
}