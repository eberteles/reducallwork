<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/11/2015
 * Time: 15:10
 */

class LocaisEspecialidades extends AppModel
{
    var $name       = "LocaisEspecialidades";
    var $useTable   = "locais_especialidades";
    var $primaryKey = "id";
    var $recursive = 1;

    var $validate = array(
    );

    var $belongsTo = array(
        'Especialidade' => array(
            'className' => 'Especialidade',
            'foreignKey' => 'co_especialidade'
        ),
        'LocalAtendimento' => array(
            'className' => 'LocalAtendimento',
            'foreignKey' => 'co_locais_atendimento'
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }
}