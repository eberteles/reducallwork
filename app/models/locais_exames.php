<?php

class LocaisExames extends AppModel
{
    var $name       = "LocaisExames";
    var $useTable   = "locais_exames";
    var $primaryKey = "id";
    var $recursive = 1;

    var $validate = array(
    );

    var $belongsTo = array(
        'ExameConsulta' => array(
            'className' => 'ExameConsulta',
            'foreignKey' => 'co_exames'
        ),
        'LocalAtendimento' => array(
            'className' => 'LocalAtendimento',
            'foreignKey' => 'co_locais_atendimento'
        )
    );

    public function beforeSave($options = array())
    {
        return parent::beforeSave($options);
    }
}
