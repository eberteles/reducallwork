<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 10/11/2015
 * Time: 15:12
 */

class LocalAtendimento extends AppModel
{
    var $name       = "LocalAtendimento";
    var $useTable   = "locais_atendimento";
    var $primaryKey = "co_locais_atendimento";

    var $validate = array(
        'uf' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo UF em branco'
            )
        ),
        'municipio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Cidade em branco'
            )
        ),
        'telefone1' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone 1 em branco'
            )
        ),
        'co_bairro' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Bairro em branco'
            )
        )
    );

    var $belongsTo = array(
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        ),
        'Bairro' => array(
            'className' => 'Bairro',
            'foreignKey' => 'co_bairro'
        )
    );

    public function beforeSave($options = array())
    {
        if(isset($this->data["LocalAtendimento"]["telefone1"]))
            $this->data["LocalAtendimento"]["telefone1"] = str_replace("(","",str_replace(")","", str_replace("-","", str_replace(" ", "", $this->data["LocalAtendimento"]["telefone1"]))));

        if(isset($this->data["LocalAtendimento"]["telefone2"]))
            $this->data["LocalAtendimento"]["telefone2"] = str_replace("(","",str_replace(")","", str_replace("-","", str_replace(" ", "", $this->data["LocalAtendimento"]["telefone2"]))));

        return parent::beforeSave($options);
    }
}