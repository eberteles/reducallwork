<?php

class Log extends AppModel
{		
    var $name = 'Log';

    var $useTable = 'logs';

    var $primaryKey = 'co_log';

    var $tpAcao = array(
        'L' => 'Login',
        'C' => 'Consulta',
        'A' => 'Alteração',
        'E' => 'Exclusão',
        'I' => 'Inclusão'
    );

    var $tpModulo = array(
        'CTR' => 'Contrato',
        'CGE' => 'Contrato - Gestores',
        'CAD' => 'Contrato - Aditivos',
        'CPG' => 'Contrato - Pagamentos',
        'CPD' => 'Contrato - Pendências',
        'COB' => 'Contrato - Observação',
        'CAN' => 'Contrato - Anexos',
        'USL' => 'Cadastro - Unidades Solicitantes',
        'TPC' => 'Cadastro - Tipo de Contrato',
        'STS' => 'Cadastro - Status',
        'DCS' => 'Cadastro - Descrição do Serviço',
        'PVL' => 'Cadastro - Privilégios',
        'GTR' => 'Cadastro - Gestores',
        'FCD' => 'Fornecedores',
        'USU' => 'Usuários',
        'REL' => 'Relatórios'
    );

    var $validate = array(
        'co_log' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        )
        
    );

    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario'
        )
    );

    function beforeValidate($options = array())
    {
        if (isset($this->data['Log']['ds_registro'])) {
            $this->data['Log']['ds_registro'] = up($this->data['Log']['ds_registro']);
        }
        
        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        App::import('Component', 'SessionComponent');
        return parent::beforeSave($options);
    }    
}
?>