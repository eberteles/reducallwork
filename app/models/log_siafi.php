<?php

class LogSiafi extends AppModel
{
    var $name = 'LogSiafi';

    var $useTable = 'logs_siafi';

    var $primaryKey = 'co_log_siafi';

    var $tiposEntidade = array(
        1 => 'Pagamento',
        2 => 'Liquidação',
        3 => 'Notas Fiscais',
        4 => 'Empenho'
    );

    var $belongsTo = array();


    public function getTiposEntidade()
    {
        return $this->tiposEntidade;
    }
}
