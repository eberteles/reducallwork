<?php

class Modalidade extends AppModel
{
	/**
	 * array(
	 *     'INSERT',
	 *     'UPDATE',
	 *     'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array(
			'INSERT',
			'UPDATE',
			'DELETE'
	);
	
    var $name = 'Modalidade';

    var $useTable = 'modalidades';

    var $primaryKey = 'co_modalidade';

    var $displayField = 'ds_modalidade';
    
    var $tiposModalidade = array(
        1 => 'Olímpico',
        2 => 'Paralímpico',
        3 => 'Pan-americano',
        4 => 'Não Olímpico'
    );
    
    var $tiposProva = array(
        1 => 'Individual',
        2 => 'Coletivo'
    );

    var $validate = array(
        
        'ds_modalidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ,
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
    );
    
    function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        if ($modulo->isCamposModalidade('nu_modalidade') == true) {
            $this->validate['nu_modalidade']['notempty'] = array(
                'rule' => 'notempty',
                'message' => 'Campo Número em Branco.'
            );
            $this->validate['nu_modalidade']['isUnique'] = array(
                'rule' => 'isUnique',
                'message' => 'Número já cadastrado.'
            );
        }
        if ($modulo->isCamposModalidade('tp_modalidade') == true) {
            $this->validate['tp_modalidade']['notempty'] = array(
                'rule' => 'notempty',
                'message' => 'Campo Tipo de Modalidade em Branco.'
            );
        }
        if ($modulo->isCamposModalidade('tp_prova') == true) {
            $this->validate['tp_prova']['notempty'] = array(
                'rule' => 'notempty',
                'message' => 'Campo Tipo de Prova em Branco.'
            );
        }
        parent::__construct();
    }

    function beforeValidate($options = array())
    {
        $this->data['Modalidade']['ds_modalidade'] = up($this->data['Modalidade']['ds_modalidade']);
        
        return parent::beforeValidate($options);
    }

    public function getByDescricao($descricao)
    {
        return $this->query(" SELECT * FROM modalidades WHERE ds_modalidade = '".($descricao)."'");
    }

    public function afterSave($options = array())
    {
        if(!isset($this->data['Modalidade']['co_modalidade']) || $this->data['Modalidade']['co_modalidade'] == null || $this->data['Modalidade']['co_modalidade'] == ''){
            $nextId = $this->find('first', array('conditions' => array('co_modalidade is not' => null),
                'order' => array('co_modalidade' => 'DESC') ));

            $this->data['Modalidade']['co_modalidade'] = $nextId['Modalidade']['co_modalidade'] + 1;
        }

        return parent::beforeSave($options);
    }

    public function beforeFind($queryData)
    {
        if (isset($queryData['conditions'][0]) && ($queryData['conditions'][0] == 'all' || $queryData['conditions'][0] == 'first')) {
            unset($queryData['conditions'][0]);
        } else {
            $queryData['conditions']['ic_ativo'] = 2;
        }
        return $queryData;
    }
}
?>