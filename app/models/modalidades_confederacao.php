<?php

class ModalidadesConfederacao extends AppModel
{

    var $name = 'ModalidadesConfederacao';

    var $useTable = 'modalidades_confederacoes';

    var $primaryKey = 'co_modalidade_confederacao';

    var $belongsTo = array(
        'Confederacao' => array(
            'className' => 'Confederacao',
            'foreignKey' => 'co_confederacao'
        )
    );
}
?>