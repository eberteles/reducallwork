<?php

/**
 * Identifica o processamento dos arquivos enviados pelo módulo SIAFI.
 */
class Movimento extends AppModel
{

    var $name = 'Movimento';

    var $useTable = 'movimentos';

    var $primaryKey = 'co_movimento';
}