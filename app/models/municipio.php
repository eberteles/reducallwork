<?php

class Municipio extends AppModel
{

    var $name = 'Municipio';

    var $useTable = 'municipios';

    var $primaryKey = 'co_municipio';

    var $displayField = 'ds_municipio';

    var $validate = array(
        
        'sg_uf' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'ds_municipio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        
    );
    
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasOne = array(
        'Uf' => array(
            'className' => 'Uf',
            'foreignKey' => 'sg_uf'
        )
    );
}
?>