<?php

class NotaCategoria extends AppModel
{

    var $name = 'NotaCategoria';

    var $useTable = 'notas_categorias';

    var $primaryKey = 'co_nota_categoria';

    var $displayField = 'ds_nota_categoria';
    
    var $order = "ds_nota_categoria ASC";

    var $validate = array(
        'ds_nota_categoria' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Campo Descrição já cadastrado.'
            )
        )
    );
    
    function beforeSave($options = array())
    {
        $this->data['NotaCategoria']['ds_nota_categoria'] = up($this->data['NotaCategoria']['ds_nota_categoria']);
        return parent::beforeValidate($options);
    }
}