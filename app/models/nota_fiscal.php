<?php

class NotaFiscal extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );


    public $tipoDAR = array(
        'O' => 'Original',
        'C' => 'Cancelamento'
    );

    var $name = 'NotaFiscal';

    var $useTable = 'notas_fiscais';

    var $primaryKey = 'co_nota';

    var $displayField = 'nu_nota';

    var $validate = array(
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'A Nota Fiscal deve estar vinculada a um Contrato'
            )
        ),

        'nu_nota' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número da Nota em branco'
            )
        )
    ,
//        'dt_recebimento' => array(
//            'notempty' => array(
//                'rule' => array(
//                    'notempty'
//                ),
//                'message' => 'Data de recebimento não pode ficar em branco'
//            )
//        ),
        'dt_nota' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data da Nota em branco'
            )
        ),
        'vl_nota' => array(
            'notempty' => array(
                'rule' => array('comparison', '>', 0),
                'message' => 'Campo Valor da Nota em branco'
            ),
            'validaVlNova' => array(
                'rule' => 'validaVlNova',
                //'message' => 'mensagem setada no construtor'
            )
        )
    );

    var $belongsTo = array(
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'co_contrato'
        )
    );

    var $hasOne = array(
        'Anexo' => array(
            'className' => 'Anexo',
            'foreignKey' => 'co_nota'
        )
    );

    var $hasMany = array(
        'NotasEmpenho' => array(
            'className' => 'NotasEmpenho',
            'foreignKey' => 'co_nota',
            'dependent' => true
        ),
        'NotasPagamento' => array(
            'className' => 'NotasPagamento',
            'foreignKey' => 'co_nota',
            'dependent' => true
        )
    );

    var $hasAndBelongsToMany = array(
        'Empenho' => array(
            'className' => 'Empenho',
            'joinTable' => 'notas_empenhos',
            'with' => 'NotasEmpenho',
            'foreignKey' => 'co_nota',
            'associationForeignKey' => 'co_empenho',
        ),
        'Pagamento' => array(
            'className' => 'Pagamento',
            'joinTable' => 'notas_pagamentos',
            'with' => 'NotasPagamento',
            'foreignKey' => 'co_nota',
            'associationForeignKey' => 'co_pagamento',
        )
    );

    function __construct()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        if (Configure::read('App.config.resource.certificadoDigital')) {
            $this->belongsTo['Assinatura'] = array(
                'className' => 'Assinatura',
                'foreignKey' => 'assinatura_id'
            );
        }

        if ($modulo->isCamposNota('co_nota_categoria')) {
            $this->belongsTo['NotaCategoria'] = array(
                'className' => 'NotaCategoria',
                'foreignKey' => 'co_nota_categoria'
            );
        }

        $this->validate['vl_nota']['validaVlNova']['message'] = __('msg_validacao_valor_nota_fiscal', true);

        parent::__construct();
    }

    function validaVlNova()
    {
        if (isset($this->data['NotaFiscal']['co_empenhos']) && is_array($this->data['NotaFiscal']['co_empenhos'])) {
            $valor_restante = 0;
            /*
                        foreach ($this->data['NotaFiscal']['co_empenhos'] as $co_empenho) {
                            $empenho = $this->Empenho->find('first', array(
                                'conditions' => array(
                                    'Empenho.co_contrato' => $this->data['NotaFiscal']['co_contrato'],
                                    'Empenho.co_empenho' => $co_empenho,
                                ),
                                'fields' => "sum(Empenho.vl_empenho) -
                                    ifnull( (SELECT sum( n.vl_nota ) vl_restante
                                    FROM notas_empenhos ne
                                    JOIN empenhos em on ne.co_empenho = em.co_empenho
                                    JOIN notas_fiscais n on ne.co_nota = n.co_nota
                                    WHERE em.co_contrato = Empenho.co_contrato), 0) vl_restante"

                            ));
                            $valor_restante += $empenho[0]['vl_restante'];
                        }
            */
            $sql = "
SELECT 
    (SUM(vl_restante) - IFNULL((
    SELECT SUM(vl_nota)
        FROM (
           SELECT n.co_nota, n.vl_nota
             FROM notas_empenhos ne
             JOIN empenhos em ON ne.co_empenho = em.co_empenho
             JOIN notas_fiscais n ON ne.co_nota = n.co_nota
            WHERE em.co_contrato = {$this->data['NotaFiscal']['co_contrato']}
              AND em.co_empenho IN (" . implode(',', $this->data['NotaFiscal']['co_empenhos']) . ")
            GROUP BY n.co_nota
            ) x
        ), 0)
    ) vl_restante
FROM empenhos
WHERE co_contrato = {$this->data['NotaFiscal']['co_contrato']}
AND co_empenho IN (" . implode(',', $this->data['NotaFiscal']['co_empenhos']) . ")";

            $result = $this->query($sql);
            $valor_restante = $result[0][0]['vl_restante'];
            /*
             * Se tiver nota recupera o valor atualmente salvo da nota para acrescentar o valor restante
             * devido a edição
             */
            if ($this->data['NotaFiscal']['co_nota']) {
                $nota = $this->find('first', array(
                    'conditions' => array(
                        'NotaFiscal.co_nota' => $this->data['NotaFiscal']['co_nota']
                    ), 'fields' => 'vl_nota'));
                $valor_restante += $nota['NotaFiscal']['vl_nota'];
            }

            if ($this->data['NotaFiscal']['vl_nota'] > $valor_restante) {
                return false;
            }
        }
        return true;
    }

    function beforeValidate($options = array())
    {
//        $this->NotaFiscal->Empenho->find('list', array(
//            'conditions' => array(
//                'Empenho.co_empenho' => $idEmpenho
//            ),
//            'fields' => array(
//                'Empenho.co_empenho','Empenho.dt_empenho'
//            )

//        ));        
        if (isset($this->data['NotaFiscal']['co_empenhos']) && empty($this->data['NotaFiscal']['co_empenhos'])) {
            $this->invalidate('co_empenhos', 'Campo Empenhos em branco');
        }

        if (isset($this->data['NotaFiscal']['ds_nota'])) {
            $this->data['NotaFiscal']['ds_nota'] = up($this->data['NotaFiscal']['ds_nota']);
        }
        if (isset($this->data['NotaFiscal']['ds_atesto'])) {
            $this->data['NotaFiscal']['ds_atesto'] = up($this->data['NotaFiscal']['ds_atesto']);
        }

        if (empty($this->data['NotaFiscal']['dt_nota'])) {
            $this->data['NotaFiscal']['dt_nota'] = null;
        }

        if (empty($this->data['NotaFiscal']['dt_recebimento'])) {
            $this->data['NotaFiscal']['dt_recebimento'] = null;
        }
        if (empty($this->data['NotaFiscal']['dt_envio'])) {
            $this->data['NotaFiscal']['dt_envio'] = null;
        }
        if (empty($this->data['NotaFiscal']['dt_atesto'])) {
            $this->data['NotaFiscal']['dt_atesto'] = null;
        }

        if (isset($this->data['NotaFiscal']['vl_nota'])) {
            $this->data['NotaFiscal']['vl_nota'] = ln($this->data['NotaFiscal']['vl_nota']);
        }

        if (isset($this->data['NotaFiscal']['vl_glosa'])) {
            $this->data['NotaFiscal']['vl_glosa'] = ln($this->data['NotaFiscal']['vl_glosa']);
        }

        if ($this->data['NotaFiscal']['dt_recebimento'] == '') {
            $this->data['NotaFiscal']['dt_recebimento'] = null;
        }

        if ((isset($this->data['NotaFiscal']['dt_vencimento'])) && $this->data['NotaFiscal']['dt_vencimento'] == '') {
            $this->data['NotaFiscal']['dt_vencimento'] = null;
        }

        if ((isset($this->data['NotaFiscal']['dt_competencia'])) && $this->data['NotaFiscal']['dt_competencia'] == '') {
            $this->data['NotaFiscal']['dt_competencia'] = null;
        }

        return parent::beforeValidate($options);
    }

    public function beforeSave($options = array())
    {
        $notasEmpenhosas = $this->NotasEmpenho->find('all', array(
            'conditions' => array(
                'NotasEmpenho.co_nota' => $this->data['NotaFiscal']['co_nota']
            )
        ));

        if ($notasEmpenhosas && $this->data['NotaFiscal']['co_empenhos'] == '') {
            foreach ($notasEmpenhosas as $empenhosas) {
                $this->NotasEmpenho->delete($empenhosas['NotasEmpenho']['co_nota_empenho']);
            }
        }
        return parent::beforeSave($options);
    }

    public function afterSave($options = array())
    {
        if (!isset($this->data['NotaFiscal']['co_nota']) || $this->data['NotaFiscal']['co_nota'] == null || $this->data['NotaFiscal']['co_nota']) {
            $nextId = $this->find('first', array('conditions' => array('NotaFiscal.co_nota is not' => null),
                'order' => array('NotaFiscal.co_nota' => 'DESC')));

            $this->data['NotaFiscal']['co_nota'] = $nextId['NotaFiscal']['co_nota'] + 1;
        }

        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['NotaFiscal']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        return parent::afterSave($options);
    }

    // deletar os pagamentos vinculados a nota fiscal
    public function afterDelete()
    {
        App::import('Model', 'Pagamento');
        $pagamentoModel = new Pagamento();

        $notasPagamento = $this->NotasPagamento->find('all', array('fields' => 'NotasPagamento.co_pagamento'));

        foreach ($notasPagamento as $index => $notaPagamento) {
            $pagamentoModel->delete($notaPagamento['NotasPagamento']['co_pagamento']);
        }
    }
}
