<?php

class NotasEmpenho extends AppModel
{

    var $name = 'NotasEmpenho';

    var $useTable = 'notas_empenhos';

    var $primaryKey = 'co_nota_empenho';

    var $order = 'NotasEmpenho.dt_nota_empenho DESC';

    public function beforeSave($options = array())
    {
        if (empty($this->data['NotasEmpenho']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['NotasEmpenho']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        if (empty($this->data['NotasEmpenho']['dt_nota_empenho'])) {
            $this->data['NotasEmpenho']['dt_nota_empenho'] = date('Y-m-d');
        }
        
        return parent::beforeSave($options);
    }

    var $belongsTo = array(
        'Empenho' => array(
            'className' => 'Empenho',
            'foreignKey' => 'co_empenho'
        ),
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'foreignKey' => 'co_nota'
        )
    );
}
?>