<?php

class NotasPagamento extends AppModel
{

    var $name = 'NotasPagamento';

    var $useTable = 'notas_pagamentos';

    var $primaryKey = 'co_nota_pagamento';

    public function beforeSave($options = array())
    {
        if (empty($this->data['NotasPagamento']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['NotasPagamento']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        return parent::beforeSave($options);
    }

    var $belongsTo = array(
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'foreignKey' => 'co_nota'
        )
    );
}
?>