<?php

class Oficio extends AppModel
{

    var $name = 'Oficio';

    var $primaryKey = 'co_oficio';

    var $validate = array(
        'co_oficio' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'nu_oficio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Ofício em branco'
            )
        ),
        'dt_recebimento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Recebimento em branco'
            )
        ),
        'pz_resposta' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Prazo de Resposta em branco',
                'allowEmpty' => true
            )
        ),
        'dt_resposta' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Resposta em branco',
                'allowEmpty' => true
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (empty($this->data['Oficio']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['Oficio']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        $this->data['Oficio']['nu_oficio'] = up($this->data['Oficio']['nu_oficio']);
        
        if (empty($this->data['Oficio']['dt_recebimento'])) {
            $this->data['Oficio']['dt_recebimento'] = null;
        }
        
        if (empty($this->data['Oficio']['dt_resposta'])) {
            $this->data['Oficio']['dt_resposta'] = null;
        }
        
        return parent::beforeSave($options);
    }
}
?>