<?php

class Operadora extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
	

    var $name = 'Operadora';

    var $useTable = 'operadoras';

    var $primaryKey = 'id';

    var $displayField = 'nome';

    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo nome em branco.'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Operadora já cadastrada'
            )  
        )
    );

    function beforeSave($options = array())
    {
        $this->data['Operadora']['nome'] = up($this->data['Operadora']['nome']);
        
        return parent::beforeSave($options);
    }
    
}
?>