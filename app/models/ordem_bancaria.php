<?php

class OrdemBancaria extends AppModel
{

    var $name = 'OrdemBancaria';

    var $useTable = 'ordem_bancaria';

    var $primaryKey = 'co_ordem_bancaria';
}