<?php

class OrdemFornecimentoServico extends AppModel
{

    public $name = 'OrdemFornecimentoServico';

    public $useTable = 'ordem_fornecimento_servico';

    public $primaryKey = 'co_ordem_fornecimento_servico';

    public $validate = array(
        
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ),
        'dt_emissao' => array(
            'date' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Emissão em branco'
            )
        ),
        'dt_final' => array(
            'rule' => array(
                'validaDtPrazoFinal',
                true
            ),
            'message' => 'O Prazo Final deve ser maior que a Data Inicial.',
            'allowEmpty' => true
        ),
        'dt_inicial' => array(
            'date' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data de Inicio em branco'
            )
        )
    );

    public function validaDtPrazoFinal()
    {
        if (isset($this->data['OrdemFornecimentoServico']['dt_inicial'])) {
            if (! isset($this->data['OrdemFornecimentoServico']['dt_final'])) {
                return false;
            }
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['OrdemFornecimentoServico']['dt_inicial'], $this->data['OrdemFornecimentoServico']['dt_final'], "US");
            if ($diferenca <= 0) {
                return false;
            }
        }
        return true;
    }

    public function beforeSave($options = array())
    {
        
        if (empty($this->data['OrdemFornecimentoServico']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['OrdemFornecimentoServico']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }
        
        if (empty($this->data['OrdemFornecimentoServico']['dt_emissao'])) {
            $this->data['OrdemFornecimentoServico']['dt_emissao'] = null;
        }
        if (empty($this->data['OrdemFornecimentoServico']['dt_inicial'])) {
            $this->data['OrdemFornecimentoServico']['dt_inicial'] = null;
        }
        if (empty($this->data['OrdemFornecimentoServico']['dt_final'])) {
            $this->data['OrdemFornecimentoServico']['dt_final'] = null;
        }
        if (isset($this->data['OrdemFornecimentoServico']['dc_descricao'])) {
            $this->data['OrdemFornecimentoServico']['dc_descricao'] = up($this->data['OrdemFornecimentoServico']['dc_descricao']);
        }
        
        return parent::beforeSave($options);
    }
}