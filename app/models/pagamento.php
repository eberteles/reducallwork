<?php

class Pagamento extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $tipoPgto = array(
        'P' => 'Pagamento',
        'C' => 'Cancelamento'
    );

    var $name = 'Pagamento';

    var $useTable = 'pagamentos';

    var $primaryKey = 'co_pagamento';

    var $validate = array(
        'co_pagamento' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),




        // 'co_empenho' => array(
        // 'numeric' => array(
        // 'rule' => array(
        // 'numeric'
        // ),
        // 'message' => 'Campo inválido'
        // ) //'allowEmpty' => false,
        // ), //'required' => false,
        // 'last' => false, // Stop validation after this rule
        // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        // 'num_serie' => array(
        // 'numeric' => array(
        // 'rule' => array(
        // 'numeric'
        // ),
        // 'message' => 'Campo inválido'
        // ) //'allowEmpty' => false,
        // ), //'required' => false,
        // 'last' => false, // Stop validation after this rule
        // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
//        'nu_mes_pagamento' => array(
//            'notempty' => array(
//                'rule' => array(
//                    'notempty'
//                ),
//                'message' => 'Campo Mês Competência em branco'
//            )
//        ) // 'allowEmpty' => false,
//// 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
//        'nu_ano_pagamento' => array(
//            'notempty' => array(
//                'rule' => array(
//                    'notempty'
//                ),
//                'message' => 'Campo Ano em branco'
//            )
//        ) // 'allowEmpty' => false,
// 'required' => false,
           // 'last' => false, // Stop validation after this rule
           // 'on' => 'create', // Limit validation to 'create' or 'update' operations
           
        // 'dt_vencimento' => array(
           // 'date' => array(
           // 'rule' => array(
           // 'date'
           // ),
           // 'message' => 'Data inválida'
           // ) //'allowEmpty' => false,
           // ),
           
        // 'dt_financeiro' => array(
           // 'date' => array(
           // 'rule' => array(
           // 'date'
           // ),
           // 'message' => 'Data inválida'
           // ) //'allowEmpty' => false,
           // ),
        'vl_pago' => array(
            'rule' => array(
                'notempty'
            ),
            'message' => 'Campo Valor Pago em branco'
        ),
        'vl_pagamento' => array(
            'rule' => array(
                'notempty'
            ),
            'message' => 'Campo Valor Bruto em branco'
        )
    )
    // 'ds_observacao' => array(
    // 'rule' => array('validaJustificativa', true),
    // 'message' => 'Informe uma Justificativa para pagamento sem Valor de Imposto.'
    // )
    ;
    // 'on' => 'create', // Limit validation to 'create' or 'update' operations
    var $belongsTo = array(
        'Empenho' => array(
            'className' => 'Empenho',
            'foreignKey' => 'co_empenho'
        ),
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'co_atividade'
        )
    );
    
    // var $hasMany = array(
    // 'NotasPagamento' => array(
    // 'className' => 'NotasPagamento',
    // 'foreignKey' => 'co_nota'
    // )
    // );
    var $hasAndBelongsToMany = array(
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'joinTable' => 'notas_pagamentos',
            'with' => 'NotasPagamento',
            'foreignKey' => 'co_pagamento',
            'associationForeignKey' => 'co_nota'
        )
    );


    public function selectPagamentos()
    {
//        $result = $this->query("SELECT * FROM pagamentos WHERE YEAR( dt_vencimento ) = ".date('Y')." ORDER BY dt_vencimento");
        $result = $this->query("SELECT * FROM pagamentos WHERE nu_ano_pagamento = ".date('Y')." ORDER BY dt_pagamento");

        return $result;

    }

    function validaJustificativa()
    {
        if (isset($this->data['Pagamento']['vl_imposto']) && $this->data['Pagamento']['vl_imposto'] <= 0 && $this->data['Pagamento']['ds_observacao'] == "") {
            return false;
        } else {
            return true;
        }
    }

    public function getTotal($co_contrato)
    {
        $result = $this->query("SELECT SUM( vl_pagamento ) as vl_total_pagamento FROM pagamentos 
                                    WHERE dt_pagamento is not null and co_contrato = '$co_contrato' ");
        return $result[0][0]['vl_total_pagamento'];
    }

    function beforeValidate($options = array())
    {
        $this->data['Pagamento']['ds_observacao'] = up($this->data['Pagamento']['ds_observacao']);
        if (isset($this->data['Pagamento']['vl_nota'])) {
            $this->data['Pagamento']['vl_nota'] = ln($this->data['Pagamento']['vl_nota']);
        }
        if (isset($this->data['Pagamento']['vl_imposto'])) {
            $this->data['Pagamento']['vl_imposto'] = ln($this->data['Pagamento']['vl_imposto']);
        }
        if (isset($this->data['Pagamento']['vl_liquido'])) {
            $this->data['Pagamento']['vl_liquido'] = ln($this->data['Pagamento']['vl_liquido']);
        }
        if (isset($this->data['Pagamento']['vl_pago'])) {
            $this->data['Pagamento']['vl_pago'] = ln($this->data['Pagamento']['vl_pago']);
        }
        if (isset($this->data['Pagamento']['vl_pagamento'])) {
            $this->data['Pagamento']['vl_pagamento'] = ln($this->data['Pagamento']['vl_pagamento']);
        }
        if (empty($this->data['Pagamento']['dt_pagamento'])) {
            $this->data['Pagamento']['dt_pagamento'] = null;
        }
        if (empty($this->data['Pagamento']['co_atividade']) || $this->data['Pagamento']['co_atividade'] <= 0) {
            $this->data['Pagamento']['co_atividade'] = null;
        }
        if (empty($this->data['Pagamento']['dt_vencimento'])) {
            $this->data['Pagamento']['dt_vencimento'] = null;
        }
        if (empty($this->data['Pagamento']['dt_financeiro'])) {
            $this->data['Pagamento']['dt_financeiro'] = null;
        }
        if (empty($this->data['Pagamento']['dt_administrativo'])) {
            $this->data['Pagamento']['dt_administrativo'] = null;
        }
        
        return parent::beforeValidate($options);
    }

    public function afterSave($options = array())
    {
        if(!isset($this->data['co_pagamento']) || $this->data['co_pagamento'] == null || $this->data['co_pagamento']){
            $nextId = $this->find('first', array('conditions' => array('co_pagamento is not' => null),
                'order' => array('co_pagamento' => 'DESC') ));

            $this->data['Pagamento']['co_pagamento'] = $nextId['Pagamento']['co_pagamento'] + 1;
        }

        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Pagamento']['co_usuario'] = $usuario['Usuario']['co_usuario'];

        return parent::beforeSave($options);
    }

    public function getPagamentosPorFornecedor()
    {
        return $result = $this->query("SELECT 
                    p.co_pagamento, p.dt_vencimento, f.nu_cnpj, p.nu_nota_fiscal 
                FROM contratos c 
                    join pagamentos p on c.co_contrato = p.co_contrato 
                    join fornecedores f on f.co_fornecedor = c.co_fornecedor");
    }

    public function contratoComFornecedor($contrato)
    {
        $retorno = false;
        $sql = $this->query("SELECT co_fornecedor 
                FROM contratos where co_contrato = $contrato ");
        if ($sql[0]['contratos']['co_fornecedor'] > 0) {
            $retorno = true;
        }
        return $retorno;
    }

    public function atualizaPagamentos($cnpj, $nota_fiscal, $nota_fiscal_serie, $dt_quitacao, $dt_vencimento, $vl_nota_fiscal, $vl_parcela)
    {
        return $this->query('update pagamentos p' . 'set p.vl_pagamento = ' . $vl_nota_fiscal . ',' . 'p.dt_pagamento = ' . $dt_quitacao . ',' . 'where p.nu_nota_fiscal = ' . $nota_fiscal . ',' . 'and p.nu_serie_nf = ' . $nota_fiscal_serie);
    }

    public function findNotasComPagamentos($co_contrato){
        return $this->query(" select
                                    p.nu_nota_fiscal as nu_nota,
                                    (p.vl_pagamento) as novo_valor
                                from
                                    pagamentos as p, notas_pagamentos as np, notas_fiscais as n
                                where
                                    p.co_contrato = $co_contrato
                                    and np.co_pagamento = p.co_pagamento
                                    and p.nu_nota_fiscal = n.nu_nota
                                ");
    }
}
?>
