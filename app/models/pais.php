<?php

class Pais extends AppModel
{

    var $name = 'Pais';

    var $useTable = 'paises';

    var $primaryKey = 'co_pais';
    
    var $displayField = 'no_pais';
}
?>