<?php

class PedidosItem extends AppModel
{

    var $name = 'PedidosItem';

    var $useTable = 'pedidos_itens';

    var $primaryKey = 'co_pedido_item';

    var $displayField = 'nu_item';

    var $order = 'Lote.nu_lote, PedidosItem.nu_item';

    var $validate = array(
        
        'nu_item' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número do Item em branco'
            )
        ),
        'ds_descricao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Objeto em branco'
            )
        ),
        'ds_unidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Unidade em branco'
            )
        ),
        'qt_pedido' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Quantidade em branco'
            )
        ),
        'vl_unitario' => array(
            'money' => array(
                'rule' => array(
                    'money'
                ),
                'message' => 'Campo Valor Unitário inválido'
            )
        )
    );

    var $belongsTo = array(
        'Lote' => array(
            'className' => 'AtasLote',
            'foreignKey' => 'co_ata_lote'
        )
    );
    
    // var $hasMany = array(
    // 'Pedido' => array(
    // 'className' => 'AtasPedido',
    // 'foreignKey' => 'co_pedido_item'
    // )
    // );
    public function beforeSave($options = array())
    {
        if (isset($this->data['PedidosItem']['ds_marca'])) {
            $this->data['PedidosItem']['ds_marca'] = up($this->data['PedidosItem']['ds_marca']);
        }
        if (isset($this->data['PedidosItem']['ds_descricao'])) {
            $this->data['PedidosItem']['ds_descricao'] = up($this->data['PedidosItem']['ds_descricao']);
        }
        if (isset($this->data['PedidosItem']['ds_unidade'])) {
            $this->data['PedidosItem']['ds_unidade'] = up($this->data['PedidosItem']['ds_unidade']);
        }
        if (isset($this->data['PedidosItem']['vl_unitario'])) {
            $this->data['PedidosItem']['vl_unitario'] = ln($this->data['PedidosItem']['vl_unitario']);
        }
        
        return parent::beforeSave($options);
    }
}
?>