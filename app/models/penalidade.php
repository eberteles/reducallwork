<?php

class Penalidade extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Penalidade';

    var $useTable = 'penalidades';

    var $primaryKey = 'co_penalidade';

    var $validate = array(

        'co_contrato' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'A Penalidade deve estar vinculada a um Contrato'
            )
        ),
        'dt_penalidade' => array(
            'date' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data da Penalidade em branco'
            )
        ),
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Observações em branco'
            )
        ),
        'co_tipo_penalidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Tipo de Penalidade em branco'
            )
        )
    );

    var $belongsTo = array(
        'NotaFiscal' => array(
            'className' => 'NotaFiscal',
            'foreignKey' => 'co_nota'
        ),
        'TipoPenalidade' => array(
            'className' => 'TipoPenalidade',
            'foreignKey' => 'co_tipo_penalidade'
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Penalidade']['ds_observacao'] = up($this->data['Penalidade']['ds_observacao']);

        $this->data['Penalidade']['mt_penalidade'] = up($this->data['Penalidade']['mt_penalidade']);

        if (empty($this->data['Penalidade']['dt_penalidade'])) {
            $this->data['Penalidade']['dt_penalidade'] = null;
        }

        if (empty($this->data['Penalidade']['dt_ini_vigencia'])) {
            $this->data['Penalidade']['dt_ini_vigencia'] = null;
        }

        if (empty($this->data['Penalidade']['dt_fim_vigencia'])) {
            $this->data['Penalidade']['dt_fim_vigencia'] = null;
        }

        if (empty($this->data['Penalidade']['dt_publicacao'])) {
            $this->data['Penalidade']['dt_publicacao'] = null;
        }

        if (empty($this->data['Penalidade']['dt_registro_sicaf'])) {
            $this->data['Penalidade']['dt_registro_sicaf'] = null;
        }

        return parent::beforeValidate($options);
    }

    public function afterSave($options = array())
    {
        if (!isset($this->data['co_penalidade']) || $this->data['co_penalidade'] == null || $this->data['co_penalidade']) {
            $nextId = $this->find('first', array('conditions' => array('co_penalidade is not' => null),
                'order' => array('co_penalidade' => 'DESC')));

            $this->data['Penalidade']['co_penalidade'] = $nextId['Penalidade']['co_penalidade'] + 1;
        }

        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Penalidade']['co_usuario'] = $usuario['Usuario']['co_usuario'];

        if (empty($this->data['Penalidade']['dt_ini_vigencia'])) {
            unset($this->data['Penalidade']['dt_ini_vigencia']);
        }

        if (empty($this->data['Penalidade']['dt_fim_vigencia'])) {
            unset($this->data['Penalidade']['dt_fim_vigencia']);
        }

        if (empty($this->data['Penalidade']['dt_publicacao'])) {
            unset($this->data['Penalidade']['dt_publicacao']);
        }

        if (empty($this->data['Penalidade']['dt_registro_sicaf'])) {
            unset($this->data['Penalidade']['dt_registro_sicaf']);
        }

        return parent::beforeSave($options);
    }
}
