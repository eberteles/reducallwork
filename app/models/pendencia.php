<?php

class Pendencia extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Pendencia';

    var $useTable = 'pendencias';

    var $primaryKey = 'co_pendencia';

    var $validate = array(
        'co_pendencia' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
          
        // 'co_contrato' => array(
          // 'numeric' => array(
          // 'rule' => array(
          // 'numeric'
          // ),
          // 'message' => 'Campo inválido'
          // ) //'allowEmpty' => false,
          // )//'required' => false,
          // //'last' => false, // Stop validation after this rule
          // //'on' => 'create', // Limit validation to 'create' or 'update' operations
          //
          //
          // ,
        'ds_pendencia' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco '
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'dt_inicio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => ' Campo Início em  branco'
            )
        ) // 'allowEmpty' => false,
,
        
        'dt_fim' => array(
            'validaDataFimAtual' => array(
                'rule' => array('validaDtFimAtual'),
                'message' => 'A Data Fim deve ser menor que a data Atual.',
                'allowEmpty' => true,
            ),
            'validaDataFim' => array(
                'rule' => array('validaDtFim'),
                'message' => 'A Data Fim deve ser maior que a Data de Início.',
                'allowEmpty' => true,
            ),
        ),
        
        'st_pendencia' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Situação em branco'
            )
        ) // 'allowEmpty' => false,
, // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        'ds_observacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Observação em branco',
                'allowEmpty' => true
            )
        ),
        
        'ds_impacto_pagamento' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Impacto Pagamento em branco'
            )
        )
    ) // 'required' => false,
      // 'last' => false, // Stop validation after this rule
      // 'on' => 'create', // Limit validation to 'create' or 'update' operations
    
    ;

    function validaDtFim()
    {
        if (! isset($this->data['Pendencia']['dt_inicio']) || $this->data['Pendencia']['dt_inicio'] == "") {
            return false;
        } else {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Pendencia']['dt_inicio'], $this->data['Pendencia']['dt_fim'], "US");
            if ($diferenca < 0) {
                return false;
            }
        }
        return true;
    }

    public function validaDtFimAtual()
    {

        $dtNow = new \DateTime('now');

        App::import('Helper', 'Print');
        $print = new PrintHelper();
        $diferencaAtual = $print->difEmDias($dtNow->format('Y-m-d'), $this->data['Pendencia']['dt_fim'], "US");

        if ($diferencaAtual > 0) {
            return false;
        }

        return true;
    }

    function updNuPendencias($coContrato)
    {
        $this->query("UPDATE `contratos` SET  `nu_pendencias` = 
               ( SELECT count(*) FROM `pendencias` WHERE `co_contrato` = $coContrato and `st_pendencia` = 'P' ) 
                WHERE  `contratos`.`co_contrato` = $coContrato ");
    }

    function beforeValidate($options = array())
    {
        $this->data['Pendencia']['ds_pendencia'] = up($this->data['Pendencia']['ds_pendencia']);
        $this->data['Pendencia']['ds_observacao'] = up($this->data['Pendencia']['ds_observacao']);
        
        if (empty($this->data['Pendencia']['dt_fim'])) {
            $this->data['Pendencia']['dt_fim'] = null;
        }
        
        return parent::beforeValidate($options);
    }

    public function afterSave($options = array())
    {
        if(!isset($this->data['co_pendencia']) || $this->data['co_pendencia'] == null || $this->data['co_pendencia']){
            $nextId = $this->find('first', array('conditions' => array('co_pendencia is not' => null),
                'order' => array('co_pendencia' => 'DESC') ));

            $this->data['Pendencia']['co_pendencia'] = $nextId['Pendencia']['co_pendencia'] + 1;
        }
        
        return parent::beforeSave($options);
    }
}
?>