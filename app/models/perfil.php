<?php
/**
 * Classe MODEL da tabela Perfis do banco GESCON.
 *
 * @category   Models
 * @package    App/Models
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @license    <licença-n2oti-link>
 * @version    Release: @package_version@
 * @since      Class available since Release 2.16.09
 */
class Perfil extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
    
	/**
	 * @var string
	 */
	public $name = 'Perfil';

	/**
	 * @var string
	 */
	public $useTable = 'perfis';

	/**
	 * @var string
	 */
	public $primaryKey = 'co_perfil';

	/**
	 * @var string
	 */
	public $displayField = 'no_perfil';

	/**
	 * @var string
	 */
	public $validate = array(
		'no_perfil' => array(
			'notempty' => array(
				'rule' => array(
						'notempty'
				),
				'message' => 'Campo perfil em branco'
			)
		)
	);
	
	/**
	 * @var array
	 */
	public $hasMany = array(
		'Permissao' => array(
			'className'		=> 'Permissao',
            'foreignKey'	=> 'co_perfil',
		)
	);
}