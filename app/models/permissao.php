<?php
/**
 * Classe MODEL da tabela Permissões do banco GESCON.
*
* @category   Models
* @package    App/Models
* @author     Rafael Yoo <rafael.yoo@n2oti.com>
* @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
* @license    <licença-n2oti-link>
* @version    Release: @package_version@
* @since      Class available since Release 2.16.09
*/
class Permissao extends AppModel
{
	/**
	 * @var string
	 */
	public $name = 'Permissao';

	/**
	 * @var string
	 */
	public $useTable = 'permissoes';

	/**
	 * @var string
	 */
	public $primaryKey = 'co_permissao';

	/**
	 * @var string
	 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'co_usuario'
		),
		'Perfil' => array(
			'className' => 'Perfil',
			'foreignKey' => 'co_perfil'
		),
		'Recurso' => array(
			'className' => 'Recurso',
			'foreignKey' => 'co_recurso'
		)
	);
	
	/**
	 * @return array
	 */
	public function getPermissaoPorPerfil( $coPerfil )
	{
		$coPerfil = (integer) $coPerfil;
		
		$sql = "SELECT r.* FROM permissoes p
				JOIN recursos r ON p.co_recurso = r.co_recurso
				WHERE p.co_perfil = " . $coPerfil . ";";
		
		return $this->query($sql);
	}
}