<?php

class PreEmpenho extends AppModel
{

    var $name = 'PreEmpenho';

    var $useTable = 'pre_empenho';

    var $primaryKey = 'co_pre_empenho';

    public function beforeSave($options = array()){

        if(isset($this->data['PreEmpenho']['vl_pre_empenho'])){
            $this->data['PreEmpenho']['vl_pre_empenho'] = ln($this->data['PreEmpenho']['vl_pre_empenho']);
        }

        if(isset($this->data['PreEmpenho']['vl_restante'])){
            $this->data['PreEmpenho']['vl_restante'] = ln($this->data['PreEmpenho']['vl_restante']);
        }

        return parent::beforeSave($options);

    }
}