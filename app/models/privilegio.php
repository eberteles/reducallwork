<?php

class Privilegio extends AppModel
{

    var $name = 'Privilegio';

    var $useTable = 'privilegios';

    var $primaryKey = 'co_privilegio';

    var $displayField = 'sg_privilegio';

    var $validate = array(
        'sg_privilegio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'ds_privilegio' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Privilegio']['sg_privilegio'] = up($this->data['Privilegio']['sg_privilegio']);
        $this->data['Privilegio']['ds_privilegio'] = up($this->data['Privilegio']['ds_privilegio']);
        
        return parent::beforeValidate($options);
    }
}
?>