<?php

class PrivilegioAcesso extends AppModel
{

    var $name = 'PrivilegioAcesso';

    var $useTable = 'privilegios_acessos';

    var $validate = array(
        'co_privilegio_acesso' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'co_acesso' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        ),
        'co_privilegio' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            
        )
    );
}
?>