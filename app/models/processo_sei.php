<?php

class ProcessoSei extends AppModel
{

    /**
     * array(
     *  'INSERT',
     *  'UPDATE',
     *  'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'ProcessoSei';

    public $useTable = 'processo_sei';

    public $primaryKey = 'co_processo_sei';

    public $displayField = 'nu_processo_sei';

    public $validate = array(

        'nu_processo_sei' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nr Processo em branco'
            ),
            'myUnique' => array(
                'rule' => array('validateUnique'),
                'message' =>'Este processo já está vinculado para esse contrato'
            ),
            'isProcessoSei' => array(
                'rule' => array('validateProcessoSei'),
                'message' => 'Processo não localizado no SEI'
            )
        )
    );

    public function beforeValidate($options = array())
    {
        $this->data[$this->name]['nu_processo_sei'] =
            preg_replace('/\D/', '', $this->data[$this->name]['nu_processo_sei']);
        unset($this->data[$this->name]['dt_cadastro']);
        return parent::beforeValidate($options);
    }

    function validateProcessoSei($data)
    {
        try {
            App::import('Helper', 'Sei');
            $sei = new SeiHelper();
            $sei->consultarProcesso($data['nu_processo_sei']);
            $valid = true;
        } catch (\Exception $e) {
            $valid = false;
        }

        return $valid;
    }

    function validateUnique($data)
    {
        $count = $this->find('count', array(
            'conditions' => array(
                'nu_processo_sei' => $data['nu_processo_sei'],
                'co_contrato' => $this->data[$this->name]['co_contrato']
            )
        ));

        return $count ? false : true;
    }

}
