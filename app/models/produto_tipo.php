<?php

class ProdutoTipo extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'ProdutoTipo';
    
    var $useTable = 'produtos_tipos';

    var $primaryKey = 'co_produto_tipo';
    
    var $displayField = 'ds_tipo';

    var $validate = array(
        'co_produto_tipo' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        ),
        'ds_tipo' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            )
        )
    );

    public function afterSave($options = array())
    {
        if (empty($this->data['ProdutoTipo']['co_usuario'])) {
            $sessao = new SessionComponent();
            $usuario = $sessao->read('usuario');
            $this->data['ProdutoTipo']['co_usuario'] = $usuario['Usuario']['co_usuario'];
        }

        if(isset($this->data['ProdutoTipo']['ds_tipo'])) {
            $this->data['ProdutoTipo']['ds_tipo'] = up($this->data['ProdutoTipo']['ds_tipo']);
        }
        
        return parent::beforeSave($options);
    }
}
?>