<?php

class Projeto extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
    
    var $name       = "Projeto";
    var $useTable   = "projetos";
    var $primaryKey = "co_projeto";
    var $displayField = 'no_projeto';
    var $recursive = 1;

    var $validate = array(
        'no_projeto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome do Projeto em branco'
            ),
             'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Nome do Projeto já cadastrada'
            )
        ),
        'ds_projeto' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição do Projeto em branco'
            )
        )
    );
    public function getByProjeto($no_projeto)
    {
        return $this->query(" SELECT * FROM projetos WHERE no_projetos = '".($no_projeto)."'");
    }
    public function afterSave($options = array())
    {
        if(!isset($this->data['co_projeto']) || $this->data['co_projeto'] == null || $this->data['co_projeto']){
            $nextId = $this->find('first', array('conditions' => array('co_projeto is not' => null),
                'order' => array('co_projeto' => 'DESC') ));

            $this->data['Projeto']['co_projeto'] = $nextId['Projeto']['co_projeto'] + 1;
        }
        
        return parent::beforeSave($options);
    }
}