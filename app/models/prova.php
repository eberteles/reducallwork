<?php

class Prova extends AppModel
{

    var $name = 'Prova';

    var $useTable = 'provas';

    var $primaryKey = 'co_prova';

    var $displayField = 'ds_prova';
    
    var $hasMany = array(
        'ProvaCategoria' => array(
            'className' => 'ProvaCategoria',
            'foreignKey' => 'co_prova',
            'dependent' => true
        ),
        'ProvaSubCategoria' => array(
            'className' => 'ProvaSubCategoria',
            'foreignKey' => 'co_prova',
            'dependent' => true
        ),
        'ProvaClassificacao' => array(
            'className' => 'ProvaClassificacao',
            'foreignKey' => 'co_prova',
            'dependent' => true
        )
    );
        
    var $validate = array(
        
        'ds_prova' => array(
            'isUnique' => array(
                'rule' => array('validaDsProva', true),
                'message' => 'Campo Nome da Prova em branco ou já cadastrado.'
            )
        )
    );
    
    function validaDsProva()
    {
        if (! isset($this->data['Prova']['ds_prova'])) {
            return false;
        } else {
            $conditions = array(
                'Prova.ds_prova' => $this->data['Prova']['ds_prova'],
                'Prova.co_modalidade' => $this->data['Prova']['co_modalidade'] );
            if (isset($this->data['Prova']['co_prova']) && $this->data['Prova']['co_prova'] > 0) {
                $conditions['Prova.co_prova != '] = $this->data['Prova']['co_prova'];
            }
            $count  = $this->find('count', array( 'conditions' => $conditions ));
            if($count > 0) {
                return false;
            }
        }
        return true;
    }
    
    function beforeValidate($options = array())
    {
        $this->data['Prova']['ds_prova'] = up($this->data['Prova']['ds_prova']);
        
        return parent::beforeValidate($options);
    }
}
?>