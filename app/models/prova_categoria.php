<?php

class ProvaCategoria extends AppModel
{

    var $name = 'ProvaCategoria';

    var $useTable = 'provas_categorias';

    var $primaryKey = 'co_prova_categoria';

    var $displayField = 'ds_prova_categoria';
    
    var $order = "ds_prova_categoria ASC";

    var $validate = array(
        'ds_prova_categoria' => array(
            'isUnique' => array(
                'rule' => array('validaDsProvaCategoria', true),
                'message' => 'Campo Nome da Categoria em branco ou já cadastrado.'
            )
        )
    );
    
    function validaDsProvaCategoria()
    {
        if (! isset($this->data['ProvaCategoria']['ds_prova_categoria']) || $this->data['ProvaCategoria']['ds_prova_categoria'] == '') {
            return false;
        } else {
            $conditions = array(
                'ProvaCategoria.ds_prova_categoria' => $this->data['ProvaCategoria']['ds_prova_categoria'],
                'ProvaCategoria.co_prova' => $this->data['ProvaCategoria']['co_prova'] );
            if (isset($this->data['ProvaCategoria']['co_prova_categoria']) && $this->data['ProvaCategoria']['co_prova_categoria'] > 0) {
                $conditions['ProvaCategoria.co_prova_categoria != '] = $this->data['ProvaCategoria']['co_prova_categoria'];
            }
            $count  = $this->find('count', array( 'conditions' => $conditions ));
            if($count > 0) {
                return false;
            }
        }
        return true;
    }

    function beforeSave($options = array())
    {
        $this->data['ProvaCategoria']['ds_prova_categoria'] = up($this->data['ProvaCategoria']['ds_prova_categoria']);
        return parent::beforeValidate($options);
    }
}