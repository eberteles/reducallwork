<?php

class ProvaClassificacao extends AppModel
{

    var $name = 'ProvaClassificacao';

    var $useTable = 'provas_classificacoes';

    var $primaryKey = 'co_prova_classificacao';

    var $displayField = 'ds_prova_classificacao';

    var $order = "ds_prova_classificacao ASC";

    var $validate = array(
        'ds_prova_classificacao' => array(
            'isUnique' => array(
                'rule' => array('validaDsProvaClassificacao', true),
                'message' => 'Campo Nome da Classificação em branco ou já cadastrado.'
            )
        )
    );
    
    function validaDsProvaClassificacao()
    {
        if (! isset($this->data['ProvaClassificacao']['ds_prova_classificacao']) || $this->data['ProvaClassificacao']['ds_prova_classificacao'] == '') {
            return false;
        } else {
            $conditions = array(
                'ProvaClassificacao.ds_prova_classificacao' => $this->data['ProvaClassificacao']['ds_prova_classificacao'],
                'ProvaClassificacao.co_prova' => $this->data['ProvaClassificacao']['co_prova'] );
            if (isset($this->data['ProvaClassificacao']['co_prova_classificacao']) && $this->data['ProvaClassificacao']['co_prova_classificacao'] > 0) {
                $conditions['ProvaClassificacao.co_prova_classificacao != '] = $this->data['ProvaClassificacao']['co_prova_classificacao'];
            }
            $count  = $this->find('count', array( 'conditions' => $conditions ));
            if($count > 0) {
                return false;
            }
        }
        return true;
    }

    function beforeSave($options = array())
    {
        $this->data['ProvaClassificacao']['ds_prova_classificacao'] = up($this->data['ProvaClassificacao']['ds_prova_classificacao']);
        return parent::beforeValidate($options);
    }
}