<?php

class ProvaSubCategoria extends AppModel
{

    var $name = 'ProvaSubCategoria';

    var $useTable = 'provas_subcategorias';

    var $primaryKey = 'co_prova_subcategoria';

    var $displayField = 'ds_prova_subcategoria';

    var $order = "ds_prova_subcategoria ASC";

    var $validate = array(
        'ds_prova_subcategoria' => array(
            'isUnique' => array(
                'rule' => array('validaDsProvaSubCategoria', true),
                'message' => 'Campo Nome da Sub-Categoria em branco ou já cadastrado.'
            )
        )
    );
    
    function validaDsProvaSubCategoria()
    {
        if (! isset($this->data['ProvaSubCategoria']['ds_prova_subcategoria']) || $this->data['ProvaSubCategoria']['ds_prova_subcategoria'] == '') {
            return false;
        } else {
            $conditions = array(
                'ProvaSubCategoria.ds_prova_subcategoria' => $this->data['ProvaSubCategoria']['ds_prova_subcategoria'],
                'ProvaSubCategoria.co_prova' => $this->data['ProvaSubCategoria']['co_prova'] );
            if (isset($this->data['ProvaSubCategoria']['co_prova_subcategoria']) && $this->data['ProvaSubCategoria']['co_prova_subcategoria'] > 0) {
                $conditions['ProvaSubCategoria.co_prova_subcategoria != '] = $this->data['ProvaSubCategoria']['co_prova_subcategoria'];
            }
            $count  = $this->find('count', array( 'conditions' => $conditions ));
            if($count > 0) {
                return false;
            }
        }
        return true;
    }

    function beforeSave($options = array())
    {
        $this->data['ProvaSubCategoria']['ds_prova_subcategoria'] = up($this->data['ProvaSubCategoria']['ds_prova_subcategoria']);
        return parent::beforeValidate($options);
    }
}