<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/11/2015
 * Time: 15:11
 */

class Providencia extends AppModel
{
    var $name       = "Providencia";
    var $useTable   = "providencias";
    var $primaryKey = "co_providencia";
    var $recursive = 1;

    var $validate = array(
        'ds_providencia' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição da Especialidade em branco'
            )
        )
    );

    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario'
        )
    );

    public function beforeSave($options = array())
    {
        $this->data['Providencia']['dt_providencia'] = date('Y-m-d H:i:s');

        return parent::beforeSave($options);
    }
}