<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/11/2015
 * Time: 15:10
 */

class Reclamacao extends AppModel
{
    var $name       = "Reclamacao";
    var $useTable   = "reclamacoes";
    var $primaryKey = "co_reclamacao";
    var $recursive = 1;

    var $validate = array(
        'nu_reclamacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Número da Reclamação em branco'
            )
        ),
        'co_fornecedor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Fornecedor em branco'
            )
        ),
        'co_contrato' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Contrato em branco'
            )
        ),
        'dt_problema' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Data da Reclamação em branco'
            ),
            'rule' => array(
                'validaDtProblema',
                true
            ),
            'message' => 'A data do problema não pode ser maior que hoje!'
        ),
        'assunto_reclamacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Assunto da Reclamação em branco'
            )
        ),
    );

    function validaDtProblema()
    {
        if (isset($this->data['Reclamacao']['dt_problema'])) {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            $diferenca = $print->difEmDias($this->data['Reclamacao']['dt_problema'], date('Y-m-d'), "US");
            if ($diferenca < 0) {
                return false;
            }
        }
        return true;
    }

    var $belongsTo = array(
        'Contrato' => array(
            'className' => 'Contrato',
            'foreignKey' => 'co_contrato'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'co_fornecedor'
        )
    );

    public function beforeSave($options = array())
    {

        if(isset($this->data["Reclamacao"]["nu_reclamacao"]))
            $this->data["Reclamacao"]["nu_reclamacao"] = str_replace("/","",$this->data["Reclamacao"]["nu_reclamacao"]);

        if(isset($this->data["Reclamacao"]["dt_problema"]) && empty($this->data["Reclamacao"]["dt_problema"]))
            $this->data["Reclamacao"]["dt_problema"] = '';

        return parent::beforeSave($options);
    }
}