<?php
/**
 * Classe MODEL da tabela Recurso do banco GESCON.
 *
 * @category   Models
 * @package    App/Models
 * @author     Rafael Yoo <rafael.yoo@n2oti.com>
 * @copyright  2016 N2O Tecnologia da Informação LDTA-ME.
 * @license    <licença-n2oti-link>
 * @version    Release: @package_version@
 * @since      Class available since Release 2.16.09
 */
class Recurso extends AppModel
{
    /**
     * @var string
     */
    public $name = 'Recurso';

    /**
     * @var string
     */
    public $useTable = 'recursos';

    /**
     * @var string
     */
    public $primaryKey = 'co_recurso';

    /**
     * @var string
     */
    public $displayField = 'no_recurso';

    /**
     * @var array
     */
    var $hasMany = array(
        'Permissao' => array(
            'className'     => 'Permissao',
            'foreignKey'    => 'co_recurso',
            'order'    		=> 'Permissao.co_recurso ASC',
            'dependent'		=> true
        )
    );

    var $validate = array(
        'no_recurso' => array(
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'O nome do recurso já está em uso.'
            ),
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo Recurso em branco'
            )
        ),
    );
}