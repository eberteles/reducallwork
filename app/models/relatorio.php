<?php

class Relatorio extends AppModel
{

    var $name = 'Relatorio';

    var $useTable = false;

    /**
     *
     * @param
     *            $sql
     * @return unknown_type
     */
    public function consulta($sql)
    {
        return $this->query(" SELECT * FROM ( $sql ) AS RELATORIO ");
    }

    public function listarPamsFase()
    {
        $pams = $this->consulta('select
                                    c.nu_pam as `Número do PAM`,
                                    c.ds_objeto as Objeto,
                                    c.dt_fase as `Início da fase`,
                                    a.dt_andamento as Inicio,
                                    s.ds_setor as Setor,
                                    f.ds_fase as Fase,
                                    f.mm_duracao_fase as Duracao
                                from
                                    contratos c,
                                    andamentos a,
                                    fases f,
                                    setores s
                                where
                                    c.nu_pam != ""
                                    AND c.co_contrato = a.co_contrato
                                    AND c.co_setor = s.co_setor
                                    AND c.co_fase = f.co_fase
                                group by
                                    c.co_contrato');

        return $pams;
    }
    
    public function listarProspeccao()
    {
        $pams = $this->consulta("select
                                    if( c.nu_pam is not null,  " . FunctionsComponent::getMaskSQL('c.nu_pam', 'pam') . ", '---') as 'Prospecção',
                                    cli.no_razao_social as `Cliente`,
                                    rep.ds_nome as `Responsável Parceiro`,
                                    red.ds_nome as `Responsável Reducall`,
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Valor Estimado',
                                    f.ds_fase as Fase
                                from
                                    contratos c
                                    LEFT JOIN fases f on c.co_fase = f.co_fase 
                                    LEFT JOIN clientes cli on c.co_cliente = cli.co_cliente 
                                    LEFT JOIN usuarios rep on c.co_parceiro = rep.co_usuario
                                    LEFT JOIN usuarios red on c.co_gestor_atual = red.co_usuario
                                where
                                    c.nu_pam != '' and 
                                    c.ic_ativo = '1'
                                order by
                                    f.co_fase");

        return $pams;
    }

    public function listarContratosPorGestor()
    {

        $contratos = $this->consulta('select ' .
            FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Número do Processo',", 'isProcesso') . '
                                    if( c.nu_contrato is not null,  ' . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ', "---") as "Número do Contrato",
                                    c.ds_objeto as Objeto,
                                    u.ds_nome as Gestor
                                from
                                    contratos c,
                                    usuarios u
                                where
                                    c.co_gestor_atual != ""
                                    AND c.co_gestor_atual = u.co_usuario
                                order by
                                    c.co_gestor_atual');

        return $contratos;
    }

    /**
     *
     * @return unknown_type
     */
    public function listarValoresPorContrato($co_instituicao)
    {
        $sql = "select " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',
                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    c.ds_objeto   as Objeto,
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Inicial',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Mensal',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Vl " . __('Global', true) . "',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format((select sum(vl_aditivo)  from aditivos a where a.co_contrato = c.co_contrato) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Aditivos',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format(sum(p.vl_pagamento) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Pagamentos',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format(( c.vl_global + ifnull( sum(a.vl_aditivo), 0) ) - ifnull( sum(p.vl_pagamento), 0) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'A Pagar'
                                from
                                    contratos c
                                    LEFT JOIN aditivos      a on a.co_contrato = c.co_contrato
                                    LEFT JOIN pagamentos    p on (p.co_contrato = c.co_contrato and dt_pagamento is not null)
                                    LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor ";

        $sql .= "where c.ic_ativo = 1 ";

        $sql .= "group by
                                    c.nu_contrato,
                                    c.ds_objeto,
                                    c.vl_inicial,
                                    c.vl_global";
        return $this->consulta($sql);
    }

    public function liquidacaoPagamentosCompetencia($mes, $ano)
    {
        $sql = "select
                    c.nu_processo as 'Número do processo',
                    c.nu_contrato as 'Número do contrato',
                    f.no_razao_social as 'Nome do fornecedor',
                    c.ds_objeto as 'Objeto',
                    c.dt_ini_vigencia as 'início da vigênciao',
                    c.dt_fim_vigencia as 'Fim da vigência',
                    c.vl_global as 'Valor global',
                    l.dt_liquidacao as 'Data da liquidação',
                    l.vl_liquidacao as 'Valor da liquidação',
                    p.dt_pagamento as 'Data último pagamento',
                    (select
                        sum(vl_pagamento)
                        from pagamentos
                        where pagamentos.co_contrato = l.co_contrato
                    ) as 'Soma dos Pagamentos'
                from liquidacao as l
                join contratos as c on l.co_contrato = c.co_contrato
                join pagamentos as p on c.co_contrato = p.co_contrato
                join fornecedores as f on c.co_fornecedor = f.co_fornecedor
                where
                    year(l.dt_liquidacao) = $ano
                    and month(l.dt_liquidacao) = $mes
                group by l.co_liquidacao";

        return $this->consulta($sql);
    }

    public function liquidacoesPorPagamento()
    {
        $sql = "select
                    c.nu_processo as 'Número do processo',
                    c.nu_contrato as 'Número do contrato',
                    f.no_razao_social as 'Nome do fornecedor',
                    c.ds_objeto as 'Objeto',
                    c.dt_ini_vigencia as 'início da vigênciao',
                    c.dt_fim_vigencia as 'Fim da vigência',
                    c.vl_global as 'Valor global',
                    l.dt_liquidacao as 'Data da liquidação',
                    l.vl_liquidacao as 'Valor da liquidação',
                    p.dt_pagamento as 'Data último pagamento',
                    (select
                        sum(vl_pagamento)
                        from pagamentos
                        where pagamentos.co_contrato = l.co_contrato
                    ) as 'Soma dos Pagamentos'
                from liquidacao as l
                join contratos as c on l.co_contrato = c.co_contrato
                join pagamentos as p on c.co_contrato = p.co_contrato
                join fornecedores as f on c.co_fornecedor = f.co_fornecedor
                group by l.co_liquidacao";

        return $this->consulta($sql);
    }

    /**
     * Relação de empenho e pagamentos por contrato
     *
     * @return STR
     */
    // lista todos os contratos ordenado por categoria e subcategoria
    public function listarCategoriasSubcategorias()
    {
        return $this->consulta("select
            if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo',
            if( cont.nu_contrato > 0,  " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato',
            ifnull(forn.no_razao_social, '---') as 'Fornecedor',
            cont.ds_objeto as 'Objeto',
            ifnull(DATE_FORMAT(if(cont.co_modalidade=1, cont.dt_ini_processo , cont.dt_ini_vigencia),'%d/%m/%Y'),'---') as 'Data Inicial',
            ifnull(DATE_FORMAT(if(cont.co_modalidade=1, cont.dt_fim_processo , cont.dt_fim_vigencia),'%d/%m/%Y'),'---') as 'Data Final',
            ifnull(cat.no_categoria,'---') as 'Categoria',
            ifnull(sub.no_subcategoria,'---') as 'Subcategoria'
            from contratos cont
		LEFT JOIN fornecedores  forn on cont.co_fornecedor = forn.co_fornecedor
		LEFT JOIN categorias cat on cont.co_categoria=cat.co_categoria
		LEFT JOIN subcategorias sub on cont.co_subcategoria=sub.co_subcategoria
            order by cont.co_categoria");
    }

    /**
     * Relatório de Contratos por Categoria
     * @item 52
     * @param $coCategoria
     * @param $isSubCategoria
     * @return unknown_type
     */
    public function listarContratosCategoria($coCategoria, $isSubCategoria)
    {

        $sql = "SELECT ";
        $sql .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');
        $sql .= "if( cont.nu_contrato > 0,  " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . " , '---') as 'Contrato', ";
        $sql .= "ifnull(forn.no_razao_social, '---') as 'Fornecedor', ";
        $sql .= 'ifnull(cont.vl_global, "---") as "Valor Global", ';
        $sql .= "cont.ds_objeto as 'Objeto', ";
        $sql .= "if(cont.co_modalidade=1, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio', ";
        $sql .= "if(cont.co_modalidade=1, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim', ";
        $sql .= "ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), if( DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m') > 1, ' mêses', ' mês')), '---') as 'Vigência' ";
        // $sql .= "ifnull(sub.no_subcategoria, '---') as 'Subcategoria' ";
        $sql .= "FROM contratos cont ";
        $sql .= "JOIN fornecedores forn on (cont.co_fornecedor=forn.co_fornecedor) ";
        $sql .= "JOIN categorias cat on (cont.co_categoria=cat.co_categoria) ";

        if ($isSubCategoria) {
            $sql .= "LEFT JOIN subcategorias sub on (cont.co_subcategoria=sub.co_subcategoria) ";
        }

        $sql .= ' WHERE ';

        if ($coCategoria) {
            $sql .= "cont.co_categoria = $coCategoria and ";
        }

        $sql .= "cont.ic_ativo = 1 ";
        $sql .= "order by cont.co_categoria";

        return $this->consulta($sql);
    }

    /**
     * Relação de contratos com pagamentos em atraso
     *
     * @return STR
     */
    public function listarContratosComPagamentosEmAtraso($co_instituicao)
    {
        $sql = "select
                                    " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',
                                    p.ds_observacao   as 'Pendência',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Inicial',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Vl " . __('Global', true) . "',
                                    ifnull(DATE_FORMAT(p.dt_vencimento, '%d/%m/%Y'), '---') as 'Vencimento',
                                    ifnull(DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y'), '---') as 'Inicio',
                                    ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Fim',
                                    if(c.st_repactuado = 'S', 'Sim', 'Não')   as 'Repactuado',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Mensal'
                                from
                                    contratos c
                                LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                                LEFT JOIN aditivos      a on c.co_contrato = a.co_contrato
                                LEFT JOIN pagamentos    p on c.co_contrato = p.co_contrato
                                LEFT JOIN garantias     g on c.co_contrato = g.co_contrato
				WHERE p.dt_pagamento is null and p.dt_vencimento < NOW() and c.ic_ativo = 1 ";


        return $this->consulta($sql);
    }

    /**
     * Relação de contratos pagos por competência
     *
     * @return STR
     */
    public function listarContratosPagosCompetencia($mes, $ano, $co_instituicao)
    {
        $sql = " SELECT
                                    " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',
                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    INSERT ( ( INSERT( ( INSERT( ( INSERT(f.nu_cnpj, 3,0, '.') ), 7,0, '.') ), 11,0, '/' ) ), 16,0, '-') as 'CNPJ',
                                    ifnull(DATE_FORMAT(if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia), '%d/%m/%Y'), '---') as 'Inicio',
                                    ifnull(DATE_FORMAT(if(c.co_modalidade=1, c.dt_fim_processo , c.dt_fim_vigencia), '%d/%m/%Y'), '---') as 'Fim',
                                    ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(c.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(c.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Vl Mensal',
                                    Concat('R$ ', Replace (Replace (Replace (Format( SUM(p.vl_pagamento) , 2), '.', '|'), ',', '.'), '|', ',')) as 'Pg Competência'
                                from
                                    contratos c
                                        LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                    LEFT JOIN pagamentos p on p.co_contrato = c.co_contrato
                WHERE
                      p.dt_pagamento IS NOT NULL and c.ic_ativo = 1 ";

        if ($mes != 0) {
            $sql .= " and nu_mes_pagamento = $mes and p.nu_ano_pagamento = $ano
                group by c.co_contrato ";
        } else {
            $sql .= " and p.nu_ano_pagamento = $ano
                group by c.co_contrato ";
        }

        return $this->consulta($sql);
    }

    /**
     * Relação de liquidações por competência
     */
    public function liquidacaoCompetencia($mes, $ano)
    {
        if ($mes != '0') {
            $sql = "select
                        c.nu_processo as 'Número do processo',
                        c.nu_contrato as 'Número do contrato',
                        f.no_razao_social as 'Nome do fornecedor',
                        c.ds_objeto as 'Objeto',
                        c.dt_ini_vigencia as 'início da vigênciao',
                        c.dt_fim_vigencia as 'Fim da vigência',
                        c.vl_global as 'Valor global',
                        l.dt_liquidacao as 'Data da liquidação',
                        if(l.co_nota is not null,
                         Concat('', Replace (Replace (Replace (Format( n.vl_nota , 2), '.', '|'), ',', '.'), '|', ',')),
                         Concat('', Replace (Replace (Replace (Format( l.vl_liquidacao , 2), '.', '|'), ',', '.'), '|', ','))) as 'Valor da liquidação'
                    from
                        liquidacao as l
                        left join notas_fiscais as n on n.co_nota = l.co_nota
                        join contratos as c on c.co_contrato = l.co_contrato
                        join fornecedores as f on f.co_fornecedor = c.co_fornecedor
                    where
                        year(l.dt_liquidacao) = '$ano' and
                        month(l.dt_liquidacao) = '$mes'
                    group by
                        l.co_liquidacao";
        } else {
            $sql = "select
                        c.nu_processo as 'Número do processo',
                        c.nu_contrato as 'Número do contrato',
                        f.no_razao_social as 'Nome do fornecedor',
                        c.ds_objeto as 'Objeto',
                        c.dt_ini_vigencia as 'início da vigênciao',
                        c.dt_fim_vigencia as 'Fim da vigência',
                        c.vl_global as 'Valor global',
                        l.dt_liquidacao as 'Data da liquidação',
                        Concat('', Replace (Replace (Replace (Format( l.vl_liquidacao , 2), '.', '|'), ',', '.'), '|', ',')) as 'Valor da liquidação'
                    from liquidacao as l
                    join notas_fiscais as n
                    join contratos as c on n.co_contrato = c.co_contrato
                    join fornecedores as f
                    where
                      year(l.dt_liquidacao) = '$ano'
                    group by l.co_liquidacao";
        }

        return $this->consulta($sql);
    }

    /**
     * Relatórios de Contratos com Pendência
     * @item 51
     * @return STR
     */
    public function listarContratosComPendencia()
    {

        return $this->consulta("SELECT
                                " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato', 2) . ", '---') as 'Contrato',
                                ifnull(forn.no_razao_social, '---') as 'Fornecedor',
                                c.ds_objeto   as Objeto,
                                p.ds_pendencia as 'Pendência',
                                p.ds_observacao as 'Observação',
                                DATE_FORMAT(c.dt_ini_vigencia,'%d/%m/%Y') as 'Data Início',
                                DATE_FORMAT(c.dt_fim_vigencia,'%d/%m/%Y') as 'Data Fim',
                                s.ds_situacao as 'Situação',
                                DATEDIFF(NOW(), p.dt_inicio) as 'Dias em Pend.'
                                FROM contratos c
                                LEFT JOIN pendencias p on (c.co_contrato = p.co_contrato and p.st_pendencia = 'P')
                                left join fornecedores forn on (c.co_fornecedor = forn.co_fornecedor) 
                                left join situacoes s on (s.co_situacao = c.co_situacao) WHERE c.nu_pendencias > 0 and c.ic_ativo = 1 ");
    }

    /**
     * Relação de contratos derivados com pendência
     *
     * @return STR
     */
    public function listarContratosDerivadosComPendencia()
    {

        return $this->consulta("SELECT
                                if( co.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('co.nu_contrato', 'contrato', 2) . ", '---') as 'Contrato pai',
                                " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato', 2) . ", '---') as 'Contrato',
                                ifnull(forn.no_razao_social, '---') as 'Fornecedor',
                                c.ds_objeto   as Objeto,
                                p.ds_pendencia as 'Pendência',
                                DATE_FORMAT(c.dt_ini_vigencia,'%d/%m/%Y') as 'Data Início',
                                DATE_FORMAT(c.dt_fim_vigencia,'%d/%m/%Y') as 'Data Fim',
                                s.ds_situacao as 'Situação',
                                DATEDIFF(NOW(), p.dt_inicio) as 'Dias em Pend.'
                                FROM contratos c
                                LEFT JOIN contratos co on (c.co_contrato_pai is not null and c.co_contrato_pai = co.co_contrato)
                                LEFT JOIN pendencias p on (c.co_contrato = p.co_contrato and p.st_pendencia = 'P')
                                left join fornecedores forn on (c.co_fornecedor = forn.co_fornecedor)
                                left join situacoes s on (s.co_situacao = c.co_situacao) WHERE c.nu_pendencias > 0 and c.ic_ativo = 1 ");
    }

    /**
     * Relação de contratos em garantia
     *
     * @return STR
     */
    public function listarProcessosEmGarantia()
    {

        return $this->consulta("SELECT
                                " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',
                                if( c.vl_global = 0, '---', ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "',
                                ifnull(forn.no_razao_social, '---') as 'Fornecedor',
                                c.ds_objeto   as Objeto,
                                DATE_FORMAT(c.dt_ini_processo , '%d/%m/%Y') as 'Início',
                                DATE_FORMAT(c.dt_fim_processo , '%d/%m/%Y') as 'Fim',
                                DATE_FORMAT(DATE_ADD(c.dt_fim_processo, INTERVAL 90 DAY) , '%d/%m/%Y') as 'Término da Garantia'
                                FROM contratos c
                                left join fornecedores forn on (c.co_fornecedor = forn.co_fornecedor)
                                WHERE (DATE_ADD(c.dt_fim_processo, INTERVAL 91 DAY) > CURDATE()) and c.ic_ativo = 1 ");
    }

    /**
     * Relatório de contratos por custos
     *
     * @return STR
     */
    public function listarContratosCustos($ordem, $co_instituicao)
    {
        $ordem = up($ordem);

        $sql = "select
                                    " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',

                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    c.ds_objeto   as Objeto,
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Vl " . __('Global', true) . "',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Inicial',
                                    ifnull(DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y'), '---') as 'Inicio',
                                    ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Fim',
                                    if(c.st_repactuado = 'S', 'Sim', 'Não')   as 'Repactuado',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Mensal',

                                    if(c.vl_global>0,
                                    Replace (Replace (Replace (Format(
                                    ( c.vl_global +
                                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = c.co_contrato), 0 ) -
                                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = c.co_contrato ), 0 )
                                    ) , 2), '.', '|'), ',', '.'), '|', ','), '---') as 'Valor Restante (R$)'

                                from
                                    contratos c
                                LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                                where c.nu_contrato != '' and c.vl_global > 0 and dt_fim_vigencia > now() and c.ic_ativo = 1 ";

        $sql .= "ORDER BY c.vl_global $ordem ";

        return $this->consulta($sql);
    }

    /**
     * Relatório de contratos maiores custos
     *
     * @return STR
     *
     */
    public function listarContratosMaioresCustos()
    {
        return $this->consulta("select
                                    " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',

                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    c.ds_objeto   as Objeto,
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Global (R$)',
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Inicial (R$)',
                                    ifnull(DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y'), '---') as 'Inicio',
                                    ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Fim',
                                    if(c.st_repactuado = 'S', 'Sim', 'Não')   as 'Repactuado',
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Mensal (R$)',
                                    if(c.vl_global>0,
                                    Replace (Replace (Replace (Format(
                                    ( c.vl_global +
                                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = c.co_contrato), 0 ) -
                                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = c.co_contrato ), 0 )
                                    ) , 2), '.', '|'), ',', '.'), '|', ','), '---') as 'Valor Restante (R$)'
                                from
                                    contratos c
                                LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                                where (c.dt_fim_vigencia > now()) and c.ic_ativo = 1
				ORDER BY c.vl_global DESC limit 0,15");
    }

    /**
     * Relatório de contratos maiores custos
     *
     * @return STR
     *
     */
    public function listarContratosMenoresCustos()
    {
        return $this->consulta("select
                                    " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',

                                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                    c.ds_objeto   as Objeto,
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Global (R$)',
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Inicial (R$)',
                                    ifnull(DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y'), '---') as 'Inicio Vigencia',
                                    ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Fim Vigencia',
                                    if(c.st_repactuado = 'S', 'Sim', 'Não')  as 'Repactuado',
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---')  as 'Mensal (R$)',
                                    if(c.vl_global>0,
                                    Replace (Replace (Replace (Format(
                                    ( c.vl_global +
                                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = c.co_contrato), 0 ) -
                                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = c.co_contrato ), 0 )
                                    ) , 2), '.', '|'), ',', '.'), '|', ','), '---') as 'Valor Restante (R$)'
                                from
                                    contratos c
                                LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                                LEFT JOIN aditivos      a on c.co_contrato = a.co_contrato
                                LEFT JOIN pagamentos    p on c.co_contrato = p.co_contrato
                                LEFT JOIN garantias     g on c.co_contrato = g.co_contrato
                                where (c.dt_fim_vigencia > now()) and c.ic_ativo = 1
				ORDER BY c.vl_global limit 0,15");
    }

    /**
     * Relatório de Atividades com Pendência
     * @item 67
     * @return unknown_type
     */
    public function listarAtividadesPendentes()
    {
        $sql = "SELECT
                  atividades.ds_atividade AS 'Atividade',
                  atividades.pc_executado AS 'Percentual Executado',
                  DATE_FORMAT(atividades.dt_fim_planejado, '%d/%m/%Y') AS 'Data Final Prevista'
                FROM atividades
                WHERE atividades.ic_pendente = 1";
        return $this->consulta($sql);
    }

    /**
     * Relatório Lista Geral de Contratos/Processos
     * @item 44, 45, 47, 49, 59
     * @return unknown_type
     */
    public function listarContratos($coStatus = null, $coTipo = null, $coTipoContratacao = null, $anoExercicio = null, $coInstituicao = null, $exibirSoAtivos = false, $intervalo = null, $situacao = null, $setor = null, $ano = null)
    {
        App::import('Helper', 'Modulo');
        $this->modulo = new ModuloHelper();

        $filtro = $this->getFiltroPerfil('cont');

        $sqlLC = " select ";
        // $sqlLC .= "if(cont.co_fiscal_atual == null) as 'Fiscal', ";

        $sqlLC .= FunctionsComponent::exibeCampoSQL( "if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');

        $colContrato = array(
            null,
            2,
            3,
            4
        );

        $sqlLC .= "if( cont.nu_contrato != '',  " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato', ";

        if ($this->modulo->isCamposContrato('co_contratante_relatorio') == true) :
            $sqlLC .= " ifnull(unid.ds_setor, '---') as '" . __('Unidade Solicitante', true) . "', ";
        endif;

        $sqlLC .= " ifnull(concat(forn.nu_cnpj,' - ',forn.no_razao_social), '---') as '" . __('Fornecedor', true) . "', ";

        $sqlLC .= "
                    cont.ds_objeto as 'Objeto',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim',
                ";

        // if (in_array($coTipo, array(1, 2))) /** Depois precisa melhorar pois essa comparação não pode ser pelo código **/
        // $sqlLC .= " ifnull(DATE_FORMAT(DATE_ADD(cont.dt_fim_processo, INTERVAL 90 DAY) , '%d/%m/%Y'), '---') as 'Término da Garantia', ";

        if (in_array($coTipo, $colContrato))

            $sqlLC .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        if ($this->modulo->isCamposContrato('vl_mensal_relatorio') == true) :
            $sqlLC .= " if(cont.vl_mensal=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Mensal (R$)', ";
        endif;

        if ($this->modulo->isCamposContrato('vl_inicial') == true) :
            $sqlLC .= " if(cont.vl_inicial=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Inicial (R$)', ";
        endif;

        if ($this->modulo->isCamposContrato('vl_global_relatorio') == true) :
            $sqlLC .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "', ";
        else :
            $sqlLC .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')), '---')) as '" . __('Valor Global (R$)', true) . "' ";
        endif;

        if ($this->modulo->isCamposContrato('ds_nome_relatorio') == true) :
            $sqlLC .= "if(user.ds_nome != '', user.ds_nome , '---') as 'Fiscal', ";
        endif;

        if ($this->modulo->isCamposContrato('ds_situacao_relatorio') == true) :
            $sqlLC .= "if(situ.ds_situacao != '', situ.ds_situacao , '---') as 'Situação', ";
        endif;

        // $sqlLC .= "select sum(vl_empenho) from empenhos e where e.co_contrato = cont.co_contrato as 'Total Empenhado (R$)', ";

        if ($this->modulo->isCamposContrato('vl_restante_relatorio') == true) :
            $sqlLC .= " Replace (Replace (Replace (Format(
                    ( cont.vl_global +
                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = cont.co_contrato), 0 ) -
                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = cont.co_contrato ), 0 )
                    ) , 2), '.', '|'), ',', '.'), '|', ',') as 'Valor Restante (R$)', ";
        endif;

        if ($this->modulo->isCamposContrato('vl_empenhado_relatorio') == true) :
            $sqlLC .= " Replace (Replace (Replace (Format(
                    (
                        ifnull( (select sum(vl_empenho)  from empenhos e where e.co_contrato = cont.co_contrato), 0 )
                    ) , 2), '.', '|'), ',', '.'), '|', ',') as 'Total Empenhado (R$)' ";
        endif;

        $sqlLC .= "from contratos cont
                    LEFT JOIN fornecedores forn on (cont.co_fornecedor = forn.co_fornecedor)
                    left join setores unid on (cont.co_contratante = unid.co_setor)
                    LEFT JOIN usuarios user on (user.co_usuario = cont.co_fiscal_atual)
                    LEFT JOIN situacoes situ on (situ.co_situacao = cont.co_situacao)";

        $sqlLC .= " where nu_contrato != '' and cont.ic_ativo = 1";

        if ($filtro != '') {
            $sqlLC .= " and $filtro ";
        }

        if ($coStatus > 0) {
            $sqlLC .= " and cont.co_situacao = $coStatus ";
        }

        if ($coTipo > 0) {
            $sqlLC .= " and cont.co_modalidade = $coTipo ";
        }

        if ($coTipoContratacao > 0) {
            $sqlLC .= " and cont.co_contratacao = $coTipoContratacao ";
        }

        if ($coInstituicao > 0) {
            $sqlLC .= " and cont.co_instituicao = $coInstituicao ";
        }

        if ($anoExercicio > 0) {
            $sqlLC .= " and DATE_FORMAT(cont.dt_ini_vigencia, '%Y') = $anoExercicio ";
        }

        if ($exibirSoAtivos) {
            $sqlLC .= " and cont.dt_fim_vigencia >= date(NOW()) ";
        }

        if($intervalo != 0){
            $sqlLC .= " and cont.dt_fim_vigencia between curdate() and curdate() + interval " . $intervalo . " DAY";
        }

        if($situacao != 0){
            $sqlLC .= " and cont.co_situacao = " . $situacao;
        }

        if($setor != 0){
            $sqlLC .= " and cont.co_contratante = " . $setor;
        }

        if($ano != 0){
            $sqlLC .= " and DATE_FORMAT( cont.dt_assinatura ,  '%Y' ) = " . $ano;
        }

        $sqlLC .= " order by cont.nu_contrato ";

        return $this->consulta($sqlLC);
    }

    // listar contratos por mês
    public function listarContratosMes($mes = 0)
    {
        // $filtro = $this->getFiltro('cont');
        $where = ' WHERE c.nu_contrato is not null AND c.ic_ativo=1';

        if ($mes) {
            $where .= ' AND CONCAT("0", MONTH(c.dt_ini_vigencia)) = ' . $mes;
        }

        $query = 'SELECT c.nu_processo as Processo, c.nu_contrato as Contrato, c.ds_objeto as Objeto,
        MONTHNAME_PT(MONTH(c.dt_ini_vigencia)) as Mês, YEAR(c.dt_ini_vigencia) as Ano FROM contratos c ' . $where . ' order by Mês';


        return $this->consulta($query);
        // exit();
    }

    /* --- */
    public function listarProcessos()
    {
        $filtro = $this->getFiltroPerfil('cont');
        $sqlLP = " select ";
        // $sqlLP .= FunctionsComponent::exibeCampoSQL( "if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');

        $sqlLP .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');
        $sqlLP .= " if( cont.nu_contrato != '', " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato', ";

        $sqlLP .= " ifnull(unid.ds_setor, '---') as '" . __('Unidade Solicitante', true) . "',
                    ifnull(forn.no_razao_social, '---') as '" . __('Fornecedor', true) . "',
                    cont.ds_objeto as 'Objeto',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim',
                ";

        $sqlLP .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        $sqlLP .= " if(cont.vl_mensal=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Mensal (R$)', ";

        $sqlLP .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "', ";

        $sqlLP .= " if(cont.vl_global>0,
                    Replace (Replace (Replace (Format(
                    ( cont.vl_global +
                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = cont.co_contrato), 0 ) -
                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = cont.co_contrato ), 0 )
                    ) , 2), '.', '|'), ',', '.'), '|', ','), '---') as 'Valor Restante (R$)'
                    from contratos cont
                    LEFT JOIN fornecedores forn on (cont.co_fornecedor = forn.co_fornecedor)
                    left join setores unid on (cont.co_contratante = unid.co_setor) ";

        $sqlLP .= " where cont.nu_processo != '' and cont.ic_ativo";

        if ($filtro != '') {
            $sqlLP .= " and $filtro ";
        }

        $sqlLP .= " and (cont.dt_fim_processo > NOW() or cont.dt_fim_processo is null) and (cont.nu_contrato = '' or cont.nu_contrato is null)";

        $sqlLP .= " order by cont.nu_processo ";

        return $this->consulta($sqlLP);
    }

    /* --- */
    public function listarCis()
    {
        $filtro = $this->getFiltroPerfil('cont');
        $sqlLP = " select ";

        $sqlLP .= "if( cont.nu_pam != '', cont.nu_pam, '---' ) as 'CI', ";
        $sqlLP .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');
        $sqlLP .= "if( cont.nu_contrato != '', " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato', ";

        $sqlLP .= " ifnull(unid.ds_setor, '---') as '" . __('Unidade Solicitante', true) . "',
                    ifnull(cont.tp_aquisicao, '---') as '" . __('Tipo de Aquisição', true) . "'";

        $sqlLP .= " from contratos cont
                    LEFT JOIN setores unid on (cont.co_contratante = unid.co_setor) ";

        $sqlLP .= " where cont.nu_pam != '' and cont.dt_cadastro_pam is not null ";

        if ($filtro != '') {
            $sqlLP .= " and $filtro ";
        }

        $sqlLP .= " order by cont.nu_pam asc ";

        return $this->consulta($sqlLP);
    }

    /* --- */
    public function listarCiPorFase($co_fase)
    {
        $filtro = $this->getFiltroPerfil('cont');
        $sqlLP = " select ";

        $sqlLP .= "if( cont.nu_pam != '', cont.nu_pam, '---' ) as 'CI', ";
        $sqlLP .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');
        $sqlLP .= "if( cont.nu_contrato != '', " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato', ";

        $sqlLP .= " ifnull(unid.ds_setor, '---') as '" . __('Unidade Solicitante', true) . "',
                    ifnull(cont.tp_aquisicao, '---') as '" . __('Tipo de Aquisição', true) . "'";

        $sqlLP .= " from contratos cont
                    LEFT JOIN setores unid on (cont.co_contratante = unid.co_setor) ";

        $sqlLP .= " where cont.nu_pam != '' and cont.dt_cadastro_pam is not null and cont.co_fase = " . $co_fase . " ";

        if ($filtro != '') {
            $sqlLP .= " and $filtro ";
        }

        $sqlLP .= " order by cont.nu_pam asc ";

        return $this->consulta($sqlLP);
    }

    // atestos relatorios
    public function contratosAtestos()
    {
        $query = 'select c.nu_contrato as "Contrato", e.nu_empenho as "Empenho", n.nu_nota as "Nº da nota", date_format(n.dt_nota, "%d/%m/%Y") as "Data nota", date_format(n.dt_atesto, "%d/%m/%Y") as "Data atesto" from contratos c join empenhos e on (e.co_contrato = c.co_contrato) join notas_fiscais n on (n.co_contrato = c.co_contrato) where c.nu_contrato is not null';

        return $this->consulta($query);
    }

    public function listarContratosProcessos($coStatus = 0, $coTipoContratacao = 0, $anoExercicio = 0)
    {
        $filtro = $this->getFiltroPerfil('cont');
        $sqlLCP = " select ";
        $sqlLCP .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');

        $colContrato = array(
            null,
            2,
            3,
            4
        );

        if (in_array($coTipo, $colContrato))
            $sqlLCP .= "if( cont.nu_contrato != '', " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . ", '---') as 'Contrato', ";

        $sqlLCP .= " ifnull(unid.ds_setor, '---') as '" . __('Unidade Solicitante', true) . "',
                    ifnull(forn.no_razao_social, '---') as '" . __('Fornecedor', true) . "',
                    cont.ds_objeto as 'Objeto',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim',
                ";

        if (in_array($coTipo, array(
            1,
            2
        )))
            $sqlLCP .= " ifnull(DATE_FORMAT(DATE_ADD(cont.dt_fim_processo, INTERVAL 90 DAY) , '%d/%m/%Y'), '---') as 'Término da Garantia', ";

        if (in_array($coTipo, $colContrato))
            $sqlLCP .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        $sqlLCP .= " if(cont.vl_mensal=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Mensal (R$)', ";

        $sqlLCP .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "', ";

        $sqlLCP .= " if(cont.vl_global>0,
                    Replace (Replace (Replace (Format(
                    ( cont.vl_global +
                        ifnull( (select sum(vl_aditivo)  from aditivos a where a.co_contrato = cont.co_contrato), 0 ) -
                        ifnull( (select sum(vl_pagamento) from pagamentos p where p.dt_pagamento is not null and p.co_contrato = cont.co_contrato ), 0 )
                    ) , 2), '.', '|'), ',', '.'), '|', ','), '---') as 'Valor Restante (R$)'

                    from contratos cont
                    LEFT JOIN fornecedores forn on cont.co_fornecedor = forn.co_fornecedor
                    left join setores unid on (cont.co_contratante = unid.co_setor) ";

        $sqlLCP .= " where nu_processo != '' and cont.ic_ativo = 1 ";

        if ($filtro != '') {
            $sqlLCP .= " and $filtro ";
        }

        // if($anoExercicio == null) {
        // $sqlLC .= " and dt_fim_vigencia > NOW() ";
        // }

        if ($coStatus > 0) {
            $sqlLCP .= " and co_situacao = $coStatus ";
        }

        if ($coTipo > 0) {
            $sqlLCP .= " and co_modalidade = $coTipo ";
        }

        if ($coTipoContratacao > 0) {
            $sqlLCP .= " and co_contratacao = $coTipoContratacao ";
        }

        if ($anoExercicio > 0) {
            $sqlLCP .= " and DATE_FORMAT(dt_ini_vigencia, '%Y') = $anoExercicio ";
        }

        $sqlLCP .= " order by cont.nu_processo ";

        return $this->consulta($sqlLCP);
    }

    /* --- */
    public function contratosAPagar()
    {
        $sql = " SELECT
				distinct c.co_contrato as 'Nº',
                                " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',

				ifnull(f.no_razao_social, '---') as 'Fornecedor',
                                c.ds_objeto   as Objeto,
				ifnull(c.tp_valor, '---')    as 'Tipo Vlr',
                                '' as 'Janeiro',
                                '' as 'Fevereiro',
                                '' as 'Março',
                                '' as 'Abril',
                                '' as 'Maio',
                                '' as 'Junho',
                                '' as 'Julho',
				m.ds_modalidade as 'Tipo',
				ifnull(DATE_FORMAT(if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia), '%d/%m/%Y'), '---') as 'Inicio',
                                ifnull(DATE_FORMAT(if(c.co_modalidade=1, c.dt_fim_processo , c.dt_fim_vigencia), '%d/%m/%Y'), '---') as 'Fim',
				ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(c.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(c.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência',
				s.ds_situacao as 'Situação',
                                c.ds_observacao  as 'Obs.'
                            from
                                contratos c
                                    LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                                    LEFT JOIN modalidades   m on c.co_modalidade = m.co_modalidade
                                    LEFT JOIN situacoes     s on c.co_situacao = s.co_situacao
                                    LEFT JOIN pagamentos    p on p.co_contrato = c.co_contrato
                            WHERE p.dt_pagamento IS NULL and c.ic_ativo = 1 and
                                    p.dt_vencimento between '2013-01-01' and '2013-07-31'";

        $resultado = $this->consulta($sql);
        if (count($resultado)) {
            // $valor = 100;
            $jan = 0;
            $fev = 0;
            $mar = 0;
            $abr = 0;
            $mai = 0;
            $jun = 0;
            $jul = 0;
            for ($i = 0; $i < count($resultado); $i++, $i++) {
                $resultado[$i]['RELATORIO']['Janeiro'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 01);
                $jan += ln($resultado[$i]['RELATORIO']['Janeiro']);
                $resultado[$i]['RELATORIO']['Fevereiro'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 02);
                $fev += ln($resultado[$i]['RELATORIO']['Fevereiro']);
                $resultado[$i]['RELATORIO']['Março'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 03);
                $mar += ln($resultado[$i]['RELATORIO']['Março']);
                $resultado[$i]['RELATORIO']['Abril'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 04);
                $abr += ln($resultado[$i]['RELATORIO']['Abril']);
                $resultado[$i]['RELATORIO']['Maio'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 05);
                $mai += ln($resultado[$i]['RELATORIO']['Maio']);
                $resultado[$i]['RELATORIO']['Junho'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 06);
                $jun += ln($resultado[$i]['RELATORIO']['Junho']);
                $resultado[$i]['RELATORIO']['Julho'] = $this->getValorAPagar($resultado[$i]['RELATORIO']['Nº'], 2013, 07);
                $jul += ln($resultado[$i]['RELATORIO']['Julho']);
            }
            $resultado[] = array(
                'RELATORIO' => array(
                    'A' => '',
                    'Nº Processo' => '<B>TOTAL</B>',
                    'B' => '',
                    'C' => '',
                    'D' => '',
                    'E' => '',
                    'JAN' => "<b>" . vlReal($jan) . "</b>",
                    'FEV' => "<b>" . vlReal($fev) . "</b>",
                    'MAR' => "<b>" . vlReal($mar) . "</b>",
                    'ABR' => "<b>" . vlReal($abr) . "</b>",
                    'MAI' => "<b>" . vlReal($mai) . "</b>",
                    'JUN' => "<b>" . vlReal($jun) . "</b>",
                    'JUL' => "<b>" . vlReal($jul) . "</b>",
                    'F' => '',
                    'G' => '',
                    'H' => '',
                    'I' => '',
                    'J' => '',
                    'K' => ''
                )
            );
        }
        return $resultado;
    }

    public function getValorAPagar($co_contrato, $ano, $mes)
    {
        $vl_total = $this->query("
                    select ifnull( Concat('R$ ', Replace (Replace (Replace (Format( SUM(vl_pagamento) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as vl_total
                       from pagamentos
                    where co_contrato = $co_contrato and DATE_FORMAT(dt_vencimento, '%Y') = $ano and DATE_FORMAT(dt_vencimento, '%m') = $mes ");

        return $vl_total[0][0]['vl_total'];
    }

    public function getPagamentosAPagar($co_contrato, $mes_ano)
    {
        return $this->query("
                    select distinct
                                     if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Número do Processo',
                                     if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Número do Contrato',
                                     c.ds_objeto as 'Objeto', DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as 'Data Início', DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y') as 'Data Fim'
                       from contratos c join pagamentos p on ( p.co_contrato = c.co_contrato )
                    where DATE_FORMAT(dt_ini_vigencia, '%Y') = $ano");
    }

    public function listarContratosParaPagamento($ano)
    {
        return $this->consulta("
                    select distinct  if( c.nu_processo != '', " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',
                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',

                    ifnull(f.no_razao_social, '---') as 'Fornecedor',
                    c.ds_objeto as 'Objeto',
                    DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as 'Data Início',
                    ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Data Fim'
                       from contratos c
                        LEFT JOIN fornecedores  f on c.co_fornecedor = f.co_fornecedor
                        join pagamentos p on ( p.co_contrato = c.co_contrato )
                    where c.co_situacao = 11 and c.ic_ativo = 1 and DATE_FORMAT(c.dt_ini_vigencia, '%Y') = $ano");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarFornecedores()
    {
        return $this->consulta("select
                                    ifnull(f.no_razao_social, '---') as '" . __('Fornecedor', true) . "',
                                    if(f.tp_fornecedor = 'J', mask(f.nu_cnpj, '##.###.###/####-##'), mask(f.nu_cnpj, '###.###.###-##') ) as 'CNPJ / CPF',
                                    (SELECT ds_municipio FROM municipios WHERE co_municipio = f.co_municipio) as 'Cidade',
                                    if(f.sg_uf = '', '---', f.sg_uf)             as 'UF',
                                    if(f.nu_telefone='', '---', f.nu_telefone)       as 'Telefone',
                                    if(f.no_responsavel='', '---', f.no_responsavel)    as '" . __("Responsável", true) . "'
                                from
                                    fornecedores f ");
    }

    /*
     * retorna os anos de acordo com os contratos cadastrados no sistema
     */
    public function anosContratos()
    {
        $anos = $this->consulta("SELECT YEAR(dt_publicacao ) as anos FROM contratos WHERE dt_publicacao IS NOT NULL AND YEAR(dt_publicacao) != 0 GROUP BY YEAR(dt_publicacao) ORDER BY anos DESC");
        $result = array();
        foreach ($anos as $z) {
            foreach ($z as $y) {
                foreach ($y as $x) {
                    $result[$x] = $x;

                }
            }
        }
        return $result;
    }

    /**
     *
     * @return unknown_type
     */
    public function listarEmpenhos($contrato, $co_instituicao)
    {
        $sql = "select

                                            if( c.nu_processo != '', " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',
                                            if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . ", '---') as 'Contrato',
                                            e.nu_empenho   as 'Empenho',
                                            if(e.tp_empenho='O','Original', if( e.tp_empenho='R','Reforço', 'Anulação' ) ) as 'Tipo',
                                            e.ds_empenho   as 'Descrição',
                                            ifnull( Concat('R$ ', Replace (Replace (Replace (Format( e.vl_empenho , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor',
                                            DATE_FORMAT(e.dt_empenho, '%d/%m/%Y') as 'Data'
                                        from
                                            contratos c
                                        join empenhos e on e.co_contrato = c.co_contrato
                                        where c.nu_contrato = '$contrato' and c.ic_ativo = 1 ";

        return $this->consulta($sql);
    }

    /**
     * Relatório Lista de Empenhos por Contrato/Processo
     * @item 48
     * @param $co_instituicao
     * @return unknown_type
     */
    public function listarEmpenhosContratosAtivos($co_instituicao)
    {
        $sql = "select

                                    if( c.nu_processo != '', " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                    e.nu_empenho   as 'Empenho',
                                    if(e.tp_empenho='O','Original', if( e.tp_empenho='R','Reforço', 'Anulação' ) ) as 'Tipo',
                                    e.ds_empenho   as 'Descrição',
                                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( e.vl_empenho , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor',
                                    DATE_FORMAT(e.dt_empenho, '%d/%m/%Y') as 'Data'
                                from
                                    contratos c
                                join empenhos e on e.co_contrato = c.co_contrato
                                where c.dt_fim_vigencia >= date(NOW()) and c.ic_ativo = 1
                                    and e.co_empenho = (select e.co_empenho from empenhos e where e.co_contrato = c.co_contrato order by e.dt_empenho DESC limit 1) ";

        $sql .= "order by e.dt_empenho DESC";
        // debug($sql);die;
        return $this->consulta($sql);
    }

    /**
     * Relatório Lista de Fiscais por Contrato
     * @item 46
     * @return unknown_type
     */
    public function listarFiscaisPorContrato()
    {
        return $this->consulta("select
                                            if( c.nu_processo != '', " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',
                                            if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                            ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                            c.ds_objeto   as 'Objeto' ,
                                            ifnull(s.ds_nome, '---')   as 'Nome " . __('Fiscal', true) . "'
                                        from
                                            contratos_fiscais cf
                                                LEFT JOIN contratos c ON c.co_contrato = cf.co_contrato
                                                LEFT JOIN fornecedores  fd ON c.co_fornecedor = fd.co_fornecedor
                                                LEFT JOIN usuarios s ON cf.co_usuario_fiscal = s.co_usuario
                                        where c.ic_ativo = 1 order by cf.co_usuario_fiscal");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarUsuariosPorSetor($co_setor)
    {
        $query = null;

        if (isset($co_setor)) {
            $query = $this->consulta("select user.ds_nome AS NOME, setor.ds_setor as setor from usuarios user
                                        left join setores setor ON setor.co_setor = user.co_setor
                                        where user.ds_nome != '' AND setor.ds_setor != '' AND setor.co_setor = '$co_setor' AND user.ic_acesso = 1 ORDER BY user.ds_nome");
        } else
            if (empty($co_setor)) {
                $query = $this->consulta("select user.ds_nome AS NOME, setor.ds_setor as setor from usuarios user
                                  left join setores setor ON setor.co_setor = user.co_setor
                                    where user.ds_nome != '' AND setor.ds_setor != '' AND user.ic_acesso = 1 ORDER BY user.ds_nome");
            }

        return $query;
    }

    /**
     *
     * @return unknown_type
     */
    public function listarUsuariosPorInstituicao($co_instituicao)
    {
        $query = null;

        if (isset($co_instituicao)) {
            $query = $this->consulta("SELECT u.ds_nome AS NOME, i.ds_instituicao AS INSTITUICAO FROM usuarios u
                                LEFT JOIN instituicoes i ON u.co_instituicoes = i.co_instituicao
                                WHERE i.ds_instituicao != '' AND i.co_instituicao = '$co_instituicao'");
        } else
            if (empty($co_instituicao)) {
                $query = $this->consulta("SELECT u.ds_nome AS NOME, i.ds_instituicao AS INSTITUICAO FROM usuarios u
                                            LEFT JOIN instituicoes i ON u.co_instituicoes = i.co_instituicao
                                            WHERE i.ds_instituicao != ''");
            }

        return $query;
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratosExpirar($dia, $mes, $ano, $dia2, $mes2, $ano2, $coStatus = false)
    {
        $sel1 = $ano . "-" . $mes . "-" . $dia;
        $sel2 = $ano2 . "-" . $mes2 . "-" . $dia2;

        $sql = "SELECT
                                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                        ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                        ifnull(fd.nu_cnpj, '---') as 'CNPJ',
                                        c.ds_objeto   as 'Objeto' ,
                                        DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as 'Inicio de Vigencia',
                                        DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y') as 'Fim de Vigencia',
                                        ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor global (R$)',
                                        ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(c.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(NOW(), '%Y%m')), ' mês(es)'), '---') as 'Expiração'
                                    FROM
                                        contratos c
                                        LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
                                    where ( c.dt_fim_vigencia between '$sel1' and '$sel2' ) and c.ic_ativo = 1 ";
        if ($coStatus) {
            $sql .= " and c.dt_fim_vigencia > NOW() ";
        }

        $sql .= " ORDER BY c.nu_contrato ASC ";

        return $this->consulta($sql);
    }

    // historicos por período
    public function listarHistoricosPorPeriodo($dia, $mes, $ano, $dia2, $mes2, $ano2)
    {
        $sel1 = $ano . "-" . $mes . "-" . $dia;
        $sel2 = $ano2 . "-" . $mes2 . "-" . $dia2;

        $sql = "SELECT
                        if( c.nu_pam != '',  " . FunctionsComponent::getMaskSQL('c.nu_pam', 'pam') . ", '---') as '" . __('PAM', true) . "',
                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                        ifnull(fd.no_razao_social, if( c.nu_pam != '',  c.ds_observacao, '---')) as '" . __('Fornecedor', true) . "',
                        h.no_assunto as 'Assunto' ,
                        DATE_FORMAT(h.dt_historico, '%d/%m/%Y') as 'Data',
                        h.ds_observacao as 'Observação' ,
                        u.ds_nome as 'Responsável'
                    FROM
                        contratos c
                        LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
                        LEFT JOIN historicos    h on c.co_contrato = h.co_contrato
                        LEFT JOIN usuarios      u on h.co_usuario = u.co_usuario";

        if ($sql1 && $sql2) {
            $sql .= " where ( h.dt_historico between $sel1 and $sel2 )";
        }

        $sql .= ' and c.ic_ativo = 1';

        $sql .= " ORDER BY h.dt_historico ASC ";

        return $this->consulta($sql);
    }

    public function listarHistoricosPorTipoOcorrencia($tipoOcorrencia)
    {
        $sql = "SELECT
            if( c.nu_pam != '',  " . FunctionsComponent::getMaskSQL('c.nu_pam', 'pam') . ", '---') as '" . __('PAM', true) . "'," . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
            if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
            ifnull(fd.no_razao_social, if( c.nu_pam != '',  c.ds_observacao, '---')) as '" . __('Fornecedor', true) . "',
            h.no_assunto as 'Assunto',
            h.tipo as 'Tipo de Ocorrência',
            DATE_FORMAT(h.dt_historico, '%d/%m/%Y') as 'Data',
            h.ds_observacao as 'Observação' ,
            u.ds_nome as 'Responsável'
        FROM
            contratos c
            LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
            LEFT JOIN historicos    h on c.co_contrato = h.co_contrato
            LEFT JOIN usuarios      u on h.co_usuario = u.co_usuario";

        if ($tipoOcorrencia) {
            $sql .= " where '{$tipoOcorrencia}' = h.tipo";
        }

        $sql .= ' and c.ic_ativo = 1';

        $sql .= " ORDER BY h.dt_historico ASC ";
        return $this->consulta($sql);
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratosValoresGlobais($cpvl1, $cpvl2)
    {
        return $this->consulta("select
                                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                        ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                        ifnull(fd.nu_cnpj, '---') as 'CNPJ',
                                        c.ds_objeto   as 'Objeto' ,
                                        ifnull(DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y'), '---') as 'Inicio de Vigencia',
                                        ifnull(DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y'), '---') as 'Fim de Vigencia',
                                        ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as '" . __('Vlr Global (R$)', true) . "'
                                    from
                                        contratos c
                                        LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
                                    where c.vl_global >= '$cpvl1'
                                    and   c.vl_global <= '$cpvl2'
                                    and c.ic_ativo = 1
                                    order by c.vl_global ASC");
    }

    /**
     * Relatório Lista de Contratos por Fiscais
     * @item 53
     * @return unknown_type
     */
    public function listarContratoPorFiscais()
    {
        return $this->consulta("select
                                            s.ds_nome   as 'Nome " . __('Fiscal', true) . "',
                                            c.ds_objeto   as 'Objeto' ,
                                            if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Número do Contrato'
                                        from
                                            contratos c
                                        left join usuarios s on c.co_fiscal_atual = s.co_usuario
                                        where c.nu_contrato != '' and c.ic_ativo = 1 and c.co_fiscal_atual is not null and c.dt_fim_vigencia > now() order by s.ds_nome
                                        ");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratoRespectivoAditivo()
    {

        return $this->consulta("select
                                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',

                                        ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                            c.ds_objeto   as 'Objeto',
                                            a.ds_aditivo  as 'Justificativa Aditivo',
                                            DATE_FORMAT(a.dt_aditivo, '%d/%m/%Y') as 'Data Aditivos',
                                            ifnull( Concat('', Replace (Replace (Replace (Format( a.vl_aditivo , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor do aditivo (R$)'
                                        from
                                            contratos c
                                        LEFT JOIN aditivos   a on c.co_contrato = a.co_contrato
                                        LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
					WHERE a.vl_aditivo and c.ic_ativo = 1 ");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratosComGarantias()
    {
        return $this->consulta("select
                                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                            c.ds_objeto   as 'Objeto',

                                            g.no_seguradora as 'Nome da Seguradora',
                                            g.ds_modalidade as 'Modalidade',
                                            ifnull( Concat('', Replace (Replace (Replace (Format( g.vl_garantia , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor (R$)',
                                            DATE_FORMAT(g.dt_inicio, '%d/%m/%Y') as 'Início',
                                            DATE_FORMAT(g.dt_fim, '%d/%m/%Y') as 'Fim'
					from
                                            contratos c
					left join garantias     g on c.co_contrato = g.co_contrato
					WHERE g.vl_garantia and c.ic_ativo = 1 ");
    }

    /**
     * Relatório Lista de Contratos sem Garantias
     * @item 50
     * @return unknown_type
     */
    public function listarContratosSemGarantias()
    {

        return $this->consulta("select
                                    if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Numero do Contrato',
                                    c.ds_objeto   as 'Objeto',
                                    DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as 'Inicio de Vigencia',
                                    DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y') as 'Fim de Vigencia',
                                    ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_inicial , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor Inicial (R$)'
                                    from
                                            contratos c
                                    left join garantias     g on c.co_contrato = g.co_contrato
                                    WHERE g.vl_garantia is null and c.ic_ativo = 1 ");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratosPorVigencia($categoria)
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        $sql = "SELECT ";
        $sql .= "" . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . " ";
        $sql .= "if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato', ";

        if (!isset($categoria) && $modulo->isCamposContrato('co_categoria'))
            $sql .= "ct.no_categoria as 'Categoria', ";

        $sql .= "ifnull(fd.no_razao_social, '---') as 'Fornecedor', ";
        $sql .= "ifnull(fd.nu_cnpj, '---') as 'CNPJ', ";
        $sql .= "c.ds_objeto   as 'Objeto', ";
        $sql .= "DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as 'Data Inicio', ";
        $sql .= "DATE_FORMAT(c.dt_fim_vigencia, '%d/%m/%Y') as 'Data Fim', ";
        $sql .= "ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(c.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(c.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";
        $sql .= "ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor mensal (R$)', ";
        $sql .= "ifnull( Concat('', Replace (Replace (Replace (Format( c.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor global (R$)' ";
        $sql .= "FROM contratos c  ";
        $sql .= "LEFT JOIN fornecedores fd on c.co_fornecedor = fd.co_fornecedor ";
        $sql .= "LEFT JOIN categorias ct on c.co_categoria = ct.co_categoria ";
        $sql .= "where c.dt_fim_vigencia is not null and c.dt_fim_vigencia >= NOW() and c.ic_ativo = 1 ";

        if (isset($categoria))
            $sql .= " and c.co_categoria = {$categoria} ";

        $sql .= "ORDER BY c.dt_fim_vigencia";

        return $this->consulta($sql);
    }

    /**
     *
     * @return unknown_type
     */
    public function listarQuantidadeContratoMensal($ano)
    {
        return $this->consulta("select
                                    DATE_FORMAT(if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia), '%m') as 'Mês',
                                    count(c.co_contrato) as 'Quantidade de Contratos'
                                from
                                    contratos c
                                where DATE_FORMAT(if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia), '%Y') = $ano
                                group by 1 UNION
                            SELECT 'Total' as 'Mês', COUNT( c.co_contrato ) AS  'Quantidade de Contratos'
                            FROM contratos c
                            WHERE DATE_FORMAT( IF( c.co_modalidade =1, c.dt_ini_processo, c.dt_ini_vigencia ) ,  '%Y' ) = $ano and c.ic_ativo = 1 order by 1 ASC
                ");
    }

    /**
     * Relatório de Contratos com Pagamento em Atraso
     * @item 54
     * @return unknown_type
     */
    public function listarQuantidadePagamentoAtraso()
    {
        $sql_contratos = " SELECT co_contrato total FROM contratos where co_situacao <> (13 or 14)";
        $total_contratos = $this->query(" SELECT count(co_contrato) total from contratos where co_contrato in ( " . $sql_contratos . " ) ");
        $total_atraso = $this->query(" select count( distinct co_contrato ) total from pagamentos where co_contrato in ( " . $sql_contratos . " ) and dt_pagamento is null and dt_vencimento < NOW() ");

        $relatorio = array();
        $relatorio[0]['RELATORIO']['Quantidade Contratos'] = $total_contratos[0][0]['total'];
        $relatorio[0]['RELATORIO']['Em Atraso'] = $total_atraso[0][0]['total'];
        $relatorio[0]['RELATORIO']['%'] = $total_atraso[0][0]['total'] * 100 / $total_contratos[0][0]['total'];
        return $relatorio;
    }

    /**
     * Relatório de Contratos com Pagamento em Atraso
     * @return unknown_type
     */
    public function contratosPagamentosAtraso()
    {
        return $this->consulta("
            SELECT contratos.nu_contrato 'Número do Contrato',
            pagamentos.nu_nota_fiscal as 'Número da Nota',
            DATE_FORMAT(dt_vencimento,'%d/%m/%Y') as 'Vencimento'
            FROM pagamentos
            INNER JOIN contratos ON pagamentos.co_contrato = contratos.co_contrato
            where dt_pagamento is null and dt_vencimento < NOW()
            ");
    }

    /**
     * Relatório de Contratos por Tipo
     * @item 58
     * @return unknown_type
     */
    public function listarQtdContratoPorTipo()
    {
        return $this->consulta("
                SELECT 
                      m.ds_modalidade 'Tipo de Contrato', 
                      COUNT(c.co_modalidade) * total.fator AS '%',
                      IFNULL(CONCAT('R$', FORMAT(SUM(c.vl_global), 2, 'de_DE')),'---') AS 'Valor global'
                FROM contratos c
                JOIN (SELECT 100/count(*) AS fator FROM contratos WHERE co_modalidade IS NOT NULL ) AS total
           LEFT JOIN modalidades m 
                  ON m.co_modalidade = c.co_modalidade
               WHERE c.co_modalidade IS NOT NULL 
               GROUP BY c.co_modalidade");
    }

    /**
     * Relatório de Contratos Celebrados Anualmente
     * @item 57
     * @param $ano
     * @return unknown_type
     */
    public function listarQtdContratoPorStatus($ano)
    {
        return $this->consulta("
                select s.ds_situacao as 'Situação', count(distinct(c.co_contrato)) as 'Quantidade', count(distinct(c.co_contrato))/(select count(c.co_contrato) from situacoes s join contratos c on c.co_situacao = s.co_situacao where year(c.dt_ini_vigencia) = " . $ano . ") as 'Porcentagem'
	              from situacoes s
                  join contratos c on c.co_situacao = s.co_situacao
                  where year(c.dt_ini_vigencia) = " . $ano . "
                  group by s.ds_situacao");
    }

    public function getAnosContrato($where = '')
    {
        return $this->query("SELECT distinct DATE_FORMAT(dt_ini_vigencia, '%Y') as Ano
                FROM contratos where dt_ini_vigencia is not null order by Ano DESC");
    }

    /**
     * Relatório de Contratos Encaminhados para Pagamento por Ano
     * @item 55
     * @return unknown_type
     */
    public function listarPcContratosPagamentoPorAno()
    {
        return $this->consulta("
                select DATE_FORMAT(if(co_modalidade=1, dt_ini_processo , dt_ini_vigencia), '%Y') as 'Ano',
                       ( 100 * count( distinct(p.co_contrato) ) ) / count( distinct(c.co_contrato) ) as '%'
                   from contratos c left join pagamentos p on ( p.co_contrato = c.co_contrato )
                where if(co_modalidade=1, dt_ini_processo , dt_ini_vigencia) is not null group by Ano order by Ano DESC");
    }

    /**
     *
     * @return unknown_type
     */
    public function listarQuantidadeContratoExercicio()
    {
        return $this->consulta("SELECT DATE_FORMAT(if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia), '%Y') as 'Ano',
                                       count(c.co_contrato) * total.fator as '%',
                                       count(c.co_contrato) as 'Numero de Contratos'
                FROM contratos c
                   join (SELECT 100/count(*) as fator from contratos where if(co_modalidade=1, dt_ini_processo , dt_ini_vigencia) is not null ) as total
                where if(c.co_modalidade=1, c.dt_ini_processo , c.dt_ini_vigencia) is not null
                                group by Ano order by Ano DESC");
    }

    // public function filtro($data) {
    //
    // if ($data['colunas']) {
    // foreach($data['colunas'] as $key => $coluna){
    // if(is_numeric($key))
    // $cols .= ' (SELECT vl_pagamento FROM pagamentos WHERE pagamentos.co_contrato = cont.co_contrato AND nu_mes_pagamento = ' . $key . ') as \'' . $coluna . '\' , ';
    // else
    // $cols .= ' ' . $key . ' , ';
    // }
    // } else {
    // $cols .= ' * , ';
    // }
    //
    // $where = 'WHERE';
    //
    // if ($data['especie'])
    // $where .= ' cont.' . $data['especie'] . ' = ' . trim($data[$data['especie']]) . ' AND ';
    //
    // if ($data['situacao'] == '1')
    // $where .= ' (SELECT count(*) FROM pagamentos pag WHERE pag.co_contrato = cont.co_contrato) > 0 AND ';
    //
    // if ($data['ano'])
    // $where .= ' RIGHT(cont.nu_processo, 4) = ' . $data['ano'] . ' AND ';
    //
    // if ($data['tipo'])
    // $where .= ' cont.co_modalidade = ' . $data['tipo'] . ' AND ';
    //
    // if ($data['co_categoria'])
    // $where .= ' cont.co_categoria = ' . $data['co_categoria'] . ' AND ';
    //
    // $sql .= "SELECT ";
    // $sql .= substr($cols, 0, -2);
    // $sql .= "FROM ";
    // $sql .= "contratos cont ";
    // $sql .= "LEFT JOIN fornecedores forn on cont.co_fornecedor = forn.co_fornecedor ";
    // $sql .= substr($where, 0, -5);
    //
    // //echo $sql; die;
    //
    // $contratos = $this->consulta($sql);
    //
    // //var_dump($contratos); die;
    //
    // return $contratos;
    // }
    public function filtro($data, $colunas)
    {

        // Lipa colunas
        unset($colunas['total']);

        // Monta as colunas
        foreach ($colunas as $nomeColuna => $infoColuna) {
            if (is_numeric($nomeColuna)) {
                $cols = ' (SELECT sum(vl_pagamento) FROM pagamentos WHERE ';
                $cols .= ' nu_ano_pagamento = ' . $data['ano'] . ' AND ';
                $cols .= ' pagamentos.co_contrato = cont.co_contrato AND ';
                $cols .= ' nu_mes_pagamento = ' . $nomeColuna . ') as \'' . $nomeColuna . '\', ';
            } else
                $cols .= $infoColuna['tabela'] . $nomeColuna . ', ';
        }

        $where = 'WHERE';

        if ($data['especie']) {
            $campos = explode('-', $data['especie']);
            foreach ($campos as $campo) {
                $where .= ' ' . $campo . ' LIKE "%' . str_replace('/', '', trim($data[$data['especie']])) . '%" OR ';
            }
            $where = substr($where, 0, -4) . ' AND ';
        }

        if ($data['situacao'] == '1')
            $where .= ' (SELECT count(*) FROM pagamentos pag WHERE pag.co_contrato = cont.co_contrato AND dt_pagamento IS NOT NULL) > 0 AND ';

        if ($data['situacao'] == '0')
            $where .= ' (SELECT count(*) FROM pagamentos pag WHERE pag.co_contrato = cont.co_contrato AND dt_pagamento IS NULL) > 0 AND ';

        if ($data['ano'])
            $where .= ' YEAR(cont.dt_ini_vigencia) = "' . $data['ano'] . '" AND ';

        if ($data['tipo'])
            $where .= ' cont.co_modalidade = "' . $data['tipo'] . '" AND ';

        if ($data['co_categoria'])
            $where .= ' cont.co_categoria = "' . $data['co_categoria'] . '" AND ';

        $sql = " SELECT ";

        $sql .= substr($cols, 0, -2);
        $sql .= " FROM ";
        $sql .= " contratos cont ";
        $sql .= " LEFT JOIN fornecedores forn on cont.co_fornecedor = forn.co_fornecedor ";
        $sql .= substr($where, 0, -5);

        $registros = $this->query(" SELECT * FROM ( " . $sql . ") AS contrato ");

        return $registros;
    }

    public function getPagamentos($co_contrato)
    {
    }

    private function getFiltroPerfil($aliasContrato = 'c')
    {
        $coAdministrador = ((defined('ADMINISTRADOR')) ? constant("ADMINISTRADOR") : false);
        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        $coGestor = ((defined('GESTOR')) ? constant("GESTOR") : false);
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        $coChefeDepartamento = ((defined('CHEFE DE DEPARTAMENTO')) ? constant("CHEFE DE DEPARTAMENTO") : false);
        $coGovernador = ((defined('GOVERNADOR')) ? constant("GOVERNADOR") : false);

        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();

        App::import('Model', 'CakeSession');
        $session = new CakeSession();

        $usuario = $session->read('usuario');
        $filtro = '';
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal) { // Fiscal
            $filtro .= " $aliasContrato.co_fiscal_atual = " . $usuario['Usuario']['co_usuario'];
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coGestor) { // Gestor
            $filtro .= " $aliasContrato.co_gestor_atual = " . $usuario['Usuario']['co_usuario'];
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor && !empty($usuario['Usuario']['co_setor'])) { // Setor
            $filtro = " $aliasContrato.co_setor = " . $usuario['Usuario']['co_setor'];
            $filtro .= " or ";
            $filtro .= " $aliasContrato.co_executante = " . $usuario['Usuario']['co_setor'];
        }
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coChefeDepartamento && !empty($usuario['Usuario']['co_setor'])) { // Chefe de Departamento
            $filtro = " $aliasContrato.co_contratante = " . $usuario['Usuario']['co_setor'];
        }

        return $filtro;
    }

    /**
     * Retorna o Percentual de contratos vencendo no período informado.
     *
     * @return array Com o percentual
     */
    public function getPercentualVencendo($prazo_ini = 0, $prazo_fim = 30, $co_instituicao = 0)
    {
        $filtro = $this->getFiltroPerfil();
        if ($filtro != '') {
            $filtro .= ' and ';
        }
        $filtro .= ' c.dt_fim_vigencia > current_date() ';

        if ($co_instituicao > 0) {
            $filtro .= ' and c.co_instituicao = ' . $co_instituicao;
        }

        $query = '
            SELECT (select count(c.co_contrato) from contratos c where c.ic_ativo = 1 and ' . $filtro . ' ) tt_geral,
                   count( distinct c.co_contrato) tt_prazo,
                   round( count( distinct c.co_contrato) / (select count(c.co_contrato) from contratos c where ' . $filtro . ' ), 2 ) * 100 pc_prazo
               FROM contratos c
            where ' . $filtro . ' and
                  DATEDIFF(c.dt_fim_vigencia, current_date()) > ' . $prazo_ini . ' and
                  DATEDIFF(c.dt_fim_vigencia, current_date()) <= ' . $prazo_fim;
        $result = $this->query($query);
        $retorno = array();
        $retorno['tt_geral'] = $result[0][0]['tt_geral'];
        $retorno['tt_prazo'] = $result[0][0]['tt_prazo'];
        $retorno['pc_prazo'] = $result[0][0]['pc_prazo'];
        return $retorno;
    }

    /**
     * Retorna o Percentual de contratos com pagamentos em atraso.
     *
     * @return array Com o percentual
     */
    public function getPercentualPgAtraso()
    {
        $filtro = $this->getFiltroPerfil();
        if ($filtro != '') {
            $filtro .= ' and ';
        }
        $filtro .= ' c.dt_fim_vigencia IS NOT NULL';

        $query = '
            select
            (select count(c.co_contrato) from contratos c where ' . $filtro . ' ) tt_geral,
            count(distinct(c.co_contrato)) tt_atraso,
            round( count( distinct c.co_contrato) / (select count(c.co_contrato) from contratos c where ' . $filtro . ' ), 2 ) * 100 pc_atraso
                    from contratos c
                            left join pagamentos p on (c.co_contrato = p.co_contrato)
            where ' . $filtro . ' and p.dt_pagamento is null and p.dt_vencimento < NOW() and c.ic_ativo = 1 ';
        $result = $this->query($query);
        $retorno = array();
        $retorno['tt_geral'] = $result[0][0]['tt_geral'];
        $retorno['tt_atraso'] = $result[0][0]['tt_atraso'];
        $retorno['pc_atraso'] = $result[0][0]['pc_atraso'];
        return $retorno;
    }

    /**
     * Retorna o Percentual de contratos ou processos com pendências.
     *
     * @return array Com o percentual
     */
    public function getPercentualPendencias()
    {
        $filtro = $this->getFiltroPerfil();
        if ($filtro != '') {
            $filtro .= ' and ';
        }
        $filtro .= ' c.dt_fim_vigencia > current_date() ';

        $query = "
            select
            (select count(c.co_contrato) from contratos c where " . $filtro . " ) tt_geral,
            count(distinct(c.co_contrato)) tt_pendencia,
            round( count( distinct c.co_contrato) / (select count(c.co_contrato) from contratos c where c.ic_ativo = 1 and " . $filtro . " ), 2 ) * 100 pc_pendencia
                    from contratos c
                            left join pendencias p on (c.co_contrato = p.co_contrato)
            where " . $filtro . " and p.st_pendencia = 'P' ";
        $result = $this->query($query);
        $retorno = array();
        $retorno['tt_geral'] = $result[0][0]['tt_geral'];
        $retorno['tt_pendencia'] = $result[0][0]['tt_pendencia'];
        $retorno['pc_pendencia'] = $result[0][0]['pc_pendencia'];
        return $retorno;
    }

    /**
     * Retorna os contratos por unidade solicitante
     *
     * @return unknown_type
     */
    public function listarContratosPorUnidadeSolicitante($coSetor, $coStatus = false)
    {
        $sqlLC = " select ";
        $sqlLC .= FunctionsComponent::exibeCampoSQL("if( cont.nu_processo != '', " . FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');

        $colContrato = array(
            null,
            2,
            3,
            4
        );

        $coTipo = array();

        if (in_array($coTipo, $colContrato))
            $sqlLC .= "if( cont.nu_contrato != '',  " . FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . " , '---') as 'Contrato', ";

        $sqlLC .= " u.ds_setor as 'Unid. Solicitante',
                    ifnull(forn.no_razao_social, '---') as 'Fornecedor',
                    cont.ds_objeto as 'Objeto',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim',
                ";

        if (in_array($coTipo, array(
            1,
            2
        )))
            $sqlLC .= " ifnull(DATE_FORMAT(DATE_ADD(cont.dt_fim_processo, INTERVAL 90 DAY) , '%d/%m/%Y'), '---') as 'Término da Garantia', ";

        if (in_array($coTipo, $colContrato))
            $sqlLC .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        $sqlLC .= " if(cont.vl_mensal=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Mensal (R$)', ";

        $sqlLC .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "'
                    from contratos cont
                    LEFT JOIN fornecedores forn on cont.co_fornecedor = forn.co_fornecedor
                    left join setores u on u.co_setor = cont.co_contratante";

        $sqlLC .= " where cont.nu_contrato != '' and cont.ic_ativo = 1 ";

        if ($coStatus) {
            $sqlLC .= " and dt_fim_vigencia > NOW() ";
        }

        // if ($coStatus > 0) {
        // $sqlLC .= " and cont.co_situacao = $coStatus ";
        // }

        if ($coSetor > 0) {
            $sqlLC .= "and (cont.co_contratante = $coSetor or
                            cont.co_contratante in (SELECT co_setor FROM setores WHERE parent_id = $coSetor or parent_id in (SELECT co_setor FROM setores WHERE parent_id = $coSetor ) ) )";
        }

        $sqlLC .= " order by cont.nu_contrato ";

        // debug($sqlLC);die;

        return $this->consulta($sqlLC);
    }

    // lista de contratos e processo por unidade solicitante
    public function listarContratosEProcessosPorUnidadeSolicitante($coSetor, $coStatus = false, $coMes = 0)
    {
        $query = "select c.nu_processo as Processo, c.nu_contrato as Contrato, s.ds_setor as `Unidade Solicitante`, f.no_razao_social
        as Fornecedor, c.ds_objeto as Objeto, c.vl_mensal as  `Valor Mensal (R$)`, c.vl_global as `Valor Global (R$)`,
        MONTHNAME_PT(MONTH(c.dt_ini_vigencia)) as Início, MONTHNAME_PT(MONTH(c.dt_fim_vigencia)) as Fim from contratos c
        join setores s on(s.co_setor = c.co_contratante)
        join fornecedores f on (c.co_fornecedor = f.co_fornecedor)";

        if ($coMes || $coSetor || $coStatus) {
            $query .= ' where';
        } else {
            $query .= ' and';
        }

        if ($coSetor) {
            $query .= " s.co_setor = $coSetor and";
        }

        if ($coMes) {
            $query .= " MONTH(c.dt_ini_vigencia) = $coMes and";
        }

        if ($coStatus) {
            $query .= ' c.dt_fim_vigencia < ' . date('Y-m-d') . ' and ';
        }

        $query .= " (c.nu_pam = \"\" or c.nu_pam is null)";
        // echo $query;exit;
        /*$sqlLCP = " select ";
        $sqlLCP .= FunctionsComponent::exibeCampoSQL( "if( cont.nu_processo != '', " .
        FunctionsComponent::getMaskSQL('cont.nu_processo', 'processo') . ", '---') as 'Processo', ", 'isProcesso');
        $colContrato = array(null,  2, 3,4);

        if (in_array($coTipo, $colContrato)) {
            // exit('n é pra passar aqui');
            $sqlLCP .= "if( cont.nu_contrato != '',  " .
            FunctionsComponent::getMaskSQL('cont.nu_contrato', 'contrato') . " , '---') as 'Contrato', ";
        }

        if($coMes != 0){
            $sqlLCP .= "MONTHNAME_PT(MONTH(cont.dt_cadastro))  as 'Mês de Cadastro', ";
            $sqlLCP .= "YEAR(cont.dt_cadastro)  as 'Ano de Cadastro', ";
        }

        $sqlLCP .= " u.ds_setor
        as 'Unid. Solicitante',
                    ifnull(forn.no_razao_social, '---') as 'Fornecedor',
                    cont.ds_objeto as 'Objeto',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_ini_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_ini_vigencia,'%d/%m/%Y'),'---') ) as 'Inicio',
                    if(cont.co_modalidade<0, ifnull(DATE_FORMAT(cont.dt_fim_processo,'%d/%m/%Y'),'---') , ifnull(DATE_FORMAT(cont.dt_fim_vigencia,'%d/%m/%Y'),'---') ) as 'Fim',
                ";

        if (in_array($coTipo, array(
            1,
            2
        )))
            $sqlLCP .= " ifnull(DATE_FORMAT(DATE_ADD(cont.dt_fim_processo, INTERVAL 90 DAY) , '%d/%m/%Y'), '---') as 'Término da Garantia', ";

        if (in_array($coTipo, $colContrato))
            $sqlLCP .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(cont.dt_fim_vigencia, '%Y%m'), DATE_FORMAT(cont.dt_ini_vigencia, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        $sqlLCP .= " if(cont.vl_mensal=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_mensal , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor Mensal (R$)', ";

        $sqlLCP .= " if(cont.vl_global=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( cont.vl_global , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as '" . __('Valor Global (R$)', true) . "'
                    from contratos cont
                    LEFT JOIN fornecedores forn on cont.co_fornecedor = forn.co_fornecedor
                    left join setores u on u.co_setor = cont.co_contratante
                    ";

        $sqlLCP .= " where cont.nu_processo != '' and cont.ic_ativo = 1 ";

        if($coStatus) {
            $sqlLCP .= " and cont.dt_fim_vigencia > NOW() ";
        }

        if ($coStatus > 0) {
            $sqlLCP .= " and cont.co_situacao = $coStatus ";
        }

        if ($coSetor > 0) {
            $sqlLCP .= "and (cont.co_contratante = $coSetor or
                            cont.co_contratante in (SELECT co_setor FROM setores
                            WHERE parent_id = $coSetor or parent_id in (SELECT co_setor FROM setores WHERE parent_id = $coSetor ) ) )";
        }

        // $sqlLCP .= "and (e.nu_empenho != '' or e.nu_empenho is not null)";

        $sqlLCP .= " order by cont.nu_contrato ";*/

        return $this->consulta($query);
    }
    /* --- */

    /* --- */
    public function listarNotasEmpenhoSemContrato($co_instituicao)
    {
        $sql = "select
                    if( c.nu_processo != '', " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',
                    e.nu_empenho   as 'Empenho',
                    s.ds_setor   as 'Unidade Solicitante',
                    f.no_razao_social   as 'Fornecedor',
                    c.ds_objeto   as 'Objeto',
                    DATE_FORMAT(c.dt_fim_processo, '%d/%m/%Y') as 'Fim Estimado',
                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( e.vl_empenho, 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor Empenho',
                    ifnull( Concat('R$ ', Replace (Replace (Replace (Format( c.vl_global, 2), '.', '|'), ',', '.'), '|', ',')), '---') as 'Valor Global'
                from contratos c
                join empenhos e on e.co_contrato = c.co_contrato
                left join setores s on s.co_setor = c.co_setor
                left join fornecedores f on f.co_fornecedor = c.co_fornecedor
                where c.dt_fim_processo >= date(NOW())
                and c.nu_contrato = '' and c.ic_ativo = 1
                and c.nu_processo != ''";

        $sql .= " order by c.dt_fim_processo DESC";

        return $this->consulta($sql);
    }

    /**
     *
     * @return unknown_type
     */
    public function listarContratoRespectivoApostilamento()
    {
        return $this->consulta("select
                                        " . FunctionsComponent::exibeCampoSQL("if( c.nu_processo != '',  " . FunctionsComponent::getMaskSQL('c.nu_processo', 'processo') . ", '---') as 'Processo',", 'isProcesso') . "
                                        if( c.nu_contrato is not null,  " . FunctionsComponent::getMaskSQL('c.nu_contrato', 'contrato') . " , '---') as 'Contrato',
                                        ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                            c.ds_objeto   as 'Objeto',
                                            a.ds_apostilamento  as 'Justificativa Apostilamento',
                                            DATE_FORMAT(a.dt_apostilamento, '%d/%m/%Y') as 'Data Apostilamentos',
                                            ifnull( Concat('', Replace (Replace (Replace (Format( a.vl_apostilamento , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor do apostilamento (R$)'
                                        from
                                            contratos c
                                        LEFT JOIN apostilamentos   a on c.co_contrato = a.co_contrato
                                        LEFT JOIN fornecedores  fd on c.co_fornecedor = fd.co_fornecedor
					WHERE a.vl_apostilamento and c.ic_ativo = 1 ");
    }

    /**
     * Métodos responsáveis pela geração de relatórios de atas
     */
    public function listarAtas()
    {
        $sqlLA = " SELECT ";
        $sqlLA .= "if( ata.nu_ata > 0,  " . FunctionsComponent::getMaskSQL('ata.nu_ata', 'ata') . " , '---') as 'Número', ";

        $sqlLA .= "ifnull(atas_categorias.ds_ata_categoria, '---') as 'Categoria', ";

        $sqlLA .= " ifnull(forn.no_razao_social, '---') as '" . __('Fornecedor', true) . "',
                    ata.ds_publicacao as 'Publicação',
                ";

        $sqlLA .= " ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(ata.dt_vigencia_fim, '%Y%m'), DATE_FORMAT(ata.dt_vigencia_inicio, '%Y%m')), ' mês(es)'), '---') as 'Vigência', ";

        $sqlLA .= " if(ata.vl_ata=0,'---',ifnull( Concat('', Replace (Replace (Replace (Format( ata.vl_ata , 2), '.', '|'), ',', '.'), '|', ',')) ,'---')) as 'Valor da Ata (R$)'
                    FROM atas ata
                    LEFT JOIN atas_categorias on ata.co_categoria_ata = atas_categorias.co_ata_categoria
                    LEFT JOIN fornecedores forn on (forn.co_fornecedor = ata.co_fornecedor) ";

        $sqlLA .= " WHERE nu_ata != '' ";

        $sqlLA .= " ORDER by ata.nu_ata ";

        return $this->consulta($sqlLA);
    }

    public function listarAtasExpirar($dia, $mes, $ano, $dia2, $mes2, $ano2, $coStatus = false)
    {
        $sel1 = $ano . "-" . $mes . "-" . $dia;
        $sel2 = $ano2 . "-" . $mes2 . "-" . $dia2;
        $sql = "SELECT
                                        if( a.nu_ata > 0,  " . FunctionsComponent::getMaskSQL('a.nu_ata', 'ata') . " , '---') as 'Número',
                                        ifnull(fd.no_razao_social, '---') as 'Fornecedor',
                                        ifnull(fd.nu_cnpj, '---') as 'CNPJ',
                                        a.ds_publicacao   as 'Publicação' ,
                                        DATE_FORMAT(a.dt_vigencia_inicio, '%d/%m/%Y') as 'Inicio de Vigência',
                                        DATE_FORMAT(a.dt_vigencia_fim, '%d/%m/%Y') as 'Fim de Vigência',
                                        ifnull( Concat('', Replace (Replace (Replace (Format( a.vl_ata , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor da Ata (R$)',
                                        ifnull(CONCAT(PERIOD_DIFF(DATE_FORMAT(a.dt_vigencia_fim, '%Y%m'), DATE_FORMAT(NOW(), '%Y%m')), ' mês(es)'), '---') as 'Expiração'
                                    FROM
                                        atas a
                                        LEFT JOIN fornecedores fd on a.co_fornecedor = fd.co_fornecedor
                                    WHERE ( a.dt_vigencia_fim between '$sel1' and '$sel2' ) ";
        if ($coStatus) {
            $sql .= " and a.dt_vigencia_fim > NOW() ";
        }

        $sql .= " ORDER BY a.nu_ata ASC ";

        return $this->consulta($sql);
    }

    public function listarAtasPorItensDisponiveis($categoria)
    {
        $sql = "SELECT ";
        $sql .= "if( a.nu_ata > 0,  " . FunctionsComponent::getMaskSQL('a.nu_ata', 'ata') . " , '---') as 'Número', ";
        $sql .= "ifnull(fd.no_razao_social, '---') as 'Fornecedor', ";
        $sql .= "i.nu_item as 'Item', ";
        $sql .= "i.ds_descricao as 'Objeto', ";
        $sql .= "i.nu_quantidade as 'Quantidade', ";
        $sql .= "(i.nu_quantidade - i.qt_utilizado) as 'Restam', ";
        $sql .= "ifnull( Concat('', Replace (Replace (Replace (Format( i.vl_unitario , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Vl. Unitário (R$)' ";
        $sql .= "FROM atas a  ";
        $sql .= "LEFT JOIN fornecedores fd on a.co_fornecedor = fd.co_fornecedor ";
        $sql .= "LEFT JOIN atas_categorias ct on a.co_categoria_ata = ct.co_ata_categoria ";
        $sql .= "LEFT JOIN atas_itens i on a.co_ata = i.co_ata ";
        $sql .= "where a.dt_vigencia_fim is not null and a.dt_vigencia_fim > NOW() and (i.nu_quantidade - i.qt_utilizado) > 0 ";

        if (isset($categoria))
            $sql .= " and a.co_categoria_ata = {$categoria} ";

        $sql .= "ORDER BY a.dt_vigencia_fim";

        return $this->consulta($sql);
    }

    public function listarPctAtaPorCategoria()
    {
        return $this->consulta("
                SELECT c.ds_ata_categoria 'Categoria', count(a.co_categoria_ata) * total.fator as '%',
                ifnull( Concat('R$ ', Replace (Replace (Replace (Format( sum(a.vl_ata) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Valor da Ata'
                FROM atas a
                   JOIN (SELECT 100/count(*) as fator from atas where co_categoria_ata is not null ) as total
                   LEFT JOIN atas_categorias c on ( c.co_ata_categoria = a.co_categoria_ata )
                WHERE a.co_categoria_ata is not null group by a.co_categoria_ata");
    }

    public function detalhadoAtas($ata)
    {
        return $this->consulta("
            select distinct
                (select s.ds_setor from setores s where s.co_setor = u.co_setor) as 'Solicitado Por',
                ai.ds_descricao as 'Objeto',
                ai.nu_quantidade as 'Qtde. Total',
                ai.qt_utilizado as 'Qtde. Solicitada',
                (ai.nu_quantidade - ai.qt_utilizado) as 'Qtde. Restante',
                DATE_FORMAT(ap.dt_pedido, '%d/%m/%Y') as 'Data Pedido',
                ifnull( Concat('R$ ', Replace (Replace (Replace (Format( ai.vl_unitario , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Vl. Unitário',
                ifnull( Concat('R$ ', Replace (Replace (Replace (Format( (ai.vl_unitario * ai.qt_utilizado) , 2), '.', '|'), ',', '.'), '|', ',')) , '---') as 'Vl. Total'
            from atas a
            join atas_itens ai on ai.co_ata = a.co_ata
            join atas_pedidos ap on ap.co_ata = a.co_ata
            join usuarios u on u.co_usuario = a.co_usuario
            where a.co_ata = $ata
                and ap.vl_pedido <> 0
        ");
    }

    public function getInstaceModulo()
    {
        App::import('Helper', 'Modulo');
        $modulo = new ModuloHelper();
        return $modulo;
    }

    /**
     * Relatório de Responsáveis por Atividade
     * @item 60, 62
     * @return unknown_type
     */
    public function responsaveisAtividades()
    {
        $query = "SELECT c.nu_contrato as Contrato, a.ds_atividade as Atividade, a.pc_executado as 'Percentual de execução', u.ds_nome as Responsável from atividades a join usuarios u on (u.co_usuario = a.co_responsavel) join contratos c on (a.co_contrato = c.co_contrato) where c.co_contrato is not null order by a.co_atividade";

        return $this->consulta($query);
    }

    /**
     * Relatório de Datas das Atividades
     * @item 63
     * @return unknown_type
     */
    public function datasAtividades()
    {
        $query = "SELECT c.nu_contrato as Contrato, a.ds_atividade as Atividade, a.dt_ini_planejado as 'Data de Início Planejada', a.dt_fim_planejado as 'Data de Término Planejada', a.dt_ini_execucao as 'Data do Início da Execução', a.dt_fim_execucao as 'Data do Término da Execução' from atividades a join usuarios u on (u.co_usuario = a.co_responsavel) join contratos c on (a.co_contrato = c.co_contrato) where c.co_contrato is not null order by a.co_atividade";

        return $this->consulta($query);
    }

    /**
     * Relatório de Atividades Atrasadas
     * @item 66
     * @return unknown_type
     */
    public function atividadesAtrasadas()
    {
        $query = 'SELECT c.nu_contrato as Contrato, c.nu_processo as Processo, a.ds_atividade as Atividade, date_format(a.dt_ini_planejado, "%d/%m/%Y") as Início, date_format(a.dt_fim_planejado, "%d/%m/%Y") as Fim, datediff(curdate(),a.dt_fim_planejado) as "Dias em atraso"  from atividades a join contratos c on (a.co_contrato = c.co_contrato) where c.co_contrato is not null and curdate() > a.dt_fim_planejado order by a.co_atividade';

        return $this->consulta($query);
    }

    /**
     * Relatório Financeiro de Atividades
     * @item 64
     * @return unknown_type
     */
    public function financeiroAtividades()
    {
        $query = "SELECT c.nu_contrato as Contrato, a.ds_atividade as Atividade, p.dt_vencimento as 'Data de Vencimento', p.dt_pagamento as 'Data de Pagamento', p.vl_pagamento as 'Valor do Pagamento', p.vl_imposto as 'Imposto', p.vl_liquido as 'Valor Líquido' from atividades a join pagamentos p on (a.co_atividade = p.co_atividade) join contratos c on (a.co_contrato = c.co_contrato) where c.co_contrato is not null order by a.co_atividade";

        return $this->consulta($query);
    }

    // liquidações por contrato
    public function liquidacaoPorContrato()
    {
        $query = "SELECT c.nu_contrato as Contrato, l.nu_liquidacao as 'Número da NL', date_format(l.dt_liquidacao,'%d/%m/%Y') as 'Data da Liquidação', if (l.vl_liquidacao > 0, Concat('R$ ',Replace (Replace  (Replace (Format(l.vl_liquidacao, 2), '.', '|'), ',', '.'), '|', ',')), '---') as 'Valor da Liquidação', l.ds_liquidacao as 'Descrição da Liquidação' from liquidacao l join contratos c on (l.co_contrato = c.co_contrato) where c.co_contrato is not null";

        return $this->consulta($query);
    }

    /**
     * Relatório de Processos por Fase de Tramitação
     * @item 56
     * @param $fase
     * @return unknown_type
     */
    public function processosPorFaseTramitacao($fase)
    {

        if (!empty($fase)) {
            $query = "SELECT 
						fs.ds_fase as Fase, 
						c.nu_processo as 'Número do Processo', 
						c.nu_contrato as Contrato, 
						c.ds_objeto as Objeto, 
						c.dt_ini_processo as 'Início da Vigência', 
						c.dt_fim_processo as 'Fim da Vigência', 
						c.vl_mensal as 'Valor Mensal', 
						c.vl_global as 'Valor Total' 
					from contratos c , fases fs
					where c.co_contrato is not null 
					and c.co_fase = '" . $fase . "'
					and c.co_fase = fs.co_fase
					group by nu_processo";
        } else {
            $query = "SELECT fs.ds_fase as Fase, c.nu_processo as 'Número do Processo', c.nu_contrato as Contrato, f.no_razao_social as Fornecedor, c.ds_objeto as Objeto, c.dt_ini_vigencia as 'Início da Vigência', c.dt_fim_vigencia as 'Fim da Vigência', c.vl_mensal as 'Valor Mensal', c.vl_global as 'Valor Total' from contratos c join fases fs on c.co_fase = fs.co_fase join fornecedores f on c.co_fornecedor = f.co_fornecedor where c.co_contrato is not null";
        }

        return $this->consulta($query);
    }

    /**
     * Relatório de Itens de Produtos Fornecidos por Contrato/Processo
     * @item 69
     * @return unknown_type
     */
    public function produtosFornecidos()
    {
        $query = "SELECT c.nu_processo as Processo, c.nu_contrato as Contrato, p.ds_produto as Produto, p.ds_fabricante as Fabricantes from produtos p join contratos c on (p.co_contrato = c.co_contrato) where c.co_contrato is not null and p.produto_recebido = 1";

        return $this->consulta($query);
    }

    /**
     * Relatório de Itens de Produtos que Compõem o Processo/Contrato
     * @item 70
     */
    public function produtosCompoem()
    {
        $query = "SELECT c.nu_processo as Processo, c.nu_contrato as Contrato, p.ds_produto as Produto, p.ds_fabricante as Fabricantes from produtos p join contratos c on (p.co_contrato = c.co_contrato) where c.co_contrato is not null";

        return $this->consulta($query);
    }

    /**
     * Relatório de Atestos por Atividades/Tarefas dos Contratos
     * @item 65
     * @return unknown_type
     */
    public function atestosAtividades()
    {
        $query = "select a.ds_atividade as 'Atividade', nf.vl_nota as 'Valor da Nota', nf.dt_atesto as 'Data do Atesto', nf.ds_atesto as 'Descrição do Atesto' from notas_fiscais nf join notas_pagamentos np on np.co_nota = nf.co_nota join pagamentos p on p.co_pagamento = np.co_pagamento join atividades a on a.co_atividade = p.co_atividade where nf.ic_atesto = 1 group by a.co_atividade";

        return $this->consulta($query);
    }

        /**
 
     * Relatório de Fornecedores
 
     * @return array
 
     */
 
    public function fornecedores() {
 
        $query = "SELECT  CASE WHEN CHAR_LENGTH(Fornecedor.nu_cnpj) = 11 THEN mask(Fornecedor.nu_cnpj, '###.###.###-##')
 
                    WHEN CHAR_LENGTH(Fornecedor.nu_cnpj) = 14 THEN mask(Fornecedor.nu_cnpj, '##.###.###/####-##') END 'CPF/CNPJ',
 
                    Fornecedor.no_razao_social 'Razão Social',
 
                    CASE WHEN Fornecedor.tp_fornecedor = 'f' THEN 'Pessoa Física'
 
                    WHEN Fornecedor.tp_fornecedor = 'j' THEN 'Pessoa Jurídica' END 'Tipo',
 
                    CASE WHEN Area.ds_area = '' OR Area.ds_area IS NULL THEN 'Não informado'
 
                    ELSE Area.ds_area END 'Área',
 
                    CASE WHEN Fornecedor.ds_email = '' OR Fornecedor.ds_email IS NULL THEN 'Não informado'
 
                    ELSE Fornecedor.ds_email END 'E-mail',
 
                    CASE WHEN Fornecedor.nu_telefone = '' OR Fornecedor.nu_telefone IS NULL THEN 'Não informado'
 
                    ELSE Fornecedor.nu_telefone END 'Telefone',
 
                    CASE WHEN Fornecedor.ic_ativo = 1 THEN 'Ativo'
 
                    ELSE 'Inativo' END 'Situação'
 
            FROM fornecedores Fornecedor
 
            LEFT JOIN areas Area ON Fornecedor.co_area = Area.co_area";
 
 
        return $this->consulta($query);
 
    }

    /*
    * [GES-599] Listar contratos com aditivos pendentes
    */

    public function listaContratosAditivosPendentes() {

        $query = "SELECT " . FunctionsComponent::getMaskSQL('nu_contrato', 'contrato') . " as 'Contrato', 
                    nu_processo as 'Processo',
                    Count(co_aditivo) as 'Aditivos Pendentes',
                    ds_nome as 'Gestor'
                    FROM contratos
                    LEFT JOIN usuarios ON contratos.co_gestor_atual = usuarios.co_usuario
                    JOIN aditivos
                    WHERE aditivos.co_contrato = contratos.co_contrato
                    AND aditivos.tp_aditivo IS NULL
                    GROUP BY nu_contrato";

        return $this->consulta($query);

    }
 
}

?>
