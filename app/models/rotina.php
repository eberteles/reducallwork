<?php

class Rotina extends AppModel
{

    var $name = 'Rotina';

    var $useTable = 'rotinas';

    var $primaryKey = 'co_rotina';

    var $validate = array(
        'co_rotina' => array(
            'numeric' => array(
                'rule' => array(
                    'numeric'
                ),
                'message' => 'Campo inválido',
                'on' => 'update'
            )
        )
        ,
        'tp_rotina' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        )
    );
}
?>