<?php

class Servico extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );
	

    var $name = 'Servico';

    var $useTable = 'servicos';

    var $primaryKey = 'co_servico';

    var $displayField = 'ds_servico';

    var $validate = array(
        'ds_servico' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
            
        ),
        'isUnique' => array(
            'rule' => array(
                'isUnique'
            ),
            'message' => 'Descrição já cadastrada'
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Servico']['ds_servico'] = up($this->data['Servico']['ds_servico']);
        
        return parent::beforeValidate($options);
    }
}
?>