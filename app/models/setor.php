<?php

class Setor extends AppModel
{
	/**
	 * array(
	 * 	'INSERT',
	 *  'UPDATE',
	 *  'DELETE',
	 * )
	 * @var array
	 */
	protected $_logAction = array(
		'INSERT',
		'UPDATE',
		'DELETE'
	);
	
    var $name = 'Setor';

    var $useTable = 'setores';

    var $primaryKey = 'co_setor';

    var $displayField = 'ds_setor';
    
    var $order = "ds_setor  ASC";

    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'co_usuario'
        ),
    );

    var $validate = array(
        'ds_setor' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Descrição em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Campo Nome já cadastrado.'
            )
        ),
        'ds_email' => array(
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Campo E-mail inválido!',
                'allowEmpty' => true
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Campo E-mail já cadastrado.',
                'allowEmpty' => true
            )
        ),
        'nu_telefone' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Telefone em branco',
                'allowEmpty' => true
            )
        )
    )
    ;


    public function getSetor($nome)
    {
        $result = $this->query("Select * from setores where ds_setor ='".$nome."'");
        return $result;
    }
    
    public function beforeSave($options = array()) {
        if (isset($this->data['Setor']['ds_email'])) {
            $this->data['Setor']['ds_email'] = up($this->data['Setor']['ds_email']);
        }
        return parent::beforeSave($options);
    }

    public function afterSave($created = true)
    {
        return parent::afterSave($created);
    }

    public function beforeDelete() {
        return parent::beforeDelete();
    }

}
?>