<?php

class Situacao extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Situacao';

    var $useTable = 'situacoes';

    var $primaryKey = 'co_situacao';

    var $displayField = 'ds_situacao';

    var $validate = array(
        'ds_situacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo descrição  em branco em branco'
            )
            // 'allowEmpty' => false,
            // 'required' => false,
            // 'last' => false, // Stop validation after this rule
            // 'on' => 'create', // Limit validation to 'create' or 'update' operations
            ,
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Descrição já cadastrada'
            )
        )
       ,
       'nu_sequencia' => array(
           'notempty' => array(
               'rule' => array(
                   'notempty'
               ),
               'message' => 'Campo sequência em branco'
           )
       )
    );

    var $hasMany = array(
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_situacao',
            'dependent' => false
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Situacao']['ds_situacao'] = up($this->data['Situacao']['ds_situacao']);

        return parent::beforeValidate($options);
    }
    public function getByDescricao($ds_situacao)
    {
        return $this->query(" SELECT * FROM situacoes WHERE ds_situacao = '".($ds_situacao)."'");
    }
    public function afterSave($options = array())
    {
        if(!isset($this->data['co_situacao']) || $this->data['co_situacao'] == null || $this->data['co_situacao']){
            $nextId = $this->find('first', array('conditions' => array('co_situacao is not' => null),
                'order' => array('co_situacao' => 'DESC') ));

            $this->data['Situacao']['co_situacao'] = $nextId['Situacao']['co_situacao'] + 1;
        }

        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $this->data['Situacao']['co_usuario'] = $usuario['Usuario']['co_usuario'];

        return parent::beforeSave($options);
    }
}
?>
