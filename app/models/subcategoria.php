<?php

class Subcategoria extends AppModel
{

    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    var $name = 'Subcategoria';

    var $useTable = 'subcategorias';

    var $primaryKey = 'co_subcategoria';

    var $displayField = 'no_subcategoria';

    var $validate = array(
        'co_categoria' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo categoria em branco'
            )
        ),
        'no_subcategoria' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo descrição em branco'
            ),
            'myUnique' => array(
                'rule' => array('validateUnique'),
                'message' => 'A sub categoria já está cadastrada'
            )
        )
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasOne = array(
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'co_categoria'
        )
    );

    function beforeValidate($options = array())
    {
        $this->data['Subcategoria']['no_subcategoria'] = up($this->data['Subcategoria']['no_subcategoria']);

        return parent::beforeValidate($options);
    }

    public function afterSave($options = array())
    {
        if (!isset($this->data['Subcategoria']['co_subcategoria']) || $this->data['Subcategoria']['co_subcategoria'] == null || $this->data['Subcategoria']['co_subcategoria'] == '') {
            $nextId = $this->find('first', array('conditions' => array('co_subcategoria is not' => null),
                'order' => array('co_subcategoria' => 'DESC')));

            $this->data['Subcategoria']['co_subcategoria'] = $nextId['Subcategoria']['co_subcategoria'] + 1;
        }

        return parent::beforeSave($options);
    }

    function validateUnique($data)
    {
        $count = $this->find('count', array(
            'conditions' => array(
                'UPPER(Subcategoria.no_subcategoria)' => strtoupper($data['no_subcategoria']),
                'Subcategoria.ic_ativo' => 2
            )
        ));

        return $count ? false : true;
    }

    function findByName($name, $ativo = 2)
    {
        $count = $this->find('count', array(
            'conditions' => array(
                'UPPER(Subcategoria.no_subcategoria)' => up($name),
                'Subcategoria.ic_ativo' => $ativo
            )
        ));

        return $count ? true : false;
    }

}
