<?php

class TipoLicitacao extends AppModel
{

    public $name = 'TipoLicitacao';

    public $primaryKey = 'co_tipo_licitacao';

    public $useTable = 'tipos_licitacoes';

    var $displayField = 'no_tipo_licitacao';

    public $validate = array(
        'no_tipo_licitacao' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Tipo Licitação em branco'
            )
        )

    );

    var $hasMany = array(
        'Licitacao' => array(
            'className' => 'Licitacao',
            'foreignKey' => 'co_tipo_situacao',
            'dependent' => false
        )
    );

    // public function beforeValidate() {

    // }

    // public function beforeSave() {

    // }

    // public function afterSave() {

    // }


}
?>
