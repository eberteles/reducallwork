<?php

class TipoPenalidade extends AppModel
{

    public $name = 'TipoPenalidade';

    public $primaryKey = 'co_tipo_penalidade';

    public $useTable = 'tb_tipo_penalidade';

    public $displayField = 'no_tipo_penalidade';

    public $validate = array(
        'no_tipo_penalidade' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Tipo de Penalidade em branco'
            )
        )
    );

    public $hasMany = array(
        'Penalidade' => array(
            'className' => 'Penalidade',
            'foreignKey' => 'co_tipo_penalidade',
            'dependent' => false
        )
    );
}
