<?php

class Uasg extends AppModel
{

    var $name = 'Uasg';

    var $useTable = 'uasgs';

    var $primaryKey = 'co_uasg';

    var $displayField = 'uasg';
    
    var $order = "co_uasg ASC";

    var $validate = array(
        'uasg' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo UASG em Branco.'
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Apenas números são permitidos'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'UASG já existente.'
            ),
            'minlength' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Mínimo de 6 caracteres'
            ),
        )
    );
}
?>