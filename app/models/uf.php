<?php

class Uf extends AppModel
{

    var $name = 'Uf';

    var $useTable = 'ufs';

    var $primaryKey = 'sg_uf';

    var $validate = array(
        'sg_uf' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        ,
        'no_uf' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo inválido'
            )
        ) // 'allowEmpty' => false,
          // 'required' => false,
          // 'last' => false, // Stop validation after this rule
          // 'on' => 'create', // Limit validation to 'create' or 'update' operations
        
        
    );
}
?>