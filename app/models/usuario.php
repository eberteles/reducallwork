<?php

class Usuario extends AppModel
{
    /**
     * array(
     *     'INSERT',
     *     'UPDATE',
     *     'DELETE',
     * )
     * @var array
     */
    protected $_logAction = array(
        'INSERT',
        'UPDATE',
        'DELETE'
    );

    public $name = 'Usuario';

    public $useTable = 'usuarios';

    public $primaryKey = 'co_usuario';

    public $displayField = 'ds_nome';

    public $validate = array(

        'nu_cpf' => array(
            'isUnique' => array(
                'rule' => array(
                    'isUniqueCpf',
                    'checkZeroCpf',
                    true
                ),
                'message' => 'Este CPF já está em uso.'
            ),
            'notempty' => array(
                'rule' => array(
                    'validaCpf',
                    true
                ),
                'message' => 'Campo CPF inválido.'
            )
        ),

        'ds_nome' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Nome em branco'
            )
        ),

        'no_usuario' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Usuário em branco'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'Este Usuário já está em uso.'
            )
        ),

        'ds_senha' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Senha em branco'
            )
        ),

        'ds_confirma_senha' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Confirmar Senha em branco'
            ),
            'identicalFieldValues' => array(
                'rule' => array(
                    'identicalFieldValues',
                    'ds_senha'
                ),
                'message' => 'O campo Confirmar senha deve ser igual ao campo Senha'
            )
        ),

        'ds_email' => array(
            'email' => array(
                'rule' => array(
                    'email'
                ),
                'message' => 'Campo E-mail inválido!'
            ),
            'isUnique' => array(
                'rule' => array(
                    'isUnique'
                ),
                'message' => 'E-mail já cadastrado.'
            )
        )
    );

    public $belongsTo = array(
        'Setor' => array(
            'className' => 'Setor',
            'foreignKey' => 'co_setor'
        ),
    );

    public $hasOne = array(
        'UsuarioPerfil' => array(
            'className' => 'UsuarioPerfil',
            'foreignKey' => 'co_usuario',
            'dependent' => true
        )
    );

    private $userPrivileges = array('DELETE','EXECUTE','INSERT','SELECT','UPDATE');

    private $usernameDatabase = null;
    private $srvAppAddr = null;
    private $saltLoginLogAuditoria = null;
    private $defaultDatabaseName = null;



    /**
     * Verifica se o usuário tem os dados mínimos completos.
     */
    public function hasCompleteData()
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');

        $isComplete = true;

        if (!isset($usuario['Usuario']['nu_cpf'])
            || empty($usuario['Usuario']['nu_cpf'])
            || !FunctionsComponent::validaCPF($usuario['Usuario']['nu_cpf'])
        ) {
            $isComplete = false;
        }
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coSetor
            && !isset($usuario['Usuario']['co_setor'])
            && empty($usuario['Usuario']['co_setor'])
        ) {
            $isComplete = false;
        }

        $coFiscal = ((defined('FISCAL')) ? constant("FISCAL") : false);
        if ($usuario['UsuarioPerfil']['co_perfil'] == $coFiscal
            && !isset($usuario['Usuario']['co_gestor'])
            && empty($usuario['Usuario']['co_gestor'])
        ) {
            $isComplete = false;
        }

        if (!isset($usuario['Usuario']['ds_email'])
            || empty($usuario['Usuario']['ds_email'])
        ) {
            $isComplete = false;
        }

        if (!isset($usuario['UsuarioPerfil']['co_perfil'])) {
            $isComplete = false;
        }

        return $isComplete;
    }

    /**
     * Verifica se esta editando os próprios dados.
     *
     * @return boolean
     */
    public function isOwnData($coUsuario)
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');

        if ($usuario['Usuario']['co_usuario'] == $coUsuario) {
            return true;
        }
        return false;
    }

    public function validaCpf()
    {
        if ($this->data['Usuario']['nu_cpf'] == 00000000000) {
            return true;
        } elseif (FunctionsComponent::validaCPF($this->data['Usuario']['nu_cpf'])) {
            return true;
        } else {
            return false;
        }
    }

    public function checkZeroCpf($cpf)
    {
        $cpf = $this->data['Usuario']['nu_cpf'];

        if ($cpf == 00000000000) {
            return true;
        }
    }

    public function beforeValidate($options = array())
    {
        $sessao = new SessionComponent();
        $usuario = $sessao->read('usuario');
        $coSetor = ((defined('SETOR')) ? constant("SETOR") : false);
        // Se PERFIL Setor não pode estar sem vinculo a um setor.
        if ($this->data['UsuarioPerfil']['co_perfil'] == $coSetor) {
            $this->validate['co_setor'] = array(
                'notempty' => array(
                    'rule' => array(
                        'notempty'
                    ),
                    'message' => 'Campo ' . __('Unidade Administrativa', true) . ' em branco'
                )
            );
        }

        // Verifica se é edição e se mudou o nome do usuário.
        if (isset($this->data['Usuario']['co_usuario'])
            && !empty($this->data['Usuario']['co_usuario'])
        ) {
            $usuarioAtual = $this->findByCoUsuario($this->data['Usuario']['co_usuario']);
            if ($usuarioAtual['Usuario']['no_usuario'] == $this->data['Usuario']['no_usuario']) {
                unset($this->validate['no_usuario']['isUnique']);
            }
            if ($usuarioAtual['Usuario']['ds_email'] == $this->data['Usuario']['ds_email']) {
                unset($this->validate['ds_email']['isUnique']);
            }
            if ($usuarioAtual['Usuario']['nu_cpf'] == $this->data['Usuario']['nu_cpf']) {
                unset($this->validate['nu_cpf']['isUnique']);
            }
        }

        if (empty($this->data['Usuario']['dt_bloqueio'])) {
            $this->data['Usuario']['dt_bloqueio'] = null;
        }

        if (empty($this->data['Usuario']['dt_liberacao'])) {
            $this->data['Usuario']['dt_liberacao'] = null;
        }

        return parent::beforeValidate($options);
    }

    /**
     * Returns false if any fields passed match any (by default, all if $or = false) of their matching values.
     *
     * @param array $fields Field/value pairs to search (if no values specified, they are pulled from $this->data)
     * @param boolean $or If false, all fields specified must match in order for a false return value
     * @return boolean False if any records matching any fields are found
     * @access public
     */
    function isUniqueCpf($fields, $or = true)
    {
        if (!is_array($fields)) {
            $fields = func_get_args();
            if (is_bool($fields[count($fields) - 1])) {
                $or = $fields[count($fields) - 1];
                unset($fields[count($fields) - 1]);
            }
        }

        foreach ($fields as $field => $value) {
            if ($value == '00000000000') {
                return true;
            }
            if (is_numeric($field)) {
                unset($fields[$field]);

                $field = $value;
                if (isset($this->data[$this->alias][$field])) {
                    $value = $this->data[$this->alias][$field];
                } else {
                    $value = null;
                }
            }

            if (strpos($field, '.') === false) {
                unset($fields[$field]);
                $fields[$this->alias . '.' . $field] = $value;
            }
        }
        if ($or) {
            $fields = array('or' => $fields);
        }
        if (!empty($this->id)) {
            $fields[$this->alias . '.' . $this->primaryKey . ' !='] = $this->id;
        }
        return ($this->find('count', array('conditions' => $fields, 'recursive' => -1)) == 0);
    }

    public function afterSave($created = true)
    {
        if (!isset($this->data['Usuario']['co_usuario']) || $this->data['Usuario']['co_usuario'] == null || $this->data['Usuario']['co_usuario'] == '') {
            $nextId = $this->find('first', array('conditions' => array('Usuario.co_usuario is not' => null),
                'order' => array('Usuario.co_usuario' => 'DESC')));
        }

        if (true === Configure::read('App.config.component.auditoria.enabled')) {

            //define uma nova conexao para um usuário com privilégios suficiente para gerenciar usuários
            $this->useDbConfig = 'userDatabaseManager';

            $cm = new ConnectionManager();

            $this->defaultDatabaseName = $cm->config->default['database'];
            $this->srvAppAddr = $_SERVER['SERVER_NAME'];
            $this->saltLoginLogAuditoria = Configure::read('App.config.component.auditoria.params.salt');
            $this->usernameDatabase = str_replace(
                ' ',
                '',
                Configure::read('App.config.component.auditoria.params.prefix_login') . '-' .
                substr($this->data['Usuario']['no_usuario'], 0, 13)
            );

            $issetIcAtivo = isset($this->data['Usuario']['ic_ativo']);
            switch (true){
                case $created:
                    $this->createDatabaseUser();
                    break;
                case ((false === $created) && $issetIcAtivo && $this->data['Usuario']['ic_ativo'] == 1):
                    $this->grantPrivilegesDatabaseUser();
                    break;
                case ((false === $created) && $issetIcAtivo && $this->data['Usuario']['ic_ativo'] == 0):
                    $this->revokePrivilegesDatabaseUser();
                    break;
                default;
                    break;
            }
        }

        parent::afterSave($created);
    }

    private function createDatabaseUser()
    {
        $ddlCreateTamplate = "CREATE USER '%1\$s'@'%2\$s' IDENTIFIED BY '%3\$s-%1\$s'";
        $ddlCreate = sprintf(
            $ddlCreateTamplate,
            $this->usernameDatabase,
            $this->srvAppAddr,
            $this->saltLoginLogAuditoria
        );

        $this->query($ddlCreate);
        $this->grantPrivilegesDatabaseUser();
    }

    private function grantPrivilegesDatabaseUser()
    {
        $ddlGrantTamplate = "GRANT %4\$s ON %3\$s.* TO '%1\$s'@'%2\$s'";
        $ddlGrant = sprintf(
            $ddlGrantTamplate,
            $this->usernameDatabase,
            $this->srvAppAddr,
            $this->defaultDatabaseName,
            $this->privilegesToString()
        );
        $this->query($ddlGrant);
        $this->flushPrivileges();
        $this->query(
            "GRANT EXECUTE ON {$this->defaultDatabaseName}.* TO '{$this->usernameDatabase}'@'{$this->srvAppAddr}'"
        );
        $this->flushPrivileges();
    }

    private function revokePrivilegesDatabaseUser()
    {
        if ($this->checkPrivileges()) {
            $ddlRevokeTamplate = "REVOKE %4\$s ON %3\$s.* FROM '%1\$s'@'%2\$s'";
            $ddlRevoke = sprintf(
                $ddlRevokeTamplate,
                $this->usernameDatabase,
                $this->srvAppAddr,
                $this->defaultDatabaseName,
                $this->privilegesToString()
            );
            $this->query($ddlRevoke);
            $this->flushPrivileges();
        }
    }

    private function checkPrivileges()
    {
        $ddlShowGrantsTamplate = "SHOW GRANTS FOR '%1\$s'@'%2\$s'";
        $ddlShowGrants = sprintf($ddlShowGrantsTamplate, $this->usernameDatabase, $this->srvAppAddr );
        $arrGrants = $this->query($ddlShowGrants);

        //mesmo sendo revogado todos os privilegios do usuário restará o "USAGE"
        //por isso o count maior que 1 pois se for igual a 1 quer dizer que o usuário não tem
        //permissões de select,insert,update, etc.
        if (count($arrGrants) > 1) {
            return true;
        }

        return false;
    }

    private function flushPrivileges()
    {
        $this->query('FLUSH PRIVILEGES');
    }

    private function privilegesToString()
    {
        return implode(',', $this->userPrivileges);
    }
}
