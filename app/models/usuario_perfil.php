<?php
class UsuarioPerfil extends AppModel
{

	public $name = 'UsuarioPerfil';

	public $useTable = 'usuario_perfis';

	public $primaryKey = 'co_usuario_perfil';

	public $validate = array(

        'co_perfil' => array(
            'notempty' => array(
                'rule' => array(
                    'notempty'
                ),
                'message' => 'Campo Perfil em branco'
            )
        )

    );

	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'co_usuario'
		),
		'Perfil' => array(
			'className' => 'Perfil',
			'foreignKey' => 'co_perfil'
		),
		'GrupoAuxiliar' => array(
			'className' => 'GrupoAuxiliar',
			'foreignKey' => 'co_usuario'
		)
	);
}