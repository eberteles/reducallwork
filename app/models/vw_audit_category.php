<?php

class VwAuditCategory extends AppModel
{
    var $name = 'VwAuditCategory';

    var $useTable = 'v_category';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

    public function getCombo()
    {
        $combo = array();
        foreach($this->find('all') as $row) {
            $operation = $row['VwAuditCategory']['operation'];
            $combo[$operation] = $operation;
        }

        return $combo;
    }

}
