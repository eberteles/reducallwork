<?php

class VwAuditUserAccessOccur extends AppModel
{
    var $name = 'VwAuditUserAccessOccur';

    var $useTable = 'v_user_access_occur';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
