<?php

class VwAuditUserHistory extends AppModel
{
    var $name = 'VwAuditUserHistory';

    var $useTable = 'v_user_history';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
