<?php

class VwAuditUserOccurDelete extends AppModel
{
    var $name = 'VwAuditUserOccurDelete';

    var $useTable = 'v_user_occur_delete';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
