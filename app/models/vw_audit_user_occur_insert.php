<?php

class VwAuditUserOccurInsert extends AppModel
{
    var $name = 'VwAuditUserOccurInsert';

    var $useTable = 'v_user_occur_insert';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
