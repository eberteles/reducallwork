<?php

class VwAuditUserOccurUpdate extends AppModel
{
    var $name = 'VwAuditUserOccurUpdate';

    var $useTable = 'v_user_occur_update';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
