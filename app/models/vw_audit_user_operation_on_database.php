<?php

class VwAuditUserOperationOnDatabase extends AppModel
{
    var $name = 'VwAuditUserOperationOnDatabase';

    var $useTable = 'v_user_operation_on_database';

    var $useDbConfig = 'logauditoria';

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

}
