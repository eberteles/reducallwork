<?php

class AjustardbShell extends Shell {
    var $uses = array(
            'Fornecedor'
    );
    
    function main() {
        
        $this->ajustarCNPJFornecedor();
        
    }
    
    function ajustarCNPJFornecedor() {
        $fornecedores   = $this->Fornecedor->listarCNPJaAjutar();
        foreach ( $fornecedores as $fornecedor ) {
            $cnpj   = $fornecedor['fornecedores']['nu_cnpj'];
            for ( $i = 14; strlen( $cnpj ) < $i; ) {
                $cnpj = "0" . $cnpj;
            }
            
            $this->Fornecedor->ajustarCNPJ($fornecedor['fornecedores']['co_fornecedor'], $cnpj);
        }
    }
    
}

?>