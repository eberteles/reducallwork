<?php

class AtualizapgtofinanceiroShell extends Shell {
    var $uses = array(
            'Pagamento',
        'Financeiro'
    );

    function main() {

        $this->getPagamentosGescon();
        $this->getPagamentosFinanceiro();
        $this->atualizaPagamentosFinanceiro();
        
    }
    
    function getPagamentosGescon()
    {
        return $this->Pagamento->getPagamentosGescon();
    }

    function getPagamentosFinanceiro()
    {
        $pagamentosGescon = $this->getPagamentosGescon();
        foreach($pagamentosGescon as $pagamentoGescon) :
            return $this->query('select g.nr_cgc_cpf, g.num_nf, g.nr_serie, g.dt_quitacao, g.dt_vencimento, g.vl_nf, g.vl_parcela
                                from ged_financeiro g
                                where g.nr_cgc_cpf = "' . $pagamentoGescon['nu_cnpj'] . '"'
                    . 'and g.num_nf = "' . $pagamentoGescon['nu_nota_fiscal'] . '"'
                    . 'and g.nr_serie = "' . $pagamentoGescon['nu_serie_nf'] . '"');
        endforeach;
    }

    function atualizaPagamentosFinanceiro()
    {
        $pagamentos = $this->getPagamentosFinanceiro();
        foreach($pagamentos as $pagamento) :
            return $this->Pagamento->atualizaPagamentosFinanceiro($pagamento['nr_cgc_cpf'],
                                                                  $pagamento['num_nf'],
                                                                  $pagamento['nr_serie'],
                                                                  $pagamento['dt_quitacao'],
                                                                  $pagamento['dt_vencimento'],
                                                                  $pagamento['vl_nf'],
                                                                  $pagamento['vl_parcela']);
        endforeach;
    }
    
}
?>