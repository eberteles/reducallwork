<?php

class AvisoShell extends Shell
{
    var $uses = array(
        'Rotina',
        'Contrato',
        'Aditivo',
        'Atividade',
        'Setor',
        'Usuario',
        'Fornecedor',
        'Cliente',
        'Alerta'
    );
    //var $tasks = array('Email');
    //var $Email;
    var $EmailProvider = null;
    var $helpers = array('Print');
    var $print = null;
    var $execucao = '';
    var $configuracao = '';

    function main()
    {
        App::import('Component', 'Functions');
        App::import('Component', 'Email');
        App::import('Helper', 'Print');
        $this->print = new PrintHelper();
        $this->EmailProvider = new EmailComponent();

        $dt_inicio = date('Y-m-d H:m:s');

        $this->configuracao = $this->Alerta->find('first');

        $this->avisarVencimentoContrato();

        $this->avisarVencimentoAutorizacaoAditivo(0);
        $this->avisarVencimentoAutorizacaoAditivo(10);

        $this->avisarAtividadeAtrasada();

        $this->Rotina->create();
        $this->Rotina->save(array('tp_rotina' => 'EC', 'dt_inicio' => $dt_inicio, 'dt_fim' => date('Y-m-d H:m:s'), 'ds_resultado' => $this->execucao));
    }

    function avisarVencimentoContrato()
    {
        // Contratos faltando 30 dias para Expirar
        $arContratosExpirando30 = $this->Contrato->listarContratosExpirando(30);
        if (count($arContratosExpirando30)) {
            $this->avisarVigencia($arContratosExpirando30, 30);
        }

        // Contratos faltando 60 dias para Expirar
        $arContratosExpirando60 = $this->Contrato->listarContratosExpirando(60);
        if (count($arContratosExpirando60)) {
            $this->avisarVigencia($arContratosExpirando60, 60);
        }

        // Contratos faltando 90 dias para Expirar
        $arContratosExpirando90 = $this->Contrato->listarContratosExpirando(90);
        if (count($arContratosExpirando90)) {
            $this->avisarVigencia($arContratosExpirando90, 90);
        }

        // Contratos faltando 120 dias para Expirar
        $arContratosExpirando120 = $this->Contrato->listarContratosExpirando(120);
        if (count($arContratosExpirando120)) {
            $this->avisarVigencia($arContratosExpirando120, 120);
        }

        // Contratos faltando 180 dias para Expirar
        $arContratosExpirando180 = $this->Contrato->listarContratosExpirando(180);
        if (count($arContratosExpirando180)) {
            $this->avisarVigencia($arContratosExpirando180, 180);
        }

        // Contratos com Suporte Encerrando em 60 duas
        if ($this->configuracao['Alerta']['ck_aviso_sup30'] == '1') {
            $contratosSup30 = $this->Contrato->find('all', array(
                'conditions' => array('GarantiaSuporte.dt_fim = ADDDATE( current_date(), 30)'),
                'recursive' => 0,
                'fields' => array('Contrato.*', 'Fornecedor.*', 'GestorAtual.*', 'GarantiaSuporte.*'),
                'joins' => array(
                    array(
                        'table' => 'garantias_suporte',
                        'alias' => 'GarantiaSuporte',
                        'conditions' => array(
                            'GarantiaSuporte.co_contrato = Contrato.co_contrato'
                        )
                    )
                )));
            $this->avisarTerminoSuporte($contratosSup30, 30);
        }
    }

    function avisarVencimentoAutorizacaoAditivo($prazo = 10)
    {
        App::import('Component', 'Functions');
        App::import('Helper', 'Print');
        $this->print = new PrintHelper();

        $dtPesquisa = new DateTime();
        if ($prazo > 0) {
            $dtPesquisa = $dtPesquisa->add(new DateInterval('P10D'));
        }

        $aditivos = $this->Aditivo->find('all', array(
            'conditions' => array('Aditivo.dt_fim_autorizacao' => $dtPesquisa->format('Y-m-d'))
        ));

        $nuRegistros = 0;
        foreach ($aditivos as $aditivo) {
            $contrato = $this->Contrato->find('first', array(
                'recursive' => 0,
                'conditions' => array('Contrato.co_contrato' => $aditivo['Aditivo']['co_contrato'])
            ));

            if (isset($contrato['GestorAtual']) && $contrato['GestorAtual']['ds_email'] != '') {
                $mensagem = $this->getTextoTerminoAutorizacaoAditivo($contrato, $aditivo, $contrato['GestorAtual']['ds_nome'], $prazo);
                $this->enviar('Término de Autorização do Aditivo', array($contrato['GestorAtual']['ds_nome'] . ' <' . $contrato['GestorAtual']['ds_email'] . '>'), $mensagem);
            }

            if (isset($contrato['FiscalAtual']) && $contrato['FiscalAtual']['ds_email'] != '') {
                $mensagem = $this->getTextoTerminoAutorizacaoAditivo($contrato, $aditivo, $contrato['FiscalAtual']['ds_nome'], $prazo);
                $this->enviar('Término de Autorização do Aditivo', array($contrato['FiscalAtual']['ds_nome'] . ' <' . $contrato['FiscalAtual']['ds_email'] . '>'), $mensagem);
            }

            $nuRegistros++;
        }
        if ($nuRegistros > 0) {
            $this->execucao .= ' Autorização Aditivo - ' . $prazo . ': ' . $nuRegistros;
        }
        echo $this->execucao;
    }

    function avisarAtividadeAtrasada()
    {
        $atividades = $this->Atividade->find('all', array(
            'conditions' => array('Atividade.pc_executado <> ' => 100, 
            'DATEDIFF(current_date(), Atividade.dt_fim_planejado) < ' => 5,
            'Atividade.dt_fim_planejado < ' => date('Y-m-d'))
        ));

        $nuRegistros = 0;
        foreach ($atividades as $atividade) {
            $mensagem = "<p style='color:#000'><h2>" . $atividade['Usuario']['ds_nome'] .
                ",</h2><p>Informamos que a atividade: " . $atividade['Atividade']['ds_atividade'] .
                ", do contrato: " . $this->contrato($atividade['Contrato']['nu_contrato']) .
                " com periodicidade: " . $atividade['Atividade']['periodicidade'] .
                ", não foi concluída no prazo de " . $atividade['Atividade']['dt_ini_planejado'] . " á " . $atividade['Atividade']['dt_fim_planejado'] . ", que foi determinado.</p>" .
                "<p><strong>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</strong></p>";
            $this->enviar('Pendencia de Atividade', array($atividade['Usuario']['ds_nome'] . ' <' . $atividade['Usuario']['ds_email'] . '>'), $mensagem);
            $nuRegistros++;
        }
        if ($nuRegistros > 0) {
            $this->execucao .= ' Atividades Atrasadas: ' . $nuRegistros;
        }
    }

    function avisarVigencia($listaContratos, $prazo)
    {
        $nuRegistros = 0;
        foreach ($listaContratos as $contrato) {
            $subject = ' Contrato expirando em ' . $prazo . ' dias';
            $fornecedor = array();
            if( isset($contrato['contratos']['co_fornecedor']) && $contrato['contratos']['co_fornecedor'] > 0) {
                $fornecedor = $this->Fornecedor->findByCoFornecedor($contrato['contratos']['co_fornecedor']);
            }

            // Gestor
            if ($this->configuracao['Alerta']['ck_aviso' . $prazo] == '1') {
                $gestor = $this->Usuario->find('first', array('conditions' => array('Usuario.co_usuario' => $contrato['contratos']['co_gestor_atual'])));

                if (isset($gestor['Usuario']) && $gestor['Usuario']['ds_email'] != '') {
                    $mensagem = $this->getTextoVigencia($prazo, $contrato,
                        'Prezado(a) ' . ucwords($gestor['Usuario']['ds_nome']), $fornecedor,
                        $contrato['contratos']['ds_objeto']);
                    $this->enviar($subject, array($gestor['Usuario']['ds_nome'] . ' <' . $gestor['Usuario']['ds_email'] . '>'), $mensagem);
                }
            }

            // Unidade
            if ($this->configuracao['Alerta']['ck_aviso_uni' . $prazo] == '1') {
                $unidade = $this->Usuario->find('first', array('conditions' => array('Usuario.co_usuario' => $contrato['contratos']['co_gestor_atual'])));

                if (isset($unidade['Setor']) && $unidade['Setor']['ds_email'] != '') {
                    $mensagem = $this->getTextoVigencia($prazo, $contrato,
                        $unidade['Setor']['ds_setor'], $fornecedor,
                        $contrato['contratos']['ds_objeto']);
                    $this->enviar($subject, array($unidade['Setor']['ds_setor'] . ' <' . $unidade['Setor']['ds_email'] . '>'), $mensagem);
                }
            }

            // Fiscal
            if ($this->configuracao['Alerta']['ck_aviso_fis' . $prazo] == '1') {
                $fiscal = $this->Usuario->find('first', array('conditions' => array('first', 'Usuario.co_usuario' => $contrato['contratos']['co_fiscal_atual'])));

                if (isset($fiscal['Usuario']) && $fiscal['Usuario']['ds_email'] != '') {
                    $mensagem = $this->getTextoVigencia($prazo, $contrato,
                        'Prezado(a) ' . $fiscal['Usuario']['ds_nome'], $fornecedor,
                        $contrato['contratos']['ds_objeto']);
                    $this->enviar($subject, array($fiscal['Usuario']['ds_nome'] . ' <' . $fiscal['Usuario']['ds_email'] . '>'), $mensagem);
                }
            }

            // Fornecedor
            if ($this->configuracao['Alerta']['ck_aviso_for' . $prazo] == '1' && isset($fornecedor['Fornecedor']) && $fornecedor['Fornecedor']['ds_email'] != '') {

                $mensagem = $this->getTextoVigencia($prazo, $contrato,
                    $fornecedor, '', $contrato['contratos']['ds_objeto']);
                $this->enviar($subject, array($fornecedor['Fornecedor']['no_razao_social'] . ' <' . $fornecedor['Fornecedor']['ds_email'] . '>'), $mensagem);
            }

            // Outros Emails
            $outros = $this->Usuario->find('all', array('conditions' => array('Usuario.ic_ativo' => 1, 'Usuario.ck_aviso' . $prazo => 1)));
            foreach ($outros as $outro) {

                if ($outro['Usuario']['ds_email'] != '') {
                    $mensagem = $this->getTextoVigencia($prazo, $contrato,
                        'Prezado(a) ' . ucwords($outro['Usuario']['ds_nome']), $fornecedor,
                        $contrato['contratos']['ds_objeto']);
                    $this->enviar($subject, array($outro['Usuario']['ds_nome'] . ' <' . $outro['Usuario']['ds_email'] . '>'), $mensagem);
                }
            }

            $nuRegistros++;
        }

        $this->execucao .= ' Prazo ' . $prazo . ': ' . $nuRegistros;

    }

    function avisarTerminoSuporte($listaContratos, $prazo)
    {
        $nuRegistros = 0;
        foreach ($listaContratos as $contrato) {
            $subject = ' Contrato com Suporte de Garantia terminando em ' . $prazo . ' dias';

            if (isset($contrato['GestorAtual']) && $contrato['GestorAtual']['ds_email'] != '') {
                $mensagem = $this->getTextoTerminoSuporte($prazo, $contrato['Contrato']['nu_processo'], $contrato['Contrato']['nu_contrato'],
                    'Prezado(a) ' . ucwords($contrato['GestorAtual']['ds_nome']), $contrato['GarantiaSuporte'], strtoupper($contrato['Fornecedor']['no_razao_social']));
                $this->enviar($subject, array($contrato['GestorAtual']['ds_nome'] . ' <' . $contrato['GestorAtual']['ds_email'] . '>'), $mensagem);
            }

            $nuRegistros++;
        }

        $this->execucao .= ' Prazo ' . $prazo . ': ' . $nuRegistros;

    }

    function getTextoVigencia($prazo, $contrato, $destinatario, $fornecedor = null, $objeto = null)
    {
        $mensagem = $destinatario . ',<br><br>';
        $mensagem .= '&nbsp;&nbsp;&nbsp;O Contrato abaixo está expirando em ' . $prazo . ' dias: <br><br>';
        if ( isset($fornecedor['Fornecedor']) && $fornecedor['Fornecedor']['no_razao_social'] != '' ) {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>' . __('Fornecedor', true) . '</b>: ' . strtoupper($fornecedor['Fornecedor']['no_razao_social']);
        }
        if ( $contrato['contratos']['co_cliente'] > 0  ) {
            $cliente = $this->Cliente->findByCoCliente($contrato['contratos']['co_cliente']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>' . __('Cliente', true) . '</b>: ' . strtoupper($cliente['Cliente']['no_razao_social']);
        }
        if ($contrato['contratos']['nu_processo'] != '') {
            $mensagem .= '&nbsp;&nbsp;&nbsp;<b>Processo</b>: ' . $this->processo($contrato['contratos']['nu_processo']);
        }
        if ( $contrato['contratos']['nu_contrato'] != '') {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Contrato</b>: ' . $this->contrato($contrato['contratos']['nu_contrato']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Assinatura</b>: ' . FunctionsComponent::data($contrato['contratos']['dt_assinatura']);
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Vigência</b>: ' . FunctionsComponent::data($contrato['contratos']['dt_ini_vigencia']) . ' à ' . FunctionsComponent::data($contrato['contratos']['dt_fim_vigencia']);
        }
        if ($objeto != '') {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Objeto</b>: ' . $objeto;
        }
        $mensagem .= '<br><br><br> -------------------------------------------------------------------';
        $mensagem .= '<br>Este é um aviso automático, favor não responder este email.';
        return $mensagem;
    }

    function getTextoTerminoSuporte($prazo, $nu_processo, $nu_contrato, $destinatario, $garantia = null, $fornecedor = null)
    {
        $mensagem = $destinatario . ',<br><br>';
        $mensagem .= '&nbsp;&nbsp;&nbsp;O Contrato abaixo está com a Garantia de Suporte terminando em ' . $prazo . ' dias: <br><br>';
        if ($nu_processo != '') {
            $mensagem .= '&nbsp;&nbsp;&nbsp;<b>Processo</b>: ' . $this->processo($nu_processo);
        }
        if ($nu_contrato != '') {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Contrato</b>: ' . $this->contrato($nu_contrato);
        }
        if ($fornecedor != '') {
            $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>' . __('Fornecedor', true) . '</b>: ' . $fornecedor;
        }
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Nº de Série</b>: ' . $garantia['nu_serie'];
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Período da Garantia</b>: ' . $garantia['dt_inicio'] . ' à ' . $garantia['dt_fim'];
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Descrição</b>: ' . $garantia['ds_garantia_suporte'];
        $mensagem .= '<br><br><br> -------------------------------------------------------------------';
        $mensagem .= '<br>Este é um aviso automático, favor não responder este email.';
        return $mensagem;
    }

    function getTextoTerminoAutorizacaoAditivo($contrato, $aditivo, $destinatario, $termino = 0)
    {
        $mensagem = 'Prezado(a) ' . $destinatario . ',<br><br>&nbsp;&nbsp;&nbsp;';

        if ($termino > 0) {
            $mensagem .= 'Faltam ' . $termino . ' dias para encerrar o prazo final de autorização do Aditivo. Verifique se o mesmo já foi autorizado.<br><br>';
        } else {
            $mensagem .= 'Encerra hoje o prazo para autorização do Aditivo. Verifique se o mesmo já foi autorizado.<br><br>';
        }

        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Nº Contrato</b>: ' . $this->contrato($contrato['Contrato']['nu_contrato']);
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Descrição</b>: ' . $aditivo['Aditivo']['no_aditivo'];
        $mensagem .= '<br>&nbsp;&nbsp;&nbsp;<b>Término Autorização</b>: ' . $aditivo['Aditivo']['dt_fim_autorizacao'];
        $mensagem .= '<br><br><br> -------------------------------------------------------------------';
        $mensagem .= '<br>Este é um aviso automático, favor não responder este email.';
        return $mensagem;
    }

    function enviar($subject, $to, $mensagem)
    {
//        $this->Email->send(array(
//            'subject' => $subject,
//            'to' => $to,
//            'sendAs' => 'html'
//        ), $mensagem);
        
        $this->EmailProvider->to = $to;

        $this->EmailProvider->subject = $subject;

        $this->EmailProvider->send($mensagem);
        
    }


    function processo($nuProcesso)
    {
        return $this->print->processo($nuProcesso);
    }

    function contrato($nuContrato)
    {
        return $this->print->contrato($nuContrato);
    }

    function teste()
    {
        $to = array();
        $to[] = 'Salles <salles@n2oti.com>';

        $this->Email->send(array(
            'subject' => 'Assunto de Envio',
            'to' => $to), 'Informamos que é um teste.');
    }
}
