<?php

/**
 * 
 * ESSA CLASSE DEVE SER EXECUTADA A CADA 5 MINUTOS
 * 
 */

class AvisosrapidosShell extends Shell {
    var $uses = array(
            'Rotina',
            'Andamento'
    );
    var $tasks = array('Email');
    var $Email;
    var $helpers = array('Print');
    var $print   = null;
    var $execucao   = '';
    var $configuracao   = '';
    
    function main() {
        App::import('Component', 'Functions');
        App::import('Helper', 'Print');
        $this->print = new PrintHelper();
        
        $dt_inicio  = date('Y-m-d H:m:s');
        
        $this->verificarFasesAtrasadas();
        
        //$this->verificarEnvioEmail();
        
        $this->Rotina->create();
        $this->Rotina->save( array( 'tp_rotina' => 'AR', 'dt_inicio' => $dt_inicio, 'dt_fim' => date('Y-m-d H:m:s'), 'ds_resultado' => $this->execucao) );
    }
    
    function verificarEnvioEmail() {
        App::import('Model', 'Email');
        $emailDb    = new Email();
        $emails = $emailDb->find('all', array( 'conditions'=>array('envio is null') ));
        $nuRegistros    = 0;
        foreach ( $emails as $email ) {
            $this->enviar($email['Email']['assunto'], $email['Email']['destinatario'], $email['Email']['mensagem']);
            $emailDb->id = $email['Email']['id'];
            $emailDb->saveField("envio", date('Y-m-d H:m:s'));
            $nuRegistros++;
        }
        $this->execucao .= ' Emails enviados: ' . $nuRegistros;
    }
    
    function verificarFasesAtrasadas() {
        App::import('Model', 'Email');
        $emailDb    = new Email();
        
        $fases  = $this->Andamento->listarProcessosFaseExpiranda();
        $nuRegistros    = 0;
        foreach ( $fases as $fase ) {            
            $email  = array();
            $email['Email']['co_setor'] = $fase['Setor']['co_setor'];
            $email['Email']['assunto']  = 'Processo com Fase atrasada no Setor';
            $email['Email']['mensagem'] = 'O Processo abaixo encontra-se com a sua Fase de Execução atrasada: <br>' . 
                    'PAM: ' . $fase['Contrato']['nu_pam'] . '<br>' .
                    'Setor: ' . $fase['Setor']['ds_setor'] . '<br>' .
                    'Fase: ' . $fase['Fase']['ds_fase'] . '<br>' .
                    'Início da Fase: ' . $this->Andamento->dataHora($fase['Andamento']['dt_andamento']) . '<br>' .
                    'Término Previsto: ' . $this->Andamento->dataHora($fase[0]['pz_execucao']);
            $email['Email']['destinatario'] = $fase['Setor']['ds_setor'] . ' <' . $fase['Setor']['ds_email'] . '>';
            
            //$emailDb->create();
            //$emailDb->save($email);
            
            $this->enviar($email['Email']['assunto'], $email['Email']['destinatario'], $email['Email']['mensagem']);
            
            $this->Andamento->id = $fase['Andamento']['co_andamento'];
            $this->Andamento->saveField("ic_aviso_expiracao", 1);
            
            $nuRegistros++;
        }
        $this->execucao .= ' Fases atrasadas: ' . $nuRegistros;
    }
            
    function enviar($subject, $to, $mensagem) {
        $this->Email->send( array(
            'subject' => $subject,
            'bcc' => array('Suporte N2 <eber@n2oti.com>'),
            'to' => $to ), $mensagem);
    }

    function teste() {
        $to = array();
        $to[] = 'Eber <eber@n2oti.com>'; 
        $to[] = 'Palmeira <eberteles@hotmail.com>'; 

        $this->Email->send( array( 
                    'subject' => 'Assunto de Envio',
                    'to' => $to), 'Informamos que é um teste.' );
    }
    
}

?>