<?php

/**
 * Rotina implementada para recuperar informações do serviço do portal comprasnet
 *
 */

App::import('Helper', 'Modulo');
App::import('Component', 'Session');

class ComprasnetShell extends Shell
{
	/**
	 * @var boolean
	 */
	var $debugger = true;
	
	/**
	 * @var string
	 */
	var $screenDebugger = false;
	
	/**
	 * @var array
	 */
	var $uses = array(
		'Uasg',
	);
	
	private function _debug($trace, $dots = false)
	{
		if ($this->debugger) {
			$d = ($dots) ? '...' : '';
			echo $trace . $d . PHP_EOL;
		}
	}
	
	/**
	 * chamada: cake comprasnet [screenDebug]
	 */
	function main()
	{
		if (isset($this->args[0])) {
			$this->screenDebugger = (boolean)$this->args[0];
		}
		
		if (!$this->screenDebugger) {
			ob_start();
		}
		
		$this->_debug('...[COMPRASNET] Started [' . date('Y-m-d h:i:s') . ']', true);
				
		if (isset($this->args[1])) {
			$nuAnoIni = $this->args[1];
		}
		
		$nuAnoFim = date('Y');
		
		try {			
			$roboUrl = Configure::read('App.config.resource.urls.roboComprasnet');
			
			$arrUasgs = $this->Uasg->find('all');
			
			foreach( $arrUasgs as $arrUasg ) {				
				for( $RETROATIVO = 0; $RETROATIVO <= ($nuAnoFim-$nuAnoIni); $RETROATIVO++ ){				
					//$urlParams = array("uasg" => $arrUasg['Uasg']['uasg'], "retroativo" => $RETROATIVO, "updatedb" => 1);
					$urlParams = "uasg=" . $arrUasg['Uasg']['uasg'] . "&retroativo=" . $RETROATIVO . "&updatedb=1";					
					$urlParams = trim($urlParams);
					$urlExec = $roboUrl . "?" . $urlParams;
					$this->_debug(" ... URL REQUEST ... URL: " . $urlExec . PHP_EOL);
					
					$ch = curl_init($urlExec);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					$data = curl_exec($ch);
					curl_close($ch);			
				}
			}
		} catch (\Exception $e) {
			echo $e->getMessage() . PHP_EOL;
		}
		
		$this->_debug('...[COMPRASNET] Stoped [' . date('Y-m-d h:i:s') . ']' . PHP_EOL . PHP_EOL, true);
		
		if (!$this->screenDebugger) {
			$strLog = ob_get_contents();
			ob_end_clean();
			$filename = APP_PATH . 'tmp/logs/shell_comprasnet_' . date('Ymd');
			$fLog = fopen($filename, 'a+');
			fwrite($fLog, $strLog);
			fclose($fLog);
		}
	}
}