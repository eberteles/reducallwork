<?php

/**
 * Rotina implementada para recuperar informações do serviço do portal compras.dados.gov.br
 *
 */

App::import('Helper', 'Modulo');
App::import('Component', 'Session');

class DadosgovbrShell extends Shell
{
	/**
	 * @var boolean
	 */
	var $debugger = true;
	
	/**
	 * @var string
	 */
	var $screenDebugger = false;
	
	var $urls = array(
		'tipo_contrato' => 'http://compras.dados.gov.br/contratos/v1/tipos_contrato.json',
		'uasg' => 'http://compras.dados.gov.br/licitacoes/v1/uasgs.json'
	);
	
	/**
	 * @var array
	 */
	var $uses = array(
		'Uasg',
		'Modalidade'
	);
	
	private function _debug($trace, $dots = false)
	{
		if ($this->debugger) {
			$d = ($dots) ? '...' : '';
			echo $trace . $d . PHP_EOL;
		}
	}
	
	/**
	 * chamada: cake Dadosgovbr [screenDebug]
	 */
	function main()
	{
		$servico = null;
		$codigo = null;
		
		if (isset($this->args[0])) {
			$this->screenDebugger = (boolean)$this->args[0];
		}
		
		if (!$this->screenDebugger) {
			ob_start();
		}
		
		$this->_debug('...[COMPRAS.DADOS.GOV.BR] Started [' . date('Y-m-d h:i:s') . ']', true);
		
		// tipo do dado a ser importador
		if (isset($this->args[1])) {
			$servico = $this->args[1];
		}
		
		if( isset($this->args[2]) ){
			$codigo = $this->args[2];
		}
		
		try {
			$url = null;
			
			if( isset($this->urls[$servico]) ){
				$url = $this->urls[$servico];
			}
			
			$params = array();
			
			if( $servico == 'uasg' ){
				$params = array(
					'id_orgao' => $codigo,
					'ativo' => 1
				);
			}
			
			if( !empty($url) ){
				$urlExec = $url;
				
				if( !empty($params) ){
					$urlExec .= "?" . http_build_query($params);
				}
				
				$ch = curl_init($urlExec);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$data = curl_exec($ch);
				curl_close($ch);
				$json = json_decode($data);
				
				if( $servico == 'uasg' ) {
					
					foreach( $json->_embedded->uasgs as $uasg ){
						
						$arrData = array(
							'Uasg' => array(
								'uasg' 		=> $uasg->id,
								'ds_nome'	=> $uasg->nome,
							)
						);
						
						$this->Uasg->create();
						if( $this->Uasg->save($arrData) ){
							$this->_debug(" ... UASG SALVA:  ... " . $uasg->id);
						} else {
							$this->_debug(" ... UASG NÃO SALVA:  ... " . $uasg->id);
						}
					}
					
				}
				
				if( $servico == 'tipo_contrato' ) {
					foreach ( $json->_embedded->TiposContrato as $tipoContrato ) {
						$arrData = array(
							'Modalidade' => array(
								'ds_modalidade' => $tipoContrato->codigo . " - " . $tipoContrato->descricao,
								'ic_ativo' => 1
							)
						);
						
						$this->Modalidade->create();
						if( $this->Modalidade->save($arrData) ){
							$this->_debug(" ... TIPO CONTRATO SALVO:  ... " . $tipoContrato->codigo . " - " . $tipoContrato->descricao);
						} else {
							$this->_debug(" ... TIPO CONTRATO NÃO SALVO:  ... " . $tipoContrato->codigo . " - " . $tipoContrato->descricao);
						}
					}
				}
				
			}
		} catch (\Exception $e) {
			echo $e->getMessage() . PHP_EOL;
		}
		
		$this->_debug('...[COMPRAS.DADOS.GOV.BR] Stoped [' . date('Y-m-d h:i:s') . ']' . PHP_EOL . PHP_EOL, true);
		
		if (!$this->screenDebugger) {
			$strLog = ob_get_contents();
			ob_end_clean();
			$filename = APP_PATH . 'tmp/logs/shell_comprasnet_' . date('Ymd');
			$fLog = fopen($filename, 'a+');
			fwrite($fLog, $strLog);
			fclose($fLog);
		}
	}
}