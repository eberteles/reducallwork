<?php

// CASO ESSA CLASSE VENHA A SER UTILIZADA, FAVOR REESCREVE-LA DENTRO DO AVISO.PHP

// Por favor, não remover os requires, mesmo se estiverem comentados

require getcwd() . DS . 'cake' . DS . 'libs' . DS . 'view' . DS . 'helpers' . DS . 'app_helper.php';
require getcwd() . DS . 'app' . DS . 'views' . DS . 'helpers' . DS . 'cliente.php';
require getcwd() . DS . 'app' . DS . 'views' . DS . 'helpers' . DS . 'modulo.php';

class GarantiaShell extends Shell {

    function main() {
        App::import('Vendor', 'phpmailer', array(
            'file' => 'phpmailer' . DS . 'class.phpmailer.php'
        ));

        // App::import('Helper', 'Html');
        // $html = new HtmlHelper;   
        
        App::Import('ConnectionManager');
        $ds = ConnectionManager::getDataSource('default');

        $mail = new PHPMailer();
        $dsn = 'mysql:host=' . $ds->config['host'] . ';dbname=' . $ds->config['database'];
        $user = $ds->config['login'];
        $pass = $ds->config['password'];
        
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = "ssl://smtp.gmail.com";
        // $mail->Host = '200.234.2a10.14';
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->Username = 'gescon.aviso@gmail.com';
        $mail->Password = 'contratos2015';
        
        $mail->From = 'gescon.aviso@gmail.com'; // Your Mail
        $mail->FromName = '[GESCON]'; // Your Name
        
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = '[GESCON] - Aviso';

        // $mail->ClearAllRecipients();
        // $mail->ClearAttachments();
        $con = null;

        try {
            // $con = new Pdo('mysql:host=mysql.n2oti.com;dbname=n2oti03', 'n2oti03', 'jotas03');
            $con = new Pdo($dsn, $user, $pass);
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $data = 'c.dt_ini_processo';
            $fiscal = 'c.co_fiscal_atual';
            $select = "SELECT DATE_FORMAT(%s, '%%d/%%m/%%Y') as iniProcesso, u.ds_email as usuarioEmail, u.ds_nome as usuarioNome, c.nu_processo as nuProcesso, c.nu_contrato as nuProcesso FROM contratos c join usuarios u on (%s = u.co_usuario) WHERE c.ic_cadastro_garantia = 0 and (DATEDIFF(CURDATE(), %s) = 120 || DATEDIFF(CURDATE(), %s) = 60 || DATEDIFF(CURDATE(), %s) = 30)";
            
            try {
                // processos com fiscais responsáveis
                $query = sprintf($select, $data, $fiscal, $data, $data, $data);
                $result1 = $con->query($query . " and c.nu_contrato = ''")->fetchAll(PDO::FETCH_OBJ);

                // processos com gestores responsáveis
                $fiscal = 'c.co_gestor_atual';
                $query = sprintf($select, $data, $fiscal, $data, $data, $data);
                $result2 = $con->query($query . " and c.nu_contrato = ''")->fetchAll(PDO::FETCH_OBJ);

                // contratos fiscais
                $fiscal = 'c.co_fiscal_atual';
                $data = 'c.dt_ini_vigencia';
                $query = sprintf($select, $data, $fiscal, $data, $data, $data);
                $result3 = $con->query($query)->fetchAll(PDO::FETCH_OBJ);

                // contratos gestores
                $fiscal = 'c.co_gestor_atual';
                $query = sprintf($select, $data, $fiscal, $data, $data, $data);
                $result4 = $con->query($query)->fetchAll(PDO::FETCH_OBJ);

            } catch(Exception $e) {
                $this->out($e->getMessage());
            }
            
            // processos 
            // $queryFiscaisProcessos = "SELECT DATE_FORMAT(c.dt_ini_processo, '%d/%m/%Y') as iniProcesso, u.ds_email as usuarioEmail, u.ds_nome as usuarioNome, c.nu_processo as nuProcesso FROM contratos c join usuarios u on (c.co_fiscal_atual = u.co_usuario) 
            // WHERE c.ic_cadastro_garantia = 0 and c.nu_contrato = '' and (DATEDIFF(CURDATE(), c.dt_ini_processo) = 120 || DATEDIFF(CURDATE(), c.dt_ini_processo) = 60 || DATEDIFF(CURDATE(), c.dt_ini_processo) = 30)";

            // $queryGestoresProcessos = "SELECT DATE_FORMAT(c.dt_ini_processo, '%d/%m/%Y') as iniProcesso, u.ds_email as usuarioEmail, u.ds_nome as usuarioNome, c.nu_contrato as nuContrato, c.nu_processo as nuProcesso FROM contratos c join usuarios u on (c.co_gestor_atual = u.co_usuario) 
            // WHERE c.ic_cadastro_garantia = 0 and (DATEDIFF(CURDATE(), c.dt_ini_processo) = 120 || DATEDIFF(CURDATE(), c.dt_ini_processo) = 60 || DATEDIFF(CURDATE(), c.dt_ini_processo) = 30)";

            // $queryFiscaisContratos = "SELECT DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as iniVigencia, u.ds_email as usuarioEmail, u.ds_nome as usuarioNome, c.contrato as nuContrato FROM contratos c join usuarios u on (c.co_fiscal_atual = u.co_usuario) 
            // WHERE c.ic_cadastro_garantia = 0 and (DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 120 || DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 60 || DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 30)";

            // $queryGestoresContratos = "SELECT DATE_FORMAT(c.dt_ini_vigencia, '%d/%m/%Y') as iniVigencia, u.ds_email as usuarioEmail, u.ds_nome as usuarioNome, c.nu_contrato as nuContrato, c.nu_processo as nuProcesso FROM contratos c join usuarios u on (c.co_gestor_atual = u.co_usuario) 
            // WHERE c.ic_cadastro_garantia = 0 and (DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 120 || DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 60 || DATEDIFF(CURDATE(), c.dt_ini_vigencia) = 30)";

            $this->out('(fiscais)contagem de contratos entre 30, 60, 120 dias: ' . debug($result1));
            $this->out('(gestores)contagem de contratos entre 30, 60, 120 dias: ' . debug($result2));
            $this->out('(fiscais)contagem de processos entre 30, 60, 120 dias: ' . debug($result3));
            $this->out('(gestores)contagem de processos entre 30, 60, 120 dias: ' . debug($result4));exit;

            $mail->AddEmbeddedImage(getcwd() . '/app/webroot/img/logo-gescon-email.png', 'logo_2u');
            
            $contratos = array_merge($result1, $result2);
            
            if (count($contratos)) {
                foreach ($contratos as $index => $contrato) {
                    // if ($index == 1) {
                    //     break;
                    // }
                    
                    // $mail->AddAddress($atividade->emailUsuario);
                    $mail->AddAddress('renan@n2oti.com');
                    
                    $mail->Body = '<img src="cid:logo_2u" />';
                    
                    $mail->Body .= "<p>O contrato/processo de nº: {$contrato['nuContrato']}</p>";

                    $mail->Body .= '<p><strong>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</strong></p>';
                        
                    $mail->Send();
                } 
            } else {
                $this->out('Não existem contratos entre entre 30, 60, 120 dias');
            }
                 
        } catch (PDOException $e) {
            $this->out('Não foi possível realizar a conexão com o banco de dados');
        }
        
    }

    function teste() {
        App::import('Vendor', 'phpmailer', array(
            'file' => 'phpmailer' . DS . 'class.phpmailer.php'
        ));
        
        $mail = new PHPMailer();
        
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = "ssl://smtp.gmail.com";
        // $mail->Host = '200.234.2a10.14';
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->Username = 'gescon.aviso@gmail.com';
        $mail->Password = 'contratos2015';
        
        $mail->From = 'renan@n2oti.com'; // Your Mail
        $mail->FromName = 'renan'; // Your Name
        $mail->AddAddress('renan@n2oti.com');
        
        // if (isset($tos) && is_array($tos)) {
        //     foreach ($tos as $to) {
        //         $mail->AddAddress($to);
        //     }
        // } else{
        //     $mail->AddAddress($tos);
        // }
        
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'assunto teste';
        $mail->Body = 'teste messsage';
        
        return $mail->Send();
        
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();
    }
}
