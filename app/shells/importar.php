<?php

class ImportarShell extends Shell {
    var $uses = array(
            'Rotina',
            'Banco',
            'Setor',
            'Pais'
    );
    var $execucao   = '';
    var $pasta      = '/app/sql/';
    var $arquivo    = '';
    var $tipo       = 0;
    
    function main() {
        $this->arquivo  = $this->args[0];
        $this->tipo     = $this->args[1];
        echo 'arquivo ' . $this->arquivo;
        echo 'tipo ' . $this->tipo;
        $dt_inicio  = date('Y-m-d H:m:s');
        
        $delimitador    = ';';
        if($this->arquivo != '') {
            $file   = fopen(ROOT . $this->pasta . $this->arquivo, 'r');
            while (($data = fgetcsv($file, 0, $delimitador)) !== FALSE) {

                if($this->tipo == 1) {
                    $this->Banco->create();
                    $this->Banco->save( array( 'nu_banco' => $data[0], 'ds_banco' => $data[1]) );
                }
                if($this->tipo == 2) {
                    $setor  = array();
                    $setor['co_setor']  = $data[0];
                    if($data[4] > 0) {
                        $setor['parent_id'] = $data[4];
                    }
                    $setor['ds_setor']  = rtrim($data[1]) . ' - ' . $data[2];
                    $this->Setor->create();
                    $this->Setor->save( $setor );
                }
                if($this->tipo == 3) {
                    $this->Pais->create();
                    $this->Pais->save( array( 'co_pais' => $data[0], 'no_pais' => $data[1]) );
                }
            }
            fclose ($file);
        }
        
        $this->Rotina->create();
        $this->Rotina->save( array( 'tp_rotina' => 'IM', 'dt_inicio' => $dt_inicio, 'dt_fim' => date('Y-m-d H:m:s'), 'ds_resultado' => $this->execucao) );
    }
    
}

?>