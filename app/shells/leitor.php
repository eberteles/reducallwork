<?php

class LeitorShell extends Shell {

    var $uses = array('Layout', 'Fornecedor', 'Contrato', 'Contratacao', 'Empenho', 'Siafi');
    private $pasta = "/arquivos/";
    private $nomeArquivo = "";

    public function main() {
        $layouts = $this->Layout->find('all');
        $this->out("Inicio do processamento, aguarde o fim do processamento!");
        try {
            $this->lerArquivo($layouts);
        } catch (Exception $e) {
            $this->out($e->getMessage());
            $this->gravaErroMovimento($e->getMessage());
            $this->out("Finalizacao inesperada, verifique o log de erro do movimento.");
            die();
        }
        $this->out("Fim do processamento ...");
    }

    protected function lerArquivo($layouts) {
        try {
            $path = ROOT . $this->pasta;
            $listaArquivos = array_diff(scandir($path), array('..', '.'));
            foreach ($listaArquivos as $file) {
                if ($this->isMovimentoValido($file)) {
                    $this->nomeArquivo = $file;
                    $registros = array();
                    $this->gravaMovimento($file);
                    $arquivo = fopen($path . $file, 'r');
                    if (filesize($path . $file) <= 0) {
                        $this->gravaErroMovimento("O arquivo $file esta vazio.");
                        fclose($arquivo);
                        continue;
                    }

                    while (!feof($arquivo)) {
                        $linha = fgets($arquivo);
                        $registros[] = $this->transformaRegistro($layouts, $linha);
                    }
                    fclose($arquivo);
                    $this->critica($registros);
                    $this->finalizaMovimento($file);
                    $this->gravarEmpenho($registros);
                }
            }
            return true;
        } catch (Exception $e) {
            $this->gravaErroMovimento($e->getMessage());
            throw $e;
        }
    }

    protected function transformaRegistro($layouts, $linha) {
        $registro = array();
        for ($i = 1; $i < count($layouts); $i++) {
            $layout = array_pop($layouts[$i]);
            $registro[$layout['ds_campo']] = substr($linha, $layout['no_ini_campo'], $layout['no_tam_campo']) . "\n";
        }
        return $registro;
    }

    protected function critica($registros) {
        foreach ($registros as $registro) {
            $contrato = array();
            try {
                $contrato = $this->recuperaContrato($registro['IT-NU-PROCESSO']);
            } catch (Exception $exc) {
                $this->gravaErroMovimento("Numero do processo " . $registro['IT-NU-PROCESSO'] . " nao cadastrado na base de dados do GESCON.");
                continue;
            }

            $fornecedor = array();
            // critica fornecedor
            try {
                $fornecedor = $this->buscaFornecedor($registro['IT-CO-FAVORECIDO']);
            } catch (Exception $e) {
                $this->gravaErroMovimento("CNPJ do Fornecedor: [" . $registro['IT-CO-FAVORECIDO'] . "] nao cadastrado na base de dados do GESCON.");
            }

            if (!empty($fornecedor)) {
                if ($contrato['co_fornecedor'] == "") {
                    $this->atualizaFornecedorNoContrato($contrato['co_contrato'], $fornecedor['co_fornecedor']);
                } else if ($contrato['co_fornecedor'] != $fornecedor['co_fornecedor']) {
                    $this->gravaErroMovimento("Codigo Fornecedor: " . $fornecedor['co_fornecedor'] . " / CNPJ: " . $fornecedor['nu_cnpj'] . "  diferente do que esta cadastrado no numero de processo [" . $contrato['nu_processo'] . "]");
                }
            }

            // critica tipo cotnratacao
            $tipoContratacao = array();
            try {
                $tipoContratacao = $this->recuperaTipoContratacao($registro['IT-IN-MODALIDADE-LICITACAO']);
            } catch (Exception $e) {
                $this->gravaErroMovimento("Tipo de contratacao [" . $registro['IT-IN-MODALIDADE-LICITACAO'] . "] nao cadastrado na base de dados do GESCON.");
            }

            if (!empty($tipoContratacao)) {
                if ($contrato['co_contratacao'] == "") {
                    $this->atualizaCampoContratacao($contrato['co_contrato'], $tipoContratacao['co_contratacao']);
                } else if ($contrato['co_contrato'] != $tipoContratacao['co_contratacao']) {
                    $this->gravaErroMovimento("Tipo contratacao [" . $tipoContratacao['co_contratacao'] . "] diferente do que esta cadastrado no processo [" . $contrato['nu_processo'] . "]");
                }
            }
        }
        return false;
    }

    protected function buscaFornecedor($cnpj) {
        $fornecedor = array_pop($this->Fornecedor->query("select * from fornecedores where nu_cnpj = $cnpj"));
        if (count($fornecedor) <= 0) {
            throw new Exception("Fornecedor [$cnpj] nao cadastrado.");
        }
        return array_pop($fornecedor);
    }

    protected function recuperaContrato($nuProcesso) {
        $contrato = array_pop($this->Contrato->query("select * from contratos where nu_processo = $nuProcesso"));
        if (empty($contrato)) {
            throw new Exception("Contrato nao encontrado para o numero de processo [$nuProcesso]");
        }
        return array_pop($contrato);
    }

    protected function recuperaTipoContratacao($coContratacao) {
        $contratacao = array_pop(array_pop($this->Contratacao->query("select * from contratacoes where co_contratacao = $coContratacao")));
        if (empty($contratacao)) {
            throw new Exception("Tipo contratacao nao cadastrada");
        }
        return $contratacao;
    }

    protected function atualizaCampoContratacao($coContrato, $coContratacao) {
        $this->Contrato->query("update contrato set co_contratacao = $coContratacao where co_contrato = $coContrato");
    }

    protected function atualizaFornecedorNoContrato($coContrato, $coFornecedor) {
        $this->Fornecedor->query("update contratos set co_fornecedor = $coFornecedor where co_contrato = $coContrato");
    }

    protected function gravarEmpenho($registros) {
        $registros = array_pop($registros);
        try {
            $contrato = $this->recuperaContrato($registros['IT-NU-PROCESSO']);
            $qry = "insert into empenhos (nu_empenho, dt_empenho, ds_empenho, nu_evento_contabil, nu_esfera_orcamentaria, nu_ptres, ds_fonte_recurso, nu_natureza_despesa, nu_ug_responsavel, co_plano_interno, vl_empenho, tp_empenho, nu_origem_material, uf_beneficiada, nu_municipio_beneficiado, ds_inciso, nu_contra_entrega, ds_amparo_legal, nu_lista, co_contrato) " .
                "values (" . $registros['GR-AN-NU-DOCUMENTO-REFERENCIA'] . " , '" . $this->transformaData($registros['IT-DA-EMISSAO']) . "' , '" . trim($registros['IT-TX-OBSERVACAO']) . "'," . $registros['GR-CODIGO-EVENTO']
                . "," . $registros['IT-IN-ESFERA-ORCAMENTARIA'] . "," . $registros['IT-CO-PROGRAMA-TRABALHO-RESUMIDO'] . ",'" . $registros['GR-FONTE-RECURSO'] . "'," . $registros['GR-NATUREZA-DESPESA']
                . "," . $registros['IT-CO-UG-RESPONSAVEL'] . ",'" . $registros['IT-CO-PLANO-INTERNO'] . "'," . $this->transformaMoeda($registros['IT-VA-TRANSACAO']) . ",'" . $registros['IT-IN-EMPENHO'] . "'," . $registros['IT-IN-ORIGEM-MATERIAL']
                . ",'" . $registros['IT-CO-UF-BENEFICIADA'] . "'," . $registros['IT-CO-MUNICIPIO-BENEFICIADO'] . ",'" . $registros['IT-CO-INCISO'] . "','" . $registros['IT-TX-AMPARO-LEGAL']
                . "','" . $registros['IT-IN-CONTRA-ENTREGA-NE'] . "','" . $registros['IT-NU-LISTA'] . "'," . $contrato['co_contrato'] . ") ";
            $this->Empenho->query($qry);
        } catch (Exception $exc) {
            $this->gravaErroMovimento($exc->getMessage());
        }
        
    }

    protected function gravaErroMovimento($mensagem) {
        $movimento = $this->recuperaMovimento($this->nomeArquivo);
        $this->Siafi->query("insert into log_criticas (ds_critica, dt_ocorrencia, nu_linha, co_movimento) values ('$mensagem', now(), null, " . $movimento['co_movimento'] . " )");
    }

    protected function gravaMovimento($arquivo) {
        $this->Siafi->query("insert into movimentos (cs_sucesso, dt_inicio, ds_nome_arquivo) values ('N', now(), '$arquivo')");
    }

    protected function finalizaMovimento($arquivo) {
        $this->Siafi->query("update movimentos set cs_sucesso = 'S', dt_fim = now() where ds_nome_arquivo = '$arquivo'");
    }

    protected function recuperaMovimento($arquivo) {
        $movimento = array_pop($this->Siafi->query("select * from movimentos where ds_nome_arquivo = '$arquivo'"));
        if (empty($movimento)) {
            throw new Exception("Movimento nao encontrato");
        }
        return array_pop($movimento);
    }

    protected function recuperaMovimentoValido($arquivo) {
        $movimento = array_pop($this->Siafi->query("select * from movimentos where ds_nome_arquivo = '$arquivo' and cs_sucesso = 'S'"));
        if (empty($movimento)) {
            throw new Exception("Movimento valido nao encontrato");
        }
        return array_pop($movimento);
    }

    protected function isMovimentoValido($arquivo) {
        try {
            $this->recuperaMovimentoValido($arquivo);
            return false;
        } catch (Exception $e) {
            return true;
        }
    }

    private function transformaData($data) {
        $dia = substr($data, 0, 2);
        $mes = substr($data, 2, 2);
        $ano = substr($data, 4, 4);
        return $ano . '-' . $mes . '-' . $dia;
    }

    private function transformaMoeda($valor) {
        $valor = trim($valor);
        $centavos = substr($valor, -2);
        $vlrInteiro = substr($valor, 0, -2);
        return $vlrInteiro . '.' . $centavos;
    }

}