<?php

/**
 * Rotina implementada para recuperar informações do serviço do siafi com a B2T
 *
 *
 *
 * Rotina implementada para recuperar informações do serviço do siafi com a B2T
 *
 * Uso:
 *    php [path_to_source_gescon]/vendors/n2oti/cake/console/cake.php -app [path_to_source_gescon]/app siafi [args]
 *
 * Exemplo:
 *    php /var/www/gescon-2/vendors/n2oti/cake/console/cake.php -app /var/www/gescon-2/app siafi 1
 *
 */

App::import('Helper', 'Modulo');
App::import('Component', 'Session');

class SiafiShell extends Shell
{
    var $uses = array(
        'Empenho',
        'Contrato',
        'Pagamento',
        'Liquidacao',
        'NotaFiscal',
        'NotasEmpenho',
        'LogSiafi',
    );

    private $debugger = true;
    private $screenDebugger = false;
    private $coGrupoPagamento = 8;
    private $coGrupoLiquidacao = 7;
    private $coGrupoEmpenho = 6;
    private $coGrupoPagamentoRestosPagar = 12;
    private $coGrupoLiquidacaoRestosPagar = 11;
    private $icImportacaoSiafi = 'S';
    private $method = 'GET';
    private $uasg = null;
    private $configs = array();
    private $arWsMethods = array(
        'busca_empenho' => '/busca_empenho',
        'grupos' => '/grupos',
        'lancamento_consolidado' => '/lancamento_consolidado',
    );


    private function _debug($trace, $dots = false)
    {
        if ($this->debugger) {
            $d = ($dots) ? '...' : '';
            echo $trace . $d . PHP_EOL;
        }
    }

    /**
     * chamada: cake siafi [client, screenDebug]
     */
    function main()
    {
        $appStartTime = \microtime(TRUE);
        $this->configs = Configure::read('App.config.component.siafi');

        if (!$this->configs['enabled']) {
            die('Componente Siafi não habilitado.' . PHP_EOL);
        }

        if (isset($this->args[0])) {
            $this->screenDebugger = (boolean)$this->args[0];
        }

        if (!$this->screenDebugger) {
            ob_start();
        }

        $this->_debug('...[SIAFI] Started [' . date('Y-m-d h:i:s') . ']', true);

        try {
            $coGestao = str_pad(
                $this->configs['params']['coGestaoOrgao'], 5, '0', STR_PAD_LEFT
            );

            $empenhos = $this->Empenho->find('all');

            foreach ($empenhos as $key => $empenho) {
                $coEmpenho = (int)$empenho['Empenho']['co_empenho'];
                $coContrato = (int)$empenho['Empenho']['co_contrato'];
                $nuEmpenho = $empenho['Empenho']['nu_empenho'];

                $contrato = $this->Contrato->find(
                    'first',
                    array(
                        'conditions' => array('Contrato.co_contrato' => $coContrato)
                    )
                );

                $this->uasg = $contrato['Contrato']['uasg'];
                $codigoEmpenho = $this->uasg . $coGestao . $nuEmpenho;
                $data = array('codigo_empenho' => $codigoEmpenho);
                $this->_debug("Invocando a API [empenho][{$codigoEmpenho}]");

                $apiResultEmpenho = $this->_callAPI('busca_empenho', $data);
                $arDados = json_decode($apiResultEmpenho);

                if (count((array)$arDados)) {
                    $this->_processaEmpenho($coContrato, $coEmpenho, $arDados);
                } else {
                    $this->_debug('Dados do empenho não localizados');
                }

                $this->_debug('Processamento Finalizado');
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        $benchmark = number_format(\microtime(TRUE) - $appStartTime, 5);
        $this->_debug('...[SIAFI] Stoped [' . date('Y-m-d h:i:s') . ']' . PHP_EOL . PHP_EOL, true);
        $this->_debug('...[SIAFI] Processamento em ' . $benchmark . ' segundos' . PHP_EOL . PHP_EOL, true);

        if (!$this->screenDebugger) {
            $strLog = ob_get_contents();
            ob_end_clean();
            $filename = APP_PATH . 'tmp/logs/shell_siafi_' . date('Ymd');
            $fLog = fopen($filename, 'a+');
            fwrite($fLog, $strLog);
            fclose($fLog);
        }
    }

    private function _processaEmpenho($coContrato, $coEmpenho, $objDados)
    {
        $empenho = $this->Empenho->find(
            'first',
            array(
                'conditions' => array(
                    'Empenho.co_empenho' => $coEmpenho,
                    'Empenho.co_contrato' => $coContrato
                )
            )
        );

        $nuEmpenho = $empenho['Empenho']['nu_empenho'];
        $this->_debug("Processando empenho [{$nuEmpenho}]");

        $dtEmissao = DateTime::createFromFormat('Y-m-d', $objDados->data_emissao);

        $valorEmpenho = $this->_getValorEmpenho($empenho['Empenho']['nu_empenho'], $dtEmissao);

        if (($empenho['Empenho']['dt_empenho'] != $objDados->data_emissao) ||
            ($empenho['Empenho']['nu_ptres'] != $objDados->ptres) ||
            ($empenho['Empenho']['vl_empenho'] != $valorEmpenho)
        ) {

            $empenho['Empenho']['dt_empenho'] = $objDados->data_emissao;
            $empenho['Empenho']['nu_ptres'] = $objDados->ptres;
            $empenho['Empenho']['vl_empenho'] = (double)$valorEmpenho;
            $empenho['Empenho']['ic_importacao_siafi'] = $this->icImportacaoSiafi;

            $this->_debug('Valor Empenho: ' . $empenho['Empenho']['vl_empenho']);

            $this->_doInsertLog($this->uasg, $nuEmpenho, array('Empenho' => $empenho['Empenho']), 4, 'U');
            $this->Empenho->save($empenho, false, array('dt_empenho', 'nu_ptres', 'vl_empenho', 'ic_importacao_siafi'));
        }

        $arDataSubmit = array(
            'ano' => $dtEmissao->format('Y'),
            'uge[]' => $this->uasg,
            'empenhoReferencia[]' => $empenho['Empenho']['nu_empenho'],
            'grupo[]' => $this->coGrupoPagamento, // pagamentos
            'campos' => 'dt_emissao,dt_transacao,dt_lancamento,cod_doc'
        );

        $valor_total_pagamento = 0;

        $anoProcessamento = $dtEmissao->format('Y');

        while ($anoProcessamento <= date('Y')) {
            $arDataSubmit['ano'] = $anoProcessamento;
            $arDataSubmit['grupo[]'] = ($anoProcessamento == $dtEmissao->format('Y')) ?
                $this->coGrupoPagamento :
                $this->coGrupoPagamentoRestosPagar;
            $this->_debug("invocando a API [pagamento][ano {$anoProcessamento}]");
            $resultPagamentos = $this->_callAPI('lancamento_consolidado', $arDataSubmit);
            $this->_processaPagamento($empenho, json_decode($resultPagamentos));

            $this->_valorTotalPagamento($valor_total_pagamento, json_decode($resultPagamentos));

            //PROCESSA LIQUIDAÇÕES
            $this->_debug("invocando a API [liquidacao][ano {$anoProcessamento}]");
            $arDataSubmit['ano'] = $anoProcessamento;
            $arDataSubmit['grupo[]'] = ($anoProcessamento == $dtEmissao->format('Y')) ?
                $this->coGrupoLiquidacao :
                $this->coGrupoLiquidacaoRestosPagar;
            $resultLiquidacoes = $this->_callAPI('lancamento_consolidado', $arDataSubmit);

            $this->_processaLiquidacao($empenho, json_decode($resultLiquidacoes));

            $anoProcessamento++;
        }

        //Atualiza o valor restante do pagamento
        $empenho['Empenho']['vl_restante'] = $empenho['Empenho']['vl_empenho'] - $valor_total_pagamento;

        $this->_doInsertLog($this->uasg, $nuEmpenho, array('Empenho' => $empenho['Empenho']), 4, 'U');
        $this->Empenho->save($empenho, false, array('vl_restante'));

        return $this;
    }

    private function _valorTotalPagamento(&$valor_total_pagamento, $arrPagamentos)
    {
        foreach ($arrPagamentos as $valorDoPagamento) {
            $valor_total_pagamento += $valorDoPagamento->valor;
        }
        return $this;
    }

    private function _getValorEmpenho($nuEmpenho, DateTime $dtEmissao)
    {
        $arDataSubmit = array(
            'ano' => $dtEmissao->format('Y'),
            'uge[]' => $this->uasg,
            'empenhoReferencia[]' => $nuEmpenho,
            'grupo[]' => $this->coGrupoEmpenho,
        );

        $resultEmpenho = json_decode($this->_callAPI('lancamento_consolidado', $arDataSubmit));

        if ($resultEmpenho) {
            return current($resultEmpenho)->valor;
        }
        return 0;
    }

    private function _processaPagamento($empenho, array $arPagamentos)
    {
        $this->_debug("\t- Processando valores de [pagamento]");
        $pgAdd = 0;
        $pgUpd = 0;

        $nuEmpenho = $empenho['Empenho']['nu_empenho'];
        foreach ($arPagamentos as $objPagamento) {

            $dtPagamento = DateTime::createFromFormat('Y-m-d', $objPagamento->dt_transacao);
            $coDocSiafi = $objPagamento->cod_doc;
            $ano = $dtPagamento->format('Y');
            $mes = $dtPagamento->format('m');
            $mdlPagamento = $this->Pagamento->find(
                'first',
                array(
                    'conditions' => array(
                        'Pagamento.co_empenho' => $empenho['Empenho']['co_empenho'],
                        'Pagamento.co_contrato' => $empenho['Empenho']['co_contrato'],
                        'Pagamento.dt_pagamento' => $dtPagamento->format('Y-m-d'),
                        'Pagamento.nu_ano_pagamento' => $ano,
                        'Pagamento.nu_mes_pagamento' => $mes,
                        'Pagamento.co_documento_siafi' => $coDocSiafi,
                    )
                )
            );

            $this->_debug('Valor Pagamento: [' . $coDocSiafi . '] ' . $objPagamento->valor);

            if (!empty($mdlPagamento)) {
                if ($objPagamento->valor != $mdlPagamento['Pagamento']['vl_pagamento']) {
                    $mdlPagamento['Pagamento']['vl_pagamento'] = $objPagamento->valor;
                    $mdlPagamento['Pagamento']['co_documento_siafi'] = $coDocSiafi;
                    $mdlPagamento['Pagamento']['ic_importacao_siafi'] = $this->icImportacaoSiafi;
                    $keysUpdate = array('vl_pagamento', 'co_documento_siafi', 'ic_importacao_siafi');
                    $this->_doUpdatePagamento($nuEmpenho, $mdlPagamento, $keysUpdate);
                    $pgUpd++;
                }
            } else {
                $arrDataInsert = array(
                    'co_contrato' => $empenho['Empenho']['co_contrato'],
                    'co_empenho' => $empenho['Empenho']['co_empenho'],
                    'nu_ano_pagamento' => $ano,
                    'dt_pagamento' => $dtPagamento->format('Y-m-d'),
                    'nu_mes_pagamento' => $mes,
                    'vl_pagamento' => $objPagamento->valor,
                    'co_documento_siafi' => $coDocSiafi,
                    'ic_importacao_siafi' => $this->icImportacaoSiafi,
                );

                $this->_doInsertPagamento($nuEmpenho, $arrDataInsert);
                $pgAdd++;
            }
        }

        $this->_debug("\t\t[{$empenho['Empenho']['nu_empenho']}] Foram adicionados {$pgAdd} pagamentos", false);
        $this->_debug("\t\t[{$empenho['Empenho']['nu_empenho']}] Foram alterado {$pgUpd} pagamentos", false);

        return $this;
    }

    private function _processaLiquidacao($empenho, array $arLiquidacoes)
    {
        $this->_debug("\t- Processando valores de [liquidacao]");

        $lqAdd = 0;
        $lqUpd = 0;
        $nuEmpenho = $empenho['Empenho']['nu_empenho'];

        foreach ($arLiquidacoes as $objLiquidacao) {

            $dtLiquidacao = DateTime::createFromFormat('Y-m-d', $objLiquidacao->dt_transacao);
            $coDocSiafi = $objLiquidacao->cod_doc;
            $mdlLiquidacao = $this->Liquidacao->find(
                'first',
                array(
                    'conditions' => array(
                        'Liquidacao.co_contrato' => $empenho['Empenho']['co_contrato'],
                        'Liquidacao.nu_liquidacao' => $coDocSiafi
                    )
                )
            );

            $this->_debug('Valor Liquidacao: [' . $coDocSiafi . '] ' . $objLiquidacao->valor);

            if (!empty($mdlLiquidacao)) {
                if ($objLiquidacao->valor != $mdlLiquidacao['Liquidacao']['vl_liquidacao']) {
                    $mdlLiquidacao['Liquidacao']['vl_liquidacao'] = $objLiquidacao->valor;
                    $mdlLiquidacao['Liquidacao']['nu_liquidacao'] = $coDocSiafi;
                    $mdlLiquidacao['Liquidacao']['ic_importacao_siafi'] = $this->icImportacaoSiafi;

                    $keysUpdate = array('vl_liquidacao', 'nu_liquidacao', 'ic_importacao_siafi');
                    $this->_doUpdateLiquidacao($nuEmpenho, $mdlLiquidacao, $keysUpdate);
                    $lqUpd++;
                }
            } else {
                $this->_doInsertLiquidacao($nuEmpenho, array(
                        'co_contrato' => $empenho['Empenho']['co_contrato'],
                        'dt_liquidacao' => $dtLiquidacao->format('Y-m-d'),
                        'vl_liquidacao' => $objLiquidacao->valor,
                        'nu_liquidacao' => $coDocSiafi,
                        'ic_importacao_siafi' => $this->icImportacaoSiafi
                    )
                );
                $lqAdd++;
            }
        }
        $this->_debug("\t\t[{$empenho['Empenho']['nu_empenho']}] Foram adicionados {$lqAdd} liquidações", false);
        $this->_debug("\t\t[{$empenho['Empenho']['nu_empenho']}] Foram alterado {$lqUpd} liquidações", false);

        return $this;
    }

    private function _doInsertLiquidacao($nuEmpenho, $arrData)
    {
        $this->Liquidacao->create();
        $savedData = $this->Liquidacao->save($arrData, false);
        $this->_doInsertLog($this->uasg, $nuEmpenho, $savedData, 2, 'I');
        return $savedData;
    }

    private function _doInsertPagamento($nuEmpenho, $arrData)
    {
        $this->Pagamento->create();
        $savedData = $this->Pagamento->save($arrData, false);
        $this->_doInsertLog($this->uasg, $nuEmpenho, $savedData, 1, 'I');
        return $savedData;
    }

    private function _doUpdateLiquidacao($nuEmpenho, $arrData, $arrKeys)
    {
        $this->Liquidacao->save($arrData, false, $arrKeys);
        $this->_doInsertLog($this->uasg, $nuEmpenho, array('Liquidacao' => $arrData), 2, 'U');
    }

    private function _doUpdatePagamento($nuEmpenho, $arrData, $arrKeys)
    {
        $this->Pagamento->save($arrData, false, $arrKeys);
        $this->_doInsertLog($this->uasg, $nuEmpenho, array('Pagamento' => $arrData['Pagamento']), 1, 'U');
    }

    private function _doInsertLog($uasg, $nuEmpenho, array $dsLog, $tpEntidade, $tpOperacao)
    {
        $this->LogSiafi->create();
        $this->LogSiafi->save(
            array(

                'dt_operacao' => date('Y-m-d H:i:s'),
                'tp_operacao' => $tpOperacao,
                'tp_entidade' => $tpEntidade,
                'nu_uasg' => $uasg,
                'nu_empenho' => $nuEmpenho,
                'ds_log' => json_encode($dsLog)
            ),
            false
        );
    }

    /**
     * @param $serviceName
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    private function _callAPI($serviceName, $data = array())
    {
        $curl = curl_init();

        $ws_url = $this->configs['params']['ws_url'];

        if (!$ws_url) {
            die('Url do serviço no definida no config.php do cliente');
        }

        $url = $ws_url . $this->arWsMethods[$serviceName];

        switch ($this->method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('AUTH-TOKEN: ' . $this->configs['params']['ws_authToken']));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        $info = curl_getinfo($curl);
        $this->_trataInfoResponse($info, $result, $url);

        curl_close($curl);

        if (strpos($result, 'Time-out') !== false) {
            throw new \Exception('Servidor indisponível');
        }

        return $result;
    }

    private function _trataInfoResponse($arInfo, $result, $url)
    {
        switch ($arInfo['http_code']) {
            case 401:
                throw new \Exception('[Acesso ao serviço não autorizado. Verifique o token de acesso] ' . $result);
                break;
            case 402:
                throw new \Exception('[Acesso negado] ' . $result);
                break;
            case 403:
                throw new \Exception('[Serviço não localizado] [' . $url . '] ' . $result);
                break;
            default:
                if ($arInfo['http_code'] != 200) {
                    throw new \Exception('[Ocorreu um erro no serviço] ' . $result);
                }
        }
    }

}
