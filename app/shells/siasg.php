<?php

class SiasgShell extends Shell {
    var $uses = array(
            'Rotina',
            'Contrato',
            'Garantia',
            'Contratacao',
            'Aditivo',
            'Fornecedor',
            'ItemAquisicao',
            'Aquisicao'
    );
    //var $tasks = array('ContratoImport');
    //var $ContratoImport;
    
    var $execucao   = '';
    var $pasta      = '/app/tmp/';
    var $usuario    = null;
    var $unidade    = null;
    var $gestor     = null;
    var $regAdd     = 0;
    var $regEdit    = 0;
    
    /**
     * chamada: cake siasg [co_usuario] [no_arquivo] [tipo] [unidade] [gestor]
     */
    function main() {
        $dt_inicio  = date('Y-m-d H:m:s');
        $this->usuario = $this->args[0];
        
        if($this->args[2] == 1) {
            $this->unidade  = $this->args[3];
            $this->gestor   = $this->args[4];
            $this->importarContratos($this->args[1]);
        }
        if($this->args[2] == 2) {
            $this->importarAditivos($this->args[1]);
        }
        if($this->args[2] == 3) {
            $this->unidade  = $this->args[3];
            $this->gestor   = $this->args[4];
            $this->atualizarDatas($this->args[1]);
        }
        if($this->args[2] == 4) {
            $this->updDtFimVigencia();
        }
        
        $this->Rotina->create();
        $this->Rotina->save( array( 'tp_rotina' => 'MS', 'dt_inicio' => $dt_inicio, 'dt_fim' => date('Y-m-d H:m:s'), 'ds_resultado' => $this->execucao) );
    }
    
    function importarContratos($arquivo = 'siasg.csv') {
        $delimitador    = ';';
        
        $file   = fopen(ROOT . $this->pasta . $arquivo, 'r');
        while (($data = fgetcsv($file, 0, $delimitador)) !== FALSE) {
            //$data[0]; Identificação do Contrato (6) UASG / (2) Tipo de Contrato (50 - Contrato / 51 - Credenciamento)
            //$data[1]; Contrato
            //$data[2]; Objeto
            //$data[3]; Processo
            //$data[4]; Identificação de Compra (6) UASG / (2) Modalidade / (9) N. Licitação
            //$data[5]; Data de publicação
            //$data[6]; CNPJ Fornecedor
            //$data[7]; Nome Fornecedor
            //$data[8]; Fundamento Legal
            //$data[9]; Início da Vigência
            //$data[10]; Termino da Vigência
            //$data[11]; Assinatura do Contrato
            //$data[12]; Garantia
            //$data[13]; Valor Global
            //$data[14]; Data Término Último Aditivo
            
            $this->importarContrato($data);
        }
        fclose ($file);
        $this->execucao .= ' Registros Novos: ' . $this->regAdd;
        $this->execucao .= ' Registros Atualizados: ' . $this->regEdit;
        debug($this->execucao);
    }
    
    function atualizarDatas($arquivo = 'siasg.csv') {
        $delimitador    = ';';
        
        $file   = fopen(ROOT . $this->pasta . $arquivo, 'r');
        while (($data = fgetcsv($file, 0, $delimitador)) !== FALSE) {
            debug($data[1]);

            $contrato   = $this->Contrato->find( 'first', array('conditions' => array('Contrato.co_ident_siasg' => $data[0])) );

            if ( !empty( $contrato ) ) {
                
                $contrato['Contrato']['dt_ini_vigencia']        = dtDb($data[9], 2);
                $contrato['Contrato']['dt_fim_vigencia']        = dtDb($data[10], 2);
                $contrato['Contrato']['dt_fim_vigencia_inicio'] = dtDb($data[10], 2);
                
                $this->Contrato->save( $contrato, false, 
                        array('dt_ini_vigencia', 'dt_fim_vigencia', 'dt_fim_vigencia_inicio') );
                $this->regEdit++;
            } else {
                $this->regAdd++;
            }
        }
        fclose ($file);
        $this->execucao .= ' Registros Novos: ' . $this->regAdd;
        $this->execucao .= ' Registros Atualizados: ' . $this->regEdit;
        debug($this->execucao);
    }
    
    function updDtFimVigencia() {
        $aditivos   = $this->Aditivo->find('all',  array( 
                        'fields' => array('MAX( Aditivo.dt_prazo ) as dt_prazo', 'co_contrato'), 
                        'group' => array('Aditivo.co_contrato') ) );
        foreach ($aditivos as $aditivo):
            debug('Contrato: ' . $aditivo["Aditivo"]["co_contrato"] . ' Data: ' . $aditivo[0]["dt_prazo"]);
            $this->Contrato->id                      = $aditivo["Aditivo"]["co_contrato"];
            $contrato = array();
            $contrato['Contrato']['co_usuario']      = $this->usuario;
            $contrato['Contrato']['dt_fim_vigencia'] = $aditivo[0]["dt_prazo"];
            $this->Contrato->save( $contrato, false, array('co_usuario', 'dt_fim_vigencia') );
        endforeach;
    }


    function importarAditivos($arquivo = 'siasg_aditivo.csv') {
        $delimitador    = ';';
        
        $file   = fopen(ROOT . $this->pasta . $arquivo, 'r');
        while (($data = fgetcsv($file, 0, $delimitador)) !== FALSE) {
            //$data[0]; Identificação do Contrato (6) UASG / (2) Tipo de Contrato (50 - Contrato / 51 - Credenciamento)
            //$data[1]; Identificação Termo Aditivo
            //$data[2]; Número Termo Aditivo
            //$data[3]; Objeto do Termo
            //$data[4]; Tipo do Termo
            //$data[5]; Publicação do Termo
            //$data[6]; Início do Termo
            //$data[7]; Fim do Termo
            //$data[8]; Valor Acrescentado
            //$data[9]; Valor Suprimido
            
            $contrato   = $this->Contrato->find( 'first', array('conditions' => array('Contrato.co_ident_siasg' => $data[0])) );

            if ( !empty( $contrato ) ) {
                $this->regEdit++;
                $this->addAditivo($data, $contrato);
            } else {
                $this->regAdd++;
            }
        }
        fclose ($file);
        $this->execucao .= ' Registros Não Localizados: ' . $this->regAdd;
        $this->execucao .= ' Registros Localizados: ' . $this->regEdit;
        debug($this->execucao);
    }
    
    function importarContrato($data) {
        debug($data[1]);
        
        $contrato   = $this->Contrato->find( 'first', array('conditions' => array('Contrato.nu_contrato' => $data[1])) );

        if ( !empty( $contrato ) ) {
            $this->edit($contrato, $data);
            $this->regEdit++;
        } else {
            $this->add($data);
            $this->regAdd++;
        }
    }
    
    function add($data) {
        $this->Contrato->create();
        
        $ds_objeto       = str_replace("Objeto: ", "", $data[2]);
        $ds_fundamento_legal = str_replace("Fundamento Legal: ", "", $data[8]);
        $modalidade      = $this->Contratacao->find( 'first', array('conditions' => array('Contratacao.co_siasg' => substr($data[4], 6, 2))) );
        $co_contratacao  = $modalidade['Contratacao']['co_contratacao'];
        $nu_licitacao    = substr($data[4], 8);
        $co_fornecedor   = $this->getFornecedor($data[6], $data[7]);
        //$vl_global       = vlReal( ln($data[16]) * $data[17] );
        $vl_global       = vlReal( ln($data[13]) );
        $dt_fim_vigencia = dtDb($data[10], 2);
        $dt_fim_vigencia_inicio = $dt_fim_vigencia;
        if(strlen($data[14]) == 11) {
            $dt_fim_vigencia    = dtDb($data[14], 2);
        }

        $this->Contrato->save( array( 'co_usuario' => $this->usuario, 'nu_contrato' => $data[1], 'ds_objeto' => $ds_objeto, 'nu_processo' => $data[3], 
                                      'co_contratacao' => $co_contratacao, 'nu_licitacao' => $nu_licitacao, 'dt_publicacao' => dtDb($data[5], 2), 
                                      'co_fornecedor' => $co_fornecedor, 'ds_fundamento_legal' => $ds_fundamento_legal, 'dt_ini_vigencia' => dtDb($data[9], 2), 
                                      'dt_fim_vigencia' => $dt_fim_vigencia, 'dt_fim_vigencia_inicio' => $dt_fim_vigencia_inicio, 'dt_assinatura' => dtDb($data[11], 2), 
                                      'vl_global' => $vl_global, 'co_contratante' => $this->unidade, 'co_gestor_atual' => $this->gestor, 
                                      'dt_cadastro' => date('Y-m-d H:m:s'), 'co_ident_siasg' => $data[0] ), false );
        $co_contrato    = $this->Contrato->getInsertID();
        
        if($data[12] != '' && up($data[12]) != 'NAO INFORMADO') {
            $this->Garantia->create();
            $this->Garantia->save( array( 'co_usuario' => $this->usuario, 'co_contrato' => $co_contrato, 'ds_modalidade' => $data[12] ), false );
        }
        
//        $co_item_aquisicao  = $this->getItemAquisicao($data[13], $data[14]);
//        $this->Aquisicao->create();
//        $this->Aquisicao->save( array( 'co_item_aquisicao' => $co_item_aquisicao, 'co_contrato' => $co_contrato, 'ds_aquisicao' => $data[15], 
//                                       'vl_aquisicao' => $data[16], 'qt_aquisicao' => $data[17] ), false);
    }
    
    function edit($contrato, $data) {
        
        $contrato['Contrato']['co_ident_siasg']         = $data[0];
        $contrato['Contrato']['ds_objeto']              = str_replace("Objeto: ", "", $data[2]);
        $contrato['Contrato']['ds_fundamento_legal']    = str_replace("Fundamento Legal: ", "", $data[8]);
        $modalidade      = $this->Contratacao->find( 'first', array('conditions' => array('Contratacao.co_siasg' => substr($data[4], 6, 2))) );
        $contrato['Contrato']['co_contratacao'] = $modalidade['Contratacao']['co_contratacao'];
        //$contrato['Contrato']['nu_licitacao']   = substr($data[4], 8);
        $contrato['Contrato']['co_fornecedor']  = $this->getFornecedor($data[6], $data[7]);

        //$contrato['Contrato']['vl_global']      = vlReal( ln($data[13]) );
        //$contrato['Contrato']['nu_processo']            = $data[3];
        $contrato['Contrato']['dt_publicacao']          = dtDb($data[5], 2);
        
//        $contrato['Contrato']['dt_ini_vigencia']        = dtDb($data[9]);
//        $contrato['Contrato']['dt_fim_vigencia']        = dtDb($data[10]);
//        if(strlen($data[14]) == 11) {
//           $contrato['Contrato']['dt_fim_vigencia_inicio']  = $contrato['Contrato']['dt_fim_vigencia'];
//           $contrato['Contrato']['dt_fim_vigencia']         = dtDb($data[14], 2);
//        }
        
        $contrato['Contrato']['dt_assinatura']          = dtDb($data[11], 2);

        $this->Contrato->save( $contrato, false, 
                array('ds_objeto', 'ds_fundamento_legal', 'co_contratacao', 'co_fornecedor', 'dt_publicacao', 'dt_assinatura', 'co_ident_siasg') );
        
//        $co_item_aquisicao  = $this->getItemAquisicao($data[13], $data[14]);
//        $this->Aquisicao->create();
//        $this->Aquisicao->save( array( 'co_item_aquisicao' => $co_item_aquisicao, 'co_contrato' => $contrato['Contrato']['co_contrato'], 
//                                       'ds_aquisicao' => $data[15], 'vl_aquisicao' => $data[16], 'qt_aquisicao' => $data[17] ), false);
    }
    
    function getFornecedor($cnpj, $nome) {
        $fornecedor = $this->Fornecedor->find( 'first', array('conditions' => array('Fornecedor.nu_cnpj' => $cnpj)) );
        if ( !isset( $fornecedor['Fornecedor']['co_fornecedor'] ) ) {
            $this->Fornecedor->create();
            $this->Fornecedor->save( array( 'tp_fornecedor' => 'J', 'nu_cnpj' => $cnpj, 'no_razao_social' => $nome ), false );
            return $this->Fornecedor->getInsertID();
        } else {
            return $fornecedor['Fornecedor']['co_fornecedor'];
        }
    }
    
    function getItemAquisicao($nu_item, $ds_item) {
        $item   = $this->ItemAquisicao->find( 'first', array('conditions' => array('ItemAquisicao.nu_item_aquisicao' => $nu_item)) );
        if ( !isset( $item['ItemAquisicao']['co_item_aquisicao'] ) ) {
            $this->ItemAquisicao->create();
            $this->ItemAquisicao->save( array( 'nu_item_aquisicao' => $nu_item, 'ds_item_aquisicao' => $ds_item ), false );
            return $this->ItemAquisicao->getInsertID();
        } else {
            return $item['ItemAquisicao']['co_item_aquisicao'];
        }
    }
    
    function addAditivo($data, $contrato) {
        $this->Aditivo->create();
        
        $ds_objeto      = str_replace("Objeto: ", "", $data[3]);
        $tp_aditivo     = $this->getTpAditivo($data[4]);
        $vl_aditivo     = vlReal( ln($data[8]) );
        if($tp_aditivo == 4) {
            $vl_aditivo = vlReal( ln($data[9]) );
        }
        
        $novoAditivo    = array('co_contrato' => $contrato['Contrato']['co_contrato'], 'co_usuario' => $this->usuario, 'tp_aditivo' => $tp_aditivo, 
                                'no_aditivo' => $data[2], 'ds_aditivo' => $ds_objeto, 'dt_aditivo' => dtDb($data[5], 2), 'vl_aditivo' => $vl_aditivo );
        
        if($tp_aditivo == 2 || $tp_aditivo == 3) {
            $novoAditivo['dt_fim_vigencia_anterior'] = dtDb($data[6], 2);
            $novoAditivo['dt_prazo']                 = dtDb($data[7], 2);
        }

        $this->Aditivo->save( $novoAditivo, false );
        
        // Atualizar Contrato

    }
    
    function getTpAditivo($tp_aditivo) {
        if($tp_aditivo == 'Valor') {
            return 1;
        }
        if($tp_aditivo == 'Vigência') {
            return 2;
        }
        if($tp_aditivo == 'Valor/Vigência') {
            return 3;
        }
        if($tp_aditivo == 'Supressão') {
            return 4;
        }
    }
    
}

?>