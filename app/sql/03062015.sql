ALTER TABLE  `empenhos` CHANGE  `ds_empenho`  `ds_empenho` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `vl_empenho`  `vl_empenho` DECIMAL( 12, 2 ) NULL ,
CHANGE  `dt_empenho`  `dt_empenho` DATE NULL;

ALTER TABLE  `log_empenhos` CHANGE  `ds_empenho`  `ds_empenho` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `vl_empenho`  `vl_empenho` DECIMAL( 12, 2 ) NULL ,
CHANGE  `dt_empenho`  `dt_empenho` DATE NULL;


ALTER TABLE  `contratacoes` CHANGE  `ds_contratacao`  `ds_contratacao` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
