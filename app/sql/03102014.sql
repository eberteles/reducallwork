ALTER TABLE  `apostilamentos` CHANGE  `no_apostilamento`  `no_apostilamento` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_apostilamento`  `ds_apostilamento` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `log_apostilamentos` CHANGE  `no_apostilamento`  `no_apostilamento` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_apostilamento`  `ds_apostilamento` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `atas` CHANGE  `ds_publicacao`  `ds_publicacao` VARCHAR( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_especificacao`  `ds_especificacao` VARCHAR( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_justificativa`  `ds_justificativa` VARCHAR( 1000 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `empenhos` CHANGE  `ds_empenho`  `ds_empenho` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `log_empenhos` CHANGE  `ds_empenho`  `ds_empenho` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `notas_fiscais` CHANGE  `ds_nota`  `ds_nota` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_atesto`  `ds_atesto` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `log_notas_fiscais` CHANGE  `ds_nota`  `ds_nota` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_atesto`  `ds_atesto` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `contratos` ADD  `dt_prazo_processo` DATE NULL COMMENT  'Prazo para conclusão da Fase atual do Processo' AFTER  `dt_tais`;

ALTER TABLE  `log_contratos` ADD  `dt_tais` DATE NULL ,
ADD  `dt_prazo_processo` DATE NULL;

DROP TRIGGER IF EXISTS `ADD_LOG_CONTRATO`;

DELIMITER //
CREATE TRIGGER `ADD_LOG_CONTRATO` AFTER INSERT ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro,
            dt_cadastro_pam = NEW.dt_cadastro_pam,
            dt_cadastro_processo = NEW.dt_cadastro_processo,
            dt_tais         = NEW.dt_tais,
            dt_prazo_processo    = NEW.dt_prazo_processo;
END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_CONTRATO`;

DELIMITER //
CREATE TRIGGER `UPD_LOG_CONTRATO` AFTER UPDATE ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro,
            dt_cadastro_pam = NEW.dt_cadastro_pam,
            dt_cadastro_processo = NEW.dt_cadastro_processo,
            dt_tais         = NEW.dt_tais,
            dt_prazo_processo    = NEW.dt_prazo_processo;
        END
//
DELIMITER ;