DROP TABLE IF EXISTS `log_ordem_fornecimento_servico`;
DROP TABLE IF EXISTS `ordem_fornecimento_servico`;

CREATE TABLE `ordem_fornecimento_servico` (
  `co_ordem_fornecimento_servico` int(11) NOT NULL AUTO_INCREMENT,
  `co_contrato` int(11) ,  
  `nu_ordem` INT NULL,
  `dt_emissao` DATE NULL,
  `dt_inicial` DATE NULL,
  `dt_final` DATE NULL,
  `dc_descricao` VARCHAR(500) NULL,
  `co_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`co_ordem_fornecimento_servico`),
  KEY `co_contrato` (`co_contrato`), 
  KEY `co_usuario` (`co_usuario`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `log_ordem_fornecimento_servico` (
  `co_loglog_ordem_fornecimento_servico` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) DEFAULT NULL,
  `tp_acao` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_ordem_fornecimento_servico` INT(11) NOT NULL,
  `co_contrato` INT(11) ,  
  `nu_ordem` INT NULL,
  `dt_emissao` DATE NULL,
  `dt_inicial` DATE NULL,
  `dt_final` DATE NULL,
  `dc_descricao` VARCHAR(500) NULL,
  KEY (`co_ordem_fornecimento_servico`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


DROP TRIGGER IF EXISTS `ADD_LOG_ORDEM_FORNECIMENTO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_ORDEM_FORNECIMENTO` AFTER INSERT ON `ordem_fornecimento_servico`
 FOR EACH ROW BEGIN
	INSERT INTO log_ordem_fornecimento_servico SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I', 

            co_ordem_fornecimento_servico   = NEW.co_ordem_fornecimento_servico,
            co_contrato     = NEW.co_contrato,
            nu_ordem        = NEW.nu_ordem,
            dt_emissao      = NEW.dt_emissao,
            dt_inicial      = NEW.dt_inicial,
            dt_final        = NEW.dt_final,
            dc_descricao    = NEW.dc_descricao;
END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_ORDEM_FORNECIMENTO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_ORDEM_FORNECIMENTO` AFTER UPDATE ON `ordem_fornecimento_servico`
 FOR EACH ROW BEGIN
	INSERT INTO log_ordem_fornecimento_servico SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A', 

            co_ordem_fornecimento_servico   = NEW.co_ordem_fornecimento_servico,
            co_contrato     = NEW.co_contrato,
            nu_ordem        = NEW.nu_ordem,
            dt_emissao      = NEW.dt_emissao,
            dt_inicial      = NEW.dt_inicial,
            dt_final        = NEW.dt_final,
            dc_descricao    = NEW.dc_descricao;
END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_ORDEM_FORNECIMENTO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_ORDEM_FORNECIMENTO` AFTER DELETE ON `ordem_fornecimento_servico`
 FOR EACH ROW BEGIN
	INSERT INTO log_ordem_fornecimento_servico SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E', 

            co_ordem_fornecimento_servico   = OLD.co_ordem_fornecimento_servico,
            co_contrato     = OLD.co_contrato,
            nu_ordem        = OLD.nu_ordem,
            dt_emissao      = OLD.dt_emissao,
            dt_inicial      = OLD.dt_inicial,
            dt_final        = OLD.dt_final,
            dc_descricao    = OLD.dc_descricao;
END
//
DELIMITER ;


ALTER TABLE anexos ADD co_ordem_fornecimento_servico INT(11) after co_evento;

ALTER TABLE log_anexos ADD co_ordem_fornecimento_servico INT(11) after co_evento;
