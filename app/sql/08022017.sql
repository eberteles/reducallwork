ALTER TABLE penalidades
ADD COLUMN dt_publicacao DATE NULL DEFAULT NULL,
ADD COLUMN dt_registro_sicaf DATE NULL DEFAULT NULL;

ALTER TABLE apostilamentos
ADD COLUMN nu_apostilamento INT(11) NULL DEFAULT NULL AFTER ds_fundamento_legal,
ADD COLUMN vl_anual DECIMAL(10,2) NULL DEFAULT NULL AFTER nu_apostilamento,
ADD COLUMN vl_mensal DECIMAL(10,2) NULL DEFAULT NULL AFTER vl_anual,
ADD COLUMN dt_assinatura DATE NULL DEFAULT NULL AFTER vl_mensal,
ADD COLUMN co_nota_empenho INT NULL DEFAULT NULL AFTER dt_assinatura,
ADD COLUMN ds_recurso_apostilamento VARCHAR(255) NULL DEFAULT NULL AFTER co_nota_empenho,
ADD INDEX co_nota_empenho_idx (co_nota_empenho ASC);
ALTER TABLE apostilamentos
ADD CONSTRAINT co_nota_empenho
FOREIGN KEY (co_nota_empenho)
REFERENCES empenhos (co_empenho)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
