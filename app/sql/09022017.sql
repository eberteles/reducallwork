ALTER TABLE contratos_fiscais
ADD COLUMN nu_portaria_nomeacao INT(11) NULL DEFAULT NULL AFTER ic_ativo,
ADD COLUMN nu_boletim_especial INT(11) NULL DEFAULT NULL AFTER nu_portaria_nomeacao,
ADD COLUMN dt_boletim_especial DATE NULL DEFAULT NULL AFTER nu_boletim_especial;

ALTER TABLE garantias
ADD COLUMN nu_registro_siafi INT(11) NULL DEFAULT NULL;

ALTER TABLE notas_fiscais
ADD COLUMN ds_competencia VARCHAR(255) NULL DEFAULT NULL AFTER dt_vencimento,
ADD COLUMN dt_competencia DATE NULL DEFAULT NULL AFTER ds_competencia;
