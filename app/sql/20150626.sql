ALTER TABLE `dsp_despesas_viagem`
  DROP `nome_funcionario`,
  DROP `matricula_funcionario`;

ALTER TABLE  `dsp_despesas_viagem` ADD  `co_funcionario` INT( 11 ) NOT NULL AFTER  `co_despesas_viagem` ,
ADD INDEX (  `co_funcionario` );

ALTER TABLE  `dsp_despesas_viagem` ADD FOREIGN KEY (  `co_funcionario` ) REFERENCES  `usuarios` (
`co_usuario`
);
