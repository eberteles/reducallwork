ALTER TABLE `n2oti01`.`lct_licitacoes`
CHANGE COLUMN `modalidade_processo` `modalidade_processo` TEXT NULL ,
CHANGE COLUMN `dt` `dt` DATE NULL ,
CHANGE COLUMN `hora` `hora` VARCHAR(10) NULL DEFAULT '00:00' ,
CHANGE COLUMN `valor_estimado` `valor_estimado` DOUBLE NULL ,
CHANGE COLUMN `valor_contratado` `valor_contratado` DOUBLE NULL ,
CHANGE COLUMN `diferenca` `diferenca` DOUBLE NULL ,
CHANGE COLUMN `prazo_execucao` `prazo_execucao` VARCHAR(255) NULL ,
CHANGE COLUMN `empresa_vencedora` `empresa_vencedora` VARCHAR(255) NULL ,
CHANGE COLUMN `fase_atual` `fase_atual` TEXT NULL ,
CHANGE COLUMN `modalidade_licitacao` `modalidade_licitacao` VARCHAR(255) NULL ;

ALTER TABLE `n2oti01`.`dsp_log_despesas_viagem`
CHANGE COLUMN `cod_funcao` `cod_funcao` INT(11) NULL ,
CHANGE COLUMN `dt_inicio` `dt_inicio` DATE NULL ,
CHANGE COLUMN `dt_final` `dt_final` DATE NULL ,
CHANGE COLUMN `cod_meio_transporte` `cod_meio_transporte` INT(11) NULL ,
CHANGE COLUMN `cod_categoria_passagem` `cod_categoria_passagem` INT(11) NULL ,
CHANGE COLUMN `valor_passagem` `valor_passagem` DOUBLE NULL ,
CHANGE COLUMN `numero_diarias` `numero_diarias` INT(11) NULL ,
CHANGE COLUMN `valor_total_diarias` `valor_total_diarias` DOUBLE NULL ;

