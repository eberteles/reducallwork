ALTER TABLE dsp_despesas_viagem ADD COLUMN pais_origem VARCHAR(255) NULL;
ALTER TABLE dsp_despesas_viagem ADD COLUMN pais_destino VARCHAR(255) NULL;
ALTER TABLE dsp_despesas_viagem CHANGE COLUMN uf_origem uf_origem VARCHAR(2) NULL;
ALTER TABLE dsp_despesas_viagem CHANGE COLUMN uf_destino uf_destino VARCHAR(2) NULL;