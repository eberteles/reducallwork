ALTER TABLE `gesconti_local`.`contratos`
CHANGE COLUMN `co_modalidade` `co_modalidade` INT(11) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`co_contrato`, `co_modalidade`);