ALTER TABLE  `usuarios` ADD  `ic_campos_pesq_ctr` VARCHAR( 50 ) NULL;

CREATE TABLE IF NOT EXISTS `colunas_resultado_pesquisa` (
  `co_coluna_resultado` int(11) NOT NULL AUTO_INCREMENT,
  `ic_modulo` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `ds_nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ds_dominio` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ds_coluna_dominio` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ds_funcao_habilita` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_funcao_exibe` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_coluna_resultado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela de colunas disponíveis para resultado da Pesquisa';


INSERT INTO `colunas_resultado_pesquisa` (`co_coluna_resultado`, `ic_modulo`, `ds_nome`, `ds_dominio`, `ds_coluna_dominio`, `ds_funcao_habilita`, `ds_funcao_exibe`) VALUES
(1, 'ctr', 'PAM', 'Contrato', 'nu_pam', 'isPam', 'exibePam'),
(2, 'ctr', 'Processo', 'Contrato', 'nu_processo', NULL, 'exibeProcesso'),
(3, 'ctr', 'Contrato', 'Contrato', 'nu_contrato', NULL, 'exibeContrato'),
(4, 'ctr', 'Unidade Solicitante', 'Setor', 'ds_setor', NULL, NULL),
(5, 'ctr', 'Confederação', 'Confederacao', 'ds_confederacao', 'isInscricao', NULL),
(6, 'ctr', 'Fase/Situação Atual', 'Contrato', '', NULL, 'exibeFaseAtual'),
(7, 'ctr', 'Fornecedor', 'Fornecedor', 'no_razao_social', NULL, NULL),
(8, 'ctr', 'Objeto', 'Contrato', 'ds_objeto', NULL, NULL),
(9, 'ctr', 'Fim da vigência', 'Contrato', '', NULL, 'exibeFimDaVigencia'),
(10, 'ctr', 'Pendências', 'Contrato', '', NULL, 'exibePendencia'),
(11, 'ctr', 'Observação', 'Contrato', 'ds_observacao', NULL, NULL),
(12, 'ctr', 'Unidade Executante', 'Executante', 'ds_setor', 'isUnidadeExecutante', NULL),
(13, 'ctr', 'Gestor', 'GestorAtual', 'ds_nome', NULL, NULL),
(14, 'ctr', 'Fiscal', 'FiscalAtual', 'ds_nome', NULL, NULL);

ALTER TABLE  `contratos` ADD  `ic_tipo_contrato` CHAR( 1 ) NOT NULL DEFAULT  'I' AFTER  `ic_ativo` ,
ADD INDEX (  `ic_tipo_contrato` );

ALTER TABLE  `usuarios` ADD  `ic_campos_pesq_cte` VARCHAR( 50 ) NULL;

INSERT INTO `colunas_resultado_pesquisa` (`co_coluna_resultado`, `ic_modulo`, `ds_nome`, `ds_dominio`, `ds_coluna_dominio`, `ds_funcao_habilita`, `ds_funcao_exibe`) VALUES
(15, 'cte', 'Processo', 'Contrato', 'nu_processo', NULL, 'exibeProcesso'),
(16, 'cte', 'Contrato', 'Contrato', 'nu_contrato', NULL, 'exibeContrato'),
(17, 'cte', 'Fase/Situação Atual', 'Contrato', '', NULL, 'exibeFaseAtual'),
(18, 'cte', 'Cliente', 'Fornecedor', 'no_razao_social', NULL, NULL),
(19, 'cte', 'Objeto', 'Contrato', 'ds_objeto', NULL, NULL),
(20, 'cte', 'Fim da vigência', 'Contrato', '', NULL, 'exibeFimDaVigencia'),
(21, 'cte', 'Pendências', 'Contrato', '', NULL, 'exibePendencia'),
(22, 'cte', 'Observação', 'Contrato', 'ds_observacao', NULL, NULL),
(23, 'cte', 'Gestor', 'GestorAtual', 'ds_nome', NULL, NULL),
(24, 'cte', 'Fiscal', 'FiscalAtual', 'ds_nome', NULL, NULL);