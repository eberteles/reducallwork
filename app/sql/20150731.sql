ALTER TABLE `cronograma_financeiro_desembolso`
CHANGE COLUMN `percentual` `percentual` FLOAT NULL DEFAULT NULL ;

ALTER TABLE `contratos`
DROP FOREIGN KEY `contratos_ibfk_3`;
ALTER TABLE `contratos`
CHANGE COLUMN `co_modalidade` `co_modalidade` INT(11) NOT NULL ,
CHANGE COLUMN `co_ident_siasg` `co_ident_siasg` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`co_contrato`, `co_modalidade`);
ALTER TABLE `contratos`
ADD CONSTRAINT `contratos_ibfk_3`
FOREIGN KEY (`co_modalidade`)
REFERENCES `modalidades` (`co_modalidade`);
