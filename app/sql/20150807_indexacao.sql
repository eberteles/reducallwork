CREATE TABLE IF NOT EXISTS `anexos_indexacoes` (
  `co_anexo_indexacao` int(11) NOT NULL AUTO_INCREMENT,
  `co_anexo` int(11) NOT NULL,
  `nu_pagina` int(5) NOT NULL DEFAULT '1',
  `ds_conteudo` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_anexo_indexacao`),
  KEY `co_anexo` (`co_anexo`),
  FULLTEXT KEY `ds_conteudo` (`ds_conteudo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela para Indexação de Arquivos';

ALTER TABLE  `anexos` ADD  `ic_indexado` INT( 1 ) NULL DEFAULT  '0' COMMENT  'Indica se o documento já foi indexado para pesquisa';