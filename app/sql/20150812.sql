INSERT INTO `privilegios` (`co_privilegio`, `sg_privilegio`, `ds_privilegio`) VALUES ('1', 'Administrador', 'Administrador do Sistema');
INSERT INTO `privilegios` (`co_privilegio`, `sg_privilegio`, `ds_privilegio`) VALUES ('2', 'Fiscal', 'Fiscal');
INSERT INTO `privilegios` (`co_privilegio`, `sg_privilegio`, `ds_privilegio`) VALUES ('3', 'Gestor', 'Gestor');
INSERT INTO `privilegios` (`co_privilegio`, `sg_privilegio`, `ds_privilegio`) VALUES ('4', 'Autoridade', 'Consulta ao Sistema');
INSERT INTO `privilegios` (`co_privilegio`, `sg_privilegio`, `ds_privilegio`) VALUES ('5', 'Setor', 'Setor');
