CREATE TABLE `anexos_pastas` (
`co_anexo_pasta` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`parent_id` INT( 11 ) NULL ,
`co_ata` INT( 11 ) NULL ,
`co_contrato` INT( 11 ) NULL ,
`ds_anexo_pasta` VARCHAR( 100 ) NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Pastas para os Anexos';

ALTER TABLE  `anexos_pastas` ADD INDEX (  `parent_id` );
ALTER TABLE  `anexos_pastas` ADD INDEX (  `co_ata` );
ALTER TABLE  `anexos_pastas` ADD INDEX (  `co_contrato` );

ALTER TABLE  `anexos_pastas` ADD FOREIGN KEY (  `parent_id` ) REFERENCES  `anexos_pastas` (
`co_anexo_pasta`
);

ALTER TABLE  `anexos_pastas` ADD FOREIGN KEY (  `co_ata` ) REFERENCES  `atas` (
`co_ata`
);

ALTER TABLE  `anexos_pastas` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `anexos` ADD  `co_anexo_pasta` INT( 11 ) NULL AFTER  `co_anexo` ,
ADD INDEX (  `co_anexo_pasta` );