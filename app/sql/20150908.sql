ALTER TABLE
	empenhos
    ADD COLUMN empenho_originario INT NOT NULL DEFAULT 0;

ALTER TABLE
	empenhos
    ADD COLUMN pendente BOOLEAN NOT NULL DEFAULT 0;
    
ALTER TABLE 
  liquidacao 
  ADD COLUMN ds_liquidacao VARCHAR(500) NULL AFTER vl_liquidacao;