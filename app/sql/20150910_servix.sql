ALTER TABLE  `contratos` CHANGE  `nu_contrato`  `nu_contrato` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `log_contratos` CHANGE  `nu_contrato`  `nu_contrato` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `fornecedores` ADD  `no_preposto` VARCHAR( 100 ) NULL AFTER  `nu_rg_responsavel`;

ALTER TABLE  `log_fornecedores` ADD  `no_preposto` VARCHAR( 100 ) NULL AFTER  `nu_rg_responsavel`;

ALTER TABLE  `contratos` ADD  `dt_fim_suporte` DATE NULL AFTER  `dt_fim_vigencia_inicio`;
ALTER TABLE  `log_contratos` ADD  `dt_fim_suporte` DATE NULL AFTER  `dt_fim_vigencia_inicio`;

ALTER TABLE  `contratos` ADD  `nu_qtd_parcelas` INT( 3 ) NULL COMMENT  'Número de parcelar que o Contrato será dividido';
ALTER TABLE  `log_contratos` ADD  `nu_qtd_parcelas` INT( 3 ) NULL;

INSERT INTO `colunas_resultado_pesquisa` (
`co_coluna_resultado` , `ic_modulo` , `ds_nome` , `ds_dominio` ,
`ds_coluna_dominio` , `ds_funcao_habilita` , `ds_funcao_exibe`
)
VALUES (NULL ,  'ctr',  'Garantias de Suporte',  'Contrato',  '', NULL ,  'exibeGarantia');

ALTER TABLE  `anexos` ADD  `co_garantia_suporte` INT( 11 ) NULL AFTER  `co_garantia`;
ALTER TABLE  `anexos` ADD  `co_produto` INT( 11 ) NULL AFTER  `co_garantia_suporte`;
ALTER TABLE  `anexos` ADD  `co_oficio` INT( 11 ) NULL AFTER  `co_produto`;

CREATE TABLE `garantias_suporte` (
  `co_garantia_suporte` int(11) NOT NULL AUTO_INCREMENT,
  `co_contrato` int(11) NOT NULL,
  `co_usuario` int(11) DEFAULT NULL,
  `nu_serie` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `ds_observacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_garantia_suporte` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_garantia_suporte`),
  KEY `co_contrato` (`co_contrato`),
  KEY `co_usuario` (`co_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `log_garantias_suporte` (
  `co_log_garantias_suporte` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) DEFAULT NULL,
  `tp_acao` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',
  `co_garantia_suporte` int(11) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `nu_serie` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `ds_observacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_garantia_suporte` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_garantias_suporte`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TRIGGER IF EXISTS `ADD_LOG_GARANTIA_SUPORTE`;

DELIMITER //
CREATE TRIGGER `ADD_LOG_GARANTIA_SUPORTE` AFTER INSERT ON `garantias_suporte`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias_suporte SET
            dt_log          = NOW(),
            co_usuario      = NEW.co_usuario,
            tp_acao         = 'I',

            co_garantia_suporte = NEW.co_garantia_suporte,
            co_contrato         = NEW.co_contrato,
            nu_serie            = NEW.nu_serie,
            dt_inicio           = NEW.dt_inicio,
            dt_fim              = NEW.dt_fim,
            ds_observacao       = NEW.ds_observacao,
            ds_garantia_suporte = NEW.ds_garantia_suporte;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_GARANTIA_SUPORTE`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_GARANTIA_SUPORTE` AFTER UPDATE ON `garantias_suporte`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias_suporte SET
            dt_log          = NOW(),
            co_usuario      = NEW.co_usuario,
            tp_acao         = 'A',

            co_garantia_suporte = NEW.co_garantia_suporte,
            co_contrato         = NEW.co_contrato,
            nu_serie            = NEW.nu_serie,
            dt_inicio           = NEW.dt_inicio,
            dt_fim              = NEW.dt_fim,
            ds_observacao       = NEW.ds_observacao,
            ds_garantia_suporte = NEW.ds_garantia_suporte;
        END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `DEL_LOG_GARANTIA_SUPORTE`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_GARANTIA_SUPORTE` AFTER DELETE ON `garantias_suporte`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias_suporte SET
            dt_log          = NOW(),
            co_usuario      = OLD.co_usuario,
            tp_acao         = 'E',

            co_garantia_suporte = OLD.co_garantia_suporte,
            co_contrato         = OLD.co_contrato,
            nu_serie            = OLD.nu_serie,
            dt_inicio           = OLD.dt_inicio,
            dt_fim              = OLD.dt_fim,
            ds_observacao       = OLD.ds_observacao,
            ds_garantia_suporte = OLD.ds_garantia_suporte;
        END
//
DELIMITER ;

CREATE TABLE  `entregas` (
`co_entrega` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
`dt_entrega` DATE NOT NULL ,
`dt_entrega_oficial` DATE NOT NULL ,
`pz_recebimento` INT( 3 ) NOT NULL ,
`dt_recebimento` DATE NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Entregas previstas para o Contrato';

ALTER TABLE  `entregas` ADD INDEX (  `co_contrato` );
ALTER TABLE  `entregas` ADD INDEX (  `co_usuario` );

ALTER TABLE  `entregas` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `entregas` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

ALTER TABLE  `alertas` ADD  `ck_aviso_sup30` CHAR( 1 ) NULL DEFAULT  '0';

CREATE TABLE `produtos_tipos` (
`co_produto_tipo` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_usuario` INT( 11 ) NULL ,
`ds_tipo` VARCHAR( 255 ) NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tipos de Produtos';

INSERT INTO `produtos_tipos` (`co_produto_tipo`, `co_usuario`, `ds_tipo`) VALUES
(1, NULL, 'HARDWARE'),
(2, NULL, 'SOFTWARE'),
(3, NULL, 'SERVIÇO');

CREATE TABLE  `produtos` (
`co_produto` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_produto_tipo` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NULL ,
`ds_produto` VARCHAR( 100 ) NOT NULL ,
`nu_serie` VARCHAR( 100 ) NOT NULL ,
`ds_garantia` VARCHAR( 100 ) NOT NULL ,
`ds_fabricante` VARCHAR( 100 ) NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela para Aba de Produtos';

ALTER TABLE  `produtos` ADD INDEX (  `co_contrato` );
ALTER TABLE  `produtos` ADD INDEX (  `co_produto_tipo` );
ALTER TABLE  `produtos` ADD INDEX (  `co_usuario` );

ALTER TABLE  `produtos` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `produtos` ADD FOREIGN KEY (  `co_produto_tipo` ) REFERENCES  `produtos_tipos` (
`co_produto_tipo`
);

ALTER TABLE  `produtos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

INSERT INTO  `produtos_tipos` (`co_produto_tipo` ,`co_usuario` ,`ds_tipo`)
VALUES (NULL , NULL ,  'HARDWARE'), (NULL , NULL ,  'SOFTWARE'), (NULL , NULL ,  'SERVIÇO');

CREATE TABLE  `oficios` (
`co_oficio` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NULL ,
`nu_oficio` VARCHAR( 100 ) NOT NULL ,
`dt_recebimento` date DEFAULT NULL,
`dt_resposta` date DEFAULT NULL,
`pz_resposta` INT( 3 ) NULL 
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela para Aba de Ofícios';

ALTER TABLE  `oficios` ADD INDEX (  `co_contrato` );
ALTER TABLE  `oficios` ADD INDEX (  `co_usuario` );

ALTER TABLE  `oficios` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `oficios` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);