ALTER TABLE pre_empenho ADD COLUMN nu_pre_empenho VARCHAR(25) NULL;
ALTER TABLE pre_empenho ADD COLUMN ds_pre_empenho VARCHAR(255) NULL;
ALTER TABLE pre_empenho ADD COLUMN vl_pre_empenho DOUBLE NULL;
ALTER TABLE pre_empenho ADD COLUMN ds_fonte_recurso VARCHAR(80) NULL;
ALTER TABLE pre_empenho ADD COLUMN dt_pre_empenho DATE NULL;
ALTER TABLE pre_empenho ADD COLUMN nu_natureza_despesa INT NULL;
ALTER TABLE pre_empenho ADD COLUMN ic_ativo INT NULL DEFAULT 1;