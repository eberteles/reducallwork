ALTER TABLE empenhos ADD COLUMN co_pre_empenho INT NULL;
ALTER TABLE empenhos ADD CONSTRAINT fk_empenho_pre_empenho FOREIGN KEY (co_pre_empenho) REFERENCES pre_empenho(co_pre_empenho);

ALTER TABLE pre_empenho ADD COLUMN co_contrato INT NULL;
ALTER TABLE pre_empenho ADD CONSTRAINT fk_pre_empenho_contrato FOREIGN KEY (co_contrato) REFERENCES contratos(co_contrato);