CREATE TABLE IF NOT EXISTS aditivos_backup
SELECT * FROM aditivos;

DROP procedure IF EXISTS `aditivos_migrate01`;

DELIMITER $$
CREATE PROCEDURE `aditivos_migrate01`()
BEGIN

DECLARE cursor_ID INT;
DECLARE cursor_DTADITIVO DATE;
DECLARE cursor_DTASSINATURA DATE;

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_i CURSOR FOR SELECT co_aditivo,dt_aditivo,dt_assinatura FROM aditivos;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN cursor_i;
read_loop: LOOP
    FETCH cursor_i INTO cursor_ID, cursor_DTADITIVO, cursor_DTASSINATURA;
    IF done THEN
        LEAVE read_loop;
    END IF;
    IF cursor_DTASSINATURA IS NULL THEN
      UPDATE aditivos SET dt_assinatura = cursor_DTADITIVO WHERE co_aditivo = cursor_ID;
    END IF;
END LOOP;
CLOSE cursor_i;

END$$

DELIMITER ;

CALL aditivos_migrate01();