ALTER TABLE empenhos ADD COLUMN empenho_reforco_originario VARCHAR(20) NULL;
ALTER TABLE empenhos ADD COLUMN empenho_anulacao_originario VARCHAR(20) NULL;
ALTER TABLE empenhos ADD COLUMN vl_restante DOUBLE NULL;
ALTER TABLE empenhos CHANGE COLUMN vl_empenho vl_empenho DOUBLE NULL;
ALTER TABLE empenhos CHANGE COLUMN nu_empenho nu_empenho VARCHAR(12) NULL;