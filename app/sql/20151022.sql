CREATE TABLE IF NOT EXISTS empenhos_backup
	SELECT * FROM empenhos;

DROP procedure IF EXISTS `empenhos_migrate01`;

DELIMITER $$
CREATE PROCEDURE `empenhos_migrate01`()
BEGIN

DECLARE counter INT DEFAULT 1;
DECLARE cursor_ID INT;
DECLARE cursor_EMPENHO_ORIGINARIO INT;
DECLARE cursor_TP_ANULACAO CHAR(1);
DECLARE cursor_DT_ANULACAO DATE;
DECLARE cursor_VL_ANULACAO DOUBLE;
DECLARE cursor_NU_EMPENHO VARCHAR(20);

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_i CURSOR FOR SELECT
		co_empenho_anulacao,co_empenho,tp_anulacao,dt_anulacao,vl_anulacao,nu_empenho FROM empenhos_anulacoes;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SET @counter = 1;
SET @max = (SELECT MAX(co_empenho) FROM empenhos);

OPEN cursor_i;
read_loop: LOOP
    FETCH cursor_i INTO cursor_ID, cursor_EMPENHO_ORIGINARIO, cursor_TP_ANULACAO, cursor_DT_ANULACAO, cursor_VL_ANULACAO, cursor_NU_EMPENHO;
    IF done THEN
        LEAVE read_loop;
    END IF;
    IF cursor_NU_EMPENHO IS NOT NULL THEN
		INSERT empenhos SET co_empenho = (@max + @counter), nu_empenho = cursor_NU_EMPENHO, vl_empenho = cursor_VL_ANULACAO, dt_empenho = cursor_DT_ANULACAO, empenho_anulacao_originario = cursor_EMPENHO_ORIGINARIO, tp_empenho = 'A';
	END IF;
    IF cursor_NU_EMPENHO IS NULL THEN
		INSERT empenhos SET co_empenho = (@max + @counter), vl_empenho = cursor_VL_ANULACAO, dt_empenho = cursor_DT_ANULACAO, empenho_anulacao_originario = cursor_EMPENHO_ORIGINARIO, tp_empenho = 'A';
	END IF;
    SET @counter = @counter + 1;
END LOOP;
CLOSE cursor_i;

END$$

DELIMITER ;


DROP procedure IF EXISTS `empenhos_migrate02`;

DELIMITER $$
CREATE PROCEDURE `empenhos_migrate02`()
BEGIN

DECLARE cursor_ID INT;
DECLARE cursor_VL_EMPENHO DOUBLE;
DECLARE cursor_VL_RESTANTE DOUBLE;

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_i CURSOR FOR SELECT
		co_empenho,vl_empenho,vl_restante FROM empenhos;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN cursor_i;
read_loop: LOOP
    FETCH cursor_i INTO cursor_ID, cursor_VL_EMPENHO, cursor_VL_RESTANTE;
    IF done THEN
        LEAVE read_loop;
    END IF;
	UPDATE empenhos SET vl_restante = cursor_VL_EMPENHO WHERE co_empenho = cursor_ID;
    SET @max = (SELECT SUM(vl_anulacao) FROM empenhos_anulacoes WHERE co_empenho = cursor_ID);
    IF @max IS NOT NULL THEN
		UPDATE empenhos SET vl_empenho = (cursor_VL_EMPENHO + @max) WHERE co_empenho = cursor_ID;
	END IF;
END LOOP;
CLOSE cursor_i;

END$$

DELIMITER ;

CALL empenhos_migrate01();
CALL empenhos_migrate02();



