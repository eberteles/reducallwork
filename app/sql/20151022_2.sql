ALTER TABLE aditivos CHANGE COLUMN vl_aditivo vl_aditivo DOUBLE NULL;

ALTER TABLE notas_fiscais CHANGE COLUMN vl_nota vl_nota DOUBLE NULL;
ALTER TABLE notas_fiscais CHANGE COLUMN vl_glosa vl_glosa DOUBLE NULL;

ALTER TABLE pagamentos CHANGE COLUMN vl_nota vl_nota DOUBLE NULL;
ALTER TABLE pagamentos CHANGE COLUMN vl_imposto vl_imposto DOUBLE NULL;
ALTER TABLE pagamentos CHANGE COLUMN vl_liquido vl_liquido DOUBLE NULL;
ALTER TABLE pagamentos CHANGE COLUMN vl_pago vl_pago DOUBLE NULL;
ALTER TABLE pagamentos CHANGE COLUMN vl_pagamento vl_pagamento DOUBLE NULL;