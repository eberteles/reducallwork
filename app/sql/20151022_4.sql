ALTER TABLE contratos ADD COLUMN insert_date timestamp NOT NULL DEFAULT current_timestamp;

DROP function IF EXISTS `MONTHNAME_PT`;

DELIMITER $$

CREATE FUNCTION `MONTHNAME_PT`(monthnumber INT) RETURNS varchar(15) CHARSET latin1
BEGIN

DECLARE mes VARCHAR(15);

CASE monthnumber
	WHEN 1 THEN
		SET mes = 'Janeiro';
	WHEN 2 THEN
		SET mes = 'Fevereiro';
	WHEN 3 THEN
		SET mes = 'Março';
	WHEN 4 THEN
		SET mes = 'Abril';
	WHEN 5 THEN
		SET mes = 'Maio';
	WHEN 6 THEN
		SET mes = 'Junho';
	WHEN 7 THEN
		SET mes = 'Julho';
  WHEN 8 THEN
		SET mes = 'Agosto';
  WHEN 9 THEN
		SET mes = 'Setembro';
  WHEN 10 THEN
		SET mes = 'Outubro';
  WHEN 11 then
		SET mes = 'Novembro';
  WHEN 12 THEN
		SET mes = 'Dezembro';
	ELSE
		SET mes = 'NULO';
END CASE;

RETURN mes;
END$$

DELIMITER ;

