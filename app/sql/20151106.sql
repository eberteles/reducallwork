CREATE TABLE contatos(
	co_contato INT NOT NULL AUTO_INCREMENT,
    co_fornecedor INT NOT NULL,
    tipo_contato VARCHAR(30) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    telefone1 VARCHAR(11) NOT NULL,
    telefone2 VARCHAR(11) NOT NULL,
    email VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_contato)
);

ALTER TABLE contatos ADD CONSTRAINT fk_contatos_fornecedor FOREIGN KEY(co_fornecedor) REFERENCES fornecedores(co_fornecedor) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE locais_atendimento(
	co_locais_atendimento INT NOT NULL AUTO_INCREMENT,
    co_fornecedor INT NOT NULL,
    uf VARCHAR(2) NOT NULL,
    endereco VARCHAR(100) NULL,
    observacoes VARCHAR(250) NULL,
    municipio INT NOT NULL,
    telefone1 VARCHAR(11) NOT NULL,
    telefone2 VARCHAR(11)  NULL,
    PRIMARY KEY(co_locais_atendimento)
);

ALTER TABLE locais_atendimento ADD CONSTRAINT fk_locais_fornecedor FOREIGN KEY(co_fornecedor) REFERENCES fornecedores(co_fornecedor) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE reclamacoes(
	co_reclamacao INT NOT NULL AUTO_INCREMENT,
    nu_reclamacao CHAR(8) NOT NULL,
    co_fornecedor INT NOT NULL,
	co_contrato INT NOT NULL,
    tp_reclamacao VARCHAR(25) NULL,
    dt_problema DATE NOT NULL,
    assunto_reclamacao VARCHAR(50) NOT NULL,
    ds_reclamacao VARCHAR(250) NULL,
    PRIMARY KEY(co_reclamacao)
);

CREATE TABLE providencias(
	co_providencia INT NOT NULL AUTO_INCREMENT,
    co_reclamacao INT NOT NULL,
    dt_reclamacao TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    co_usuario INT NOT NULL,
    ds_providencia VARCHAR(500) NOT NULL,
    PRIMARY KEY(co_providencia)
);

ALTER TABLE reclamacoes ADD CONSTRAINT fk_reclamacao_fornecedor FOREIGN KEY(co_fornecedor) REFERENCES fornecedores(co_fornecedor) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE reclamacoes ADD CONSTRAINT fk_reclamacao_contrato FOREIGN KEY(co_contrato) REFERENCES contratos(co_contrato) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE providencias ADD CONSTRAINT fk_providencia_reclamacao FOREIGN KEY(co_reclamacao) REFERENCES reclamacoes(co_reclamacao) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE providencias ADD CONSTRAINT fk_providencia_usuario FOREIGN KEY(co_usuario) REFERENCES usuarios(co_usuario) ON DELETE RESTRICT ON UPDATE CASCADE;
