CREATE TABLE elogios(
	co_elogio INT NOT NULL AUTO_INCREMENT,
    co_fornecedor INT NOT NULL,
    co_contrato INT NOT NULL,
    nu_elogio CHAR(8) NOT NULL,
    ds_elogio VARCHAR(300) NOT NULL,
    PRIMARY KEY(co_elogio)
);

ALTER TABLE elogios ADD CONSTRAINT fk_elogios_fornecedor FOREIGN KEY(co_fornecedor) REFERENCES fornecedores(co_fornecedor) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE elogios ADD CONSTRAINT fk_elogios_contrato FOREIGN KEY(co_contrato) REFERENCES contratos(co_contrato) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE fornecedores ADD COLUMN indicador_valor VARCHAR(15) NULL;
ALTER TABLE fornecedores ADD COLUMN indicador_importancia VARCHAR(15) NULL;
ALTER TABLE fornecedores ADD COLUMN ocs_psa CHAR(3) NULL;

CREATE TABLE graduacao(
	co_graduacao INT NOT NULL AUTO_INCREMENT,
    ds_graduacao VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_graduacao)
);

CREATE TABLE tipo_inspecao(
	co_tipo_inspecao INT NOT NULL AUTO_INCREMENT,
    ds_tipo_inspecao VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_tipo_inspecao)
);

CREATE TABLE especialidade(
	co_especialidade INT NOT NULL AUTO_INCREMENT,
    ds_especialidade VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_especialidade)
);

ALTER TABLE usuarios ADD COLUMN co_graduacao INT NULL;
ALTER TABLE usuarios ADD COLUMN no_guerra VARCHAR(30) NULL;
ALTER TABLE usuarios ADD CONSTRAINT fk_usuarios_graduacao FOREIGN KEY(co_graduacao) REFERENCES graduacao(co_graduacao) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE usuarios ADD COLUMN nu_conselho VARCHAR(20) NULL;

ALTER TABLE atividades ADD COLUMN co_tipo_inspecao INT NULL;
ALTER TABLE atividades ADD COLUMN ds_assunto_email VARCHAR(50) NULL;

CREATE TABLE locais_especialidades(
	co_locais_atendimento INT NOT NULL,
    co_especialidade INT NOT NULL,
    PRIMARY KEY(co_locais_atendimento,co_especialidade)
);

ALTER TABLE locais_especialidades ADD CONSTRAINT fk_la_especialidade FOREIGN KEY(co_locais_atendimento) REFERENCES locais_atendimento(co_locais_atendimento) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE locais_especialidades ADD CONSTRAINT fk_especialidade_la FOREIGN KEY(co_especialidade) REFERENCES especialidade(co_especialidade) ON DELETE RESTRICT ON UPDATE CASCADE;