DELIMITER $$
CREATE PROCEDURE `objeto_migrate`()
BEGIN

DECLARE counter INT DEFAULT 1;
DECLARE cursor_ID INT;
DECLARE cursor_OBSERVACOES VARCHAR(255);
DECLARE cursor_OBJETO VARCHAR(1500);
DECLARE cursor_PAM VARCHAR(30);

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_i CURSOR FOR SELECT
		co_contrato,ds_observacao,ds_objeto,nu_pam FROM contratos;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN cursor_i;
read_loop: LOOP
    FETCH cursor_i INTO cursor_ID, cursor_OBSERVACOES, cursor_OBJETO, cursor_PAM;
    IF done THEN
        LEAVE read_loop;
    END IF;
    IF cursor_OBJETO IS NULL AND cursor_PAM IS NOT NULL THEN
		UPDATE contratos SET ds_objeto = cursor_OBSERVACOES, ds_observacao = '' WHERE co_contrato = cursor_ID;
	END IF;
END LOOP;
CLOSE cursor_i;

END$$

DELIMITER ;