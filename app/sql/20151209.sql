CREATE TABLE local_especialidades(
	co_local_especialidades INT NOT NULL AUTO_INCREMENT,
    co_especialidade INT NOT NULL,
    co_locais_atendimento INT NOT NULL,
	PRIMARY KEY(co_local_especialidades, co_especialidade, co_locais_atendimento)
);

ALTER TABLE local_especialidades ADD CONSTRAINT fk_especialidade_locais_atendimento FOREIGN KEY(co_especialidade) REFERENCES especialidade(co_especialidade) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE local_especialidades ADD CONSTRAINT fk_locais_atendimento_especialidade FOREIGN KEY(co_locais_atendimento) REFERENCES locais_atendimento(co_locais_atendimento) ON DELETE RESTRICT ON UPDATE CASCADE;