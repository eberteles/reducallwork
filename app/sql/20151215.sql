DROP TABLE locais_especialidades;
CREATE TABLE locais_especialidades(
  id INT NOT NULL AUTO_INCREMENT,
  co_locais_atendimento INT NOT NULL,
  co_especialidade INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (co_locais_atendimento) REFERENCES locais_atendimento(co_locais_atendimento),
  FOREIGN KEY (co_especialidade) REFERENCES especialidade(co_especialidade)
);

ALTER TABLE elogios ADD COLUMN nu_prefix INT NOT NULL;
ALTER TABLE elogios ADD COLUMN nu_year INT NULL;
ALTER TABLE reclamacoes ADD COLUMN nu_prefix INT NOT NULL;
ALTER TABLE reclamacoes ADD COLUMN nu_year INT NULL;