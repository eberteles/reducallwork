ALTER TABLE liquidacao ADD COLUMN co_contrato INT NULL;
ALTER TABLE liquidacao ADD FOREIGN KEY (co_contrato) REFERENCES contratos(co_contrato);