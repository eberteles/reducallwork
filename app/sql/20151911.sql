UPDATE
  filtros
SET
  no_filtro = 'gestor',
  tp_filtro = 'select',
  ds_filtro = 'Gestor do Contrato',
  no_coluna = 'Usuario.co_usuario',
  no_dominio = 'Usuario',
  ds_condicao = 'Usuario.co_privilegio = 3'
WHERE
  co_filtro='21';