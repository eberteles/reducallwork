CREATE TABLE  `contratos_contatos` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
`nome` VARCHAR( 100 ) NOT NULL ,
`cargo` VARCHAR( 100 ) NULL ,
`departamento` VARCHAR( 100 ) NULL ,
`email` VARCHAR( 100 ) NULL ,
`telefone_um` VARCHAR( 15 ) NULL ,
`telefone_dois` VARCHAR( 15 ) NULL ,
`endereco` VARCHAR( 500 ) NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Contatos Relativos a um Contrato';

ALTER TABLE  `contratos_contatos` ADD INDEX (  `co_contrato` );

ALTER TABLE  `contratos_contatos` ADD INDEX (  `co_usuario` );

ALTER TABLE  `contratos_contatos` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `contratos_contatos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);