CREATE TABLE IF NOT EXISTS `numeracao_pam` (
  `aa_pam` year(4) NOT NULL,
  `ct_pam` int(4) NOT NULL,
  UNIQUE KEY `aa_pam` (`aa_pam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE  `contratos` ADD  `ds_requisitante` VARCHAR( 100 ) NULL ,
ADD  `pt_requisitante` VARCHAR( 100 ) NULL ,
ADD  `tl_requisitante` VARCHAR( 15 ) NULL ,
ADD  `dt_solicitacao` DATE NULL ,
ADD  `ds_assunto` VARCHAR( 250 ) NULL ,
ADD  `ds_objeto_plano` VARCHAR( 500 ) NULL ,
ADD  `ds_vantagem` VARCHAR( 500 ) NULL ,
ADD  `ds_criterio` VARCHAR( 500 ) NULL ,
ADD  `ds_justificativa` VARCHAR( 500 ) NULL ,
ADD  `ds_resultado` VARCHAR( 500 ) NULL ,
ADD  `ds_demanda` VARCHAR( 500 ) NULL ,
ADD  `ds_aproveitamento` VARCHAR( 500 ) NULL

CREATE TABLE  `contratos_itens` (
`co_contrato_item` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_fornecedor` INT( 11 ) NULL ,
`nu_ordem` INT( 4 ) NOT NULL ,
`nu_item_pregao` INT( 10 ) NULL ,
`nu_unidade` VARCHAR( 25 ) NOT NULL ,
`qt_solicitada` INT( 10 ) NOT NULL ,
`vl_unitario` DOUBLE NULL ,
`ds_demanda` VARCHAR( 250 ) NOT NULL ,
`co_siasg` INT( 10 ) NULL ,
`qt_consumo` INT( 10 ) NULL ,
`co_cat` VARCHAR( 15 ) NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Itens do PAM';

ALTER TABLE  `contratos_itens` ADD INDEX (  `co_contrato` );

ALTER TABLE  `contratos_itens` ADD INDEX (  `co_fornecedor` );

ALTER TABLE  `contratos_itens` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `contratos_itens` ADD FOREIGN KEY (  `co_fornecedor` ) REFERENCES  `fornecedores` (
`co_fornecedor`
);

ALTER TABLE  `contratos` ADD  `ds_gerenciador_ata` VARCHAR( 50 ) NULL;