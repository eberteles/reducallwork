ALTER TABLE `usuarios`
ADD COLUMN `licenca_nominal` VARCHAR(1) NULL DEFAULT NULL,
ADD COLUMN `licenca_concorrente` VARCHAR(1) NULL DEFAULT NULL,
ADD COLUMN `licenca_bi` VARCHAR(1) NULL DEFAULT NULL;

UPDATE `usuarios` SET `licenca_nominal`='1' WHERE `co_usuario`!='0';