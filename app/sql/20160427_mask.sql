DROP FUNCTION mask;

CREATE FUNCTION `mask`(unformatted_value char(32), format_string char(32)) RETURNS char(32) CHARSET utf8
    DETERMINISTIC
BEGIN
# declare variables
declare input_len tinyint;
declare output_len tinyint;
declare ini_unf tinyint;
declare ini_len tinyint;
declare temp_char char;

# initialize variables
set input_len = length(unformatted_value);
set output_len = length(format_string);
set ini_len = 1;
set ini_unf = 1;

# versão nova
while ( ini_len <= output_len ) do

    set temp_char = substr(format_string, ini_len, 1);
    if ( temp_char = '#' ) then
        if ( ini_unf <= input_len ) then
            set format_string = insert(format_string, ini_len, 1, substr(unformatted_value, ini_unf, 1));
            set ini_unf = ini_unf + 1;
        else
            set format_string = insert(format_string, ini_len, 1, '0');
        end if;
    end if;

    set ini_len = ini_len + 1;
end while;

return format_string;
END