create table assinaturas_login(
  id int not null auto_increment primary key,
  cod_usuario int null,
  cod_contrato int null,
  constraint fk_usuario_assinatura
  foreign key(cod_usuario) references usuarios(co_usuario),
  constraint fk_contrato_assinatura
  foreign key(cod_contrato) references contratos(co_contrato)
);

create table assinaturas_requisitante(
  id int not null auto_increment primary key,
  nome_completo varchar(75) not null,
  cargo varchar(45) null,
  data_assinatura datetime null,
  referencia text null,
  co_contrato int not null
);

alter table contratos
  add column co_assinatura_ordenador int null,
  add column co_assinatura_chefe int null,
  add constraint fk_assinatura_ordenador foreign key(co_assinatura_ordenador) references usuarios(co_usuario),
  add constraint fk_assinatura_chefia foreign key(co_assinatura_chefe) references usuarios(co_usuario);