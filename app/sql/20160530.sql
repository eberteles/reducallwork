create table bairros(
	id int not null auto_increment primary key,
  bairro varchar(80) not null
);

alter table locais_atendimento
	add column co_bairro int null,
  add constraint fk_locais_atendimento_bairro foreign key(co_bairro) references bairros(id);

create table exames_consultas(
	id int not null auto_increment primary key,
  codigo varchar(15) not null,
  nome varchar(80) not null,
  observacoes varchar(150) null,
  co_especialidade int not null,
  constraint fk_espec_exme_cnslta foreign key(co_especialidade) references especialidade(co_especialidade)
);