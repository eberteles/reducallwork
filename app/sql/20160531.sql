INSERT INTO bairros(bairro) VALUES("Bairro Alto da Boa Vista (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Arapoanga (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Área Alfa (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Área de Desenvolvimento Econômico (Águas Claras)");
INSERT INTO bairros(bairro) VALUES("Bairro Área de Desenvolvimento Econômico (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Área de Desenvolvimento Econômico (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Área Octogonal");
INSERT INTO bairros(bairro) VALUES("Bairro Área Universitária (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Areal (Águas Claras)");
INSERT INTO bairros(bairro) VALUES("Bairro Asa Norte");
INSERT INTO bairros(bairro) VALUES("Bairro Asa Sul");
INSERT INTO bairros(bairro) VALUES("Bairro Bela Vista (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Bonsucesso (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Brazlândia");
INSERT INTO bairros(bairro) VALUES("Bairro Campus Universitário Darcy Ribeiro");
INSERT INTO bairros(bairro) VALUES("Bairro Candangolândia");
INSERT INTO bairros(bairro) VALUES("Bairro Ceilândia Centro (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Ceilândia Norte (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Ceilândia Sul (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Centro (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Cidade Nova (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Chácaras Ana Maria (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Comercial e Residencial Sobradinho (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Coohaplan - Itiquira (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Guirra (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Império dos Nobres (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Mansões Sobradinho (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Mestre D'Armas (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Mirante da Serra (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Nosso Lar (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Nova Esperança (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Parque Mônaco (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Parque Mônaco II (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Prado (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Privê Lucena Roriz (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Residencial Morada Nobre (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Residencial Santa Maria (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Residencial Santa Mônica (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Santa Mônica (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Condomínio Vale dos Pinheiros (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Cruzeiro Novo");
INSERT INTO bairros(bairro) VALUES("Bairro Cruzeiro Velho");
INSERT INTO bairros(bairro) VALUES("Bairro Del Lago I (Itapoã)");
INSERT INTO bairros(bairro) VALUES("Bairro Del Lago II (Itapoã)");
INSERT INTO bairros(bairro) VALUES("Bairro Del Lago II (Paranoá)");
INSERT INTO bairros(bairro) VALUES("Bairro Engenho das Lages (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas I (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas II (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas III (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas IV (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas V (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Mestre D'Armas VI (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estância Planaltina (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Estâncias Vila Rica (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Fazenda Mestre D'Armas (Etapa I - Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Fazenda Mestre D'Armas (Etapa II - Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Fazenda Mestre D'Armas (Etapa III - Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Fazendinha (Itapoã)");
INSERT INTO bairros(bairro) VALUES("Bairro Gama");
INSERT INTO bairros(bairro) VALUES("Bairro Grande Colorado (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Granja do Torto");
INSERT INTO bairros(bairro) VALUES("Bairro Guará I");
INSERT INTO bairros(bairro) VALUES("Bairro Guará II");
INSERT INTO bairros(bairro) VALUES("Bairro Incra 8 (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Itapoã I");
INSERT INTO bairros(bairro) VALUES("Bairro Itapoã II");
INSERT INTO bairros(bairro) VALUES("Bairro Jardim Roriz (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Jardins Mangueiral (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro João Cândido (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Mansões Abraão I (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Mansões Abraão II (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Mansões do Amanhecer (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Metropolitana (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Morro Azul (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Norte (Águas Claras)");
INSERT INTO bairros(bairro) VALUES("Bairro Nossa Senhora de Fátima (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Nova Colina (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Bandeirante");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Rural Alagados (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Rural Hortigranjeiro de Santa Maria");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Rural Lago Oeste (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Rural Santa Maria");
INSERT INTO bairros(bairro) VALUES("Bairro Núcleo Rural Vargem Bonita (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Paranoá");
INSERT INTO bairros(bairro) VALUES("Bairro Paranoá Parque (Paranoá)");
INSERT INTO bairros(bairro) VALUES("Bairro Planaltina");
INSERT INTO bairros(bairro) VALUES("Bairro Ponte Alta (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Portal do Amanhecer (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Portal do Amanhecer I (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Portal do Amanhecer III (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Portal do Amanhecer V (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Portal do Amanhecer V (Privê - Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Quadras Econômicas Lúcio Costa (Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Quinta dos Ipês (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Quintas do Amanhecer II (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Quintas do Amanhecer III (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Recanto das Emas");
INSERT INTO bairros(bairro) VALUES("Bairro Recanto do Sossego (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Recanto Feliz (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Região dos Lagos (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Bica do DER (Gleba B - Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Cachoeira (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Condomínio Marissol (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial do Bosque (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Flamboyant (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Itaipu (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Morro da Cruz (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Nova Esperança (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Nova Planaltina (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Samauma (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Sandray (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Santos Dumont (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial São Francisco I (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial São Francisco II (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Sarandy (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Residencial Vitória (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Riacho Fundo I");
INSERT INTO bairros(bairro) VALUES("Bairro Riacho Fundo II");
INSERT INTO bairros(bairro) VALUES("Bairro Samambaia Norte (Samambaia)");
INSERT INTO bairros(bairro) VALUES("Bairro Samambaia Sul (Samambaia)");
INSERT INTO bairros(bairro) VALUES("Bairro San Sebastian (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Santa Maria");
INSERT INTO bairros(bairro) VALUES("Bairro São Bartolomeu (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro São Francisco (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro São Gabriel (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Serra Azul (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Administrativo (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Central (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Central (Vila Estrutural - Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Comercial Central (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Áreas Isoladas Sul (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Chácaras Córrego da Onça (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Desenvolvimento Econômico (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Educação (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Habitações Individuais Norte");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Habitações Individuais Sul");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Hotéis e Diversões (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Indústrias Bernardo Sayão (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Mansões de Sobradinho");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Mansões do Lago Norte");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Mansões Dom Bosco");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Mansões Mestre D'Armas (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Mansões Park Way");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Materiais de Construção (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Postos e Motéis Norte (Lago Norte)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor de Postos e Motéis Sul (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Econômico de Sobradinho (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Especial (Vila Estrutural - Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Arniqueira (Águas Claras)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Contagem (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Fercal (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Jardim Botânico (Lago Sul)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Pôr do Sol (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Ribeirão (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Samambaia (Vicente Pires)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Sol Nascente (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Taquari (Lago Norte)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Tororó (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Vereda Grande (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Vicente Pires");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Habitacional Vicente Pires (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Hospitalar (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Industrial (Ceilândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Industrial (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Industrial (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Industrial (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Leste (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Leste (Vila Estrutural - Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Mansões Itiquira (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Meireles (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Militar Urbano");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Noroeste");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Norte (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Norte (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Norte (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Norte (Vila Estrutural - Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Oeste (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Oeste (Sobradinho II)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Oeste (Vila Estrutural - Guará)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Placa da Mercedes (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Policial Sul");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Recreativo e Cultural (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Residencial Leste (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Residencial Mestre D'Armas (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Residencial Norte (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Residencial Oeste (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Residencial Oeste (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Sudoeste");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Sul (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Sul (Gama)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Sul (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Tradicional (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Tradicional (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Setor Tradicional (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Setores Complementares");
INSERT INTO bairros(bairro) VALUES("Bairro Sobradinho");
INSERT INTO bairros(bairro) VALUES("Bairro Sul (Águas Claras)");
INSERT INTO bairros(bairro) VALUES("Bairro Taguatinga Centro (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Taguatinga Norte (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Taguatinga Sul (Taguatinga)");
INSERT INTO bairros(bairro) VALUES("Bairro Taquara (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Vale das Acácias (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Vale do Amanhecer (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Vale do Sol (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Varjão");
INSERT INTO bairros(bairro) VALUES("Bairro Veneza I (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Veneza II (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Veneza III (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Veredas (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Cauhy (Núcleo Bandeirante)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila da Telebrasília");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Dimas (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila do Boa (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Estrutural");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Feliz (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Nossa Senhora de Fátima (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Nova (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Planalto");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Rabelo I (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Rabelo II (Sobradinho)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila São José (Brazlândia)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila São José (São Sebastião)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila São José (Vicente Pires)");
INSERT INTO bairros(bairro) VALUES("Bairro Vila Vicentina (Planaltina)");
INSERT INTO bairros(bairro) VALUES("Bairro Zona Cívico-Administrativa");
INSERT INTO bairros(bairro) VALUES("Bairro Zona de Dinamização (Santa Maria)");
INSERT INTO bairros(bairro) VALUES("Bairro Zona Industrial");
INSERT INTO bairros(bairro) VALUES("Bairro Zona Industrial (Guará)");
INSERT INTO bairros(bairro) VALUES("Outros Códigos Postais");
