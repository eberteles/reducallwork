ALTER TABLE `liquidacao`
CHANGE COLUMN `vl_liquidacao` `vl_liquidacao` DOUBLE NULL DEFAULT NULL ;

create table locais_exames(
	id int not null auto_increment primary key,
  co_locais_atendimento int not null,
  co_exames int not null,
  constraint fk_locais_exames foreign key(co_locais_atendimento) references locais_atendimento(co_locais_atendimento),
  constraint fk_exames_locais foreign key(co_exames) references exames_consultas(id)
);