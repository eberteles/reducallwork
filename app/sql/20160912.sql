-- Este sql é so para o HFA

ALTER TABLE atividades ADD nome VARCHAR(255) NOT NULL;
ALTER TABLE atividades ADD periodicidade VARCHAR(255) DEFAULT NULL;
ALTER TABLE atividades ADD dia_execucao VARCHAR(255) DEFAULT NULL;
ALTER TABLE atividades ADD funcao_responsavel VARCHAR(255) NOT NULL;
/*ALTER TABLE n2oti01.atividades ADD todos_contratos INT(1) NOT NULL;*/
ALTER TABLE atividades DROP COLUMN todos_contratos;

ALTER TABLE atividades ADD pendencia VARCHAR(255);

/*CREATE TABLE n2oti01.atividades_contratos (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	id_atividade INT NOT NULL,
	id_contrato INT NULL,
	co_responsavel INT NULL,
	CONSTRAINT atividades_contratos_atividades_FK FOREIGN KEY (id_atividade) REFERENCES n2oti01.atividades(co_atividade),
	CONSTRAINT atividades_contratos_contratos_FK FOREIGN KEY (id_contrato) REFERENCES n2oti01.contratos(co_contrato)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;*/
