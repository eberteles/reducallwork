ALTER TABLE contratos ADD ds_tipo_servico VARCHAR(255) DEFAULT NULL NULL;
ALTER TABLE usuarios ADD COLUMN is_logged INT NOT NULL DEFAULT 0 COMMENT 'Flag de verificação de usuário logado (0 => deslogado, 1 => logado)';

ALTER TABLE setores ADD no_responsavel VARCHAR(255) DEFAULT NULL NULL;
ALTER TABLE setores ADD nu_cpf varchar(11) DEFAULT NULL NULL;

