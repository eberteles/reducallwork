ALTER TABLE  `usuarios` ADD  `ip_ult_acesso` VARCHAR( 20 ) NULL

CREATE TABLE `assinaturas` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`usuario_id` INT( 11 ) NOT NULL ,
`authentication_id` INT( 11 ) NOT NULL ,
`texto` TEXT NOT NULL ,
`assinatura` TEXT NOT NULL ,
`usuario_token` VARCHAR( 500 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`modified` DATETIME NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela com as Assinaturas Eletrôncas';

ALTER TABLE  `assinaturas` ADD INDEX (  `usuario_id` );

ALTER TABLE  `assinaturas` ADD FOREIGN KEY (  `usuario_id` ) REFERENCES `usuarios` (
`co_usuario`
);

ALTER TABLE  `log_usuarios` CHANGE  `nu_cpf`  `nu_cpf` VARCHAR( 14 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT  'CPF ou Matrícula';

ALTER TABLE  `usuarios` CHANGE  `nu_cpf`  `nu_cpf` VARCHAR( 14 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT  'CPF ou Matrícula';

ALTER TABLE  `notas_fiscais` ADD  `assinatura_id` INT( 11 ) NULL AFTER  `co_contrato`;
ALTER TABLE  `notas_fiscais` ADD INDEX (  `assinatura_id` );
ALTER TABLE  `notas_fiscais` ADD FOREIGN KEY (  `assinatura_id` ) REFERENCES `assinaturas` (
`id`
);
ALTER TABLE  `log_notas_fiscais` ADD  `assinatura_id` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `andamentos` ADD  `assinatura_id` INT( 11 ) NULL AFTER  `co_fase`;
ALTER TABLE  `andamentos` ADD INDEX (  `assinatura_id` );
ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `assinatura_id` ) REFERENCES `assinaturas` (
`id`
);

