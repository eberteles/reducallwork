CREATE TABLE `clausulas_contratuais` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
`nu_clausula` VARCHAR( 100 ) NOT NULL ,
`ds_clausula` VARCHAR( 500 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT = 'Tabela de Clausulas Contratuais';

CREATE TABLE `projetos` (
  co_projeto int not null auto_increment primary key,
  no_projeto varchar(30) not null,
  ds_projeto varchar(300) not null,
  ic_ativo int not null default 1
);

ALTER TABLE usuarios ADD COLUMN co_projeto INT NULL, ADD FOREIGN KEY (co_projeto) REFERENCES projetos(co_projeto);