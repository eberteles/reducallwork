DROP TRIGGER IF EXISTS `ADD_LOG_aditivos`;
DROP TRIGGER IF EXISTS `UPD_LOG_aditivos`;
DROP TRIGGER IF EXISTS `DEL_LOG_aditivos`;
DROP TRIGGER IF EXISTS `ADD_LOG_aditivos_backup`;
DROP TRIGGER IF EXISTS `UPD_LOG_aditivos_backup`;
DROP TRIGGER IF EXISTS `DEL_LOG_aditivos_backup`;
DROP TRIGGER IF EXISTS `ADD_LOG_alertas`;
DROP TRIGGER IF EXISTS `UPD_LOG_alertas`;
DROP TRIGGER IF EXISTS `DEL_LOG_alertas`;
DROP TRIGGER IF EXISTS `ADD_LOG_andamentos`;
DROP TRIGGER IF EXISTS `UPD_LOG_andamentos`;
DROP TRIGGER IF EXISTS `DEL_LOG_andamentos`;
DROP TRIGGER IF EXISTS `ADD_LOG_anexos`;
DROP TRIGGER IF EXISTS `UPD_LOG_anexos`;
DROP TRIGGER IF EXISTS `DEL_LOG_anexos`;
DROP TRIGGER IF EXISTS `ADD_LOG_anexos_indexacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_anexos_indexacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_anexos_indexacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_anexos_pastas`;
DROP TRIGGER IF EXISTS `UPD_LOG_anexos_pastas`;
DROP TRIGGER IF EXISTS `DEL_LOG_anexos_pastas`;
DROP TRIGGER IF EXISTS `ADD_LOG_apostilamentos`;
DROP TRIGGER IF EXISTS `UPD_LOG_apostilamentos`;
DROP TRIGGER IF EXISTS `DEL_LOG_apostilamentos`;
DROP TRIGGER IF EXISTS `ADD_LOG_aquisicoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_aquisicoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_aquisicoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_areas`;
DROP TRIGGER IF EXISTS `UPD_LOG_areas`;
DROP TRIGGER IF EXISTS `DEL_LOG_areas`;
DROP TRIGGER IF EXISTS `ADD_LOG_assinaturas`;
DROP TRIGGER IF EXISTS `UPD_LOG_assinaturas`;
DROP TRIGGER IF EXISTS `DEL_LOG_assinaturas`;
DROP TRIGGER IF EXISTS `ADD_LOG_assinaturas_login`;
DROP TRIGGER IF EXISTS `UPD_LOG_assinaturas_login`;
DROP TRIGGER IF EXISTS `DEL_LOG_assinaturas_login`;
DROP TRIGGER IF EXISTS `ADD_LOG_assinaturas_requisitante`;
DROP TRIGGER IF EXISTS `UPD_LOG_assinaturas_requisitante`;
DROP TRIGGER IF EXISTS `DEL_LOG_assinaturas_requisitante`;
DROP TRIGGER IF EXISTS `ADD_LOG_atas`;
DROP TRIGGER IF EXISTS `UPD_LOG_atas`;
DROP TRIGGER IF EXISTS `DEL_LOG_atas`;
DROP TRIGGER IF EXISTS `ADD_LOG_atas_categorias`;
DROP TRIGGER IF EXISTS `UPD_LOG_atas_categorias`;
DROP TRIGGER IF EXISTS `DEL_LOG_atas_categorias`;
DROP TRIGGER IF EXISTS `ADD_LOG_atas_itens`;
DROP TRIGGER IF EXISTS `UPD_LOG_atas_itens`;
DROP TRIGGER IF EXISTS `DEL_LOG_atas_itens`;
DROP TRIGGER IF EXISTS `ADD_LOG_atas_lotes`;
DROP TRIGGER IF EXISTS `UPD_LOG_atas_lotes`;
DROP TRIGGER IF EXISTS `DEL_LOG_atas_lotes`;
DROP TRIGGER IF EXISTS `ADD_LOG_atas_pedidos`;
DROP TRIGGER IF EXISTS `UPD_LOG_atas_pedidos`;
DROP TRIGGER IF EXISTS `DEL_LOG_atas_pedidos`;
DROP TRIGGER IF EXISTS `ADD_LOG_atividades`;
DROP TRIGGER IF EXISTS `UPD_LOG_atividades`;
DROP TRIGGER IF EXISTS `DEL_LOG_atividades`;
DROP TRIGGER IF EXISTS `ADD_LOG_bancos`;
DROP TRIGGER IF EXISTS `UPD_LOG_bancos`;
DROP TRIGGER IF EXISTS `DEL_LOG_bancos`;
DROP TRIGGER IF EXISTS `ADD_LOG_categorias`;
DROP TRIGGER IF EXISTS `UPD_LOG_categorias`;
DROP TRIGGER IF EXISTS `DEL_LOG_categorias`;
DROP TRIGGER IF EXISTS `ADD_LOG_clausulas_contratuais`;
DROP TRIGGER IF EXISTS `UPD_LOG_clausulas_contratuais`;
DROP TRIGGER IF EXISTS `DEL_LOG_clausulas_contratuais`;
DROP TRIGGER IF EXISTS `ADD_LOG_colunas_resultado_pesquisa`;
DROP TRIGGER IF EXISTS `UPD_LOG_colunas_resultado_pesquisa`;
DROP TRIGGER IF EXISTS `DEL_LOG_colunas_resultado_pesquisa`;
DROP TRIGGER IF EXISTS `ADD_LOG_confederacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_confederacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_confederacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_contratacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_contratacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_contratacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_contratos`;
DROP TRIGGER IF EXISTS `UPD_LOG_contratos`;
DROP TRIGGER IF EXISTS `DEL_LOG_contratos`;
DROP TRIGGER IF EXISTS `ADD_LOG_contratos_fiscais`;
DROP TRIGGER IF EXISTS `UPD_LOG_contratos_fiscais`;
DROP TRIGGER IF EXISTS `DEL_LOG_contratos_fiscais`;
DROP TRIGGER IF EXISTS `ADD_LOG_contratos_itens`;
DROP TRIGGER IF EXISTS `UPD_LOG_contratos_itens`;
DROP TRIGGER IF EXISTS `DEL_LOG_contratos_itens`;
DROP TRIGGER IF EXISTS `ADD_LOG_cronograma_financeiro_desembolso`;
DROP TRIGGER IF EXISTS `UPD_LOG_cronograma_financeiro_desembolso`;
DROP TRIGGER IF EXISTS `DEL_LOG_cronograma_financeiro_desembolso`;
DROP TRIGGER IF EXISTS `ADD_LOG_dashboards`;
DROP TRIGGER IF EXISTS `UPD_LOG_dashboards`;
DROP TRIGGER IF EXISTS `DEL_LOG_dashboards`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_cargos`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_cargos`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_cargos`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_categoria_passagem`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_categoria_passagem`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_categoria_passagem`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_despesas_viagem`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_despesas_viagem`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_despesas_viagem`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_funcao`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_funcao`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_funcao`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_cargos`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_cargos`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_cargos`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_categoria_passagem`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_categoria_passagem`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_categoria_passagem`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_despesas_viagem`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_despesas_viagem`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_despesas_viagem`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_funcao`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_funcao`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_funcao`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_meio_transporte`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_meio_transporte`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_meio_transporte`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_log_unidade_lotacao`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_log_unidade_lotacao`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_log_unidade_lotacao`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_meio_transporte`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_meio_transporte`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_meio_transporte`;
DROP TRIGGER IF EXISTS `ADD_LOG_dsp_unidade_lotacao`;
DROP TRIGGER IF EXISTS `UPD_LOG_dsp_unidade_lotacao`;
DROP TRIGGER IF EXISTS `DEL_LOG_dsp_unidade_lotacao`;
DROP TRIGGER IF EXISTS `ADD_LOG_elogios`;
DROP TRIGGER IF EXISTS `UPD_LOG_elogios`;
DROP TRIGGER IF EXISTS `DEL_LOG_elogios`;
DROP TRIGGER IF EXISTS `ADD_LOG_empenhos`;
DROP TRIGGER IF EXISTS `UPD_LOG_empenhos`;
DROP TRIGGER IF EXISTS `DEL_LOG_empenhos`;
DROP TRIGGER IF EXISTS `ADD_LOG_empenhos_anulacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_empenhos_anulacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_empenhos_anulacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_empenhos_backup`;
DROP TRIGGER IF EXISTS `UPD_LOG_empenhos_backup`;
DROP TRIGGER IF EXISTS `DEL_LOG_empenhos_backup`;
DROP TRIGGER IF EXISTS `ADD_LOG_entregas`;
DROP TRIGGER IF EXISTS `UPD_LOG_entregas`;
DROP TRIGGER IF EXISTS `DEL_LOG_entregas`;
DROP TRIGGER IF EXISTS `ADD_LOG_especialidade`;
DROP TRIGGER IF EXISTS `UPD_LOG_especialidade`;
DROP TRIGGER IF EXISTS `DEL_LOG_especialidade`;
DROP TRIGGER IF EXISTS `ADD_LOG_eventos`;
DROP TRIGGER IF EXISTS `UPD_LOG_eventos`;
DROP TRIGGER IF EXISTS `DEL_LOG_eventos`;
DROP TRIGGER IF EXISTS `ADD_LOG_eventos_competidores`;
DROP TRIGGER IF EXISTS `UPD_LOG_eventos_competidores`;
DROP TRIGGER IF EXISTS `DEL_LOG_eventos_competidores`;
DROP TRIGGER IF EXISTS `ADD_LOG_eventos_participantes`;
DROP TRIGGER IF EXISTS `UPD_LOG_eventos_participantes`;
DROP TRIGGER IF EXISTS `DEL_LOG_eventos_participantes`;
DROP TRIGGER IF EXISTS `ADD_LOG_eventos_resultados`;
DROP TRIGGER IF EXISTS `UPD_LOG_eventos_resultados`;
DROP TRIGGER IF EXISTS `DEL_LOG_eventos_resultados`;
DROP TRIGGER IF EXISTS `ADD_LOG_fases`;
DROP TRIGGER IF EXISTS `UPD_LOG_fases`;
DROP TRIGGER IF EXISTS `DEL_LOG_fases`;
DROP TRIGGER IF EXISTS `ADD_LOG_filtros`;
DROP TRIGGER IF EXISTS `UPD_LOG_filtros`;
DROP TRIGGER IF EXISTS `DEL_LOG_filtros`;
DROP TRIGGER IF EXISTS `ADD_LOG_fiscais`;
DROP TRIGGER IF EXISTS `UPD_LOG_fiscais`;
DROP TRIGGER IF EXISTS `DEL_LOG_fiscais`;
DROP TRIGGER IF EXISTS `ADD_LOG_fluxos`;
DROP TRIGGER IF EXISTS `UPD_LOG_fluxos`;
DROP TRIGGER IF EXISTS `DEL_LOG_fluxos`;
DROP TRIGGER IF EXISTS `ADD_LOG_fornecedores`;
DROP TRIGGER IF EXISTS `UPD_LOG_fornecedores`;
DROP TRIGGER IF EXISTS `DEL_LOG_fornecedores`;
DROP TRIGGER IF EXISTS `ADD_LOG_fornecedores_storage`;
DROP TRIGGER IF EXISTS `UPD_LOG_fornecedores_storage`;
DROP TRIGGER IF EXISTS `DEL_LOG_fornecedores_storage`;
DROP TRIGGER IF EXISTS `ADD_LOG_garantias`;
DROP TRIGGER IF EXISTS `UPD_LOG_garantias`;
DROP TRIGGER IF EXISTS `DEL_LOG_garantias`;
DROP TRIGGER IF EXISTS `ADD_LOG_garantias_suporte`;
DROP TRIGGER IF EXISTS `UPD_LOG_garantias_suporte`;
DROP TRIGGER IF EXISTS `DEL_LOG_garantias_suporte`;
DROP TRIGGER IF EXISTS `ADD_LOG_ged_financeiro`;
DROP TRIGGER IF EXISTS `UPD_LOG_ged_financeiro`;
DROP TRIGGER IF EXISTS `DEL_LOG_ged_financeiro`;
DROP TRIGGER IF EXISTS `ADD_LOG_graduacao`;
DROP TRIGGER IF EXISTS `UPD_LOG_graduacao`;
DROP TRIGGER IF EXISTS `DEL_LOG_graduacao`;
DROP TRIGGER IF EXISTS `ADD_LOG_historicos`;
DROP TRIGGER IF EXISTS `UPD_LOG_historicos`;
DROP TRIGGER IF EXISTS `DEL_LOG_historicos`;
DROP TRIGGER IF EXISTS `ADD_LOG_inspecao`;
DROP TRIGGER IF EXISTS `UPD_LOG_inspecao`;
DROP TRIGGER IF EXISTS `DEL_LOG_inspecao`;
DROP TRIGGER IF EXISTS `ADD_LOG_instituicoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_instituicoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_instituicoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_itens_aquisicoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_itens_aquisicoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_itens_aquisicoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_layouts`;
DROP TRIGGER IF EXISTS `UPD_LOG_layouts`;
DROP TRIGGER IF EXISTS `DEL_LOG_layouts`;
DROP TRIGGER IF EXISTS `ADD_LOG_liquidacao`;
DROP TRIGGER IF EXISTS `UPD_LOG_liquidacao`;
DROP TRIGGER IF EXISTS `DEL_LOG_liquidacao`;
DROP TRIGGER IF EXISTS `ADD_LOG_locais_especialidades`;
DROP TRIGGER IF EXISTS `UPD_LOG_locais_especialidades`;
DROP TRIGGER IF EXISTS `DEL_LOG_locais_especialidades`;
DROP TRIGGER IF EXISTS `ADD_LOG_logs`;
DROP TRIGGER IF EXISTS `UPD_LOG_logs`;
DROP TRIGGER IF EXISTS `DEL_LOG_logs`;
DROP TRIGGER IF EXISTS `ADD_LOG_meses`;
DROP TRIGGER IF EXISTS `UPD_LOG_meses`;
DROP TRIGGER IF EXISTS `DEL_LOG_meses`;
DROP TRIGGER IF EXISTS `ADD_LOG_modalidades`;
DROP TRIGGER IF EXISTS `UPD_LOG_modalidades`;
DROP TRIGGER IF EXISTS `DEL_LOG_modalidades`;
DROP TRIGGER IF EXISTS `ADD_LOG_modalidades_confederacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_modalidades_confederacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_modalidades_confederacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_movimentos`;
DROP TRIGGER IF EXISTS `UPD_LOG_movimentos`;
DROP TRIGGER IF EXISTS `DEL_LOG_movimentos`;
DROP TRIGGER IF EXISTS `ADD_LOG_municipios`;
DROP TRIGGER IF EXISTS `UPD_LOG_municipios`;
DROP TRIGGER IF EXISTS `DEL_LOG_municipios`;
DROP TRIGGER IF EXISTS `ADD_LOG_notas_empenhos`;
DROP TRIGGER IF EXISTS `UPD_LOG_notas_empenhos`;
DROP TRIGGER IF EXISTS `DEL_LOG_notas_empenhos`;
DROP TRIGGER IF EXISTS `ADD_LOG_notas_fiscais`;
DROP TRIGGER IF EXISTS `UPD_LOG_notas_fiscais`;
DROP TRIGGER IF EXISTS `DEL_LOG_notas_fiscais`;
DROP TRIGGER IF EXISTS `ADD_LOG_notas_pagamentos`;
DROP TRIGGER IF EXISTS `UPD_LOG_notas_pagamentos`;
DROP TRIGGER IF EXISTS `DEL_LOG_notas_pagamentos`;
DROP TRIGGER IF EXISTS `ADD_LOG_numeracao_contrato`;
DROP TRIGGER IF EXISTS `UPD_LOG_numeracao_contrato`;
DROP TRIGGER IF EXISTS `DEL_LOG_numeracao_contrato`;
DROP TRIGGER IF EXISTS `ADD_LOG_numeracao_empenho`;
DROP TRIGGER IF EXISTS `UPD_LOG_numeracao_empenho`;
DROP TRIGGER IF EXISTS `DEL_LOG_numeracao_empenho`;
DROP TRIGGER IF EXISTS `ADD_LOG_numeracao_pam`;
DROP TRIGGER IF EXISTS `UPD_LOG_numeracao_pam`;
DROP TRIGGER IF EXISTS `DEL_LOG_numeracao_pam`;
DROP TRIGGER IF EXISTS `ADD_LOG_numeracao_processo`;
DROP TRIGGER IF EXISTS `UPD_LOG_numeracao_processo`;
DROP TRIGGER IF EXISTS `DEL_LOG_numeracao_processo`;
DROP TRIGGER IF EXISTS `ADD_LOG_oficios`;
DROP TRIGGER IF EXISTS `UPD_LOG_oficios`;
DROP TRIGGER IF EXISTS `DEL_LOG_oficios`;
DROP TRIGGER IF EXISTS `ADD_LOG_ordem_fornecimento_servico`;
DROP TRIGGER IF EXISTS `UPD_LOG_ordem_fornecimento_servico`;
DROP TRIGGER IF EXISTS `DEL_LOG_ordem_fornecimento_servico`;
DROP TRIGGER IF EXISTS `ADD_LOG_pagamentos`;
DROP TRIGGER IF EXISTS `UPD_LOG_pagamentos`;
DROP TRIGGER IF EXISTS `DEL_LOG_pagamentos`;
DROP TRIGGER IF EXISTS `ADD_LOG_paises`;
DROP TRIGGER IF EXISTS `UPD_LOG_paises`;
DROP TRIGGER IF EXISTS `DEL_LOG_paises`;
DROP TRIGGER IF EXISTS `ADD_LOG_pedidos_itens`;
DROP TRIGGER IF EXISTS `UPD_LOG_pedidos_itens`;
DROP TRIGGER IF EXISTS `DEL_LOG_pedidos_itens`;
DROP TRIGGER IF EXISTS `ADD_LOG_penalidades`;
DROP TRIGGER IF EXISTS `UPD_LOG_penalidades`;
DROP TRIGGER IF EXISTS `DEL_LOG_penalidades`;
DROP TRIGGER IF EXISTS `ADD_LOG_pendencias`;
DROP TRIGGER IF EXISTS `UPD_LOG_pendencias`;
DROP TRIGGER IF EXISTS `DEL_LOG_pendencias`;
DROP TRIGGER IF EXISTS `ADD_LOG_pre_empenho`;
DROP TRIGGER IF EXISTS `UPD_LOG_pre_empenho`;
DROP TRIGGER IF EXISTS `DEL_LOG_pre_empenho`;
DROP TRIGGER IF EXISTS `ADD_LOG_privilegios`;
DROP TRIGGER IF EXISTS `UPD_LOG_privilegios`;
DROP TRIGGER IF EXISTS `DEL_LOG_privilegios`;
DROP TRIGGER IF EXISTS `ADD_LOG_produtos`;
DROP TRIGGER IF EXISTS `UPD_LOG_produtos`;
DROP TRIGGER IF EXISTS `DEL_LOG_produtos`;
DROP TRIGGER IF EXISTS `ADD_LOG_produtos_tipos`;
DROP TRIGGER IF EXISTS `UPD_LOG_produtos_tipos`;
DROP TRIGGER IF EXISTS `DEL_LOG_produtos_tipos`;
DROP TRIGGER IF EXISTS `ADD_LOG_projetos`;
DROP TRIGGER IF EXISTS `UPD_LOG_projetos`;
DROP TRIGGER IF EXISTS `DEL_LOG_projetos`;
DROP TRIGGER IF EXISTS `ADD_LOG_provas`;
DROP TRIGGER IF EXISTS `UPD_LOG_provas`;
DROP TRIGGER IF EXISTS `DEL_LOG_provas`;
DROP TRIGGER IF EXISTS `ADD_LOG_provas_categorias`;
DROP TRIGGER IF EXISTS `UPD_LOG_provas_categorias`;
DROP TRIGGER IF EXISTS `DEL_LOG_provas_categorias`;
DROP TRIGGER IF EXISTS `ADD_LOG_provas_classificacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_provas_classificacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_provas_classificacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_provas_subcategorias`;
DROP TRIGGER IF EXISTS `UPD_LOG_provas_subcategorias`;
DROP TRIGGER IF EXISTS `DEL_LOG_provas_subcategorias`;
DROP TRIGGER IF EXISTS `ADD_LOG_rotinas`;
DROP TRIGGER IF EXISTS `UPD_LOG_rotinas`;
DROP TRIGGER IF EXISTS `DEL_LOG_rotinas`;
DROP TRIGGER IF EXISTS `ADD_LOG_servicos`;
DROP TRIGGER IF EXISTS `UPD_LOG_servicos`;
DROP TRIGGER IF EXISTS `DEL_LOG_servicos`;
DROP TRIGGER IF EXISTS `ADD_LOG_setores`;
DROP TRIGGER IF EXISTS `UPD_LOG_setores`;
DROP TRIGGER IF EXISTS `DEL_LOG_setores`;
DROP TRIGGER IF EXISTS `ADD_LOG_siafi_arquivo`;
DROP TRIGGER IF EXISTS `UPD_LOG_siafi_arquivo`;
DROP TRIGGER IF EXISTS `DEL_LOG_siafi_arquivo`;
DROP TRIGGER IF EXISTS `ADD_LOG_siafi_ne`;
DROP TRIGGER IF EXISTS `UPD_LOG_siafi_ne`;
DROP TRIGGER IF EXISTS `DEL_LOG_siafi_ne`;
DROP TRIGGER IF EXISTS `ADD_LOG_siafi_nl`;
DROP TRIGGER IF EXISTS `UPD_LOG_siafi_nl`;
DROP TRIGGER IF EXISTS `DEL_LOG_siafi_nl`;
DROP TRIGGER IF EXISTS `ADD_LOG_situacoes`;
DROP TRIGGER IF EXISTS `UPD_LOG_situacoes`;
DROP TRIGGER IF EXISTS `DEL_LOG_situacoes`;
DROP TRIGGER IF EXISTS `ADD_LOG_subcategorias`;
DROP TRIGGER IF EXISTS `UPD_LOG_subcategorias`;
DROP TRIGGER IF EXISTS `DEL_LOG_subcategorias`;
DROP TRIGGER IF EXISTS `ADD_LOG_tipo_inspecao`;
DROP TRIGGER IF EXISTS `UPD_LOG_tipo_inspecao`;
DROP TRIGGER IF EXISTS `DEL_LOG_tipo_inspecao`;
DROP TRIGGER IF EXISTS `ADD_LOG_uasgs`;
DROP TRIGGER IF EXISTS `UPD_LOG_uasgs`;
DROP TRIGGER IF EXISTS `DEL_LOG_uasgs`;
DROP TRIGGER IF EXISTS `ADD_LOG_ufs`;
DROP TRIGGER IF EXISTS `UPD_LOG_ufs`;
DROP TRIGGER IF EXISTS `DEL_LOG_ufs`;
DROP TRIGGER IF EXISTS `ADD_LOG_usuarios`;
DROP TRIGGER IF EXISTS `UPD_LOG_usuarios`;
DROP TRIGGER IF EXISTS `DEL_LOG_usuarios`;

DROP TABLE IF EXISTS log_aditivos;
DROP TABLE IF EXISTS log_aditivos_backup;
DROP TABLE IF EXISTS log_alertas;
DROP TABLE IF EXISTS log_andamentos;
DROP TABLE IF EXISTS log_anexos;
DROP TABLE IF EXISTS log_anexos_indexacoes;
DROP TABLE IF EXISTS log_anexos_pastas;
DROP TABLE IF EXISTS log_apostilamentos;
DROP TABLE IF EXISTS log_aquisicoes;
DROP TABLE IF EXISTS log_areas;
DROP TABLE IF EXISTS log_assinaturas;
DROP TABLE IF EXISTS log_assinaturas_login;
DROP TABLE IF EXISTS log_assinaturas_requisitante;
DROP TABLE IF EXISTS log_atas;
DROP TABLE IF EXISTS log_atas_categorias;
DROP TABLE IF EXISTS log_atas_itens;
DROP TABLE IF EXISTS log_atas_lotes;
DROP TABLE IF EXISTS log_atas_pedidos;
DROP TABLE IF EXISTS log_atividades;
DROP TABLE IF EXISTS log_bancos;
DROP TABLE IF EXISTS log_categorias;
DROP TABLE IF EXISTS log_clausulas_contratuais;
DROP TABLE IF EXISTS log_colunas_resultado_pesquisa;
DROP TABLE IF EXISTS log_confederacoes;
DROP TABLE IF EXISTS log_contratacoes;
DROP TABLE IF EXISTS log_contratos;
DROP TABLE IF EXISTS log_contratos_fiscais;
DROP TABLE IF EXISTS log_contratos_itens;
DROP TABLE IF EXISTS log_cronograma_financeiro_desembolso;
DROP TABLE IF EXISTS log_dashboards;
DROP TABLE IF EXISTS log_dsp_cargos;
DROP TABLE IF EXISTS log_dsp_categoria_passagem;
DROP TABLE IF EXISTS log_dsp_despesas_viagem;
DROP TABLE IF EXISTS log_dsp_funcao;
DROP TABLE IF EXISTS log_dsp_log_cargos;
DROP TABLE IF EXISTS log_dsp_log_categoria_passagem;
DROP TABLE IF EXISTS log_dsp_log_despesas_viagem;
DROP TABLE IF EXISTS log_dsp_log_funcao;
DROP TABLE IF EXISTS log_dsp_log_meio_transporte;
DROP TABLE IF EXISTS log_dsp_log_unidade_lotacao;
DROP TABLE IF EXISTS log_dsp_meio_transporte;
DROP TABLE IF EXISTS log_dsp_unidade_lotacao;
DROP TABLE IF EXISTS log_elogios;
DROP TABLE IF EXISTS log_empenhos;
DROP TABLE IF EXISTS log_empenhos_anulacoes;
DROP TABLE IF EXISTS log_empenhos_backup;
DROP TABLE IF EXISTS log_entregas;
DROP TABLE IF EXISTS log_especialidade;
DROP TABLE IF EXISTS log_eventos;
DROP TABLE IF EXISTS log_eventos_competidores;
DROP TABLE IF EXISTS log_eventos_participantes;
DROP TABLE IF EXISTS log_eventos_resultados;
DROP TABLE IF EXISTS log_fases;
DROP TABLE IF EXISTS log_filtros;
DROP TABLE IF EXISTS log_fiscais;
DROP TABLE IF EXISTS log_fluxos;
DROP TABLE IF EXISTS log_fornecedores;
DROP TABLE IF EXISTS log_fornecedores_storage;
DROP TABLE IF EXISTS log_garantias;
DROP TABLE IF EXISTS log_garantias_suporte;
DROP TABLE IF EXISTS log_ged_financeiro;
DROP TABLE IF EXISTS log_graduacao;
DROP TABLE IF EXISTS log_historicos;
DROP TABLE IF EXISTS log_inspecao;
DROP TABLE IF EXISTS log_instituicoes;
DROP TABLE IF EXISTS log_itens_aquisicoes;
DROP TABLE IF EXISTS log_layouts;
DROP TABLE IF EXISTS log_liquidacao;
DROP TABLE IF EXISTS log_locais_especialidades;
DROP TABLE IF EXISTS log_logs;
DROP TABLE IF EXISTS log_meses;
DROP TABLE IF EXISTS log_modalidades;
DROP TABLE IF EXISTS log_modalidades_confederacoes;
DROP TABLE IF EXISTS log_movimentos;
DROP TABLE IF EXISTS log_municipios;
DROP TABLE IF EXISTS log_notas_empenhos;
DROP TABLE IF EXISTS log_notas_fiscais;
DROP TABLE IF EXISTS log_notas_pagamentos;
DROP TABLE IF EXISTS log_numeracao_contrato;
DROP TABLE IF EXISTS log_numeracao_empenho;
DROP TABLE IF EXISTS log_numeracao_pam;
DROP TABLE IF EXISTS log_numeracao_processo;
DROP TABLE IF EXISTS log_oficios;
DROP TABLE IF EXISTS log_ordem_fornecimento_servico;
DROP TABLE IF EXISTS log_pagamentos;
DROP TABLE IF EXISTS log_paises;
DROP TABLE IF EXISTS log_pedidos_itens;
DROP TABLE IF EXISTS log_penalidades;
DROP TABLE IF EXISTS log_pendencias;
DROP TABLE IF EXISTS log_pre_empenho;
DROP TABLE IF EXISTS log_privilegios;
DROP TABLE IF EXISTS log_produtos;
DROP TABLE IF EXISTS log_produtos_tipos;
DROP TABLE IF EXISTS log_projetos;
DROP TABLE IF EXISTS log_provas;
DROP TABLE IF EXISTS log_provas_categorias;
DROP TABLE IF EXISTS log_provas_classificacoes;
DROP TABLE IF EXISTS log_provas_subcategorias;
DROP TABLE IF EXISTS log_rotinas;
DROP TABLE IF EXISTS log_servicos;
DROP TABLE IF EXISTS log_setores;
DROP TABLE IF EXISTS log_siafi_arquivo;
DROP TABLE IF EXISTS log_siafi_ne;
DROP TABLE IF EXISTS log_siafi_nl;
DROP TABLE IF EXISTS log_situacoes;
DROP TABLE IF EXISTS log_subcategorias;
DROP TABLE IF EXISTS log_tipo_inspecao;
DROP TABLE IF EXISTS log_uasgs;
DROP TABLE IF EXISTS log_ufs;
DROP TABLE IF EXISTS log_usuarios;

CREATE TABLE `log_aditivos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_aditivo int not null ,
	 co_contrato int not null ,
	 no_aditivo varchar(255) null ,
	 tp_aditivo char(1) not null ,
	 dt_aditivo date null ,
	 vl_aditivo decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_assinatura date null ,
	 dt_prazo date null ,
	 ds_aditivo varchar(1500) not null ,
	 co_usuario int not null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(20) null ,
	 dt_vigencia_de date null ,
	 dt_fim_vigencia date null ,
	 data datetime null
);

CREATE TABLE `log_aditivos_backup` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_aditivo int not null ,
	 co_contrato int not null ,
	 no_aditivo varchar(255) null ,
	 tp_aditivo char(1) not null ,
	 dt_aditivo date not null ,
	 vl_aditivo decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_assinatura date null ,
	 dt_prazo date null ,
	 ds_aditivo varchar(1500) not null ,
	 co_usuario int not null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(20) null ,
	 dt_vigencia_de date null ,
	 data datetime null
);

CREATE TABLE `log_alertas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_alerta int not null ,
	 ds_email_from varchar(100) null ,
	 ds_smtp_host varchar(40) null ,
	 ds_smtp_port varchar(10) null ,
	 ds_smtp_user varchar(100) null ,
	 ds_smtp_pass varchar(20) null ,
	 ck_aviso30 char(1) null ,
	 ck_aviso_uni30 char(1) null ,
	 ck_aviso_fis30 char(1) null ,
	 ck_aviso_for30 char(1) null ,
	 ck_aviso60 char(1) null ,
	 ck_aviso_uni60 char(1) null ,
	 ck_aviso_fis60 char(1) null ,
	 ck_aviso_for60 char(1) null ,
	 ck_aviso90 char(1) null ,
	 ck_aviso_uni90 char(1) null ,
	 ck_aviso_fis90 char(1) null ,
	 ck_aviso_for90 char(1) null ,
	 ck_aviso120 char(1) null ,
	 ck_aviso_uni120 char(1) null ,
	 ck_aviso_fis120 char(1) null ,
	 ck_aviso_for120 char(1) null ,
	 ck_aviso_sup30 char(1) null ,
	 ck_aviso_muda_fase int null ,
	 data datetime null
);

CREATE TABLE `log_andamentos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_andamento int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 co_fornecedor int null ,
	 nu_sequencia int null ,
	 co_setor int null ,
	 co_fase int null ,
	 assinatura_id int null ,
	 dt_andamento datetime not null ,
	 dt_fim datetime null ,
	 ds_andamento varchar(255) null ,
	 ds_justificativa varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_anexos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_anexo int not null ,
	 co_anexo_pasta int null ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_atividade int null ,
	 co_contrato_fiscal int null ,
	 co_aditivo int null ,
	 co_apostilamento int null ,
	 co_garantia int null ,
	 co_garantia_suporte int null ,
	 co_produto int null ,
	 co_oficio int null ,
	 co_empenho int null ,
	 co_penalidade int null ,
	 co_nota int null ,
	 co_pendencia int null ,
	 co_pagamento int null ,
	 co_historico int null ,
	 co_evento int null ,
	 co_ordem_fornecimento_servico int null ,
	 dt_anexo date null ,
	 tp_documento int not null ,
	 ds_anexo varchar(255) not null ,
	 ds_extensao varchar(5) not null ,
	 conteudo mediumblob not null ,
	 ds_observacao varchar(255) null ,
	 ic_atesto int not null ,
	 assinatura text null ,
	 chave text null ,
	 co_usuario_assinatura int null ,
	 ds_usuario_token text null ,
	 ic_indexado int null ,
	 data datetime null
);

CREATE TABLE `log_anexos_indexacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_anexo_indexacao int not null ,
	 co_anexo int not null ,
	 nu_pagina int not null ,
	 ds_conteudo text not null ,
	 data datetime null
);

CREATE TABLE `log_anexos_pastas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_anexo_pasta int not null ,
	 parent_id int null ,
	 co_ata int null ,
	 co_contrato int null ,
	 ds_anexo_pasta varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_apostilamentos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_apostilamento int not null ,
	 co_contrato int not null ,
	 no_apostilamento varchar(255) null ,
	 tp_apostilamento char(1) not null ,
	 dt_apostilamento date not null ,
	 vl_apostilamento decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_prazo date null ,
	 ds_apostilamento varchar(1500) not null ,
	 co_usuario int not null ,
	 ds_fundamento_legal varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_aquisicoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_aquisicao int not null ,
	 co_item_aquisicao int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 ds_aquisicao varchar(500) not null ,
	 dt_aquisicao date null ,
	 vl_aquisicao decimal not null ,
	 qt_aquisicao int not null ,
	 data datetime null
);

CREATE TABLE `log_areas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_area int not null ,
	 parent_id int null ,
	 ds_area varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_assinaturas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 usuario_id int not null ,
	 authentication_id int not null ,
	 texto text not null ,
	 assinatura text not null ,
	 usuario_token varchar(500) not null ,
	 created datetime not null ,
	 modified datetime not null ,
	 data datetime null
);

CREATE TABLE `log_assinaturas_login` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 cod_usuario int null ,
	 cod_contrato int null ,
	 data datetime null
);

CREATE TABLE `log_assinaturas_requisitante` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 nome_completo varchar(75) not null ,
	 cargo varchar(45) null ,
	 data_assinatura datetime null ,
	 referencia text null ,
	 co_contrato int not null ,
	 data datetime null
);

CREATE TABLE `log_atas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ata int not null ,
	 co_categoria_ata int not null ,
	 co_fornecedor int not null ,
	 nu_ata varchar(9) not null ,
	 nu_processo varchar(20) not null ,
	 ds_publicacao varchar(1000) null ,
	 tp_ata char(1) not null ,
	 tp_pregao char(1) null ,
	 ds_especificacao varchar(1000) null ,
	 ds_justificativa varchar(1000) null ,
	 dt_vigencia_inicio date not null ,
	 dt_vigencia_fim date not null ,
	 vl_ata decimal null ,
	 vl_mensal decimal null ,
	 vl_anual decimal not null ,
	 tp_status char(1) not null ,
	 dt_cadastro date not null ,
	 co_usuario int not null ,
	 data datetime null
);

CREATE TABLE `log_atas_categorias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ata_categoria int not null ,
	 ds_ata_categoria varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_atas_itens` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ata_item int not null ,
	 co_ata int not null ,
	 co_ata_lote int null ,
	 nu_item int not null ,
	 ds_marca varchar(255) null ,
	 ds_descricao varchar(500) not null ,
	 ds_unidade varchar(20) not null ,
	 nu_quantidade int not null ,
	 qt_utilizado int not null ,
	 vl_unitario decimal not null ,
	 data datetime null
);

CREATE TABLE `log_atas_lotes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ata_lote int not null ,
	 co_ata int not null ,
	 nu_lote varchar(10) not null ,
	 ds_objeto varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_atas_pedidos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ata_pedido int not null ,
	 co_ata int not null ,
	 co_contrato int null ,
	 ds_pedido varchar(255) not null ,
	 vl_pedido decimal not null ,
	 tp_pedido char(1) not null ,
	 dt_pedido date not null ,
	 co_usuario int not null ,
	 data datetime null
);

CREATE TABLE `log_atividades` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_atividade int not null ,
	 co_contrato int not null ,
	 parent_id int null ,
	 co_usuario int not null ,
	 co_responsavel int not null ,
	 ds_atividade varchar(255) not null ,
	 tp_andamento varchar(1) not null ,
	 dt_ini_planejado date not null ,
	 dt_fim_planejado date not null ,
	 dt_ini_execucao date null ,
	 dt_fim_execucao date null ,
	 pc_executado int not null ,
	 co_tipo_inspecao int null ,
	 ds_assunto_email varchar(50) null ,
	 atividade_atestada int not null ,
	 nome varchar(255) not null ,
	 periodicidade varchar(255) null ,
	 dia_execucao varchar(255) null ,
	 funcao_responsavel varchar(255) not null ,
	 pendencia varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_bancos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_banco int not null ,
	 nu_banco varchar(6) not null ,
	 ds_banco varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_categorias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_categoria int not null ,
	 no_categoria varchar(100) not null ,
	 co_usuario int null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_clausulas_contratuais` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_contrato int not null ,
	 co_usuario int not null ,
	 nu_clausula varchar(100) not null ,
	 ds_clausula varchar(500) not null ,
	 created datetime not null ,
	 updated datetime not null ,
	 data datetime null
);

CREATE TABLE `log_colunas_resultado_pesquisa` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_coluna_resultado int not null ,
	 ic_modulo char(3) not null ,
	 ds_nome varchar(50) not null ,
	 ds_dominio varchar(20) not null ,
	 ds_coluna_dominio varchar(20) not null ,
	 ds_funcao_habilita varchar(20) null ,
	 ds_funcao_exibe varchar(20) null ,
	 data datetime null
);

CREATE TABLE `log_confederacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_confederacao int not null ,
	 parent_id int null ,
	 ds_confederacao varchar(100) not null ,
	 ic_cob varchar(1) null ,
	 ic_cpb varchar(1) null ,
	 no_contato varchar(255) null ,
	 ds_email varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 nu_celular varchar(15) null ,
	 ds_endereco varchar(255) null ,
	 ds_complemento varchar(100) null ,
	 ds_bairro varchar(255) null ,
	 sg_uf varchar(2) null ,
	 co_municipio int null ,
	 nu_cep varchar(10) null ,
	 co_usuario int null ,
	 data datetime null
);

CREATE TABLE `log_contratacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_contratacao int not null ,
	 ds_contratacao varchar(100) not null ,
	 co_siafi int null ,
	 co_siasg varchar(2) null ,
	 at_contratacao int null ,
	 data datetime null
);

CREATE TABLE `log_contratos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_contrato int not null ,
	 co_instituicao int null ,
	 co_situacao int null ,
	 co_modalidade int null ,
	 co_servico int null ,
	 co_categoria int null ,
	 co_subcategoria int null ,
	 co_fornecedor int null ,
	 co_contratante int null ,
	 co_executante int null ,
	 co_contratacao int null ,
	 co_fiscal_atual int null ,
	 co_gestor_atual int null ,
	 co_confederacao int null ,
	 co_federacao int null ,
	 co_classificacao int null ,
	 co_evento int null ,
	 co_prova int null ,
	 nu_sequencia int null ,
	 ic_ativo char(1) null ,
	 ic_tipo_contrato char(1) not null ,
	 ds_motivo text null ,
	 co_fase int null ,
	 dt_fase date null ,
	 co_setor int null ,
	 is_desvio_fluxo char(1) null ,
	 nu_pam varchar(30) null ,
	 nu_contrato varchar(40) null ,
	 nu_processo varchar(20) null ,
	 tp_aquisicao varchar(2) null ,
	 ic_ata_vigente varchar(1) null ,
	 ic_ata_orgao varchar(1) null ,
	 ic_dispensa_licitacao varchar(1) null ,
	 ds_objeto varchar(1500) null ,
	 nu_licitacao varchar(9) null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(500) null ,
	 dt_autorizacao_pam date null ,
	 dt_assinatura date null ,
	 dt_ini_vigencia date null ,
	 dt_fim_vigencia date null ,
	 dt_fim_vigencia_inicio date null ,
	 dt_ini_processo date null ,
	 dt_fim_processo date null ,
	 st_repactuado varchar(1) null ,
	 vl_inicial double null ,
	 vl_mensal double null ,
	 tp_valor varchar(1) null ,
	 vl_servico double null ,
	 vl_material double null ,
	 vl_global double null ,
	 ds_observacao varchar(255) null ,
	 fg_financeiro varchar(1) null ,
	 nu_pendencias int null ,
	 dt_cadastro timestamp null ,
	 dt_cadastro_pam timestamp null ,
	 dt_cadastro_processo timestamp null ,
	 dt_tais date null ,
	 dt_prazo_processo date null ,
	 co_usuario int not null ,
	 co_ident_siasg varchar(20) null ,
	 nu_qtd_parcelas int null ,
	 co_solicitante int not null ,
	 co_responsavel int null ,
	 ds_requisitante varchar(100) null ,
	 pt_requisitante varchar(100) null ,
	 tl_requisitante varchar(15) null ,
	 dt_solicitacao date null ,
	 ds_assunto varchar(250) null ,
	 ds_objeto_plano varchar(500) null ,
	 ds_vantagem varchar(500) null ,
	 ds_criterio varchar(500) null ,
	 ds_justificativa varchar(500) null ,
	 ds_resultado varchar(500) null ,
	 ds_demanda varchar(500) null ,
	 ds_aproveitamento varchar(500) null ,
	 ds_gerenciador_ata varchar(50) null ,
	 ic_prod_presente varchar(1) null ,
	 ic_pesq_preco varchar(1) null ,
	 ic_inexigivel varchar(1) null ,
	 ic_serv_continuado varchar(1) null ,
	 ic_prod_parcelado varchar(1) null ,
	 co_assinatura_ordenador int null ,
	 co_assinatura_chefe int null ,
	 dt_assinatura_chefe datetime null ,
	 dt_assinatura_ordenador datetime null ,
	 co_contrato_pai int null ,
	 uasg varchar(6) null ,
	 ds_tipo_servico varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_contratos_fiscais` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_contrato_fiscal int not null ,
	 co_usuario int not null ,
	 co_contrato int not null ,
	 co_usuario_fiscal int null ,
	 dt_inicio date not null ,
	 dt_fim date null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_contratos_itens` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_contrato_item int not null ,
	 co_contrato int not null ,
	 co_fornecedor int null ,
	 nu_ordem int not null ,
	 nu_item_pregao int null ,
	 nu_unidade varchar(25) not null ,
	 qt_solicitada int not null ,
	 vl_unitario double null ,
	 ds_demanda varchar(250) not null ,
	 co_siasg int null ,
	 qt_consumo int null ,
	 co_cat varchar(15) null ,
	 data datetime null
);

CREATE TABLE `log_cronograma_financeiro_desembolso` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_cronograma_financeiro_desembolso int not null ,
	 co_contrato int null ,
	 descricao varchar(200) null ,
	 mes int null ,
	 dt_inicio date null ,
	 dt_data_final date null ,
	 percentual float null ,
	 ds_valor decimal null ,
	 data datetime null
);

CREATE TABLE `log_dashboards` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_dashboard int not null ,
	 co_usuario int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_cargos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_cargo int not null ,
	 nome_cargo varchar(50) null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_categoria_passagem` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_categoria_passagem int not null ,
	 nome_categoria_passagem varchar(50) null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_despesas_viagem` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_despesas_viagem int not null ,
	 numero_processo varchar(20) not null ,
	 motivo_viagem varchar(255) not null ,
	 cod_unidade_lotacao int not null ,
	 nome_funcionario varchar(50) not null ,
	 matricula_funcionario varchar(15) not null ,
	 cod_cargo int not null ,
	 cod_funcao int not null ,
	 origem varchar(50) not null ,
	 uf_origem varchar(2) not null ,
	 destino varchar(50) not null ,
	 uf_destino varchar(2) not null ,
	 dt_inicio date not null ,
	 dt_final date not null ,
	 cod_meio_transporte int not null ,
	 cod_categoria_passagem int not null ,
	 valor_passagem double not null ,
	 numero_diarias int not null ,
	 valor_total_diarias double not null ,
	 valor_total_viagem double null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_funcao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_funcao int not null ,
	 nome_funcao varchar(50) null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_meio_transporte` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_meio_transporte int not null ,
	 nome_meio_transporte varchar(50) null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_dsp_unidade_lotacao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_unidade int not null ,
	 nome_unidade varchar(50) not null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_elogios` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_elogio int not null ,
	 co_fornecedor int not null ,
	 co_contrato int not null ,
	 nu_elogio char(8) not null ,
	 ds_elogio varchar(300) not null ,
	 data datetime null
);

CREATE TABLE `log_empenhos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_empenho int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_empenho varchar(12) null ,
	 tp_empenho varchar(1) not null ,
	 ds_empenho varchar(1500) not null ,
	 co_plano_interno varchar(20) null ,
	 vl_empenho double null ,
	 dt_empenho date null ,
	 nu_ptres varchar(20) null ,
	 sub_nu_ptres int null ,
	 nu_evento_contabil int null ,
	 nu_esfera_orcamentaria int null ,
	 ds_fonte_recurso varchar(100) null ,
	 nu_natureza_despesa varchar(10) null ,
	 ds_inciso varchar(2) null ,
	 ds_amparo_legal varchar(8) null ,
	 nu_ug_responsavel int null ,
	 nu_origem_material int null ,
	 uf_beneficiada varchar(2) null ,
	 nu_municipio_beneficiado int null ,
	 nu_contra_entrega int null ,
	 nu_lista varchar(12) null ,
	 tp_anulacao char(1) null ,
	 dt_anulacao date null ,
	 vl_anulacao decimal null ,
	 vl_original decimal null ,
	 empenho_reforco_originario varchar(20) null ,
	 empenho_anulacao_originario varchar(20) null ,
	 vl_restante double null ,
	 co_pre_empenho int null ,
	 nu_processo_pagamento varchar(20) null ,
	 data datetime null
);

CREATE TABLE `log_empenhos_anulacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_empenho_anulacao int not null ,
	 co_empenho int not null ,
	 tp_anulacao char(1) not null ,
	 dt_anulacao date not null ,
	 vl_anulacao decimal not null ,
	 nu_empenho varchar(20) null ,
	 data datetime null
);

CREATE TABLE `log_empenhos_backup` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_empenho int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_empenho varchar(12) not null ,
	 tp_empenho varchar(1) not null ,
	 ds_empenho varchar(1500) not null ,
	 co_plano_interno varchar(20) null ,
	 vl_empenho decimal not null ,
	 dt_empenho date null ,
	 nu_ptres varchar(20) null ,
	 sub_nu_ptres int null ,
	 nu_evento_contabil int null ,
	 nu_esfera_orcamentaria int null ,
	 ds_fonte_recurso varchar(100) null ,
	 nu_natureza_despesa varchar(10) null ,
	 ds_inciso varchar(2) null ,
	 ds_amparo_legal varchar(8) null ,
	 nu_ug_responsavel int null ,
	 nu_origem_material int null ,
	 uf_beneficiada varchar(2) null ,
	 nu_municipio_beneficiado int null ,
	 nu_contra_entrega int null ,
	 nu_lista varchar(12) null ,
	 tp_anulacao char(1) null ,
	 dt_anulacao date null ,
	 vl_anulacao decimal null ,
	 vl_original decimal null ,
	 empenho_reforco_originario varchar(20) null ,
	 empenho_anulacao_originario varchar(20) null ,
	 data datetime null
);

CREATE TABLE `log_entregas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_entrega int not null ,
	 co_contrato int not null ,
	 co_usuario int not null ,
	 dt_entrega date not null ,
	 dt_entrega_oficial date not null ,
	 pz_recebimento int not null ,
	 dt_recebimento date not null ,
	 data datetime null
);

CREATE TABLE `log_especialidade` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_especialidade int not null ,
	 ds_especialidade varchar(50) not null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_eventos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_evento int not null ,
	 co_confederacao int not null ,
	 co_usuario int not null ,
	 co_modalidade int not null ,
	 co_prova int not null ,
	 co_prova_categoria int null ,
	 co_prova_subcategoria int null ,
	 co_prova_classificacao int null ,
	 co_categoria int not null ,
	 nu_ano int not null ,
	 ds_evento varchar(500) not null ,
	 ic_evento char(1) not null ,
	 dt_inicio date not null ,
	 dt_fim date not null ,
	 ds_local varchar(500) not null ,
	 ic_sexo char(1) null ,
	 data datetime null
);

CREATE TABLE `log_eventos_competidores` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_evento_competidor int not null ,
	 co_evento int not null ,
	 sg_uf varchar(2) null ,
	 co_pais varchar(2) null ,
	 data datetime null
);

CREATE TABLE `log_eventos_participantes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_evento_participante int not null ,
	 co_evento int not null ,
	 sg_uf varchar(2) null ,
	 co_pais varchar(2) null ,
	 data datetime null
);

CREATE TABLE `log_eventos_resultados` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_evento_resultado int not null ,
	 co_evento int not null ,
	 co_fornecedor int not null ,
	 nu_classificacao int not null ,
	 data datetime null
);

CREATE TABLE `log_fases` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_fase int not null ,
	 ds_fase varchar(200) not null ,
	 mm_duracao_fase int null ,
	 co_usuario int null ,
	 data datetime null
);

CREATE TABLE `log_filtros` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_filtro int not null ,
	 no_filtro varchar(50) not null ,
	 tp_filtro varchar(20) not null ,
	 ds_filtro varchar(100) not null ,
	 no_coluna varchar(100) null ,
	 no_dominio varchar(50) null ,
	 ds_formula varchar(255) null ,
	 ds_condicao varchar(255) null ,
	 tb_join varchar(100) null ,
	 tp_join varchar(50) null ,
	 ds_group varchar(255) null ,
	 is_limpar int null ,
	 is_operador int null ,
	 data datetime null
);

CREATE TABLE `log_fiscais` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_fiscal int not null ,
	 co_setor int not null ,
	 nu_cpf_fiscal varchar(11) not null ,
	 no_fiscal varchar(100) not null ,
	 ds_email varchar(255) not null ,
	 nu_siape int not null ,
	 nu_portaria int null ,
	 nu_telefone varchar(10) not null ,
	 st_fiscal varchar(1) not null ,
	 ds_observacao varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_fluxos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 nu_sequencia int not null ,
	 tp_operador int null ,
	 co_fase int null ,
	 co_setor int null ,
	 nu_sequencia_um int null ,
	 nu_sequencia_dois int null ,
	 data datetime null
);

CREATE TABLE `log_fornecedores` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_fornecedor int not null ,
	 tp_fornecedor varchar(1) null ,
	 nu_cnpj varchar(14) not null ,
	 no_razao_social varchar(100) not null ,
	 no_nome_fantasia varchar(100) null ,
	 ds_endereco varchar(100) null ,
	 nu_endereco varchar(15) null ,
	 ds_bairro varchar(255) null ,
	 ds_complemento varchar(255) null ,
	 nu_cep varchar(10) null ,
	 co_municipio int null ,
	 ds_municipio varchar(255) null ,
	 sg_uf varchar(2) null ,
	 ds_email varchar(50) null ,
	 nu_telefone varchar(15) null ,
	 nu_fax varchar(15) null ,
	 no_responsavel varchar(100) null ,
	 nu_cpf_responsavel varchar(11) null ,
	 nu_rg_responsavel varchar(30) null ,
	 no_preposto varchar(100) null ,
	 co_banco int null ,
	 nu_agencia varchar(6) null ,
	 nu_conta varchar(15) null ,
	 dt_inclusao datetime null ,
	 co_area int null ,
	 co_sub_area int null ,
	 ds_area varchar(150) null ,
	 ds_subarea varchar(150) null ,
	 ds_observacao varchar(255) null ,
	 co_usuario int null ,
	 tp_sexo varchar(1) null ,
	 tp_raca int null ,
	 dt_nascimento date null ,
	 nu_identidade varchar(30) null ,
	 ds_orgao varchar(15) null ,
	 dt_expedicao date null ,
	 tp_graduacao int null ,
	 sg_uf_origem varchar(2) null ,
	 co_municipio_origem int null ,
	 ic_patrocinio varchar(1) null ,
	 ic_ativo int null ,
	 ds_senha varchar(32) null ,
	 indicador_valor varchar(15) null ,
	 indicador_importancia varchar(15) null ,
	 ocs_psa char(3) null ,
	 is_fornecedor_orgao char(1) null ,
	 bairro varchar(120) null ,
	 data datetime null
);

CREATE TABLE `log_fornecedores_storage` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 modalidade varchar(70) null ,
	 data datetime null
);

CREATE TABLE `log_garantias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_garantia int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 ds_modalidade varchar(100) not null ,
	 vl_garantia decimal null ,
	 dt_inicio date null ,
	 dt_fim date null ,
	 no_seguradora varchar(100) null ,
	 nu_apolice varchar(50) null ,
	 ds_endereco varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 ds_observacao varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_garantias_suporte` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_garantia_suporte int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_serie varchar(100) not null ,
	 dt_inicio date null ,
	 dt_fim date null ,
	 ds_observacao varchar(255) null ,
	 ds_garantia_suporte varchar(255) null ,
	 data datetime null
);

CREATE TABLE `log_ged_financeiro` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 nr_cgc_cpf varchar(15) not null ,
	 nm_fornecedor varchar(200) null ,
	 num_nf varchar(10) null ,
	 nr_serie varchar(10) null ,
	 dt_quitacao date null ,
	 dt_vencimento date null ,
	 tp_quitacao varchar(2) null ,
	 vl_nf decimal null ,
	 vl_parcela decimal null ,
	 data datetime null
);

CREATE TABLE `log_graduacao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_graduacao int not null ,
	 ds_graduacao varchar(50) not null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_historicos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_historico int not null ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_evento int null ,
	 co_situacao int null ,
	 co_usuario int null ,
	 no_assunto varchar(50) not null ,
	 ds_historico varchar(100) null ,
	 dt_historico datetime not null ,
	 ds_observacao varchar(500) not null ,
	 data datetime null
);

CREATE TABLE `log_inspecao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_inspecao int not null ,
	 ds_inspecao varchar(50) not null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_instituicoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_instituicao int not null ,
	 co_instituicao_pai int null ,
	 nu_instituicao int not null ,
	 ds_instituicao varchar(200) not null ,
	 co_usuario int not null ,
	 data datetime null
);

CREATE TABLE `log_itens_aquisicoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_item_aquisicao int not null ,
	 nu_item_aquisicao int not null ,
	 ds_item_aquisicao varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_layouts` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_registro int not null ,
	 co_layout varchar(5) not null ,
	 ds_campo varchar(40) not null ,
	 no_tam_campo int not null ,
	 no_ini_campo int not null ,
	 no_fim_campo int not null ,
	 data datetime null
);

CREATE TABLE `log_liquidacao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_liquidacao int not null ,
	 dt_liquidacao date null ,
	 dt_recebimento_sefin date null ,
	 dt_envio_sefin date null ,
	 co_nota int null ,
	 vl_liquidacao float null ,
	 vl_imposto int null ,
	 co_contrato int null ,
	 ds_liquidacao varchar(400) null ,
	 nu_liquidacao varchar(20) null ,
	 data datetime null
);

CREATE TABLE `log_locais_especialidades` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_locais_atendimento int not null ,
	 co_especialidade int not null ,
	 data datetime null
);

CREATE TABLE `log_logs` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_log int not null ,
	 dt_log datetime not null ,
	 co_usuario int not null ,
	 tp_acao char(1) not null ,
	 tp_modulo char(3) null ,
	 ds_registro varchar(100) null ,
	 nu_ip varchar(20) not null ,
	 data datetime null
);

CREATE TABLE `log_meses` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 nome varchar(20) not null ,
	 data datetime null
);

CREATE TABLE `log_modalidades` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_modalidade int not null ,
	 nu_modalidade int null ,
	 ds_modalidade varchar(100) not null ,
	 tp_modalidade int null ,
	 tp_prova int null ,
	 co_usuario int null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_modalidades_confederacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_modalidade_confederacao int not null ,
	 co_confederacao int not null ,
	 co_modalidade int not null ,
	 data datetime null
);

CREATE TABLE `log_movimentos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_movimento int not null ,
	 cs_sucesso char(1) not null ,
	 dt_inicio datetime not null ,
	 dt_fim datetime null ,
	 ds_nome_arquivo varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_municipios` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_municipio int not null ,
	 sg_uf varchar(2) not null ,
	 ds_municipio varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_notas_empenhos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_nota_empenho int not null ,
	 co_empenho int not null ,
	 co_nota int not null ,
	 co_usuario int null ,
	 dt_nota_empenho date not null ,
	 data datetime null
);

CREATE TABLE `log_notas_fiscais` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_nota int not null ,
	 co_contrato int not null ,
	 assinatura_id int null ,
	 nu_nota varchar(15) not null ,
	 nu_serie varchar(4) null ,
	 dt_recebimento date not null ,
	 dt_envio date null ,
	 vl_nota decimal not null ,
	 vl_glosa decimal null ,
	 ds_nota varchar(500) null ,
	 ic_atesto int null ,
	 dt_atesto date null ,
	 ds_atesto varchar(500) null ,
	 co_usuario int not null ,
	 dt_nota date null ,
	 dt_vencimento date null ,
	 data datetime null
);

CREATE TABLE `log_notas_pagamentos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_nota_pagamento int not null ,
	 co_pagamento int not null ,
	 co_nota int not null ,
	 co_usuario int null ,
	 data datetime null
);

CREATE TABLE `log_numeracao_contrato` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 aa_contrato year not null ,
	 ct_contrato int not null ,
	 data datetime null
);

CREATE TABLE `log_numeracao_empenho` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 aa_empenho year not null ,
	 ct_empenho int not null ,
	 data datetime null
);

CREATE TABLE `log_numeracao_pam` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 aa_pam year not null ,
	 ct_pam int not null ,
	 data datetime null
);

CREATE TABLE `log_numeracao_processo` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 aa_processo year not null ,
	 ct_processo int not null ,
	 data datetime null
);

CREATE TABLE `log_oficios` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_oficio int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_oficio varchar(100) not null ,
	 dt_recebimento date null ,
	 dt_resposta date null ,
	 pz_resposta int null ,
	 data datetime null
);

CREATE TABLE `log_ordem_fornecimento_servico` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_ordem_fornecimento_servico int not null ,
	 co_contrato int null ,
	 nu_ordem int null ,
	 dt_emissao date null ,
	 dt_inicial date null ,
	 dt_final date null ,
	 dc_descricao varchar(500) null ,
	 co_usuario int null ,
	 data datetime null
);

CREATE TABLE `log_pagamentos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_pagamento int not null ,
	 co_empenho int null ,
	 co_contrato int not null ,
	 co_atividade int null ,
	 nu_mes_pagamento varchar(2) not null ,
	 nu_ano_pagameto varchar(4) not null ,
	 dt_vencimento date null ,
	 dt_pagamento date null ,
	 dt_financeiro date null ,
	 dt_administrativo date null ,
	 nu_nota_fiscal varchar(20) null ,
	 nu_serie_nf varchar(3) null ,
	 vl_nota decimal null ,
	 vl_imposto decimal null ,
	 vl_liquido decimal null ,
	 vl_pago decimal null ,
	 vl_pagamento decimal not null ,
	 ds_observacao varchar(255) null ,
	 co_usuario int null ,
	 vl_orcamento double null ,
	 data datetime null
);

CREATE TABLE `log_paises` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_pais varchar(2) not null ,
	 no_pais varchar(64) not null ,
	 data datetime null
);

CREATE TABLE `log_pedidos_itens` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_pedido_item int not null ,
	 co_ata_pedido int not null ,
	 co_ata_item int not null ,
	 co_ata_lote int null ,
	 nu_item int not null ,
	 ds_marca varchar(255) null ,
	 ds_descricao varchar(500) not null ,
	 ds_unidade varchar(20) not null ,
	 qt_pedido int not null ,
	 vl_unitario decimal not null ,
	 data datetime null
);

CREATE TABLE `log_penalidades` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_penalidade int not null ,
	 co_contrato int not null ,
	 co_nota int null ,
	 dt_penalidade date not null ,
	 ds_observacao varchar(255) null ,
	 ic_situacao int not null ,
	 mt_penalidade varchar(255) null ,
	 co_usuario int not null ,
	 data datetime null
);

CREATE TABLE `log_pendencias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_pendencia int not null ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_situacao int null ,
	 ds_pendencia varchar(255) not null ,
	 dt_inicio date not null ,
	 dt_fim date null ,
	 st_pendencia varchar(1) not null ,
	 ds_observacao varchar(500) null ,
	 co_usuario int null ,
	 ds_impacto_pagamento int null ,
	 data datetime null
);

CREATE TABLE `log_pre_empenho` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_pre_empenho int not null ,
	 it_da_transacao varchar(8) null ,
	 gr_ug_gestao_an_numero_peuq varchar(23) null ,
	 it_da_emissao varchar(8) null ,
	 it_da_limite varchar(8) null ,
	 it_co_ug_favorecida int null ,
	 it_co_gestao_favorecida int null ,
	 it_tx_observacao varchar(255) null ,
	 gr_codigo_evento int null ,
	 it_in_esfera_orcamentaria int null ,
	 it_co_programa_trabalho_resumido int null ,
	 gr_fonte_recurso varchar(10) null ,
	 gr_natureza_despesa int null ,
	 it_co_ug_responsavel int null ,
	 it_co_plano_interno varchar(11) null ,
	 it_va_transacao decimal null ,
	 it_co_ug_doc_referencia int null ,
	 it_co_gestao_doc_referencia int null ,
	 it_me_lancamento int null ,
	 it_in_saldo int null ,
	 it_in_cancelamento_pe int null ,
	 it_sq_pe_original int null ,
	 it_sq_pe_cancelamento int null ,
	 it_qt_lancamento int null ,
	 it_op_cambial int null ,
	 co_contrato int null ,
	 nu_pre_empenho varchar(25) null ,
	 ds_pre_empenho varchar(255) null ,
	 vl_pre_empenho double null ,
	 ds_fonte_recurso varchar(80) null ,
	 dt_pre_empenho date null ,
	 nu_natureza_despesa int null ,
	 ic_ativo int null ,
	 data datetime null
);

CREATE TABLE `log_privilegios` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_privilegio int not null ,
	 sg_privilegio varchar(15) not null ,
	 ds_privilegio varchar(50) not null ,
	 data datetime null
);

CREATE TABLE `log_produtos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_produto int not null ,
	 co_contrato int not null ,
	 co_produto_tipo int not null ,
	 co_usuario int null ,
	 ds_produto varchar(100) not null ,
	 nu_serie varchar(100) not null ,
	 ds_garantia varchar(100) not null ,
	 ds_fabricante varchar(100) not null ,
	 produto_recebido int not null ,
	 data datetime null
);

CREATE TABLE `log_produtos_tipos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_produto_tipo int not null ,
	 co_usuario int null ,
	 ds_tipo varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_projetos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_projeto int not null ,
	 no_projeto varchar(30) not null ,
	 ds_projeto varchar(300) not null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_provas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_prova int not null ,
	 co_modalidade int not null ,
	 ds_prova varchar(500) not null ,
	 is_sexo char(1) null ,
	 data datetime null
);

CREATE TABLE `log_provas_categorias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_prova_categoria int not null ,
	 co_prova int not null ,
	 ds_prova_categoria varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_provas_classificacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_prova_classificacao int not null ,
	 co_prova int not null ,
	 ds_prova_classificacao varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_provas_subcategorias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_prova_subcategoria int not null ,
	 co_prova int not null ,
	 ds_prova_subcategoria varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_rotinas` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_rotina int not null ,
	 tp_rotina varchar(2) not null ,
	 dt_inicio datetime not null ,
	 dt_fim datetime null ,
	 ds_resultado varchar(500) null ,
	 data datetime null
);

CREATE TABLE `log_servicos` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_servico int not null ,
	 ds_servico varchar(255) not null ,
	 data datetime null
);

CREATE TABLE `log_setores` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_setor int not null ,
	 parent_id int null ,
	 ds_setor varchar(100) not null ,
	 ds_email varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 ic_setor varchar(1) null ,
	 nu_setor varchar(14) null ,
	 ic_cob varchar(1) null ,
	 ic_cpb varchar(1) null ,
	 no_contrato varchar(255) null ,
	 nu_celular varchar(15) null ,
	 ds_endereco varchar(255) null ,
	 ds_complemento varchar(100) null ,
	 ds_bairro varchar(255) null ,
	 sg_uf varchar(2) null ,
	 co_municipio int null ,
	 nu_cep varchar(10) null ,
	 co_usuario int null ,
	 ic_ativo int not null ,
	 no_responsavel varchar(255) null ,
	 nu_cpf varchar(11) null ,
	 data datetime null
);

CREATE TABLE `log_siafi_arquivo` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 nome varchar(100) null ,
	 data datetime not null
);

CREATE TABLE `log_siafi_ne` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 id_arquivo int not null ,
	 it_co_usuario varchar(11) null ,
	 it_co_terminal_usuario varchar(8) null ,
	 it_da_transacao varchar(8) null ,
	 it_ho_transacao int null ,
	 it_co_ug_operador int null ,
	 gr_ug_gestao_an_numero_neuq_1 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_2 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_3 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_4 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_5 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_6 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_7 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_8 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_9 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_10 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_11 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_12 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_13 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_14 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_15 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_16 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_17 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_18 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_19 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_20 varchar(23) null ,
	 gr_an_nu_documento_referencia varchar(12) null ,
	 it_da_emissao varchar(8) null ,
	 it_in_favorecido int null ,
	 it_co_favorecido varchar(14) null ,
	 it_tx_observacao varchar(234) null ,
	 gr_codigo_evento int null ,
	 it_in_esfera_orcamentaria int null ,
	 it_co_programa_trabalho_resumido int null ,
	 gr_fonte_recurso varchar(10) null ,
	 gr_natureza_despesa int null ,
	 it_co_ug_responsavel int null ,
	 it_co_plano_interno varchar(11) null ,
	 it_va_transacao int null ,
	 it_in_modalidade_licitacao int null ,
	 it_in_empenho int null ,
	 it_tx_referencia_dispensa varchar(20) null ,
	 it_in_origem_material int null ,
	 it_nu_processo varchar(20) null ,
	 it_co_uf_beneficiada varchar(2) null ,
	 it_co_municipio_beneficiado int null ,
	 it_co_inciso varchar(2) null ,
	 it_tx_amparo_legal varchar(8) null ,
	 it_co_ug_doc_referencia int null ,
	 it_co_gestao_doc_referencia int null ,
	 it_in_emissao int null ,
	 it_me_lancamento int null ,
	 it_va_cronograma_1 int null ,
	 it_va_cronograma_2 int null ,
	 it_va_cronograma_3 int null ,
	 it_va_cronograma_4 int null ,
	 it_va_cronograma_5 int null ,
	 it_va_cronograma_6 int null ,
	 it_va_cronograma_7 int null ,
	 it_va_cronograma_8 int null ,
	 it_va_cronograma_9 int null ,
	 it_va_cronograma_10 int null ,
	 it_va_cronograma_11 int null ,
	 it_va_cronograma_12 int null ,
	 it_va_cronograma_13 int null ,
	 it_va_cronograma_14 int null ,
	 it_va_cronograma_15 int null ,
	 it_va_cronograma_16 int null ,
	 it_va_cronograma_17 int null ,
	 it_va_cronograma_18 int null ,
	 it_va_cronograma_19 int null ,
	 it_va_cronograma_20 int null ,
	 it_co_sistema_origem varchar(10) null ,
	 it_di_cronograma_1 int null ,
	 it_di_cronograma_2 int null ,
	 it_di_cronograma_3 int null ,
	 it_di_cronograma_4 int null ,
	 it_di_cronograma_5 int null ,
	 it_di_cronograma_6 int null ,
	 it_di_cronograma_7 int null ,
	 it_di_cronograma_8 int null ,
	 it_di_cronograma_9 int null ,
	 it_di_cronograma_10 int null ,
	 it_di_cronograma_11 int null ,
	 it_di_cronograma_12 int null ,
	 it_di_cronograma_13 int null ,
	 it_di_cronograma_14 int null ,
	 it_di_cronograma_15 int null ,
	 it_di_cronograma_16 int null ,
	 it_di_cronograma_17 int null ,
	 it_di_cronograma_18 int null ,
	 it_di_cronograma_19 int null ,
	 it_di_cronograma_20 int null ,
	 it_in_contra_entrega_ne int null ,
	 it_in_situacao_credor_sicaf int null ,
	 it_da_vencimento_1 varchar(8) null ,
	 it_da_vencimento_2 varchar(8) null ,
	 it_da_vencimento_3 varchar(8) null ,
	 it_da_vencimento_4 varchar(8) null ,
	 it_da_vencimento_5 varchar(8) null ,
	 it_da_vencimento_6 varchar(8) null ,
	 it_da_vencimento_7 varchar(8) null ,
	 it_da_vencimento_8 varchar(8) null ,
	 it_da_vencimento_9 varchar(8) null ,
	 it_da_vencimento_10 varchar(8) null ,
	 it_da_vencimento_11 varchar(8) null ,
	 it_da_vencimento_12 varchar(8) null ,
	 it_da_vencimento_13 varchar(8) null ,
	 it_da_vencimento_14 varchar(8) null ,
	 it_da_vencimento_15 varchar(8) null ,
	 it_da_vencimento_16 varchar(8) null ,
	 it_da_vencimento_17 varchar(8) null ,
	 it_da_vencimento_18 varchar(8) null ,
	 it_da_vencimento_19 varchar(8) null ,
	 it_da_vencimento_20 varchar(8) null ,
	 it_da_pagamento_1 varchar(8) null ,
	 it_da_pagamento_2 varchar(8) null ,
	 it_da_pagamento_3 varchar(8) null ,
	 it_da_pagamento_4 varchar(8) null ,
	 it_da_pagamento_5 varchar(8) null ,
	 it_da_pagamento_6 varchar(8) null ,
	 it_da_pagamento_7 varchar(8) null ,
	 it_da_pagamento_8 varchar(8) null ,
	 it_da_pagamento_9 varchar(8) null ,
	 it_da_pagamento_10 varchar(8) null ,
	 it_da_pagamento_11 varchar(8) null ,
	 it_da_pagamento_12 varchar(8) null ,
	 it_da_pagamento_13 varchar(8) null ,
	 it_da_pagamento_14 varchar(8) null ,
	 it_da_pagamento_15 varchar(8) null ,
	 it_da_pagamento_16 varchar(8) null ,
	 it_da_pagamento_17 varchar(8) null ,
	 it_da_pagamento_18 varchar(8) null ,
	 it_da_pagamento_19 varchar(8) null ,
	 it_da_pagamento_20 varchar(8) null ,
	 it_va_cronogramado_1 int null ,
	 it_va_cronogramado_2 int null ,
	 it_va_cronogramado_3 int null ,
	 it_va_cronogramado_4 int null ,
	 it_va_cronogramado_5 int null ,
	 it_va_cronogramado_6 int null ,
	 it_va_cronogramado_7 int null ,
	 it_va_cronogramado_8 int null ,
	 it_va_cronogramado_9 int null ,
	 it_va_cronogramado_10 int null ,
	 it_va_cronogramado_11 int null ,
	 it_va_cronogramado_12 int null ,
	 it_va_cronogramado_13 int null ,
	 it_va_cronogramado_14 int null ,
	 it_va_cronogramado_15 int null ,
	 it_va_cronogramado_16 int null ,
	 it_va_cronogramado_17 int null ,
	 it_va_cronogramado_18 int null ,
	 it_va_cronogramado_19 int null ,
	 it_va_cronogramado_20 int null ,
	 it_qt_lancamento int null ,
	 it_co_msg_documento int null ,
	 it_nu_precatorio varchar(20) null ,
	 it_in_pagamento_precatorio varchar(1) null ,
	 it_nu_original varchar(20) null ,
	 it_da_atualizacao varchar(8) null ,
	 it_in_atualizacao int null ,
	 it_nu_lista_1 varchar(12) null ,
	 it_nu_lista_2 varchar(12) null ,
	 it_nu_lista_3 varchar(12) null ,
	 it_nu_lista_4 varchar(12) null ,
	 it_nu_lista_5 varchar(12) null ,
	 it_nu_lista_6 varchar(12) null ,
	 it_nu_lista_7 varchar(12) null ,
	 it_nu_lista_8 varchar(12) null ,
	 it_nu_lista_9 varchar(12) null ,
	 it_nu_lista_10 varchar(12) null ,
	 it_nu_lista_11 varchar(12) null ,
	 it_nu_lista_12 varchar(12) null ,
	 it_nu_lista_13 varchar(12) null ,
	 it_nu_lista_14 varchar(12) null ,
	 it_nu_lista_15 varchar(12) null ,
	 it_nu_lista_16 varchar(12) null ,
	 it_nu_lista_17 varchar(12) null ,
	 it_nu_lista_18 varchar(12) null ,
	 it_nu_lista_19 varchar(12) null ,
	 it_nu_lista_20 varchar(12) null ,
	 it_in_liquidacao int null ,
	 it_op_cambial int null ,
	 gr_an_numero_doc_transferencia_1 varchar(12) null ,
	 gr_an_numero_doc_transferencia_2 varchar(12) null ,
	 gr_an_numero_doc_transferencia_3 varchar(12) null ,
	 gr_an_numero_doc_transferencia_4 varchar(12) null ,
	 gr_an_numero_doc_transferencia_5 varchar(12) null ,
	 gr_an_numero_doc_transferencia_6 varchar(12) null ,
	 gr_an_numero_doc_transferencia_7 varchar(12) null ,
	 gr_an_numero_doc_transferencia_8 varchar(12) null ,
	 gr_an_numero_doc_transferencia_9 varchar(12) null ,
	 gr_an_numero_doc_transferencia_10 varchar(12) null ,
	 gr_an_numero_doc_transferencia_11 varchar(12) null ,
	 gr_an_numero_doc_transferencia_12 varchar(12) null ,
	 gr_an_numero_doc_transferencia_13 varchar(12) null ,
	 gr_an_numero_doc_transferencia_14 varchar(12) null ,
	 gr_an_numero_doc_transferencia_15 varchar(12) null ,
	 gr_an_numero_doc_transferencia_16 varchar(12) null ,
	 gr_an_numero_doc_transferencia_17 varchar(12) null ,
	 gr_an_numero_doc_transferencia_18 varchar(12) null ,
	 gr_an_numero_doc_transferencia_19 varchar(12) null ,
	 gr_an_numero_doc_transferencia_20 varchar(12) null ,
	 it_in_estorno_cancelamento int null ,
	 gr_conta_passivo varchar(9) null ,
	 it_co_conta_corrente_permanente_1 varchar(78) null ,
	 it_co_conta_corrente_permanente_2 varchar(78) null ,
	 it_co_conta_corrente_permanente_3 varchar(78) null ,
	 it_co_conta_corrente_permanente_4 varchar(78) null ,
	 it_co_conta_corrente_permanente_5 varchar(78) null ,
	 it_co_conta_corrente_permanente_6 varchar(78) null ,
	 it_co_conta_corrente_permanente_7 varchar(78) null ,
	 it_co_conta_corrente_permanente_8 varchar(78) null ,
	 it_co_conta_corrente_permanente_9 varchar(78) null ,
	 it_co_conta_corrente_permanente_10 varchar(78) null ,
	 it_co_conta_corrente_permanente_11 varchar(78) null ,
	 it_co_conta_corrente_permanente_12 varchar(78) null ,
	 it_co_conta_corrente_permanente_13 varchar(78) null ,
	 it_co_conta_corrente_permanente_14 varchar(78) null ,
	 it_co_conta_corrente_permanente_15 varchar(78) null ,
	 it_co_conta_corrente_permanente_16 varchar(78) null ,
	 it_co_conta_corrente_permanente_17 varchar(78) null ,
	 it_co_conta_corrente_permanente_18 varchar(78) null ,
	 it_co_conta_corrente_permanente_19 varchar(78) null ,
	 it_co_conta_corrente_permanente_20 varchar(78) null ,
	 it_va_conta_corrente_permanente_1 int null ,
	 it_va_conta_corrente_permanente_2 int null ,
	 it_va_conta_corrente_permanente_3 int null ,
	 it_va_conta_corrente_permanente_4 int null ,
	 it_va_conta_corrente_permanente_5 int null ,
	 it_va_conta_corrente_permanente_6 int null ,
	 it_va_conta_corrente_permanente_7 int null ,
	 it_va_conta_corrente_permanente_8 int null ,
	 it_va_conta_corrente_permanente_9 int null ,
	 it_va_conta_corrente_permanente_10 int null ,
	 it_va_conta_corrente_permanente_11 int null ,
	 it_va_conta_corrente_permanente_12 int null ,
	 it_va_conta_corrente_permanente_13 int null ,
	 it_va_conta_corrente_permanente_14 int null ,
	 it_va_conta_corrente_permanente_15 int null ,
	 it_va_conta_corrente_permanente_16 int null ,
	 it_va_conta_corrente_permanente_17 int null ,
	 it_va_conta_corrente_permanente_18 int null ,
	 it_va_conta_corrente_permanente_19 int null ,
	 it_va_conta_corrente_permanente_20 int null ,
	 it_co_conta_corrente_financeiro varchar(78) null ,
	 it_tp_transferencia_doc varchar(2) null ,
	 data datetime null
);

CREATE TABLE `log_siafi_nl` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 id_arquivo int not null ,
	 it_co_usuario varchar(11) null ,
	 it_co_terminal_usuario varchar(8) null ,
	 it_da_transacao varchar(8) null ,
	 it_ho_transacao int null ,
	 it_co_ug_operador int null ,
	 gr_ug_gestao_an_numero_nluq varchar(23) null ,
	 it_da_emissao varchar(8) null ,
	 it_da_valorizacao varchar(8) null ,
	 it_co_titulo_credito varchar(12) null ,
	 it_da_vencimento_titulo_credito varchar(8) null ,
	 it_op_cambial int null ,
	 it_in_inversao_saldo_doc int null ,
	 it_tx_observacao varchar(234) null ,
	 it_tx_observacao_documento_1 varchar(78) null ,
	 it_tx_observacao_documento_2 varchar(78) null ,
	 it_tx_observacao_documento_3 varchar(78) null ,
	 it_tx_observacao_documento_4 varchar(78) null ,
	 it_tx_observacao_documento_5 varchar(78) null ,
	 it_tx_observacao_documento_6 varchar(78) null ,
	 it_tx_observacao_documento_7 varchar(78) null ,
	 it_tx_observacao_documento_8 varchar(78) null ,
	 it_tx_observacao_documento_9 varchar(78) null ,
	 it_tx_observacao_documento_10 varchar(78) null ,
	 it_tx_observacao_documento_11 varchar(78) null ,
	 it_tx_observacao_documento_12 varchar(78) null ,
	 it_tx_observacao_documento_13 varchar(78) null ,
	 it_tx_observacao_documento_14 varchar(78) null ,
	 it_tx_observacao_documento_15 varchar(78) null ,
	 it_tx_observacao_documento_16 varchar(78) null ,
	 it_tx_observacao_documento_17 varchar(78) null ,
	 it_tx_observacao_documento_18 varchar(78) null ,
	 it_tx_observacao_documento_19 varchar(78) null ,
	 it_tx_observacao_documento_20 varchar(78) null ,
	 it_in_favorecido int null ,
	 it_co_favorecido varchar(14) null ,
	 gr_codigo_evento_1 int null ,
	 gr_codigo_evento_2 int null ,
	 gr_codigo_evento_3 int null ,
	 gr_codigo_evento_4 int null ,
	 gr_codigo_evento_5 int null ,
	 gr_codigo_evento_6 int null ,
	 gr_codigo_evento_7 int null ,
	 gr_codigo_evento_8 int null ,
	 gr_codigo_evento_9 int null ,
	 gr_codigo_evento_10 int null ,
	 gr_codigo_evento_11 int null ,
	 gr_codigo_evento_12 int null ,
	 gr_codigo_evento_13 int null ,
	 gr_codigo_evento_14 int null ,
	 gr_codigo_evento_15 int null ,
	 gr_codigo_evento_16 int null ,
	 gr_codigo_evento_17 int null ,
	 gr_codigo_evento_18 int null ,
	 gr_codigo_evento_19 int null ,
	 gr_codigo_evento_20 int null ,
	 it_co_inscricao1_1 varchar(14) null ,
	 it_co_inscricao1_2 varchar(14) null ,
	 it_co_inscricao1_3 varchar(14) null ,
	 it_co_inscricao1_4 varchar(14) null ,
	 it_co_inscricao1_5 varchar(14) null ,
	 it_co_inscricao1_6 varchar(14) null ,
	 it_co_inscricao1_7 varchar(14) null ,
	 it_co_inscricao1_8 varchar(14) null ,
	 it_co_inscricao1_9 varchar(14) null ,
	 it_co_inscricao1_10 varchar(14) null ,
	 it_co_inscricao1_11 varchar(14) null ,
	 it_co_inscricao1_12 varchar(14) null ,
	 it_co_inscricao1_13 varchar(14) null ,
	 it_co_inscricao1_14 varchar(14) null ,
	 it_co_inscricao1_15 varchar(14) null ,
	 it_co_inscricao1_16 varchar(14) null ,
	 it_co_inscricao1_17 varchar(14) null ,
	 it_co_inscricao1_18 varchar(14) null ,
	 it_co_inscricao1_19 varchar(14) null ,
	 it_co_inscricao1_20 varchar(14) null ,
	 it_co_inscricao2_1 varchar(14) null ,
	 it_co_inscricao2_2 varchar(14) null ,
	 it_co_inscricao2_3 varchar(14) null ,
	 it_co_inscricao2_4 varchar(14) null ,
	 it_co_inscricao2_5 varchar(14) null ,
	 it_co_inscricao2_6 varchar(14) null ,
	 it_co_inscricao2_7 varchar(14) null ,
	 it_co_inscricao2_8 varchar(14) null ,
	 it_co_inscricao2_9 varchar(14) null ,
	 it_co_inscricao2_10 varchar(14) null ,
	 it_co_inscricao2_11 varchar(14) null ,
	 it_co_inscricao2_12 varchar(14) null ,
	 it_co_inscricao2_13 varchar(14) null ,
	 it_co_inscricao2_14 varchar(14) null ,
	 it_co_inscricao2_15 varchar(14) null ,
	 it_co_inscricao2_16 varchar(14) null ,
	 it_co_inscricao2_17 varchar(14) null ,
	 it_co_inscricao2_18 varchar(14) null ,
	 it_co_inscricao2_19 varchar(14) null ,
	 it_co_inscricao2_20 varchar(14) null ,
	 gr_classificacao1_1 int null ,
	 gr_classificacao1_2 int null ,
	 gr_classificacao1_3 int null ,
	 gr_classificacao1_4 int null ,
	 gr_classificacao1_5 int null ,
	 gr_classificacao1_6 int null ,
	 gr_classificacao1_7 int null ,
	 gr_classificacao1_8 int null ,
	 gr_classificacao1_9 int null ,
	 gr_classificacao1_10 int null ,
	 gr_classificacao1_11 int null ,
	 gr_classificacao1_12 int null ,
	 gr_classificacao1_13 int null ,
	 gr_classificacao1_14 int null ,
	 gr_classificacao1_15 int null ,
	 gr_classificacao1_16 int null ,
	 gr_classificacao1_17 int null ,
	 gr_classificacao1_18 int null ,
	 gr_classificacao1_19 int null ,
	 gr_classificacao1_20 int null ,
	 gr_classificacao2_1 int null ,
	 gr_classificacao2_2 int null ,
	 gr_classificacao2_3 int null ,
	 gr_classificacao2_4 int null ,
	 gr_classificacao2_5 int null ,
	 gr_classificacao2_6 int null ,
	 gr_classificacao2_7 int null ,
	 gr_classificacao2_8 int null ,
	 gr_classificacao2_9 int null ,
	 gr_classificacao2_10 int null ,
	 gr_classificacao2_11 int null ,
	 gr_classificacao2_12 int null ,
	 gr_classificacao2_13 int null ,
	 gr_classificacao2_14 int null ,
	 gr_classificacao2_15 int null ,
	 gr_classificacao2_16 int null ,
	 gr_classificacao2_17 int null ,
	 gr_classificacao2_18 int null ,
	 gr_classificacao2_19 int null ,
	 gr_classificacao2_20 int null ,
	 it_co_inscricao01_1 varchar(30) null ,
	 it_co_inscricao01_2 varchar(30) null ,
	 it_co_inscricao01_3 varchar(30) null ,
	 it_co_inscricao01_4 varchar(30) null ,
	 it_co_inscricao01_5 varchar(30) null ,
	 it_co_inscricao01_6 varchar(30) null ,
	 it_co_inscricao01_7 varchar(30) null ,
	 it_co_inscricao01_8 varchar(30) null ,
	 it_co_inscricao01_9 varchar(30) null ,
	 it_co_inscricao01_10 varchar(30) null ,
	 it_co_inscricao01_11 varchar(30) null ,
	 it_co_inscricao01_12 varchar(30) null ,
	 it_co_inscricao01_13 varchar(30) null ,
	 it_co_inscricao01_14 varchar(30) null ,
	 it_co_inscricao01_15 varchar(30) null ,
	 it_co_inscricao01_16 varchar(30) null ,
	 it_co_inscricao01_17 varchar(30) null ,
	 it_co_inscricao01_18 varchar(30) null ,
	 it_co_inscricao01_19 varchar(30) null ,
	 it_co_inscricao01_20 varchar(30) null ,
	 it_co_inscricao02_1 varchar(30) null ,
	 it_co_inscricao02_2 varchar(30) null ,
	 it_co_inscricao02_3 varchar(30) null ,
	 it_co_inscricao02_4 varchar(30) null ,
	 it_co_inscricao02_5 varchar(30) null ,
	 it_co_inscricao02_6 varchar(30) null ,
	 it_co_inscricao02_7 varchar(30) null ,
	 it_co_inscricao02_8 varchar(30) null ,
	 it_co_inscricao02_9 varchar(30) null ,
	 it_co_inscricao02_10 varchar(30) null ,
	 it_co_inscricao02_11 varchar(30) null ,
	 it_co_inscricao02_12 varchar(30) null ,
	 it_co_inscricao02_13 varchar(30) null ,
	 it_co_inscricao02_14 varchar(30) null ,
	 it_co_inscricao02_15 varchar(30) null ,
	 it_co_inscricao02_16 varchar(30) null ,
	 it_co_inscricao02_17 varchar(30) null ,
	 it_co_inscricao02_18 varchar(30) null ,
	 it_co_inscricao02_19 varchar(30) null ,
	 it_co_inscricao02_20 varchar(30) null ,
	 gr_classificacao_orcamentaria1_1 varchar(8) null ,
	 gr_classificacao_orcamentaria1_2 varchar(8) null ,
	 gr_classificacao_orcamentaria1_3 varchar(8) null ,
	 gr_classificacao_orcamentaria1_4 varchar(8) null ,
	 gr_classificacao_orcamentaria1_5 varchar(8) null ,
	 gr_classificacao_orcamentaria1_6 varchar(8) null ,
	 gr_classificacao_orcamentaria1_7 varchar(8) null ,
	 gr_classificacao_orcamentaria1_8 varchar(8) null ,
	 gr_classificacao_orcamentaria1_9 varchar(8) null ,
	 gr_classificacao_orcamentaria1_10 varchar(8) null ,
	 gr_classificacao_orcamentaria1_11 varchar(8) null ,
	 gr_classificacao_orcamentaria1_12 varchar(8) null ,
	 gr_classificacao_orcamentaria1_13 varchar(8) null ,
	 gr_classificacao_orcamentaria1_14 varchar(8) null ,
	 gr_classificacao_orcamentaria1_15 varchar(8) null ,
	 gr_classificacao_orcamentaria1_16 varchar(8) null ,
	 gr_classificacao_orcamentaria1_17 varchar(8) null ,
	 gr_classificacao_orcamentaria1_18 varchar(8) null ,
	 gr_classificacao_orcamentaria1_19 varchar(8) null ,
	 gr_classificacao_orcamentaria1_20 varchar(8) null ,
	 gr_classificacao_orcamentaria2_1 varchar(8) null ,
	 gr_classificacao_orcamentaria2_2 varchar(8) null ,
	 gr_classificacao_orcamentaria2_3 varchar(8) null ,
	 gr_classificacao_orcamentaria2_4 varchar(8) null ,
	 gr_classificacao_orcamentaria2_5 varchar(8) null ,
	 gr_classificacao_orcamentaria2_6 varchar(8) null ,
	 gr_classificacao_orcamentaria2_7 varchar(8) null ,
	 gr_classificacao_orcamentaria2_8 varchar(8) null ,
	 gr_classificacao_orcamentaria2_9 varchar(8) null ,
	 gr_classificacao_orcamentaria2_10 varchar(8) null ,
	 gr_classificacao_orcamentaria2_11 varchar(8) null ,
	 gr_classificacao_orcamentaria2_12 varchar(8) null ,
	 gr_classificacao_orcamentaria2_13 varchar(8) null ,
	 gr_classificacao_orcamentaria2_14 varchar(8) null ,
	 gr_classificacao_orcamentaria2_15 varchar(8) null ,
	 gr_classificacao_orcamentaria2_16 varchar(8) null ,
	 gr_classificacao_orcamentaria2_17 varchar(8) null ,
	 gr_classificacao_orcamentaria2_18 varchar(8) null ,
	 gr_classificacao_orcamentaria2_19 varchar(8) null ,
	 gr_classificacao_orcamentaria2_20 varchar(8) null ,
	 it_va_transacao_1 int null ,
	 it_va_transacao_2 int null ,
	 it_va_transacao_3 int null ,
	 it_va_transacao_4 int null ,
	 it_va_transacao_5 int null ,
	 it_va_transacao_6 int null ,
	 it_va_transacao_7 int null ,
	 it_va_transacao_8 int null ,
	 it_va_transacao_9 int null ,
	 it_va_transacao_10 int null ,
	 it_va_transacao_11 int null ,
	 it_va_transacao_12 int null ,
	 it_va_transacao_13 int null ,
	 it_va_transacao_14 int null ,
	 it_va_transacao_15 int null ,
	 it_va_transacao_16 int null ,
	 it_va_transacao_17 int null ,
	 it_va_transacao_18 int null ,
	 it_va_transacao_19 int null ,
	 it_va_transacao_20 int null ,
	 it_me_lancamento int null ,
	 it_co_sistema_origem varchar(10) null ,
	 it_nu_processo varchar(20) null ,
	 it_qt_lancamento int null ,
	 it_da_leitura_auditor_spb varchar(8) null ,
	 it_co_operacao_spb int null ,
	 it_nu_referencia_1 varchar(17) null ,
	 it_nu_referencia_2 varchar(17) null ,
	 it_nu_referencia_3 varchar(17) null ,
	 it_nu_referencia_4 varchar(17) null ,
	 it_nu_referencia_5 varchar(17) null ,
	 it_nu_referencia_6 varchar(17) null ,
	 it_nu_referencia_7 varchar(17) null ,
	 it_nu_referencia_8 varchar(17) null ,
	 it_nu_referencia_9 varchar(17) null ,
	 it_nu_referencia_10 varchar(17) null ,
	 it_nu_referencia_11 varchar(17) null ,
	 it_nu_referencia_12 varchar(17) null ,
	 it_nu_referencia_13 varchar(17) null ,
	 it_nu_referencia_14 varchar(17) null ,
	 it_nu_referencia_15 varchar(17) null ,
	 it_nu_referencia_16 varchar(17) null ,
	 it_nu_referencia_17 varchar(17) null ,
	 it_nu_referencia_18 varchar(17) null ,
	 it_nu_referencia_19 varchar(17) null ,
	 it_nu_referencia_20 varchar(17) null ,
	 it_in_controle_processo_me int null ,
	 it_co_ug_empenho int null ,
	 it_co_gestao_empenho int null ,
	 data datetime null
);

CREATE TABLE `log_situacoes` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_situacao int not null ,
	 tp_situacao char(1) not null ,
	 nu_sequencia int not null ,
	 ds_situacao varchar(200) not null ,
	 ic_financeiro int null ,
	 co_usuario int null ,
	 ic_ativo int not null ,
	 data datetime null
);

CREATE TABLE `log_subcategorias` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_subcategoria int not null ,
	 co_categoria int not null ,
	 no_subcategoria varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_tipo_inspecao` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_tipo_inspecao int not null ,
	 ds_tipo_inspecao varchar(50) not null ,
	 data datetime null
);

CREATE TABLE `log_uasgs` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_uasg int not null ,
	 uasg varchar(6) not null ,
	 data datetime null
);

CREATE TABLE `log_ufs` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 sg_uf varchar(2) not null ,
	 no_uf varchar(100) not null ,
	 data datetime null
);

CREATE TABLE `log_usuarios` (

	 id int not null primary key auto_increment,
	 log_co_usuario int null,
	 log_ds_nome varchar(50) null,
	 log_ds_email varchar(75) null,
	 log_ip varchar(20) null,
	 log_datahora datetime null,
	 co_usuario int not null ,
	 co_privilegio int null ,
	 co_setor int null ,
	 co_gestor int null ,
	 ic_acesso int null ,
	 nu_cpf varchar(14) not null ,
	 ds_nome varchar(100) not null ,
	 no_usuario varchar(100) null ,
	 ds_senha varchar(32) null ,
	 ds_email varchar(100) null ,
	 nu_telefone varchar(15) null ,
	 dt_liberacao datetime null ,
	 dt_bloqueio datetime null ,
	 dt_ult_acesso datetime null ,
	 ds_privilegios varchar(255) null ,
	 co_usuario_cad int null ,
	 ic_ativo int null ,
	 co_instituicoes int null ,
	 ic_campos_pesq_ctr varchar(50) null ,
	 ic_campos_pesq_cte varchar(50) null ,
	 co_graduacao int null ,
	 no_guerra varchar(30) null ,
	 nu_conselho varchar(20) null ,
	 licenca_nominal varchar(1) null ,
	 licenca_concorrente varchar(1) null ,
	 licenca_bi varchar(1) null ,
	 co_funcao int null ,
	 co_cargo int null ,
	 ck_aviso30 int null ,
	 ck_aviso60 int null ,
	 ck_aviso90 int null ,
	 ck_aviso120 int null ,
	 is_logged int not null ,
	 ip_ult_acesso varchar(20) null ,
	 co_projeto int null ,
	 data datetime null
);


