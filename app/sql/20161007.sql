CREATE TABLE `emails` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_setor` INT( 11 ) NULL ,
`co_remetente` INT( 11 ) NULL ,
`co_destinatario` INT( 11 ) NULL ,
`assunto` VARCHAR( 200 ) NOT NULL ,
`mensagem` TEXT NOT NULL ,
`destinatario` VARCHAR( 200 ) NOT NULL ,
`tipo` CHAR( 5 ) NOT NULL DEFAULT 'html',
`visualizado` INT( 1 ) NOT NULL DEFAULT '0',
`envio` DATETIME NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT = 'Caixa de Entrada de Emails';