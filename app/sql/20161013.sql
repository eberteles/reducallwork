ALTER TABLE `usuarios` 
ADD COLUMN `ck_aviso30` INT(1) NULL AFTER `co_projeto`,
ADD COLUMN `ck_aviso60` INT(1) NULL AFTER `ck_aviso30`,
ADD COLUMN `ck_aviso90` INT(1) NULL AFTER `ck_aviso60`,
ADD COLUMN `ck_aviso120` INT(1) NULL AFTER `ck_aviso90`,
ADD COLUMN `ic_alerta_email` INT(1) NULL DEFAULT 0 AFTER `ck_aviso120`;

-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: mysql.n2oti.com    Database: n2oti08
-- ------------------------------------------------------
-- Server version	5.5.43-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `co_setor` int(11) DEFAULT NULL,
  `co_remetente` int(11) DEFAULT NULL,
  `co_destinatario` int(11) DEFAULT NULL,
  `assunto` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `destinatario` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` char(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `visualizado` int(1) NOT NULL DEFAULT '0',
  `envio` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Caixa de Entrada de Emails';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES (1,NULL,23,23,'[GESCON] - ','<p style=\'color:#000\'>Olá, Admin.<br><br>Você deve realizar a atividade Atividade Teste, no período de : 25/09/2016 à 13/10/2016<br><br>DECORAÇÃO DE AMBIENTE<br><br><p><b>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</b></p>','Admin <eber@n2oti.com>','html',0,'2016-10-14 10:55:56','2016-10-14 10:56:07','2016-10-14 10:56:07');
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-14 11:08:58
