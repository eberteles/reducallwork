ALTER TABLE  `aditivos` ADD  `dt_fim_autorizacao` DATE NULL;

ALTER TABLE  `log_aditivos` ADD  `dt_fim_autorizacao` DATE NULL;

ALTER TABLE `aditivos` CHANGE `ds_fundamento_legal` `ds_fundamento_legal` VARCHAR(255) ;

ALTER TABLE `log_aditivos` CHANGE `ds_fundamento_legal` `ds_fundamento_legal` VARCHAR(255) ;