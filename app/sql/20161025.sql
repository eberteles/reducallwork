CREATE TABLE IF NOT EXISTS `notas_categorias` (
  `co_nota_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `ds_nota_categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_nota_categoria`)
);

ALTER TABLE  `notas_fiscais` ADD  `co_nota_categoria` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `log_notas_fiscais` ADD  `co_nota_categoria` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE `atividades` ADD COLUMN `ic_encerrada` INT(1) NULL DEFAULT 0 AFTER `atividade_atestada`;
