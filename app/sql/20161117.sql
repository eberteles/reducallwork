ALTER TABLE `empenhos` 
ADD COLUMN `nu_id_combin` INT(11) NULL AFTER `co_pre_empenho`,
ADD COLUMN `nu_codigo_orcamentario` INT(11) NULL AFTER `nu_id_combin`,
ADD COLUMN `nu_conta_contabil` INT(11) NULL AFTER `nu_codigo_orcamentario`,
ADD COLUMN `nu_centro_custo` INT(11) NULL AFTER `nu_conta_contabil`;