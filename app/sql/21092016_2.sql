CREATE TABLE uasgs ( co_uasg INT NOT NULL, uasg VARCHAR(6) NOT NULL, PRIMARY KEY (co_uasg))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci
COMMENT = 'Tabela para gerenciamento de UASGs';

ALTER TABLE uasgs CHANGE COLUMN co_uasg co_uasg INT(11) NOT NULL AUTO_INCREMENT ;
