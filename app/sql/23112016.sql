ALTER TABLE atividades CHANGE COLUMN nome nome VARCHAR(255) NULL DEFAULT NULL ;
ALTER TABLE atividades CHANGE COLUMN funcao_responsavel funcao_responsavel VARCHAR(255) NULL DEFAULT NULL;

ALTER TABLE contratos_fiscais CHANGE COLUMN co_usuario co_usuario INT(11) NULL DEFAULT NULL ;

ALTER TABLE penalidades CHANGE COLUMN co_usuario co_usuario INT(11) NULL DEFAULT NULL ;

# verificar
# ALTER TABLE log_contratos_fiscais CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_pendencias CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_penalidades CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_empenhos CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_notas_fiscais CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_liquidacoess CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;
# ALTER TABLE log_pagamentos CHANGE COLUMN log_ip log_ip VARCHAR(255) NULL DEFAULT NULL ;

ALTER TABLE contratos ADD COLUMN co_fase_anterior INT(11) NULL DEFAULT NULL;

ALTER TABLE notas_fiscais CHANGE COLUMN co_usuario co_usuario INT(11) NULL DEFAULT NULL ;

