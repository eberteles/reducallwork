ALTER TABLE  `log_empenhos` CHANGE  `ds_fonte_recurso`  `ds_fonte_recurso` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `log_empenhos` CHANGE  `nu_natureza_despesa`  `nu_natureza_despesa` VARCHAR( 10 ) NULL DEFAULT NULL;

ALTER TABLE  `fornecedores` ADD  `ds_senha` VARCHAR( 32 ) NULL;

ALTER TABLE  `log_fornecedores` ADD  `ds_senha` VARCHAR( 32 ) NULL;

ALTER TABLE  `fornecedores` CHANGE  `nu_fax`  `nu_fax` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `log_fornecedores` CHANGE  `nu_fax`  `nu_fax` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

DROP TABLE `candidatos`;

ALTER TABLE `contratos`
  DROP `co_tipo_inscricao`,
  DROP `co_candidato`;

ALTER TABLE  `contratos` CHANGE  `co_denominacao`  `co_evento` INT( 11 ) NULL DEFAULT NULL;

ALTER TABLE  `contratos` ADD  `co_prova` INT( 11 ) NULL AFTER  `co_evento`;

ALTER TABLE  `andamentos` ADD  `co_fornecedor` INT( 11 ) NULL AFTER  `co_usuario`;