ALTER TABLE empenhos ADD sub_nu_ptres INT (4) after nu_ptres;

ALTER TABLE log_empenhos ADD sub_nu_ptres INT (4) after nu_ptres;

DROP TRIGGER IF EXISTS `ADD_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_EMPENHO` AFTER INSERT ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_empenho       = NEW.co_empenho,
            co_contrato      = NEW.co_contrato,
            nu_empenho       = NEW.nu_empenho,
            tp_empenho       = NEW.tp_empenho,
            ds_empenho       = NEW.ds_empenho,
            co_plano_interno = NEW.co_plano_interno,
            vl_empenho       = NEW.vl_empenho,
            dt_empenho       = NEW.dt_empenho,
            nu_ptres         = NEW.nu_ptres,
            sub_nu_ptres     = NEW.sub_nu_ptres,
            nu_evento_contabil      = NEW.nu_evento_contabil,
            nu_esfera_orcamentaria  = NEW.nu_esfera_orcamentaria,
            ds_fonte_recurso    = NEW.ds_fonte_recurso,
            nu_natureza_despesa = NEW.nu_natureza_despesa,
            ds_inciso           = NEW.ds_inciso,
            ds_amparo_legal     = NEW.ds_amparo_legal,
            nu_ug_responsavel   = NEW.nu_ug_responsavel,
            nu_origem_material  = NEW.nu_origem_material,
            uf_beneficiada      = NEW.uf_beneficiada,
            nu_municipio_beneficiado = NEW.nu_municipio_beneficiado,
            nu_contra_entrega   = NEW.nu_contra_entrega,
            nu_lista            = NEW.nu_lista;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_EMPENHO` AFTER UPDATE ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_empenho       = NEW.co_empenho,
            co_contrato      = NEW.co_contrato,
            nu_empenho       = NEW.nu_empenho,
            tp_empenho       = NEW.tp_empenho,
            ds_empenho       = NEW.ds_empenho,
            co_plano_interno = NEW.co_plano_interno,
            vl_empenho       = NEW.vl_empenho,
            dt_empenho       = NEW.dt_empenho,
            nu_ptres         = NEW.nu_ptres,
            sub_nu_ptres     = NEW.sub_nu_ptres,
            nu_evento_contabil      = NEW.nu_evento_contabil,
            nu_esfera_orcamentaria  = NEW.nu_esfera_orcamentaria,
            ds_fonte_recurso    = NEW.ds_fonte_recurso,
            nu_natureza_despesa = NEW.nu_natureza_despesa,
            ds_inciso           = NEW.ds_inciso,
            ds_amparo_legal     = NEW.ds_amparo_legal,
            nu_ug_responsavel   = NEW.nu_ug_responsavel,
            nu_origem_material  = NEW.nu_origem_material,
            uf_beneficiada      = NEW.uf_beneficiada,
            nu_municipio_beneficiado = NEW.nu_municipio_beneficiado,
            nu_contra_entrega   = NEW.nu_contra_entrega,
            nu_lista            = NEW.nu_lista;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_EMPENHO` AFTER DELETE ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_empenho       = OLD.co_empenho,
            co_contrato      = OLD.co_contrato,
            nu_empenho       = OLD.nu_empenho,
            tp_empenho       = OLD.tp_empenho,
            ds_empenho       = OLD.ds_empenho,
            co_plano_interno = OLD.co_plano_interno,
            vl_empenho       = OLD.vl_empenho,
            dt_empenho       = OLD.dt_empenho,
            nu_ptres         = OLD.nu_ptres,
            sub_nu_ptres     = OLD.sub_nu_ptres,
            nu_evento_contabil      = OLD.nu_evento_contabil,
            nu_esfera_orcamentaria  = OLD.nu_esfera_orcamentaria,
            ds_fonte_recurso    = OLD.ds_fonte_recurso,
            nu_natureza_despesa = OLD.nu_natureza_despesa,
            ds_inciso           = OLD.ds_inciso,
            ds_amparo_legal     = OLD.ds_amparo_legal,
            nu_ug_responsavel   = OLD.nu_ug_responsavel,
            nu_origem_material  = OLD.nu_origem_material,
            uf_beneficiada      = OLD.uf_beneficiada,
            nu_municipio_beneficiado = OLD.nu_municipio_beneficiado,
            nu_contra_entrega   = OLD.nu_contra_entrega,
            nu_lista            = OLD.nu_lista;
        END
//
DELIMITER ;