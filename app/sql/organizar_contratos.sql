CREATE FUNCTION `org_contratos`( nu_contrato VARCHAR(15) ) RETURNS varchar(15) CHARSET utf8
BEGIN
DECLARE final_date VARCHAR(15);
DECLARE final_number VARCHAR(5);
DECLARE siasg_org VARCHAR(15);

SET final_date = SUBSTRING(nu_contrato,6,4);
SET final_number = SUBSTRING(nu_contrato,1,5);

SET siasg_org = CONCAT(final_date,final_number);
RETURN siasg_org;
END