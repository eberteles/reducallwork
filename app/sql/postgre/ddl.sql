﻿CREATE SEQUENCE seq_aditivos INCREMENT 1 START 1;

CREATE TABLE aditivos (
	 co_aditivo int not null  primary key default nextval('seq_aditivos'::regclass) ,
	 co_contrato int not null ,
	 no_aditivo varchar(255) null ,
	 tp_aditivo char(1) null ,
	 dt_aditivo date null ,
	 vl_aditivo decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_assinatura date null ,
	 dt_prazo date null ,
	 ds_aditivo varchar(1500) not null ,
	 co_usuario int not null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(20) null ,
	 dt_vigencia_de date null ,
	 dt_fim_vigencia date null 
); 

CREATE SEQUENCE seq_aditivos_backup INCREMENT 1 START 1;

CREATE TABLE aditivos_backup (
	 co_aditivo int not null ,
	 co_contrato int not null ,
	 no_aditivo varchar(255) null ,
	 tp_aditivo char(1) not null ,
	 dt_aditivo date not null ,
	 vl_aditivo decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_assinatura date null ,
	 dt_prazo date null ,
	 ds_aditivo varchar(1500) not null ,
	 co_usuario int not null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(20) null ,
	 dt_vigencia_de date null 
); 

CREATE SEQUENCE seq_alertas INCREMENT 1 START 1;

CREATE TABLE alertas (
	 co_alerta int not null  primary key default nextval('seq_alertas'::regclass) ,
	 ds_email_from varchar(100) null ,
	 ds_smtp_host varchar(40) null ,
	 ds_smtp_port varchar(10) null ,
	 ds_smtp_user varchar(100) null ,
	 ds_smtp_pass varchar(20) null ,
	 ck_aviso30 char(1) null ,
	 ck_aviso_uni30 char(1) null ,
	 ck_aviso_fis30 char(1) null ,
	 ck_aviso_for30 char(1) null ,
	 ck_aviso60 char(1) null ,
	 ck_aviso_uni60 char(1) null ,
	 ck_aviso_fis60 char(1) null ,
	 ck_aviso_for60 char(1) null ,
	 ck_aviso90 char(1) null ,
	 ck_aviso_uni90 char(1) null ,
	 ck_aviso_fis90 char(1) null ,
	 ck_aviso_for90 char(1) null ,
	 ck_aviso120 char(1) null ,
	 ck_aviso_uni120 char(1) null ,
	 ck_aviso_fis120 char(1) null ,
	 ck_aviso_for120 char(1) null ,
	 ck_aviso_sup30 char(1) null ,
	 ck_aviso_muda_fase int null 
); 

CREATE SEQUENCE seq_andamentos INCREMENT 1 START 1;

CREATE TABLE andamentos (
	 co_andamento int not null  primary key default nextval('seq_andamentos'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 co_fornecedor int null ,
	 nu_sequencia int null ,
	 co_setor int null ,
	 co_fase int null ,
	 assinatura_id int null ,
	 dt_andamento timestamp not null ,
	 dt_fim timestamp null ,
	 ds_andamento varchar(255) null ,
	 ds_justificativa varchar(255) null ,
	 ic_aviso_expiracao int not null 
); 

CREATE SEQUENCE seq_anexos INCREMENT 1 START 1;

CREATE TABLE anexos (
	 co_anexo int not null  primary key default nextval('seq_anexos'::regclass) ,
	 co_anexo_pasta int null ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_atividade int null ,
	 co_contrato_fiscal int null ,
	 co_aditivo int null ,
	 co_apostilamento int null ,
	 co_garantia int null ,
	 co_garantia_suporte int null ,
	 co_produto int null ,
	 co_oficio int null ,
	 co_empenho int null ,
	 co_penalidade int null ,
	 co_nota int null ,
	 co_pendencia int null ,
	 co_pagamento int null ,
	 co_historico int null ,
	 co_evento int null ,
	 co_ordem_fornecimento_servico int null ,
	 dt_anexo date null ,
	 tp_documento int not null ,
	 ds_anexo varchar(255) not null ,
	 ds_extensao varchar(5) not null ,
	 conteudo bytea not null ,
	 ds_observacao varchar(255) null ,
	 ic_atesto int not null ,
	 assinatura text null ,
	 chave text null ,
	 co_usuario_assinatura int null ,
	 ds_usuario_token text null ,
	 ic_indexado int null 
); 

CREATE SEQUENCE seq_anexos_indexacoes INCREMENT 1 START 1;

CREATE TABLE anexos_indexacoes (
	 co_anexo_indexacao int not null  primary key default nextval('seq_anexos_indexacoes'::regclass) ,
	 co_anexo int not null ,
	 nu_pagina int not null ,
	 ds_conteudo text not null 
); 

CREATE SEQUENCE seq_anexos_pastas INCREMENT 1 START 1;

CREATE TABLE anexos_pastas (
	 co_anexo_pasta int not null  primary key default nextval('seq_anexos_pastas'::regclass) ,
	 parent_id int null ,
	 co_ata int null ,
	 co_contrato int null ,
	 ds_anexo_pasta varchar(100) not null 
); 

CREATE SEQUENCE seq_apostilamentos INCREMENT 1 START 1;

CREATE TABLE apostilamentos (
	 co_apostilamento int not null  primary key default nextval('seq_apostilamentos'::regclass) ,
	 co_contrato int not null ,
	 no_apostilamento varchar(255) null ,
	 tp_apostilamento char(1) not null ,
	 dt_apostilamento date not null ,
	 vl_apostilamento decimal null ,
	 dt_fim_vigencia_anterior date null ,
	 dt_prazo date null ,
	 ds_apostilamento varchar(1500) not null ,
	 co_usuario int not null ,
	 ds_fundamento_legal varchar(255) null 
); 

CREATE SEQUENCE seq_app_user INCREMENT 1 START 1;

CREATE TABLE app_user (
	 co_app_user int not null  primary key default nextval('seq_app_user'::regclass) ,
	 co_usuario int not null ,
	 usuario_ds_nome varchar(45) null ,
	 user_ip varchar(15) not null ,
	 mac_addres_info varchar(100) null 
); 

CREATE SEQUENCE seq_aquisicoes INCREMENT 1 START 1;

CREATE TABLE aquisicoes (
	 co_aquisicao int not null  primary key default nextval('seq_aquisicoes'::regclass) ,
	 co_item_aquisicao int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 ds_aquisicao varchar(500) not null ,
	 dt_aquisicao date null ,
	 vl_aquisicao decimal not null ,
	 qt_aquisicao int not null 
); 

CREATE SEQUENCE seq_areas INCREMENT 1 START 1;

CREATE TABLE areas (
	 co_area int not null  primary key default nextval('seq_areas'::regclass) ,
	 parent_id int null ,
	 ds_area varchar(100) not null 
); 

CREATE SEQUENCE seq_assinaturas INCREMENT 1 START 1;

CREATE TABLE assinaturas (
	 id int not null  primary key default nextval('seq_assinaturas'::regclass) ,
	 usuario_id int not null ,
	 authentication_id int not null ,
	 texto text not null ,
	 assinatura text not null ,
	 usuario_token varchar(500) not null ,
	 created timestamp not null ,
	 modified timestamp not null 
); 

CREATE SEQUENCE seq_assinaturas_login INCREMENT 1 START 1;

CREATE TABLE assinaturas_login (
	 id int not null  primary key default nextval('seq_assinaturas_login'::regclass) ,
	 cod_usuario int null ,
	 cod_contrato int null 
); 

CREATE SEQUENCE seq_assinaturas_requisitante INCREMENT 1 START 1;

CREATE TABLE assinaturas_requisitante (
	 id int not null  primary key default nextval('seq_assinaturas_requisitante'::regclass) ,
	 nome_completo varchar(75) not null ,
	 cargo varchar(45) null ,
	 data_assinatura timestamp null ,
	 referencia text null ,
	 co_contrato int not null 
); 

CREATE SEQUENCE seq_atas INCREMENT 1 START 1;

CREATE TABLE atas (
	 co_ata int not null  primary key default nextval('seq_atas'::regclass) ,
	 co_categoria_ata int not null ,
	 co_fornecedor int not null ,
	 nu_ata varchar(9) not null ,
	 nu_processo varchar(20) not null ,
	 ds_publicacao varchar(1000) null ,
	 tp_ata char(1) not null ,
	 tp_pregao char(1) null ,
	 ds_especificacao varchar(1000) null ,
	 ds_justificativa varchar(1000) null ,
	 dt_vigencia_inicio date not null ,
	 dt_vigencia_fim date not null ,
	 vl_ata decimal null ,
	 vl_mensal decimal null ,
	 vl_anual decimal not null ,
	 tp_status char(1) not null ,
	 dt_cadastro date not null ,
	 co_usuario int not null 
); 

CREATE SEQUENCE seq_atas_categorias INCREMENT 1 START 1;

CREATE TABLE atas_categorias (
	 co_ata_categoria int not null  primary key default nextval('seq_atas_categorias'::regclass) ,
	 ds_ata_categoria varchar(255) not null 
); 

CREATE SEQUENCE seq_atas_itens INCREMENT 1 START 1;

CREATE TABLE atas_itens (
	 co_ata_item int not null  primary key default nextval('seq_atas_itens'::regclass) ,
	 co_ata int not null ,
	 co_ata_lote int null ,
	 nu_item int not null ,
	 ds_marca varchar(255) null ,
	 ds_descricao varchar(500) not null ,
	 ds_unidade varchar(20) not null ,
	 nu_quantidade int not null ,
	 qt_utilizado int not null ,
	 vl_unitario decimal not null 
); 

CREATE SEQUENCE seq_atas_lotes INCREMENT 1 START 1;

CREATE TABLE atas_lotes (
	 co_ata_lote int not null  primary key default nextval('seq_atas_lotes'::regclass) ,
	 co_ata int not null ,
	 nu_lote varchar(10) not null ,
	 ds_objeto varchar(255) not null 
); 

CREATE SEQUENCE seq_atas_pedidos INCREMENT 1 START 1;

CREATE TABLE atas_pedidos (
	 co_ata_pedido int not null  primary key default nextval('seq_atas_pedidos'::regclass) ,
	 co_ata int not null ,
	 co_contrato int null ,
	 ds_pedido varchar(255) not null ,
	 vl_pedido decimal not null ,
	 tp_pedido char(1) not null ,
	 dt_pedido date not null ,
	 co_usuario int not null 
); 

CREATE SEQUENCE seq_atividades INCREMENT 1 START 1;

CREATE TABLE atividades (
	 co_atividade int not null  primary key default nextval('seq_atividades'::regclass) ,
	 co_contrato int not null ,
	 parent_id int null ,
	 co_usuario int not null ,
	 co_responsavel int not null ,
	 ds_atividade varchar(255) not null ,
	 tp_andamento varchar(1) not null ,
	 dt_ini_planejado date not null ,
	 dt_ini_execucao date null ,
	 dt_fim_execucao date null ,
	 pc_executado int not null ,
	 co_tipo_inspecao int null ,
	 ds_assunto_email varchar(50) null ,
	 atividade_atestada int not null ,
	 nome varchar(255) not null ,
	 periodicidade varchar(255) null ,
	 dia_execucao varchar(255) null ,
	 funcao_responsavel varchar(255) not null ,
	 pendencia varchar(255) null ,
	 dt_fim_planejado date null ,
	 ic_pendente int null 
); 

CREATE SEQUENCE seq_bancos INCREMENT 1 START 1;

CREATE TABLE bancos (
	 co_banco int not null  primary key default nextval('seq_bancos'::regclass) ,
	 nu_banco varchar(6) not null ,
	 ds_banco varchar(100) not null 
); 

CREATE SEQUENCE seq_categorias INCREMENT 1 START 1;

CREATE TABLE categorias (
	 co_categoria int not null  primary key default nextval('seq_categorias'::regclass) ,
	 no_categoria varchar(100) not null ,
	 co_usuario int null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_clausulas_contratuais INCREMENT 1 START 1;

CREATE TABLE clausulas_contratuais (
	 id int not null  primary key default nextval('seq_clausulas_contratuais'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int not null ,
	 nu_clausula varchar(100) not null ,
	 ds_clausula varchar(500) not null ,
	 created timestamp not null ,
	 updated timestamp not null 
); 

CREATE SEQUENCE seq_colunas_resultado_pesquisa INCREMENT 1 START 1;

CREATE TABLE colunas_resultado_pesquisa (
	 co_coluna_resultado int not null  primary key default nextval('seq_colunas_resultado_pesquisa'::regclass) ,
	 ic_modulo char(3) not null ,
	 ds_nome varchar(50) not null ,
	 ds_dominio varchar(20) not null ,
	 ds_coluna_dominio varchar(20) not null ,
	 ds_funcao_habilita varchar(20) null ,
	 ds_funcao_exibe varchar(20) null 
); 

CREATE SEQUENCE seq_confederacoes INCREMENT 1 START 1;

CREATE TABLE confederacoes (
	 co_confederacao int not null  primary key default nextval('seq_confederacoes'::regclass) ,
	 parent_id int null ,
	 ds_confederacao varchar(100) not null ,
	 ic_cob varchar(1) null ,
	 ic_cpb varchar(1) null ,
	 no_contato varchar(255) null ,
	 ds_email varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 nu_celular varchar(15) null ,
	 ds_endereco varchar(255) null ,
	 ds_complemento varchar(100) null ,
	 ds_bairro varchar(255) null ,
	 sg_uf varchar(2) null ,
	 co_municipio int null ,
	 nu_cep varchar(10) null ,
	 co_usuario int null 
); 

CREATE SEQUENCE seq_contratacoes INCREMENT 1 START 1;

CREATE TABLE contratacoes (
	 co_contratacao int not null  primary key default nextval('seq_contratacoes'::regclass) ,
	 ds_contratacao varchar(100) not null ,
	 co_siafi int null ,
	 co_siasg varchar(2) null ,
	 at_contratacao int null 
); 

CREATE SEQUENCE seq_contratos INCREMENT 1 START 1;

CREATE TABLE contratos (
	 co_contrato int not null  primary key default nextval('seq_contratos'::regclass) ,
	 co_instituicao int null ,
	 co_situacao int null ,
	 co_modalidade int null ,
	 co_servico int null ,
	 co_categoria int null ,
	 co_subcategoria int null ,
	 co_fornecedor int null ,
	 co_contratante int null ,
	 co_executante int null ,
	 co_contratacao int null ,
	 co_fiscal_atual int null ,
	 co_gestor_atual int null ,
	 co_confederacao int null ,
	 co_federacao int null ,
	 co_classificacao int null ,
	 co_evento int null ,
	 co_prova int null ,
	 nu_sequencia int null ,
	 ic_ativo char(1) null ,
	 ic_tipo_contrato char(1) not null ,
	 ds_motivo text null ,
	 co_fase int null ,
	 dt_fase date null ,
	 co_setor int null ,
	 is_desvio_fluxo char(1) null ,
	 nu_pam varchar(30) null ,
	 nu_contrato varchar(40) null ,
	 nu_processo varchar(20) null ,
	 tp_aquisicao varchar(2) null ,
	 ic_ata_vigente varchar(1) null ,
	 ic_ata_orgao varchar(1) null ,
	 ic_dispensa_licitacao varchar(1) null ,
	 ds_objeto varchar(1500) null ,
	 nu_licitacao varchar(9) null ,
	 dt_publicacao date null ,
	 ds_fundamento_legal varchar(500) null ,
	 dt_autorizacao_pam date null ,
	 dt_assinatura date null ,
	 dt_ini_vigencia date null ,
	 dt_fim_vigencia date null ,
	 dt_fim_vigencia_inicio date null ,
	 dt_ini_processo date null ,
	 dt_fim_processo date null ,
	 st_repactuado varchar(1) null ,
	 vl_inicial double precision null ,
	 vl_mensal double precision null ,
	 tp_valor varchar(1) null ,
	 vl_servico double precision null ,
	 vl_material double precision null ,
	 vl_global double precision null ,
	 ds_observacao varchar(255) null ,
	 fg_financeiro varchar(1) null ,
	 nu_pendencias int null ,
	 dt_cadastro timestamp null ,
	 dt_cadastro_pam timestamp null ,
	 dt_cadastro_processo timestamp null ,
	 dt_tais date null ,
	 dt_prazo_processo date null ,
	 co_usuario int not null ,
	 co_ident_siasg varchar(20) null ,
	 nu_qtd_parcelas int null ,
	 co_solicitante int not null ,
	 co_responsavel int null ,
	 ds_requisitante varchar(100) null ,
	 pt_requisitante varchar(100) null ,
	 tl_requisitante varchar(15) null ,
	 dt_solicitacao date null ,
	 ds_assunto varchar(250) null ,
	 ds_objeto_plano varchar(500) null ,
	 ds_vantagem varchar(500) null ,
	 ds_criterio varchar(500) null ,
	 ds_justificativa varchar(500) null ,
	 ds_resultado varchar(500) null ,
	 ds_demanda varchar(500) null ,
	 ds_aproveitamento varchar(500) null ,
	 ds_gerenciador_ata varchar(50) null ,
	 ic_prod_presente varchar(1) null ,
	 ic_pesq_preco varchar(1) null ,
	 ic_inexigivel varchar(1) null ,
	 ic_serv_continuado varchar(1) null ,
	 ic_prod_parcelado varchar(1) null ,
	 co_assinatura_ordenador int null ,
	 co_assinatura_chefe int null ,
	 dt_assinatura_chefe timestamp null ,
	 dt_assinatura_ordenador timestamp null ,
	 ds_tipo_servico varchar(255) null ,
	 uasg varchar(6) null ,
	 co_contrato_pai int null 
); 

CREATE SEQUENCE seq_contratos_fiscais INCREMENT 1 START 1;

CREATE TABLE contratos_fiscais (
	 co_contrato_fiscal int not null  primary key default nextval('seq_contratos_fiscais'::regclass) ,
	 co_usuario int not null ,
	 co_contrato int not null ,
	 co_usuario_fiscal int null ,
	 dt_inicio date not null ,
	 dt_fim date null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_contratos_itens INCREMENT 1 START 1;

CREATE TABLE contratos_itens (
	 co_contrato_item int not null  primary key default nextval('seq_contratos_itens'::regclass) ,
	 co_contrato int not null ,
	 co_fornecedor int null ,
	 nu_ordem int not null ,
	 nu_item_pregao int null ,
	 nu_unidade varchar(25) not null ,
	 qt_solicitada int not null ,
	 vl_unitario double precision null ,
	 ds_demanda varchar(250) not null ,
	 co_siasg int null ,
	 qt_consumo int null ,
	 co_cat varchar(15) null 
); 

CREATE SEQUENCE seq_cronograma_financeiro_desembolso INCREMENT 1 START 1;

CREATE TABLE cronograma_financeiro_desembolso (
	 co_cronograma_financeiro_desembolso int not null  primary key default nextval('seq_cronograma_financeiro_desembolso'::regclass) ,
	 co_contrato int null ,
	 descricao varchar(200) null ,
	 mes int null ,
	 dt_inicio date null ,
	 dt_data_final date null ,
	 percentual float null ,
	 ds_valor decimal null 
); 

CREATE SEQUENCE seq_dashboards INCREMENT 1 START 1;

CREATE TABLE dashboards (
	 co_dashboard int not null  primary key default nextval('seq_dashboards'::regclass) ,
	 co_usuario int not null 
); 

CREATE SEQUENCE seq_dsp_cargos INCREMENT 1 START 1;

CREATE TABLE dsp_cargos (
	 co_cargo int not null  primary key default nextval('seq_dsp_cargos'::regclass) ,
	 nome_cargo varchar(50) null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_dsp_categoria_passagem INCREMENT 1 START 1;

CREATE TABLE dsp_categoria_passagem (
	 co_categoria_passagem int not null  primary key default nextval('seq_dsp_categoria_passagem'::regclass) ,
	 nome_categoria_passagem varchar(50) null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_dsp_despesas_viagem INCREMENT 1 START 1;

CREATE TABLE dsp_despesas_viagem (
	 co_despesas_viagem int not null  primary key default nextval('seq_dsp_despesas_viagem'::regclass) ,
	 numero_processo varchar(20) not null ,
	 motivo_viagem varchar(255) not null ,
	 cod_unidade_lotacao int not null ,
	 nome_funcionario varchar(50) not null ,
	 matricula_funcionario varchar(15) not null ,
	 cod_cargo int not null ,
	 cod_funcao int not null ,
	 origem varchar(50) not null ,
	 uf_origem varchar(2) not null ,
	 destino varchar(50) not null ,
	 uf_destino varchar(2) not null ,
	 dt_inicio date not null ,
	 dt_final date not null ,
	 cod_meio_transporte int not null ,
	 cod_categoria_passagem int not null ,
	 valor_passagem double precision not null ,
	 numero_diarias int not null ,
	 valor_total_diarias double precision not null ,
	 valor_total_viagem double precision null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_dsp_funcao INCREMENT 1 START 1;

CREATE TABLE dsp_funcao (
	 co_funcao int not null  primary key default nextval('seq_dsp_funcao'::regclass) ,
	 nome_funcao varchar(50) null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_dsp_log_cargos INCREMENT 1 START 1;

CREATE TABLE dsp_log_cargos (
	 co_cargo int not null  primary key default nextval('seq_dsp_log_cargos'::regclass) ,
	 nome_cargo varchar(50) null 
); 

CREATE SEQUENCE seq_dsp_log_categoria_passagem INCREMENT 1 START 1;

CREATE TABLE dsp_log_categoria_passagem (
	 co_categoria_passagem int not null  primary key default nextval('seq_dsp_log_categoria_passagem'::regclass) ,
	 nome_categoria_passagem varchar(50) null 
); 

CREATE SEQUENCE seq_dsp_log_despesas_viagem INCREMENT 1 START 1;

CREATE TABLE dsp_log_despesas_viagem (
	 co_despesas_viagem int not null  primary key default nextval('seq_dsp_log_despesas_viagem'::regclass) ,
	 numero_processo varchar(20) not null ,
	 motivo_viagem varchar(255) not null ,
	 cod_unidade_lotacao int not null ,
	 nome_funcionario varchar(50) not null ,
	 matricula_funcionario varchar(15) not null ,
	 cod_cargo int not null ,
	 cod_funcao int not null ,
	 origem varchar(50) not null ,
	 uf_origem varchar(2) not null ,
	 destino varchar(50) not null ,
	 uf_destino varchar(2) not null ,
	 dt_inicio date not null ,
	 dt_final date not null ,
	 cod_meio_transporte int not null ,
	 cod_categoria_passagem int not null ,
	 valor_passagem double precision not null ,
	 numero_diarias int not null ,
	 valor_total_diarias double precision not null ,
	 valor_total_viagem double precision null 
); 

CREATE SEQUENCE seq_dsp_log_funcao INCREMENT 1 START 1;

CREATE TABLE dsp_log_funcao (
	 co_funcao int not null  primary key default nextval('seq_dsp_log_funcao'::regclass) ,
	 nome_funcao varchar(50) null 
); 

CREATE SEQUENCE seq_dsp_log_meio_transporte INCREMENT 1 START 1;

CREATE TABLE dsp_log_meio_transporte (
	 co_meio_transporte int not null  primary key default nextval('seq_dsp_log_meio_transporte'::regclass) ,
	 nome_meio_transporte varchar(50) null 
); 

CREATE SEQUENCE seq_dsp_log_unidade_lotacao INCREMENT 1 START 1;

CREATE TABLE dsp_log_unidade_lotacao (
	 co_unidade int not null  primary key default nextval('seq_dsp_log_unidade_lotacao'::regclass) ,
	 nome_unidade varchar(50) not null 
); 

CREATE SEQUENCE seq_dsp_meio_transporte INCREMENT 1 START 1;

CREATE TABLE dsp_meio_transporte (
	 co_meio_transporte int not null  primary key default nextval('seq_dsp_meio_transporte'::regclass) ,
	 nome_meio_transporte varchar(50) null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_dsp_unidade_lotacao INCREMENT 1 START 1;

CREATE TABLE dsp_unidade_lotacao (
	 co_unidade int not null  primary key default nextval('seq_dsp_unidade_lotacao'::regclass) ,
	 nome_unidade varchar(50) not null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_elogios INCREMENT 1 START 1;

CREATE TABLE elogios (
	 co_elogio int not null  primary key default nextval('seq_elogios'::regclass) ,
	 co_fornecedor int not null ,
	 co_contrato int not null ,
	 nu_elogio char(8) not null ,
	 ds_elogio varchar(300) not null 
); 

CREATE SEQUENCE seq_emails INCREMENT 1 START 1;

CREATE TABLE emails (
	 id int not null  primary key default nextval('seq_emails'::regclass) ,
	 co_setor int null ,
	 co_remetente int null ,
	 co_destinatario int null ,
	 assunto varchar(200) not null ,
	 mensagem text not null ,
	 destinatario varchar(200) not null ,
	 tipo char(5) not null ,
	 visualizado int not null ,
	 envio timestamp null ,
	 created timestamp not null ,
	 updated timestamp not null 
); 

CREATE SEQUENCE seq_empenhos INCREMENT 1 START 1;

CREATE TABLE empenhos (
	 co_empenho int not null  primary key default nextval('seq_empenhos'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_empenho varchar(12) null ,
	 tp_empenho varchar(1) not null ,
	 ds_empenho varchar(1500) not null ,
	 co_plano_interno varchar(20) null ,
	 vl_empenho double precision null ,
	 dt_empenho date null ,
	 nu_ptres varchar(20) null ,
	 sub_nu_ptres int null ,
	 nu_evento_contabil int null ,
	 nu_esfera_orcamentaria int null ,
	 ds_fonte_recurso varchar(100) null ,
	 nu_natureza_despesa varchar(10) null ,
	 ds_inciso varchar(2) null ,
	 ds_amparo_legal varchar(8) null ,
	 nu_ug_responsavel int null ,
	 nu_origem_material int null ,
	 uf_beneficiada varchar(2) null ,
	 nu_municipio_beneficiado int null ,
	 nu_contra_entrega int null ,
	 nu_lista varchar(12) null ,
	 tp_anulacao char(1) null ,
	 dt_anulacao date null ,
	 vl_anulacao decimal null ,
	 vl_original decimal null ,
	 empenho_reforco_originario varchar(20) null ,
	 empenho_anulacao_originario varchar(20) null ,
	 vl_restante double precision null ,
	 co_pre_empenho int null ,
	 nu_processo_pagamento varchar(20) null 
); 

CREATE SEQUENCE seq_empenhos_anulacoes INCREMENT 1 START 1;

CREATE TABLE empenhos_anulacoes (
	 co_empenho_anulacao int not null  primary key default nextval('seq_empenhos_anulacoes'::regclass) ,
	 co_empenho int not null ,
	 tp_anulacao char(1) not null ,
	 dt_anulacao date not null ,
	 vl_anulacao decimal not null ,
	 nu_empenho varchar(20) null 
); 

CREATE SEQUENCE seq_empenhos_backup INCREMENT 1 START 1;

CREATE TABLE empenhos_backup (
	 co_empenho int not null ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_empenho varchar(12) not null ,
	 tp_empenho varchar(1) not null ,
	 ds_empenho varchar(1500) not null ,
	 co_plano_interno varchar(20) null ,
	 vl_empenho decimal not null ,
	 dt_empenho date null ,
	 nu_ptres varchar(20) null ,
	 sub_nu_ptres int null ,
	 nu_evento_contabil int null ,
	 nu_esfera_orcamentaria int null ,
	 ds_fonte_recurso varchar(100) null ,
	 nu_natureza_despesa varchar(10) null ,
	 ds_inciso varchar(2) null ,
	 ds_amparo_legal varchar(8) null ,
	 nu_ug_responsavel int null ,
	 nu_origem_material int null ,
	 uf_beneficiada varchar(2) null ,
	 nu_municipio_beneficiado int null ,
	 nu_contra_entrega int null ,
	 nu_lista varchar(12) null ,
	 tp_anulacao char(1) null ,
	 dt_anulacao date null ,
	 vl_anulacao decimal null ,
	 vl_original decimal null ,
	 empenho_reforco_originario varchar(20) null ,
	 empenho_anulacao_originario varchar(20) null 
); 

CREATE SEQUENCE seq_entregas INCREMENT 1 START 1;

CREATE TABLE entregas (
	 co_entrega int not null  primary key default nextval('seq_entregas'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int not null ,
	 dt_entrega date not null ,
	 dt_entrega_oficial date not null ,
	 pz_recebimento int not null ,
	 dt_recebimento date not null 
); 

CREATE SEQUENCE seq_especialidade INCREMENT 1 START 1;

CREATE TABLE especialidade (
	 co_especialidade int not null  primary key default nextval('seq_especialidade'::regclass) ,
	 ds_especialidade varchar(50) not null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_eventos INCREMENT 1 START 1;

CREATE TABLE eventos (
	 co_evento int not null  primary key default nextval('seq_eventos'::regclass) ,
	 co_confederacao int not null ,
	 co_usuario int not null ,
	 co_modalidade int not null ,
	 co_prova int not null ,
	 co_prova_categoria int null ,
	 co_prova_subcategoria int null ,
	 co_prova_classificacao int null ,
	 co_categoria int not null ,
	 nu_ano int not null ,
	 ds_evento varchar(500) not null ,
	 ic_evento char(1) not null ,
	 dt_inicio date not null ,
	 dt_fim date not null ,
	 ds_local varchar(500) not null ,
	 ic_sexo char(1) null 
); 

CREATE SEQUENCE seq_eventos_competidores INCREMENT 1 START 1;

CREATE TABLE eventos_competidores (
	 co_evento_competidor int not null  primary key default nextval('seq_eventos_competidores'::regclass) ,
	 co_evento int not null ,
	 sg_uf varchar(2) null ,
	 co_pais varchar(2) null 
); 

CREATE SEQUENCE seq_eventos_participantes INCREMENT 1 START 1;

CREATE TABLE eventos_participantes (
	 co_evento_participante int not null  primary key default nextval('seq_eventos_participantes'::regclass) ,
	 co_evento int not null ,
	 sg_uf varchar(2) null ,
	 co_pais varchar(2) null 
); 

CREATE SEQUENCE seq_eventos_resultados INCREMENT 1 START 1;

CREATE TABLE eventos_resultados (
	 co_evento_resultado int not null  primary key default nextval('seq_eventos_resultados'::regclass) ,
	 co_evento int not null ,
	 co_fornecedor int not null ,
	 nu_classificacao int not null 
); 

CREATE SEQUENCE seq_fases INCREMENT 1 START 1;

CREATE TABLE fases (
	 co_fase int not null  primary key default nextval('seq_fases'::regclass) ,
	 ds_fase varchar(200) not null ,
	 mm_duracao_fase int null ,
	 co_usuario int null 
); 

CREATE SEQUENCE seq_filtros INCREMENT 1 START 1;

CREATE TABLE filtros (
	 co_filtro int not null  primary key default nextval('seq_filtros'::regclass) ,
	 no_filtro varchar(50) not null ,
	 tp_filtro varchar(20) not null ,
	 ds_filtro varchar(100) not null ,
	 no_coluna varchar(100) null ,
	 no_dominio varchar(50) null ,
	 ds_formula varchar(255) null ,
	 ds_condicao varchar(255) null ,
	 tb_join varchar(100) null ,
	 tp_join varchar(50) null ,
	 ds_group varchar(255) null ,
	 is_limpar int null ,
	 is_operador int null 
); 

CREATE SEQUENCE seq_fiscais INCREMENT 1 START 1;

CREATE TABLE fiscais (
	 co_fiscal int not null  primary key default nextval('seq_fiscais'::regclass) ,
	 co_setor int not null ,
	 nu_cpf_fiscal varchar(11) not null ,
	 no_fiscal varchar(100) not null ,
	 ds_email varchar(255) not null ,
	 nu_siape int not null ,
	 nu_portaria int null ,
	 nu_telefone varchar(10) not null ,
	 st_fiscal varchar(1) not null ,
	 ds_observacao varchar(255) not null 
); 

CREATE SEQUENCE seq_fluxos INCREMENT 1 START 1;

CREATE TABLE fluxos (
	 nu_sequencia int not null  primary key default nextval('seq_fluxos'::regclass) ,
	 tp_operador int null ,
	 co_fase int null ,
	 co_setor int null ,
	 nu_sequencia_um int null ,
	 nu_sequencia_dois int null 
); 

CREATE SEQUENCE seq_fornecedores INCREMENT 1 START 1;

CREATE TABLE fornecedores (
	 co_fornecedor int not null  primary key default nextval('seq_fornecedores'::regclass) ,
	 tp_fornecedor varchar(1) null ,
	 nu_cnpj varchar(14) not null ,
	 no_razao_social varchar(100) not null ,
	 no_nome_fantasia varchar(100) null ,
	 ds_endereco varchar(100) null ,
	 nu_endereco varchar(15) null ,
	 ds_bairro varchar(255) null ,
	 ds_complemento varchar(255) null ,
	 nu_cep varchar(10) null ,
	 co_municipio int null ,
	 ds_municipio varchar(255) null ,
	 sg_uf varchar(2) null ,
	 ds_email varchar(50) null ,
	 nu_telefone varchar(15) null ,
	 nu_fax varchar(15) null ,
	 no_responsavel varchar(100) null ,
	 nu_cpf_responsavel varchar(11) null ,
	 nu_rg_responsavel varchar(30) null ,
	 no_preposto varchar(100) null ,
	 co_banco int null ,
	 nu_agencia varchar(6) null ,
	 nu_conta varchar(15) null ,
	 dt_inclusao timestamp null ,
	 co_area int null ,
	 co_sub_area int null ,
	 ds_area varchar(150) null ,
	 ds_subarea varchar(150) null ,
	 ds_observacao varchar(255) null ,
	 co_usuario int null ,
	 tp_sexo varchar(1) null ,
	 tp_raca int null ,
	 dt_nascimento date null ,
	 nu_identidade varchar(30) null ,
	 ds_orgao varchar(15) null ,
	 dt_expedicao date null ,
	 tp_graduacao int null ,
	 sg_uf_origem varchar(2) null ,
	 co_municipio_origem int null ,
	 ic_patrocinio varchar(1) null ,
	 ic_ativo int null ,
	 ds_senha varchar(32) null ,
	 indicador_valor varchar(15) null ,
	 indicador_importancia varchar(15) null ,
	 ocs_psa char(3) null ,
	 is_fornecedor_orgao char(1) null ,
	 bairro varchar(120) null 
); 

CREATE SEQUENCE seq_fornecedores_storage INCREMENT 1 START 1;

CREATE TABLE fornecedores_storage (
	 id int not null  primary key default nextval('seq_fornecedores_storage'::regclass) ,
	 modalidade varchar(70) null 
); 

CREATE SEQUENCE seq_garantias INCREMENT 1 START 1;

CREATE TABLE garantias (
	 co_garantia int not null  primary key default nextval('seq_garantias'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 ds_modalidade varchar(100) not null ,
	 vl_garantia decimal null ,
	 dt_inicio date null ,
	 dt_fim date null ,
	 no_seguradora varchar(100) null ,
	 nu_apolice varchar(50) null ,
	 ds_endereco varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 ds_observacao varchar(255) null 
); 

CREATE SEQUENCE seq_garantias_suporte INCREMENT 1 START 1;

CREATE TABLE garantias_suporte (
	 co_garantia_suporte int not null  primary key default nextval('seq_garantias_suporte'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_serie varchar(100) not null ,
	 dt_inicio date null ,
	 dt_fim date null ,
	 ds_observacao varchar(255) null ,
	 ds_garantia_suporte varchar(255) null 
); 

CREATE SEQUENCE seq_ged_financeiro INCREMENT 1 START 1;

CREATE TABLE ged_financeiro (
	 nr_cgc_cpf varchar(15) not null ,
	 nm_fornecedor varchar(200) null ,
	 num_nf varchar(10) null ,
	 nr_serie varchar(10) null ,
	 dt_quitacao date null ,
	 dt_vencimento date null ,
	 tp_quitacao varchar(2) null ,
	 vl_nf decimal null ,
	 vl_parcela decimal null 
); 

CREATE SEQUENCE seq_graduacao INCREMENT 1 START 1;

CREATE TABLE graduacao (
	 co_graduacao int not null  primary key default nextval('seq_graduacao'::regclass) ,
	 ds_graduacao varchar(50) not null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_historicos INCREMENT 1 START 1;

CREATE TABLE historicos (
	 co_historico int not null  primary key default nextval('seq_historicos'::regclass) ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_evento int null ,
	 co_situacao int null ,
	 co_usuario int null ,
	 no_assunto varchar(50) not null ,
	 ds_historico varchar(100) null ,
	 dt_historico timestamp not null ,
	 ds_observacao varchar(500) not null 
); 

CREATE SEQUENCE seq_inspecao INCREMENT 1 START 1;

CREATE TABLE inspecao (
	 co_inspecao int not null  primary key default nextval('seq_inspecao'::regclass) ,
	 ds_inspecao varchar(50) not null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_instituicoes INCREMENT 1 START 1;

CREATE TABLE instituicoes (
	 co_instituicao int not null  primary key default nextval('seq_instituicoes'::regclass) ,
	 co_instituicao_pai int null ,
	 nu_instituicao int not null ,
	 ds_instituicao varchar(200) not null ,
	 co_usuario int not null 
); 

CREATE SEQUENCE seq_itens_aquisicoes INCREMENT 1 START 1;

CREATE TABLE itens_aquisicoes (
	 co_item_aquisicao int not null  primary key default nextval('seq_itens_aquisicoes'::regclass) ,
	 nu_item_aquisicao int not null ,
	 ds_item_aquisicao varchar(255) not null 
); 

CREATE SEQUENCE seq_layouts INCREMENT 1 START 1;

CREATE TABLE layouts (
	 co_registro int not null  primary key default nextval('seq_layouts'::regclass) ,
	 co_layout varchar(5) not null ,
	 ds_campo varchar(40) not null ,
	 no_tam_campo int not null ,
	 no_ini_campo int not null ,
	 no_fim_campo int not null 
); 

CREATE SEQUENCE seq_liquidacao INCREMENT 1 START 1;

CREATE TABLE liquidacao (
	 co_liquidacao int not null  primary key default nextval('seq_liquidacao'::regclass) ,
	 dt_liquidacao date null ,
	 dt_recebimento_sefin date null ,
	 dt_envio_sefin date null ,
	 co_nota int null ,
	 vl_liquidacao float null ,
	 vl_imposto int null ,
	 co_contrato int null ,
	 ds_liquidacao varchar(400) null ,
	 nu_liquidacao varchar(20) null 
); 

CREATE SEQUENCE seq_locais_especialidades INCREMENT 1 START 1;

CREATE TABLE locais_especialidades (
	 co_locais_atendimento int not null ,
	 co_especialidade int not null 
); 

CREATE SEQUENCE seq_logs INCREMENT 1 START 1;

CREATE TABLE logs (
	 co_log int not null  primary key default nextval('seq_logs'::regclass) ,
	 dt_log timestamp not null ,
	 co_usuario int not null ,
	 tp_acao char(1) not null ,
	 tp_modulo char(3) null ,
	 ds_registro varchar(100) null ,
	 nu_ip varchar(20) not null 
); 

CREATE SEQUENCE seq_meses INCREMENT 1 START 1;

CREATE TABLE meses (
	 id int not null  primary key default nextval('seq_meses'::regclass) ,
	 nome varchar(20) not null 
); 

CREATE SEQUENCE seq_modalidades INCREMENT 1 START 1;

CREATE TABLE modalidades (
	 co_modalidade int not null  primary key default nextval('seq_modalidades'::regclass) ,
	 nu_modalidade int null ,
	 ds_modalidade varchar(100) not null ,
	 tp_modalidade int null ,
	 tp_prova int null ,
	 co_usuario int null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_modalidades_confederacoes INCREMENT 1 START 1;

CREATE TABLE modalidades_confederacoes (
	 co_modalidade_confederacao int not null  primary key default nextval('seq_modalidades_confederacoes'::regclass) ,
	 co_confederacao int not null ,
	 co_modalidade int not null 
); 

CREATE SEQUENCE seq_movimentos INCREMENT 1 START 1;

CREATE TABLE movimentos (
	 co_movimento int not null  primary key default nextval('seq_movimentos'::regclass) ,
	 cs_sucesso char(1) not null ,
	 dt_inicio timestamp not null ,
	 dt_fim timestamp null ,
	 ds_nome_arquivo varchar(100) not null 
); 

CREATE SEQUENCE seq_municipios INCREMENT 1 START 1;

CREATE TABLE municipios (
	 co_municipio int not null  primary key default nextval('seq_municipios'::regclass) ,
	 sg_uf varchar(2) not null ,
	 ds_municipio varchar(100) not null 
); 

CREATE SEQUENCE seq_notas_empenhos INCREMENT 1 START 1;

CREATE TABLE notas_empenhos (
	 co_nota_empenho int not null  primary key default nextval('seq_notas_empenhos'::regclass) ,
	 co_empenho int not null ,
	 co_nota int not null ,
	 co_usuario int null ,
	 dt_nota_empenho date not null 
); 

CREATE SEQUENCE seq_notas_fiscais INCREMENT 1 START 1;

CREATE TABLE notas_fiscais (
	 co_nota int not null  primary key default nextval('seq_notas_fiscais'::regclass) ,
	 co_contrato int not null ,
	 assinatura_id int null ,
	 nu_nota varchar(15) not null ,
	 nu_serie varchar(4) null ,
	 dt_recebimento date not null ,
	 dt_envio date null ,
	 vl_nota decimal not null ,
	 vl_glosa decimal null ,
	 ds_nota varchar(500) null ,
	 ic_atesto int null ,
	 dt_atesto date null ,
	 ds_atesto varchar(500) null ,
	 co_usuario int not null ,
	 dt_nota date null ,
	 dt_vencimento date null 
); 

CREATE SEQUENCE seq_notas_pagamentos INCREMENT 1 START 1;

CREATE TABLE notas_pagamentos (
	 co_nota_pagamento int not null  primary key default nextval('seq_notas_pagamentos'::regclass) ,
	 co_pagamento int not null ,
	 co_nota int not null ,
	 co_usuario int null 
); 

CREATE SEQUENCE seq_numeracao_contrato INCREMENT 1 START 1;

CREATE TABLE numeracao_contrato (
	 aa_contrato bigint not null  primary key default nextval('seq_numeracao_contrato'::regclass) ,
	 ct_contrato int not null 
); 

CREATE SEQUENCE seq_numeracao_empenho INCREMENT 1 START 1;

CREATE TABLE numeracao_empenho (
	 aa_empenho bigint not null  primary key default nextval('seq_numeracao_empenho'::regclass) ,
	 ct_empenho int not null 
); 

CREATE SEQUENCE seq_numeracao_pam INCREMENT 1 START 1;

CREATE TABLE numeracao_pam (
	 aa_pam bigint not null  primary key default nextval('seq_numeracao_pam'::regclass) ,
	 ct_pam int not null 
); 

CREATE SEQUENCE seq_numeracao_processo INCREMENT 1 START 1;

CREATE TABLE numeracao_processo (
	 aa_processo bigint not null  primary key default nextval('seq_numeracao_processo'::regclass) ,
	 ct_processo int not null 
); 

CREATE SEQUENCE seq_oficios INCREMENT 1 START 1;

CREATE TABLE oficios (
	 co_oficio int not null  primary key default nextval('seq_oficios'::regclass) ,
	 co_contrato int not null ,
	 co_usuario int null ,
	 nu_oficio varchar(100) not null ,
	 dt_recebimento date null ,
	 dt_resposta date null ,
	 pz_resposta int null 
); 

CREATE SEQUENCE seq_ordem_fornecimento_servico INCREMENT 1 START 1;

CREATE TABLE ordem_fornecimento_servico (
	 co_ordem_fornecimento_servico int not null  primary key default nextval('seq_ordem_fornecimento_servico'::regclass) ,
	 co_contrato int null ,
	 nu_ordem int null ,
	 dt_emissao date null ,
	 dt_inicial date null ,
	 dt_final date null ,
	 dc_descricao varchar(500) null ,
	 co_usuario int null 
); 

CREATE SEQUENCE seq_pagamentos INCREMENT 1 START 1;

CREATE TABLE pagamentos (
	 co_pagamento int not null  primary key default nextval('seq_pagamentos'::regclass) ,
	 co_empenho int null ,
	 co_contrato int not null ,
	 co_atividade int null ,
	 nu_mes_pagamento varchar(2) not null ,
	 nu_ano_pagameto varchar(4) not null ,
	 dt_vencimento date null ,
	 dt_pagamento date null ,
	 dt_financeiro date null ,
	 dt_administrativo date null ,
	 nu_nota_fiscal varchar(20) null ,
	 nu_serie_nf varchar(3) null ,
	 vl_nota decimal null ,
	 vl_imposto decimal null ,
	 vl_liquido decimal null ,
	 vl_pago decimal null ,
	 vl_pagamento decimal not null ,
	 ds_observacao varchar(255) null ,
	 co_usuario int null ,
	 vl_orcamento double precision null 
); 

CREATE SEQUENCE seq_paises INCREMENT 1 START 1;

CREATE TABLE paises (
	 co_pais varchar(2) not null  primary key default nextval('seq_paises'::regclass) ,
	 no_pais varchar(64) not null 
); 

CREATE SEQUENCE seq_pedidos_itens INCREMENT 1 START 1;

CREATE TABLE pedidos_itens (
	 co_pedido_item int not null  primary key default nextval('seq_pedidos_itens'::regclass) ,
	 co_ata_pedido int not null ,
	 co_ata_item int not null ,
	 co_ata_lote int null ,
	 nu_item int not null ,
	 ds_marca varchar(255) null ,
	 ds_descricao varchar(500) not null ,
	 ds_unidade varchar(20) not null ,
	 qt_pedido int not null ,
	 vl_unitario decimal not null 
); 

CREATE SEQUENCE seq_penalidades INCREMENT 1 START 1;

CREATE TABLE penalidades (
	 co_penalidade int not null  primary key default nextval('seq_penalidades'::regclass) ,
	 co_contrato int not null ,
	 co_nota int null ,
	 dt_penalidade date not null ,
	 ds_observacao varchar(255) null ,
	 ic_situacao int not null ,
	 mt_penalidade varchar(255) null ,
	 co_usuario int not null 
); 

CREATE SEQUENCE seq_pendencias INCREMENT 1 START 1;

CREATE TABLE pendencias (
	 co_pendencia int not null  primary key default nextval('seq_pendencias'::regclass) ,
	 co_ata int null ,
	 co_contrato int null ,
	 co_situacao int null ,
	 ds_pendencia varchar(255) not null ,
	 dt_inicio date not null ,
	 dt_fim date null ,
	 st_pendencia varchar(1) not null ,
	 ds_observacao varchar(500) null ,
	 co_usuario int null ,
	 ds_impacto_pagamento int null 
); 

CREATE SEQUENCE seq_pre_empenho INCREMENT 1 START 1;

CREATE TABLE pre_empenho (
	 co_pre_empenho int not null  primary key default nextval('seq_pre_empenho'::regclass) ,
	 it_da_transacao varchar(8) null ,
	 gr_ug_gestao_an_numero_peuq varchar(23) null ,
	 it_da_emissao varchar(8) null ,
	 it_da_limite varchar(8) null ,
	 it_co_ug_favorecida int null ,
	 it_co_gestao_favorecida int null ,
	 it_tx_observacao varchar(255) null ,
	 gr_codigo_evento int null ,
	 it_in_esfera_orcamentaria int null ,
	 it_co_programa_trabalho_resumido int null ,
	 gr_fonte_recurso varchar(10) null ,
	 gr_natureza_despesa int null ,
	 it_co_ug_responsavel int null ,
	 it_co_plano_interno varchar(11) null ,
	 it_va_transacao decimal null ,
	 it_co_ug_doc_referencia int null ,
	 it_co_gestao_doc_referencia int null ,
	 it_me_lancamento int null ,
	 it_in_saldo int null ,
	 it_in_cancelamento_pe int null ,
	 it_sq_pe_original int null ,
	 it_sq_pe_cancelamento int null ,
	 it_qt_lancamento int null ,
	 it_op_cambial int null ,
	 co_contrato int null ,
	 nu_pre_empenho varchar(25) null ,
	 ds_pre_empenho varchar(255) null ,
	 vl_pre_empenho double precision null ,
	 ds_fonte_recurso varchar(80) null ,
	 dt_pre_empenho date null ,
	 nu_natureza_despesa int null ,
	 ic_ativo int null 
); 

CREATE SEQUENCE seq_privilegios INCREMENT 1 START 1;

CREATE TABLE privilegios (
	 co_privilegio int not null  primary key default nextval('seq_privilegios'::regclass) ,
	 sg_privilegio varchar(15) not null ,
	 ds_privilegio varchar(50) not null 
); 

CREATE SEQUENCE seq_produtos INCREMENT 1 START 1;

CREATE TABLE produtos (
	 co_produto int not null  primary key default nextval('seq_produtos'::regclass) ,
	 co_contrato int not null ,
	 co_produto_tipo int not null ,
	 co_usuario int null ,
	 ds_produto varchar(100) not null ,
	 nu_serie varchar(100) not null ,
	 ds_garantia varchar(100) not null ,
	 ds_fabricante varchar(100) not null ,
	 produto_recebido int not null 
); 

CREATE SEQUENCE seq_produtos_tipos INCREMENT 1 START 1;

CREATE TABLE produtos_tipos (
	 co_produto_tipo int not null  primary key default nextval('seq_produtos_tipos'::regclass) ,
	 co_usuario int null ,
	 ds_tipo varchar(255) not null 
); 

CREATE SEQUENCE seq_projetos INCREMENT 1 START 1;

CREATE TABLE projetos (
	 co_projeto int not null  primary key default nextval('seq_projetos'::regclass) ,
	 no_projeto varchar(30) not null ,
	 ds_projeto varchar(300) not null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_provas INCREMENT 1 START 1;

CREATE TABLE provas (
	 co_prova int not null  primary key default nextval('seq_provas'::regclass) ,
	 co_modalidade int not null ,
	 ds_prova varchar(500) not null ,
	 is_sexo char(1) null 
); 

CREATE SEQUENCE seq_provas_categorias INCREMENT 1 START 1;

CREATE TABLE provas_categorias (
	 co_prova_categoria int not null  primary key default nextval('seq_provas_categorias'::regclass) ,
	 co_prova int not null ,
	 ds_prova_categoria varchar(255) not null 
); 

CREATE SEQUENCE seq_provas_classificacoes INCREMENT 1 START 1;

CREATE TABLE provas_classificacoes (
	 co_prova_classificacao int not null  primary key default nextval('seq_provas_classificacoes'::regclass) ,
	 co_prova int not null ,
	 ds_prova_classificacao varchar(255) not null 
); 

CREATE SEQUENCE seq_provas_subcategorias INCREMENT 1 START 1;

CREATE TABLE provas_subcategorias (
	 co_prova_subcategoria int not null  primary key default nextval('seq_provas_subcategorias'::regclass) ,
	 co_prova int not null ,
	 ds_prova_subcategoria varchar(255) not null 
); 

CREATE SEQUENCE seq_rotinas INCREMENT 1 START 1;

CREATE TABLE rotinas (
	 co_rotina int not null  primary key default nextval('seq_rotinas'::regclass) ,
	 tp_rotina varchar(2) not null ,
	 dt_inicio timestamp not null ,
	 dt_fim timestamp null ,
	 ds_resultado varchar(500) null 
); 

CREATE SEQUENCE seq_servicos INCREMENT 1 START 1;

CREATE TABLE servicos (
	 co_servico int not null  primary key default nextval('seq_servicos'::regclass) ,
	 ds_servico varchar(255) not null 
); 

CREATE SEQUENCE seq_setores INCREMENT 1 START 1;

CREATE TABLE setores (
	 co_setor int not null  primary key default nextval('seq_setores'::regclass) ,
	 parent_id int null ,
	 ds_setor varchar(100) not null ,
	 ds_email varchar(255) null ,
	 nu_telefone varchar(15) null ,
	 ic_setor varchar(1) null ,
	 nu_setor varchar(14) null ,
	 ic_cob varchar(1) null ,
	 ic_cpb varchar(1) null ,
	 no_contrato varchar(255) null ,
	 nu_celular varchar(15) null ,
	 ds_endereco varchar(255) null ,
	 ds_complemento varchar(100) null ,
	 ds_bairro varchar(255) null ,
	 sg_uf varchar(2) null ,
	 co_municipio int null ,
	 nu_cep varchar(10) null ,
	 co_usuario int null ,
	 ic_ativo int not null ,
	 no_responsavel varchar(255) null ,
	 nu_cpf varchar(11) null 
); 

CREATE SEQUENCE seq_siafi_arquivo INCREMENT 1 START 1;

CREATE TABLE siafi_arquivo (
	 id int not null  primary key default nextval('seq_siafi_arquivo'::regclass) ,
	 nome varchar(100) null ,
	 data timestamp not null 
); 

CREATE SEQUENCE seq_siafi_ne INCREMENT 1 START 1;

CREATE TABLE siafi_ne (
	 id int not null  primary key default nextval('seq_siafi_ne'::regclass) ,
	 id_arquivo int not null ,
	 it_co_usuario varchar(11) null ,
	 it_co_terminal_usuario varchar(8) null ,
	 it_da_transacao varchar(8) null ,
	 it_ho_transacao int null ,
	 it_co_ug_operador int null ,
	 gr_ug_gestao_an_numero_neuq_1 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_2 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_3 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_4 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_5 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_6 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_7 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_8 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_9 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_10 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_11 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_12 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_13 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_14 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_15 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_16 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_17 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_18 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_19 varchar(23) null ,
	 gr_ug_gestao_an_numero_neuq_20 varchar(23) null ,
	 gr_an_nu_documento_referencia varchar(12) null ,
	 it_da_emissao varchar(8) null ,
	 it_in_favorecido int null ,
	 it_co_favorecido varchar(14) null ,
	 it_tx_observacao varchar(234) null ,
	 gr_codigo_evento int null ,
	 it_in_esfera_orcamentaria int null ,
	 it_co_programa_trabalho_resumido int null ,
	 gr_fonte_recurso varchar(10) null ,
	 gr_natureza_despesa int null ,
	 it_co_ug_responsavel int null ,
	 it_co_plano_interno varchar(11) null ,
	 it_va_transacao int null ,
	 it_in_modalidade_licitacao int null ,
	 it_in_empenho int null ,
	 it_tx_referencia_dispensa varchar(20) null ,
	 it_in_origem_material int null ,
	 it_nu_processo varchar(20) null ,
	 it_co_uf_beneficiada varchar(2) null ,
	 it_co_municipio_beneficiado int null ,
	 it_co_inciso varchar(2) null ,
	 it_tx_amparo_legal varchar(8) null ,
	 it_co_ug_doc_referencia int null ,
	 it_co_gestao_doc_referencia int null ,
	 it_in_emissao int null ,
	 it_me_lancamento int null ,
	 it_va_cronograma_1 int null ,
	 it_va_cronograma_2 int null ,
	 it_va_cronograma_3 int null ,
	 it_va_cronograma_4 int null ,
	 it_va_cronograma_5 int null ,
	 it_va_cronograma_6 int null ,
	 it_va_cronograma_7 int null ,
	 it_va_cronograma_8 int null ,
	 it_va_cronograma_9 int null ,
	 it_va_cronograma_10 int null ,
	 it_va_cronograma_11 int null ,
	 it_va_cronograma_12 int null ,
	 it_va_cronograma_13 int null ,
	 it_va_cronograma_14 int null ,
	 it_va_cronograma_15 int null ,
	 it_va_cronograma_16 int null ,
	 it_va_cronograma_17 int null ,
	 it_va_cronograma_18 int null ,
	 it_va_cronograma_19 int null ,
	 it_va_cronograma_20 int null ,
	 it_co_sistema_origem varchar(10) null ,
	 it_di_cronograma_1 int null ,
	 it_di_cronograma_2 int null ,
	 it_di_cronograma_3 int null ,
	 it_di_cronograma_4 int null ,
	 it_di_cronograma_5 int null ,
	 it_di_cronograma_6 int null ,
	 it_di_cronograma_7 int null ,
	 it_di_cronograma_8 int null ,
	 it_di_cronograma_9 int null ,
	 it_di_cronograma_10 int null ,
	 it_di_cronograma_11 int null ,
	 it_di_cronograma_12 int null ,
	 it_di_cronograma_13 int null ,
	 it_di_cronograma_14 int null ,
	 it_di_cronograma_15 int null ,
	 it_di_cronograma_16 int null ,
	 it_di_cronograma_17 int null ,
	 it_di_cronograma_18 int null ,
	 it_di_cronograma_19 int null ,
	 it_di_cronograma_20 int null ,
	 it_in_contra_entrega_ne int null ,
	 it_in_situacao_credor_sicaf int null ,
	 it_da_vencimento_1 varchar(8) null ,
	 it_da_vencimento_2 varchar(8) null ,
	 it_da_vencimento_3 varchar(8) null ,
	 it_da_vencimento_4 varchar(8) null ,
	 it_da_vencimento_5 varchar(8) null ,
	 it_da_vencimento_6 varchar(8) null ,
	 it_da_vencimento_7 varchar(8) null ,
	 it_da_vencimento_8 varchar(8) null ,
	 it_da_vencimento_9 varchar(8) null ,
	 it_da_vencimento_10 varchar(8) null ,
	 it_da_vencimento_11 varchar(8) null ,
	 it_da_vencimento_12 varchar(8) null ,
	 it_da_vencimento_13 varchar(8) null ,
	 it_da_vencimento_14 varchar(8) null ,
	 it_da_vencimento_15 varchar(8) null ,
	 it_da_vencimento_16 varchar(8) null ,
	 it_da_vencimento_17 varchar(8) null ,
	 it_da_vencimento_18 varchar(8) null ,
	 it_da_vencimento_19 varchar(8) null ,
	 it_da_vencimento_20 varchar(8) null ,
	 it_da_pagamento_1 varchar(8) null ,
	 it_da_pagamento_2 varchar(8) null ,
	 it_da_pagamento_3 varchar(8) null ,
	 it_da_pagamento_4 varchar(8) null ,
	 it_da_pagamento_5 varchar(8) null ,
	 it_da_pagamento_6 varchar(8) null ,
	 it_da_pagamento_7 varchar(8) null ,
	 it_da_pagamento_8 varchar(8) null ,
	 it_da_pagamento_9 varchar(8) null ,
	 it_da_pagamento_10 varchar(8) null ,
	 it_da_pagamento_11 varchar(8) null ,
	 it_da_pagamento_12 varchar(8) null ,
	 it_da_pagamento_13 varchar(8) null ,
	 it_da_pagamento_14 varchar(8) null ,
	 it_da_pagamento_15 varchar(8) null ,
	 it_da_pagamento_16 varchar(8) null ,
	 it_da_pagamento_17 varchar(8) null ,
	 it_da_pagamento_18 varchar(8) null ,
	 it_da_pagamento_19 varchar(8) null ,
	 it_da_pagamento_20 varchar(8) null ,
	 it_va_cronogramado_1 int null ,
	 it_va_cronogramado_2 int null ,
	 it_va_cronogramado_3 int null ,
	 it_va_cronogramado_4 int null ,
	 it_va_cronogramado_5 int null ,
	 it_va_cronogramado_6 int null ,
	 it_va_cronogramado_7 int null ,
	 it_va_cronogramado_8 int null ,
	 it_va_cronogramado_9 int null ,
	 it_va_cronogramado_10 int null ,
	 it_va_cronogramado_11 int null ,
	 it_va_cronogramado_12 int null ,
	 it_va_cronogramado_13 int null ,
	 it_va_cronogramado_14 int null ,
	 it_va_cronogramado_15 int null ,
	 it_va_cronogramado_16 int null ,
	 it_va_cronogramado_17 int null ,
	 it_va_cronogramado_18 int null ,
	 it_va_cronogramado_19 int null ,
	 it_va_cronogramado_20 int null ,
	 it_qt_lancamento int null ,
	 it_co_msg_documento int null ,
	 it_nu_precatorio varchar(20) null ,
	 it_in_pagamento_precatorio varchar(1) null ,
	 it_nu_original varchar(20) null ,
	 it_da_atualizacao varchar(8) null ,
	 it_in_atualizacao int null ,
	 it_nu_lista_1 varchar(12) null ,
	 it_nu_lista_2 varchar(12) null ,
	 it_nu_lista_3 varchar(12) null ,
	 it_nu_lista_4 varchar(12) null ,
	 it_nu_lista_5 varchar(12) null ,
	 it_nu_lista_6 varchar(12) null ,
	 it_nu_lista_7 varchar(12) null ,
	 it_nu_lista_8 varchar(12) null ,
	 it_nu_lista_9 varchar(12) null ,
	 it_nu_lista_10 varchar(12) null ,
	 it_nu_lista_11 varchar(12) null ,
	 it_nu_lista_12 varchar(12) null ,
	 it_nu_lista_13 varchar(12) null ,
	 it_nu_lista_14 varchar(12) null ,
	 it_nu_lista_15 varchar(12) null ,
	 it_nu_lista_16 varchar(12) null ,
	 it_nu_lista_17 varchar(12) null ,
	 it_nu_lista_18 varchar(12) null ,
	 it_nu_lista_19 varchar(12) null ,
	 it_nu_lista_20 varchar(12) null ,
	 it_in_liquidacao int null ,
	 it_op_cambial int null ,
	 gr_an_numero_doc_transferencia_1 varchar(12) null ,
	 gr_an_numero_doc_transferencia_2 varchar(12) null ,
	 gr_an_numero_doc_transferencia_3 varchar(12) null ,
	 gr_an_numero_doc_transferencia_4 varchar(12) null ,
	 gr_an_numero_doc_transferencia_5 varchar(12) null ,
	 gr_an_numero_doc_transferencia_6 varchar(12) null ,
	 gr_an_numero_doc_transferencia_7 varchar(12) null ,
	 gr_an_numero_doc_transferencia_8 varchar(12) null ,
	 gr_an_numero_doc_transferencia_9 varchar(12) null ,
	 gr_an_numero_doc_transferencia_10 varchar(12) null ,
	 gr_an_numero_doc_transferencia_11 varchar(12) null ,
	 gr_an_numero_doc_transferencia_12 varchar(12) null ,
	 gr_an_numero_doc_transferencia_13 varchar(12) null ,
	 gr_an_numero_doc_transferencia_14 varchar(12) null ,
	 gr_an_numero_doc_transferencia_15 varchar(12) null ,
	 gr_an_numero_doc_transferencia_16 varchar(12) null ,
	 gr_an_numero_doc_transferencia_17 varchar(12) null ,
	 gr_an_numero_doc_transferencia_18 varchar(12) null ,
	 gr_an_numero_doc_transferencia_19 varchar(12) null ,
	 gr_an_numero_doc_transferencia_20 varchar(12) null ,
	 it_in_estorno_cancelamento int null ,
	 gr_conta_passivo varchar(9) null ,
	 it_co_conta_corrente_permanente_1 varchar(78) null ,
	 it_co_conta_corrente_permanente_2 varchar(78) null ,
	 it_co_conta_corrente_permanente_3 varchar(78) null ,
	 it_co_conta_corrente_permanente_4 varchar(78) null ,
	 it_co_conta_corrente_permanente_5 varchar(78) null ,
	 it_co_conta_corrente_permanente_6 varchar(78) null ,
	 it_co_conta_corrente_permanente_7 varchar(78) null ,
	 it_co_conta_corrente_permanente_8 varchar(78) null ,
	 it_co_conta_corrente_permanente_9 varchar(78) null ,
	 it_co_conta_corrente_permanente_10 varchar(78) null ,
	 it_co_conta_corrente_permanente_11 varchar(78) null ,
	 it_co_conta_corrente_permanente_12 varchar(78) null ,
	 it_co_conta_corrente_permanente_13 varchar(78) null ,
	 it_co_conta_corrente_permanente_14 varchar(78) null ,
	 it_co_conta_corrente_permanente_15 varchar(78) null ,
	 it_co_conta_corrente_permanente_16 varchar(78) null ,
	 it_co_conta_corrente_permanente_17 varchar(78) null ,
	 it_co_conta_corrente_permanente_18 varchar(78) null ,
	 it_co_conta_corrente_permanente_19 varchar(78) null ,
	 it_co_conta_corrente_permanente_20 varchar(78) null ,
	 it_va_conta_corrente_permanente_1 int null ,
	 it_va_conta_corrente_permanente_2 int null ,
	 it_va_conta_corrente_permanente_3 int null ,
	 it_va_conta_corrente_permanente_4 int null ,
	 it_va_conta_corrente_permanente_5 int null ,
	 it_va_conta_corrente_permanente_6 int null ,
	 it_va_conta_corrente_permanente_7 int null ,
	 it_va_conta_corrente_permanente_8 int null ,
	 it_va_conta_corrente_permanente_9 int null ,
	 it_va_conta_corrente_permanente_10 int null ,
	 it_va_conta_corrente_permanente_11 int null ,
	 it_va_conta_corrente_permanente_12 int null ,
	 it_va_conta_corrente_permanente_13 int null ,
	 it_va_conta_corrente_permanente_14 int null ,
	 it_va_conta_corrente_permanente_15 int null ,
	 it_va_conta_corrente_permanente_16 int null ,
	 it_va_conta_corrente_permanente_17 int null ,
	 it_va_conta_corrente_permanente_18 int null ,
	 it_va_conta_corrente_permanente_19 int null ,
	 it_va_conta_corrente_permanente_20 int null ,
	 it_co_conta_corrente_financeiro varchar(78) null ,
	 it_tp_transferencia_doc varchar(2) null 
); 

CREATE SEQUENCE seq_siafi_nl INCREMENT 1 START 1;

CREATE TABLE siafi_nl (
	 id int not null  primary key default nextval('seq_siafi_nl'::regclass) ,
	 id_arquivo int not null ,
	 it_co_usuario varchar(11) null ,
	 it_co_terminal_usuario varchar(8) null ,
	 it_da_transacao varchar(8) null ,
	 it_ho_transacao int null ,
	 it_co_ug_operador int null ,
	 gr_ug_gestao_an_numero_nluq varchar(23) null ,
	 it_da_emissao varchar(8) null ,
	 it_da_valorizacao varchar(8) null ,
	 it_co_titulo_credito varchar(12) null ,
	 it_da_vencimento_titulo_credito varchar(8) null ,
	 it_op_cambial int null ,
	 it_in_inversao_saldo_doc int null ,
	 it_tx_observacao varchar(234) null ,
	 it_tx_observacao_documento_1 varchar(78) null ,
	 it_tx_observacao_documento_2 varchar(78) null ,
	 it_tx_observacao_documento_3 varchar(78) null ,
	 it_tx_observacao_documento_4 varchar(78) null ,
	 it_tx_observacao_documento_5 varchar(78) null ,
	 it_tx_observacao_documento_6 varchar(78) null ,
	 it_tx_observacao_documento_7 varchar(78) null ,
	 it_tx_observacao_documento_8 varchar(78) null ,
	 it_tx_observacao_documento_9 varchar(78) null ,
	 it_tx_observacao_documento_10 varchar(78) null ,
	 it_tx_observacao_documento_11 varchar(78) null ,
	 it_tx_observacao_documento_12 varchar(78) null ,
	 it_tx_observacao_documento_13 varchar(78) null ,
	 it_tx_observacao_documento_14 varchar(78) null ,
	 it_tx_observacao_documento_15 varchar(78) null ,
	 it_tx_observacao_documento_16 varchar(78) null ,
	 it_tx_observacao_documento_17 varchar(78) null ,
	 it_tx_observacao_documento_18 varchar(78) null ,
	 it_tx_observacao_documento_19 varchar(78) null ,
	 it_tx_observacao_documento_20 varchar(78) null ,
	 it_in_favorecido int null ,
	 it_co_favorecido varchar(14) null ,
	 gr_codigo_evento_1 int null ,
	 gr_codigo_evento_2 int null ,
	 gr_codigo_evento_3 int null ,
	 gr_codigo_evento_4 int null ,
	 gr_codigo_evento_5 int null ,
	 gr_codigo_evento_6 int null ,
	 gr_codigo_evento_7 int null ,
	 gr_codigo_evento_8 int null ,
	 gr_codigo_evento_9 int null ,
	 gr_codigo_evento_10 int null ,
	 gr_codigo_evento_11 int null ,
	 gr_codigo_evento_12 int null ,
	 gr_codigo_evento_13 int null ,
	 gr_codigo_evento_14 int null ,
	 gr_codigo_evento_15 int null ,
	 gr_codigo_evento_16 int null ,
	 gr_codigo_evento_17 int null ,
	 gr_codigo_evento_18 int null ,
	 gr_codigo_evento_19 int null ,
	 gr_codigo_evento_20 int null ,
	 it_co_inscricao1_1 varchar(14) null ,
	 it_co_inscricao1_2 varchar(14) null ,
	 it_co_inscricao1_3 varchar(14) null ,
	 it_co_inscricao1_4 varchar(14) null ,
	 it_co_inscricao1_5 varchar(14) null ,
	 it_co_inscricao1_6 varchar(14) null ,
	 it_co_inscricao1_7 varchar(14) null ,
	 it_co_inscricao1_8 varchar(14) null ,
	 it_co_inscricao1_9 varchar(14) null ,
	 it_co_inscricao1_10 varchar(14) null ,
	 it_co_inscricao1_11 varchar(14) null ,
	 it_co_inscricao1_12 varchar(14) null ,
	 it_co_inscricao1_13 varchar(14) null ,
	 it_co_inscricao1_14 varchar(14) null ,
	 it_co_inscricao1_15 varchar(14) null ,
	 it_co_inscricao1_16 varchar(14) null ,
	 it_co_inscricao1_17 varchar(14) null ,
	 it_co_inscricao1_18 varchar(14) null ,
	 it_co_inscricao1_19 varchar(14) null ,
	 it_co_inscricao1_20 varchar(14) null ,
	 it_co_inscricao2_1 varchar(14) null ,
	 it_co_inscricao2_2 varchar(14) null ,
	 it_co_inscricao2_3 varchar(14) null ,
	 it_co_inscricao2_4 varchar(14) null ,
	 it_co_inscricao2_5 varchar(14) null ,
	 it_co_inscricao2_6 varchar(14) null ,
	 it_co_inscricao2_7 varchar(14) null ,
	 it_co_inscricao2_8 varchar(14) null ,
	 it_co_inscricao2_9 varchar(14) null ,
	 it_co_inscricao2_10 varchar(14) null ,
	 it_co_inscricao2_11 varchar(14) null ,
	 it_co_inscricao2_12 varchar(14) null ,
	 it_co_inscricao2_13 varchar(14) null ,
	 it_co_inscricao2_14 varchar(14) null ,
	 it_co_inscricao2_15 varchar(14) null ,
	 it_co_inscricao2_16 varchar(14) null ,
	 it_co_inscricao2_17 varchar(14) null ,
	 it_co_inscricao2_18 varchar(14) null ,
	 it_co_inscricao2_19 varchar(14) null ,
	 it_co_inscricao2_20 varchar(14) null ,
	 gr_classificacao1_1 int null ,
	 gr_classificacao1_2 int null ,
	 gr_classificacao1_3 int null ,
	 gr_classificacao1_4 int null ,
	 gr_classificacao1_5 int null ,
	 gr_classificacao1_6 int null ,
	 gr_classificacao1_7 int null ,
	 gr_classificacao1_8 int null ,
	 gr_classificacao1_9 int null ,
	 gr_classificacao1_10 int null ,
	 gr_classificacao1_11 int null ,
	 gr_classificacao1_12 int null ,
	 gr_classificacao1_13 int null ,
	 gr_classificacao1_14 int null ,
	 gr_classificacao1_15 int null ,
	 gr_classificacao1_16 int null ,
	 gr_classificacao1_17 int null ,
	 gr_classificacao1_18 int null ,
	 gr_classificacao1_19 int null ,
	 gr_classificacao1_20 int null ,
	 gr_classificacao2_1 int null ,
	 gr_classificacao2_2 int null ,
	 gr_classificacao2_3 int null ,
	 gr_classificacao2_4 int null ,
	 gr_classificacao2_5 int null ,
	 gr_classificacao2_6 int null ,
	 gr_classificacao2_7 int null ,
	 gr_classificacao2_8 int null ,
	 gr_classificacao2_9 int null ,
	 gr_classificacao2_10 int null ,
	 gr_classificacao2_11 int null ,
	 gr_classificacao2_12 int null ,
	 gr_classificacao2_13 int null ,
	 gr_classificacao2_14 int null ,
	 gr_classificacao2_15 int null ,
	 gr_classificacao2_16 int null ,
	 gr_classificacao2_17 int null ,
	 gr_classificacao2_18 int null ,
	 gr_classificacao2_19 int null ,
	 gr_classificacao2_20 int null ,
	 it_co_inscricao01_1 varchar(30) null ,
	 it_co_inscricao01_2 varchar(30) null ,
	 it_co_inscricao01_3 varchar(30) null ,
	 it_co_inscricao01_4 varchar(30) null ,
	 it_co_inscricao01_5 varchar(30) null ,
	 it_co_inscricao01_6 varchar(30) null ,
	 it_co_inscricao01_7 varchar(30) null ,
	 it_co_inscricao01_8 varchar(30) null ,
	 it_co_inscricao01_9 varchar(30) null ,
	 it_co_inscricao01_10 varchar(30) null ,
	 it_co_inscricao01_11 varchar(30) null ,
	 it_co_inscricao01_12 varchar(30) null ,
	 it_co_inscricao01_13 varchar(30) null ,
	 it_co_inscricao01_14 varchar(30) null ,
	 it_co_inscricao01_15 varchar(30) null ,
	 it_co_inscricao01_16 varchar(30) null ,
	 it_co_inscricao01_17 varchar(30) null ,
	 it_co_inscricao01_18 varchar(30) null ,
	 it_co_inscricao01_19 varchar(30) null ,
	 it_co_inscricao01_20 varchar(30) null ,
	 it_co_inscricao02_1 varchar(30) null ,
	 it_co_inscricao02_2 varchar(30) null ,
	 it_co_inscricao02_3 varchar(30) null ,
	 it_co_inscricao02_4 varchar(30) null ,
	 it_co_inscricao02_5 varchar(30) null ,
	 it_co_inscricao02_6 varchar(30) null ,
	 it_co_inscricao02_7 varchar(30) null ,
	 it_co_inscricao02_8 varchar(30) null ,
	 it_co_inscricao02_9 varchar(30) null ,
	 it_co_inscricao02_10 varchar(30) null ,
	 it_co_inscricao02_11 varchar(30) null ,
	 it_co_inscricao02_12 varchar(30) null ,
	 it_co_inscricao02_13 varchar(30) null ,
	 it_co_inscricao02_14 varchar(30) null ,
	 it_co_inscricao02_15 varchar(30) null ,
	 it_co_inscricao02_16 varchar(30) null ,
	 it_co_inscricao02_17 varchar(30) null ,
	 it_co_inscricao02_18 varchar(30) null ,
	 it_co_inscricao02_19 varchar(30) null ,
	 it_co_inscricao02_20 varchar(30) null ,
	 gr_classificacao_orcamentaria1_1 varchar(8) null ,
	 gr_classificacao_orcamentaria1_2 varchar(8) null ,
	 gr_classificacao_orcamentaria1_3 varchar(8) null ,
	 gr_classificacao_orcamentaria1_4 varchar(8) null ,
	 gr_classificacao_orcamentaria1_5 varchar(8) null ,
	 gr_classificacao_orcamentaria1_6 varchar(8) null ,
	 gr_classificacao_orcamentaria1_7 varchar(8) null ,
	 gr_classificacao_orcamentaria1_8 varchar(8) null ,
	 gr_classificacao_orcamentaria1_9 varchar(8) null ,
	 gr_classificacao_orcamentaria1_10 varchar(8) null ,
	 gr_classificacao_orcamentaria1_11 varchar(8) null ,
	 gr_classificacao_orcamentaria1_12 varchar(8) null ,
	 gr_classificacao_orcamentaria1_13 varchar(8) null ,
	 gr_classificacao_orcamentaria1_14 varchar(8) null ,
	 gr_classificacao_orcamentaria1_15 varchar(8) null ,
	 gr_classificacao_orcamentaria1_16 varchar(8) null ,
	 gr_classificacao_orcamentaria1_17 varchar(8) null ,
	 gr_classificacao_orcamentaria1_18 varchar(8) null ,
	 gr_classificacao_orcamentaria1_19 varchar(8) null ,
	 gr_classificacao_orcamentaria1_20 varchar(8) null ,
	 gr_classificacao_orcamentaria2_1 varchar(8) null ,
	 gr_classificacao_orcamentaria2_2 varchar(8) null ,
	 gr_classificacao_orcamentaria2_3 varchar(8) null ,
	 gr_classificacao_orcamentaria2_4 varchar(8) null ,
	 gr_classificacao_orcamentaria2_5 varchar(8) null ,
	 gr_classificacao_orcamentaria2_6 varchar(8) null ,
	 gr_classificacao_orcamentaria2_7 varchar(8) null ,
	 gr_classificacao_orcamentaria2_8 varchar(8) null ,
	 gr_classificacao_orcamentaria2_9 varchar(8) null ,
	 gr_classificacao_orcamentaria2_10 varchar(8) null ,
	 gr_classificacao_orcamentaria2_11 varchar(8) null ,
	 gr_classificacao_orcamentaria2_12 varchar(8) null ,
	 gr_classificacao_orcamentaria2_13 varchar(8) null ,
	 gr_classificacao_orcamentaria2_14 varchar(8) null ,
	 gr_classificacao_orcamentaria2_15 varchar(8) null ,
	 gr_classificacao_orcamentaria2_16 varchar(8) null ,
	 gr_classificacao_orcamentaria2_17 varchar(8) null ,
	 gr_classificacao_orcamentaria2_18 varchar(8) null ,
	 gr_classificacao_orcamentaria2_19 varchar(8) null ,
	 gr_classificacao_orcamentaria2_20 varchar(8) null ,
	 it_va_transacao_1 int null ,
	 it_va_transacao_2 int null ,
	 it_va_transacao_3 int null ,
	 it_va_transacao_4 int null ,
	 it_va_transacao_5 int null ,
	 it_va_transacao_6 int null ,
	 it_va_transacao_7 int null ,
	 it_va_transacao_8 int null ,
	 it_va_transacao_9 int null ,
	 it_va_transacao_10 int null ,
	 it_va_transacao_11 int null ,
	 it_va_transacao_12 int null ,
	 it_va_transacao_13 int null ,
	 it_va_transacao_14 int null ,
	 it_va_transacao_15 int null ,
	 it_va_transacao_16 int null ,
	 it_va_transacao_17 int null ,
	 it_va_transacao_18 int null ,
	 it_va_transacao_19 int null ,
	 it_va_transacao_20 int null ,
	 it_me_lancamento int null ,
	 it_co_sistema_origem varchar(10) null ,
	 it_nu_processo varchar(20) null ,
	 it_qt_lancamento int null ,
	 it_da_leitura_auditor_spb varchar(8) null ,
	 it_co_operacao_spb int null ,
	 it_nu_referencia_1 varchar(17) null ,
	 it_nu_referencia_2 varchar(17) null ,
	 it_nu_referencia_3 varchar(17) null ,
	 it_nu_referencia_4 varchar(17) null ,
	 it_nu_referencia_5 varchar(17) null ,
	 it_nu_referencia_6 varchar(17) null ,
	 it_nu_referencia_7 varchar(17) null ,
	 it_nu_referencia_8 varchar(17) null ,
	 it_nu_referencia_9 varchar(17) null ,
	 it_nu_referencia_10 varchar(17) null ,
	 it_nu_referencia_11 varchar(17) null ,
	 it_nu_referencia_12 varchar(17) null ,
	 it_nu_referencia_13 varchar(17) null ,
	 it_nu_referencia_14 varchar(17) null ,
	 it_nu_referencia_15 varchar(17) null ,
	 it_nu_referencia_16 varchar(17) null ,
	 it_nu_referencia_17 varchar(17) null ,
	 it_nu_referencia_18 varchar(17) null ,
	 it_nu_referencia_19 varchar(17) null ,
	 it_nu_referencia_20 varchar(17) null ,
	 it_in_controle_processo_me int null ,
	 it_co_ug_empenho int null ,
	 it_co_gestao_empenho int null 
); 

CREATE SEQUENCE seq_situacoes INCREMENT 1 START 1;

CREATE TABLE situacoes (
	 co_situacao int not null  primary key default nextval('seq_situacoes'::regclass) ,
	 tp_situacao char(1) not null ,
	 nu_sequencia int not null ,
	 ds_situacao varchar(200) not null ,
	 ic_financeiro int null ,
	 co_usuario int null ,
	 ic_ativo int not null 
); 

CREATE SEQUENCE seq_subcategorias INCREMENT 1 START 1;

CREATE TABLE subcategorias (
	 co_subcategoria int not null  primary key default nextval('seq_subcategorias'::regclass) ,
	 co_categoria int not null ,
	 no_subcategoria varchar(100) not null 
); 

CREATE SEQUENCE seq_tipo_inspecao INCREMENT 1 START 1;

CREATE TABLE tipo_inspecao (
	 co_tipo_inspecao int not null  primary key default nextval('seq_tipo_inspecao'::regclass) ,
	 ds_tipo_inspecao varchar(50) not null 
); 

CREATE SEQUENCE seq_uasgs INCREMENT 1 START 1;

CREATE TABLE uasgs (
	 co_uasg int not null  primary key default nextval('seq_uasgs'::regclass) ,
	 uasg varchar(6) not null 
); 

CREATE SEQUENCE seq_ufs INCREMENT 1 START 1;

CREATE TABLE ufs (
	 sg_uf varchar(2) not null  primary key default nextval('seq_ufs'::regclass) ,
	 no_uf varchar(100) not null 
); 

CREATE SEQUENCE seq_usuarios INCREMENT 1 START 1;

CREATE TABLE usuarios (
	 co_usuario int not null  primary key default nextval('seq_usuarios'::regclass) ,
	 co_privilegio int null ,
	 co_setor int null ,
	 co_gestor int null ,
	 ic_acesso int null ,
	 nu_cpf varchar(14) not null ,
	 ds_nome varchar(100) not null ,
	 no_usuario varchar(100) null ,
	 ds_senha varchar(32) null ,
	 ds_email varchar(100) null ,
	 nu_telefone varchar(15) null ,
	 dt_liberacao timestamp null ,
	 dt_bloqueio timestamp null ,
	 dt_ult_acesso timestamp null ,
	 ds_privilegios varchar(255) null ,
	 co_usuario_cad int null ,
	 ic_ativo int null ,
	 co_instituicoes int null ,
	 ic_campos_pesq_ctr varchar(50) null ,
	 ic_campos_pesq_cte varchar(50) null ,
	 co_graduacao int null ,
	 no_guerra varchar(30) null ,
	 nu_conselho varchar(20) null ,
	 licenca_nominal varchar(1) null ,
	 licenca_concorrente varchar(1) null ,
	 licenca_bi varchar(1) null ,
	 co_funcao int null ,
	 co_cargo int null ,
	 ck_aviso30 int null ,
	 ck_aviso60 int null ,
	 ck_aviso90 int null ,
	 ck_aviso120 int null ,
	 is_logged int not null ,
	 ip_ult_acesso varchar(20) null ,
	 co_projeto int null 
); 