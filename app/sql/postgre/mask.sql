CREATE FUNCTION mask(unformatted_value char(32), format_string char(32)) RETURNS char(32) AS
 $$
 	DECLARE
    	output_len smallint;
    	ini_unf smallint;
    	ini_len smallint;
    	temp_char char;
        input_len int;

    BEGIN
        input_len := length(unformatted_value);
        output_len := length(format_string);
        ini_len := 1;
        ini_unf := 1;

        while ( ini_len <= output_len ) loop

    temp_char := substr(format_string, ini_len, 1);
    if ( temp_char = '#' ) then
        if ( ini_unf <= input_len ) then
            format_string := overlay(format_string placing substr(unformatted_value, ini_unf, 1) from ini_len for 1);
            ini_unf := ini_unf + 1;
        else
            format_string := overlay(format_string placing "0" from ini_len for 1);
        end if;
    end if;

    ini_len := ini_len + 1;
end loop;

return format_string;


  	END;

$$
LANGUAGE 'plpgsql'
