

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema prototipoLicitacao
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Table `prototipoLicitacao`.`tipo_licitacao`
-- -----------------------------------------------------
-- tipos_licitacoes
CREATE TABLE IF NOT EXISTS `tipos_licitacoes` (
  `co_tipo_licitacao` INT NOT NULL AUTO_INCREMENT,
  `no_tipo_licitacao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`co_tipo_licitacao`))
ENGINE = InnoDB;

-- licitacoes
CREATE TABLE IF NOT EXISTS `licitacoes` (
  `co_licitacao` INT NOT NULL AUTO_INCREMENT,
  `co_contratacao` INT NOT NULL,
  `co_tipo_licitacao` INT NOT NULL,
  `co_fornecedor` INT NOT NULL,
  `co_situacao` INT NOT NULL,
  `nu_licitacao` INT NOT NULL,
  `nu_processo` INT NULL,
  `dt_publicacao` DATE NOT NULL,
  `dt_abertura` DATETIME NOT NULL,	
  `vl_estimado` FLOAT NULL,
  `vl_contratado` FLOAT NULL,
  `nu_diferenca` INT NULL,
  `ds_objeto` TEXT NULL,
  `ds_prazo_execucao` VARCHAR(20) NULL,
  `ic_ativo` INT(1) NULL DEFAULT 1,

  PRIMARY KEY (`co_licitacao`),
  INDEX `fk_licitacao_modalidade_licitacao_idx` (`co_contratacao` ASC),
  INDEX `fk_licitacao_tipo_licitacao1_idx` (`co_tipo_licitacao` ASC),
  INDEX `fk_licitacao_situacoes1_idx` (`co_situacao` ASC),
  INDEX `fk_licitacao_fornecedores1_idx` (`co_fornecedor` ASC),
  CONSTRAINT `fk_licitacao_modalidade_licitacao`
    FOREIGN KEY (`co_contratacao`)
    REFERENCES `contratacoes` (`co_contratacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_tipo_licitacao1`
    FOREIGN KEY (`co_tipo_licitacao`)
    REFERENCES `tipos_licitacoes` (`co_tipo_licitacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_situacoes1`
    FOREIGN KEY (`co_situacao`)
    REFERENCES `situacoes` (`co_situacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_licitacao_fornecedores1`
    FOREIGN KEY (`co_fornecedor`)
    REFERENCES `fornecedores` (`co_fornecedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- Alter table anexos
ALTER TABLE `anexos` 
ADD COLUMN `co_licitacao` INT NULL AFTER `ic_indexado`,
ADD INDEX `fk_anexos_licitacoes_idx` (`co_licitacao` ASC);
ALTER TABLE `anexos` 
ADD CONSTRAINT `fk_anexos_licitacoes`
  FOREIGN KEY (`co_licitacao`)
  REFERENCES `licitacoes` (`co_licitacao`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


ALTER TABLE `tipos_licitacoes` 
ADD COLUMN `ic_ativo` INT(1) NULL DEFAULT 1;


-- insert tipos_licitacoes
INSERT INTO `tipos_licitacoes` (`no_tipo_licitacao`) VALUES ('Menor Preço');
INSERT INTO `tipos_licitacoes` (`no_tipo_licitacao`) VALUES ('Melhor Técnica');
INSERT INTO `tipos_licitacoes` (`no_tipo_licitacao`) VALUES ('Melhor Técnica e Menor Preço');
INSERT INTO `tipos_licitacoes` (`no_tipo_licitacao`) VALUES ('Maior Lance');


-- alter table situacoes
ALTER TABLE `situacoes` ADD COLUMN `ic_licitacao` INT(1) NULL DEFAULT 0;

-- alter table contratacoes
ALTER TABLE `contratacoes` ADD COLUMN `ic_ativo` INT(1) NULL DEFAULT 1;
ALTER TABLE `contratacoes` ADD COLUMN `ic_licitacao` INT(1) NULL DEFAULT 0;

-- insert contratacoes
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('PREGÃO PRESENCIAL', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('CONVITE', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('CONCORÊNCIA', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('CONCURSO', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('LEILÃO', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('RDC', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('DISPENSA', 2, 1);
INSERT INTO `contratacoes` (`ds_contratacao`, `at_contratacao`, `ic_licitacao`) VALUES ('REGISTRO DE PREÇO', 2, 1);

-- insert situações
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('SUSPENSO', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('EM ANDAMENTO', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('REALIZADAS', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('REVOGADAS', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('SUSPENSAS', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('FRACASSADAS', 1);
INSERT INTO `situacoes` (`ds_situacao`, `ic_licitacao`) VALUES ('DESERTAS', 1);

-- alter table fornecedores
ALTER TABLE `fornecedores` 
ADD COLUMN `ic_suspenso` INT(1) NULL DEFAULT 0,
ADD COLUMN `ds_penalidade_aplicada` VARCHAR(100),
ADD COLUMN `dt_ini_penalidade` DATE NULL,
ADD COLUMN `dt_fim_penalidade` DATE NULL;

ALTER TABLE `log_fornecedores` 
ADD COLUMN `ic_suspenso` INT(1) NULL DEFAULT 0,
ADD COLUMN `ds_penalidade_aplicada` VARCHAR(100) NULL,
ADD COLUMN `dt_ini_penalidade` DATE NULL,
ADD COLUMN `dt_fim_penalidade` DATE NULL;
