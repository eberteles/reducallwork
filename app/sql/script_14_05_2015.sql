ALTER TABLE usuarios ADD COLUMN ck_aviso30 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE usuarios ADD COLUMN ck_aviso60 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE usuarios ADD COLUMN ck_aviso90 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE usuarios ADD COLUMN ck_aviso120 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';

ALTER TABLE log_usuarios ADD COLUMN ck_aviso30 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE log_usuarios ADD COLUMN ck_aviso60 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE log_usuarios ADD COLUMN ck_aviso90 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';
ALTER TABLE log_usuarios ADD COLUMN ck_aviso120 INT NULL DEFAULT 0 COMMENT 'Campo criado para outros emails que serão alertados de expiração de contratos';