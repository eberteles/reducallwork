ALTER TABLE contratos ADD ic_ativo CHAR(1) NULL DEFAULT 1 after nu_sequencia;
ALTER TABLE contratos ADD ds_motivo TEXT DEFAULT NULL after ic_ativo;

ALTER TABLE log_contratos ADD ic_ativo CHAR(1) NULL DEFAULT 1 after nu_sequencia;
ALTER TABLE log_contratos ADD ds_motivo TEXT DEFAULT NULL after ic_ativo;

INSERT INTO `filtros` (`co_filtro`, `no_filtro`, `tp_filtro`, `ds_filtro`, `no_coluna`, `no_dominio`, `ds_formula`, `ds_condicao`, `tb_join`, `tp_join`, `ds_group`, `is_limpar`, `is_operador`) VALUES
(21, 'contratos_inativos', 'fixo', 'Exibir contratos desativados', 'Contrato.ic_ativo', NULL, ' Contrato.ic_ativo = 0 ', NULL, NULL, NULL, NULL, NULL, NULL);