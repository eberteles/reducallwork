CREATE TABLE cronograma_financeiro_desembolso(
    co_cronograma_financeiro_desembolso INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    co_contrato INT(11),
    descricao VARCHAR(200),
    mes int(3),
    dt_inicio DATE,
    dt_data_final DATE,
    percentual int(3),
    ds_valor decimal(10, 2),
    FOREIGN KEY (co_contrato) REFERENCES contratos(co_contrato) 
    
)ENGINE=INNODB;