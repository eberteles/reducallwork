-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 16/05/2014 às 20h00min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `ged`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `layouts`
--

CREATE TABLE IF NOT EXISTS `layouts` (
  `co_registro` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sequencial de registros',
  `co_layout` varchar(5) NOT NULL COMMENT 'Código do registro de layout. Ex.: XX999 - onde XX é a sigla do sistema e 999 é o sequencial do layout.',
  `ds_campo` varchar(40) NOT NULL COMMENT 'Descrição/Nome do campo do layout',
  `no_tam_campo` int(11) NOT NULL COMMENT 'Tamanho do campo no layout TXT em caracteres. Este campo é um facilitador para a linguagem PHP.',
  `no_ini_campo` int(11) NOT NULL COMMENT 'Posição inicial do campo no layout',
  `no_fim_campo` int(11) NOT NULL COMMENT 'Posição final do campo no layout',
  PRIMARY KEY (`co_registro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=149 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
