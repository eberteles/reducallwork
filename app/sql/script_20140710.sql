-- 
-- AREAS
-- 
CREATE TABLE `areas` (
  `co_area` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `ds_area` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_area`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE  `fornecedores` ADD  `co_area` INT( 11 ) NULL AFTER  `dt_inclusao`;

ALTER TABLE  `fornecedores` ADD INDEX (  `co_area` );

ALTER TABLE  `fornecedores` ADD FOREIGN KEY (  `co_area` ) REFERENCES `areas` (
`co_area`
);