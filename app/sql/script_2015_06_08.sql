CREATE TABLE lct_lotes(
	id INT NOT NULL AUTO_INCREMENT,
    co_licitacao INT NOT NULL,
    objeto TEXT NOT NULL,
    modalidade_processo TEXT NULL,
    data DATETIME NULL,
    valor_estimado DOUBLE NULL,
    valor_contratado DOUBLE NULL,
    diferenca DOUBLE NULL,
    prazo_execucao VARCHAR(255) NULL,
    empresa_vencedora VARCHAR(255) NULL,
    fase_atual TEXT NULL,
    modalidade_licitacao VARCHAR(255) NULL,
    ic_ativo INT NOT NULL DEFAULT 2,
    PRIMARY KEY (id)
);

ALTER TABLE lct_lotes ADD CONSTRAINT fk_licitacao_lotes FOREIGN KEY lct_licitacoes(id) REFERENCES co_licitacao ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE lct_fases(
	id INT NOT NULL AUTO_INCREMENT,
    data DATE NOT NULL,
    descricao TEXT NOT NULL,
    co_licitacao INT NOT NULL,
    ic_ativo INT NOT NULL DEFAULT 2,
    PRIMARY KEY (id)
);