DROP TABLE lct_fases;

CREATE TABLE lct_fases(
	id INT NOT NULL AUTO_INCREMENT,
    data DATE NOT NULL,
    descricao VARCHAR(255) NOT NULL,
    co_licitacao INT NOT NULL,
    ic_ativo INT NOT NULL DEFAULT 2,
    PRIMARY KEY(id)
)ENGINE=InnoDB;

ALTER TABLE lct_fases ADD CONSTRAINT fk_licitacoes_fases FOREIGN KEY (co_licitacao) REFERENCES lct_licitacoes(id) ON DELETE RESTRICT ON UPDATE CASCADE;

