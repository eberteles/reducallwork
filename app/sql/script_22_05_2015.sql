CREATE TABLE dsp_despesas_viagem(
	co_despesas_viagem INT NOT NULL AUTO_INCREMENT,
    numero_processo VARCHAR(20) NOT NULL,
    motivo_viagem VARCHAR(255) NOT NULL,
    cod_unidade_lotacao INT NOT NULL,
    nome_funcionario VARCHAR(50) NOT NULL,
    matricula_funcionario VARCHAR(15) NOT NULL,
    cod_cargo INT NOT NULL,
    cod_funcao INT NOT NULL,
    origem VARCHAR(50) NOT NULL,
    uf_origem VARCHAR(2) NOT NULL,
    destino VARCHAR(50) NOT NULL,
    uf_destino VARCHAR(2) NOT NULL,
    dt_inicio DATE NOT NULL,
    dt_final DATE NOT NULL,
    cod_meio_transporte INT NOT NULL,
    cod_categoria_passagem INT NOT NULL,
    valor_passagem DOUBLE NOT NULL,
    numero_diarias INT NOT NULL,
    valor_total_diarias DOUBLE NOT NULL,
    valor_total_viagem DOUBLE,
    PRIMARY KEY(co_despesas_viagem)
)Engine=InnoDB;

CREATE TABLE dsp_unidade_lotacao(
	co_unidade INT NOT NULL AUTO_INCREMENT,
    nome_unidade VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_unidade)
)Engine=InnoDB;

CREATE TABLE dsp_cargos(
	co_cargo INT NOT NULL AUTO_INCREMENT,
    nome_cargo VARCHAR(50),
    PRIMARY KEY(co_cargo)
)Engine=InnoDB;

CREATE TABLE dsp_funcao(
	co_funcao INT NOT NULL AUTO_INCREMENT,
    nome_funcao VARCHAR(50),
    PRIMARY KEY(co_funcao)
)Engine=InnoDB;

CREATE TABLE dsp_meio_transporte(
	co_meio_transporte INT NOT NULL AUTO_INCREMENT,
    nome_meio_transporte VARCHAR(50),
    PRIMARY KEY(co_meio_transporte)
)Engine=InnoDB;

CREATE TABLE dsp_categoria_passagem(
	co_categoria_passagem INT NOT NULL AUTO_INCREMENT,
    nome_categoria_passagem VARCHAR(50),
    PRIMARY KEY(co_categoria_passagem)
)Engine=InnoDB;

ALTER TABLE dsp_despesas_viagem ADD CONSTRAINT fk_unidade_despesas FOREIGN KEY (cod_unidade_lotacao) REFERENCES dsp_unidade_lotacao(co_unidade) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE dsp_despesas_viagem ADD CONSTRAINT fk_cargos_despesas FOREIGN KEY (cod_cargo) REFERENCES dsp_cargos(co_cargo) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE dsp_despesas_viagem ADD CONSTRAINT fk_funcao_despesas FOREIGN KEY (cod_funcao) REFERENCES dsp_funcao(co_funcao) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE dsp_despesas_viagem ADD CONSTRAINT fk_meio_transporte_despesas FOREIGN KEY (cod_meio_transporte) REFERENCES dsp_meio_transporte(co_meio_transporte) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE dsp_despesas_viagem ADD CONSTRAINT fk_categoria_pas_despesas FOREIGN KEY (cod_categoria_passagem) REFERENCES dsp_categoria_passagem(co_categoria_passagem) ON DELETE RESTRICT ON UPDATE CASCADE;


CREATE TABLE dsp_log_despesas_viagem(
	co_despesas_viagem INT NOT NULL AUTO_INCREMENT,
    numero_processo VARCHAR(20) NOT NULL,
    motivo_viagem VARCHAR(255) NOT NULL,
    cod_unidade_lotacao INT NOT NULL,
    nome_funcionario VARCHAR(50) NOT NULL,
    matricula_funcionario VARCHAR(15) NOT NULL,
    cod_cargo INT NOT NULL,
    cod_funcao INT NOT NULL,
    origem VARCHAR(50) NOT NULL,
    uf_origem VARCHAR(2) NOT NULL,
    destino VARCHAR(50) NOT NULL,
    uf_destino VARCHAR(2) NOT NULL,
    dt_inicio DATE NOT NULL,
    dt_final DATE NOT NULL,
    cod_meio_transporte INT NOT NULL,
    cod_categoria_passagem INT NOT NULL,
    valor_passagem DOUBLE NOT NULL,
    numero_diarias INT NOT NULL,
    valor_total_diarias DOUBLE NOT NULL,
    valor_total_viagem DOUBLE,
    PRIMARY KEY(co_despesas_viagem)
)Engine=InnoDB;

CREATE TABLE dsp_log_unidade_lotacao(
	co_unidade INT NOT NULL AUTO_INCREMENT,
    nome_unidade VARCHAR(50) NOT NULL,
    PRIMARY KEY(co_unidade)
)Engine=InnoDB;

CREATE TABLE dsp_log_cargos(
	co_cargo INT NOT NULL AUTO_INCREMENT,
    nome_cargo VARCHAR(50),
    PRIMARY KEY(co_cargo)
)Engine=InnoDB;

CREATE TABLE dsp_log_funcao(
	co_funcao INT NOT NULL AUTO_INCREMENT,
    nome_funcao VARCHAR(50),
    PRIMARY KEY(co_funcao)
)Engine=InnoDB;

CREATE TABLE dsp_log_meio_transporte(
	co_meio_transporte INT NOT NULL AUTO_INCREMENT,
    nome_meio_transporte VARCHAR(50),
    PRIMARY KEY(co_meio_transporte)
)Engine=InnoDB;

CREATE TABLE dsp_log_categoria_passagem(
	co_categoria_passagem INT NOT NULL AUTO_INCREMENT,
    nome_categoria_passagem VARCHAR(50),
    PRIMARY KEY(co_categoria_passagem)
)Engine=InnoDB;

