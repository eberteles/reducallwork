ALTER TABLE  `fornecedores` ADD  `tp_sexo` VARCHAR( 1 ) NULL ,
ADD  `tp_raca` INT( 1 ) NULL ,
ADD  `dt_nascimento` DATE NULL ,
ADD  `nu_identidade` VARCHAR( 30 ) NULL ,
ADD  `ds_orgao` VARCHAR( 15 ) NULL ,
ADD  `dt_expedicao` DATE NULL ,
ADD  `tp_graduacao` INT( 1 ) NULL ,
ADD  `sg_uf_origem` VARCHAR( 2 ) NULL ,
ADD  `co_municipio_origem` INT( 11 ) NULL ,
ADD  `ic_patrocinio` VARCHAR( 1 ) NULL;

ALTER TABLE  `fornecedores` ADD  `co_sub_area` INT( 11 ) NULL AFTER  `co_area`;