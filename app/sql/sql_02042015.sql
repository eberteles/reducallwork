CREATE TABLE `empenhos_anulacoes` (
`co_empenho_anulacao` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_empenho` INT NOT NULL ,
`tp_anulacao` CHAR( 1 ) NOT NULL COMMENT  'P - Parcial / T - Total',
`dt_anulacao` DATE NOT NULL ,
`vl_anulacao` DECIMAL( 10, 2 ) NOT NULL ,
INDEX (  `co_empenho` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Anulações dos Empenhos';

ALTER TABLE  `situacoes` ADD  `ic_ativo` CHAR( 1 ) NULL DEFAULT  '0' COMMENT  'Indica se a Situação é para quando o registro está Ativo';