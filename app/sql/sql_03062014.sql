CREATE TABLE `bancos` (
`co_banco` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`nu_banco` VARCHAR( 6 ) NOT NULL ,
`ds_banco` VARCHAR( 100 ) NOT NULL ,
PRIMARY KEY (  `co_banco` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Listagem FEBRABAN de Bancos';

-- 
-- Executar o Comando ou importar o arquivo CSV de outra forma:
    cake importar bancos.csv 1
--

ALTER TABLE  `fornecedores` CHANGE  `nu_banco`  `co_banco` INT( 11 ) NULL;

UPDATE `fornecedores` SET  `co_banco` = NULL;

ALTER TABLE  `fornecedores` ADD INDEX (  `co_banco` );

ALTER TABLE  `fornecedores` ADD FOREIGN KEY (  `co_banco` ) REFERENCES  `bancos` (
`co_banco`
);

ALTER TABLE  `log_fornecedores` CHANGE  `nu_banco`  `co_banco` INT( 11 ) NULL;

ALTER TABLE  `garantias` CHANGE  `vl_garantia`  `vl_garantia` DECIMAL( 10, 2 ) NULL ,
CHANGE  `dt_inicio`  `dt_inicio` DATE NULL ,
CHANGE  `dt_fim`  `dt_fim` DATE NULL ,
CHANGE  `no_seguradora`  `no_seguradora` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_endereco`  `ds_endereco` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `nu_telefone`  `nu_telefone` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_garantias` CHANGE  `vl_garantia`  `vl_garantia` DECIMAL( 10, 2 ) NULL ,
CHANGE  `dt_inicio`  `dt_inicio` DATE NULL ,
CHANGE  `dt_fim`  `dt_fim` DATE NULL ,
CHANGE  `no_seguradora`  `no_seguradora` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_endereco`  `ds_endereco` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `nu_telefone`  `nu_telefone` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `empenhos` CHANGE  `co_plano_interno`  `co_plano_interno` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

CREATE TABLE `itens_aquisicoes` (
`co_item_aquisicao` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`nu_item_aquisicao` INT( 11 ) NOT NULL ,
`ds_item_aquisicao` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `co_item_aquisicao` ) ,
UNIQUE (
`nu_item_aquisicao`
)
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Itens de Aquisições';

CREATE TABLE `aquisicoes` (
`co_aquisicao` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_item_aquisicao` INT( 11 ) NOT NULL ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NULL ,
`ds_aquisicao` VARCHAR( 255 ) NOT NULL ,
`dt_aquisicao` DATE NULL ,
`vl_aquisicao` DECIMAL( 10, 2 ) NOT NULL ,
`qt_aquisicao` INT( 5 ) NOT NULL ,
PRIMARY KEY (  `co_aquisicao` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Aquisições do Contrato';

ALTER TABLE  `garantias` CHANGE  `vl_garantia`  `vl_garantia` DECIMAL( 10, 2 ) NULL ,
CHANGE  `dt_inicio`  `dt_inicio` DATE NULL ,
CHANGE  `dt_fim`  `dt_fim` DATE NULL ,
CHANGE  `no_seguradora`  `no_seguradora` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_endereco`  `ds_endereco` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `nu_telefone`  `nu_telefone` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `aquisicoes` ADD INDEX (  `co_item_aquisicao` );
ALTER TABLE  `aquisicoes` ADD INDEX (  `co_contrato` );
ALTER TABLE  `aquisicoes` ADD INDEX (  `co_usuario` );

ALTER TABLE  `aquisicoes` ADD FOREIGN KEY (  `co_item_aquisicao` ) REFERENCES  `itens_aquisicoes` (
`co_item_aquisicao`
);

ALTER TABLE  `aquisicoes` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `aquisicoes` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

ALTER TABLE  `garantias` ADD INDEX (  `co_usuario` );

ALTER TABLE  `garantias` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES `contratos` (
`co_contrato`
);

ALTER TABLE  `garantias` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES `usuarios` (
`co_usuario`
);

ALTER TABLE  `contratos` ADD  `co_ident_siasg` VARCHAR( 20 ) NULL AFTER  `co_usuario`;

ALTER TABLE  `contratos` CHANGE  `ds_objeto`  `ds_objeto` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `aquisicoes` CHANGE  `ds_aquisicao`  `ds_aquisicao` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;