ALTER TABLE  `atas` CHANGE  `ds_publicacao`  `ds_publicacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `vl_mensal`  `vl_mensal` DECIMAL( 10, 2 ) NULL;

ALTER TABLE  `atas` ADD  `tp_pregao` CHAR( 1 ) NULL COMMENT  'P - Presencial, E - Eletrônico' AFTER  `tp_ata` ,
ADD  `ds_especificacao` VARCHAR( 500 ) NULL AFTER  `tp_pregao`;

ALTER TABLE  `alertas` ADD  `ck_aviso120` CHAR( 1 ) NULL DEFAULT NULL;
ALTER TABLE  `alertas` ADD  `ck_aviso_uni30` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso30`;
ALTER TABLE  `alertas` ADD  `ck_aviso_fis30` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_uni30`;
ALTER TABLE  `alertas` ADD  `ck_aviso_for30` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_fis30`;
ALTER TABLE  `alertas` ADD  `ck_aviso_uni60` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso60`;
ALTER TABLE  `alertas` ADD  `ck_aviso_fis60` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_uni60`;
ALTER TABLE  `alertas` ADD  `ck_aviso_for60` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_fis60`;
ALTER TABLE  `alertas` ADD  `ck_aviso_uni90` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso90`;
ALTER TABLE  `alertas` ADD  `ck_aviso_fis90` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_uni90`;
ALTER TABLE  `alertas` ADD  `ck_aviso_for90` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_fis90`;
ALTER TABLE  `alertas` ADD  `ck_aviso_uni120` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso120`;
ALTER TABLE  `alertas` ADD  `ck_aviso_fis120` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_uni120`;
ALTER TABLE  `alertas` ADD  `ck_aviso_for120` CHAR( 1 ) NULL DEFAULT NULL AFTER  `ck_aviso_fis120`;

DROP TABLE  `log_andamentos`;

ALTER TABLE  `categorias` ADD  `co_usuario` INT( 11 ) NULL;
ALTER TABLE  `log_categorias` CHANGE  `co_usuario`  `co_usuario` INT( 11 ) NULL;

--
-- Gatilhos `categorias`
--
DROP TRIGGER IF EXISTS `ADD_LOG_CATEGORIA`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_CATEGORIA` AFTER INSERT ON `categorias`
 FOR EACH ROW BEGIN
	INSERT INTO log_categorias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_categoria    = NEW.co_categoria,
            no_categoria    = NEW.no_categoria;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_CATEGORIA`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_CATEGORIA` AFTER UPDATE ON `categorias`
 FOR EACH ROW BEGIN
	INSERT INTO log_categorias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_categoria    = NEW.co_categoria,
            no_categoria    = NEW.no_categoria;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_CATEGORIA`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_CATEGORIA` AFTER DELETE ON `categorias`
 FOR EACH ROW BEGIN
	INSERT INTO log_categorias SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_categoria    = OLD.co_categoria,
            no_categoria    = OLD.no_categoria;
        END
//
DELIMITER ;

DROP TABLE  `log_empenhos`;
CREATE TABLE `log_empenhos` (
  `co_log_empenhos` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NOT NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_empenho` int(10) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `nu_empenho` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tp_empenho` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `ds_empenho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_plano_interno` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `vl_empenho` decimal(10,2) NOT NULL,
  `dt_empenho` date NOT NULL,
  `nu_ptres` int(10) NOT NULL,
  `nu_evento_contabil` int(6) DEFAULT NULL COMMENT 'Código do Evento Contábil do SIAFI',
  `nu_esfera_orcamentaria` int(1) DEFAULT NULL,
  `ds_fonte_recurso` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_natureza_despesa` int(6) DEFAULT NULL,
  `ds_inciso` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_amparo_legal` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_ug_responsavel` int(6) DEFAULT NULL COMMENT 'Unidade Gestora Responsável',
  `nu_origem_material` int(1) DEFAULT NULL COMMENT '1 - Origem Nacional, 2 - Material Estrangeiro adquirido no Brasil, 3 - Importação Direta',
  `uf_beneficiada` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_municipio_beneficiado` int(4) DEFAULT NULL COMMENT 'Código IBGE do município beneficiado',
  `nu_contra_entrega` int(1) DEFAULT NULL COMMENT '0 - NÃO SE APLICA, 1 - FIXADO PELA STN, 2 - FIXADO PELA PRÓPRIA UG',
  `nu_lista` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_empenhos`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gatilhos `empenhos`
--
DROP TRIGGER IF EXISTS `ADD_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_EMPENHO` AFTER INSERT ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_empenho       = NEW.co_empenho,
            co_contrato      = NEW.co_contrato,
            nu_empenho       = NEW.nu_empenho,
            tp_empenho       = NEW.tp_empenho,
            ds_empenho       = NEW.ds_empenho,
            co_plano_interno = NEW.co_plano_interno,
            vl_empenho       = NEW.vl_empenho,
            dt_empenho       = NEW.dt_empenho,
            nu_ptres         = NEW.nu_ptres,
            nu_evento_contabil      = NEW.nu_evento_contabil,
            nu_esfera_orcamentaria  = NEW.nu_esfera_orcamentaria,
            ds_fonte_recurso    = NEW.ds_fonte_recurso,
            nu_natureza_despesa = NEW.nu_natureza_despesa,
            ds_inciso           = NEW.ds_inciso,
            ds_amparo_legal     = NEW.ds_amparo_legal,
            nu_ug_responsavel   = NEW.nu_ug_responsavel,
            nu_origem_material  = NEW.nu_origem_material,
            uf_beneficiada      = NEW.uf_beneficiada,
            nu_municipio_beneficiado = NEW.nu_municipio_beneficiado,
            nu_contra_entrega   = NEW.nu_contra_entrega,
            nu_lista            = NEW.nu_lista;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_EMPENHO` AFTER UPDATE ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_empenho       = NEW.co_empenho,
            co_contrato      = NEW.co_contrato,
            nu_empenho       = NEW.nu_empenho,
            tp_empenho       = NEW.tp_empenho,
            ds_empenho       = NEW.ds_empenho,
            co_plano_interno = NEW.co_plano_interno,
            vl_empenho       = NEW.vl_empenho,
            dt_empenho       = NEW.dt_empenho,
            nu_ptres         = NEW.nu_ptres,
            nu_evento_contabil      = NEW.nu_evento_contabil,
            nu_esfera_orcamentaria  = NEW.nu_esfera_orcamentaria,
            ds_fonte_recurso    = NEW.ds_fonte_recurso,
            nu_natureza_despesa = NEW.nu_natureza_despesa,
            ds_inciso           = NEW.ds_inciso,
            ds_amparo_legal     = NEW.ds_amparo_legal,
            nu_ug_responsavel   = NEW.nu_ug_responsavel,
            nu_origem_material  = NEW.nu_origem_material,
            uf_beneficiada      = NEW.uf_beneficiada,
            nu_municipio_beneficiado = NEW.nu_municipio_beneficiado,
            nu_contra_entrega   = NEW.nu_contra_entrega,
            nu_lista            = NEW.nu_lista;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_EMPENHO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_EMPENHO` AFTER DELETE ON `empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_empenho       = OLD.co_empenho,
            co_contrato      = OLD.co_contrato,
            nu_empenho       = OLD.nu_empenho,
            tp_empenho       = OLD.tp_empenho,
            ds_empenho       = OLD.ds_empenho,
            co_plano_interno = OLD.co_plano_interno,
            vl_empenho       = OLD.vl_empenho,
            dt_empenho       = OLD.dt_empenho,
            nu_ptres         = OLD.nu_ptres,
            nu_evento_contabil      = OLD.nu_evento_contabil,
            nu_esfera_orcamentaria  = OLD.nu_esfera_orcamentaria,
            ds_fonte_recurso    = OLD.ds_fonte_recurso,
            nu_natureza_despesa = OLD.nu_natureza_despesa,
            ds_inciso           = OLD.ds_inciso,
            ds_amparo_legal     = OLD.ds_amparo_legal,
            nu_ug_responsavel   = OLD.nu_ug_responsavel,
            nu_origem_material  = OLD.nu_origem_material,
            uf_beneficiada      = OLD.uf_beneficiada,
            nu_municipio_beneficiado = OLD.nu_municipio_beneficiado,
            nu_contra_entrega   = OLD.nu_contra_entrega,
            nu_lista            = OLD.nu_lista;
        END
//
DELIMITER ;

ALTER TABLE  `fornecedores` ADD  `co_usuario` INT( 11 ) NULL;
ALTER TABLE  `fornecedores` ADD  `no_nome_fantasia` VARCHAR( 100 ) NULL AFTER  `no_razao_social`;

DROP TABLE  `log_fornecedores`;
CREATE TABLE `log_fornecedores` (
  `co_log_fornecedores` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_fornecedor` int(11) NOT NULL,
  `tp_fornecedor` varchar(1) COLLATE utf8_unicode_ci DEFAULT 'J',
  `nu_cnpj` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `no_razao_social` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_nome_fantasia` varchar(100) COLLATE utf8_unicode_ci NULL,
  `ds_endereco` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_endereco` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_bairro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_complemento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co_municipio` int(11) DEFAULT NULL,
  `ds_municipio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sg_uf` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_fax` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_responsavel` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_cpf_responsavel` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_rg_responsavel` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co_banco` int(11) DEFAULT NULL,
  `nu_agencia` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_conta` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dt_inclusao` datetime DEFAULT NULL,
  `co_area` int(11) DEFAULT NULL,
  `ds_area` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_subarea` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_observacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_fornecedores`),
  KEY `co_fornecedor` (`co_fornecedor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

--
-- Gatilhos `fornecedores`
--
DROP TRIGGER IF EXISTS `ADD_LOG_FORNECEDOR`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_FORNECEDOR` AFTER INSERT ON `fornecedores`
 FOR EACH ROW BEGIN
	INSERT INTO log_fornecedores SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_fornecedor   = NEW.co_fornecedor,
            tp_fornecedor   = NEW.tp_fornecedor,
            nu_cnpj         = NEW.nu_cnpj,
            no_razao_social = NEW.no_razao_social,
            no_nome_fantasia = NEW.no_razao_social,
            ds_endereco     = NEW.ds_endereco,
            nu_endereco     = NEW.nu_endereco,
            ds_bairro       = NEW.ds_bairro,
            ds_complemento  = NEW.ds_complemento,
            nu_cep          = NEW.nu_cep,
            co_municipio    = NEW.co_municipio,
            ds_municipio    = NEW.ds_municipio,
            sg_uf           = NEW.sg_uf,
            ds_email        = NEW.ds_email,
            nu_telefone     = NEW.nu_telefone,
            nu_fax          = NEW.nu_fax,
            no_responsavel  = NEW.no_responsavel,
            nu_cpf_responsavel  = NEW.nu_cpf_responsavel,
            nu_rg_responsavel   = NEW.nu_rg_responsavel,
            co_banco        = NEW.co_banco,
            nu_agencia      = NEW.nu_agencia,
            nu_conta        = NEW.nu_conta,
            dt_inclusao     = NEW.dt_inclusao,
            co_area         = NEW.co_area,
            ds_area         = NEW.ds_area,
            ds_subarea      = NEW.ds_subarea,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_FORNECEDOR`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_FORNECEDOR` AFTER UPDATE ON `fornecedores`
 FOR EACH ROW BEGIN
	INSERT INTO log_fornecedores SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_fornecedor   = NEW.co_fornecedor,
            tp_fornecedor   = NEW.tp_fornecedor,
            nu_cnpj         = NEW.nu_cnpj,
            no_razao_social = NEW.no_razao_social,
            no_nome_fantasia = NEW.no_razao_social,
            ds_endereco     = NEW.ds_endereco,
            nu_endereco     = NEW.nu_endereco,
            ds_bairro       = NEW.ds_bairro,
            ds_complemento  = NEW.ds_complemento,
            nu_cep          = NEW.nu_cep,
            co_municipio    = NEW.co_municipio,
            ds_municipio    = NEW.ds_municipio,
            sg_uf           = NEW.sg_uf,
            ds_email        = NEW.ds_email,
            nu_telefone     = NEW.nu_telefone,
            nu_fax          = NEW.nu_fax,
            no_responsavel  = NEW.no_responsavel,
            nu_cpf_responsavel  = NEW.nu_cpf_responsavel,
            nu_rg_responsavel   = NEW.nu_rg_responsavel,
            co_banco        = NEW.co_banco,
            nu_agencia      = NEW.nu_agencia,
            nu_conta        = NEW.nu_conta,
            dt_inclusao     = NEW.dt_inclusao,
            co_area         = NEW.co_area,
            ds_area         = NEW.ds_area,
            ds_subarea      = NEW.ds_subarea,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_FORNECEDOR`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_FORNECEDOR` AFTER DELETE ON `fornecedores`
 FOR EACH ROW BEGIN
	INSERT INTO log_fornecedores SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_fornecedor   = OLD.co_fornecedor,
            tp_fornecedor   = OLD.tp_fornecedor,
            nu_cnpj         = OLD.nu_cnpj,
            no_razao_social = OLD.no_razao_social,
            no_nome_fantasia = OLD.no_razao_social,
            ds_endereco     = OLD.ds_endereco,
            nu_endereco     = OLD.nu_endereco,
            ds_bairro       = OLD.ds_bairro,
            ds_complemento  = OLD.ds_complemento,
            nu_cep          = OLD.nu_cep,
            co_municipio    = OLD.co_municipio,
            ds_municipio    = OLD.ds_municipio,
            sg_uf           = OLD.sg_uf,
            ds_email        = OLD.ds_email,
            nu_telefone     = OLD.nu_telefone,
            nu_fax          = OLD.nu_fax,
            no_responsavel  = OLD.no_responsavel,
            nu_cpf_responsavel  = OLD.nu_cpf_responsavel,
            nu_rg_responsavel   = OLD.nu_rg_responsavel,
            co_banco        = OLD.co_banco,
            nu_agencia      = OLD.nu_agencia,
            nu_conta        = OLD.nu_conta,
            dt_inclusao     = OLD.dt_inclusao,
            co_area         = OLD.co_area,
            ds_area         = OLD.ds_area,
            ds_subarea      = OLD.ds_subarea,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

DROP TABLE  `log_garantias`;
CREATE TABLE `log_garantias` (
  `co_log_garantias` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_garantia` int(11) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `ds_modalidade` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vl_garantia` decimal(10,2) DEFAULT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `no_seguradora` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_apolice` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_observacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_garantias`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

--
-- Gatilhos `garantias`
--
DROP TRIGGER IF EXISTS `ADD_LOG_GARANTIA`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_GARANTIA` AFTER INSERT ON `garantias`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_garantia     = NEW.co_garantia,
            co_contrato     = NEW.co_contrato,
            ds_modalidade   = NEW.ds_modalidade,
            vl_garantia     = NEW.vl_garantia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            no_seguradora   = NEW.no_seguradora,
            nu_apolice      = NEW.nu_apolice,
            ds_endereco     = NEW.ds_endereco,
            nu_telefone     = NEW.nu_telefone,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_GARANTIA`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_GARANTIA` AFTER UPDATE ON `garantias`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_garantia     = NEW.co_garantia,
            co_contrato     = NEW.co_contrato,
            ds_modalidade   = NEW.ds_modalidade,
            vl_garantia     = NEW.vl_garantia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            no_seguradora   = NEW.no_seguradora,
            nu_apolice      = NEW.nu_apolice,
            ds_endereco     = NEW.ds_endereco,
            nu_telefone     = NEW.nu_telefone,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_GARANTIA`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_GARANTIA` AFTER DELETE ON `garantias`
 FOR EACH ROW BEGIN
	INSERT INTO log_garantias SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_garantia     = OLD.co_garantia,
            co_contrato     = OLD.co_contrato,
            ds_modalidade   = OLD.ds_modalidade,
            vl_garantia     = OLD.vl_garantia,
            dt_inicio       = OLD.dt_inicio,
            dt_fim          = OLD.dt_fim,
            no_seguradora   = OLD.no_seguradora,
            nu_apolice      = OLD.nu_apolice,
            ds_endereco     = OLD.ds_endereco,
            nu_telefone     = OLD.nu_telefone,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

ALTER TABLE  `modalidades` ADD  `co_usuario` INT( 11 ) NULL ,
ADD INDEX (  `co_usuario` );

--
-- Gatilhos `modalidades`
--
DROP TRIGGER IF EXISTS `ADD_LOG_MODALIDADE`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_MODALIDADE` AFTER INSERT ON `modalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_modalidades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_modalidade   = NEW.co_modalidade,
            nu_modalidade   = NEW.nu_modalidade,
            ds_modalidade   = NEW.ds_modalidade;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_MODALIDADE`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_MODALIDADE` AFTER UPDATE ON `modalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_modalidades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_modalidade   = NEW.co_modalidade,
            nu_modalidade   = NEW.nu_modalidade,
            ds_modalidade   = NEW.ds_modalidade;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_MODALIDADE`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_MODALIDADE` AFTER DELETE ON `modalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_modalidades SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_modalidade   = OLD.co_modalidade,
            nu_modalidade   = OLD.nu_modalidade,
            ds_modalidade   = OLD.ds_modalidade;
        END
//
DELIMITER ;

--
-- Gatilhos `pagamentos`
--
DROP TRIGGER IF EXISTS `ADD_LOG_PAGAMENTO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_PAGAMENTO` AFTER INSERT ON `pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_pagamento    = NEW.co_pagamento,
            co_empenho      = NEW.co_empenho,
            co_contrato     = NEW.co_contrato,
            co_atividade    = NEW.co_atividade,
            nu_mes_pagamento = NEW.nu_mes_pagamento,
            nu_ano_pagameto = NEW.nu_ano_pagameto,
            dt_vencimento   = NEW.dt_vencimento,
            dt_pagamento    = NEW.dt_pagamento,
            dt_financeiro   = NEW.dt_financeiro,
            dt_administrativo = NEW.dt_administrativo,
            nu_nota_fiscal  = NEW.nu_nota_fiscal,
            nu_serie_nf     = NEW.nu_serie_nf,
            vl_nota         = NEW.vl_nota,
            vl_imposto      = NEW.vl_imposto,
            vl_liquido      = NEW.vl_liquido,
            vl_pago         = NEW.vl_pago,
            vl_pagamento    = NEW.vl_pagamento,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_PAGAMENTO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_PAGAMENTO` AFTER UPDATE ON `pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_pagamento    = NEW.co_pagamento,
            co_empenho      = NEW.co_empenho,
            co_contrato     = NEW.co_contrato,
            co_atividade    = NEW.co_atividade,
            nu_mes_pagamento = NEW.nu_mes_pagamento,
            nu_ano_pagameto = NEW.nu_ano_pagameto,
            dt_vencimento   = NEW.dt_vencimento,
            dt_pagamento    = NEW.dt_pagamento,
            dt_financeiro   = NEW.dt_financeiro,
            dt_administrativo = NEW.dt_administrativo,
            nu_nota_fiscal  = NEW.nu_nota_fiscal,
            nu_serie_nf     = NEW.nu_serie_nf,
            vl_nota         = NEW.vl_nota,
            vl_imposto      = NEW.vl_imposto,
            vl_liquido      = NEW.vl_liquido,
            vl_pago         = NEW.vl_pago,
            vl_pagamento    = NEW.vl_pagamento,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_PAGAMENTO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_PAGAMENTO` AFTER DELETE ON `pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_pagamento    = OLD.co_pagamento,
            co_empenho      = OLD.co_empenho,
            co_contrato     = OLD.co_contrato,
            co_atividade    = OLD.co_atividade,
            nu_mes_pagamento = OLD.nu_mes_pagamento,
            nu_ano_pagameto = OLD.nu_ano_pagameto,
            dt_vencimento   = OLD.dt_vencimento,
            dt_pagamento    = OLD.dt_pagamento,
            dt_financeiro   = OLD.dt_financeiro,
            dt_administrativo = OLD.dt_administrativo,
            nu_nota_fiscal  = OLD.nu_nota_fiscal,
            nu_serie_nf     = OLD.nu_serie_nf,
            vl_nota         = OLD.vl_nota,
            vl_imposto      = OLD.vl_imposto,
            vl_liquido      = OLD.vl_liquido,
            vl_pago         = OLD.vl_pago,
            vl_pagamento    = OLD.vl_pagamento,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

ALTER TABLE  `pendencias` ADD  `co_usuario` INT( 11 ) NULL ,
ADD INDEX (  `co_usuario` );

DROP TABLE  `log_pendencias`;
CREATE TABLE `log_pendencias` (
  `co_log_pendencias` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_pendencia` int(11) NOT NULL,
  `co_ata` int(11) NULL,
  `co_contrato` int(11) NULL,
  `ds_pendencia` varchar(100) NOT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date DEFAULT NULL,
  `st_pendencia` varchar(1) NOT NULL,
  `ds_observacao` varchar(255) NOT NULL,
  PRIMARY KEY (`co_log_pendencias`),
  KEY `co_contrato` (`co_contrato`),
  KEY `co_ata` (`co_ata`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gatilhos `pendencias`
--
DROP TRIGGER IF EXISTS `ADD_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_PENDENCIA` AFTER INSERT ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_pendencia    = NEW.co_pendencia,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            ds_pendencia    = NEW.ds_pendencia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            st_pendencia    = NEW.st_pendencia,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_PENDENCIA` AFTER UPDATE ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_pendencia    = NEW.co_pendencia,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            ds_pendencia    = NEW.ds_pendencia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            st_pendencia    = NEW.st_pendencia,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_PENDENCIA` AFTER DELETE ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_pendencia    = OLD.co_pendencia,
            co_ata          = OLD.co_ata,
            co_contrato     = OLD.co_contrato,
            ds_pendencia    = OLD.ds_pendencia,
            dt_inicio       = OLD.dt_inicio,
            dt_fim          = OLD.dt_fim,
            st_pendencia    = OLD.st_pendencia,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

ALTER TABLE  `setores` ADD  `co_usuario` INT( 11 ) NULL ,
ADD INDEX (  `co_usuario` );

DROP TABLE  `log_setores`;
CREATE TABLE `log_setores` (
  `co_log_setores` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_setor` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `ds_setor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ds_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ic_setor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_setores`),
  KEY `co_setor` (`co_setor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gatilhos `setores`
--
DROP TRIGGER IF EXISTS `ADD_LOG_SETOR`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_SETOR` AFTER INSERT ON `setores`
 FOR EACH ROW BEGIN
	INSERT INTO log_setores SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_setor        = NEW.co_setor,
            parent_id       = NEW.parent_id,
            ds_setor        = NEW.ds_setor,
            ds_email        = NEW.ds_email,
            ic_setor        = NEW.ic_setor;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_SETOR`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_SETOR` AFTER UPDATE ON `setores`
 FOR EACH ROW BEGIN
	INSERT INTO log_setores SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_setor        = NEW.co_setor,
            parent_id       = NEW.parent_id,
            ds_setor        = NEW.ds_setor,
            ds_email        = NEW.ds_email,
            ic_setor        = NEW.ic_setor;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_SETOR`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_SETOR` AFTER DELETE ON `setores`
 FOR EACH ROW BEGIN
	INSERT INTO log_setores SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_setor        = OLD.co_setor,
            parent_id       = OLD.parent_id,
            ds_setor        = OLD.ds_setor,
            ds_email        = OLD.ds_email,
            ic_setor        = OLD.ic_setor;
        END
//
DELIMITER ;

ALTER TABLE  `situacoes` ADD  `co_usuario` INT( 11 ) NULL ,
ADD INDEX (  `co_usuario` );

--
-- Gatilhos `situacoes`
--
DROP TRIGGER IF EXISTS `ADD_LOG_SITUACAO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_SITUACAO` AFTER INSERT ON `situacoes`
 FOR EACH ROW BEGIN
	INSERT INTO log_situacoes SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_situacao     = NEW.co_situacao,
            ds_situacao     = NEW.ds_situacao,
            ic_financeiro   = NEW.ic_financeiro;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_SITUACAO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_SITUACAO` AFTER UPDATE ON `situacoes`
 FOR EACH ROW BEGIN
	INSERT INTO log_situacoes SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_situacao     = NEW.co_situacao,
            ds_situacao     = NEW.ds_situacao,
            ic_financeiro   = NEW.ic_financeiro;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_SITUACAO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_SITUACAO` AFTER DELETE ON `situacoes`
 FOR EACH ROW BEGIN
	INSERT INTO log_situacoes SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_situacao     = OLD.co_situacao,
            ds_situacao     = OLD.ds_situacao,
            ic_financeiro   = OLD.ic_financeiro;
        END
//
DELIMITER ;

ALTER TABLE  `usuarios` ADD  `co_usuario_cad` INT( 11 ) NULL;

CREATE TABLE `log_usuarios` (
  `co_log_usuarios` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario_cad` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_usuario` int(11) NOT NULL,
  `co_privilegio` int(11) DEFAULT NULL,
  `co_setor` int(11) DEFAULT NULL,
  `co_gestor` int(11) DEFAULT NULL,
  `ic_acesso` int(1) DEFAULT '0',
  `nu_cpf` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `ds_nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_usuario` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_senha` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dt_liberacao` datetime DEFAULT NULL,
  `dt_bloqueio` datetime DEFAULT NULL,
  `dt_ult_acesso` datetime DEFAULT NULL,
  `ds_privilegios` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_log_usuarios`),
  KEY `co_usuario` (`co_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Gatilhos `usuarios`
--
DROP TRIGGER IF EXISTS `ADD_LOG_USUARIO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_USUARIO` AFTER INSERT ON `usuarios`
 FOR EACH ROW BEGIN
	INSERT INTO log_usuarios SET 
            dt_log          = NOW(), 
            co_usuario_cad  = NEW.co_usuario_cad, 
            tp_acao         = 'I',

            co_usuario      = NEW.co_usuario,
            co_privilegio   = NEW.co_privilegio,
            co_setor        = NEW.co_setor,
            co_gestor       = NEW.co_gestor,
            ic_acesso       = NEW.ic_acesso,
            nu_cpf          = NEW.nu_cpf,
            ds_nome         = NEW.ds_nome,
            no_usuario      = NEW.no_usuario,
            ds_senha        = NEW.ds_senha,
            ds_email        = NEW.ds_email,
            nu_telefone     = NEW.nu_telefone,
            dt_liberacao    = NEW.dt_liberacao,
            dt_bloqueio     = NEW.dt_bloqueio,
            dt_ult_acesso   = NEW.dt_ult_acesso,
            ds_privilegios  = NEW.ds_privilegios;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_USUARIO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_USUARIO` AFTER UPDATE ON `usuarios`
 FOR EACH ROW BEGIN
	INSERT INTO log_usuarios SET 
            dt_log          = NOW(), 
            co_usuario_cad  = NEW.co_usuario_cad, 
            tp_acao         = 'A',

            co_usuario      = NEW.co_usuario,
            co_privilegio   = NEW.co_privilegio,
            co_setor        = NEW.co_setor,
            co_gestor       = NEW.co_gestor,
            ic_acesso       = NEW.ic_acesso,
            nu_cpf          = NEW.nu_cpf,
            ds_nome         = NEW.ds_nome,
            no_usuario      = NEW.no_usuario,
            ds_senha        = NEW.ds_senha,
            ds_email        = NEW.ds_email,
            nu_telefone     = NEW.nu_telefone,
            dt_liberacao    = NEW.dt_liberacao,
            dt_bloqueio     = NEW.dt_bloqueio,
            dt_ult_acesso   = NEW.dt_ult_acesso,
            ds_privilegios  = NEW.ds_privilegios;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_USUARIO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_USUARIO` AFTER DELETE ON `usuarios`
 FOR EACH ROW BEGIN
	INSERT INTO log_usuarios SET 
            dt_log          = NOW(), 
            co_usuario_cad  = OLD.co_usuario_cad, 
            tp_acao         = 'E',

            co_usuario      = OLD.co_usuario,
            co_privilegio   = OLD.co_privilegio,
            co_setor        = OLD.co_setor,
            co_gestor       = OLD.co_gestor,
            ic_acesso       = OLD.ic_acesso,
            nu_cpf          = OLD.nu_cpf,
            ds_nome         = OLD.ds_nome,
            no_usuario      = OLD.no_usuario,
            ds_senha        = OLD.ds_senha,
            ds_email        = OLD.ds_email,
            nu_telefone     = OLD.nu_telefone,
            dt_liberacao    = OLD.dt_liberacao,
            dt_bloqueio     = OLD.dt_bloqueio,
            dt_ult_acesso   = OLD.dt_ult_acesso,
            ds_privilegios  = OLD.ds_privilegios;
        END
//
DELIMITER ;

CREATE TABLE `log_apostilamentos` (
  `co_log_apostilamentos` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_apostilamento` int(10) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `no_apostilamento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tp_apostilamento` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 - Apostilamento de Valor 2 - Apostilamento de Prazo 3 - Apostilamento de Valor e Prazo',
  `dt_apostilamento` date NOT NULL,
  `vl_apostilamento` decimal(10,2) DEFAULT NULL,
  `dt_fim_vigencia_anterior` date DEFAULT NULL,
  `dt_prazo` date DEFAULT NULL,
  `ds_apostilamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_log_apostilamentos`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

--
-- Gatilhos `apostilamentos`
--
DROP TRIGGER IF EXISTS `ADD_LOG_APOSTILAMENTO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_APOSTILAMENTO` AFTER INSERT ON `apostilamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_apostilamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_apostilamento    = NEW.co_apostilamento,
            co_contrato         = NEW.co_contrato,
            no_apostilamento    = NEW.no_apostilamento,
            tp_apostilamento    = NEW.tp_apostilamento,
            dt_apostilamento    = NEW.dt_apostilamento,
            vl_apostilamento    = NEW.vl_apostilamento,
            dt_fim_vigencia_anterior = NEW.dt_fim_vigencia_anterior,
            dt_prazo            = NEW.dt_prazo,
            ds_apostilamento    = NEW.ds_apostilamento;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_APOSTILAMENTO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_APOSTILAMENTO` AFTER UPDATE ON `apostilamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_apostilamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_apostilamento    = NEW.co_apostilamento,
            co_contrato         = NEW.co_contrato,
            no_apostilamento    = NEW.no_apostilamento,
            tp_apostilamento    = NEW.tp_apostilamento,
            dt_apostilamento    = NEW.dt_apostilamento,
            vl_apostilamento    = NEW.vl_apostilamento,
            dt_fim_vigencia_anterior = NEW.dt_fim_vigencia_anterior,
            dt_prazo            = NEW.dt_prazo,
            ds_apostilamento    = NEW.ds_apostilamento;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_APOSTILAMENTO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_APOSTILAMENTO` AFTER DELETE ON `apostilamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_apostilamentos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_apostilamento    = OLD.co_apostilamento,
            co_contrato         = OLD.co_contrato,
            no_apostilamento    = OLD.no_apostilamento,
            tp_apostilamento    = OLD.tp_apostilamento,
            dt_apostilamento    = OLD.dt_apostilamento,
            vl_apostilamento    = OLD.vl_apostilamento,
            dt_fim_vigencia_anterior = OLD.dt_fim_vigencia_anterior,
            dt_prazo            = OLD.dt_prazo,
            ds_apostilamento    = OLD.ds_apostilamento;
        END
//
DELIMITER ;

CREATE TABLE `log_fases` (
  `co_log_fases` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_fase` int(11) NOT NULL,
  `ds_fase` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`co_log_fases`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE  `fases` ADD  `co_usuario` INT( 11 ) NULL ,
ADD INDEX (  `co_usuario` );

--
-- Gatilhos `fases`
--
DROP TRIGGER IF EXISTS `ADD_LOG_FASE`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_FASE` AFTER INSERT ON `fases`
 FOR EACH ROW BEGIN
	INSERT INTO log_fases SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_fase         = NEW.co_fase,
            ds_fase         = NEW.ds_fase;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_FASE`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_FASE` AFTER UPDATE ON `fases`
 FOR EACH ROW BEGIN
	INSERT INTO log_fases SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_fase         = NEW.co_fase,
            ds_fase         = NEW.ds_fase;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_FASE`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_FASE` AFTER DELETE ON `fases`
 FOR EACH ROW BEGIN
	INSERT INTO log_fases SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_fase         = OLD.co_fase,
            ds_fase         = OLD.ds_fase;
        END
//
DELIMITER ;

CREATE TABLE `log_notas_fiscais` (
  `co_notas_fiscais` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_nota` int(11) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `nu_nota` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nu_serie` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dt_recebimento` date NOT NULL,
  `vl_nota` decimal(10,2) NOT NULL,
  `vl_glosa` decimal(10,2) DEFAULT NULL,
  `ds_nota` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ic_atesto` int(1) DEFAULT NULL,
  `dt_atesto` date DEFAULT NULL,
  `ds_atesto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`co_notas_fiscais`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TRIGGER IF EXISTS `ADD_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_NOTA_FISCAL` AFTER INSERT ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_nota         = NEW.co_nota,
            co_contrato     = NEW.co_contrato,
            nu_nota         = NEW.nu_nota,
            nu_serie        = NEW.nu_serie,
            dt_recebimento  = NEW.dt_recebimento,
            vl_nota         = NEW.vl_nota,
            vl_glosa        = NEW.vl_glosa,
            ds_nota         = NEW.ds_nota,
            ic_atesto       = NEW.ic_atesto,
            dt_atesto       = NEW.dt_atesto,
            ds_atesto       = NEW.ds_atesto;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_NOTA_FISCAL` AFTER UPDATE ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_nota         = NEW.co_nota,
            co_contrato     = NEW.co_contrato,
            nu_nota         = NEW.nu_nota,
            nu_serie        = NEW.nu_serie,
            dt_recebimento  = NEW.dt_recebimento,
            vl_nota         = NEW.vl_nota,
            vl_glosa        = NEW.vl_glosa,
            ds_nota         = NEW.ds_nota,
            ic_atesto       = NEW.ic_atesto,
            dt_atesto       = NEW.dt_atesto,
            ds_atesto       = NEW.ds_atesto;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_NOTA_FISCAL` AFTER DELETE ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_nota         = OLD.co_nota,
            co_contrato     = OLD.co_contrato,
            nu_nota         = OLD.nu_nota,
            nu_serie        = OLD.nu_serie,
            dt_recebimento  = OLD.dt_recebimento,
            vl_nota         = OLD.vl_nota,
            vl_glosa        = OLD.vl_glosa,
            ds_nota         = OLD.ds_nota,
            ic_atesto       = OLD.ic_atesto,
            dt_atesto       = OLD.dt_atesto,
            ds_atesto       = OLD.ds_atesto;
        END
//
DELIMITER ;

ALTER TABLE  `pagamentos` CHANGE  `nu_nota_fiscal`  `nu_nota_fiscal` VARCHAR( 20 ) NULL DEFAULT NULL ,
CHANGE  `nu_serie_nf`  `nu_serie_nf` VARCHAR( 3 ) NULL DEFAULT NULL;

ALTER TABLE  `log_pagamentos` CHANGE  `nu_serie_nf`  `nu_serie_nf` VARCHAR( 3 ) NULL DEFAULT NULL ,
CHANGE  `nu_nota_fiscal`  `nu_nota_fiscal` VARCHAR( 20 ) NULL DEFAULT NULL;

DROP TABLE IF EXISTS `filtros`;
CREATE TABLE IF NOT EXISTS `filtros` (
  `co_filtro` int(11) NOT NULL AUTO_INCREMENT,
  `no_filtro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tp_filtro` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ds_filtro` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_coluna` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_dominio` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_formula` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_condicao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tb_join` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tp_join` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_limpar` int(1) DEFAULT NULL,
  `is_operador` int(1) DEFAULT NULL,
  PRIMARY KEY (`co_filtro`),
  KEY `no_filtro` (`no_filtro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Filtros para Pesquisa Avançada' AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `filtros`
--

INSERT INTO `filtros` (`co_filtro`, `no_filtro`, `tp_filtro`, `ds_filtro`, `no_coluna`, `no_dominio`, `ds_formula`, `ds_condicao`, `tb_join`, `tp_join`, `ds_group`, `is_limpar`, `is_operador`) VALUES
(1, 'valor_mensal', 'money', 'Valor Mensal', 'Contrato.vl_mensal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'numero_processo', 'txt', 'Numero do Processo', 'Contrato.nu_processo', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(3, 'vencendo_dias', 'intrangedate', 'Contratos Vencendo entre (dias)', 'Contrato.dt_fim_vigencia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'fiscal', 'select', 'Fiscal do Contrato', 'Contrato.co_fiscal_atual', 'GrupoAuxiliar', NULL, '', NULL, NULL, NULL, NULL, NULL),
(5, 'data_assinatura', 'date', 'Data de Assinatura', 'Contrato.dt_assinatura', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'fim_vigencia_entre', 'daterange', 'Fim da Vigencia entre', 'Contrato.dt_fim_vigencia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'valor_global_entre', 'moneyrange', 'Valor Global entre', 'Contrato.vl_global', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'numero_contrato', 'txt', 'Numero do Contrato', 'Contrato.nu_contrato', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(9, 'descricao_objeto', 'txt', 'Objeto', 'Contrato.ds_objeto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'fase', 'select', 'Fase de Tramitacao', 'Contrato.co_fase', 'Fase', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'situacao', 'select', 'Situacao', 'Contrato.co_situacao', 'Situacao', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'fornecedor', 'select', 'Fornecedor', 'Contrato.co_fornecedor', 'Fornecedor', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'unidade', 'select', 'Unidade Solicitante', 'Contrato.co_contratante', 'Setor', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'modalidade', 'select', 'Tipo de Contrato', 'Contrato.co_modalidade', 'Modalidade', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'percentual_aditivo', 'int', 'Contratos com Percentual de Aditivos maior que (%)', NULL, 'Aditivo', NULL, NULL, 'aditivos', NULL, 'Contrato.co_contrato HAVING ( sum(Aditivo.vl_aditivo) * 100 / Contrato.vl_global ) > ?', NULL, NULL),
(16, 'dias_pendencia', 'int', 'Pendencias abertas a mais de ( dias )', NULL, 'Pendencia', 'Pendencia.dt_inicio < date_sub(current_date(), interval ? day) and Pendencia.st_pendencia = ''P''', NULL, 'pendencias', NULL, 'Contrato.co_contrato', NULL, NULL),
(17, 'dias_pagamento', 'int', 'Pagamentos em atraso a mais de ( dias )', NULL, 'Pagamento', 'Pagamento.dt_vencimento < date_sub(current_date(), interval ? day) and Pagamento.dt_pagamento is null', NULL, 'pagamentos', NULL, 'Contrato.co_contrato', NULL, NULL),
(18, 'so_pam', 'fixo', 'Exibir somente PAMs', NULL, NULL, 'Contrato.nu_pam != '''' and (Contrato.nu_contrato = '''' or Contrato.nu_contrato is null)', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'so_processo', 'fixo', 'Exibir somente Processos', NULL, NULL, 'Contrato.nu_processo != '''' and (Contrato.nu_contrato = '''' or Contrato.nu_contrato is null)', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'contratos_ativos', 'fixo', 'Exibir Contratos ativos', NULL, NULL, 'Contrato.nu_contrato != '''' and Contrato.dt_fim_vigencia > now() ', NULL, NULL, NULL, NULL, NULL, NULL);

ALTER TABLE  `empenhos` CHANGE  `co_plano_interno`  `co_plano_interno` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `nu_ptres`  `nu_ptres` INT( 10 ) NULL;

ALTER TABLE  `log_empenhos` CHANGE  `co_usuario`  `co_usuario` INT( 11 ) NULL ,
CHANGE  `co_plano_interno`  `co_plano_interno` INT( 11 ) NULL ,
CHANGE  `nu_ptres`  `nu_ptres` INT( 10 ) NULL;

ALTER TABLE  `log_empenhos` CHANGE  `co_plano_interno`  `co_plano_interno` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `atas` ADD  `vl_ata` DECIMAL( 10, 2 ) NULL AFTER  `dt_vigencia_fim`;

ALTER TABLE  `atas` ADD  `ds_justificativa` VARCHAR( 500 ) NULL AFTER  `ds_especificacao`;

ALTER TABLE  `aditivos` CHANGE  `no_aditivo`  `no_aditivo` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `ds_aditivo`  `ds_aditivo` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `log_aditivos` ADD  `no_aditivo` VARCHAR( 255 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `log_aditivos` CHANGE  `ds_aditivo`  `ds_aditivo` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

DROP TRIGGER IF EXISTS `ADD_LOG_ADITIVO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_ADITIVO` AFTER INSERT ON `aditivos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_ADITIVOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
            co_aditivo      = NEW.co_aditivo,
            co_contrato     = NEW.co_contrato,
            no_aditivo      = NEW.no_aditivo,
            dt_aditivo      = NEW.dt_aditivo,
            vl_aditivo      = NEW.vl_aditivo,
            ds_aditivo      = NEW.ds_aditivo;
        END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `UPD_LOG_ADITIVO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_ADITIVO` AFTER UPDATE ON `aditivos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_ADITIVOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
            co_aditivo      = NEW.co_aditivo,
            co_contrato     = NEW.co_contrato,
            no_aditivo      = NEW.no_aditivo,
            dt_aditivo      = NEW.dt_aditivo,
            vl_aditivo      = NEW.vl_aditivo,
            ds_aditivo      = NEW.ds_aditivo;
        END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `DEL_LOG_ADITIVO`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_ADITIVO` AFTER DELETE ON `aditivos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_ADITIVOS SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
            co_aditivo      = OLD.co_aditivo,
            co_contrato     = OLD.co_contrato,
            no_aditivo      = OLD.no_aditivo,
            dt_aditivo      = OLD.dt_aditivo,
            vl_aditivo      = OLD.vl_aditivo,
            ds_aditivo      = OLD.ds_aditivo;
        END
//
DELIMITER ;