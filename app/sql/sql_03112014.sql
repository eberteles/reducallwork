ALTER TABLE  `fases` ADD  `mm_duracao_fase` INT( 12 ) NULL AFTER  `ds_fase`;

CREATE TABLE `numeracao_empenho` (
  `aa_empenho` year(4) NOT NULL DEFAULT '0000',
  `ct_empenho` int(4) NOT NULL,
  PRIMARY KEY (`aa_empenho`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
