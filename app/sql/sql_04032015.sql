  ALTER TABLE  `modalidades` ADD  `tp_modalidade` INT( 2 ) NULL AFTER  `ds_modalidade` ,
  ADD  `tp_prova` INT( 2 ) NULL AFTER  `tp_modalidade`;

  ALTER TABLE  `modalidades` CHANGE  `nu_modalidade`  `nu_modalidade` INT( 11 ) NULL;

  CREATE TABLE IF NOT EXISTS `confederacoes` (
    `co_confederacao` int(11) NOT NULL AUTO_INCREMENT,
    `parent_id` int(11) DEFAULT NULL,
    `ds_confederacao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `ic_cob` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ic_cpb` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `no_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nu_celular` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_complemento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_bairro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `sg_uf` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `co_municipio` int(11) DEFAULT NULL,
    `nu_cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
    `co_usuario` int(11) DEFAULT NULL,
    PRIMARY KEY (`co_confederacao`),
    KEY `co_usuario` (`co_usuario`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

  CREATE TABLE IF NOT EXISTS `log_confederacoes` (
    `co_log_confederacao` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `dt_log` datetime NOT NULL,
    `co_usuario` int(11) DEFAULT NULL,
    `tp_acao` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

    `co_confederacao` int(11) NOT NULL,
    `parent_id` int(11) DEFAULT NULL,
    `ds_confederacao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `ic_cob` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ic_cpb` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `no_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nu_telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nu_celular` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_complemento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ds_bairro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `sg_uf` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `co_municipio` int(11) DEFAULT NULL,
    `nu_cep` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
    KEY `co_confederacao` (`co_confederacao`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;


  DROP TRIGGER IF EXISTS `ADD_LOG_CONFEDERACAO`;
  DELIMITER //
  CREATE TRIGGER `ADD_LOG_CONFEDERACAO` AFTER INSERT ON `confederacoes`
   FOR EACH ROW BEGIN
    INSERT INTO log_confederacoes SET
              dt_log          = NOW(),
              co_usuario      = NEW.co_usuario,
              tp_acao         = 'I',

              co_confederacao = NEW.co_confederacao,
              parent_id       = NEW.parent_id,
              ds_confederacao = NEW.ds_confederacao,
              ic_cob          = NEW.ic_cob,
              ic_cpb          = NEW.ic_cpb,
              no_contato      = NEW.no_contato,
              ds_email        = NEW.ds_email,
              nu_telefone     = NEW.nu_telefone,
              nu_celular      = NEW.nu_celular,
              ds_endereco     = NEW.ds_endereco,
              ds_complemento  = NEW.ds_complemento,
              ds_bairro       = NEW.ds_bairro,
              sg_uf           = NEW.sg_uf,
              co_municipio    = NEW.co_municipio,
              nu_cep          = NEW.nu_cep;
  END
  //
  DELIMITER ;

  DROP TRIGGER IF EXISTS `UPD_LOG_CONFEDERACAO`;
  DELIMITER //
  CREATE TRIGGER `UPD_LOG_CONFEDERACAO` AFTER UPDATE ON `confederacoes`
   FOR EACH ROW BEGIN
    INSERT INTO log_confederacoes SET
              dt_log          = NOW(),
              co_usuario      = NEW.co_usuario,
              tp_acao         = 'A',

              co_confederacao = NEW.co_confederacao,
              parent_id       = NEW.parent_id,
              ds_confederacao = NEW.ds_confederacao,
              ic_cob          = NEW.ic_cob,
              ic_cpb          = NEW.ic_cpb,
              no_contato      = NEW.no_contato,
              ds_email        = NEW.ds_email,
              nu_telefone     = NEW.nu_telefone,
              nu_celular      = NEW.nu_celular,
              ds_endereco     = NEW.ds_endereco,
              ds_complemento  = NEW.ds_complemento,
              ds_bairro       = NEW.ds_bairro,
              sg_uf           = NEW.sg_uf,
              co_municipio    = NEW.co_municipio,
              nu_cep          = NEW.nu_cep;
  END
  //
  DELIMITER ;

  DROP TRIGGER IF EXISTS `DEL_LOG_CONFEDERACAO`;
  DELIMITER //
  CREATE TRIGGER `DEL_LOG_CONFEDERACAO` AFTER DELETE ON `confederacoes`
   FOR EACH ROW BEGIN
    INSERT INTO log_confederacoes SET
              dt_log          = NOW(),
              co_usuario      = OLD.co_usuario,
              tp_acao         = 'E',

              co_confederacao = OLD.co_confederacao,
              parent_id       = OLD.parent_id,
              ds_confederacao = OLD.ds_confederacao,
              ic_cob          = OLD.ic_cob,
              ic_cpb          = OLD.ic_cpb,
              no_contato      = OLD.no_contato,
              ds_email        = OLD.ds_email,
              nu_telefone     = OLD.nu_telefone,
              nu_celular      = OLD.nu_celular,
              ds_endereco     = OLD.ds_endereco,
              ds_complemento  = OLD.ds_complemento,
              ds_bairro       = OLD.ds_bairro,
              sg_uf           = OLD.sg_uf,
              co_municipio    = OLD.co_municipio,
              nu_cep          = OLD.nu_cep;
  END
  //
  DELIMITER ;


  CREATE TABLE `modalidades_confederacoes` (
  `co_modalidade_confederacao` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_confederacao` INT( 11 ) NOT NULL ,
  `co_modalidade` INT( 11 ) NOT NULL
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Vinculação de Modalidade a Confederação';

  ALTER TABLE  `modalidades_confederacoes` ADD INDEX (  `co_confederacao` );
  ALTER TABLE  `modalidades_confederacoes` ADD INDEX (  `co_modalidade` );

  ALTER TABLE  `modalidades_confederacoes` ADD FOREIGN KEY (  `co_confederacao` ) REFERENCES  `confederacoes` (
  `co_confederacao`
  );

  ALTER TABLE  `modalidades_confederacoes` ADD FOREIGN KEY (  `co_modalidade` ) REFERENCES  `modalidades` (
  `co_modalidade`
  );

  CREATE TABLE `provas` (
  `co_prova` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_modalidade` INT( 11 ) NOT NULL ,
  `ds_prova` VARCHAR( 500 ) NOT NULL ,
  `is_sexo` CHAR( 1 ) NULL DEFAULT  '0' COMMENT  'Prova separada por sexo?',
  INDEX (  `co_modalidade` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Provas da Modalidade';

  ALTER TABLE  `provas` ADD FOREIGN KEY (  `co_modalidade` ) REFERENCES  `modalidades` (
  `co_modalidade`
  );

  CREATE TABLE `provas_categorias` (
  `co_prova_categoria` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_prova` INT( 11 ) NOT NULL ,
  `ds_prova_categoria` VARCHAR( 255 ) NOT NULL ,
  INDEX (  `co_prova` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

  ALTER TABLE  `provas_categorias` ADD FOREIGN KEY (  `co_prova` ) REFERENCES  `provas` (
  `co_prova`
  );

  CREATE TABLE `provas_subcategorias` (
  `co_prova_subcategoria` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_prova` INT( 11 ) NOT NULL ,
  `ds_prova_subcategoria` VARCHAR( 255 ) NOT NULL ,
  INDEX (  `co_prova` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

  ALTER TABLE  `provas_subcategorias` ADD FOREIGN KEY (  `co_prova` ) REFERENCES  `provas` (
  `co_prova`
  );

  CREATE TABLE `provas_classificacoes` (
  `co_prova_classificacao` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_prova` INT( 11 ) NOT NULL ,
  `ds_prova_classificacao` VARCHAR( 255 ) NOT NULL ,
  INDEX (  `co_prova` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

  ALTER TABLE  `provas_classificacoes` ADD FOREIGN KEY (  `co_prova` ) REFERENCES  `provas` (
  `co_prova`
  );

  CREATE TABLE  `eventos` (
  `co_evento` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_confederacao` INT( 11 ) NOT NULL ,
  `co_usuario` INT( 11 ) NOT NULL ,
  `co_modalidade` INT( 11 ) NOT NULL ,
  `co_prova` INT( 11 ) NOT NULL ,
  `co_prova_categoria` INT( 11 ) NULL ,
  `co_prova_subcategoria` INT( 11 ) NULL ,
  `co_prova_classificacao` INT( 11 ) NULL ,
  `co_categoria` INT( 11 ) NOT NULL COMMENT  'Tipo de Evento',
  `nu_ano` INT( 4 ) NOT NULL ,
  `ds_evento` VARCHAR( 500 ) NOT NULL ,
  `ic_evento` CHAR( 1 ) NOT NULL DEFAULT  'N',
  `dt_inicio` DATE NOT NULL ,
  `dt_fim` DATE NOT NULL ,
  `ds_local` VARCHAR( 500 ) NOT NULL ,
  `ic_sexo` CHAR( 1 ) NULL
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Eventos';

  ALTER TABLE  `eventos` ADD INDEX (  `co_confederacao` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_usuario` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_modalidade` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_prova` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_prova_categoria` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_prova_subcategoria` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_prova_classificacao` );
  ALTER TABLE  `eventos` ADD INDEX (  `co_categoria` );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_confederacao` ) REFERENCES  `confederacoes` (
  `co_confederacao`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
  `co_usuario`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_modalidade` ) REFERENCES  `modalidades` (
  `co_modalidade`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_prova` ) REFERENCES  `provas` (
  `co_prova`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_prova_categoria` ) REFERENCES  `provas_categorias` (
  `co_prova_categoria`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_prova_subcategoria` ) REFERENCES  `provas_subcategorias` (
  `co_prova_subcategoria`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_prova_classificacao` ) REFERENCES  `provas_classificacoes` (
  `co_prova_classificacao`
  );

  ALTER TABLE  `eventos` ADD FOREIGN KEY (  `co_categoria` ) REFERENCES  `categorias` (
  `co_categoria`
  );

  CREATE TABLE  `log_eventos` (
  `co_log_evento` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) DEFAULT NULL,
  `tp_acao` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_evento` INT( 11 ) NOT NULL,
  `co_confederacao` INT( 11 ) NOT NULL ,
  `co_modalidade` INT( 11 ) NOT NULL ,
  `co_prova` INT( 11 ) NOT NULL ,
  `co_prova_categoria` INT( 11 ) NULL ,
  `co_prova_subcategoria` INT( 11 ) NULL ,
  `co_prova_classificacao` INT( 11 ) NULL ,
  `co_categoria` INT( 11 ) NOT NULL COMMENT  'Tipo de Evento',
  `nu_ano` INT( 4 ) NOT NULL ,
  `ds_evento` VARCHAR( 500 ) NOT NULL ,
  `ic_evento` CHAR( 1 ) NOT NULL DEFAULT  'N',
  `dt_inicio` DATE NOT NULL ,
  `dt_fim` DATE NOT NULL ,
  `ds_local` VARCHAR( 500 ) NOT NULL ,
  `ic_sexo` CHAR( 1 ) NULL ,
  INDEX (  `co_confederacao` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela Log de Eventos';


  DROP TRIGGER IF EXISTS `ADD_LOG_EVENTO`;
  DELIMITER //
  CREATE TRIGGER `ADD_LOG_EVENTO` AFTER INSERT ON `eventos`
   FOR EACH ROW BEGIN
    INSERT INTO log_eventos SET
              dt_log          = NOW(),
              co_usuario      = NEW.co_usuario,
              tp_acao         = 'I',

              co_evento       = NEW.co_evento,
              co_confederacao = NEW.co_confederacao,
              co_modalidade   = NEW.co_modalidade,
              co_prova        = NEW.co_prova,
              co_prova_categoria = NEW.co_prova_categoria,
              co_prova_subcategoria = NEW.co_prova_subcategoria,
              co_prova_classificacao = NEW.co_prova_classificacao,
              co_categoria    = NEW.co_categoria,
              nu_ano          = NEW.nu_ano,
              ds_evento       = NEW.ds_evento,
              ic_evento       = NEW.ic_evento,
              dt_inicio       = NEW.dt_inicio,
              dt_fim          = NEW.dt_fim,
              ds_local        = NEW.ds_local,
              ic_sexo         = NEW.ic_sexo;
  END
  //
  DELIMITER ;

  DROP TRIGGER IF EXISTS `UPD_LOG_EVENTO`;
  DELIMITER //
  CREATE TRIGGER `UPD_LOG_EVENTO` AFTER UPDATE ON `eventos`
   FOR EACH ROW BEGIN
    INSERT INTO log_eventos SET
              dt_log          = NOW(),
              co_usuario      = NEW.co_usuario,
              tp_acao         = 'A',

              co_evento       = NEW.co_evento,
              co_confederacao = NEW.co_confederacao,
              co_modalidade   = NEW.co_modalidade,
              co_prova        = NEW.co_prova,
              co_prova_categoria = NEW.co_prova_categoria,
              co_prova_subcategoria = NEW.co_prova_subcategoria,
              co_prova_classificacao = NEW.co_prova_classificacao,
              co_categoria    = NEW.co_categoria,
              nu_ano          = NEW.nu_ano,
              ds_evento       = NEW.ds_evento,
              ic_evento       = NEW.ic_evento,
              dt_inicio       = NEW.dt_inicio,
              dt_fim          = NEW.dt_fim,
              ds_local        = NEW.ds_local,
              ic_sexo         = NEW.ic_sexo;
  END
  //
  DELIMITER ;

  DROP TRIGGER IF EXISTS `DEL_LOG_EVENTO`;
  DELIMITER //
  CREATE TRIGGER `DEL_LOG_EVENTO` AFTER DELETE ON `eventos`
   FOR EACH ROW BEGIN
    INSERT INTO log_eventos SET
              dt_log          = NOW(),
              co_usuario      = OLD.co_usuario,
              tp_acao         = 'E',

              co_evento       = OLD.co_evento,
              co_confederacao = OLD.co_confederacao,
              co_modalidade   = OLD.co_modalidade,
              co_prova        = OLD.co_prova,
              co_prova_categoria = OLD.co_prova_categoria,
              co_prova_subcategoria = OLD.co_prova_subcategoria,
              co_prova_classificacao = OLD.co_prova_classificacao,
              co_categoria    = OLD.co_categoria,
              nu_ano          = OLD.nu_ano,
              ds_evento       = OLD.ds_evento,
              ic_evento       = OLD.ic_evento,
              dt_inicio       = OLD.dt_inicio,
              dt_fim          = OLD.dt_fim,
              ds_local        = OLD.ds_local,
              ic_sexo         = OLD.ic_sexo;
  END
  //
  DELIMITER ;

  CREATE TABLE paises (co_pais VARCHAR(2) NOT NULL, no_pais VARCHAR(64) NOT NULL, PRIMARY KEY(co_pais)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

  CREATE TABLE `eventos_participantes` (
  `co_evento_participante` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_evento` INT( 11 ) NOT NULL ,
  `sg_uf` VARCHAR( 2 ) NULL ,
  `co_pais` VARCHAR( 2 ) NULL ,
  INDEX (  `co_evento` ),
  INDEX (  `sg_uf` ),
  INDEX (  `co_pais` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

  ALTER TABLE  `eventos_participantes` ADD FOREIGN KEY (  `co_evento` ) REFERENCES  `eventos` (
  `co_evento`
  );
  ALTER TABLE  `eventos_participantes` ADD FOREIGN KEY (  `sg_uf` ) REFERENCES  `ufs` (
  `sg_uf`
  );
  ALTER TABLE  `eventos_participantes` ADD FOREIGN KEY (  `co_pais` ) REFERENCES  `paises` (
  `co_pais`
  );

  CREATE TABLE `eventos_competidores` (
  `co_evento_competidor` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_evento` INT( 11 ) NOT NULL ,
  `sg_uf` VARCHAR( 2 ) NULL ,
  `co_pais` VARCHAR( 2 ) NULL ,
  INDEX (  `co_evento` ),
  INDEX (  `sg_uf` ),
  INDEX (  `co_pais` )
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

  ALTER TABLE  `eventos_competidores` ADD FOREIGN KEY (  `co_evento` ) REFERENCES  `eventos` (
  `co_evento`
  );
  ALTER TABLE  `eventos_competidores` ADD FOREIGN KEY (  `sg_uf` ) REFERENCES  `ufs` (
  `sg_uf`
  );
  ALTER TABLE  `eventos_competidores` ADD FOREIGN KEY (  `co_pais` ) REFERENCES  `paises` (
  `co_pais`
  );

  CREATE TABLE  `eventos_resultados` (
  `co_evento_resultado` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `co_evento` INT( 11 ) NOT NULL ,
  `co_fornecedor` INT( 11 ) NOT NULL ,
  `nu_classificacao` INT( 3 ) NOT NULL
  ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Resultados do Evento';

  ALTER TABLE  `eventos_resultados` ADD INDEX (  `co_evento` );
  ALTER TABLE  `eventos_resultados` ADD INDEX (  `co_fornecedor` );

  ALTER TABLE  `eventos_resultados` ADD FOREIGN KEY (  `co_evento` ) REFERENCES  `eventos` (
  `co_evento`
  );

  ALTER TABLE  `eventos_resultados` ADD FOREIGN KEY (  `co_fornecedor` ) REFERENCES  `fornecedores` (
  `co_fornecedor`
  );

  ALTER TABLE  `anexos` ADD  `co_evento` INT( 11 ) NULL AFTER  `co_contrato` ,
  ADD INDEX (  `co_evento` );

  ALTER TABLE  `anexos` ADD FOREIGN KEY (  `co_evento` ) REFERENCES  `eventos` (
  `co_evento`
  );

  ALTER TABLE  `historicos` ADD  `co_evento` INT( 11 ) NULL AFTER  `co_contrato` ,
  ADD INDEX (  `co_evento` );

  ALTER TABLE  `historicos` ADD FOREIGN KEY (  `co_evento` ) REFERENCES  `eventos` (
  `co_evento`
  );
