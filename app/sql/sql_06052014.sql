CREATE TABLE `notas_fiscais` (
`co_nota` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`nu_nota` VARCHAR( 15 ) NOT NULL ,
`nu_serie` VARCHAR( 4 ) NULL ,
`dt_recebimento` DATE NOT NULL ,
`vl_nota` DECIMAL( 10, 2 ) NOT NULL ,
`ds_nota` VARCHAR( 255 ) NULL ,
`ic_atesto` INT( 1 ) NULL ,
`dt_atesto` DATE NULL ,
`ds_atesto` VARCHAR( 255 ) NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
INDEX (  `co_contrato` ),
INDEX (  `co_usuario` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Notas Fiscais / Atestos';

ALTER TABLE  `notas_fiscais` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES `contratos` (
`co_contrato`
);

ALTER TABLE  `notas_fiscais` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES `usuarios` (
`co_usuario`
);

CREATE TABLE `penalidades` (
`co_penalidade` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_nota` INT( 11 ) NULL ,
`dt_penalidade` DATE NOT NULL ,
`ds_observacao` VARCHAR( 255 ) NULL ,
`ic_situacao` INT( 1 ) NOT NULL ,
`mt_penalidade` VARCHAR( 255 ) NULL ,
`co_usuario` INT( 11 ) NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de Penalidades';
ALTER TABLE  `penalidades` ADD INDEX (  `co_contrato` );
ALTER TABLE  `penalidades` ADD INDEX (  `co_nota` );
ALTER TABLE  `penalidades` ADD INDEX (  `co_usuario` );

ALTER TABLE  `penalidades` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES `contratos` (
`co_contrato`
);

ALTER TABLE  `penalidades` ADD FOREIGN KEY (  `co_nota` ) REFERENCES `notas_fiscais` (
`co_nota`
);

ALTER TABLE  `penalidades` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES `usuarios` (
`co_usuario`
);

CREATE TABLE `log_penalidades` (
`co_log_penalidades` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`dt_log` DATETIME NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
`tp_acao` CHAR( 1 ) NOT NULL COMMENT  'L, C, A, E, I',
`co_penalidade` INT( 11 ) NOT NULL ,
`co_contrato` INT( 11 ) NOT NULL ,
`co_nota` INT( 11 ) NULL ,
`dt_penalidade` DATE NOT NULL ,
`ds_observacao` VARCHAR( 255 ) NULL ,
`ic_situacao` INT( 1 ) NOT NULL ,
`mt_penalidade` VARCHAR( 255 ) NULL 
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Log de Penalidades';

ALTER TABLE  `log_penalidades` ADD INDEX (  `co_usuario` );
ALTER TABLE  `log_penalidades` ADD INDEX (  `co_penalidade` );
ALTER TABLE  `log_penalidades` ADD INDEX (  `co_contrato` );
ALTER TABLE  `log_penalidades` ADD INDEX (  `co_nota` );

DROP TRIGGER IF EXISTS `ADD_LOG_PENALIDADE`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_PENALIDADE` AFTER INSERT ON `penalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_penalidades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
            co_penalidade   = NEW.co_penalidade,
            co_contrato     = NEW.co_contrato,
            co_nota         = NEW.co_nota,
            dt_penalidade   = NEW.dt_penalidade,
            ds_observacao   = NEW.ds_observacao,
            ic_situacao     = NEW.ic_situacao,
            mt_penalidade   = NEW.mt_penalidade;
        END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `UPD_LOG_PENALIDADE`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_PENALIDADE` AFTER UPDATE ON `penalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_penalidades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
            co_penalidade   = NEW.co_penalidade,
            co_contrato     = NEW.co_contrato,
            co_nota         = NEW.co_nota,
            dt_penalidade   = NEW.dt_penalidade,
            ds_observacao   = NEW.ds_observacao,
            ic_situacao     = NEW.ic_situacao,
            mt_penalidade   = NEW.mt_penalidade;
        END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `DEL_LOG_PENALIDADE`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_PENALIDADE` AFTER DELETE ON `penalidades`
 FOR EACH ROW BEGIN
	INSERT INTO log_penalidades SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
            co_penalidade   = OLD.co_penalidade,
            co_contrato     = OLD.co_contrato,
            co_nota         = OLD.co_nota,
            dt_penalidade   = OLD.dt_penalidade,
            ds_observacao   = OLD.ds_observacao,
            ic_situacao     = OLD.ic_situacao,
            mt_penalidade   = OLD.mt_penalidade;
        END
//
DELIMITER ;

ALTER TABLE  `anexos` ADD  `co_nota` INT( 11 ) NULL AFTER  `co_contrato_fiscal` ,
ADD INDEX (  `co_nota` );

ALTER TABLE  `contratos` ADD  `dt_tais` DATE NULL AFTER  `dt_cadastro_processo`;