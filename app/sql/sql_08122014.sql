ALTER TABLE  `contratos` ADD  `co_instituicao` INT( 11 ) NULL AFTER  `co_contrato` ,
ADD INDEX (  `co_instituicao` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `co_instituicao` ) REFERENCES `instituicoes` (
`co_instituicao`
);

ALTER TABLE  `log_contratos` ADD  `co_instituicao` INT( 11 ) NULL AFTER  `co_contrato` ,
ADD INDEX (  `co_instituicao` );