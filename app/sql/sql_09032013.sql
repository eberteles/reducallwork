SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE  TABLE IF NOT EXISTS `layouts` (
  `co_registro` INT(11) NOT NULL COMMENT 'Sequencial de registros' ,
  `co_layout` VARCHAR(5) NOT NULL COMMENT 'Código do registro de layout. Ex.: XX999 - onde XX é a sigla do sistema e 999 é o sequencial do layout.' ,
  `ds_campo` VARCHAR(40) NOT NULL COMMENT 'Descrição/Nome do campo do layout' ,
  `no_tam_campo` INT(11) NOT NULL COMMENT 'Tamanho do campo no layout TXT em caracteres. Este campo é um facilitador para a linguagem PHP.' ,
  `no_ini_campo` INT(11) NOT NULL COMMENT 'Posição inicial do campo no layout' ,
  `no_fim_campo` INT(11) NOT NULL COMMENT 'Posição final do campo no layout' ,
  PRIMARY KEY (`co_registro`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `movimentos` (
  `co_movimento` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do movimento' ,
  `cs_sucesso` CHAR(1) NOT NULL COMMENT 'Concluído como sucesso \'S\' ou \'N\'' ,
  `dt_inicio` DATETIME NOT NULL COMMENT 'Data e hora do início do processamento' ,
  `dt_fim` DATETIME NULL DEFAULT NULL COMMENT 'Data e hora do fim do processamento' ,
  `ds_nome_arquivo` VARCHAR(100) NOT NULL COMMENT 'Nome do arquivo que foi lido' ,
  PRIMARY KEY (`co_movimento`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `log_criticas` (
  `co_critica` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Código da crítica no movimento' ,
  `ds_critica` VARCHAR(200) NOT NULL COMMENT 'Descrição da crítica' ,
  `dt_ocorrencia` DATETIME NOT NULL COMMENT 'Data e hora da ocorrênciada crítica no movimento' ,
  `co_movimento` INT(11) NOT NULL ,
  `nu_linha` INT(11) NULL DEFAULT NULL COMMENT 'número da linha que ocorreu o erro.' ,
  PRIMARY KEY (`co_critica`) ,
  INDEX `fk_log_criticas_movimentos1` (`co_movimento` ASC) ,
  CONSTRAINT `fk_log_criticas_movimentos1`
    FOREIGN KEY (`co_movimento` )
    REFERENCES `movimentos` (`co_movimento` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
