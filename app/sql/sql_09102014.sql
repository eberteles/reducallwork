ALTER TABLE  `contratos` CHANGE  `co_contratante`  `co_contratante` INT( 11 ) NULL DEFAULT NULL;

ALTER TABLE  `contratos` ADD  `co_executante` INT( 11 ) NULL AFTER  `co_contratante` ,
ADD INDEX (  `co_executante` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `co_executante` ) REFERENCES `setores` (
`co_setor`
);
