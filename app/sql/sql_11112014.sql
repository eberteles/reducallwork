ALTER TABLE  `log_contratos` CHANGE  `nu_pam`  `nu_pam` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE  `log_contratos` CHANGE  `nu_processo`  `nu_processo` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `log_contratos` CHANGE  `co_contratante`  `co_contratante` INT( 11 ) NULL DEFAULT NULL;