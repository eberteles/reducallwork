CREATE TABLE `atas_lotes` (
`co_ata_lote` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_ata` INT( 11 ) NOT NULL ,
`nu_lote` VARCHAR( 10 ) NOT NULL ,
`ds_objeto` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `co_ata_lote` ) ,
INDEX (  `co_ata` )
) ENGINE = INNODB COMMENT =  'Tabela de Lotes de uma Ata';

ALTER TABLE  `atas_lotes` ADD FOREIGN KEY (  `co_ata` ) REFERENCES `atas` (
`co_ata`
);

CREATE TABLE `atas_itens` (
`co_ata_item` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_ata` INT( 11 ) NOT NULL ,
`co_ata_lote` INT( 11 ) NULL ,
`nu_item` INT( 5 ) NOT NULL ,
`ds_marca` VARCHAR( 255 ) NULL ,
`ds_descricao` VARCHAR( 500 ) NOT NULL ,
`ds_unidade` VARCHAR( 20 ) NOT NULL ,
`nu_quantidade` INT( 5 ) NOT NULL ,
`qt_utilizado` INT( 5 ) NOT NULL DEFAULT  '0' , 
`vl_unitario` DECIMAL( 10, 2 ) NOT NULL ,
PRIMARY KEY (  `co_ata_item` ) ,
INDEX (  `co_ata` ,  `co_ata_lote` )
) ENGINE = INNODB COMMENT =  'Tabela com os Itens das Atas';

ALTER TABLE  `atas_itens` ADD FOREIGN KEY (  `co_ata` ) REFERENCES `atas` (
`co_ata`
);

ALTER TABLE  `atas_itens` ADD FOREIGN KEY (  `co_ata_lote` ) REFERENCES `atas_lotes` (
`co_ata_lote`
);

ALTER TABLE  `anexos` ADD  `co_ata` INT( 11 ) NULL AFTER  `co_anexo` ,
ADD INDEX (  `co_ata` );

ALTER TABLE  `anexos` CHANGE  `co_contrato`  `co_contrato` INT( 11 ) NULL;

CREATE TABLE `atas_pedidos` (
`co_ata_pedido` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_ata` INT( 11 ) NOT NULL ,
`co_contrato` INT( 11 ) NULL ,
`ds_pedido` VARCHAR( 255 ) NOT NULL , 
`vl_pedido` DECIMAL( 10, 2 ) NOT NULL ,
`tp_pedido` CHAR( 1 ) NOT NULL ,
`dt_pedido` DATE NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
PRIMARY KEY (  `co_ata_pedido` ) ,
INDEX (  `co_ata` ,  `co_contrato` ,  `co_usuario` )
) ENGINE = INNODB COMMENT =  'Tabela com os Pedidos das Atas';

ALTER TABLE  `atas_pedidos` ADD FOREIGN KEY (  `co_ata` ) REFERENCES `atas` (
`co_ata`
);

ALTER TABLE  `atas_pedidos` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES `contratos` (
`co_contrato`
);

ALTER TABLE  `atas_pedidos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES `usuarios` (
`co_usuario`
);

ALTER TABLE  `pendencias` CHANGE  `co_contrato`  `co_contrato` INT( 11 ) NULL;

ALTER TABLE  `pendencias` ADD  `co_ata` INT( 11 ) NULL AFTER  `co_pendencia` ,
ADD INDEX (  `co_ata` );

ALTER TABLE  `pendencias` ADD FOREIGN KEY (  `co_ata` ) REFERENCES `atas` (
`co_ata`
);

ALTER TABLE  `historicos` CHANGE  `co_contrato`  `co_contrato` INT( 11 ) NULL;

ALTER TABLE  `historicos` ADD  `co_ata` INT( 11 ) NULL AFTER  `co_historico` ,
ADD INDEX (  `co_ata` );

ALTER TABLE  `log_historicos` CHANGE  `co_contrato`  `co_contrato` INT( 11 ) NULL;

ALTER TABLE  `log_historicos` ADD  `co_ata` INT( 11 ) NULL AFTER  `co_historico`;

DROP TRIGGER IF EXISTS `ADD_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_HISTORICOS` AFTER INSERT ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
			
            co_historico    = NEW.co_historico,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            no_assunto      = NEW.no_assunto,
            ds_historico    = NEW.ds_historico,
            dt_historico    = NEW.dt_historico,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_HISTORICOS` AFTER UPDATE ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
			 			
            co_historico    = NEW.co_historico,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            no_assunto      = NEW.no_assunto,
            ds_historico    = NEW.ds_historico,
            dt_historico    = NEW.dt_historico,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_HISTORICOS` AFTER DELETE ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
			 			
            co_historico    = OLD.co_historico,
            co_ata          = OLD.co_ata,
            co_contrato     = OLD.co_contrato,
            no_assunto      = OLD.no_assunto,
            ds_historico    = OLD.ds_historico,
            dt_historico    = OLD.dt_historico,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

CREATE TABLE `pedidos_itens` (
`co_pedido_item` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_ata_pedido` INT( 11 ) NOT NULL ,
`co_ata_item` INT( 11 ) NOT NULL, 
`co_ata_lote` INT( 11 ) NULL ,
`nu_item` INT( 5 ) NOT NULL ,
`ds_marca` VARCHAR( 255 ) NULL ,
`ds_descricao` VARCHAR( 500 ) NOT NULL ,
`ds_unidade` VARCHAR( 20 ) NOT NULL ,
`qt_pedido` INT( 5 ) NOT NULL ,
`vl_unitario` DECIMAL( 10, 2 ) NOT NULL ,
PRIMARY KEY (  `co_pedido_item` ) ,
INDEX (  `co_ata_pedido` , `co_ata_item`,  `co_ata_lote` )
) ENGINE = INNODB COMMENT =  'Tabela com os Itens dos Pedidos';

ALTER TABLE  `pedidos_itens` ADD FOREIGN KEY (  `co_ata_pedido` ) REFERENCES `atas_pedidos` (
`co_ata_pedido`
);

ALTER TABLE  `pedidos_itens` ADD FOREIGN KEY (  `co_ata_item` ) REFERENCES  `atas_itens` (
`co_ata_item`
);

ALTER TABLE  `pedidos_itens` ADD FOREIGN KEY (  `co_ata_lote` ) REFERENCES `atas_lotes` (
`co_ata_lote`
);