CREATE TABLE  `atas_categorias` (
`co_ata_categoria` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`ds_ata_categoria` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `co_ata_categoria` )
) ENGINE = INNODB COMMENT =  'Tabela de Categorias de Atas';

CREATE TABLE  `atas` (
`co_ata` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`co_categoria_ata` INT( 11 ) NOT NULL ,
`co_fornecedor` INT( 11 ) NOT NULL ,
`nu_ata` VARCHAR( 9 ) NOT NULL ,
`nu_processo` VARCHAR( 20 ) NOT NULL ,
`ds_publicacao` VARCHAR( 255 ) NOT NULL ,
`tp_ata` CHAR( 1 ) NOT NULL ,
`dt_vigencia_inicio` DATE NOT NULL ,
`dt_vigencia_fim` DATE NOT NULL ,
`vl_mensal` DECIMAL( 10, 2 ) NOT NULL ,
`vl_anual` DECIMAL( 10, 2 ) NOT NULL ,
`tp_status` CHAR( 1 ) NOT NULL ,
`dt_cadastro` DATE NOT NULL ,
`co_usuario` INT( 11 ) NOT NULL ,
PRIMARY KEY (  `co_ata` )
) ENGINE = INNODB COMMENT =  'Tabela Módulo de Atas';