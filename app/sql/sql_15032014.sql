CREATE TABLE  `fases` (
`co_fase` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ds_fase` VARCHAR( 200 ) NOT NULL  
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Fases de Tramitação do Processo';

ALTER TABLE  `contratos` CHANGE  `co_situacao`  `co_situacao` INT( 11 ) NULL;
ALTER TABLE  `log_contratos` CHANGE  `co_situacao`  `co_situacao` INT( 11 ) NULL;

ALTER TABLE  `contratos` CHANGE  `st_repactuado`  `st_repactuado` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE  `log_contratos` CHANGE  `st_repactuado`  `st_repactuado` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `contratacoes` ADD  `co_siasg` VARCHAR( 2 ) NULL COMMENT  '01 – Convite, 02 – Tomada de Preço, 03 – Concorrência, 04 – Concorrência Internacional, 05 – Pregão, 20 – Concurso';

ALTER TABLE  `contratos` ADD  `nu_licitacao` VARCHAR( 9 ) NULL AFTER  `ds_objeto` ,
ADD  `dt_publicacao` DATE NULL AFTER  `nu_licitacao` ,
ADD  `ds_fundamento_legal` VARCHAR( 255 ) NULL AFTER  `dt_publicacao` ,
ADD  `dt_assinatura` DATE NULL AFTER  `ds_fundamento_legal`;

ALTER TABLE  `contratos` CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_contratos_fiscais` CHANGE  `dt_fim`  `dt_fim` DATE NULL;

ALTER TABLE  `anexos` CHANGE  `dt_anexo`  `dt_anexo` DATE NULL;

DROP FUNCTION IF EXISTS `mask`;
DELIMITER //

CREATE FUNCTION mask (unformatted_value BIGINT, format_string CHAR(32))
RETURNS CHAR(32) DETERMINISTIC

BEGIN
# Declare variables
DECLARE input_len TINYINT;
DECLARE output_len TINYINT;
DECLARE temp_char CHAR;

# Initialize variables
SET input_len = LENGTH(unformatted_value);
SET output_len = LENGTH(format_string);

# Construct formated string
WHILE ( output_len > 0 ) DO

SET temp_char = SUBSTR(format_string, output_len, 1);
IF ( temp_char = '#' ) THEN
IF ( input_len > 0 ) THEN
SET format_string = INSERT(format_string, output_len, 1, SUBSTR(unformatted_value, input_len, 1));
SET input_len = input_len - 1;
ELSE
SET format_string = INSERT(format_string, output_len, 1, '0');
END IF;
END IF;

SET output_len = output_len - 1;
END WHILE;

RETURN format_string;
END //

DELIMITER ;

ALTER TABLE  `log_aditivos` CHANGE  `vl_aditivo`  `vl_aditivo` DECIMAL( 10, 2 ) NULL;

ALTER TABLE  `aditivos` ADD  `dt_fim_vigencia_anterior` DATE NULL AFTER  `vl_aditivo`;

ALTER TABLE  `contratos` ADD  `dt_fim_vigencia_inicio` DATE NULL AFTER  `dt_fim_vigencia`;

ALTER TABLE  `aditivos` ADD  `no_aditivo` VARCHAR( 100 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `fornecedores` CHANGE  `nu_rg_responsavel`  `nu_rg_responsavel` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

CREATE TABLE  `fluxos` (
`nu_sequencia` INT( 11 ) NOT NULL ,
`tp_operador` INT( 2 ) NULL ,
`co_fase` INT( 11 ) NULL ,
`co_setor` INT( 11 ) NULL ,
`nu_sequencia_um` INT( 11 ) NULL ,
`nu_sequencia_dois` INT( 11 ) NULL ,
PRIMARY KEY (  `nu_sequencia` ) ,
INDEX (  `co_fase` ,  `co_setor` )
) ENGINE = INNODB COMMENT =  'Tabela de Fluxo Processual';

ALTER TABLE  `fluxos` ADD FOREIGN KEY (  `co_fase` ) REFERENCES  `fases` (
`co_fase`
);

ALTER TABLE  `fluxos` ADD FOREIGN KEY (  `co_setor` ) REFERENCES  `setores` (
`co_setor`
);

ALTER TABLE  `contratos` ADD  `nu_pam` VARCHAR( 9 ) NULL AFTER  `co_gestor_atual` ,
ADD INDEX (  `nu_pam` );

ALTER TABLE  `contratos` CHANGE  `nu_processo`  `nu_processo` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_contratos` CHANGE  `nu_processo`  `nu_processo` VARCHAR( 17 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_contratos` ADD  `nu_pam` VARCHAR( 11 ) NULL AFTER  `co_gestor_atual` ,
ADD INDEX (  `nu_pam` );

ALTER TABLE  `contratos` ADD INDEX (  `nu_contrato` );

ALTER TABLE  `contratos` ADD INDEX (  `nu_processo` );

ALTER TABLE  `log_contratos` ADD INDEX (  `nu_contrato` );

ALTER TABLE  `log_contratos` ADD INDEX (  `nu_processo` );

ALTER TABLE  `anexos` ADD  `ds_extensao` VARCHAR( 4 ) NULL;

ALTER TABLE  `contratos` ADD  `dt_autorizacao_pam` DATE NULL AFTER  `ds_fundamento_legal`;

ALTER TABLE  `log_contratos` ADD  `dt_autorizacao_pam` DATE NULL AFTER  `ds_fundamento_legal`;

ALTER TABLE  `contratos` CHANGE  `ds_objeto`  `ds_objeto` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_contratos` CHANGE  `ds_objeto`  `ds_objeto` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_contratos` ADD  `nu_licitacao` VARCHAR( 9 ) NULL AFTER  `ds_objeto` ,
ADD  `dt_publicacao` DATE NULL AFTER  `nu_licitacao` ,
ADD  `ds_fundamento_legal` VARCHAR( 255 ) NULL AFTER  `dt_publicacao` ,
ADD  `dt_assinatura` DATE NULL AFTER  `ds_fundamento_legal`;

ALTER TABLE  `log_contratos` CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `contratos` ADD  `nu_sequencia` INT( 11 ) NULL AFTER  `co_gestor_atual` ,
ADD  `co_fase` INT( 11 ) NULL AFTER  `nu_sequencia` ,
ADD INDEX (  `co_fase` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `co_fase` ) REFERENCES  `fases` (
`co_fase`
);

ALTER TABLE  `log_contratos` ADD  `nu_sequencia` INT( 11 ) NULL AFTER  `co_gestor_atual` ,
ADD  `co_fase` INT( 11 ) NULL AFTER  `nu_sequencia` ,
ADD INDEX (  `co_fase` );

ALTER TABLE  `contratos` ADD INDEX (  `nu_sequencia` );

ALTER TABLE  `log_contratos` ADD INDEX (  `nu_sequencia` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `nu_sequencia` ) REFERENCES  `fluxos` (
`nu_sequencia`
);

INSERT INTO  `privilegios` (
`co_privilegio` ,
`sg_privilegio` ,
`ds_privilegio`
)
VALUES (
'5',  'Setor',  'Setor'
);

ALTER TABLE  `contratos` ADD  `co_setor` INT( 11 ) NULL AFTER  `co_fase` ,
ADD INDEX (  `co_setor` );

ALTER TABLE  `log_contratos` ADD  `co_setor` INT( 11 ) NULL AFTER  `co_fase` ,
ADD INDEX (  `co_setor` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `co_setor` ) REFERENCES  `setores` (
`co_setor`
);

ALTER TABLE  `contratos` CHANGE  `co_contratante`  `co_contratante` INT( 10 ) NULL;

ALTER TABLE  `log_contratos` CHANGE  `co_contratante`  `co_contratante` INT( 10 ) NULL;

ALTER TABLE  `contratos` ADD  `dt_fase` DATE NULL AFTER  `co_fase`;

ALTER TABLE  `log_contratos` ADD  `dt_fase` DATE NULL AFTER  `co_fase`;

ALTER TABLE  `andamentos` ADD  `nu_sequencia` INT( 11 ) NULL AFTER  `co_usuario` ,
ADD INDEX (  `nu_sequencia` );

ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `nu_sequencia` ) REFERENCES  `fluxos` (
`nu_sequencia`
);

ALTER TABLE  `andamentos` ADD  `dt_fim` DATE NULL AFTER  `dt_andamento`;

ALTER TABLE  `andamentos` CHANGE  `ds_andamento`  `ds_andamento` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `contratos` ADD  `tp_aquisicao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `nu_processo` ,
ADD  `ic_ata_vigente` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `tp_aquisicao` ,
ADD  `ic_ata_orgao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `ic_ata_vigente` ,
ADD  `ic_dispensa_licitacao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `ic_ata_orgao`;

ALTER TABLE  `log_contratos` ADD  `tp_aquisicao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `nu_processo` ,
ADD  `ic_ata_vigente` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `tp_aquisicao` ,
ADD  `ic_ata_orgao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `ic_ata_vigente` ,
ADD  `ic_dispensa_licitacao` VARCHAR( 1 ) NULL COMMENT  'S ou N' AFTER  `ic_ata_orgao`;

ALTER TABLE  `usuarios` ADD  `ic_acesso` INT( 1 ) DEFAULT  '0' AFTER  `co_gestor`;

ALTER TABLE  `usuarios` CHANGE  `co_privilegio`  `co_privilegio` INT( 11 ) NULL;

ALTER TABLE  `usuarios` CHANGE  `no_usuario`  `no_usuario` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `usuarios` CHANGE  `ds_senha`  `ds_senha` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `usuarios` CHANGE  `dt_liberacao`  `dt_liberacao` DATETIME NULL;

ALTER TABLE  `usuarios` CHANGE  `ds_privilegios`  `ds_privilegios` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `contratos` CHANGE  `dt_cadastro`  `dt_cadastro` TIMESTAMP NULL;

ALTER TABLE  `contratos` ADD  `dt_cadastro_pam` TIMESTAMP NULL AFTER  `dt_cadastro` ,
ADD  `dt_cadastro_processo` TIMESTAMP NULL AFTER  `dt_cadastro_pam`;

ALTER TABLE  `log_contratos` CHANGE  `dt_cadastro`  `dt_cadastro` TIMESTAMP NULL;

ALTER TABLE  `log_contratos` ADD  `dt_cadastro_pam` TIMESTAMP NULL AFTER  `dt_cadastro` ,
ADD  `dt_cadastro_processo` TIMESTAMP NULL AFTER  `dt_cadastro_pam`;

INSERT INTO `setores` (`co_setor`, `ds_setor`, `ds_email`, `nu_telefone`, `ic_setor`) VALUES
(1, 'SEÃ‡ÃƒO DE CONTRATOS', 'vnunes@hfa.mil.br', '(61) 3966-2432', NULL),
(2, 'DIVISÃƒO ADMINISTRATIVA', null, null, NULL),
(3, 'ORDENADOR DE DESPESAS', null, null, NULL),
(4, 'PESQUISA DE MERCADO', null, null, NULL),
(5, 'SEÃ‡ÃƒO DE LICITAÃ‡ÃµES', 'chseclicitacao@gmail.com', '(61) 3966-2498', NULL),
(6, 'AQUISIÃ‡ÃµES', null, null, NULL),
(7, 'SEÃ‡ÃƒO DE FINANÃ‡AS', 'difin.hfa@gmail.com', '(61) 3966-2433', NULL),
(8, 'SALA DE RECEBIMENTO', NULL, NULL, NULL),
(9, 'SEÃ‡ÃƒO DE ALMOXARIFADO', NULL, NULL, NULL),
(10, 'CONFORMIDADE', NULL, NULL, NULL);

INSERT INTO `fases` (`co_fase`, `ds_fase`) VALUES
(1, 'PEDIDO'),
(2, 'PESQUISA DE MERCADO'),
(3, 'TRIAGEM'),
(4, 'INSTRUMENTAÃ‡ÃƒO DO PROCESSO'),
(5, 'EMPENHO'),
(6, 'FORMALIZAÃ‡ÃƒO DO TERMO DE CONTRATO'),
(7, 'PEDIDO DE EMPENHO'),
(8, 'EXECUÃ‡ÃƒO'),
(9, 'BAIXA PAGAMENTO'),
(10, 'ARQUIVAR'),
(11, 'PUBLICIDADE'),
(12, 'LANÃ‡A IRP'),
(13, 'ELABORAÃ‡ÃƒO DO EDITAL'),
(14, 'PARECER JURÃDICO'),
(15, 'ANÃLISE E PARECER'),
(16, 'RETIFICAÃ‡ÃƒO / RATIFICAÃ‡ÃƒO'),
(17, 'ABERTURA LICITAÃ‡ÃƒO'),
(18, 'HOMOLOGA LICITAÃ‡ÃƒO'),
(19, 'RECEBIMENTO'),
(20, 'ATESTO DE NOTAS'),
(21, 'LIQUIDAÃ‡ÃƒO'),
(22, 'CONFORMIDADE'),
(23, 'JUNTADA AO PROCESSO EXISTENTE'),
(24, 'SANEAMENTO DO PROCESSO'),
(25, 'REALIZA AJUDICAÃ‡ÃƒO DA LICITAÃ‡ÃƒO'),
(26, 'JUNTADA DE DOCUMENTOS');


INSERT INTO `fluxos` (
`nu_sequencia` ,
`tp_operador` ,
`co_fase` ,
`co_setor` ,
`nu_sequencia_um` ,
`nu_sequencia_dois`
)
VALUES (
'1', NULL ,  '1',  '1',  '2', NULL
), (
'2', NULL ,  '3',  '3',  '3',  '1'
), (
'3',  '1', NULL , NULL ,  '4',  '16'
), (
'4', NULL ,  '2',  '4',  '5',  '2'
), (
'5', NULL ,  '3',  '3',  '6',  '4'
), (
'6',  '4', NULL , NULL ,  '7',  '46'
), (
'7', NULL ,  '4',  '6',  '8',  '5'
), (
'8', NULL ,  '5',  '6',  '9',  '7'
), (
'9', NULL ,  '6',  '1',  '10',  '8'
), (
'10', NULL ,  '7',  '1',  '11',  '9'
), (
'11', NULL ,  '5',  '6',  '12',  '10'
), (
'12', NULL ,  '11',  '1',  '13',  '10'
), (
'13', NULL ,  '8',  '1',  '14',  '12'
), (
'14', NULL ,  '9',  '7',  '15',  '13'
), (
'15', NULL ,  '10',  '10',  NULL,  NULL
), (
'16',  '2', NULL , NULL ,  '17',  '24'
), (
'17',  '3', NULL , NULL ,  '18',  '38'
), (
'18', NULL ,  '23',  '6',  '19',  '2'
), (
'19', NULL ,  '5',  '6',  '20',  '18'
), (
'20', NULL ,  '19',  '8',  '21',  '19'
), (
'21', NULL ,  '20',  '9',  '22',  '20'
), (
'22', NULL ,  '21',  '9',  '23',  '21'
), (
'23', NULL ,  '22',  '10',  '45',  '22'
), (
'24', NULL ,  '2',  '4',  '25',  '2'
), (
'25', NULL ,  '3',  '3',  '26',  '24'
), (
'26', '4',  NULL,  NULL,  '69',  '27'
), (
'27', NULL ,  '4',  '5',  '28',  '25'
), (
'28', NULL ,  '12',  '5',  '29',  '27'
), (
'29', NULL ,  '13',  '5',  '30',  '28'
), (
'30', NULL ,  '14',  '5',  '31',  '29'
), (
'31', NULL ,  '15',  '5',  '32',  '30'
), (
'32', NULL ,  '24',  '5',  '33',  '31'
), (
'33', NULL ,  '11',  '5',  '34',  '32'
), (
'34', NULL ,  '17',  '5',  '35',  '33'
), (
'35', NULL ,  '25',  '5',  '36',  '34'
), (
'36', NULL ,  '18',  '5',  '37',  '35'
), (
'37', '1' ,  NULL,  NULL,  '39',  '63'
), (
'38', NULL ,  '26',  '6',  '74',  '2'
), (
'39', NULL ,  '6',  '1',  '36',  '40'
), (
'40', NULL ,  '7',  '1',  '41',  '39'
), (
'41', NULL ,  '5',  '6',  '42',  '40'
), (
'42', NULL ,  '11',  '1',  '43',  '40'
), (
'43', NULL ,  '8',  '1',  '44',  '42'
), (
'44', NULL ,  '9',  '7',  '15',  '43'
), (
'45', NULL ,  '9',  '7',  '15',  '23'
), (
'46', NULL ,  '4',  '5',  '47',  '5'
), (
'47', NULL ,  '12',  '5',  '48',  '46'
), (
'48', NULL ,  '13',  '5',  '49',  '47'
), (
'49', NULL ,  '14',  '5',  '50',  '48'
), (
'50', NULL ,  '15',  '5',  '51',  '49'
), (
'51', NULL ,  '24',  '5',  '52',  '50'
), (
'52', NULL ,  '11',  '5',  '53',  '51'
), (
'53', NULL ,  '17',  '5',  '54',  '52'
), (
'54', NULL ,  '25',  '5',  '55',  '53'
), (
'55', NULL ,  '18',  '5',  '56',  '54'
), (
'56', '1' ,  NULL,  NULL,  '57',  '80'
), (
'57', NULL ,  '6',  '1',  '58',  '55'
), (
'58', NULL ,  '7',  '1',  '59',  '57'
), (
'59', NULL ,  '5',  '6',  '60',  '58'
), (
'60', NULL ,  '11',  '1',  '61',  '58'
), (
'61', NULL ,  '8',  '1',  '62',  '60'
), (
'62', NULL ,  '9',  '7',  '15',  '61'
), (
'63', NULL ,  '5',  '6',  '64',  '36'
), (
'64', NULL ,  '19',  '8',  '65',  '63'
), (
'65', NULL ,  '20',  '9',  '66',  '64'
), (
'66', NULL ,  '21',  '9',  '67',  '65'
), (
'67', NULL ,  '22',  '10',  '68',  '66'
), (
'68', NULL ,  '9',  '7',  '15',  '67'
), (
'69', NULL ,  '19',  '8',  '70',  '25'
), (
'70', NULL ,  '20',  '9',  '71',  '69'
), (
'71', NULL ,  '21',  '9',  '72',  '70'
), (
'72', NULL ,  '22',  '10',  '73',  '71'
), (
'73', NULL ,  '9',  '7',  '15',  '72'
), (
'74', NULL ,  '5',  '6',  '75',  '38'
), (
'75', NULL ,  '19',  '8',  '76',  '74'
), (
'76', NULL ,  '20',  '9',  '77',  '75'
), (
'77', NULL ,  '21',  '9',  '78',  '76'
), (
'78', NULL ,  '22',  '10',  '79',  '77'
), (
'79', NULL ,  '9',  '7',  '15',  '78'
), (
'80', NULL ,  '5',  '6',  '81',  '55'
), (
'81', NULL ,  '19',  '8',  '82',  '80'
), (
'82', NULL ,  '20',  '9',  '83',  '81'
), (
'83', NULL ,  '21',  '9',  '84',  '82'
), (
'84', NULL ,  '22',  '10',  '85',  '83'
), (
'85', NULL ,  '9',  '7',  '15',  '84'
);

ALTER TABLE  `setores` ADD  `parent_id` INT( 11 ) NULL AFTER  `co_setor`;

CREATE TABLE  `candidatos` (
`co_candidato` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`nu_cpf` VARCHAR( 11 ) NOT NULL ,
`no_nome` VARCHAR( 100 ) NOT NULL ,
`tp_sexo` VARCHAR( 1 ) NOT NULL ,
`tp_raca` INT( 1 ) NOT NULL ,
`dt_nascimento` DATE NOT NULL ,
`nu_identidade` VARCHAR( 30 ) NOT NULL ,
`ds_orgao` VARCHAR( 15 ) NOT NULL ,
`dt_expedicao` DATE NOT NULL ,
`tp_graduacao` INT( 1 ) NOT NULL ,
`sg_uf` VARCHAR( 2 ) NOT NULL ,
`co_municipio` INT( 11 ) NOT NULL ,
`ic_patrocinio` VARCHAR( 1 ) NOT NULL ,
`ds_endereco` VARCHAR( 255 ) NOT NULL ,
`nu_endereco` VARCHAR( 20 ) NOT NULL ,
`nu_cep` VARCHAR( 10 ) NOT NULL ,
`sg_uf_end` VARCHAR( 2 ) NOT NULL ,
`co_municipio_end` INT( 11 ) NOT NULL ,
`ds_email` VARCHAR( 50 ) NOT NULL ,
`nu_telefone` VARCHAR( 15 ) NOT NULL ,
`nu_celular` VARCHAR( 15 ) NOT NULL ,
PRIMARY KEY (  `co_candidato` ) ,
UNIQUE (
`nu_cpf`
)
) ENGINE = INNODB COMMENT =  'Candidatos para o Módulo de Inscrição';

ALTER TABLE  `contratos` ADD  `co_candidato` INT( 11 ) NULL AFTER  `co_gestor_atual` ,
ADD INDEX (  `co_candidato` );

ALTER TABLE  `contratos` ADD FOREIGN KEY (  `co_candidato` ) REFERENCES  `candidatos` (
`co_candidato`
);

ALTER TABLE  `log_contratos` ADD  `co_candidato` INT( 11 ) NULL AFTER  `co_gestor_atual` ,
ADD INDEX (  `co_candidato` );

ALTER TABLE  `contratos` ADD  `co_tipo_inscricao` INT( 11 ) NULL AFTER  `co_candidato` ,
ADD  `co_confederacao` INT( 11 ) NULL AFTER  `co_tipo_inscricao` ,
ADD  `co_federacao` INT( 11 ) NULL AFTER  `co_confederacao` ,
ADD  `co_classificacao` INT( 11 ) NULL AFTER  `co_federacao` ,
ADD  `co_denominacao` INT( 11 ) NULL AFTER  `co_classificacao`;

ALTER TABLE  `log_contratos` ADD  `co_tipo_inscricao` INT( 11 ) NULL AFTER  `co_candidato` ,
ADD  `co_confederacao` INT( 11 ) NULL AFTER  `co_tipo_inscricao` ,
ADD  `co_federacao` INT( 11 ) NULL AFTER  `co_confederacao` ,
ADD  `co_classificacao` INT( 11 ) NULL AFTER  `co_federacao` ,
ADD  `co_denominacao` INT( 11 ) NULL AFTER  `co_classificacao`;

ALTER TABLE  `contratos` CHANGE  `nu_pam`  `nu_pam` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;