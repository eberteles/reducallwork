ALTER TABLE  `usuarios` CHANGE  `nu_cpf`  `nu_cpf` VARCHAR( 11 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT  'CPF ou Matrícula',
CHANGE  `ds_email`  `ds_email` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_usuarios` CHANGE  `nu_cpf`  `nu_cpf` VARCHAR( 11 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT  'CPF ou Matrícula',
CHANGE  `ds_email`  `ds_email` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;