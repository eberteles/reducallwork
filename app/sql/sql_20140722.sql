-- 
-- APOSTILAMENTOS
-- 
CREATE TABLE `apostilamentos` (
  `co_apostilamento` int(10) NOT NULL AUTO_INCREMENT,
  `co_contrato` int(11) NOT NULL,
  `no_apostilamento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tp_apostilamento` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 - Apostilamento de Valor 2 - Apostilamento de Prazo 3 - Apostilamento de Valor e Prazo',
  `dt_apostilamento` date NOT NULL,
  `vl_apostilamento` decimal(10,2) DEFAULT NULL,
  `dt_fim_vigencia_anterior` date DEFAULT NULL,
  `dt_prazo` date DEFAULT NULL,
  `ds_apostilamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `co_usuario` int(11) NOT NULL,
  PRIMARY KEY (  `co_apostilamento` ) ,
  INDEX (  `co_contrato` ,  `co_usuario` )
) ENGINE = INNODB COMMENT =  'Tabela de Apostilamentos';

ALTER TABLE  `apostilamentos` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `apostilamentos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);