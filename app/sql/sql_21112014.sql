
CREATE TABLE  `notas_empenhos` (
`co_nota_empenho` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_empenho` INT( 11 ) NOT NULL ,
`co_nota` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NULL ,
`dt_nota_empenho` DATE NOT NULL ,
INDEX (  `co_empenho` ),
INDEX (  `co_nota` ),
INDEX (  `co_usuario` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de relação de Notas e Empenhos';

ALTER TABLE  `notas_empenhos` ADD FOREIGN KEY (  `co_empenho` ) REFERENCES  `empenhos` (
`co_empenho`
);

ALTER TABLE  `notas_empenhos` ADD FOREIGN KEY (  `co_nota` ) REFERENCES  `notas_fiscais` (
`co_nota`
);

ALTER TABLE  `notas_empenhos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

DROP TRIGGER IF EXISTS `ADD_NOTAS_EMPENHOS`;
DELIMITER //
CREATE TRIGGER `ADD_NOTAS_EMPENHOS` AFTER INSERT ON `notas_empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_nota_empenho = NEW.co_nota_empenho,
            co_empenho      = NEW.co_empenho,
            co_nota         = NEW.co_nota;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_NOTAS_EMPENHOS`;
DELIMITER //
CREATE TRIGGER `UPD_NOTAS_EMPENHOS` AFTER UPDATE ON `notas_empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_nota_empenho = NEW.co_nota_empenho,
            co_empenho      = NEW.co_empenho,
            co_nota         = NEW.co_nota;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_NOTAS_EMPENHOS`;
DELIMITER //
CREATE TRIGGER `DEL_NOTAS_EMPENHOS` AFTER DELETE ON `notas_empenhos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_empenhos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_nota_empenho = OLD.co_nota_empenho,
            co_empenho      = OLD.co_empenho,
            co_nota         = OLD.co_nota;
        END
//
DELIMITER ;

CREATE TABLE IF NOT EXISTS `log_notas_empenhos` (
  `co_log_notas_empenhos` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_nota_empenho` INT( 11 ) NOT NULL ,
  `co_empenho` INT( 11 ) NOT NULL ,
  `co_nota` INT( 11 ) NOT NULL ,
  PRIMARY KEY (`co_log_notas_empenhos`),
  INDEX (  `co_empenho` ),
  INDEX (  `co_nota` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;


CREATE TABLE  `notas_pagamentos` (
`co_nota_pagamento` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_pagamento` INT( 11 ) NOT NULL ,
`co_nota` INT( 11 ) NOT NULL ,
`co_usuario` INT( 11 ) NULL ,
INDEX (  `co_pagamento` ),
INDEX (  `co_nota` ),
INDEX (  `co_usuario` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela de relação de Notas e Pagamentos';

ALTER TABLE  `notas_pagamentos` ADD FOREIGN KEY (  `co_pagamento` ) REFERENCES  `pagamentos` (
`co_pagamento`
);

ALTER TABLE  `notas_pagamentos` ADD FOREIGN KEY (  `co_nota` ) REFERENCES  `notas_fiscais` (
`co_nota`
);

ALTER TABLE  `notas_pagamentos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

DROP TRIGGER IF EXISTS `ADD_NOTAS_PAGAMENTOS`;
DELIMITER //
CREATE TRIGGER `ADD_NOTAS_PAGAMENTOS` AFTER INSERT ON `notas_pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_nota_pagamento = NEW.co_nota_pagamento,
            co_pagamento      = NEW.co_pagamento,
            co_nota           = NEW.co_nota;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_NOTAS_PAGAMENTOS`;
DELIMITER //
CREATE TRIGGER `UPD_NOTAS_PAGAMENTOS` AFTER UPDATE ON `notas_pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_nota_pagamento = NEW.co_nota_pagamento,
            co_pagamento      = NEW.co_pagamento,
            co_nota           = NEW.co_nota;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_NOTAS_PAGAMENTOS`;
DELIMITER //
CREATE TRIGGER `DEL_NOTAS_PAGAMENTOS` AFTER DELETE ON `notas_pagamentos`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_pagamentos SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_nota_pagamento = OLD.co_nota_pagamento,
            co_pagamento      = OLD.co_pagamento,
            co_nota           = OLD.co_nota;
        END
//
DELIMITER ;

CREATE TABLE IF NOT EXISTS `log_notas_pagamentos` (
  `co_log_notas_pagamentos` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_nota_pagamento` INT( 11 ) NOT NULL ,
  `co_pagamento` INT( 11 ) NOT NULL ,
  `co_nota` INT( 11 ) NOT NULL ,
  PRIMARY KEY (`co_log_notas_pagamentos`),
  INDEX (  `co_pagamento` ),
  INDEX (  `co_nota` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE  `notas_fiscais` ADD  `dt_envio` DATE NULL AFTER  `dt_recebimento`;

ALTER TABLE  `log_notas_fiscais` ADD  `dt_envio` DATE NULL AFTER  `dt_recebimento`;

DROP TRIGGER IF EXISTS `ADD_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_NOTA_FISCAL` AFTER INSERT ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_nota         = NEW.co_nota,
            co_contrato     = NEW.co_contrato,
            nu_nota         = NEW.nu_nota,
            nu_serie        = NEW.nu_serie,
            dt_recebimento  = NEW.dt_recebimento,
            dt_envio        = NEW.dt_envio,
            vl_nota         = NEW.vl_nota,
            vl_glosa        = NEW.vl_glosa,
            ds_nota         = NEW.ds_nota,
            ic_atesto       = NEW.ic_atesto,
            dt_atesto       = NEW.dt_atesto,
            ds_atesto       = NEW.ds_atesto;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_NOTA_FISCAL` AFTER UPDATE ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_nota         = NEW.co_nota,
            co_contrato     = NEW.co_contrato,
            nu_nota         = NEW.nu_nota,
            nu_serie        = NEW.nu_serie,
            dt_recebimento  = NEW.dt_recebimento,
            dt_envio        = NEW.dt_envio,
            vl_nota         = NEW.vl_nota,
            vl_glosa        = NEW.vl_glosa,
            ds_nota         = NEW.ds_nota,
            ic_atesto       = NEW.ic_atesto,
            dt_atesto       = NEW.dt_atesto,
            ds_atesto       = NEW.ds_atesto;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_NOTA_FISCAL`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_NOTA_FISCAL` AFTER DELETE ON `notas_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO log_notas_fiscais SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_nota         = OLD.co_nota,
            co_contrato     = OLD.co_contrato,
            nu_nota         = OLD.nu_nota,
            nu_serie        = OLD.nu_serie,
            dt_recebimento  = OLD.dt_recebimento,
            dt_envio        = OLD.dt_envio,
            vl_nota         = OLD.vl_nota,
            vl_glosa        = OLD.vl_glosa,
            ds_nota         = OLD.ds_nota,
            ic_atesto       = OLD.ic_atesto,
            dt_atesto       = OLD.dt_atesto,
            ds_atesto       = OLD.ds_atesto;
        END
//
DELIMITER ;

ALTER TABLE  `empenhos` CHANGE  `nu_ptres`  `nu_ptres` VARCHAR( 20 ) NULL DEFAULT NULL;
ALTER TABLE  `log_empenhos` CHANGE  `nu_ptres`  `nu_ptres` VARCHAR( 20 ) NULL DEFAULT NULL;

ALTER TABLE  `liquidacao` ADD  `dt_liquidacao` DATE NULL AFTER  `co_liquidacao`;

ALTER TABLE  `atas_itens` CHANGE  `nu_quantidade`  `nu_quantidade` INT( 10 ) NOT NULL ,
CHANGE  `qt_utilizado`  `qt_utilizado` INT( 10 ) NOT NULL DEFAULT  '0';