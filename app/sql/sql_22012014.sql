ALTER TABLE  `contratos` ADD  `co_fiscal_atual` INT( 11 ) NULL AFTER  `co_contratacao` ,
ADD  `co_gestor_atual` INT( 11 ) NULL AFTER  `co_fiscal_atual`;

ALTER TABLE  `log_contratos` ADD  `co_fiscal_atual` INT( 11 ) NULL AFTER  `co_contratante` ,
ADD  `co_gestor_atual` INT( 11 ) NULL AFTER  `co_fiscal_atual`;

ALTER TABLE `contratos`
  ADD CONSTRAINT `contratos_ibfk_6` FOREIGN KEY (`co_fiscal_atual`) REFERENCES `usuarios` (`co_usuario`),
  ADD CONSTRAINT `contratos_ibfk_7` FOREIGN KEY (`co_gestor_atual`) REFERENCES `usuarios` (`co_usuario`);


DROP TRIGGER IF EXISTS `ADD_LOG_CONTRATO`;

DELIMITER //
CREATE TRIGGER `ADD_LOG_CONTRATO` AFTER INSERT ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro;
END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_CONTRATO`;

DELIMITER //
CREATE TRIGGER `UPD_LOG_CONTRATO` AFTER UPDATE ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro;
        END
//
DELIMITER ;

ALTER TABLE  `usuarios` ADD  `co_gestor` INT( 11 ) NULL AFTER  `co_setor`;

ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`co_gestor`) REFERENCES `usuarios` (`co_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE  `usuarios` ADD  `ds_nome` VARCHAR( 100 ) NOT NULL AFTER  `nu_cpf`;

ALTER TABLE  `usuarios` ADD  `nu_telefone` VARCHAR( 15 ) NULL AFTER  `ds_email`;


ALTER TABLE  `log_contratos_fiscais` CHANGE  `co_fiscal`  `co_usuario_fiscal` INT( 11 ) UNSIGNED NOT NULL

DROP TABLE `contratos_fiscais`;

CREATE TABLE IF NOT EXISTS `contratos_fiscais` (
  `co_contrato_fiscal` int(11) NOT NULL AUTO_INCREMENT,
  `co_usuario` int(11) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `co_usuario_fiscal` int(11) DEFAULT NULL,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  PRIMARY KEY (`co_contrato_fiscal`),
  KEY `co_contrato` (`co_contrato`),
  KEY `co_usuario_fiscal` (`co_usuario_fiscal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Gatilhos `contratos_fiscais`
--
DROP TRIGGER IF EXISTS `ADD_LOG_CONTRATOS_FISCAIS`;

DELIMITER //
CREATE TRIGGER `ADD_LOG_CONTRATOS_FISCAIS` AFTER INSERT ON `contratos_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO `log_contratos_fiscais` SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
			
            co_contrato_fiscal = NEW.co_contrato_fiscal,
            co_contrato        = NEW.co_contrato,
            co_usuario_fiscal  = NEW.co_usuario_fiscal,
            dt_inicio          = NEW.dt_inicio,
            dt_fim             = NEW.dt_fim;
            
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_CONTRATOS_FISCAIS`;

DELIMITER //
CREATE TRIGGER `UPD_LOG_CONTRATOS_FISCAIS` AFTER UPDATE ON `contratos_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO `log_contratos_fiscais` SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
			 			
            co_contrato_fiscal = NEW.co_contrato_fiscal,
            co_contrato        = NEW.co_contrato,
            co_usuario_fiscal  = NEW.co_usuario_fiscal,
            dt_inicio          = NEW.dt_inicio,
            dt_fim             = NEW.dt_fim;

        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_CONTRATOS_FISCAIS`;

DELIMITER //
CREATE TRIGGER `DEL_LOG_CONTRATOS_FISCAIS` AFTER DELETE ON `contratos_fiscais`
 FOR EACH ROW BEGIN
	INSERT INTO `log_contratos_fiscais` SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
			 			
            co_contrato_fiscal = OLD.co_contrato_fiscal,
            co_contrato        = OLD.co_contrato,
            co_usuario_fiscal  = OLD.co_usuario_fiscal,
            dt_inicio          = OLD.dt_inicio,
            dt_fim             = OLD.dt_fim;

        END
//
DELIMITER ;

ALTER TABLE `contratos_fiscais`
  ADD CONSTRAINT `contratos_fiscais_ibfk_1` FOREIGN KEY (`co_usuario_fiscal`) REFERENCES `usuarios` (`co_usuario`),
  ADD CONSTRAINT `contratos_fiscais_ibfk_2` FOREIGN KEY (`co_contrato`) REFERENCES `contratos` (`co_contrato`);

CREATE TABLE  `atividades` (
`co_atividade` INT( 11 ) NOT NULL AUTO_INCREMENT,
`co_contrato` INT( 11 ) NOT NULL ,
`parent_id` INT( 11 ) NULL,
`co_usuario` int(11) NOT NULL,
`co_responsavel` INT( 11 ) NOT NULL ,
`ds_atividade` VARCHAR( 255 ) NOT NULL,
`tp_andamento` VARCHAR( 1 ) NOT NULL COMMENT  'N / E / P / F',
`dt_ini_planejado` DATE NOT NULL ,
`dt_fim_planejado` DATE NOT NULL ,
`dt_ini_execucao` DATE NULL ,
`dt_fim_execucao` DATE NULL ,
`pc_executado` INT( 3 ) NOT NULL DEFAULT  '0',
PRIMARY KEY (`co_atividade`),
KEY `co_contrato` (`co_contrato`),
KEY `co_usuario` (`co_usuario`),
KEY `co_responsavel` (`co_responsavel`)
) ENGINE = INNODB COMMENT =  'Tabela de Atividades de Execução';

CREATE TABLE IF NOT EXISTS `log_atividades` (
  `co_log_atividades` int(11) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NOT NULL,
  `tp_acao` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'L, C, A, E, I',

  `co_atividade` int(11) NOT NULL,
  `co_contrato` int(11) NOT NULL,
  `co_responsavel` int(11) NOT NULL,
  `ds_atividade` VARCHAR( 255 ) NOT NULL,
  `tp_andamento` VARCHAR( 1 ) NOT NULL COMMENT  'N / E / P / F',
  `dt_ini_planejado` DATE NOT NULL ,
  `dt_fim_planejado` DATE NOT NULL ,
  `dt_ini_execucao` DATE NULL ,
  `dt_fim_execucao` DATE NULL ,
  `pc_executado` INT( 3 ) NOT NULL DEFAULT  '0',
  PRIMARY KEY (`co_log_atividades`),
  KEY `co_contrato` (`co_contrato`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TRIGGER IF EXISTS `ADD_LOG_ATIVIDADES`;

DELIMITER //
CREATE TRIGGER `ADD_LOG_ATIVIDADES` AFTER INSERT ON `atividades`
 FOR EACH ROW BEGIN
	INSERT INTO log_atividades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
			
            co_atividade     = NEW.co_atividade,
            co_contrato      = NEW.co_contrato,
            co_responsavel   = NEW.co_responsavel,
            ds_atividade     = NEW.ds_atividade,
            tp_andamento     = NEW.tp_andamento,
            dt_ini_planejado = NEW.dt_ini_planejado,
            dt_fim_planejado = NEW.dt_fim_planejado,
            dt_ini_execucao  = NEW.dt_ini_execucao,
            dt_fim_execucao  = NEW.dt_fim_execucao,
            pc_executado     = NEW.pc_executado;
            
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_ATIVIDADES`;

DELIMITER //
CREATE TRIGGER `UPD_LOG_ATIVIDADES` AFTER UPDATE ON `atividades`
 FOR EACH ROW BEGIN
	INSERT INTO log_atividades SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
			 			
            co_atividade     = NEW.co_atividade,
            co_contrato      = NEW.co_contrato,
            co_responsavel   = NEW.co_responsavel,
            ds_atividade     = NEW.ds_atividade,
            tp_andamento     = NEW.tp_andamento,
            dt_ini_planejado = NEW.dt_ini_planejado,
            dt_fim_planejado = NEW.dt_fim_planejado,
            dt_ini_execucao  = NEW.dt_ini_execucao,
            dt_fim_execucao  = NEW.dt_fim_execucao,
            pc_executado     = NEW.pc_executado;

        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_ATIVIDADES`;

DELIMITER //
CREATE TRIGGER `DEL_LOG_ATIVIDADES` AFTER DELETE ON `atividades`
 FOR EACH ROW BEGIN
	INSERT INTO log_atividades SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
			 			
            co_atividade     = OLD.co_atividade,
            co_contrato      = OLD.co_contrato,
            co_responsavel   = OLD.co_responsavel,
            ds_atividade     = OLD.ds_atividade,
            tp_andamento     = OLD.tp_andamento,
            dt_ini_planejado = OLD.dt_ini_planejado,
            dt_fim_planejado = OLD.dt_fim_planejado,
            dt_ini_execucao  = OLD.dt_ini_execucao,
            dt_fim_execucao  = OLD.dt_fim_execucao,
            pc_executado     = OLD.pc_executado;

        END
//
DELIMITER ;

ALTER TABLE  `pagamentos` ADD  `co_atividade` INT( 11 ) NULL AFTER  `co_contrato` ,
ADD INDEX (  `co_atividade` );

ALTER TABLE  `log_pagamentos` ADD  `co_atividade` INT( 11 ) NULL AFTER  `co_contrato` ,
ADD INDEX (  `co_atividade` );

ALTER TABLE  `anexos` ADD  `co_atividade` INT( 11 ) NULL AFTER  `co_contrato` ,
ADD INDEX (  `co_atividade` );

ALTER TABLE  `anexos` ADD  `tp_documento` INT( 1 ) NOT NULL DEFAULT  '6' COMMENT  '1 - Atestos, 2 - Nota Fiscal, 3 - Contrato, 4 - Edital, 5 - Pareceres, 6 - Outros Documentos' AFTER  `co_atividade`;

ALTER TABLE  `anexos` ADD  `dt_anexo` DATE NOT NULL AFTER  `co_atividade`;

ALTER TABLE  `anexos` ADD  `ds_observacao` VARCHAR( 255 ) NULL ,
ADD  `ic_atesto` INT( 1 ) NOT NULL DEFAULT  '0' COMMENT  '0 - Não, 1 - Sim'

ALTER TABLE  `anexos` ADD  `co_contrato_fiscal` INT( 11 ) NULL AFTER  `co_atividade` ,
ADD INDEX (  `co_contrato_fiscal` );