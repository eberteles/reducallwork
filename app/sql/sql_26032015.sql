// ===============================================================================
// CUIDADO AO EXECUTAR ESSE ARQUIVO!!! VC IRA SUBSTITUIR OS DADOS JA EXISTENTES!!!
// ===============================================================================

SET FOREIGN_KEY_CHECKS = 0;

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (50, 50, 'CONTRATO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'CONTRATO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (51, 51, 'CREDENCIAMENTO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'CREDENCIAMENTO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (52, 52, 'COMODATO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'COMODATO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (53, 53, 'ARRENDAMENTO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'ARRENDAMENTO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (54, 54, 'CONCESSÃO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'CONCESSÃO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (55, 55, 'TERMO ADITIVO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'TERMO ADITIVO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (56, 56, 'TERMO DE ADESÃO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'TERMO DE ADESÃO';

INSERT INTO modalidades (co_modalidade, nu_modalidade, ds_modalidade,co_usuario)
VALUES (57, 57, 'CCONVÊNIO', null) ON DUPLICATE KEY UPDATE ds_modalidade = 'CCONVÊNIO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (1, 'CONVITE', '01', '01') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONVITE';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (2, 'TOMADA DE PREÇOS', '02', '02') ON DUPLICATE KEY UPDATE ds_contratacao = 'TOMADA DE PREÇOS';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (3, 'CONCORRÊNCIA', '03', '03') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONCORRÊNCIA';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (4, 'CONCORRÊNCIA INTERNACIONAL', '04', '04') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONCORRÊNCIA INTERNACIONAL';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (5, 'PREGÃO', '05', '05') ON DUPLICATE KEY UPDATE ds_contratacao = 'PREGÃO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (6, 'DISPOENSA DE LICITAÇÃO', '06', '06')  ON DUPLICATE KEY UPDATE ds_contratacao = 'DISPOENSA DE LICITAÇÃO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (7, 'INEXIGIBILIDADE DE LICITAÇÃO', '07', '07') ON DUPLICATE KEY UPDATE ds_contratacao = 'INEXIGIBILIDADE DE LICITAÇÃO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (20, 'CONCURSO', '20', '20') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONCURSO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (22, 'TOMADA DE PREÇOS POR TÉCNICA E PREÇO', '22', '22') ON DUPLICATE KEY UPDATE ds_contratacao = 'TOMADA DE PREÇOS POR TÉCNICA E PREÇO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (33, 'CONCORRÊNCIA POR TÉCICA E PREÇO', '33', '33') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONCORRÊNCIA POR TÉCICA E PREÇO';

INSERT INTO contratacoes (co_contratacao, ds_contratacao, co_siafi,co_siasg)
VALUES (44, 'CONCORRÊNCIA INTERNACIONAL POR TÉCNICA E PREÇO', '44', '44') ON DUPLICATE KEY UPDATE ds_contratacao = 'CONCORRÊNCIA INTERNACIONAL POR TÉCNICA E PREÇO';