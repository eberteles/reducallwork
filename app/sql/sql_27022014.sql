CREATE TABLE  `dashboards` (
`co_dashboard` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`co_usuario` INT( 11 ) NOT NULL ,
INDEX (  `co_usuario` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT =  'Tabela com as configuração do Dashboard do usuário';

ALTER TABLE  `dashboards` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);