
ALTER TABLE  `aditivos` CHANGE  `tp_aditivo`  `tp_aditivo` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT  '1' COMMENT '1 - Aditivo de Valor, 2 - Aditivo de Prazo, 3 - Aditivo de Valor e Prazo, 4 - Aditivo de Supressão';

DROP FUNCTION IF EXISTS `mask`;
DELIMITER //

CREATE FUNCTION mask (unformatted_value CHAR(32), format_string CHAR(32))
RETURNS CHAR(32) DETERMINISTIC

BEGIN
# Declare variables
DECLARE input_len TINYINT;
DECLARE output_len TINYINT;
DECLARE temp_char CHAR;

# Initialize variables
SET input_len = LENGTH(unformatted_value);
SET output_len = LENGTH(format_string);

# Construct formated string
WHILE ( output_len > 0 ) DO

    SET temp_char = SUBSTR(format_string, output_len, 1);
    IF ( temp_char = '#' ) THEN
        IF ( input_len > 0 ) THEN
            SET format_string = INSERT(format_string, output_len, 1, SUBSTR(unformatted_value, input_len, 1));
            SET input_len = input_len - 1;
        ELSE
            SET format_string = INSERT(format_string, output_len, 1, '0');
        END IF;
    END IF;

    SET output_len = output_len - 1;
END WHILE;

RETURN format_string;
END //

DELIMITER ;

ALTER TABLE  `aditivos` CHANGE  `ds_aditivo`  `ds_aditivo` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;