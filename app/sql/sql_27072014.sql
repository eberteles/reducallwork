ALTER TABLE  `notas_fiscais` ADD  `vl_glosa` DECIMAL( 10, 2 ) NULL AFTER  `vl_nota`;

ALTER TABLE  `garantias` ADD  `nu_apolice` VARCHAR( 50 ) NULL AFTER  `no_seguradora`;

ALTER TABLE  `garantias` CHANGE  `vl_garantia`  `vl_garantia` DECIMAL( 10, 2 ) NULL ,
CHANGE  `dt_inicio`  `dt_inicio` DATE NULL ,
CHANGE  `dt_fim`  `dt_fim` DATE NULL ,
CHANGE  `no_seguradora`  `no_seguradora` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_endereco`  `ds_endereco` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `nu_telefone`  `nu_telefone` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `anexos` ADD  `co_aditivo` INT( 10 ) NULL AFTER  `co_contrato_fiscal`;

ALTER TABLE  `anexos` ADD  `co_apostilamento` INT( 11 ) NULL AFTER  `co_aditivo`;

ALTER TABLE  `anexos` ADD  `co_garantia` INT( 11 ) NULL AFTER  `co_apostilamento`;

ALTER TABLE  `anexos` ADD  `co_empenho` INT( 10 ) NULL AFTER  `co_garantia`;

ALTER TABLE  `anexos` ADD  `co_penalidade` INT( 11 ) NULL AFTER  `co_empenho`;

ALTER TABLE  `anexos` ADD  `co_pendencia` INT( 11 ) NULL AFTER  `co_nota`;

ALTER TABLE  `anexos` ADD  `co_pagamento` INT( 10 ) NULL AFTER  `co_pendencia`;

ALTER TABLE  `anexos` ADD  `co_historico` INT( 11 ) NULL AFTER  `co_pagamento`;