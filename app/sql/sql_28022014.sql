ALTER TABLE  `empenhos` CHANGE  `nu_empenho`  `nu_empenho` VARCHAR( 12 ) NOT NULL; 

ALTER TABLE  `empenhos` CHANGE  `co_plano_interno`  `co_plano_interno` VARCHAR( 11 ) NOT NULL;

ALTER TABLE `empenhos`  ADD `nu_evento_contabil` INT(6) NULL COMMENT 'Código do Evento Contábil do SIAFI' AFTER `nu_ptres`,  
                        ADD `nu_esfera_orcamentaria` INT(1) NULL AFTER `nu_evento_contabil`,  
                        ADD `ds_fonte_recurso` VARCHAR(10) NULL AFTER `nu_esfera_orcamentaria`,  
                        ADD `nu_natureza_despesa` INT(6) NULL AFTER `ds_fonte_recurso`,  
                        ADD `ds_inciso` VARCHAR(2) NULL AFTER `nu_natureza_despesa`,  
                        ADD `ds_amparo_legal` VARCHAR(8) NULL AFTER `ds_inciso`;

ALTER TABLE `empenhos`  ADD `nu_ug_responsavel` INT(6) NULL COMMENT 'Unidade Gestora Responsável',  
                        ADD `nu_origem_material` INT(1) NULL COMMENT '1 - Origem Nacional, 2 - Material Estrangeiro adquirido no Brasil, 3 - Importação Direta',  
                        ADD `uf_beneficiada` VARCHAR(2) NULL,  
                        ADD `nu_municipio_beneficiado` INT(4) NULL COMMENT 'Código IBGE do município beneficiado',  
                        ADD `nu_contra_entrega` INT(1) NULL COMMENT '0 - NÃO SE APLICA, 1 - FIXADO PELA STN, 2 - FIXADO PELA PRÓPRIA UG',  
                        ADD `nu_lista` VARCHAR(12) NULL;

ALTER TABLE `contratacoes`  ADD `co_siafi` INT(2) NOT NULL COMMENT '01 - Concurso, 02 - Convite, 03 - Tomada de Preço, 04 - Concorrência, 06 - Dispensa de Licitação, 07 - Inexigível, 08 - Não se Aplica, 09 - Suprimento de Fundo, 11 - Consulta, 12 - Pregão' AFTER `ds_contratacao`;

ALTER TABLE  `contratos` CHANGE  `nu_processo`  `nu_processo` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;