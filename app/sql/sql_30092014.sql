ALTER TABLE  `situacoes` ADD  `tp_situacao` CHAR( 1 ) NOT NULL DEFAULT  'C' AFTER  `co_situacao`;

ALTER TABLE  `situacoes` ADD  `nu_sequencia` INT( 3 ) NOT NULL DEFAULT  '0' AFTER  `tp_situacao`;

ALTER TABLE  `contratos` CHANGE  `ds_objeto`  `ds_objeto` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `log_contratos` CHANGE  `ds_objeto`  `ds_objeto` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `aditivos` CHANGE  `ds_aditivo`  `ds_aditivo` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `log_aditivos` CHANGE  `ds_aditivo`  `ds_aditivo` VARCHAR( 1500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `pendencias` ADD  `co_situacao` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `pendencias` CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `pendencias` CHANGE  `ds_pendencia`  `ds_pendencia` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE  `log_pendencias` ADD  `co_situacao` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `log_pendencias` CHANGE  `ds_observacao`  `ds_observacao` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `log_pendencias` CHANGE  `ds_pendencia`  `ds_pendencia` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

--
-- Gatilhos `pendencias`
--
DROP TRIGGER IF EXISTS `ADD_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_PENDENCIA` AFTER INSERT ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',

            co_pendencia    = NEW.co_pendencia,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            co_situacao     = NEW.co_situacao,
            ds_pendencia    = NEW.ds_pendencia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            st_pendencia    = NEW.st_pendencia,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_PENDENCIA` AFTER UPDATE ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',

            co_pendencia    = NEW.co_pendencia,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            co_situacao     = NEW.co_situacao,
            ds_pendencia    = NEW.ds_pendencia,
            dt_inicio       = NEW.dt_inicio,
            dt_fim          = NEW.dt_fim,
            st_pendencia    = NEW.st_pendencia,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_PENDENCIA`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_PENDENCIA` AFTER DELETE ON `pendencias`
 FOR EACH ROW BEGIN
	INSERT INTO log_pendencias SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',

            co_pendencia    = OLD.co_pendencia,
            co_ata          = OLD.co_ata,
            co_contrato     = OLD.co_contrato,
            co_situacao     = OLD.co_situacao,
            ds_pendencia    = OLD.ds_pendencia,
            dt_inicio       = OLD.dt_inicio,
            dt_fim          = OLD.dt_fim,
            st_pendencia    = OLD.st_pendencia,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;

ALTER TABLE  `pendencias` ADD INDEX (  `co_situacao` );

ALTER TABLE  `pendencias` ADD FOREIGN KEY (  `co_situacao` ) REFERENCES  `situacoes` (
`co_situacao`
);


ALTER TABLE  `historicos` ADD  `co_situacao` INT( 11 ) NULL AFTER  `co_contrato`;

ALTER TABLE  `historicos` ADD INDEX (  `co_situacao` );

ALTER TABLE  `historicos` ADD FOREIGN KEY (  `co_situacao` ) REFERENCES  `situacoes` (
`co_situacao`
);

ALTER TABLE  `log_historicos` ADD  `co_situacao` INT( 11 ) NULL AFTER  `co_contrato`;

DROP TRIGGER IF EXISTS `ADD_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_HISTORICOS` AFTER INSERT ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
			
            co_historico    = NEW.co_historico,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            co_situacao     = NEW.co_situacao,
            no_assunto      = NEW.no_assunto,
            ds_historico    = NEW.ds_historico,
            dt_historico    = NEW.dt_historico,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_HISTORICOS` AFTER UPDATE ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A',
			 			
            co_historico    = NEW.co_historico,
            co_ata          = NEW.co_ata,
            co_contrato     = NEW.co_contrato,
            co_situacao     = NEW.co_situacao,
            no_assunto      = NEW.no_assunto,
            ds_historico    = NEW.ds_historico,
            dt_historico    = NEW.dt_historico,
            ds_observacao   = NEW.ds_observacao;
        END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `DEL_LOG_HISTORICOS`;
DELIMITER //
CREATE TRIGGER `DEL_LOG_HISTORICOS` AFTER DELETE ON `historicos`
 FOR EACH ROW BEGIN
	INSERT INTO LOG_HISTORICOS SET 
            dt_log          = NOW(), 
            co_usuario      = OLD.co_usuario, 
            tp_acao         = 'E',
			 			
            co_historico    = OLD.co_historico,
            co_ata          = OLD.co_ata,
            co_contrato     = OLD.co_contrato,
            co_situacao     = OLD.co_situacao,
            no_assunto      = OLD.no_assunto,
            ds_historico    = OLD.ds_historico,
            dt_historico    = OLD.dt_historico,
            ds_observacao   = OLD.ds_observacao;
        END
//
DELIMITER ;