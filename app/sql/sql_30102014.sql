ALTER TABLE  `contratos` ADD  `is_desvio_fluxo` CHAR( 1 ) NULL DEFAULT  '0' AFTER  `co_setor`;

ALTER TABLE  `log_contratos` ADD  `is_desvio_fluxo` CHAR( 1 ) NULL DEFAULT  '0' AFTER  `co_setor`;

ALTER TABLE  `log_contratos` ADD  `co_executante` INT( 11 ) NULL AFTER  `co_contratante` ,
ADD INDEX (  `co_executante` );

ALTER TABLE  `log_contratos` ADD  `co_contratacao` INT( 11 ) NULL AFTER  `co_executante`;

ALTER TABLE  `log_contratos` ADD  `dt_autorizacao_pam` DATE NULL AFTER  `ds_fundamento_legal` ,
ADD  `dt_fim_vigencia_inicio` DATE NULL AFTER  `dt_autorizacao_pam` ,
ADD  `vl_servico` DECIMAL( 10, 2 ) NULL AFTER  `dt_fim_vigencia_inicio` ,
ADD  `vl_material` DECIMAL( 10, 2 ) NULL AFTER  `vl_servico`;

DROP TRIGGER IF EXISTS `ADD_LOG_CONTRATO`;
DELIMITER //
CREATE TRIGGER `ADD_LOG_CONTRATO` AFTER INSERT ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I', 

            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_executante   = NEW.co_executante,
            co_contratacao  = NEW.co_contratacao,
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_sequencia    = NEW.nu_sequencia,
            co_fase         = NEW.co_fase,
            dt_fase         = NEW.dt_fase,
            co_setor        = NEW.co_setor,
            is_desvio_fluxo = NEW.is_desvio_fluxo,
            nu_pam          = NEW.nu_pam,
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            tp_aquisicao    = NEW.tp_aquisicao,
            ic_ata_vigente  = NEW.ic_ata_vigente,
            ic_ata_orgao    = NEW.ic_ata_orgao,
            ic_dispensa_licitacao = NEW.ic_dispensa_licitacao,
            ds_objeto       = NEW.ds_objeto, 
            nu_licitacao    = NEW.nu_licitacao,
            dt_publicacao   = NEW.dt_publicacao,
            ds_fundamento_legal = NEW.ds_fundamento_legal,
            dt_autorizacao_pam  = NEW.dt_autorizacao_pam,
            dt_assinatura   = NEW.dt_assinatura,
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_fim_vigencia_inicio = NEW.dt_fim_vigencia_inicio, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_servico      = NEW.vl_servico,
            vl_material     = NEW.vl_material,
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro,
            dt_cadastro_pam = NEW.dt_cadastro_pam,
            dt_cadastro_processo = NEW.dt_cadastro_processo,
            dt_tais         = NEW.dt_tais,
            dt_prazo_processo    = NEW.dt_prazo_processo;
END
//
DELIMITER ;

DROP TRIGGER IF EXISTS `UPD_LOG_CONTRATO`;
DELIMITER //
CREATE TRIGGER `UPD_LOG_CONTRATO` AFTER UPDATE ON `contratos`
 FOR EACH ROW BEGIN
	INSERT INTO log_contratos SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A', 

            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            co_executante   = NEW.co_executante,
            co_contratacao  = NEW.co_contratacao,
            co_fiscal_atual = NEW.co_fiscal_atual, 
            co_gestor_atual = NEW.co_gestor_atual, 
            nu_sequencia    = NEW.nu_sequencia,
            co_fase         = NEW.co_fase,
            dt_fase         = NEW.dt_fase,
            co_setor        = NEW.co_setor,
            is_desvio_fluxo = NEW.is_desvio_fluxo,
            nu_pam          = NEW.nu_pam,
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            tp_aquisicao    = NEW.tp_aquisicao,
            ic_ata_vigente  = NEW.ic_ata_vigente,
            ic_ata_orgao    = NEW.ic_ata_orgao,
            ic_dispensa_licitacao = NEW.ic_dispensa_licitacao,
            ds_objeto       = NEW.ds_objeto, 
            nu_licitacao    = NEW.nu_licitacao,
            dt_publicacao   = NEW.dt_publicacao,
            ds_fundamento_legal = NEW.ds_fundamento_legal,
            dt_autorizacao_pam  = NEW.dt_autorizacao_pam,
            dt_assinatura   = NEW.dt_assinatura,
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_fim_vigencia_inicio = NEW.dt_fim_vigencia_inicio, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_servico      = NEW.vl_servico,
            vl_material     = NEW.vl_material,
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro,
            dt_cadastro_pam = NEW.dt_cadastro_pam,
            dt_cadastro_processo = NEW.dt_cadastro_processo,
            dt_tais         = NEW.dt_tais,
            dt_prazo_processo    = NEW.dt_prazo_processo;
        END
//
DELIMITER ;

ALTER TABLE  `andamentos` ADD  `co_setor` INT( 11 ) NULL AFTER  `nu_sequencia` ,
ADD  `co_fase` INT( 11 ) NULL AFTER  `co_setor`;

ALTER TABLE  `andamentos` ADD  `ds_justificativa` VARCHAR( 255 ) NULL;

ALTER TABLE  `andamentos` CHANGE  `dt_fim`  `dt_fim` DATETIME NULL DEFAULT NULL;

ALTER TABLE  `andamentos` ADD INDEX (  `co_setor` );

ALTER TABLE  `andamentos` ADD INDEX (  `co_fase` );


ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `co_contrato` ) REFERENCES  `contratos` (
`co_contrato`
);

ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `co_usuario` ) REFERENCES  `usuarios` (
`co_usuario`
);

ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `nu_sequencia` ) REFERENCES  `fluxos` (
`nu_sequencia`
);

ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `co_setor` ) REFERENCES  `setores` (
`co_setor`
);

ALTER TABLE  `andamentos` ADD FOREIGN KEY (  `co_fase` ) REFERENCES  `fases` (
`co_fase`
);