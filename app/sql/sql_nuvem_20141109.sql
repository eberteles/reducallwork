
CREATE TABLE `instituicoes` (
  `co_instituicao` int(11) NOT NULL AUTO_INCREMENT,
  `co_instituicao_pai` int(11) NOT NULL,
  `nu_instituicao` int(11) NOT NULL,
  `ds_instituicao` varchar(200) NOT NULL,
  `co_usuario` int(11) NOT NULL,
  PRIMARY KEY (`co_instituicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `log_instituicoes` (
  `co_log_instituicoes` int(10) NOT NULL AUTO_INCREMENT,
  `dt_log` datetime NOT NULL,
  `co_usuario` int(11) NOT NULL,
  `tp_acao` char(1) NOT NULL,
  `co_instituicao` int(11) NOT NULL,
  `co_instituicao_pai` int(11) NOT NULL,
  `nu_instituicao` int(11) NOT NULL,
  `ds_instituicao` varchar(200) NOT NULL,
  PRIMARY KEY (`co_log_instituicoes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE  `usuarios` ADD `co_instituicoes` INT(11) NULL;