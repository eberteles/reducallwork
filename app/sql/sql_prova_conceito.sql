ALTER TABLE  `usuarios` ADD  `co_setor` INT( 11 ) NULL AFTER  `co_privilegio`;
ALTER TABLE  `usuarios` ADD  `ds_privilegios` VARCHAR( 255 ) NOT NULL;

ALTER TABLE  `contratos` ADD  `co_usuario` INT( 11 ) NOT NULL;
ALTER TABLE  `aditivos` ADD  `co_usuario` INT( 11 ) NOT NULL;

ALTER TABLE  `anexos` ADD  `ds_extensao` VARCHAR( 5 ) NOT NULL AFTER  `ds_anexo`;

ALTER TABLE  `aditivos` ADD  `dt_prazo` DATE NULL AFTER  `vl_aditivo`;

ALTER TABLE  `aditivos` CHANGE  `vl_aditivo`  `vl_aditivo` DECIMAL( 10, 2 ) NULL;

CREATE TABLE  `contratacoes` (
`co_contratacao` INT( 11 ) NULL AUTO_INCREMENT ,
`ds_contratacao` VARCHAR( 100 ) NOT NULL ,
PRIMARY KEY (  `co_contratacao` )
)

ALTER TABLE  `contratos` ADD  `co_contratacao` INT( 11 ) NULL AFTER  `co_contratante`

CREATE TABLE IF NOT EXISTS `alertas` (
  `co_alerta` int(11) NOT NULL AUTO_INCREMENT,
  `ds_email_from` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_smtp_host` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_smtp_port` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_smtp_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ds_smtp_pass` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ck_aviso30` char(1),
  `ck_aviso60` char(1),
  `ck_aviso90` char(1),
  PRIMARY KEY (`co_alerta`)
)



DELIMITER //
	CREATE TRIGGER ADD_LOG_CONTRATO AFTER INSERT ON CONTRATOS 
	FOR EACH ROW BEGIN
	INSERT INTO LOG_CONTRATOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro;
        END //
        DELIMITER;
        

DELIMITER //
	CREATE TRIGGER UPD_LOG_CONTRATO AFTER UPDATE ON CONTRATOS 
	FOR EACH ROW BEGIN
	INSERT INTO LOG_CONTRATOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'A', 
            co_contrato     = NEW.co_contrato, 
            co_situacao     = NEW.co_situacao, 
            co_modalidade   = NEW.co_modalidade, 
            co_servico      = NEW.co_servico, 
            co_categoria    = NEW.co_categoria, 
            co_subcategoria = NEW.co_subcategoria, 
            co_fornecedor   = NEW.co_fornecedor, 
            co_contratante  = NEW.co_contratante, 
            nu_contrato     = NEW.nu_contrato, 
            nu_processo     = NEW.nu_processo, 
            ds_objeto       = NEW.ds_objeto, 
            dt_ini_vigencia = NEW.dt_ini_vigencia, 
            dt_fim_vigencia = NEW.dt_fim_vigencia, 
            dt_ini_processo = NEW.dt_ini_processo, 
            dt_fim_processo = NEW.dt_fim_processo, 
            st_repactuado   = NEW.st_repactuado, 
            vl_inicial      = NEW.vl_inicial, 
            vl_mensal       = NEW.vl_mensal, 
            tp_valor        = NEW.tp_valor, 
            vl_global       = NEW.vl_global, 
            ds_observacao   = NEW.ds_observacao, 
            fg_financeiro   = NEW.fg_financeiro, 
            nu_pendencias   = NEW.nu_pendencias, 
            dt_cadastro     = NEW.dt_cadastro;
        END //
        DELIMITER;


DELIMITER //
	CREATE TRIGGER ADD_LOG_ADITIVO AFTER UPDATE ON ADITIVOS 
	FOR EACH ROW BEGIN
	INSERT INTO LOG_ADITIVOS SET 
            dt_log          = NOW(), 
            co_usuario      = NEW.co_usuario, 
            tp_acao         = 'I',
            co_aditivo      = NEW.co_aditivo,
            co_contrato     = NEW.co_contrato,
            dt_aditivo      = NEW.dt_aditivo,
            vl_aditivo      = NEW.vl_aditivo,
            ds_aditivo      = NEW.ds_aditivo;
        END //
        DELIMITER;

