<?php
App::import('Model','Aditivo');

class AditivoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.aditivo' );
	function startTest() {
		$this->Aditivo =& ClassRegistry::init('Aditivo');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Aditivo);
		ClassRegistry::flush();
	}
}