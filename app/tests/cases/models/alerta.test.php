<?php
App::import('Model','Alerta');

class AlertaTestCase extends CakeTestCase {
	
	var $fixtures = array( 'app.alerta' );
	
	function startTest() {
		$this->Alerta =& ClassRegistry::init('Alerta');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Alerta);
		ClassRegistry::flush();
	}
}