<?php
App::import('Model','Andamento');

class AndamentoTestCase extends CakeTestCase {
	
	var $fixtures = array( 'app.andamento' );
	
	function startTest() {
		$this->Andamento =& ClassRegistry::init('Andamento');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Andamento);
		ClassRegistry::flush();
	}
}