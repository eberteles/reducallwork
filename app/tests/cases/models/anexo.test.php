<?php
App::import('Model','Anexo');

class AnexoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.anexo' );
	function startTest() {
		$this->Anexo =& ClassRegistry::init('Anexo');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Anexo);
		ClassRegistry::flush();
	}
}