<?php
App::import('Model','AnexoPasta');

class AnexoPastaTestCase extends CakeTestCase {
	var $fixtures = array( 'app.anexo_pasta' );
	function startTest() {
		$this->AnexoPasta =& ClassRegistry::init('AnexoPasta');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->AnexoPasta);
		ClassRegistry::flush();
	}
}