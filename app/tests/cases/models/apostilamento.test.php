<?php
App::import('Model','Apostilamento');

class ApostilamentoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.apostilamento' );
	function startTest() {
		$this->Apostilamento =& ClassRegistry::init('Apostilamento');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Apostilamento);
		ClassRegistry::flush();
	}
}