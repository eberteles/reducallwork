<?php
App::import('Model','Aquisicao');

class AquisicaoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.aquisicao' );
	function startTest() {
		$this->Aquisicao =& ClassRegistry::init('Aquisicao');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Aquisicao);
		ClassRegistry::flush();
	}
}