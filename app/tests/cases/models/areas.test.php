<?php
App::import('Model','Area');

class AreaTestCase extends CakeTestCase {
	var $fixtures = array( 'app.area' );
	
	function startTest() {
		$this->Area =& ClassRegistry::init('Area');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Area);
		ClassRegistry::flush();
	}
}