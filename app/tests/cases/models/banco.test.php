<?php
App::import('Model','Banco');

class BancoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.banco' );
	function startTest() {
		$this->Banco =& ClassRegistry::init('Banco');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Banco);
		ClassRegistry::flush();
	}
		
	function testCamposObrigatorios() {	
		$result = $this->Banco->hasField('ds_banco');
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}
}