<?php
App::import('Model','Contratacao');

class ContratacaoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.contratacao' );
	
	function startTest() {
		$this->Contratacao =& ClassRegistry::init('Contratacao');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Contratacao);
		ClassRegistry::flush();
	}
		
	function testCamposObrigatorios() {	
		$result = $this->Contratacao->hasField('ds_banco');
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}
}