<?php
App::import('Model','Fornecedor');

class FornecedorTestCase extends CakeTestCase {
	var $fixtures = array( 'app.fornecedor' );
	
	function startTest() {
		$this->Fornecedor =& ClassRegistry::init('Fornecedor');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Fornecedor);
		ClassRegistry::flush();
	}
	
	function testCamposObrigatorios() {		
		$result = $this->Fornecedor->hasField('ic_ativo');
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}	
}