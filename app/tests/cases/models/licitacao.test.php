<?php
App::import('Model','Licitacao');

class LicitacaoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.licitacao' );
	
	function startTest() {
		$this->Licitacao =& ClassRegistry::init('Licitacao');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Licitacao);
		ClassRegistry::flush();
	}
	
	function testCriacao() {		
		$result = $this->Licitacao->findAll();
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}	
}