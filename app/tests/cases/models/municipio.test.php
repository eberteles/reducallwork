<?php
App::import('Model','Municipio');

class MunicipioTestCase extends CakeTestCase {
	var $fixtures = array( 'app.municipio', 'app.uf' );
	function startTest() {
		$this->Municipio =& ClassRegistry::init('Municipio');
	}
	
	function endTest() {
		unset($this->Municipio);
		ClassRegistry::flush();
	}
	
	
	function testCamposObrigatorios() {	
		$result = $this->Municipio->hasField('ds_municipio');
		
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}
}