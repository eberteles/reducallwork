<?php
App::import('Model','Situacao');

class SituacaoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.situacao' );
	
	function startTest() {
		$this->Situacao =& ClassRegistry::init('Situacao');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Situacao);
		ClassRegistry::flush();
	}
	
	function testCriacao() {		
		$situacao = array(
			'Situacao' => array(
				'tp_situacao' => 'C',
				'nu_sequencia' => 0,
				'ds_situacao' => 'Teste',
				'ic_ativo' => 1,
			)
		);
		
		$result = $this->Situacao->save($situacao);
		$expected = array(
			true
		);
	
		$this->assertEqual($result, $expected);
	}	
}