<?php
App::import('Model', 'Subcategoria');

class SubcategoriaTestCase extends CakeTestCase
{
    var $fixtures = array('app.subcategoria', 'app.categoria', 'app.log');

    function startTest()
    {
        $this->Subcategoria = ClassRegistry::init('Subcategoria');
        $this->dropTables = false;
    }

    function endTest()
    {
        unset($this->Subcategoria);
        ClassRegistry::flush();
    }

    function testCriacao()
    {
        $subcategoria = array(
            'Subcategoria' => array(
                'co_categoria' => 1,
                'no_subcategoria' => 'SubCategoria Teste',
                'ic_ativo' => 2,
            )
        );
        $saved = $this->Subcategoria->save($subcategoria);
        $this->assertTrue(
            isset($saved['Subcategoria']['co_subcategoria']),
            'Erro ao salvar Subcategoria'
        );
    }

    function testNoSubcategoriaObrigatorio()
    {
        $subcategoria = array(
            'Subcategoria' => array(
                'co_categoria' => 1,
//                'no_subcategoria' => 'SubCategoria 1',
                'no_subcategoria' => null,
                'ic_ativo' => 2,
            )
        );
        $this->Subcategoria->save($subcategoria);

        $this->assertEqual(
            $this->Subcategoria->validationErrors['no_subcategoria'],
            'Campo descrição em branco',
            'Erro ao verificar a obrigatoriedade do Nome da Subcategoria'
        );
    }

    function testNoSubcategoriaUnico()
    {
        $subcategoria = array(
            'Subcategoria' => array(
                'co_categoria' => 1,
                'no_subcategoria' => 'SubCategoria 1',
                'ic_ativo' => 2,
            )
        );
        $this->Subcategoria->save($subcategoria);

        $this->assertEqual(
            $this->Subcategoria->validationErrors['no_subcategoria'],
            'A sub categoria já está cadastrada',
            'Erro ao verificar a unicidade do Nome da Subcategoria'
        );
    }

    function testFindByName()
    {
        $result = $this->Subcategoria->findByName('SubCategoria 1');
        $this->assertTrue($result, "Não encontrou o registro 'SubCategoria 1'");
    }

}
