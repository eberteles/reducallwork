<?php
App::import('Model','TipoLicitacao');

class TipoLicitacaoTestCase extends CakeTestCase {
	var $fixtures = array( 'app.tipo_licitacao' );
	
	function startTest() {
		$this->TipoLicitacao =& ClassRegistry::init('TipoLicitacao');
		$this->dropTables = false;
	}
	
	function endTest() {
		unset($this->TipoLicitacao);
		ClassRegistry::flush();
	}

	function testCamposObrigatorios() {	
		$result = $this->TipoLicitacao->hasField('ic_ativo');
		$expected = array(
				true
		);
	
		$this->assertEqual($result, $expected);
	}
}