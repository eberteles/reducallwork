<?php
App::import('Model','Uf');

class UfTestCase extends CakeTestCase {
	var $fixtures = array( 'app.uf' );
	
	function startTest() {
		$this->Uf =& ClassRegistry::init('Uf');
		# $this->dropTables = false;
	}
	
	function endTest() {
		unset($this->Uf);
		ClassRegistry::flush();
	}
	
	function testUfs() {
		$result = $this->Uf->findBySgUf('DF');
		
		$expected = array(
			'Uf' => array( 'sg_uf' => 'DF', 'no_uf' => 'Distrito Federal' ),
		);
	
		$this->assertEqual($result, $expected);
	}
}