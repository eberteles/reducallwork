<?php

class CategoriaFixture extends CakeTestFixture
{
    var $name = 'Categoria';
    var $import = array(
        'model' => 'Categoria',
        'records' => array(
            array(
                'Categoria' => array(
                    'co_categoria' => 1,
                    'no_subcategoria' => 'CATEGORIA 1',
                    'ic_ativo' => 2,
                )
            ),
        )
    );
}
