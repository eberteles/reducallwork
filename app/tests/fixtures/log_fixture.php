<?php

class LogFixture extends CakeTestFixture
{
    var $name = 'Log';
    var $import = array(
        'model' => 'Log',
        'records' => array()
    );
}
