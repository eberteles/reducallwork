<?php

class SubcategoriaFixture extends CakeTestFixture
{
    var $name = 'Subcategoria';
    var $import = 'Subcategoria';

    var $records = array(
        array(
            'co_subcategoria' => 1,
            'co_categoria' => 1,
            'no_subcategoria' => 'SUBCATEGORIA 1',
            'ic_ativo' => 2,
        ),
        array(
            'co_subcategoria' => 2,
            'co_categoria' => 1,
            'no_subcategoria' => 'SUBCATEGORIA 2',
            'ic_ativo' => 2,
        ),
        array(
            'co_subcategoria' => 3,
            'co_categoria' => 1,
            'no_subcategoria' => 'SUBCATEGORIA 3',
            'ic_ativo' => 1,
        )
    );

}
