<div class="acessos form"><?php echo $this->Form->create('Acesso');?>
<fieldset><legend><?php __('Acesso'); ?></legend> <?php
echo $this->Form->input('co_acesso');
echo $this->Form->input('ds_acesso');
echo $this->Form->input('Privilegio');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('Acesso.id')), null, sprintf(__('Tem certeza de que deseja excluir # %s?', true), $this->Form->value('Acesso.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Acessos', true), array('action' => 'index'));?></li>
	<li><?php echo $this->Html->link(__('Listar Privilegios', true), array('controller' => 'privilegios', 'action' => 'index')); ?>
	</li>
	<li><?php echo $this->Html->link(__('Novo Privilegio', true), array('controller' => 'privilegios', 'action' => 'add')); ?>
	</li>
</ul>
</div>
