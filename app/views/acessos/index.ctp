<div class="acessos index">
    <div class="page-header position-relative">
<h1><?php __('Acessos');?></h1>
    </div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('co_acesso');?></th>
		<th><?php echo $this->Paginator->sort('ds_acesso');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($acessos as $acesso):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $acesso['Acesso']['co_acesso']; ?>&nbsp;</td>
		<td><?php echo $acesso['Acesso']['ds_acesso']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $acesso['Acesso']['co_acesso']; ?> <?php echo $this->Html->link($this->Html->image('ico_procurar.gif'), array('action' => 'view', $id ), array('escape' => false)); ?>
		<?php echo $this->Html->link($this->Html->image('ico_alterar.gif'), array('action' => 'edit', $id), array('escape' => false)); ?>
		<?php echo $this->Html->link($this->Html->image('ico_excluir.gif'), array('action' => 'delete', $id), array('escape' => false), sprintf(__('Tem certeza de que deseja excluir # %s?', true), $id)); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Acesso', true), array('action' => 'add')); ?></li>
	<li><?php echo $this->Html->link(__('Listar Privilegios', true), array('controller' => 'privilegios', 'action' => 'index')); ?>
	</li>
	<li><?php echo $this->Html->link(__('Novo Privilegio', true), array('controller' => 'privilegios', 'action' => 'add')); ?>
	</li>
</ul>
</div>
