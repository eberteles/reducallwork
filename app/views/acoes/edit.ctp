<div class="acoes form"><?php echo $this->Form->create('Acao');?>
<fieldset><legend><?php __('Acao'); ?></legend> <?php
echo $this->Form->input('co_acao');
echo $this->Form->input('ds_acao');
echo $this->Form->input('Usuario');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('Acao.id')), null, sprintf(__('Tem certeza de que deseja excluir # %s?', true), $this->Form->value('Acao.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Acoes', true), array('action' => 'index'));?></li>
	<li><?php echo $this->Html->link(__('Listar Usuarios', true), array('controller' => 'usuarios', 'action' => 'index')); ?>
	</li>
	<li><?php echo $this->Html->link(__('Novo Usuario', true), array('controller' => 'usuarios', 'action' => 'add')); ?>
	</li>
</ul>
</div>
