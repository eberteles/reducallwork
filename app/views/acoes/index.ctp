<div class="acoes index">
    <div class="page-header position-relative"><h1><?php __('Acoes');?></h1></div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('co_acao');?></th>
		<th><?php echo $this->Paginator->sort('ds_acao');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($acoes as $acao):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $acao['Acao']['co_acao']; ?>&nbsp;</td>
		<td><?php echo $acao['Acao']['ds_acao']; ?>&nbsp;</td>
		<td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $acao['Acao']['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $acao['Acao']['id'])); ?>
		<?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $acao['Acao']['id']), null, sprintf(__('Tem certeza de que deseja excluir # %s?', true), $acao['Acao']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Acao', true), array('action' => 'add')); ?></li>
	<li><?php echo $this->Html->link(__('Listar Usuarios', true), array('controller' => 'usuarios', 'action' => 'index')); ?>
	</li>
	<li><?php echo $this->Html->link(__('Novo Usuario', true), array('controller' => 'usuarios', 'action' => 'add')); ?>
	</li>
</ul>
</div>
