<?php
echo $this->Html->script( 'inicia-datetimepicker' );
$tp_aditivo = $aditivo['tp_aditivo'];
?>
<div class="aditivos form">
    <?php echo $this->Form->create('Aditivo', array(
        'url' => "/aditivos/edit/$id/$coContrato",
        'id' => "edit_aditivo"
    ));?>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar o Aditivo - <b>Campos com * são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'aditivos', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Aditivos">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span12 ">
            <div class="widget-header widget-header-small"><h4>Alterar <?php echo $this->Print->tpAditivo($aditivo['tp_aditivo']); ?></h4></div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->hidden('co_aditivo');
                                echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                                echo $this->Form->hidden('contrato_fim_vigencia', array('value' => $dtFimVigenciaContrato));
                                echo $this->Form->hidden('vl_global', array('value' => $vl_global, 'maxlength' => 18));
                                echo $this->Form->hidden('tp_aditivo', array('value' => $tp_aditivo));
                                if($this->Modulo->isCamposContrato('no_aditivo') == true) {
                                    echo $this->Form->input('no_aditivo', array('label' => 'Descrição', 'required' => true));
                                }

                                echo $this->Form->input('ds_fundamento_legal', array('label' => __('Fundamento legal', true), 'type'=>'text', 'class'=>'input-xlarge', 'maxlength' => 150));

                                echo $this->Form->input('ds_aditivo', array(
                                    'label' => __('Justificativa', true),
                                    'type' => 'textarea',
                                    'class' => 'input-xlarge',
                                    'onKeyup' => '$(this).limit("1500","#charsLeft")',
                                    'after' => '<br><span id="charsLeft">1500</span> caracteres restantes.'
                                ));

                                ?>
                                    <table id='valorPercentual'>
                                        <tr>
                                            <td>
                                                <?php
                                                echo $this->Form->input('vl_aditivo', array('label' => 'Valor Aditivo (R$)', 'class' => 'input-small', 'value' => $this->Print->real($aditivo['vl_aditivo']), 'maxlength' => 18));
                                                ?>
                                            </td>
                                            <td>&nbsp;&nbsp;<b> ou </b>&nbsp;&nbsp;</td>
                                            <td>
                                                <?php
                                                echo $this->Form->input('pc_aditivo', array('class' => 'input-mini alert-tooltip', 'title' => 'Preencher caso deseje informar o percentual do Aditivo.', 'label' => 'Percentual do Aditivo (%)', 'type' => 'text', 'value' => $this->Print->real($aditivo['pc_aditivo'])));
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                            </dl>
                        </div>

                        <div class="span4">
                            <div class="control-group">
                                <?php
                                /*
                                *   [GES-626] O sistema deve permitir a alteração do tipo do aditivo.
                                */
                                echo $this->Form->input('tp_aditivo', array(
                                    'type' => 'select',
                                    'label' => 'Tipo do Aditivo:',
                                    'empty' => 'Selecione',
                                    'onchange' => 'regrasFormulario(this)',
                                    'options' => Aditivo::getTipos()
                                ));
                                echo $this->Form->input('tp_aditivo_valor', array(
                                    'type' => 'select',
                                    'disabled' => 'disabled',
                                    'empty' => 'Selecione',
                                    'label' => 'Classificação do Aditivo de Valor:',
                                    'options' => Aditivo::getClassificacaoAditivoValor()
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="span4">
                            <dl class="dl-horizontal">
                                <div class="required">
                                    <?php

                                        echo $this->Form->input('dt_assinatura', array(
                                            'before' => '<div class="input-append date datetimepicker" id="dataDeAssinatura">',
                                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                            'class' => 'input-small',
                                            'label' => 'Data de assinatura',
                                            'type'=>'text',
                                            'id' => 'DtAssinatura'
                                        ));

                                        echo $this->Form->input('dt_publicacao', array(
                                            'before' => '<div class="input-append date datetimepicker" id="dataDePublicacao">',
                                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                            'class' => 'input-small',
                                            'label' => 'Data de publicação',
                                            'type'=>'text'
                                        ));

                                        ?>
                                        <div id="erro_data_publicacao" class="error-message">Data de Publicação do Aditivo não pode ser maior que Data Final de Vigência do Contrato! Insira uma data válida e tente novamente!</div>
                                        <div id="erro_data_publicacao_branco" class="error-message">Campo Data de Publicação em branco!</div>
                                </div>
                                <?php

                                echo $this->Form->input('dt_aditivo', array(
                                    'before' => '<div class="input-append date datetimepicker" id="dataDoAditivo">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy',
                                    'mask' => '99/99/9999',
                                    'class' => 'input-small',
                                    'label' => 'Data do Aditivo',
                                    'type' => 'text'
                                ));

                                ?>

                                    <table id='dataPrazo'>
                                        <tr>
                                            <td>
                                                <?php

                                                echo $this->Form->input('dt_prazo', array(
                                                    'before' => '<div id="divDtPrazo" class="input-append date datetimepicker">',
                                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span>',
                                                    'data-format' => 'dd/MM/yyyy',
                                                    'mask' => '99/99/9999',
                                                    'class' => 'input-small',
                                                    'label' => 'Data final da Vigência',
                                                    'type' => 'text'
                                                ));

                                                ?>
                                            </td>
                                            <td>&nbsp;&nbsp;<b> ou </b>&nbsp;&nbsp;</td>
                                            <td>
                                                <label>Prazo em Dias</label>
                                                <input id="AditivoDtPrazoEmDias" class="input-small" type="number" name="data[Aditivo][dt_prazo_em_dias]" style="margin-top: 10px;">
                                            </td>
                                        </tr>
                                    </table>
                                    <?php

                                echo $this->Form->hidden('dt_fim_autorizacao', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small alert-tooltip AditivoDtFimAutorizacao',
                                    'title' => 'O sistema avisará 10 dias antes da data informanda o término da autorização do aditivo.',
                                    'label' => 'Término Autorização',
                                    'type'=>'text'
                                ));


                                echo $this->Form->input('dt_fim_autorizacao', array(
                                    // echo $this->Form->hidden('dt_fim_autorizacao', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small alert-tooltip AditivoDtFimAutorizacao',
                                    'title' => 'O sistema avisará 10 dias antes da data informanda o término da autorização do aditivo.',
                                    'label' => 'Término Autorização',
                                    'type'=>'text'
                                ));
                                ?>
                            </dl>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <div id="aguarde" class="modal fade" data-backdrop="static" style="display: none;background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
        <br><br><br><br><br>&nbsp;<br><center>Aguarde...</center><br>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" id="salvarAditivo" title="Salvar Aditivo"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm" > Limpar</button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

</div>

<script type="text/javascript">


    $('#LimparForm').click(function () {
        $('#AditivoNoAditivo').val('');
        $('#AditivoDsFundamentoLegal').val('');
        $('#AditivoDsAditivo').val('');
        $('#AditivoVlAditivo').val('');
        $('#AditivoPcAditivo').val('');
        $('#AditivoDtAditivo').val('');
        $('.AditivoDtFimAutorizacao').val('');
    });


    $(document).ready(function() {

        $('#erro_data_publicacao').hide();
        $('#erro_data_publicacao_branco').hide();

        $( "#AditivoPcAditivo" ).on('focusout', function(e) {
            var percentual = parseFloat( $("#AditivoPcAditivo").val().replace(",","."));
            if(percentual > 0) {
                $("#AditivoVlAditivo").unmask();
                $("#AditivoVlAditivo").val( formatReal( (percentual / 100 * $("#AditivoVlGlobal").val()).toFixed(2).replace(".","") ) );
            }
        });

        $( "#AditivoVlAditivo" ).on('focusout', function(e) {
            $("#AditivoPcAditivo").unmask();
            $("#AditivoPcAditivo").val( "0,00" );
        });

        $('#salvarAditivo').on('click', function(){
            if(!checkData()){
                return false;
            }
        });

        $("#AditivoVlAditivo").maskMoney({thousands:'.', decimal:','});
        $("#AditivoPcAditivo").maskMoney({thousands:'.', decimal:','});

        <?php
        if ( in_array($tp_aditivo, array(
            Aditivo::ADITIVO_PRAZO,
            Aditivo::ADITIVO_VALOR_PRAZO,
        ))) {
        ?>
        $('#AditivoDtPrazoEmDias').change(function() {
            if ($("#AditivoDtPrazoEmDias").val() != ""){

                var value = moment($("#AditivoDtAditivo").val(), 'DD/MM/YYYY');

                $("#AditivoDtPrazo").val(
                    value
                        .add($('#AditivoDtPrazoEmDias').val(), 'days')
                        .format('DD/MM/YYYY')
                );

            }
        });
        <?php } ?>
        regrasFormulario($("#AditivoTpAditivo")[0]);
    });

    /*
    *   [GES-625] Regras de formulario
    */
    function regrasFormulario(valor){

        campoTipoAditivoValor(valor.value);
        $('#dataPrazo').show();
        $('#valorPercentual').show();
        $('#dataDeAssinatura').show();
        $('#dataDoAditivo').show();
        $('#dataDePublicacao').show();

        switch(valor.value) {
            case "<?php echo Aditivo::ADITIVO_VALOR; ?>":
                $('#dataPrazo').hide();
                break;
            case "<?php echo Aditivo::ADITIVO_VALOR_PRAZO; ?>":
                break;
            case "<?php echo Aditivo::ADITIVO_PRAZO; ?>":
                $('#valorPercentual').hide();
                break;
            case "<?php echo Aditivo::ADITIVO_OUTROS; ?>":
                $('#valorPercentual').hide();
                $('#dataDoAditivo').hide();
                $('#dataPrazo').hide();
                break;
            default:
                break;
        }
    }

    function campoTipoAditivoValor(valor){
        if(valor == <?php echo Aditivo::ADITIVO_VALOR; ?> || valor == <?php echo Aditivo::ADITIVO_VALOR_PRAZO; ?>)  {
            $('#AditivoTpAditivoValor').prop("disabled", false);
            $('#divTipoAditivoValor').addClass("required");
        }else{
            $('#AditivoTpAditivoValor').prop("disabled", true);
            $('#divTipoAditivoValor').removeClass("required");
        }
    }

    function checkData(){
        var dataPublicacao = $('#AditivoDtPublicacao').val();
        var dataFimVigenciaContrato = $("#AditivoDtPrazo").val();
        var dataFimVigenciaProcesso = $("#AditivoContratoFimProcesso").val();

        var dataFim = (dataFimVigenciaContrato !== '') ? dataFimVigenciaContrato : dataFimVigenciaProcesso;

        if(dataPublicacao == ''){
            $('#erro_data_publicacao').hide();
            $('#erro_data_publicacao_branco').show();
            return false;
        }else if(dataFim != '' && ($.datepicker.parseDate('dd/mm/yy', dataPublicacao)) > ($.datepicker.parseDate('dd/mm/yy', dataFim))) {
            $('#erro_data_publicacao_branco').hide();
            $('#erro_data_publicacao').show();
            return false;
        } else {
            $('#erro_data_publicacao').hide();
            $('#erro_data_publicacao_branco').hide();
            return true;
        }
    }

</script>