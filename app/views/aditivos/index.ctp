<?php
$usuario = $this->Session->read('usuario');
$hasPenalidade = $this->Session->read('hasPenalidade');
?>
<div class="aditivos index">
   
    <table cellpadding="0" cellspacing="0" style="background-color: white"
           class="table table-hover table-bordered table-striped" id="tbAditivo">
        <tr>
            <th><?php __('Descrição'); ?></th>
            <th><?php __('Tipo de Aditivo'); ?></th>
            <!--th>< ?php __('Fundamento legal');?></th-->
            <th><?php __('Data assinatura'); ?></th>
            <th><?php __('Valor Aditivo'); ?></th>
            <th><?php __('Prazo Aditivo'); ?></th>
            <th><?php __('Justificativa'); ?></th>
            <th><?php __('Término Autorização'); ?></th>
            <th class="actions">
                <?php __('Ações'); ?>
            </th>
        </tr>
        <?php
        $i = 0;
        foreach ($aditivos as $aditivo) :
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }

            $tipoDoAditivo = Aditivo::getTipos();
            $tipoDoAditivoDeValor = Aditivo::getClassificacaoAditivoValor();

            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $aditivo['Aditivo']['no_aditivo']; ?>&nbsp;</td>
                <td title="<?php echo $tipoDoAditivoDeValor[$aditivo['Aditivo']['tp_aditivo_valor']]; ?>"><?php echo $tipoDoAditivo[$aditivo['Aditivo']['tp_aditivo']]; ?>&nbsp;</td>
                <!-- td>< ?php echo $aditivo['Aditivo']['ds_fundamento_legal']; ?>&nbsp;</td-->
                <td><?php echo $aditivo['Aditivo']['dt_assinatura']; ?>&nbsp;</td>
                <td><?php echo $this->Formatacao->moeda($aditivo['Aditivo']['vl_aditivo']); ?>&nbsp;</td>
                <td><?php if (!empty($aditivo['Aditivo']['dt_prazo'])) {
                        echo $aditivo['Aditivo']['dt_aditivo'] . ' à ' . $aditivo['Aditivo']['dt_prazo'];
                    } ?>&nbsp;</td>

                <td><?php echo $aditivo['Aditivo']['ds_aditivo']; ?>&nbsp;</td>
                <td><?php echo $aditivo['Aditivo']['dt_fim_autorizacao']; ?>&nbsp;</td>
                <td class="actions"><?php $id = $aditivo['Aditivo']['co_aditivo']; ?>
                    <div class="btn-group acoes">
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo"
                           data-toggle="modal"><i
                                    class="silk-icon-folder-table-btn alert-tooltip"
                                    title="Anexar Aditivo" style="display: inline-block;"></i></a>
                        <?php echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'aditivos/')) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
<?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'aditivos/add', $hasPenalidade)): ?>
    <div class="actions">
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">
                Selecione uma Ação
                <!--span class="required" title="Required">*</span-->
            </p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'aditivos', 'action' => 'add', $coContrato))?>" id="btn-add-aditivo" data-toggle="modal"
                   class="btn btn-small btn-primary">Adicionar</a>
            </div>
        </div>

    </div>
<?php endif; ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-hidden="true">×
        </button>
        <h3 id="myModalLabel">Suporte Documental - Anexar Aditivo</h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo"></div>
</div>

<script type="text/javascript">

    $('.alert-tooltip').tooltip();

    $('#tbAditivo tr td a.v_anexo').click(function () {
        var url_an = '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $coContrato)); ?>/aditivo/' + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_an,
            data: {},
            success: function (result) {
                $('#fis_anexo').html("");
                $('#fis_anexo').append(result);
            }
        });
    });

    $(document).ready(function () {
        $('#add_concluir').click(function () {
            $(location).attr('href', '<?php echo $this->Html->url(array('controller' => 'aditivos', 'action' => 'add', $coContrato))?>/' + $('#tp_aditivo').val());
        });
    });

</script>
