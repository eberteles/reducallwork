<?php $usuario = $this->Session->read('usuario'); ?>
<?php echo $this->Form->create('Alerta'); ?>

<?php echo $this->Form->input('co_alerta', array('type' => 'hidden')) ?>

    <div class="row-fluid">

        <!--        <div class="row-fluid" id="msg-success">-->
        <!--            <div class="span12">-->
        <!--                <div class="alert alert-warning">-->
        <!--                    Usuário habilitado para o recebimento de alertas!-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!---->
        <!--        <div class="row-fluid" id="msg-warning">-->
        <!--            <div class="span12">-->
        <!--                <div class="alert alert-warning">-->
        <!--                    O número de CPF é inválido!-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!---->
        <!--        <div class="row-fluid" id="msg-error">-->
        <!--            <div class="span12">-->
        <!--                <div class="alert alert-warning">-->
        <!--                    Usuário desabilitado com sucesso!-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->

        <div class="row-fluid">

            <div class="span12">

                <div class="page-header position-relative">
                    <h1>
                        <?php __('Central de Alertas'); ?>
                    </h1>
                </div>

                <div id="accordion4" class="accordion">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a href="#collapseAbaExp" data-parent="#accordion4" data-toggle="collapse"
                               class="accordion-toggle" id="expiracao">
                                Aviso sobre Expiração de
                                <?php __('Contratos'); ?>
                                :
                            </a>
                        </div>

                        <div class="accordion-body in collapse" id="collapseAbaExp" style="height: auto;width: 100%;">
                            <div class="accordion-inner">

                                <div class="row-fluid">
                                    <div class="span4">

                                        <table cellpadding="0" cellspacing="0"
                                               class="table table-hover table-bordered table-striped">
                                            <tr>
                                                <th width="12%">
                                                    <?php __('Dias'); ?>
                                                </th>
                                                <th width="22%">
                                                    <?php __('Gestores'); ?>
                                                </th>

                                                <?php if ($this->Modulo->isCamposContrato('co_contratante') || $this->Modulo->isCamposContrato('co_executante')) { ?>
                                                    <th width="22%">
                                                        <?php __('Unidade'); ?>
                                                        do
                                                        <?php __('Gestor'); ?>
                                                    </th>
                                                <?php } ?>

                                                <?php if (Configure::read('App.config.resource.available.fiscal')) { ?>
                                                    <th width="22%">
                                                        <?php __('Fiscais'); ?>
                                                    </th>
                                                <?php } ?>

                                                <th width="22%">
                                                    <?php __('Fornecedores'); ?>
                                                </th>
                                            </tr>

                                            <?php foreach ($vencimentosDashboard as $vencimentoDashboard) { ?>
                                                <tr>
                                                    <td class="center">
                                                        <b>
                                                            <?php echo $vencimentoDashboard; ?>
                                                        </b>
                                                    </td>
                                                    <td class="center">
                                                        <?php echo $this->Form->checkbox('ck_aviso' . $vencimentoDashboard); ?>
                                                    </td>

                                                    <?php if ($this->Modulo->isCamposContrato('co_contratante') || $this->Modulo->isCamposContrato('co_executante')) { ?>
                                                        <td class="center">
                                                            <?php echo $this->Form->checkbox('ck_aviso_uni' . $vencimentoDashboard); ?>
                                                        </td>
                                                    <?php } ?>

                                                    <?php if (Configure::read('App.config.resource.available.fiscal')) { ?>
                                                        <td class="center">
                                                            <?php echo $this->Form->checkbox('ck_aviso_fis' . $vencimentoDashboard); ?>
                                                        </td>
                                                    <?php } ?>

                                                    <td class="center">
                                                        <?php echo $this->Form->checkbox('ck_aviso_for' . $vencimentoDashboard); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?> <!-- End Foreach-->

                                        </table>

                                        <div class="row-fluid">
                                            <div class="span12">
                                                <p>Avisar os usuários pertinentes aos setores quando o processo mudar de
                                                    fase? <?php echo $this->Form->checkbox('ck_aviso_muda_fase'); ?></p>
                                            </div>
                                        </div>

                                    </div><!--End span4-->

                                    <div class="span8">

                                        <table cellpadding="0" cellspacing="0" id="tableAddEmails"
                                               class="table table-hover table-bordered"
                                               style="background-color: #F9F9F9;">
                                            <tr>
                                                <th width="20%"><?php __('CPF'); ?><b style="color: red;"> *</b></th>
                                                <th width="30%"><?php __('Nome'); ?><b style="color: red;"> *</b></th>
                                                <th width="30%"><?php __('Email'); ?><b style="color: red;"> *</b></th>
                                                <?php foreach ($vencimentosDashboard as $vencimentoDashboard) { ?>
                                                    <th width="3%"><?php echo $vencimentoDashboard; ?></th>
                                                <?php } ?>
                                                <th width="4%">Inserido</th>
                                                <th width="4%">Excluir</th>
                                            </tr>
                                        </table>

                                        <div class="align-right">
                                            <div class="btn-group">
                                                <button class="btn btn-small btn-circle btn-info" type="button"
                                                        onclick="addRow()"><i class="icon-plus"></i> Adicionar mais um
                                                    email
                                                </button>
                                            </div>
                                        </div>

                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar">
                    Salvar
                </button>
                <a href="/" class="btn btn-small" title="Voltar">Voltar</a>
            </div>
        </div>

    </div><!--End row-fluid-->


    <div id="view_novo_auxiliar" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="fechar">×</button>
            <h3 id="myModalLabel" class="">Novo Auxiliar</h3>
        </div>
        <div id="iframe_auxiliar" class="modal-body-iframe"></div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            carregaCadastrados();
        });

        function carregaCadastrados() {
            var i;
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'loadEmails'))?>" + "/", function (data) {
                if (data.msg = 'true') {
                    for (i = 0; i < data.emails.length; i++) {
                        addRow(data.emails[i]["GrupoAuxiliar"]["nu_cpf"], data.emails[i]["GrupoAuxiliar"]["ds_nome"],
                            data.emails[i]["GrupoAuxiliar"]["ds_email"], data.emails[i]["GrupoAuxiliar"]["ck_aviso30"],
                            data.emails[i]["GrupoAuxiliar"]["ck_aviso60"], data.emails[i]["GrupoAuxiliar"]["ck_aviso90"],
                            data.emails[i]["GrupoAuxiliar"]["ck_aviso120"], data.emails[i]["GrupoAuxiliar"]["co_usuario"]);
                    }
                } else {
                    console.log("Empty loadEmails");
                }

            });
        }

        function addRow(cpf, nome, email, ck30, ck60, ck90, ck120, id) {
            var table = document.getElementById("tableAddEmails");
            var numOfRows = table.rows.length;
            var numOfCols = table.rows[numOfRows - 1].cells.length;
            var i = table.rows.length;
            // Insere uma linha no fim da tabela.
            var newRow = table.insertRow(numOfRows);

            // Faz um loop para criar as colunas
            for (var j = 0; j < numOfCols; j++) {
                newCell = newRow.insertCell(j);
                if (j == 0) {
                    if (cpf != null) {
                        newCell.innerHTML = "<input name='cpf' type='text' class='input-medium nuCpf' id='nuCpf" + i + "' onchange='verificaCPF(this)' required value='" + cpf + "' /><input type='hidden' id='id" + i + "' value='" + id + "' />";
                    } else {
                        newCell.innerHTML = "<input name='cpf' type='text' class='input-medium nuCpf' id='nuCpf" + i + "' onblur='verificaCPF(this)' required /><input type='hidden' id='id" + i + "' value='' />";
                    }
                }
                if (j == 1) {
                    if (nome != null) {
                        newCell.innerHTML = "<input name='nome" + i + "' type='text' id='nome" + i + "' onkeyup='maiuscula(this," + i + ")' onchange='updateEmail(this)' value='" + nome + "' />";
                    } else {
                        newCell.innerHTML = "<input name='nome" + i + "' onkeyup='maiuscula(this," + i + ")' type='text' required id='nome" + i + "' />";
                    }
                }
                if (j == 2) {
                    if (email != null) {
                        newCell.innerHTML = "<input name='email" + i + "' type='email' id='email" + i + "' onkeyup='updateEmail(this)' required class='input-medium' value='" + email + "' />";
                    } else {
                        newCell.innerHTML = "<input name='email" + i + "' type='email' id='email" + i + "' onkeyup='validarCampos(" + i + ")' required class='input-medium' />";
                    }
                }
                if (j == 3) {
                    if (ck30 != null && ck30 == 1) {
                        newCell.innerHTML = "<input type='checkbox' id='cb30" + i + "' value='1' onchange='updateEmail(this)' checked />";
                    } else {
                        if (ck30 != null && ck30 != 1) {
                            newCell.innerHTML = "<input type='checkbox' id='cb30" + i + "' value='1' onchange='updateEmail(this)' />";
                        } else {
                            newCell.innerHTML = "<input onchange='' type='checkbox' id='cb30" + i + "' value='1' />";
                        }
                    }
                }
                if (j == 4) {
                    if (ck60 != null && ck60 == 1) {
                        newCell.innerHTML = "<input type='checkbox' id='cb60" + i + "' onchange='updateEmail(this)' value='1' checked />";
                    } else {
                        if (ck60 != null && ck60 != 1) {
                            newCell.innerHTML = "<input type='checkbox' id='cb60" + i + "' value='1' onchange='updateEmail(this)' />";
                        } else {
                            newCell.innerHTML = "<input onchange='' type='checkbox' id='cb60" + i + "' value='1' />";
                        }
                    }
                }
                if (j == 5) {
                    if (ck90 != null && ck90 == 1) {
                        newCell.innerHTML = "<input type='checkbox' id='cb90" + i + "' onchange='updateEmail(this)' value='1' checked />";
                    } else {
                        if (ck90 != null && ck90 != 1) {
                            newCell.innerHTML = "<input type='checkbox' id='cb90" + i + "' value='1' onchange='updateEmail(this)' />";
                        } else {
                            newCell.innerHTML = "<input onchange='' type='checkbox' id='cb90" + i + "' value='1' />";
                        }
                    }
                }
                if (j == 6) {
                    if (ck120 != null && ck120 == 1) {
                        newCell.innerHTML = "<input type='checkbox' id='cb120" + i + "' onchange='updateEmail(this)' value='1' checked />";
                    } else {
                        if (ck120 != null && ck120 != 1) {
                            newCell.innerHTML = "<input type='checkbox' id='cb120" + i + "' value='1' onchange='updateEmail(this)' />";
                        } else {
                            newCell.innerHTML = "<input onchange='' type='checkbox' id='cb120" + i + "' value='1' />";
                        }
                    }
                }
                if (j == 7) {
                    if (cpf != null && nome != null && email != null) {
                        newCell.innerHTML = "<button class='btn btn-small btn-success center' disabled='disabled' type='button' id='btnSave" + i + "'><i id='iconSave" + i + "' class='icon-ok'></i></button>";
                    } else {
                        newCell.innerHTML = "<button class='btn btn-small btn-primary center' type='button' onclick='insertEmail(this)' id='btnSave" + i + "'><i id='iconSave" + i + "' class='icon-save'></i></button>";
                    }
                }
                if (j == 8) {
                    if (cpf != null || nome != null || email != null) {
                        newCell.innerHTML = "<button class='btn btn-small btn-danger center' type='button' onclick='removeEmail(this)' ><i class='icon-remove'></i></button>";
                    } else {
                        newCell.innerHTML = "<button class='btn btn-small btn-danger center' type='button' onclick='removeLine(this)' ><i class='icon-remove'></i></button>";
                    }
                }
                $('.nuCpf').mask('999.999.999-99');
                var btn = "#btnSave" + i;
                if (cpf != null && nome != null && email != null) {
                    $(btn).show();
                } else {
                    $(btn).hide();
                }
            }
        }

        function maiuscula(z, i) {
            v = z.value.toUpperCase();
            z.value = v;

            validarCampos(i);
        }

        function alertJaCadastrado() {
            alert("Usuário habilitado para o recebimento de alertas!");
        }

        function updateEmail(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;

            if (validarCampos(i, true) == true) {
                table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-primary';
                table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-save';
                var btn = "btnSave" + i;
                document.getElementById(btn).setAttribute("onclick", "setUpdatedEmail(this)");
            }
        }

        function resetField(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            var btn = "#btnSave" + i;
            $(btn).hide();
        }

        function removeEmail(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            table.rows[i].cells[3].childNodes[0].checked = false;
            table.rows[i].cells[4].childNodes[0].checked = false;
            table.rows[i].cells[5].childNodes[0].checked = false;
            table.rows[i].cells[6].childNodes[0].checked = false;
            deleteEmail(r);
            // setUpdatedEmail(r);
//        removeLine(r);
        }

        function removeLine(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            //console.log(i);
            table.deleteRow(i);
        }

        function insertEmail(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            var btn = "#nuCpf" + i;

            // console.log('r: ', r);return;
            if (validarCheckboxes(i) == false) {
                alert('Selecione pelo menos uma das opções de prazo.');
            } else {
                var cpf = $(btn).val();
                cpf = cpf.replace(/[^\d]+/g, '');

                var nme = "#nome" + i;
                var nome = $(nme).val();
                var eml = "#email" + i;
                var email = $(eml).val();

                // Validação dos checkboxes para 30, 60, 90 e 120 dias
                var ck30 = document.getElementById("cb30" + i).checked;
                if (ck30) {
                    ck30 = 1;
                } else {
                    ck30 = 0;
                }

                var ck60 = document.getElementById("cb60" + i).checked;
                if (ck60) {
                    ck60 = 1;
                } else {
                    ck60 = 0;
                }

                var ck90 = document.getElementById("cb90" + i).checked;
                if (ck90) {
                    ck90 = 1;
                } else {
                    ck90 = 0;
                }

                var ck120 = document.getElementById("cb120" + i).checked;
                if (ck120) {
                    ck120 = 1;
                } else {
                    ck120 = 0;
                }

                $.getJSON("<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'addEmail'))?>" + "/" + cpf + "/" + nome + "/" + email + "/" + ck30 + "/" + ck60 + "/" + ck90 + "/" + ck120, function (data) {

                    if (data.msg == 'true') {
                        console.log("Entrou no certo");
                        var table = document.getElementById("tableAddEmails");
                        var i = r.parentNode.parentNode.rowIndex;

                        table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-success';
                        table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-ok';

                        var btn = "btnSave" + i;
                        var idtd = "#id" + i;
                        document.getElementById(btn).setAttribute("onclick", "setUpdatedEmail(this)");
                        document.getElementById(btn).setAttribute("disabled", "true");
                        $(idtd).val(data.conteudo[0].GrupoAuxiliar.co_usuario);
                        table.rows[i].cells[2].childNodes[0].setAttribute("onkeyup", "updateEmail(this)");
                        table.rows[i].cells[8].childNodes[0].setAttribute("onclick", "removeEmail(this)");

                    } else {
                        console.log("Entrou aqui insert");
                        alert('Não foi possível inserir o usuário! Verifique se os campos foram preenchidos corretamente!');
                        var table = document.getElementById("tableAddEmails");
                        var i = r.parentNode.parentNode.rowIndex;
                        table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                        table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';

                    }
                });
            }
        }


        function deleteEmail(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            var idtd = "#id" + i;
            var id = $(idtd).val();

            $.ajax({
                url: 'grupo_auxiliares/deleteemail/' + id,
                type: 'POST',
                data: {},

                success: function (data) {
                    if (data.trim() == "true") {
                        removeLine(r);
                        $("#msg-success").hide();
                        $("#msg-warning").hide();
                        $("#msg-error").show();
                    } else {
                        alert('Algum erro ocorreu');
                    }
                },
                error: function (xhr, error) {
                    alert('Algum erro ocorreu');
                }
            });
        }

        function setUpdatedEmail(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            var btn = "#nuCpf" + i;

            if (validarCheckboxes(i) == false) {
                alert('Selecione pelo menos uma das opções de prazo.');
            } else {

                var cpf = $(btn).val();
                cpf = cpf.replace(/[^\d]+/g, '');

                var nme = "#nome" + i;
                var nome = $(nme).val();
                var eml = "#email" + i;
                var email = $(eml).val();
                var idtd = "#id" + i;
                var id = $(idtd).val();

                // Validação dos checkboxes para 30, 60, 90 e 120 dias
                var ck30 = document.getElementById("cb30" + i).checked;
                if (ck30) {
                    ck30 = 1;
                } else {
                    ck30 = 0;
                }

                var ck60 = document.getElementById("cb60" + i).checked;
                if (ck60) {
                    ck60 = 1;
                } else {
                    ck60 = 0;
                }

                var ck90 = document.getElementById("cb90" + i).checked;
                if (ck90) {
                    ck90 = 1;
                } else {
                    ck90 = 0;
                }

                var ck120 = document.getElementById("cb120" + i).checked;
                if (ck120) {
                    ck120 = 1;
                } else {
                    ck120 = 0;
                }

                $.getJSON("<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'editEmail'))?>" + "/" + cpf + "/" + nome + "/" + email + "/" + ck30 + "/" + ck60 + "/" + ck90 + "/" + ck120 + "/" + id, function (data) {

                    if (data.msg == 'true') {
                        console.log("Entrou no certo - updateEmail");
                        var table = document.getElementById("tableAddEmails");
                        var i = r.parentNode.parentNode.rowIndex;
                        //console.log(table.rows[i].cells[7].childNodes);
                        $(r).removeClass('btn-primary').addClass('btn-success');
                        $(r).find('i').removeClass('icon-save').addClass('icon-ok');

                        if (ck30 == 0 && ck60 == 0 && ck90 == 0 && ck120 == 0) {
                        } else {
                            table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-success';
                            table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-ok';
                            var btn = "btnSave" + i;
                            document.getElementById(btn).setAttribute("disabled", "true");
                        }
                    } else {
                        console.log("Entrou aqui update");
                        var table = document.getElementById("tableAddEmails");
                        var i = r.parentNode.parentNode.rowIndex;
                        //console.log(table.rows[i].cells[7].childNodes);
                        if (ck30 == 0 && ck60 == 0 && ck90 == 0 && ck120 == 0) {
                        } else {
                            table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                            table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';

                            alert('Não foi possível atualizar! Verifique se os campos foram preenchidos corretamente!');

                        }
                    }
                });
            }
        }


        function validarCampos(i, isUpdate) {
            if (isUpdate == 'undefined') {
                isUpdate = null;
            }
            var table = document.getElementById("tableAddEmails");
            var btn = "btnSave" + i;

            document.getElementById(btn).removeAttribute("disabled");

            var cpf = table.rows[i].cells[0].childNodes[0].value;
            var nome = table.rows[i].cells[1].childNodes[0].value;
            var email = table.rows[i].cells[2].childNodes[0].value;

            if (cpf != '' && nome != '' && email != '') {
                if (isUpdate) {
                    return true;
                } else {
                    displayBtn(i);
                }
            } else {
                table.rows[i].cells[7].childNodes[0].removeAttribute("style");
                table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                table.rows[i].cells[7].childNodes[0].tooltip = 'CPF inválido';
                table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';

                document.getElementById(btn).setAttribute("disabled", "true");
            }
        }

        function validarCheckboxes(i) {
            // Validação dos checkboxes para 30, 60, 90 e 120 dias
            var vazio = [];

            var ck30 = document.getElementById("cb30" + i).checked;
            if (!ck30) {
                vazio.push('1');
            }

            var ck60 = document.getElementById("cb60" + i).checked;
            if (!ck60) {
                vazio.push('2');
            }

            var ck90 = document.getElementById("cb90" + i).checked;
            if (!ck90) {
                vazio.push('3');
            }

            var ck120 = document.getElementById("cb120" + i).checked;
            if (!ck120) {
                vazio.push('4');
            }

            if (vazio.length == 4) {
                return false;
            } else {
                return true;
            }

        }
        function displayBtn(i) {
            var table = document.getElementById("tableAddEmails");

            table.rows[i].cells[7].childNodes[0].removeAttribute("style");
            table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-primary';
            table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-save';

        }

    </script>

<?php //echo $this->Html->scriptStart() ?>
    <script>
        $('#nuCpf').mask('999.999.999-99');
        $("#isValidMessage").hide();
        $("#view_novo_auxiliar").hide();
        $("#fechar").click(function () {
            $("#view_novo_auxiliar").hide();
        });
        var valor = false;

        function verificaCPF(r) {
            var table = document.getElementById("tableAddEmails");
            var i = r.parentNode.parentNode.rowIndex;
            var btn = "#nuCpf" + i;

            var cpf = $(btn).val();

            // Regex Milagroso (O mais lindo de todos os tempos)
            cpf = cpf.replace(/[^\d]+/g, '');

            if (cpf.length != 11) {
                console.log("Diferente de 11");
                var table = document.getElementById("tableAddEmails");
                var i = r.parentNode.parentNode.rowIndex;

                table.rows[i].cells[7].childNodes[0].removeAttribute("style");
                table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                table.rows[i].cells[7].childNodes[0].tooltip = 'CPF inválido';
                table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';

                var btn = "btnSave" + i;
                document.getElementById(btn).setAttribute("disabled", "true");

            } else {
                if (cpf == '11111111111' || cpf == '22222222222' || cpf == '33333333333' || cpf == '44444444444' || cpf == '55555555555' || cpf == '66666666666' || cpf == '77777777777' || cpf == '88888888888' || cpf == '99999999999') {
                    console.log("CPF inválido");
                    var table = document.getElementById("tableAddEmails");
                    var i = r.parentNode.parentNode.rowIndex;
                    //console.log(table.rows[i].cells[7].childNodes);
                    table.rows[i].cells[7].childNodes[0].removeAttribute("style");
                    table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                    table.rows[i].cells[7].childNodes[0].tooltip = 'CPF inválido';
                    table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';
                    var btn = "btnSave" + i;
                    document.getElementById(btn).setAttribute("disabled", "true");
                } else {
                    $.getJSON("<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'verifyCPF')) ?>" + "/" + cpf, function (data) {

                        if (data.validation == 'true') {

                            var table = document.getElementById("tableAddEmails");
                            var i = r.parentNode.parentNode.rowIndex;
                            validarCampos(i);

                        } else {
                            if (data.validation == 'existCPF') {
                                var table = document.getElementById("tableAddEmails");
                                var i = r.parentNode.parentNode.rowIndex;
                                if (data.ativo == 1) {
                                    table.rows[i].cells[0].childNodes[0].value = '';

                                    alert(data.message);
                                    return;
                                } else {
                                    table.rows[i].cells[7].childNodes[0].removeAttribute("style");
                                    table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-primary';
                                    table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-save';
                                    var btn = "btnSave" + i;
                                    document.getElementById(btn).setAttribute("onclick", "updateEmail(this)");

                                    table.rows[i].cells[0].childNodes[1].value = data.id;
                                    table.rows[i].cells[1].childNodes[0].value = data.nome;
                                    table.rows[i].cells[2].childNodes[0].value = data.email;

                                    if (data.ck30 == 1) {
                                        table.rows[i].cells[3].childNodes[0].checked = true;
                                    }
                                    if (data.ck60 == 1) {
                                        table.rows[i].cells[4].childNodes[0].checked = true;
                                    }
                                    if (data.ck90 == 1) {
                                        table.rows[i].cells[5].childNodes[0].checked = true;
                                    }
                                    if (data.ck120 == 1) {
                                        table.rows[i].cells[6].childNodes[0].checked = true;
                                    }

                                    table.rows[i].cells[0].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[1].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[2].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[3].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[4].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[5].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };
                                    table.rows[i].cells[6].childNodes[0].onchange = function () {
                                        updateEmail(r)
                                    };

                                    table.rows[i].cells[8].childNodes[0].onclick = function () {
                                        table.rows[i].cells[3].childNodes[0].checked = false;
                                        table.rows[i].cells[4].childNodes[0].checked = false;
                                        table.rows[i].cells[5].childNodes[0].checked = false;
                                        table.rows[i].cells[6].childNodes[0].checked = false;
                                        setUpdatedEmail(r);
                                        removeLine(r);
                                    };
                                }
                            } else {
                                var table = document.getElementById("tableAddEmails");
                                var i = r.parentNode.parentNode.rowIndex;

                                table.rows[i].cells[7].childNodes[0].removeAttribute("style");
                                table.rows[i].cells[7].childNodes[0].className = 'btn btn-small btn-warning';
                                table.rows[i].cells[7].childNodes[0].tooltip = 'CPF inválido';
                                table.rows[i].cells[7].childNodes[0].childNodes[0].className = 'icon-warning-sign';

                                var btn = "btnSave" + i;
                                document.getElementById(btn).setAttribute("disabled", "true");

                                alert('CPF inválido!');
                            }
                        }
                    })
                }
            }
        }
    </script>

<?php //echo $this->Html->scriptEnd() ?>