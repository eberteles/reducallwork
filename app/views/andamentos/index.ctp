
<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped">
	<tr>
		<th width="10%"><?php __('Data');?></th>
		<th width="10%"><?php __('Tempo');?></th>
		<th width="10%"><?php __('Usuário');?></th>
		<th width="20%"><?php __('Fase');?></th>
		<th width="20%"><?php __('Observações');?></th>
	</tr>
	<?php 
            $i = 0;
            foreach ($andamentos as $andamento):
	?>
	<tr>
		<td><?php echo $andamento['Andamento']['dt_andamento']; ?>&nbsp;</td>
		<td><?php
                        if($andamento['Andamento']['nu_sequencia'] > 0 || $andamento['Andamento']['co_setor'] > 0) {
                            echo $this->Print->getPrazoDecorrido($andamento['Andamento']['dt_andamento'], $andamento['Andamento']['dt_fim']);
                            $faseAtual  = null;
                            if($andamento['Fluxo']['nu_sequencia'] > 0) {
                                $faseAtual  = $andamento['Fluxo']['Fase'];
                            }
                            if($andamento['Fase']['co_fase'] > 0) {
                                $faseAtual  = $andamento['Fase'];
                            }
                            if($faseAtual != null) {
                                echo $this->Print->checkPrazoDecorridoAndamento($andamento['Andamento']['dt_andamento'], $andamento['Andamento']['dt_fim'], $faseAtual['mm_duracao_fase']);
                            }
                        } else {
                            echo '--';
                        }
                    ?>&nbsp;
                </td>
                <td><?php 
                        if( isset($andamento['Andamento']['co_fornecedor']) && $andamento['Andamento']['co_fornecedor'] > 0 ) { 
                            echo __('Fornecedor') . ' - ' . $andamento['Fornecedor']['no_razao_social']; 
                        } else {
                            echo $andamento['Usuario']['ds_nome']; 
                        }
                    ?>&nbsp;</td>
		<td><?php 
                    if(Configure::read('App.config.resource.certificadoDigital') && $andamento['Andamento']['assinatura_id'] > 0) {?>
                            <i class="icon-certificate green bigger-130" title="" data-original-title="Fase Assinada" data-rel="popover" data-trigger="hover" data-placement="bottom" 
                                  data-content="<b>Data</b>: <?php echo $andamento['Assinatura']['created']; ?><BR>
                                                <b>Usuário</b>: <?php echo $andamento['Assinatura']['Usuario']['no_usuario']; ?><BR>
                                                <b>Assinado por</b>: <?php echo $andamento['Assinatura']['usuario_token']; ?><BR>" data-html="true">
                            </i>&nbsp;&nbsp;<?php
                    }
                    if( isset($andamento['Andamento']['co_fase']) && $andamento['Andamento']['co_fase'] > 0 ) { 
                        echo '<i class="icon-share-alt red alert-tooltip" title="O Fluxo foi desviado para esta Fase."></i> ' . $fases[$andamento['Andamento']['co_fase']];     
                    }
                    else {
                        if( $andamento['Andamento']['nu_sequencia'] > 0 && $andamento['Fluxo']['co_fase'] > 0 ) { 
                            echo $fases[$andamento['Fluxo']['co_fase']];
                        } else {
                            echo '--';
                        }
                    }
                    ?>&nbsp;
                </td>
		<td><?php 
                    if($andamento['Andamento']['ds_justificativa'] != '') {
                        echo '<b>Justificativa:</b> ' . $andamento['Andamento']['ds_justificativa'] . '<BR>';
                    }
                    echo $andamento['Andamento']['ds_andamento']; ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
</table>
                     
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>