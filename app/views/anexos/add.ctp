<?php
$usuario = $this->Session->read ('usuario');
echo $this->Html->script( 'inicia-datetimepicker' );
?>
    <div class="aditivos form">
        <?php echo $this->Form->create('Anexo', array('type' => 'file', 'url' => array('controller' => 'anexos', 'action' => 'add', $coContrato,$modulo,$idModulo)));?>

        <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Anexo - <b>Campos com * são obrigatórios.</p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'index', $coContrato, $modulo, $idModulo)); ?>" class="btn btn-small btn-primary" title="Listar Anexos">Listagem</a>
            </div>
        </div>

        <div class="row-fluid">

            <div class="span12 ">
                <div class="widget-header widget-header-small"><h4>Novo Anexo</h4></div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span4">
                                <?php
                                echo $this->Form->hidden('co_anexo');
                                if($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
                                    echo $this->Form->hidden('co_ata', array('value' => $coContrato));
                                } elseif($modulo == "evento") {
                                    echo $this->Form->hidden('co_evento', array('value' => $coContrato));
                                } else {
                                    echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                                }

                                if($modulo == "atividade" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_atividade', array('value' => $idModulo));
                                }
                                if($modulo == "fiscal" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_contrato_fiscal', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '7'));
                                }
                                if($modulo == "aditivo" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_aditivo', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '17'));
                                }
                                if($modulo == "apostilamento" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_apostilamento', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '10'));
                                }
                                if($modulo == "garantia" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_garantia', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '11'));
                                }
                                if($modulo == "garantia_suporte" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_garantia_suporte', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '19'));
                                }
                                if($modulo == "produto" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_produto', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '20'));
                                }
                                if($modulo == "oficio" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_oficio', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '21'));
                                }
                                if($modulo == "empenho" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_empenho', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '12'));
                                }
                                if($modulo == "penalidade" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_penalidade', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '13'));
                                }
                                if($modulo == "nota" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_nota', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '2'));
                                }
                                if(($modulo == "pendencia" || $modulo == "pendencia_ata" ) && $idModulo > 0) {
                                    echo $this->Form->hidden('co_pendencia', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '14'));
                                }
                                if($modulo == "pagamento" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_pagamento', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '15'));
                                }
                                if(($modulo == "historico" || $modulo == "historico_ata") && $idModulo > 0) {
                                    echo $this->Form->hidden('co_historico', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '16'));
                                }
                                if($modulo == "ordem_de_fornecimento" && $idModulo > 0) {
                                    echo $this->Form->hidden('co_ordem_fornecimento_servico', array('value' => $idModulo));
                                    echo $this->Form->hidden('tp_documento', array('value' => '18'));
                                }

                                echo $this->Form->input('co_anexo_pasta', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Pasta', true), 'options' => $this->Imprimir->getArrayAnexosPastas($anexos_pastas), 'escape' => false,
                                    'after' => $this->Print->getBtnEditCombo('Nova Pasta ', 'AbrirNovaPasta', '#view_nova_pasta', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/add')) ) );

                                if($modulo == "" || $modulo == "padrao") {
                                    echo $this->Form->input('tp_documento', array('label' => 'Tipo de Documento', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $tpAnexo));
                                }

                                echo $this->Form->input('dt_anexo', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Data do Anexo', 'type'=>'text'));

                                echo $this->Form->input('ds_anexo', array('label' => 'Descrição', 'size' => '100'));
                                echo $this->Form->input('conteudo', array('type' => 'file', 'label' => 'Anexo') );
                                ?>
                                <div id="erro_conteudo" class="error-message">Campo anexo em branco</div>
                            </div>
                            <div class="span4" id="campos_atesto" style="display: none;">
                                Atestado? <br>
                                <input name="data[Anexo][ic_atesto]" type="radio" id="idAtestoSim" value="1"/> Sim &nbsp;&nbsp;&nbsp;
                                <input name="data[Anexo][ic_atesto]" type="radio" id="idAtestoNao" value="0" checked="checked" /> Não<br><br>
                                <?php echo $this->Form->input('ds_observacao', array('label'=>'Observações','type' => 'texarea', 'cols'=>'45', 'rows'=>'4',
                                    'onKeyup'=>'$(this).limit("255","#charsLeft")',
                                    'after' => '<br><span id="charsLeft"></span> caracteres restantes.')); echo '<br />'; ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" id="salvarAnexo" title="Salvar Anexo"> Salvar</button>
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm"> Limpar</button>
                <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
            </div>
        </div>

    </div>

    <div id="view_nova_pasta" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova Pasta</h3>
        </div>
        <div class="modal-body-iframe" id="add_pasta">
        </div>
    </div>

    <div id="aguarde" class="modal fade" data-backdrop="static" style="display: none;background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
        <br> <br> <br> <br> <br>&nbsp;<br>
        <center>Aguarde...</center>
        <br>
    </div>
    <script type="text/javascript">

        $('#erro_conteudo').hide();

        $('#LimparForm').click(function () {

            $('#AnexoCoAnexoPasta').val('');
            $('#AnexoDtAnexo').val('');
            $('#AnexoDsAnexo').val('');

        });


    </script>
<?php echo $this->Html->scriptStart() ?>

    $('#salvarAnexo').click(function(){
    if($('#AnexoConteudo').val() == ''){
    $('#erro_conteudo').show();
    return false;
    }

    $('#erro_conteudo').hide();
    $('#aguarde').modal();
    });

    $("#AnexoTpDocumento").change(function() {
    var id_selecionado  = $(this).val();
    if(id_selecionado == 1){
    $('#campos_atesto').attr('style', "display: ;");
    } else {
    $('#campos_atesto').attr('style', "display: none;");
    }
    });

    $('#AnexoConteudo').ace_file_input({
    no_file:'Nenhum arquivo selecionado ...',
    btn_choose:'Selecionar',
    btn_change:'Alterar',
    droppable:false,
    thumbnail:false, //| true | large,
    //whitelist:'gif|png|jpg|jpeg',
    //blacklist:'exe|php'
    });


    $('a.remove').on('click', function() {
    $('#AnexoConteudo').ace_file_input('reset_input');
    });


    $("#AbrirNovaPasta").bind('click', function(e) {
    var url_pt = "<?php echo $this->Html->url(array('controller' => 'anexos_pastas', 'action' => 'iframe_add', $coContrato, $modulo, 1)); ?>";
    $.ajax({
    type:"POST",
    url:url_pt,
    data:{
    },
    success:function(result){
    $('#add_pasta').html("");
    $('#add_pasta').append(result);
    }
    })
    });

    function atzComboPasta(co_anexo_pasta) {
    $.getJSON("<?php echo $this->Html->url(array('controller' => 'anexos_pastas', 'action' => 'listar') )?>", null, function(data){
    var options = '<option value="">Selecione..</option>';
    $.each(data.anexos_pastas, function(key, value) {
    options += '<option value="' + value.co_anexo_pasta + '">' + value.ds_anexo_pasta.str_replace('\\', '') + '</option>';
    });
    $("select#AnexoCoAnexoPasta").html(options);
    $("#AnexoCoAnexoPasta option[value=" + co_anexo_pasta + "]").attr("selected", true);
    });
    }
<?php echo $this->Html->scriptEnd() ?>