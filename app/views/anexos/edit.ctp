<?php 
    $usuario = $this->Session->read ('usuario');
?>
<div class="aditivos form">
<?php echo $this->Form->create('Anexo', array('url' => array('controller' => 'anexos', 'action' => 'edit',$id,$coContrato,$modulo,$idModulo)));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar um Anexo - <b>Campos com * são obrigatórios.</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'index', $coContrato, $modulo, $idModulo)); ?>" class="btn btn-small btn-primary" title="Listar Anexos">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
            <div class="span12 ">
              <div class="widget-header widget-header-small"><h4>Alterar Anexo</h4></div>
             
            <div class="widget-body">
              <div class="widget-main">
              	     <dl class="dl-horizontal">
                    <?php
                        echo $this->Form->hidden('co_anexo');
                        if($modulo == "ata") {
                            echo $this->Form->hidden('co_ata', array('value' => $coContrato));
                        } elseif($modulo == "evento") {
                            echo $this->Form->hidden('co_evento', array('value' => $coContrato));
                        } else {
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                        }
                        echo $this->Form->hidden('co_atividade');
                        echo $this->Form->hidden('dt_anexo');
                        echo $this->Form->hidden('ds_extensao');
                        
                        echo $this->Form->input('co_anexo_pasta', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Pasta', true), 'options' => $this->Imprimir->getArrayAnexosPastas($anexos_pastas), 'escape' => false, 
                                'after' => $this->Print->getBtnEditCombo('Nova Pasta ', 'AbrirNovaPasta', '#view_nova_pasta', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/add')) ) ); 

                        echo $this->Form->input('ds_anexo', array('label' => 'Descrição'));
                    ?>
	             </dl>
	           </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Anexo"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar" > Limpar</button> 
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

    <div id="view_nova_pasta" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova Pasta</h3>
        </div>
        <div class="modal-body-iframe" id="add_pasta">
        </div>
    </div>

<?php echo $this->Html->scriptStart() ?>
        
    $("#AbrirNovaPasta").bind('click', function(e) {
        var url_pt = "<?php echo $this->Html->url(array('controller' => 'anexos_pastas', 'action' => 'iframe_add', $coContrato, $modulo, 1)); ?>";
        $.ajax({
            type:"POST",
            url:url_pt,
            data:{
                },
                success:function(result){
                    $('#add_pasta').html("");
                    $('#add_pasta').append(result);
                }
            })
    });
    
    function atzComboPasta(co_anexo_pasta) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'anexos_pastas', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data.anexos_pastas, function(key, value) {
                    options += '<option value="' + value.co_anexo_pasta + '">' + value.ds_anexo_pasta + '</option>';
               });
               $("select#AnexoCoAnexoPasta").html(options);
               $("#AnexoCoAnexoPasta option[value=" + co_anexo_pasta + "]").attr("selected", true);
        });
    }
        
<?php echo $this->Html->scriptEnd() ?>