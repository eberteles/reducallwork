<?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' );
    echo $this->Form->create('Anexo');
?>


    <script>

   function limpar()
   {
        $("select option").removeAttr('selected').filter(":contains(Selecione..)").attr('selected', true);
        $('input').attr('value', '');
   }

    </script>

    <div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Suporte Documental'); ?></h1></div>

            <div class="row-fluid">
   		<div class="span3">
    		<div class="controls">
    			<?php echo $this->Form->input('palavra_chave', array('class' => 'input-xlarge','label' => __('Palavra Chave', true) )); ?>
   		</div>
                </div>
   		<div class="span2">
    		<div class="controls">
    			<?php
                            $extensoes  = array(
                                '1' => 'PDF', '2' => 'Documento (TXT, DOC, DOCX, ODT)', '3' => 'Planilha (XLS, XLSX, ODS)', '4'=> 'Apresentação (PPT, PPTX, ODP)',
                                '5' => 'Página WEB (HTM, HTML)', '6' => 'Imagem (JPG, PNG, TIF, SVG, ODG)');
                            echo $this->Form->input('tp_extensao', array('class' => 'input-large', 'type' => 'select', 'label' => __('Extensão do Arquivo', true), 'empty' => 'Selecione..', 'options' => $extensoes )); ?>
   		</div>
                </div>
		<div class="span2">
    		<div class="controls">
    			<?php echo $this->Form->input('tipo_documento', array('class' => 'input-large', 'type' => 'select', 'label' => __('Tipo de Documento', true), 'empty' => 'Selecione..', 'options' => $tpAnexo )); ?>
    		</div>
                </div>
                <div class="span2">
                    <div class="controls">
                        <table>
                            <tr>
                                <td>
                        <?php
                            echo $this->Form->input('dt_inicio', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Data Documento - De:', 'type'=>'text'));
                        ?>
                                </td>
                                <td>&nbsp;-&nbsp;</td>
                                <td>
                        <?php
                            echo $this->Form->input('dt_fim', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Até:', 'type'=>'text'));
                        ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        <div class="form-actions">
             <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
                <button rel="tooltip" type="reset" onclick="limpar();" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
             </div>
        </div>

    <?php
    $in = 'in';
    $i  = 0;
    foreach ($anexos as $anexo):
        if($i > 0) {$in = '';}
        $i++;
    ?>
<div id="accordion<?php echo $anexo['Anexo']['co_anexo'];?>" class="accordion">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a href="#collapseIdent<?php echo $anexo['Anexo']['co_anexo'];?>" data-parent="#accordion<?php echo $anexo['Anexo']['co_anexo'];?>" data-toggle="collapse" class="accordion-toggle">
                <img alt="<?php echo $anexo['Anexo']['ds_extensao'];?>" src="<?php echo $this->base; ?>/img/extensao/<?php echo $anexo['Anexo']['ds_extensao'];?>.png"> &nbsp;
                    <?php echo $anexo['Anexo']['ds_anexo'];?> 
                    <?php if($anexo['Anexo']['dt_anexo'] != ''){ echo ' - ' . $anexo['Anexo']['dt_anexo'];}?>
                    - <?php echo $tpAnexo[$anexo['Anexo']['tp_documento']];?>
            </a>
        </div>
        
        <div class="accordion-body <?php echo $in; ?> collapse" id="collapseIdent<?php echo $anexo['Anexo']['co_anexo'];?>">
            <div class="accordion-inner">
                
                <div class="row-fluid">

                    <div class="span1">
                        <?php echo $this->Html->image('/img/extensao/abrir.png', array(
                            'class'=> "alert-tooltip", 'title'=>"Abrir o Documento", 'url'=>"#", 
                            'onClick'=>"javascript:imprimir('" . $this->Html->url(array('controller' => 'anexos', 'action' => 'lerArquivo', $anexo['Anexo']['co_anexo'], 1)) . "');")); ?>
                    </div>
                    <div class="span11">
                        <?php
                        if(isset($anexo['AnexoIndexacao'])) {
                            $count  = 0;
                            foreach ($anexo['AnexoIndexacao'] as $indexacao):
                                if($count > 0) { echo '<hr>'; }
                                echo '<b>Página ' . $indexacao['nu_pagina'] . '</b><BR>';
                                $texto  = $indexacao['ds_conteudo'];
                                $inicio = stripos($indexacao['ds_conteudo'], $this->data['Anexo']['palavra_chave']);
                                if($inicio > 100 ) {$inicio -= 100;} else { $inicio = 0; }
                                if(stripos($indexacao['ds_conteudo'], $this->data['Anexo']['palavra_chave']) > 0) {
                                    $texto  = substr($indexacao['ds_conteudo'], $inicio, 300);
                                }
                                echo nl2br( 
                                        str_ireplace($this->data['Anexo']['palavra_chave'], '<b>' . $this->data['Anexo']['palavra_chave'] . '</b>', $texto)
                                     );
                                $count++;
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
    <?php endforeach; ?>

</div>

<div id="view_dominio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="tituloAbaDominio">Visualizar Documento</h3>
    </div>
    <div class="modal-body maior" id="impressao">OBS: Para alguns tipos de documentos o navegador fará o download do Documento.<BR>Favor verificar a área de download do mesmo.</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

    });
    
    function imprimir(url_abrir) {
        $('#view_dominio').modal();
        $('#impressao').html("");
        $.ajax({
            type:"POST",
            url:"<?php echo $this->base; ?>/anexos/iframe_abrir/",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data:{
                "data[url]": url_abrir
                },
                success:function(result){
                    $('#impressao').append(result);
                }
            })
    }
</script>
