<?php echo $this->Form->create('AnexoPasta', array('url' => "/anexos_pastas/add/$idModulo/$modulo/$modal")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>
    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Pasta'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                <?php
                    echo $this->Form->hidden('co_anexo_pasta');
                    if ($modulo == "ata" || $modulo == "pendencia_ata" || $modulo == "historico_ata") {
                        echo $this->Form->hidden('co_ata', array('value'=>$idModulo));
                    } else {
                        echo $this->Form->hidden('co_contrato', array('value'=>$idModulo));
                    }
                    echo $this->Form->input('parent_id', array('class' => 'chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Vinculado a', true), 'options' => $imprimir->getArrayAnexosPastas($anexos_pastas), 'escape' => false));
                    echo $this->Form->input('ds_anexo_pasta', array('class' => 'input-xlarge', 'label' => 'Nome'));
                ?>
                </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
