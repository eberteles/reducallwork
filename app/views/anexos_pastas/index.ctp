
<div class="row-fluid">
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'anexos_pastas', 'action' => 'add', $idModulo, $modulo)); ?>" class="btn btn-small btn-primary" title="Nova Pasta"><i class="icon-plus icon-white"></i> Nova Pasta</a>
          </div>
        </div>

    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tbAnexoPasta">
	<tr>
		<th><?php __('Nome');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php

	foreach ($anexos_pastas as $anexo_pasta):
            $imprimir->listaTabelaAnexoPasta($anexo_pasta, null, $idModulo, $modulo);
        endforeach;

        ?>
    </table>
<p>
    <?php
//echo $this->Paginator->counter(array(
//	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
//));
?></p>

<!--<div class="pagination">
    <ul>
        < ?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        < ?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> < ?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>-->
</div>

<?php echo $this->Html->scriptStart() ?>

    $(document).ready(function() {
        $('.tree').treegrid({
            'initialState': 'collapsed',
            'saveState': true,
            treeColumn: 0
        });
    })

<?php echo $this->Html->scriptEnd() ?>
