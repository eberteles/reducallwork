<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<div class="apostilamentos form">
    <?php
    echo $this->Form->create('Apostilamento', array('url' => "/apostilamentos/add/$coContrato/$tp_apostilamento"));
    ?>

    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Apostilamento - <b>Campos com *
                são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'apostilamentos', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar Apostilamentos">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span12 ">
            <div class="widget-header widget-header-small"><h4>
                    Novo <?php echo $this->Print->tpApostilamento($tp_apostilamento); ?></h4></div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->hidden('co_apostilamento');
                                echo $this->Form->hidden('co_contrato', array('value' => $coContrato, "class" => "numbersOnly"));
                                echo $this->Form->hidden('vl_global', array('value' => $vl_global));
                                echo $this->Form->hidden('tp_apostilamento', array('value' => $tp_apostilamento));
                                echo $this->Form->input('no_apostilamento', array('label' => 'Descrição'));
                                if ($tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR ||
                                    $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO ||
                                    $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_SUPRESSAO
                                ) {
                                    ?>
                                    <table style="table-layout:auto !important">
                                        <tr>
                                            <td>
                                                <?php
                                                echo $this->Form->input('vl_apostilamento', array('label' => 'Valor Apostilamento (R$)', 'class' => 'input-xsmall'));
                                                ?>
                                            </td>
                                            <td stype=""><b> ou </b>&nbsp;&nbsp;</td>
                                            <td>
                                                <?php
                                                echo $this->Form->input('pc_apostilamento', array('class' => 'input-mini alert-tooltip', 'title' => 'Preencher caso deseje informar o percentual do Apostilamento.', 'label' => 'Percentual do Apostilamento (%)', 'type' => 'text'));
                                                ?>
                                            </td>
                                        </tr>
                                        <?php if ($tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO) : ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    echo $this->Form->input('dt_fim_vigencia_anterior', array(
                                                        'before' => '<div class="input-append date datetimepicker">',
                                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                                        'class' => 'input-small',
                                                        'label' => 'Data inicial da vigência',
                                                        'type' => 'text'
                                                    ));

                                                    echo $this->Form->input('dt_prazo', array(
                                                        'before' => '<div id="divDtPrazo" class="input-append date datetimepicker">',
                                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                                        'class' => 'input-small', 'label' => 'Data final da vigência', 'type' => 'text'));
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                    <?php

                                } else if (
                                    $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_PRAZO ||
                                    //essa condição não faz muito sentido pois ela ja esta no IF
                                    $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO
                                ) {
                                    echo $this->Form->input('dt_fim_vigencia_anterior', array(
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small',
                                        'label' => 'Data inicial da vigência',
                                        'type' => 'text'
                                    ));

                                    echo $this->Form->input('dt_prazo', array(
                                        'before' => '<div id="divDtPrazo" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small',
                                        'label' => 'Data final da vigência', 'type' => 'text'));

                                } else if ($tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_REPACTUACAO) {
                                    echo $this->Form->input('nu_apostilamento', array('label' => 'Nº Apostilamento'));
                                    echo $this->Form->input('ds_recurso_apostilamento', array('label' => 'Recurso do apostilamento(valor)'));
                                    echo $this->Form->input('vl_mensal', array('label' => 'Valor mensal'));
                                    echo $this->Form->input('vl_anual', array('label' => 'Valor anual'));
                                    echo $this->Form->input('dt_assinatura', array(
                                        'before' => '<div id="divDtPrazo" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data de assinatura', 'type' => 'text'));
                                    echo $this->Form->input(
                                        'co_nota_empenho',
                                        array(
                                            'class' => 'input-xlarge',
                                            'type' => 'select',
                                            'empty' => 'Selecione...',
                                            'label' => 'Notas de empenho',
                                            'options' => $notasEmpenho,
                                        )
                                    );
                                }
                                ?>
                            </dl>
                        </div>
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->input('dt_apostilamento', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data do Apostilamento', 'type' => 'text'));
                                echo $this->Form->input('ds_apostilamento', array('label' => 'Justificativa', 'type' => 'textarea', 'class' => 'input-xlarge',
                                    'onKeyup' => '$(this).limit("1500","#charsLeft")',
                                    'after' => '<br><span id="charsLeft">1500</span> caracteres restantes.'));
                                echo '<br />';
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div id="aguarde" class="modal fade" data-backdrop="static"
         style="display: none;background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
        <br><br><br><br><br>&nbsp;<br>
        <center>Aguarde...</center>
        <br>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" id="salvarApostilamento"
                    title="Salvar Apostilamento"> Salvar
            </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"
                    id="Limpar"> Limpar
            </button>
            <a href="<?php echo $this->Html->url(array('controller' => 'apostilamentos', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small" title="Listar Apostilamentos">Voltar</a>
        </div>
    </div>

</div>
<script>

    $(document).ready(function () {
        $("#ApostilamentoVlMensal").maskMoney({thousands: '.', decimal: ','});
        $("#ApostilamentoVlAnual").maskMoney({thousands: '.', decimal: ','});

        $("#ApostilamentoPcApostilamento").on('focusout', function (e) {
            var percentual = parseFloat($("#ApostilamentoPcApostilamento").val().replace(",", "."));
            if (percentual > 0) {
                $("#ApostilamentoVlApostilamento").unmask();
                $("#ApostilamentoVlApostilamento").val(formatReal((percentual / 100 * $("#ApostilamentoVlGlobal").val()).toFixed(2).replace(".", "")));
            }
        });

        $("#ApostilamentoVlApostilamento").on('focusout', function (e) {
            $("#ApostilamentoPcApostilamento").unmask();
            $("#ApostilamentoPcApostilamento").val("0,00");
        });

        $('#salvarApostilamento').click(function () {
            $('#aguarde').modal();
        });

        <?php if($tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_PRAZO || $tp_apostilamento == Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO) { ?>
        $("#ApostilamentoDtFimVigenciaAnterior").val($("#dt_fim_vigencia_contrato", parent.document).val());
        <?php } ?>

        $("#ApostilamentoVlApostilamento").maskMoney({thousands: '.', decimal: ','});
        $("#ApostilamentoPcApostilamento").maskMoney({thousands: '.', decimal: ','});
    });

</script>