<script type="text/javascript">
    $(function () {
        parent.atualizaTotalizaValores();
        parent.atualizaTotalizaPercentuais();
    });

    $('.alert-tooltip').tooltip();

    $('#tbApostilamento tr td a.v_anexo').click(function () {
        var url_an = "<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $coContrato)); ?>/apostilamento/" + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_an,
            data: {},
            success: function (result) {
                $('#fis_anexo').html("");
                $('#fis_anexo').append(result);
            }
        })
    });

    $(document).ready(function () {
        $('#add_concluir').click(function () {
            $(location).attr('href', '<?php echo $this->Html->url(array('controller' => 'apostilamentos', 'action' => 'add', $coContrato))?>/' + $('#tp_apostilamento').val());
        })
    });
</script>

<?php $usuario = $this->Session->read('usuario'); ?>
<div class="apostilamentos index">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped" id="tbApostilamento">
        <tr>
            <th><?php __('Descrição'); ?></th>
            <th><?php __('Tipo de Apostilamento'); ?></th>
            <th><?php __('Data'); ?></th>
            <th>
                <?php __('Fundamento Legal') ?>
            </th>
            <th><?php __('Valor Apostilamento'); ?></th>
            <th><?php __('Prazo Apostilamento'); ?></th>
            <th><?php __('Justificativa'); ?></th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($apostilamentos as $apostilamento):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $apostilamento['Apostilamento']['no_apostilamento']; ?>&nbsp;</td>
                <td><?php echo $this->Print->tpApostilamento($apostilamento['Apostilamento']['tp_apostilamento']); ?></td>
                <td><?php echo $apostilamento['Apostilamento']['dt_apostilamento']; ?>&nbsp;</td>
                <td><?php echo $apostilamento['Apostilamento']['ds_fundamento_legal'] ? $apostilamento['Apostilamento']['ds_fundamento_legal'] : '-' ?></td>
                <td><?php echo $this->Formatacao->moeda($apostilamento['Apostilamento']['vl_apostilamento']); ?></td>
                <td><?php if (!empty($apostilamento['Apostilamento']['dt_prazo'])) {
                        echo $apostilamento['Apostilamento']['dt_fim_vigencia_anterior'] . ' à ' . $apostilamento['Apostilamento']['dt_prazo'];
                    } ?>&nbsp;</td>
                <td><?php echo $apostilamento['Apostilamento']['ds_apostilamento']; ?>&nbsp;</td>
                <td class="actions"><?php $id = $apostilamento['Apostilamento']['co_apostilamento']; ?>
                    <div class="btn-group acoes">
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i
                                    class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Apostilamento"
                                    style="display: inline-block;"></i></a>
                        <?php echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'apostilamentos/')) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
<?php
if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'apostilamentos/add')): ?>
    <div class="actions">
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">Selecione uma Ação
                <!--span class="required" title="Required">*</span--></p>
            <div class="pull-right btn-group">
                <a href="#add-apostilamento" id="btn-add-apostilamento" data-toggle="modal"
                   class="btn btn-small btn-primary">Adicionar</a>
            </div>
        </div>

    </div>
<?php endif; ?>

<div id="add-apostilamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Adicionar Apostilamento</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="control-group">
                <?php
                echo $this->Form->input(
                    'tp_apostilamento',
                    array(
                        'type' => 'select',
                        'label' => 'Tipo do Novo Apostilamento:',
                        'options' => Apostilamento::getTipos()
                    )
                );
                ?>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-small btn-primary" data-dismiss="modal" id="add_concluir"><i
                    class="icon-ok icon-white"></i> Confirmar
        </button>
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental - Anexar Apostilamento</h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo">
    </div>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

    $('#tbApostilamento tr td a.v_anexo').click(function () {
        var url_an = "<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $coContrato)); ?>/apostilamento/" + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_an,
            data: {},
            success: function (result) {
                $('#fis_anexo').html("");
                $('#fis_anexo').append(result);
            }
        })
    });

    $(document).ready(function () {
        $('#add_concluir').click(function () {
            $(location).attr('href', '<?php echo $this->Html->url(array('controller' => 'apostilamentos', 'action' => 'add', $coContrato)) ?>/' + $('#tp_apostilamento').val());
        })
    });

<?php echo $this->Html->scriptEnd() ?>
