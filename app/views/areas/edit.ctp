<?php echo $this->Form->create('Area', array('onsubmit' => "return confirm('Ao editar essa Área, você alterará todos os contratos e outros registros que possuem essa Área atrelada. Deseja prosseguir com essa alteração? (Sua alteração ficará registrada em log de alterações de Áreas)');"));?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Área'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->hidden('co_area');
                echo $this->Form->input('parent_id', array('class' => 'input-xxlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Vinculada a', true), 'options' => $imprimirArea->getArrayAreas($areas), 'escape' => false));
                echo $this->Form->input('ds_area', array('class' => 'input-xlarge','label' => 'Descrição'));
                ?>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // desalibitar o option de coSetor igual
        $('#AreaParentId option[value=' + $('#AreaCoArea').val() + ']').addClass('disabled').prop('disabled', true).trigger("chosen:updated");
    });
</script>
