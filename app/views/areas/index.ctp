<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Áreas'); ?></h1></div>
    <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/index') ): ?>
    <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--></p>
    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left"><span class="required" title="Required">Campos com * são obrigatórios</span> </p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'areas', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Nova Área'); ?>"><i class="icon-plus icon-white"></i> <?php __('Nova Área'); ?></a>
        </div>
    </div>
    <?php endif; ?>
    <?php echo $this->Form->create('Areas', array('url' => array('controller' => 'areas', 'action' => 'index'))); ?>
    <div class="row-fluid">
        	<div class="span3">
        		<div class="controls">
        			<?php echo $this->Form->input('ds_area', array('class' => 'input-xlarge','label' => __('Nome da Área', true),'maxLength'=>'40')); ?>
        		</div>
        	</div>
                </div>
     <div class="form-actions">
                <div class="btn-group">
                  <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
                  <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                  <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
     </div>
	<?php echo $this->Form->end(); ?>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th><?php __('Nome');?></th>
                <th class="actions"><?php __('Ações');?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($areas as $area):
                $imprimirArea->listaTabelaArea($area);
            endforeach; 
            ?>
        </tbody>
    </table>    
</div>

<?php echo $this->Html->scriptStart() ?>

    $(document).ready(function() {
        $('.tree').treegrid({
            'initialState': 'collapsed',
            'saveState': true,
            treeColumn: 0
        });
    })

<?php echo $this->Html->scriptEnd() ?>