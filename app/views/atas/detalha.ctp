﻿<?php
    $usuario = $this->Session->read ('usuario');
?>

<div class="atas form">
	                        
  <div class="acoes-formulario-top clearfix" >
            <div class="span4">
                <div class="pull-left btn-group">
                    <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas/edit') ) { ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'atas', 'action' => 'edit' , $id)); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Editar"><i class="icon-pencil icon-white"></i> Editar</a> 
                    <?php } ?>
                            <!-- a href="#view_impressao" class="v_impressao btn btn-small btn-primary" data-toggle="modal"><i class="icon-print"></i> Imprimir</a--> 
                            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
             </div>
    </div>
    
    <div id="accordion1" class="acordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" id="identificacao">
                    <?php __('Ata'); echo ": " . $this->Print->ata($ata['Ata']['nu_ata']); ?>
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                <div class="accordion-inner">

              <div class="row-fluid">

                <div class="span12 ">

                  <div class="body-box">
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Tipo de Ata') ?>: </dt>
                            <dd><?php echo $tpAta[ $ata['Ata']['tp_ata'] ]; ?></dd>
                            
                            <dt><?php __('Nº Processo Originário') ?>: </dt>
                            <dd><?php echo $this->Print->processo($ata['Ata']['nu_processo']); ?></dd>
                            
                            <dt><?php __('Pregão') ?>: </dt>
                            <dd><?php echo $tpPregao[ $ata['Ata']['tp_pregao'] ]; ?>&nbsp;</dd>
                            
                            <dt><?php __('Fornecedor') ?>: </dt>
                            <dd><?php echo $ata['Fornecedor']['no_razao_social']; ?></dd>
                            
                            <dt><?php __('Categoria') ?>: </dt>
                            <dd><?php echo $ata['Categoria']['ds_ata_categoria']; ?></dd>
                         </dl>
                      </div>
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Publicação') ?>: </dt>
                            <dd><?php echo $ata['Ata']['ds_publicacao']; ?>&nbsp;</dd>
                            
                            <dt><?php __('Especificações') ?>: </dt>
                            <dd><?php echo $ata['Ata']['ds_especificacao']; ?>&nbsp;</dd>
                            
                            <dt><?php __('Justificativas') ?>: </dt>
                            <dd><?php echo $ata['Ata']['ds_justificativa']; ?>&nbsp;</dd>
                         </dl>
                      </div>
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Vigência') ?>: </dt>
                            <dd><?php echo $ata['Ata']['dt_vigencia_inicio']; ?> &nbsp;&nbsp;à&nbsp;&nbsp; <?php echo $ata['Ata']['dt_vigencia_fim']; ?></dd>
                            
                            <dt><?php __('Valor da Ata') ?>: </dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($ata['Ata']['vl_ata']); ?>&nbsp;</dd>
                            
                            <dt><?php __('Valor Mensal') ?>: </dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($ata['Ata']['vl_mensal']); ?>&nbsp;</dd>
                            
                            <dt><?php __('Valor Anual') ?>: </dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($ata['Ata']['vl_anual']); ?></dd>
                         </dl>
                      </div>
                   </div>
                 </div>
               </div>

             </div>
           </div>
        </div>
    </div><br>

<?php
    
    $inativoall = true;
    $abaAtiva    = null;

    $abas = array();
    
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas_itens/index') ) {
            $abas[] = array( 'fsItens', __('Itens da Ata', true), true, $this->Html->url(array('controller' => 'atas_itens', 'action' => 'iframe', $id)) );
    }
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas_pedidos/index') ) {
            $abas[] = array( 'fsPedidos', __('Pedidos da Ata', true), true, $this->Html->url(array('controller' => 'atas_pedidos', 'action' => 'iframe', $id)) );
    }
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas/index') ) {
            $abas[] = array( 'fsPendencias', 'Pendências', true, $this->Html->url(array('controller' => 'pendencias', 'action' => 'iframe', $id, 'ata')) );
            $abas[] = array( 'fsHistoricos', 'Histórico', true,$this->Html->url(array('controller' => 'historicos', 'action' => 'iframe', $id, 'ata')) );
            $abas[] = array( 'fsAnexos', __('Suporte Documental', true), true, $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $id, 'ata')) ); 
    }
    
    if(count($abas) > 0) {
?>
                
    <div id="accordion4" class="accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseAba" data-parent="#accordion4" data-toggle="collapse" class="accordion-toggle" id="complemento">
                  Complemento
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseAba" style="height: auto;">
                <div class="accordion-inner">

                  <div class="row-fluid">  


                    <div class="box-tabs-white">
                    <ul class="nav nav-tabs" id="myTab">
                    <?php echo $aba->render( $abas ); ?>
                     </ul>
                     <div class="tab-content" id="myTabContent">

                    </div>

                    </div>

                  </div>


                </div>
            </div>
        </div>
    </div>
<?php
    }
?>

                  
</div>


<?php echo $this->Html->scriptStart() ?>

	$(document).ready(function(){
		//atualizaTotalizaValores();
		$("#myTab > li > a").first(function(){
			$(this).click();
		});		
	});
	function carregarAba(element,aba){
		$("#myTab > li").each(function(){
			$(this).removeClass('active');
		});
		element.parent().addClass('active');
		$("#myTabContent").load(element.attr('req'));
	}
	
	$( function() {                
                $("#myTabContent").load( $("#li_fsItens > a").attr('req') );
	});
        
	
<?php echo $this->Html->scriptEnd() ?>