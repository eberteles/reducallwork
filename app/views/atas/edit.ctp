<?php
echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Form->create('Ata');
echo $this->Form->hidden('co_ata');
echo $this->Form->hidden('tp_status');
?>

<b>Campos com * são obrigatórios.</b><br>
<div class="row-fluid">
    <div class="span8">
        <div class="widget-header widget-header-small"><h4>Dados da <?php __('Ata'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="row-fluid">
                        <?php
                        echo $this->Form->hidden('dt_cadastro');
                        echo $this->Form->input('tp_ata', array('class' => 'input-xsmall', 'type' => 'select', 'label' => 'Tipo de Ata', 'options' => $tpAta));
                        echo $this->Form->input('nu_ata', array('class' => 'input-small', 'label' => 'Nº Ata', 'mask'=>FunctionsComponent::pegarFormato( 'ata' )));
                        echo $this->Form->input('nu_processo', array('class' => 'input-xlarge', 'label'=>'Nº Processo Originário'));
                        echo $this->Form->input('tp_pregao', array('class' => 'input-xsmall', 'type' => 'select', 'label' => 'Pregão', 'options' => $tpPregao));
                        echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Fornecedor', 'options' => $fornecedores));
                        echo $this->Form->input('co_categoria_ata', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Categoria', 'options' => $categorias));
                        ?>
                    </div>
                  </div>
                  <div class="span6">
                    <?php
                    echo $this->Form->input('ds_publicacao', array('class' => 'input-xlarge', 'label' => 'Publicação', 'rows'=>'4', 'type' => 'textarea',
                        'onKeyup'=>'$(this).limit("1000","#charsLeft")', 
                        'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                    echo '<br />';
                    echo $this->Form->input('ds_especificacao', array('class' => 'input-xlarge', 'label' => 'Especificações', 'rows'=>'4', 'type' => 'textarea',
                        'onKeyup'=>'$(this).limit("1000","#charsLeft1")', 
                        'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'));
                    echo '<br />';
                    echo $this->Form->input('ds_justificativa', array('class' => 'input-xlarge', 'label' => 'Justificativas', 'rows'=>'4', 'type' => 'textarea',
                        'onKeyup'=>'$(this).limit("1000","#charsLeft2")', 
                        'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'));
                    ?>
                  </div>
                </div>
              </div>
            </div>
    </div>
    <div class="span4">
        <div class="widget-header widget-header-small"><h4><?php __('Datas / Valores'); ?></h4></div>
        <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                    <div class="span6">
                <?php
                echo $this->Form->input('dt_vigencia_inicio', array(
                        'before' => '<div class="input-append date datetimepicker">', 
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                        'class' => 'input-small','label' => 'Início da Vigência', 'type'=>'text'));
                ?>
                    </div>
                    <div class="span6">
                <?php
                echo $this->Form->input('dt_vigencia_fim', array(
                        'before' => '<div class="input-append date datetimepicker">', 
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                        'class' => 'input-small','label' => 'Fim da Vigência', 'type'=>'text'));
                ?>
                    </div>
                </div>
                <div class="row-fluid">
                <?php
                    echo $this->Form->input('vl_ata', array('class' => 'input-xlarge','label'=>'Valor da Ata (R$)', 'value' => $this->Print->real( $this->data['Ata']['vl_ata'] ) ));
                    echo $this->Form->input('vl_mensal', array('class' => 'input-xlarge','label'=>'Valor Mensal (R$)', 'value' => $this->Print->real( $this->data['Ata']['vl_mensal'] ) ));
                    echo $this->Form->input('vl_anual', array('class' => 'input-xlarge','label'=>'Valor Anual (R$)', 'value' => $this->Print->real( $this->data['Ata']['vl_anual'] ) ));
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar" id="Salvar"> Salvar</button> 
        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
        <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>



<script type="text/javascript">
    
    $(function(){
    
        setMascaraCampo("#AtaNuProcesso", "<?php echo FunctionsComponent::pegarFormato( 'processo' ); ?>");
        
        $("#AtaVlAta").maskMoney({thousands:'.', decimal:','});
        $("#AtaVlMensal").maskMoney({thousands:'.', decimal:','});
        $("#AtaVlAnual").maskMoney({thousands:'.', decimal:','});
        
    });

</script>
