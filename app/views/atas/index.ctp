<div id="root"
    data-token="<?php echo Configure::read('App.config.api.atas.token'); ?>"
    data-api-base-uri="<?php echo Configure::read('App.config.api.atas.url'); ?>"></div>

<script type="text/javascript">
let head = document.getElementsByTagName("head")[0];
let link = document.createElement("link");
link.type = 'text/css';
link.rel = "stylesheet";
link.href = "/static/css/main.5a6c68c8.css";

head.appendChild(link);

</script>
<script src="/static/js/main.cf2d01c5.js" type="text/javascript"></script>
