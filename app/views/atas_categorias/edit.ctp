<?php echo $this->Form->create('AtaCategoria', array('url' => "/atas_categorias/edit/$id")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Categoria de Ata'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_ata_categoria');
                echo $this->Form->input('ds_ata_categoria', array('type' => 'text', 'class' => 'input-xlarge', 'label' => 'Descrição'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>
