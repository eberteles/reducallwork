<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="aditivos form">
<?php 
    echo $this->Form->create('AtasItem', array('url' => "/atas_itens/edit/$id/$coAta"));
?>

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar um Item da Ata - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'atas_itens', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Itens">Listagem</a> 
          </div>
        </div>
    
       <div class="row-fluid">
                        
            <div class="span12 ">
                <div class="widget-header widget-header-small"><h4>Alterar Item</h4></div>
             
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span4">
              	     <dl class="dl-horizontal">
                    <?php
                        if(count($lotes) > 0) {
                            echo $this->Form->input('co_ata_lote', array('label' => 'Lote', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $lotes));
                        }
                        
                        echo $this->Form->input('nu_item', array('class' => 'input-small', 'label'=>'Nº Item'));
                        
                        echo $this->Form->input('ds_marca', array('class' => 'input-xlarge', 'label'=>'Marca'));
                        
                        echo $this->Form->input('ds_descricao', array('class' => 'input-xlarge', 'label'=>'Descrição', 'type'=>'textarea', 'rows'=>'4', 'cols'=>'40',
                            'onKeyup'=>'$(this).limit("500","#charsLeft")', 
                            'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                        echo '<br />';
                    ?>
	             </dl>
                  </div>
                  <div class="span4">
                    <dl class="dl-horizontal">
                    <?php
                        echo $this->Form->input('ds_unidade', array('class' => 'input-small', 'label'=>'Unidade'));
                        
                        echo $this->Form->input('nu_quantidade', array('class' => 'input-small', 'label'=>'Quantidade'));
                        
                        echo $this->Form->input('vl_unitario', array('class' => 'input-large','label'=>'Valor Unitário (R$)', 'value' => $this->Print->real( $this->data['AtasItem']['vl_unitario'] ) ));
                    ?>
                    </dl>
	         </div>
                </div>
              </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Item"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>

    $(function(){
    
        $("#AtasItemVlUnitario").maskMoney({thousands:'.', decimal:','});
        $("#AtasItemNuItem").maskMoney({precision:0, allowZero:false, thousands:''});
        $("#AtasItemNuQuantidade").maskMoney({precision:0, allowZero:false, thousands:''});
        
    }); 

<?php echo $this->Html->scriptEnd() ?>