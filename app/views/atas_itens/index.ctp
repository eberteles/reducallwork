<?php echo $this->Html->scriptStart() ?>

	$( function() {
		//parent.atualizaTotalizaValores();
	});

<?php echo $this->Html->scriptEnd() ?>
        
<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="atas_itens index" id="pg_atas_itens">
<!-- h2>< ?php __('AtasItems');?></h2 -->

<?php
    $i = 0;
    $nu_lote        = '';
    $primeiro_reg   = true;
    $temLote        = false;
    foreach ($itens as $item): 
        $i++;
        if( ($nu_lote == '' && $item['Lote']['nu_lote'] != '') || ($nu_lote != '' && $item['Lote']['nu_lote'] != '' && $item['Lote']['nu_lote'] != $nu_lote) ) {
            $temLote    = true;
            if(!$primeiro_reg && $nu_lote == '') {
                echo "</table>";
                $primeiro_reg   = true;
            }
            if($nu_lote != '') {
                echo "</table></div></div></div></div><br>";
                $primeiro_reg   = true;
            }
            $nu_lote = $item['Lote']['nu_lote'];
?>
<div id="accordion<?php echo $item['Lote']['co_ata_lote']; ?>" class="acordion">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a href="#collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                Lote <?php echo $item['Lote']['nu_lote']; ?> - <?php echo $item['Lote']['ds_objeto']; ?>
            </a>
        </div>

        <div class="accordion-body in collapse" id="collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" style="height: auto;">
            <div class="accordion-inner">
<?php
        }
        if($primeiro_reg) {
            $primeiro_reg = false;
?>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped"  id="tbItem">
	<tr>
		<th width="5%"><?php __('Item');?></th>
		<th width="10%"><?php __('Marca');?></th>
		<th width="45%"><?php __('Objeto');?></th>
		<th width="5%"><?php __('Unidade');?></th>
		<th width="10%"><?php __('Qtd Registrada');?></th>
		<th width="10%"><?php __('Restam');?></th>
		<th width="10%"><?php __('Valor Unitário');?></th>
		<th width="10%"><?php __('Valor Total');?></th>
		<th width="5%" class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
        }
//        $qtd_pedido = 0;
//        foreach ($item['Pedido'] as $pedido):
//            $qtd_pedido += $pedido['nu_quantidade'];
//        endforeach;
	?>
	<tr>
                <td><?php echo $item['AtasItem']['nu_item']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_marca']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_descricao']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_unidade']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['qt_utilizado']; ?></td>
                <td><?php echo $item['AtasItem']['nu_quantidade'] - $item['AtasItem']['qt_utilizado']; ?> - <?php echo $this->Print->precisao(100 - ($item['AtasItem']['qt_utilizado'] * 100) / $item['AtasItem']['nu_quantidade'], 2); ?>%</td>
		<td><?php echo $this->Formatacao->moeda( $item['AtasItem']['vl_unitario'] ); ?>&nbsp;</td>
		<td><?php echo $this->Formatacao->moeda( $item['AtasItem']['vl_unitario'] * $item['AtasItem']['nu_quantidade'] ); ?>&nbsp;</td>
		<td class="actions"><?php $id = $item['AtasItem']['co_ata_item']; ?> 
			<div class="btn-group acoes">	
				<?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coAta , 'class' => 'btn', 'local_acao' => 'atas_itens/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
    </table>                
<?php
    if($temLote) {
?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>
                
</div>
<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas/edit') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="#add-lote" id="btn-add-lote" data-toggle="modal" class="btn btn-small btn-primary">Adicionar Lote</a> 
            <a href="<?php echo $this->Html->url(array('controller' => 'atas_itens', 'action' => 'add', $coAta)); ?>" class="btn btn-small btn-primary">Adicionar Item</a>
          </div>
        </div>

</div>
<?php } ?>
        
<div id="add-lote" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="myModalLabel">Adicionar Lote</h4>
  </div>
  <div class="modal-body">
    <form class="form-horizontal">
      <div class="control-group">
          <?php
                echo $this->Form->hidden('co_ata', array('value' => $coAta));
                echo $this->Form->hidden('url_base', array('value' => $this->base));
          ?>
          <div class="required" id="id_nu_lote">
          <?php 
                echo $this->Form->input('nu_lote', array('class' => 'input-small', 'label'=>'Nº Lote'));
          ?>
          </div>
          <div class="required" id="id_ds_objeto">
          <?php 
                echo $this->Form->input('ds_objeto', array('class' => 'input-xlarge', 'label'=>'Objeto do Lote'));
          ?>
          </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
  <button class="btn btn-small btn-primary" data-dismiss="modal" id="add_concluir"><i class="icon-ok icon-white"></i> Confirmar</button>
  <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
  </div>
</div>
        
        
<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();
        
    $(document).ready(function(){
    
        $('#btn-add-lote').click(function(){

            $('#nu_lote').val('');
            $('#ds_objeto').val('');
            $('#id_error_nu_lote').remove();
            $('#id_error_ds_objeto').remove();
        
        })
        
        $('#add_concluir').click(function(){
            $('#id_error_nu_lote').remove();
            $('#id_error_ds_objeto').remove();

            var validacao   = false;
            if($('#nu_lote').val() == '') {
                $('#id_nu_lote').append('<div class="error-message" id="id_error_nu_lote">Campo Nº Lote em branco.</div>');
                validacao   = true;
            }
            if($('#ds_objeto').val() == '') {
                $('#id_ds_objeto').append('<div class="error-message" id="id_error_ds_objeto">Campo Objeto do Lote em branco.</div>');
                validacao   = true;
            }

            if(validacao) {
                return false;
            } else { // Gravar Lote
                var url_add = "<?php echo $this->Html->url(array ('controller' => 'atas_itens', 'action' => 'add_lote')); ?>";
                $.ajax({
                    type:"POST",
                    url:url_add,
                    data:{
                        "data[co_ata]":$('#co_ata').val(),
                        "data[nu_lote]":$('#nu_lote').val(),
                        "data[ds_objeto]":$('#ds_objeto').val()
                    },
                    success:function(result){
                        //$('#id_pg_execucao').html("");
                        //$('#pg_atas_itens').append(result);
                    }
                })
            }
        })
    
    });
    
<?php echo $this->Html->scriptEnd() ?>