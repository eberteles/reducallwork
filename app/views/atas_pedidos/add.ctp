<?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="pedidos form">
<?php 
    echo $this->Form->create('AtasPedido', array('url' => "/atas_pedidos/add/$coAta"));
?>

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Pedido à Ata - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'atas_pedidos', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Itens">Listagem</a> 
          </div>
        </div>
    
       <div class="row-fluid">
                        
            <div class="span12 ">
                <div class="widget-header widget-header-small"><h4>Novo Pedido</h4></div>
             
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span4">
              	     <dl class="dl-horizontal">
                    <?php
                        echo $this->Form->hidden('co_ata', array('value' => $coAta));
                        echo $this->Form->hidden('tp_pedido', array('value' => 'P'));
                        echo $this->Form->hidden('co_usuario', array('value' => $usuario['Usuario']['co_usuario']));
                        //echo $this->Form->hidden('co_contrato');
                        
                        echo $this->Form->input('ds_pedido', array('class' => 'input-xlarge', 'label'=>'Descrição'));
                    ?>
	             </dl>
                  </div>
                  <div class="span1">
                    <dl class="dl-horizontal">
                    <?php
                        //echo $this->Form->input('nu_processo', array('class' => 'input-small', 'label'=>'Nº Processo', 'mask'=>FunctionsComponent::pegarFormato( 'processo' )));
                    ?>
                    </dl>
	         </div>
                  <div class="span1">
              	     <dl class="dl-horizontal">
<!--                         <br><center>ou</center>-->
	             </dl>
                  </div>
                  <div class="span1">
                    <dl class="dl-horizontal">
                    <?php
                        //echo $this->Form->input('nu_contrato', array('class' => 'input-small', 'label'=>'Nº Contrato', 'mask'=>FunctionsComponent::pegarFormato( 'contrato' )));
                    ?>
                    </dl>
	         </div>
                </div>
                <div class="row-fluid">
                    
<?php
    $i = 0;
    $nu_lote        = '';
    $primeiro_reg   = true;
    $temLote        = false;
    foreach ($itens as $item): 
        $i++;
        if( ($nu_lote == '' && $item['Lote']['nu_lote'] != '') || ($nu_lote != '' && $item['Lote']['nu_lote'] != '' && $item['Lote']['nu_lote'] != $nu_lote) ) {
            $temLote    = true;
            if(!$primeiro_reg && $nu_lote == '') {
                echo "</table>";
                $primeiro_reg   = true;
            }
            if($nu_lote != '') {
                echo "</table></div></div></div></div><br>";
                $primeiro_reg   = true;
            }
            $nu_lote = $item['Lote']['nu_lote'];
?>
<div id="accordion<?php echo $item['Lote']['co_ata_lote']; ?>" class="acordion">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a href="#collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                Lote <?php echo $item['Lote']['nu_lote']; ?> - <?php echo $item['Lote']['ds_objeto']; ?>
            </a>
        </div>

        <div class="accordion-body in collapse" id="collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" style="height: auto;">
            <div class="accordion-inner">
<?php
        }
        if($primeiro_reg) {
            $primeiro_reg = false;
?>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped"  id="tbPedido">
	<tr>
		<th width="5%"><?php __('Item');?></th>
		<th width="10%"><?php __('Marca');?></th>
		<th width="45%"><?php __('Objeto');?></th>
		<th width="5%"><?php __('Unidade');?></th>
		<th width="10%"><?php __('Restam');?></th>
		<th width="10%"><?php __('Valor Unitário');?></th>
		<th width="5%"><?php __('Qt Pedir');?></th>
                <th width="10%"><?php __('Valor Pedido');?></th>
	</tr>
	<?php
        }
//        $qtd_pedido = 0;
//        foreach ($item['Pedido'] as $pedido):
//            $qtd_pedido += $pedido['nu_quantidade'];
//        endforeach;
        echo $this->Form->hidden('co_ata_item][' . $item['AtasItem']['co_ata_item'], array('value' => $item['AtasItem']['co_ata_item']));
	?>
	<tr valor="<?php echo str_replace('.', '', $item['AtasItem']['vl_unitario']); ?>" quantidade="<?php echo $item['AtasItem']['nu_quantidade'] - $item['AtasItem']['qt_utilizado']; ?>" codigo="<?php echo $item['AtasItem']['co_ata_item']; ?>">
                <td><?php echo $item['AtasItem']['nu_item']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_marca']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_descricao']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['ds_unidade']; ?>&nbsp;</td>
                <td><?php echo $item['AtasItem']['nu_quantidade'] - $item['AtasItem']['qt_utilizado']; ?> - <?php echo $this->Print->precisao(100 - ($item['AtasItem']['qt_utilizado'] * 100) / $item['AtasItem']['nu_quantidade'], 2); ?>%</td>
		<td><?php echo $this->Formatacao->moeda( $item['AtasItem']['vl_unitario'] ); ?>&nbsp;</td>
		<td><?php echo $this->Form->input('qt_pedir][' . $item['AtasItem']['co_ata_item'], array('class' => 'input-xmini numero', 'label'=>'')); ?></td>
		<td><?php echo $this->Form->input('vl_pedir][' . $item['AtasItem']['co_ata_item'], array('class' => 'input-small dinheiro', 'label'=>'', 'readonly'=>'readonly', 'id'=>'AtasPedidoVlPedir' . $item['AtasItem']['co_ata_item'])); ?></td>
	</tr>
	<?php endforeach; ?>
    </table>                
<?php
    if($temLote) {
?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>
                    
                </div>
              </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Pedido"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>

    $(function(){
    
        $(".numero").maskMoney({precision:0, allowZero:false, thousands:''});
        
    }); 
    
    $(document).ready(function() {
        $('#tbPedido tr td div input.numero').focusout(function(){
            if( parseInt( $(this).val() ) > parseInt( $(this).parents('tr').attr('quantidade') ) ) {
                alert('A quantidade infordada é maior do que o restante para este Item!');
                $(this).val( $(this).parents('tr').attr('quantidade') );
            }
            $('#AtasPedidoVlPedir' + $(this).parents('tr').attr('codigo')).val( 
                formatReal($(this).val() * parseFloat( $(this).parents('tr').attr('valor') ) ) );
        })
    })
    
    function formatReal( int )
    {
            var tmp = int+'';
            var neg = false;
            if(tmp.indexOf("-") == 0)
            {
                neg = true;
                tmp = tmp.replace("-","");
            }

            if(tmp.length == 1) tmp = "0"+tmp

            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6)
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

            if( tmp.length > 9)
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");

            if( tmp.length > 12)
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");

            if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
            if(tmp.indexOf(",") == 0) tmp = tmp.replace(",","0,");

        return (neg ? '-'+tmp : tmp);
    }

<?php echo $this->Html->scriptEnd() ?>