<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="pedidos form">

	<div class="acoes-formulario-top clearfix" >
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'atas_pedidos', 'action' => 'index', $coAta)); ?>" class="btn btn-small btn-primary" title="Listar Itens">Listagem</a> 
          </div>
        </div>
    
       <div class="row-fluid">
                        
            <div class="span12 ">
                <div class="widget-header widget-header-small"><h4>Detalhes do Pedido</h4></div>
             
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span4">
              	     <dl class="dl-horizontal">
                         <dt><?php __('Descrição') ?>: </dt>
                         <dd><?php echo $pedido['AtasPedido']['ds_pedido']; ?></dd>
                         <dt><?php __('Data do Pedido') ?>: </dt>
                         <dd><?php echo $pedido['AtasPedido']['dt_pedido']; ?></dd>
	             </dl>
                  </div>
                  <div class="span4">
                    <dl class="dl-horizontal">
                        <dt><?php __('Total do Pedido') ?>: </dt>
                        <dd class="right"><?php echo $this->Formatacao->moeda($pedido['AtasPedido']['vl_pedido']); ?></dd>
                    <?php
                        //echo $this->Form->input('nu_processo', array('class' => 'input-small', 'label'=>'Nº Processo', 'mask'=>FunctionsComponent::pegarFormato( 'processo' )));
                    ?>
                    </dl>
	         </div>
                </div>
                <div class="row-fluid">
                    
<?php
    $i = 0;
    $nu_lote        = '';
    $primeiro_reg   = true;
    $temLote        = false;
    foreach ($pedido['PedidosItem'] as $item): 
        $i++;
        if( ($nu_lote == '' && isset($item['Lote']['nu_lote']) ) || ($nu_lote != '' && isset($item['Lote']['nu_lote']) && $item['Lote']['nu_lote'] != $nu_lote) ) {
            $temLote    = true;
            if(!$primeiro_reg && $nu_lote == '') {
                echo "</table>";
                $primeiro_reg   = true;
            }
            if($nu_lote != '') {
                echo "</table></div></div></div></div><br>";
                $primeiro_reg   = true;
            }
            $nu_lote = $item['Lote']['nu_lote'];
?>
<div id="accordion<?php echo $item['Lote']['co_ata_lote']; ?>" class="acordion">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a href="#collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle">
                Lote <?php echo $item['Lote']['nu_lote']; ?> - <?php echo $item['Lote']['ds_objeto']; ?>
            </a>
        </div>

        <div class="accordion-body in collapse" id="collapseIdent<?php echo $item['Lote']['co_ata_lote']; ?>" style="height: auto;">
            <div class="accordion-inner">
<?php
        }
        if($primeiro_reg) {
            $primeiro_reg = false;
?>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped"  id="tbPedido">
	<tr>
		<th width="5%"><?php __('Item');?></th>
		<th width="10%"><?php __('Marca');?></th>
		<th width="45%"><?php __('Objeto');?></th>
		<th width="5%"><?php __('Unidade');?></th>
		<th width="10%"><?php __('Valor Unitário');?></th>
		<th width="5%"><?php __('Quantidade');?></th>
                <th width="10%"><?php __('Valor Pedido');?></th>
	</tr>
	<?php
        }
	?>
	<tr>
                <td><?php echo $item['nu_item']; ?>&nbsp;</td>
                <td><?php echo $item['ds_marca']; ?>&nbsp;</td>
                <td><?php echo $item['ds_descricao']; ?>&nbsp;</td>
                <td><?php echo $item['ds_unidade']; ?>&nbsp;</td>
		<td><?php echo $this->Formatacao->moeda( $item['vl_unitario'] ); ?>&nbsp;</td>
                <td><?php echo $item['qt_pedido']; ?>&nbsp;</td>
                <td><?php echo $this->Formatacao->moeda( $item['vl_unitario'] * $item['qt_pedido'] ); ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
    </table>                
<?php
    if($temLote) {
?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>
                    
                </div>
              </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>