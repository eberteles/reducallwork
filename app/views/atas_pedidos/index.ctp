<?php echo $this->Html->scriptStart() ?>

	$( function() {
		//parent.atualizaTotalizaValores();
	});

<?php echo $this->Html->scriptEnd() ?>
        
<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="atas_pedidos index" id="pg_atas_pedidos">
<!-- h2>< ?php __('AtasPedidos');?></h2 -->

    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped"  id="tbPedido">
	<tr>
		<th width="35%"><?php __('Descrição');?></th>
<!--		<th width="20%"><?php __('Processo / Contrato');?></th>-->
<!--		<th width="20%"><?php __('Unidade Solicitante');?></th>-->
		<th width="10%"><?php __('Data Pedido');?></th>
		<th width="10%"><?php __('Valor Total');?></th>
		<th width="5%" class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
        $nuProcessoContrato = '';
        foreach ($pedidos as $pedido): 
            if($pedido['Contrato']['nu_processo'] != '') {
                $nuProcessoContrato = $this->Print->processo($pedido['Contrato']['nu_processo']);
            }
            if($pedido['Contrato']['nu_contrato'] != '') {
                if($nuProcessoContrato != '') {
                    $nuProcessoContrato += ' / ';
                }
                $nuProcessoContrato += $this->Print->contrato($pedido['Contrato']['nu_contrato']);
            }
            $id = $pedido['AtasPedido']['co_ata_pedido'];
	?>
	<tr>
                <td><?php echo $this->Html->link($pedido['AtasPedido']['ds_pedido'], $this->Html->url(array('controller' => 'atas_pedidos', 'action' => 'detalha', $id,$coAta))); ?>&nbsp;</td>
<!--                <td><?php echo $nuProcessoContrato; ?>&nbsp;</td>-->
<!--                <td><?php if($pedido['Contrato']['co_contratante'] > 0) {echo $setores[$pedido['Contrato']['co_contratante']];} ?>&nbsp;</td>-->
                <td><?php echo $pedido['AtasPedido']['dt_pedido']; ?>&nbsp;</td>
		<td><?php echo $this->Formatacao->moeda( $pedido['AtasPedido']['vl_pedido'] ); ?>&nbsp;</td>
		<td class="actions">
                    <div class="btn-group acoes">	
				<?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coAta , 'class' => 'btn', 'local_acao' => 'atas_pedidos/index' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
    </table>                
                
</div>
<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'atas/edit') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'atas_pedidos', 'action' => 'add', $coAta)); ?>" class="btn btn-small btn-primary">Adicionar Pedido</a>
          </div>
        </div>

</div>
<?php } ?>
        
<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();
    
<?php echo $this->Html->scriptEnd() ?>