<?php echo $this->Html->script('inicia-datetimepicker');
echo $this->Html->script('moment/moment.js'); ?>

<div class="acoes-formulario-top clearfix" >
    <div class="span12">
        <div class="pull-right btn-group">
            <a class="btn btn-primary btn-small btn-rounded" href="<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'atividades_fiscalizacao')); ?>">Atividades de fiscalização</a>
        </div>
    </div>
</div>

<div class="widget-header widget-header-small">
    <h4>Nova Atividade</h4>
</div>
<div class="widget-body">
    <div class="widget-main">
        <?php
      echo $this->Form->create('Atividade', array(
          'id' => 'formNovaAtividade',
          'action' => 'add_atividade',
          'method' => 'POST',
          'class' => "form-horizontal")
        );

        // if($this->Modulo->getHasNewFornecedores()) {
            // echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
        // }

        // vincular atividade
        /*echo $this->Form->input('parent_id', array(
            'label' => 'Vincular a ' . __('Atividade', true),
            'type' => 'select',
            'empty' => 'Selecione...',
            'options' => $atividades_pai
        ));*/ ?>

        <!-- Nome da atividade -->
        <div class="required" id="">
            <?php echo $this->Form->input('nome', array(
                'id' => 'nomeAtividade',
                'label' => 'Nome da atividade',
                'type' => 'text',
                'class' => 'input-xxlarge control-labels'
            )) ?>
            <span id="message_nome_atividade" style="color: red;display:none"><b>Campo nome da atividade em branco </b></span>
        </div>

        <!-- Contrato -->  
            <p class="help-block">Contratos</p>          
            <fieldset style="width:545px !important">
                <!--<legend>&nbsp;Contratos</legend>-->
                <p><select name="data[Atividade][tipo]"id="selectContratos" class="input-medium">
                    <option value="0" selected="selected">Específicos</option>
                    <option value="1">Todos</option>
                </select></p>
                
                <select id="selectAtividadesContratos" name="data[Atividade][contratos][]" data-placeholder="Selecione os contratos" class="input-xxlarge chosen-select" multiple="true">
                <?php foreach ($contratosIds as $id => $nuContrato) : ?>
                    <option value="<?php echo $id ?>"><?php echo $nuContrato ?></option>
                <?php endforeach ?>
                </select>
            </fieldset>

        <!-- Descrição -->
        <div class="required control-groups" id="id_ds_atividade">
            <?php echo $this->Form->input('ds_atividade', array(
                'id' => 'ds_atividade',
                'label' => 'Descrição da Atividade',
                'class' => 'input-xxlarge',
                'type' => 'textarea',
                'maxlength' => 250,
                // 'cols' => 10,
                'rows' => 8
            )); ?>
            <span id="message_atividade" style="color: red;display:none"><b>Campo descrição da atividade deve possuir entre 3 e 100 caracteres!</b></span>
            <span id="message_atividade2" style="color: red;display:none"><b>Campo descrição em branco</b>
            </span>
        </div>

        <!-- Inicio da atividade 
        <div class="required">
            <?php
            /*echo $this->Form->input('dt_ini_planejado', array(
                'id' => 'dt_ini_planejado',
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                'class' => 'input-small',
                'label' => 'Início da atividade', 
                'type'=>'text'));
            */?>
            <span id="message_dt_inicio_atividade" style="color: red;display:none"><b>Campo inicio da atividade em branco</b></span>
            <span id="message_dt_inicio_atividade2" style="color: red;display:none"><b>A data do inicio da atividade é maior que a data do fim do contrato</b></span>
        </div>
        
        fim da atividade
        <div class="" id="__id_dt_fim_planejado">
            <?php
            /*echo $this->Form->input('dt_fim_planejado', array(
                'id' => 'dt_fim_planejado',
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                'class' => 'input-small',
                'label' => 'Término da atividade',
                'type'=>'text'));
            */?>
            <span id="message_dt_fim_atividade" style="color: red;display:none"><b>A data do término da atividade é maior que a data do fim do contrato</b></span><br />
            <span id="message_dt_fim_atividade2" style="color: red;display:none"><b>A data do término da atividade é menor que a data do início da atividade</b></span>
        </div>-->

        <fieldset>
            <!-- Periodicidade -->
                <?php
                echo $this->Form->input('periodicidade', array(
                    'type' => 'select',
                    'id' => 'periodicidade',
                    'class' => ' input-xlarge',
                    'label' => 'Periodicidade',
                    'empty' => 'Selecione...',
                    'options' => $periodicidadeArr
                ));
                ?>

            <!-- Dia para execução -->
                <?php
                echo $this->Form->input('dia_execucao', array(
                    'type' => 'select',
                    'id' => 'dia_execucao',
                    'class' => 'span2 input-xlarge control-labels',
                    'label' => 'Dia para execução',
                    'empty' => 'Selecione...',
                    'options' => $dia_execucaoArr 
                ));
                ?>
        </fieldset>
        <div class="span12">
            <?php
            if($this->Modulo->getHasNewFornecedores()) {
                echo $this->Form->input('ds_assunto_email', array('label' => 'Assunto (Para envio de email)'));
            }
            ?>
        </div>
        <div style="padding-left:8px" class="form-actions">
            <div style="float:left !important" class="btn-group">
                <button id="btnSalvar" rel="tooltip" type="submit" class="btn btn-small btn-primary" title="Salvar atividade">
                    Salvar
                </button>
                 <!--<button rel="tooltip" type="reset" title="Limpar dados preenchidos"class="btn btn-small">
                    Limpar
                </button>-->
                <a href="<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'atividades_fiscalizacao'))?>" title="Voltar" class="btn btn-small">
                Voltar
                </a>
		    </div>
            </div>
        </form>
	</div>
    </div>
</div>

<script type="text/javascript">
    // verifica se existe algum campo inválido e desabilita o botão de salvar
    function checkInvalid() {
        var ids = [
            '#nomeAtividade',
            '#ds_atividade',
            '#dt_ini_planejado'
        ];
        var count = 0;

        for (var i = 0; i < ids.length; i++) {
            if (ids[i] == '#dt_ini_planejado') {
                if ($(ids[i]).val() == '__/__/____' || $(ids[i]).val() == '' || $(ids[i]).val() == ' ') {
                    count++;
                }
                
            } else if ($(ids[i]).val() == '' || $(ids[i]).val() == ' ') {
                count++;
            } 
        }
        count ? $('#btnSalvar').prop('disabled', true) : $('#btnSalvar').prop('disabled', false);
    }

    $(document).ready(function() {
        // periodicidade
        $("#periodicidade").on('input', function () {
            var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
            if (arr.indexOf($('#periodicidade').val()) != -1) {
                $('#dia_execucao').attr('disabled', 'disabled');
                $('#dia_execucao').prop('disabled', 'disabled');
            } else {
                $("#dia_execucao").removeAttr('disabled');
                $('#dia_execucao').removeProp('disabled');
            }
        });

        // nome do responsável
        $('#co_responsavel').on('input', function () {
            if ($('#co_responsavel').val() == '' || $('#co_responsavel').val() == '')  {
                $('#message_co_responsavel_atividade').show();
            } else {
                $('#message_co_responsavel_atividade').hide();
            }
        });     

        // função
        $('#funcao_responsavel').on('input', function () {
            if ($('#funcao_responsavel').val() == '' || $('#funcao_responsavel').val() == '')  {
                $('#btnSalvar').attr('disabled', 'disabled');
                $('#message_funcaoResponsavel').show();
            } else {
                $('#message_funcaoResponsavel').hide();
            }
        });

        // nome atividade
        $('#nomeAtividade').on('input blur', function () {
            if ($('#nomeAtividade').val() == '' || $('#nomeAtividade').val() == '')  {
                $('#message_nome_atividade').show();
            } else {
                $('#message_nome_atividade').hide();
            }
        });

        // descrição da atividade
        $('#ds_atividade').on('input blur', function(){
            var tamanho = $('#ds_atividade').val().length;
            if (tamanho < 3 || tamanho > 100) {
                $('#message_atividade').show();
                checkInvalid();
            } else {
                $('#message_atividade').hide();
                $('#message_atividade2').hide();
            }
        });

        $('#dt_ini_planejado + span').on('click', function () {
            $('#dt_ini_planejado').focus();
        });

        // data de inicio da atividade
        //  $('#dt_ini_planejado').on('blur', function () {
        //      var data_ini = $('#dt_ini_planejado').val();
        //         if (data_ini == "" || data_ini == " " || data_ini == '__/__/____')  {
        //             $('#message_dt_inicio_atividade').show();
        //             $('#btnSalvar').prop('disabled', true);
        //         } else {
        //             $('#btnSalvar').prop('disabled', false);
        //             $('#message_dt_inicio_atividade').hide();

        //             var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
        //             var dataInicioAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');
        //             var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
        //             // console.log('days:', dataInicioAtividade.diff(dataVigenciaContrato, 'days'));

        //             if (dataInicioAtividade.diff(dataVigenciaContrato, 'days') > 0) {
        //                 $('#message_dt_inicio_atividade2').show();
        //                 $('#btnSalvar').prop('disabled', true);

        //                 if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
        //                     $('#btnSalvar').prop('disabled', true);
        //                     $('#message_dt_fim_atividade').show();
        //                 }
        //                 return;
        //             } else {
        //             	$('#message_dt_inicio_atividade2').hide();
        //                 $('#btnSalvar').prop('disabled', false);
        //             }

        //             if (dataFimAtividade.diff(dataInicioAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
        //                 $('#message_dt_fim_atividade2').show();
        //                 $('#btnSalvar').prop('disabled', true);
        //             } else {
        //                 $('#btnSalvar').prop('disabled', false);
        //                 $('#message_dt_fim_atividade2').hide();
        //             }

        //         }
        // });

        // $('#dt_fim_planejado + span').on('click', function () {
        //     $('#dt_fim_planejado').focus();
        // });

        // // data do fim da atividade
        // $("#dt_fim_planejado").on('blur', function () {
        //     var data_fim = $('#dt_fim_planejado').val();
		//     if (data_fim != "" || data_fim != " " || data_fim != '__/__/____') {
        //         var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
    	// 		var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
        //         var dataIniAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');

    	// 		if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
        //             $('#message_dt_fim_atividade').show();
        //             $('#btnSalvar').prop('disabled', true);
        //             return;
    	// 		} else {
    	// 			$('#message_dt_fim_atividade').hide();
        //             $('#btnSalvar').prop('disabled', false);
    	// 		}

        //         if (dataFimAtividade.diff(dataIniAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
        //             $('#message_dt_fim_atividade2').show();
        //             $('#btnSalvar').prop('disabled', true);
        //         } else {
        //             $('#message_dt_fim_atividade2').hide();
        //             $('#btnSalvar').prop('disabled', false);
        //         }
        //     }
		// });

        $('#btnSalvar').on('click', function(e) {
            $('#message_dt_inicio_atividade').hide();
            $('#message_co_responsavel_atividade').hide();
            $('#message_funcaoResponsavel').hide();
            $('#message_nome_atividade').hide();
            $('#message_atividade').hide();
            $('#message_atividade2').hide();

            var dataFimPlanejado = $('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '' ? 
                $('#dt_fim_vigencia_atividade').val() :
                $('#dt_fim_planejado').val();

            $('#dt_fim_planejado').attr('value', dataFimPlanejado);
        
            var validacao = false;
            
            // nome atividade
            if ($('#nomeAtividade').val() == '') {
                $('#message_nome_atividade').show();
                validacao = true;
            }

            // descrição
            if ($('#ds_atividade').val() == '') {
                $('#message_atividade2').show();
                validacao = true;
            }

            // inicio atividade
            if ($('#dt_ini_planejado').val() == '') {
                $('#message_dt_inicio_atividade').show();
                validacao = true;
            }

            if ($('#message_dt_fim_atividade').is(":visible") || $('#message_dt_inicio_atividade').is(":visible") || $('#message_dt_inicio_atividade2').is(":visible") || $('#message_dt_fim_atividade2').is(':visible')    ) {
                validacao = true;
            }

            if ($('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '__/__/__') {
                // $('#dt_fim_planejado').val($('#dt_fim_vigencia_atividade').val()); 
            } 

            if (validacao) {
                e.preventDefault();
                return false;
            }
        });

    });
</script>