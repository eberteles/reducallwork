<?php
// debug($atividadesContratos);exit;
?>

<div class="page-header position-relative"><h1><?php __('Atividades de Fiscalização'); ?></h1></div>

<div class="acoes-formulario-top clearfix">
    <div class="span12">
        <div class="pull-right btn-group">
            <a class="btn btn-primary btn-small" href="<?php
            echo $this->Html->url(array('controller' => 'atividades', 'action' => 'add_atividade')); ?>">Nova
                Atividade</a>
        </div>
    </div>
</div>

<div id="alertasAtividades"></div>
<div id="table-atividades-fiscalizacao" style="overflow:visible" class="row-fluid">


    <table cellpadding="0" cellspacing="0" border="0" style="width:100% !important" id="table-atividades-contratos"
           class="table table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>ITEM</th>
            <th>Atividade</th>
            <th>Contrato</th>
            <th>Periodicidade</th>
            <th>Ações</th>
        </tr>
        </thead>
        <!--<tfoot>
            <tr>
                <td colspan="5" style="text-align: left;">
                    <button onclick="AddTableRow()" type="button">Adicionar Produto</button>
                </td>
            </tr>
        </tfoot>-->
        <tbody>
        <?php $count = 1;
        foreach ($atividadesContratos as $atividadeContrato) : ?>
            <tr>
                <td><?php echo $count++; ?></td>

                <td><?php echo $atividadeContrato['Atividade']['nome'] ?></td>

                <td>
                    <?php echo $atividadeContrato['Atividade']['todos_contratos'] ? 'Todos' : $atividadeContrato[0]['contratos'] ?>
                </td>

                <td>
                    <?php echo !empty($atividadeContrato['Atividade']['periodicidade']) ? $atividadeContrato['Atividade']['periodicidade'] : '-' ?>
                </td>

                <td>
                    <div class="btn-group">
                        <!-- detalhar -->
                        <a href="#" data-id="<?php echo $atividadeContrato['AtividadeContrato']['id_atividade'] ?>"
                           class="modal-detalharAtividade btn btn-default" data-target="#detalhar-atividade"
                           data-toggle="modal">
                            <i class="icon-eye-open " title="Detalhar">
                            </i>
                        </a>&nbsp;

                        <a href="/atividades/editar_atividade/<?php echo $atividadeContrato['AtividadeContrato']['id_atividade'] ?>"
                           class="btn btn-default">
                            <i class="icon-edit"></i>
                        </a>
                        <a data-toggle="modal" href="#"
                           data-id="<?php echo $atividadeContrato['AtividadeContrato']['id_atividade'] ?>"
                           title="excluir atividade" class="modal-delAtividade btn btn-danger"
                           data-target="#confirm-delete-atividade">
                            <i class="icon-remove"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>

<!-- modal detalhar atvidade -->
<div style="width:760px;left:20%;margin:0 auto;top:4%" id="detalhar-atividade" class="modal hide" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h2 style="color: #010101" id="myModalLabel">Detalhes <?php __('Atividade'); ?></h2>
    </div>

    <div class="modal-body">
        <div id="gifAtv"></div>

        <div style="display:none;" id="showDetalhes">
            <div class="row-fluid">

                <div class="span6">
                    <h4>Responsável</h4>
                    <span class="" class="center" id="detalheAtvResponsavel"><span>
                </div>

                <div class="span6">
                    <h4>Função</h4>
                    <span class="" id="detalheAtvFuncao"></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <h4>Nome da Atividade</h4>
                    <span class="" id="detalheAtvNomeAtividade"></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <h4>Descrição da Atividade</h4>
                    <span class="" id="detalheAtvDescricao"></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span4">
                    <h4>Periodicidade</h4>
                    <span class="" id="detalheAtvPeriodicidade"></span>
                </div>

                <div class="span4">
                    <h4>Início da Atividade</h4>
                    <span class="" id="detalheAtvIniAtividade"></span>
                </div>

                <div class="span4">
                    <h4>Término da Atividade</h4>
                    <span class="" id="detalheAtvFimAtividade"></span>
                </div>
            </div>

            <div clas="row-fluid">
                <div class="span4">
                    <h4>Dia para execução</h4>
                    <span class="" class="text-center" id="detalheAtvDiaExecucao"></span>
                </div>
            </div>
        </div>
    </div> <!-- modal body -->
    <div class="modal-footer">
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>


<!--  modal confirm -->
<div class="modal fade hide" id="confirm-delete-atividade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Excluir Atividade</h4>
            </div>

            <div class="modal-body">
                <strong>Tem certeza?</strong>
                <!--<p class="debug-url"></p>-->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-small" data-dismiss="modal">Fechar</button>
                <a id="ok-delete-atividade" class="btn btn-danger btn-ok btn-small">Ok</a>
            </div>
        </div>
    </div>
</div>

<script>
    $().ready(function () {
        // detalhar atividade
        $(document).on('click', '.modal-detalharAtividade', function () {
            event.preventDefault();
            var self = $(this);
            var coAtividade = $(this).data('id');

            $('#gifAtv').hasClass('hideGif') ? $('#gifAtv').hide() : $('#gifAtv').show();

            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'detalhar_atividade')); ?>/' + coAtividade,
                method: 'GET',
                dataType: 'json'
            })
                .done(function (resp) {
                    $('#gifAtv').addClass('hideGif').hide();
                    $('#showDetalhes').show();
                    $('#detalheAtvResponsavel').html(resp.Usuario.ds_nome || '-');
                    $('#detalheAtvFuncao').html(resp.Atividade.funcao_responsavel || '-');
                    $('#detalheAtvNomeAtividade').html(resp.Atividade.nome);
                    $('#detalheAtvDescricao').html(resp.Atividade.ds_atividade);
                    $('#detalheAtvPeriodicidade').html(resp.Atividade.periodicidade || '-');
                    $('#detalheAtvIniAtividade').html(resp.Atividade.dt_ini_planejado || '-');
                    $('#detalheAtvFimAtividade').html(resp.Atividade.dt_fim_planejado || '-');
                    $('#detalheAtvDiaExecucao').html(resp.Atividade.dia_execucao || '-');
                });
        });
    });
</script>


