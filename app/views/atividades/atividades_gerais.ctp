<div class="page-header position-relative"><h1><?php __('Atividades'); ?></h1></div>

<div class="row-fluid">

    <div class="clearfix" >
        <div class="span12">
            <div class="pull-left">
                <!--<a class="btn btn-primary btn-small btn-rounded" href="<?php //echo $this->Html->url(array ('controller' => 'atividades', 'action' => 'atividades_fiscalizacao')); ?>">Atividades</a>-->
            </div>
        </div>
    </div>

    <div class="left span9">
        <div class="span3"> <?php echo $this->Form->create('Atividade', array(
            'action' => 'atividades_gerais',
            'controller' => 'atividades',
            'class' => 'form-inline',
            'type' => 'GET',
            // 'style' => '-webkit-flex:1 1 auto; flex:1 1 auto;',
            'style' => 'width:235px;'
        )) ?>
            <button type="submit" title="Atividades pendentes são: Atividades que já se passaram o tempo para execução, porém não foram resolvidas." id="btnPendentes" style="height:85px;" class="alert-tooltip btn btn-default btn-large">
                <span style="font-size:2.6em">
                    <?php if (true) {
                        $c = $contagemPendentes;
                        if ($c < 10) {
                            echo '0';
                        }
                        echo $c;
                    }?>
                </span>
                <span style="font-size:2.1em">Pendentes</span>
            </button>
            <input type="hidden" name="filtro" value="pendentes" />
        <?php echo $this->Form->end() ?>
        </div>

        <div class="span3">
        <?php echo $this->Form->create('Atividade', array(
            'action' => 'atividades_gerais',
            'controller' => 'atividades',
            'type' => 'GET',
            'class' => 'form-inline',
            // 'style' => '-webkit-flex:1 1 auto; flex:1 1 auto;'
            'style' => 'width:235px'
        )) ?>
            <input type="hidden" name="filtro" value="andamentos" />
            <button id="btnAndamentos" style="height:85px;" title="Atividades em andamento são: Atividades que ainda não foram concluídas, mas ainda estão no prazo de execução." class="alert-tooltip btn btn-default btn-large">
                <span style="font-size:2.6em">
                    <?php if (true) {
                        $c = $contagemAndamentos;
                        if ($c < 10) {
                            echo '0';
                        }
                        echo $c;
                    }?>
                </span>
                <span style="font-size:1.9em">Andamentos</span>
            </button>
        <?php echo $this->Form->end() ?>
        </div>

        <div class="span3">
        <?php echo $this->Form->create('Atividade', array(
            'action' => 'atividades_gerais',
            'controller' => 'atividades',
            'type' => 'GET',
            'class' => 'form-inline',
            // 'style' => '-webkit-flex:1 1 auto; flex:1 1 auto;'
            'style' => "width:235px"
        )) ?>
            <input type="hidden" name="filtro" value="concluidos" />
            <button id="btnConcluidos" style="width:100%;height:85px" title="Atividades concluídas são: Atividades que foram 100% concluídas." class="alert-tooltip btn btn-default btn-large">
                <span style="display:block;font-size:2.6em">
                    <?php if (true) {
                        $c = $contagemConcluidos;
                        if ($c < 10) {
                            echo '0';
                        }
                        echo $c;
                    }?>
                </span>
                <span style="font-size:1.9em">Concluídos</span>
            </button>
        <?php echo $this->Form->end() ?>
        </div>
    </div>


<div style="margin-bottom:40px" class="clearfix"></div>

<p class='help-block'>
    Para efetuar a pesquisa é obrigatório preencher ao menos 1 campo
</p>

    <?php echo $this->Form->create('Atividade', array(
        'action' => 'atividades_gerais',
        'controller' => 'atividades',
        'type' => 'GET',
        'class' => 'form-inline',
        // 'style' => '-webkit-flex:1 1 auto; flex:1 1 auto'
    )) ?>

    <div class="span3">
        <select id="nuContrato" name="filtroPesquisa[contrato]" class="selectAtv input-xlarge chosen-select" data-placeholder="Nº Contrato">
            <option value="" selected="selected"></option>
            <?php foreach ($contratos as $index => $contrato) : ?>
                <option value="<?php echo $contrato['Atividade']['co_contrato'] ?>">
                    <?php echo $contrato['Contrato']['nu_contrato'] ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="span3">
        <select id="nomeResponsavel"  name="filtroPesquisa[responsavel]" class="selectAtv input-xlarge chosen-select" data-placeholder="Responsável">
                <option value="" selected="selected"></option>
                <?php foreach ($responsaveis as $index => $responsavel) : ?>
                    <option value="<?php echo $responsavel['a']['co_responsavel'] ?>"><?php echo $responsavel['u']['ds_nome'] ?></option>
                <?php endforeach ?>
        </select>
    </div>

    <div class="span3">
        <select id="periodicidade"  name="filtroPesquisa[periodicidade]" class="selectAtv input-large chosen-select" data-placeholder="Periodicidade">
            <option value="" selected="selected"></option>
            <?php foreach ($periodicidades as $periodicidade) : ?>
                <option value="<?php echo $periodicidade ?>"><?php echo $periodicidade ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="span3">
        <select id="diaExecucao" name="filtroPesquisa[dia_execucao]" class="selectAtv input-medium chosen-select" data-placeholder="Dia para execução">
            <option value="" selected="selected"></option>
            <?php foreach ($dias_execucao as $dia_execucao) : ?>
                <option value="<?php echo $dia_execucao ?>"><?php echo $dia_execucao ?></option>
            <?php endforeach ?>
        </select>
    </div>

    <br /><br />
    <button id="btnAtividadesGeraisPesquisar" class="btn btn-primary btn-small">Pesquisar</button>
    <?php if (count($atividades)) : ?>
        <button type="button" id="btnImprimir" class="btn btn-default btn-small" url="<?php echo $this->base . '/' . $url ?>">Imprimir</button>

    <?php endif ?>
    <?php echo $this->Form->end() ?>
</div>

<div style="margin-top:60px">
    <?php if (isset($atividades)) : ?>

    <!--<form class="form-search">
        <div class="row-fluid">
            <div class"span8">
                <input id="pesquisaAtividades" type="text" class="pesquisarAtividades input-xlarge search-query" placeholder="Pesquisar atividades" />
            </div>
        </div>
    </form>-->
    <?php
    echo $this->Form->create('atividade', array('style' => 'margin-top: -50px', 'type' => 'GET', 'controller' => 'atividades', 'action' => 'atividades_gerais'));

    // if ($filter == 'filtro') {
    //     echo "<input type=\"hidden\" name=\"filtro\" value=\"$queryStringImpressao\"";
    // } elseif ($filter == 'filtroPesquisas') {
    //     foreach ($queryStringImpressao as $name => $value) : ?>
            <!--<input type="hidden" name="filtroPesquisa[<?php echo $name ?>]" value="<?php //echo $value ?>" />-->

    <?php //endforeach; } ?>

    <input type="hidden" name="imprimir" value="pdf" />
    <span id="qs" style="display:none"><?php echo $queryString ?></span>


    <?php echo $this->Form->end() ?>

        <table cellpadding="0" cellspacing="0" border="0" style="width:100% !important" id="table-atividades-contratos-gerais" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Contrato</th>
                    <th>Responsável</th>
                    <th>Atividade</th>
                    <th>Periodicidade</th>
                    <th>Conclusão</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($atividades as $atividade) : ?>
                    <tr co_contrato="<?php echo $atividade['Contrato']['co_contrato']; ?>" id="<?php echo $atividade['Atividade']['co_atividade']; ?>">
                        <td data-html="true" data-rel="popover" data-trigger="hover" data-content="Gestor: <?php echo $atividade['Contrato']['GestorAtual']['ds_nome'] ?><br>Fornecedor: <?php echo $atividade['Contrato']['Fornecedor']['no_razao_social'] ?><br> Vigência: <?php echo $atividade['Contrato']['dt_fim_vigencia'] ?>" class="alert-tooltip">
                            <?php echo $this->Html->link($atividade['Contrato']['nu_contrato'], array(
                                'controller' => 'contratos',
                                'action' => 'detalha',
                                $atividade['Contrato']['co_contrato']
                            )) ?>
                        </td>
                        <td><?php echo !empty($atividade['Usuario']['ds_nome']) ? $atividade['Usuario']['ds_nome'] : '-' ?></td>
                        <td><?php echo $atividade['Atividade']['nome'] ?></td>
                        <td><?php echo !empty($atividade['Atividade']['periodicidade']) ? $atividade['Atividade']['periodicidade'] : '-' ?></td>
                        <td><?php
                            if ($atividade['Atividade']['pendencia']) : ?>
                                <!-- pendente -->
                                <a style="text-decoration:none;color:#101;font-size: 16px;" href="#">
                                    <i class="icon-exclamation-sign alert-tooltip" title="<?php echo $atividade['Atividade']['pendencia'] ?>" style="cursor:normal;display: inline-block;"></i>
                                </a>&nbsp;
                            <?php endif;
                            // $color = $atividade['Atividade']['pc_executado'] < 100 ? "#FAA732" : '#68BC31';
                            echo "<strong>{$atividade['Atividade']['pc_executado']}%</strong>" ?>
                        </td>
                        <td>
                            <!-- email -->
                            <a class="v_email" href="#view_email" data-toggle="modal">
                                <i class="icon-envelope icon-large alert-tooltip" title="Email" style="display: inline-block;color: #000000;position: relative;top: -4px;"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
</div>

<!-- modal impressão -->
<div id="imprimirModal" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <p class="pull-right">
        <a id="imprimirPdf" href="#imprimirPdf" class="btn btn-small btn-primary" ><i class="icon-print"></i> Imprimir</a>
        <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    </p>
    <h3 id="tituloAbaImpressoa">Impressão de Pesquisa</h3>
  </div>
    <div class="modal-body maior" id="impressao" styles="background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
    </div>
</div>

<!-- modal email -->
<div style="top:3%" id="view_email" class="modal hide maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Informações do Envio de E-mail</h3>
    </div>
    <div class="modal-body" id="add_email">
    </div>
</div>

<script>
     function checkInvalid() {
        var campos = [
            '#nuContrato',
            '#nomeResponsavel',
            '#periodicidade',
            '#diaExecucao'
        ];
        var count = 0;

         for (var i = 0; i < campos.length; i++) {
            //  if (campos[i] == '#periodicidade' && $(campos[i]).val() == 'Diariamente') {
            //  }
            
            if ($(campos[i]).text() == '' || $(campos[i]).text() == ' ') {
                count++;
            }
        }

        if (count) {
            $('#btnAtividadesGeraisPesquisar').prop('disabled', true);
        } else {
            $('#btnAtividadesGeraisPesquisar').prop('disabled', false);
        }
    }

    $().ready(function () {
        var dataTable;
        $('.alert-tooltip').tooltip();

        // $('.selectAtv + div.chosen-container').css('margin-right', '35px');

        $("#pesquisaAtividades").keyup(function() {
            $('#table-atividades-contratos-gerais').dataTable().fnFilter(this.value);
        });

        $('#btnAtividadesGeraisPesquisar').prop('disabled', true);


        $('#nuContrato, #periodicidade, #nomeResponsavel, #diaExecucao').chosen().change(function () {
            checkInvalid();
        });

         $('#table-atividades-contratos-gerais').DataTable({
            ordering: false,
            sorting: false,
            pageLength: 10,
            // info: true,
            bLengthChange: false,
            // pagingType: 'simple',
            language: {
                search: '<strong>Pesquisar</strong>:',
                zeroRecords: "Nehum resultado encontrato",
                infoFiltered: "",
                info: 'Mostrando _START_ - _END_ de _TOTAL_ registros',
                paginate: {
                    previous: '‹‹ Anterior',
                    next:     'Proximo ››'
                },
                // "info": "Showing page _PAGE_ of _PAGES_",
                infoEmpty: "Nenhum resultado encontrado",
            }
        });
        $('#table-atividades-contratos-gerais_filter').remove();
        $('#table-atividades-contratos-gerais_wrapper').find('div.row-fluid .span6:nth-child(1)').remove();
    });

    $('#imprimirPdf').click(function(){
        var frm = document.getElementById("iframe_imp_contrato").contentWindow;
        frm.focus();
        frm.print();
        return false;

    });

    function imprimir(url_imp) {
        $('#imprimirModal').modal();
        var queryString = $('#qs').html();

        $.ajax({
            type: 'POST',
            url:"<?php echo $this->base; ?>/relatorios/iframe_imprimir/",
            // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data:{
                "data[url]": url_imp  + queryString + '&imprimir=pdf',
                },
                success:function(result){
                    $('#impressao').html(result);
                }
            })
    }
    
    $('#table-atividades-contratos-gerais tbody tr td a.v_email').click(function() {
        var url_pg = "<?php echo $this->base; ?>/atividades/iframe/" + $(this).parents('tr').attr('co_contrato') + "/" + $(this).parents('tr').attr('id');
        $.ajax({
            type:"POST",
            url:url_pg,
            success:function(result){
                $('#add_email').html("");
                console.log(result);
                $('#add_email').append(result);
            }
        })
    })

    $('#btnImprimir').on('click', function (e) {
        e.preventDefault();
        imprimir($(this).attr('url'));
    });

    // $('#imprimir_pdf').click(function(){
    //      printElement(document.getElementById("impressao"));
    //      window.print();
    // });

    // prepara layout para impressão
    /*function printElement(elem, append, delimiter) {
        var domClone = elem.cloneNode(true);

        var $printSection = document.getElementById("printSection");

        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            document.body.appendChild($printSection);
        }

        if (append !== true) {
            $printSection.innerHTML = "";
        }

        else if (append === true) {
            if (typeof(delimiter) === "string") {
                $printSection.innerHTML += delimiter;
            }
            else if (typeof(delimiter) === "object") {
                $printSection.appendChlid(delimiter);
            }
        }

        $printSection.appendChild(domClone);
    }*/

</script>
