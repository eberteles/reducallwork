<?php echo $this->Html->script('inicia-datetimepicker') ?>
<div class="acoes-formulario-top clearfix">
    <div class="span4">
        <div class="pull-left btn-group">
            <a class="btn btn-primary btn-small btn-rounded"
               href="<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'atividades_fiscalizacao')) ?>">Atividades
                de fiscalização</a>
        </div>
    </div>
</div>

<div class="widget-header widget-header-small">
    <h4>Editar Atividade</h4>
</div>
<div class="widget-body">
    <div class="widget-main">
        <!--<input type="hidden" id="dt_fim_vigencia_atividade" value="<?php // echo $dt_fim_vigencia['Contrato']['dt_fim_vigencia'] ?>" /> -->
        <?php
        echo $this->Form->create('Atividade', array(
                'id' => 'formEditarAtividade',
                'action' => 'editar_atividade',
                'method' => 'POST',
                'class' => "form-horizontal")
        );
        echo $this->Form->hidden('co_atividade', array('value' => $atividade['Atividade']['co_atividade']));

        if ($this->Modulo->getHasNewFornecedores()) {
            echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
        }
        ?>

        <!-- Nome da atividade -->
        <div class="required" id="">
            <?php echo $this->Form->input('nome', array(
                'id' => 'nomeAtividade',
                'label' => 'Nome da atividade',
                'type' => 'text',
                'value' => $atividade['Atividade']['nome'],
                'class' => 'input-xxlarge control-labels'
            )) ?>
            <span id="message_nome_atividade" style="color: red;display:none"><b>Campo nome da atividade em branco </b></span>
        </div>

        <!-- Contrato -->
        <p class="help-block">Contratos</p>
        <fieldset style="width:545px !important">
            <!--<legend>&nbsp;Contratos</legend>-->
            <select name="data[Atividade][tipo]" id="selectContratos" class="input-medium">
                <option value="0" selected="selected">Específicos</option>
                <option value="1">Todos</option>
            </select>

            <select id="selectAtividadesContratos" name="data[Atividade][contratos][]"
                    data-placeholder="Selecione os contratos" class="input-xxlarge chosen-select" multiple="true">
                <?php foreach ($contratosIds as $id => $nuContrato) {
                    echo "<option value=\"$id\"";
                    for ($i = 0; $i < count($atividadesContratosIds); $i++) {
                        if ($atividadesContratosIds[$i]['AtividadeContrato']['id_contrato'] == $id) {
                            echo ' selected="selected" ';
                        }
                    }
                    echo ">$nuContrato</option>";
                } ?>
            </select>
        </fieldset>

        <!-- Descrição -->
        <div class="required control-groups" id="id_ds_atividade">
            <?php echo $this->Form->input('ds_atividade', array(
                'id' => 'ds_atividade',
                'label' => 'Descrição da Atividade',
                'class' => 'input-xxlarge',
                'type' => 'textarea',
                'maxlength' => 250,
                // 'cols' => 10,
                'rows' => 8,
                'value' => $atividade['Atividade']['ds_atividade']
            )); ?>
            <span id="message_atividade" style="color: red;display:none"><b>Campo descrição da atividade deve possuir entre 3 e 100 caracteres!</b></span>
        </div>

        <!-- Inicio da atividade -->
        <div class="required">
            <?php
            echo $this->Form->input('dt_ini_planejado', array(
                'id' => 'dt_ini_planejado',
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i class="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small',
                'label' => 'Início da atividade', 'type' => 'text',
                'value' => $atividade['Atividade']['dt_ini_planejado']));
            ?>
            <span id="message_dt_inicio_atividade" style="color: red;display:none"><b>Campo inicio da atividade em branco</b></span>
            <span id="message_dt_inicio_atividade2" style="color: red;display:none"><b>A data do inicio da atividade é maior que a data do fim do contrato</b></span>
        </div>


        <!-- fim da atividade -->
        <div class="" id="__id_dt_fim_planejado">
            <?php
            echo $this->Form->input('dt_fim_planejado', array(
                'id' => 'dt_fim_planejado',
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small',
                'label' => 'Término da atividade',
                'type' => 'text',
                'value' => $atividade['Atividade']['dt_fim_planejado']));
            ?>
            <span id="message_dt_fim_atividade" style="color: red;display:none"><b>A data do fim da atividade é maior que a data do fim do contrato</b></span>
        </div>

        <fieldset>
            <!-- Periodicidade -->
            <?php
            echo $this->Form->input('periodicidade', array(
                'type' => 'select',
                'id' => 'periodicidade',
                'class' => ' input-xlarge',
                'label' => 'Periodicidade',
                'empty' => 'Selecione...',
                'selected' => $atividade['Atividade']['periodicidade'],
                'options' => $periodicidadeArr
            ));
            ?>

            <!-- Dia para execução -->
            <?php
            echo $this->Form->input('dia_execucao', array(
                'type' => 'select',
                'id' => 'dia_execucao',
                'class' => 'span2 input-xlarge control-labels',
                'selected' => $atividade['Atividade']['dia_execucao'],
                'label' => 'Dia para execução',
                'empty' => 'Selecione...',
                'options' => $dia_execucaoArr
            ));
            ?>
        </fieldset>
        <div class="span12">
            <?php
            if ($this->Modulo->getHasNewFornecedores()) {
                echo $this->Form->input('ds_assunto_email', array('label' => 'Assunto (Para envio de email)'));
            }
            ?>
        </div>
        <div style="padding-left:8px" class="form-actions">
            <div style="float:left !important" class="btn-group">
                <button id="btnSalvar" rel="tooltip" type="submit" class="btn btn-small btn-primary"
                        title="Editar atividade">
                    Salvar
                </button>
                <!--<button rel="tooltip" type="reset" title="Limpar dados preenchidos"class="btn btn-small">
                   Limpar
               </button>-->
                <a href="<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'atividades_fiscalizacao')) ?>" title="Voltar" class="btn btn-small">
                    Voltar
                </a>
            </div>
        </div>
        </form>
    </div>
</div>
</div>

<script type="text/javascript">
    // verifica se existe algum campo inválido e desabilita o botão de salvar
    function checkInvalid() {
        var ids = [
            '#nomeAtividade',
            '#ds_atividade',
            '#dt_ini_planejado'
        ];
        var count = 0;

        for (var i = 0; i < ids.length; i++) {
            if (ids[i] == '#dt_ini_planejado') {
                if ($(ids[i]).val() == '__/__/____' || $(ids[i]).val() == '' || $(ids[i]).val() == ' ') {
                    count++;
                }

            } else if ($(ids[i]).val() == '' || $(ids[i]).val() == ' ') {
                count++;
            }
        }
        count ? $('#btnSalvar').attr('disabled', 'disabled') : $('#btnSalvar').removeAttr('disabled');
    }

    $(document).ready(function () {
        // periodicidade
        $("#periodicidade").on('input', function () {
            var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
            if (arr.indexOf($('#periodicidade').val()) != -1) {
                // console.log('disabled');
                $('#dia_execucao').attr('disabled', 'disabled');
            } else {
                // console.log('not disabled');
                $("#dia_execucao").removeAttr('disabled');
            }
        });

        // nome do responsável
        $('#co_responsavel').on('input', function () {
            if ($('#co_responsavel').val() == '' || $('#co_responsavel').val() == '') {
                $('#message_co_responsavel_atividade').show();
                checkInvalid();
            } else {
                $('#message_co_responsavel_atividade').hide();
                checkInvalid();
            }
        });

        // nome atividade
        $('#nomeAtividade').on('input blur', function () {
            if ($('#nomeAtividade').val() == '' || $('#nomeAtividade').val() == '') {
                $('#message_nome_atividade').show();
                checkInvalid();
            } else {
                $('#message_nome_atividade').hide();
                checkInvalid();
            }
        });


        // descrição da atividade
        $('#ds_atividade').on('input blur', function () {
            var tamanho = $('#ds_atividade').val().length;
            if (tamanho < 3 || tamanho > 100) {
                $('#message_atividade').show();
                checkInvalid();
            } else {
                $('#message_atividade').hide();
                checkInvalid();
                return false;
            }
        });
        // $('#dt_ini_planejado + span').hide();
        // data de inicio da atividade
        $('#dt_ini_planejado + span').on('click', function () {
            $('#dt_ini_planejado').focus();
        });
        $('#dt_ini_planejado').on('change input', function () {
            // onSelect: function () {

            var data_ini = $('#dt_ini_planejado').val();
            if (data_ini == "" || data_ini == " " || data_ini == '__/__/____') {
                $('#message_dt_inicio_atividade').show();
                checkInvalid();
            } else {
                $('#message_dt_inicio_atividade').hide();
                // var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'YYYY-MM-DD');
                // var dataInicioAtiviade = moment($('#dt_ini_planejamento').val(), 'DD/MM/YYYY');
                // if (dataInicioAtiviade.diff(dataVigenciaContrato, 'days') > 0) {
                //     $('#message_dt_fim_atividade2').show();
                // } else {
                // 	$('#message_dt_fim_atividade').hide();
                // }

                // console.log(dataEmpenho.diff(dataVigenciaContrato, 'days'));
                checkInvalid();
            }
        }).on('blur', function () {
            var data_ini = $('#dt_ini_planejado').val();
            if (data_ini == "" || data_ini == " " || data_ini == '__/__/____') {
                $('#message_dt_inicio_atividade').show();
                checkInvalid();
            } else {
                $('#message_dt_inicio_atividade').hide();
                // var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'YYYY-MM-DD');
                // var dataInicioAtiviade = moment($('#dt_ini_planejamento').val(), 'DD/MM/YYYY');
                // if (dataInicioAtiviade.diff(dataVigenciaContrato, 'days') > 0) {
                //     $('#message_dt_fim_atividade2').show();
                // } else {
                // 	$('#message_dt_fim_atividade').hide();
                // }

                // console.log(dataEmpenho.diff(dataVigenciaContrato, 'days'));
                checkInvalid();
            }
        });

        // data do fim da atividade
        $("#dt_fim_planejado").datepicker({
            onSelect: function () {
                var data_fim = $('#dt_fim_planejado').val();
                if (data_fim != "" || data_fim != " " || data_fim != '__/__/____') {
                    var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'YYYY-MM-DD');
                    var dataFimAtiviade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
                    if (dataFimAtiviade.diff(dataVigenciaContrato, 'days') > 0) {
                        $('#message_dt_inicio_atividade2').show();
                    } else {
                        $('#message_dt_inicio_atividade2').hide();
                    }
                }
            }
        }).on('blur', function () {
            var data_fim = $('#dt_fim_planejado').val();
            if (data_fim != "" || data_fim != " " || data_fim != '__/__/____') {
                var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'YYYY-MM-DD');
                var dataFimAtiviade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
                if (dataFimAtiviade.diff(dataVigenciaContrato, 'days') > 0) {
                    $('#message_dt_inicio_atividade2').show();
                } else {
                    $('#message_dt_inicio_atividade2').hide();
                }
            }
        });
    });
</script>