﻿<script type="text/javascript">
    //usado em tabela_execucao.js
    var baseUrlAtividadesController = '<?php echo $this->Html->url(array('controller' => 'atividades')); ?>';
    var baseUrlPagamentosController = '<?php echo $this->Html->url(array('controller' => 'pagamentos')); ?>';
</script>

<?php
$usuario = $this->Session->read('usuario');
echo $this->Html->script('inicia-datetimepicker');
echo $this->Html->script('tabela_execucao');
echo $this->Html->script('moment/moment.js');
?>
<input type="hidden" id="base_url_atv" value="<?php echo $this->base; ?>">
<input type="hidden" id="dt_fim_vigencia_atividade"
       value="<?php echo $dt_fim_vigencia['Contrato']['dt_fim_vigencia'] ?>"/>
<?php echo $this->Form->hidden('co_contrato', array('value' => $coContrato)); ?>

<div id="id_pg_execucao">
    <div class="row-fluid">
        <div class="padding" style="margin-left:4px">
            <label for="selAtvs">
                <select style="width:120px;text-align:left" class="input-small table" id="selAtvs">
                    <option id="fazer" value="N">A fazer</option>
                    <option id="conc" value="F">Concluídas</option>
                    <option id="todas" value="T">Todas</option>
                </select>
                <span id="gifLoadAtv"></span>
            </label>
        </div>

        <div id="boxTableAtv" class="row-fluid">
            <table class="table tree table-hover table-condensed" id="tabelaExecucao">
                <thead>
                <tr sstyle="background-color: #f9f9f9;">
                    <th colspan="1"><?php __('Atividade'); ?></th>
                    <?php if ($modulo->getAtividadesAvancado()) { ?>
                        <th>Periodicidade</th>
                    <?php } ?>
                    <th>Responsável</th>
                    <th colspan="1">Período Planejado</th>
                    <th colspan="1">Período Executado</th>
                    <th>Término</th>
                    <th>%</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                // atividades pais
                foreach ($atividades as $atividade) :
                    $class_tree = '';
                    if (isset($atividade['hasChildren']) && $atividade['hasChildren']) {
                        $class_tree = 'parent ';
                    }
                    if (isset($atividade['children']) && $atividade['children']) {
                        $class_tree .= "treegrid-" . $atividade['Atividade']['co_atividade'];
                    }
                    ?>
                    <tr class="<?php echo $class_tree; ?> <?php echo $this->Print->alertAtividade($atividade); ?>"
                        id="<?php echo $atividade['Atividade']['co_atividade']; ?>">
                        <!-- nome atividade -->
                        <td sstyle="word-wrap: break-word;min-width: 700px;max-width: 700px;white-space:normal;"
                            class="edit_descricao">
                            <?php if (isset($atividade['hasChildren']) && $atividade['hasChildren']) : ?>
                                <i class="treegrid-expander icon-chevron-right"></i>
                            <?php endif;
                            echo $this->Modulo->getAtividadesAvancado() ? $atividade['Atividade']['nome'] : $atividade['Atividade']['ds_atividade'] ?>
                        </td>

                        <?php if ($modulo->getAtividadesAvancado()) { ?>
                            <td>
                                <?php echo $atividade['Atividade']['periodicidade'] ? $atividade['Atividade']['periodicidade'] : '-' ?>
                            </td>
                        <?php } ?>

                        <!-- responsável -->
                        <td class="edit_responsavel" id="ini<?php echo $atividade['Atividade']['co_atividade'] ?>"
                            title="<?php echo $atividade['Atividade']['co_responsavel']; ?>">
                            <?php
                            // if (!isset($atividade['children']) || count($atividade['children']) == 0 ) {
                            // if ($atividade['Atividade']['tp_andamento'] != 'E') {
                            if ($atividade['Usuario']['ds_nome']) {
                                echo $this->Print->iconeAtividade($atividade['Atividade']['tp_andamento']);
                            }
                            // }
                            echo $atividade['Usuario']['ds_nome'] ? " {$atividade['Usuario']['ds_nome']}" : '-';
                            ?>
                        </td>

                        <!-- inicio planejado -->
                        <td>
                            <div class="alert-tooltip" title="">
                                <?php echo $atividade['Atividade']['dt_ini_planejado'] ? $atividade['Atividade']['dt_ini_planejado'] : '-' ?>
                                &nbsp;&nbsp;&nbsp;
                                <!--</div>-->
                                <!--</td>-->

                                <!-- fim planejado -->
                                <!--<td>-->
                                <!--<div class="alert-tooltip" title="Data Fim Planejado">-->
                                <?php echo !empty($atividade['Atividade']['dt_fim_planejado']) ? $atividade['Atividade']['dt_fim_planejado'] : '-' ?>
                            </div>
                        </td>

                        <!-- inicio executado -->
                        <td>
                            <div class="alert-tooltip" title="">
                                <?php echo $atividade['Atividade']['dt_ini_execucao'] ? ($atividade['Atividade']['dt_ini_execucao']) : '-';
                                echo '&nbsp;&nbsp;&nbsp;' ?>
                                <!--</div>-->
                                <!--</td>-->

                                <!-- fim executado -->
                                <!--<td> -->
                                <!-- <div class="alert-tooltip" title="Data Fim Executado">-->
                                <?php echo !empty($atividade['Atividade']['dt_fim_execucao']) ? ($atividade['Atividade']['dt_fim_execucao']) : '-' ?>
                            </div>
                        </td>

                        <!-- término -->
                        <td>
                            <?php echo $this->Print->prazoAtividade($atividade['Atividade']['tp_andamento'], $atividade['Atividade']['dt_fim_planejado']); ?>
                        </td>

                        <!-- porcentagem executada -->
                        <td <?php
                        $class;
                        if ($this->Modulo->getAtividadesAvancado()) {
                            $class = "modalEditarPercentual";
                        } else {
                            $class = 'edit_percentual';
                        }

                        if (!isset($atividade['children']) || count($atividade['children']) == 0) {
                            echo 'class="' . $class . '"';
                        } ?> title="<?php echo $atividade['Atividade']['pc_executado']; ?>"
                             data-id="<?php echo $atividade['Atividade']['co_atividade'] ?>"
                             pendencia="<?php echo isset($atividade['Atividade']['pendencia']) ? $atividade['Atividade']['pendencia'] : '' ?>">
                            <?php echo "{$atividade['Atividade']['pc_executado']}%";
                            ?>

                        </td>

                        <!--td><div id="slider" style="width: 100px;"></div></td-->

                        <!-- ações -->
                        <td sstyle="text-align:center">
                            <?php
                            if ($this->Modulo->getAtividadesAvancado()) {
                                if ($atividade['Atividade']['pendencia']) : ?>

                                    <!-- pendente -->
                                    <a id="atvPendenciaTooltip<?php echo $atividade['Atividade']['co_atividade'] ?>"
                                       class="alert-tooltip" title="<?php echo $atividade['Atividade']['pendencia'] ?>"
                                       onclick=""
                                       style="text-decoration:none;color:#101;font-size: 16px;cursor:pointer">
                                        <i class="icon-exclamation-sign"
                                           style="cursor:normal;display: inline-block;"></i>
                                    </a>
                                <?php endif; ?>

                                <!-- detalhar -->
                                <a href="#" style="text-decoration:none;color:#010;font-size: 20px;"
                                   data-id="<?php echo $atividade['Atividade']['co_atividade'] ?>"
                                   class="modal-detalharAtividade" data-target="#detalhar-atividade"
                                   data-toggle="modal">
                                    <i class="icon-eye-open alert-tooltip" title="Detalhar"
                                       style="display: inline-block;">
                                    </i>
                                </a>
                            <?php } ?>

                            <!-- pendência -->
                            <?php if (isset($atividade['Atividade']['ic_pendente']) && $atividade['Atividade']['ic_pendente'] == 1) { ?>
                                <a href="#" class="del_pendencia"><i
                                            class="icon-exclamation-sign red bigger-150 alert-tooltip"
                                            title="Atividade Pendente (Clique para tirar a Pendência)"
                                            style="display: inline-block;"></i></a>&nbsp;
                            <?php } else { ?>
                                <a href="#" class="apl_pendencia"><i class="icon-ok-circle bigger-150 alert-tooltip"
                                                                     title="Atividade Normal (Clique para aplicar a Pendência)"
                                                                     style="display: inline-block;"></i></a>&nbsp;
                            <?php } ?>

                            <!-- financeiro -->
                            <a class="v_financeiro" href="#view_financeiro" data-toggle="modal">
                                <i class="silk-icon-calculator alert-tooltip" title="Orçamentário/Financeiro"
                                   style="display: inline-block;"></i>
                            </a>&nbsp;

                            <!-- atesto da atividade -->
                            <!--                            <a class="v_atesto" href="#" id="atividade_atesto_-->
                            <?php //echo $atividade['Atividade']['co_atividade']
                            ?><!--" data-id="--><?php //echo $atividade['Atividade']['co_atividade']
                            ?><!--">-->
                            <!--                                --><?php //if($atividade['Atividade']['atividade_atestada'] == 0) {
                            ?>
                            <!--                                    <i class="icon-check alert-tooltip" title="Atestar Atividade" id="atesto_-->
                            <?php //echo $atividade['Atividade']['co_atividade']
                            ?><!--" style="display: inline-block;"></i>-->
                            <!--                                --><?php //} else {
                            ?>
                            <!--                                    <i class="icon-certificate alert-tooltip" title="Atividade Atestada" style="display: inline-block;"></i>-->
                            <!--                                --><?php //}
                            ?>
                            <!--                            </a>&nbsp;-->

                            <!-- anexo -->
                            <a class="v_anexo" href="#view_anexo" data-toggle="modal">
                                <i class="silk-icon-folder-table alert-tooltip" title="Suporte Documental"
                                   style="display: inline-block;"></i>
                            </a>&nbsp;

                            <!-- email -->
                            <a class="v_email" href="#view_email" data-toggle="modal">
                                <i class="icon-envelope icon-large alert-tooltip" title="Email"
                                   style="display: inline-block;color: #000000;position: relative;top: -4px;"></i>
                            </a>&nbsp;

                            <?php
                            if ($atividade['Atividade']['pc_executado'] != 100) {
                                ?>
                                <a class="del_atividade" href="#d_atividade">
                                    <i class="silk-icon-cross alert-tooltip" title="Excluir"
                                       style="display: inline-block;"></i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>

                    <!-- sub atividades -->
                    <?php
                    $i++;
                    if (isset($atividade['children']) && $atividade['children']) :
                        foreach ($atividade['children'] as $tarefa) :
                            $class_tree = "treegrid-parent-" . $tarefa['Atividade']['parent_id'];
                            ?>
                            <tr class="<?php echo $class_tree; ?> <?php $this->Print->alertAtividade($tarefa); ?>"
                                id="<?php echo $tarefa['Atividade']['co_atividade']; ?>">

                                <!-- nome -->
                                <td sstyle="word-wrap: break-word;min-width: 700px;max-width: 700px;white-space:normal"
                                    class="edit_descricao">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->Modulo->getAtividadesAvancado() ? $tarefa['Atividade']['nome'] : $tarefa['Atividade']['ds_atividade']; ?>
                                </td>

                                <?php if ($modulo->getAtividadesAvancado()) { ?>
                                    <td>
                                        <?php echo $tarefa['Atividade']['periodicidade'] ? $atividade['Atividade']['periodicidade'] : '-' ?>
                                    </td>
                                <?php } ?>

                                <!-- responsável -->
                                <td sstyle="text-align:center"
                                    id="ini<?php echo $tarefa['Atividade']['co_atividade'] ?>" class="edit_responsavel"
                                    title="<?php echo $tarefa['Atividade']['co_responsavel']; ?>">
                                    <?php echo !empty($tarefa['Usuario']['ds_nome']) ? $this->Print->iconeAtividade($tarefa['Atividade']['tp_andamento']) . ' ' . $tarefa['Usuario']['ds_nome'] : '-' ?>
                                </td>

                                <!-- inicio planejado -->
                                <td sstyle="text-align:center">
                                    <div class="alert-tooltip" title="">
                                        <?php echo !empty($tarefa['Atividade']['dt_ini_planejado']) ? $tarefa['Atividade']['dt_ini_planejado'] : '-' ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <!--</div>-->
                                        <!--</td> -->

                                        <!-- fim planejado -->
                                        <!--<td sstyle="text-align:center">-->
                                        <!--<div class="alert-tooltip" title="Data Fim Planejado">-->
                                        <?php echo $tarefa['Atividade']['dt_fim_planejado']; ?>
                                    </div>
                                </td>

                                <!-- inicio executado -->
                                <td sstyle="text-align:center">
                                    <div class="alert-tooltip" title="Data Início Executado">
                                        <?php echo $tarefa['Atividade']['dt_ini_execucao'] ? $tarefa['Atividade']['dt_ini_execucao'] : '-' ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <!--</div>-->
                                        <!--</td>-->

                                        <!-- fim executado -->
                                        <!--<td sstyle="text-align:center">-->
                                        <!--<div class="alert-tooltip" title="Data Fim Executado">-->
                                        <?php echo $tarefa['Atividade']['dt_fim_execucao'] ? $tarefa['Atividade']['dt_fim_execucao'] : '-' ?>
                                    </div>
                                </td>

                                <!-- término -->
                                <td>
                                    <?php echo $this->Print->prazoAtividade($tarefa['Atividade']['tp_andamento'], $tarefa['Atividade']['dt_fim_planejado']); ?>
                                </td>

                                <!-- porcentagem executada -->
                                <?php $class = $this->Modulo->getAtividadesAvancado() ? 'modalEditarPercentual' : 'edit_percentual'; ?>
                                <td sstyle="text-align:center"
                                    class="<?php echo $class ?>"
                                    title="<?php echo $tarefa['Atividade']['pc_executado']; ?>"
                                    data-id="<?php echo $tarefa['Atividade']['co_atividade'] ?>"
                                    pendencia="<?php echo isset($tarefa['Atividade']['pendencia']) ? $tarefa['Atividade']['pendencia'] : '' ?>"><?php echo $tarefa['Atividade']['pc_executado'] . '%' ?>
                                </td>

                                <!-- ações -->
                                <td sstyle="text-align: center;">
                                    <div class="float">
                                        <?php
                                        if ($this->Modulo->getAtividadesAvancado()) {
                                            if ($tarefa['Atividade']['pendencia']) : ?>
                                                <!-- pendente -->
                                                <a id="atvPendenciaTooltip<?php echo $tarefa['Atividade']['co_atividade'] ?>"
                                                   class="alert-tooltip"
                                                   title="<?php echo $tarefa['Atividade']['pendencia'] ?>"
                                                   data-placement="top" onclick=""
                                                   style="text-decoration:none;color:#101;font-size: 16px;cursor:pointer">
                                                    <i class="icon-exclamation-sign"
                                                       style="cursor:normal;display: inline-block;"></i>
                                                </a>&nbsp;
                                            <?php endif; ?>

                                            <!-- detalhar -->
                                            <a href="#" style="text-decoration:none;color:#010;font-size: 20px;"
                                               data-id="<?php echo $tarefa['Atividade']['co_atividade'] ?>"
                                               class="modal-detalharAtividade" data-target="#detalhar-atividade"
                                               data-toggle="modal">
                                                <i class="icon-eye-open" title="Detalha"
                                                   style="display: inline-block;"></i>
                                            </a>&nbsp;
                                        <?php } ?>

                                        <!-- pendência -->
                                        <?php if ($tarefa['Atividade']['ic_pendente'] == 1) { ?>
                                            <a class="del_pendencia"><i
                                                        class="icon-exclamation-sign red bigger-150 alert-tooltip"
                                                        title="Atividade Pendente (Clique para tirar a Pendência)"
                                                        style="display: inline-block;"></i></a>&nbsp;
                                        <?php } else { ?>
                                            <a class="apl_pendencia"><i class="icon-ok-circle bigger-150 alert-tooltip"
                                                                        title="Atividade Normal (Clique para aplicar a Pendência)"
                                                                        style="display: inline-block;"></i></a>&nbsp;
                                        <?php } ?>

                                        <!-- financeiro -->
                                        <a class="v_financeiro" href="#view_financeiro" data-toggle="modal">
                                            <i class="silk-icon-calculator alert-tooltip"
                                               title="Orçamentário/Financeiro" style="display: inline-block;"></i>
                                        </a>&nbsp;

                                        <!-- anexo -->
                                        <a class="v_anexo" href="#view_anexo" data-toggle="modal">
                                            <i class="silk-icon-folder-table alert-tooltip" title="Suporte Documental"
                                               style="display: inline-block;"></i>
                                        </a>&nbsp;

                                        <!-- email -->
                                        <!--<a class="v_email" href="#view_email" data-toggle="modal">
                                            <i class="ui-icon-mail-closed alert-tooltip" title="Email" style="display: inline-block;"></i>
                                        </a>&nbsp;-->

                                        <a class="v_email" href="#view_email" data-toggle="modal">
                                            <i class="icon-envelope icon-large alert-tooltip" title="Email"
                                               style="display: inline-block;color: #000000;position: relative;top: -4px;"></i>
                                        </a>&nbsp;

                                        <!-- deletar -->
                                        <a class="del_atividade" href="#d_atividade"><i
                                                    class="silk-icon-cross alert-tooltip" title="Excluir"
                                                    style="display: inline-block;"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;

                endforeach;
                ?>
                </tbody>
            </table>
            <?php //if ($this->Modulo->getAtividadesAvancado()) {  ?>
            <!--<div class="pagination pull-right" style="margin-right: 5px">
                <ul>
                    <?php //echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                    <?php //echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>
                    <?php //echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                </ul>
            </div>-->
            <?php //} ?>
            <div class="clearfix"></div>
            <div class="acoes-formulario-top clearfix">
                <p class="requiredLegend pull-left">
                    Selecione uma Ação<!--span class="required" title="Required">*</span-->
                </p>

                <div class="pull-right btn-group">
                    <a href="#add-atividade" id="bt-add-atividade" data-toggle="modal"
                       title="Adicionar <?php __('Atividade'); ?>" class="btn btn-small btn-primary">
                        <i class="icon-plus icon-white"></i>
                        <?php __('Atividade'); ?>
                    </a>
                </div>
            </div>


        </div>

    </div> <!-- row-fluid -->

</div> <!-- pg-execucao -->

<!-- id_percentual -->
<div style="display: none;">
    <select class="input-mini" id="id_percentual">
        <option value="0">0%</option>
        <option value="5">5%</option>
        <option value="10">10%</option>
        <option value="15">15%</option>
        <option value="20">20%</option>
        <option value="25">25%</option>
        <option value="30">30%</option>
        <option value="35">35%</option>
        <option value="40">40%</option>
        <option value="45">45%</option>
        <option value="50">50%</option>
        <option value="55">55%</option>
        <option value="60">60%</option>
        <option value="65">65%</option>
        <option value="70">70%</option>
        <option value="75">75%</option>
        <option value="80">80%</option>
        <option value="85">85%</option>
        <option value="90">90%</option>
        <option value="95">95%</option>
        <option value="100">100%</option>
    </select>
</div>

<!--
 <div id="dialog-confirm" title="Cadastro de Atividade" hidden="hidden">
     <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Já existe atividade cadastrada para esse responsável na data escolhida.<br />Deseja adicionar nova atividade?</p>
 </div>
 -->

<!-- modal alerta inicio atividade -->
<div style="top:3%" id="modalAlertaIniAtividade" class="modal hide" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Alerta</h3>
    </div>
    <div class="modal-body">
        <p style="font-size:14px">É necessário iniciar a atividade primeiro.</p>
    </div>
</div>

<!-- modal financeiro -->
<div style="top:3%" id="view_financeiro" class="modal hide maior" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Orçamentário/Financeiro</h3>
    </div>
    <div class="modal-body" id="atv_financeiro">
    </div>
</div>

<!-- modal email -->
<div style="top:3%" id="view_email" class="modal hide maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Informações do Envio de E-mail</h3>
    </div>
    <div class="modal-body" id="add_email">
    </div>
</div>

<!-- modal anexo -->
<div style="top:3%" id="view_anexo" class="modal hide maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental</h3>
    </div>
    <div class="modal-body" id="atv_anexo">
    </div>
</div>

<!-- modal detalhar atvidade -->
<div style="width:760px;left:20%;margin:0 auto;top:4%" id="detalhar-atividade" class="modal hide" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h2 style="color: #010101" id="myModalLabel">Detalhes <?php __('Atividade'); ?></h2>
    </div>

    <div id="modalBodyDetalharEditar" class="modal-body">
        <div id="gifAtv"></div>

        <div style="display:none;" id="showDetalhes">
            <div class="row-fluid">

                <div class="span6">
                    <h4>Responsável</h4>
                    <span class="center detalheAtvResponsavel" id=""><span>
                </div>

                <div class="span6">
                    <h4>Função</h4>
                    <span class="detalheAtvFuncao" id=""></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <h4>Nome da Atividade</h4>
                    <span class="detalheAtvNomeAtividade" id=""></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <h4>Descrição da Atividade</h4>
                    <span class="detalheAtvDescricao" id=""></span>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span4">
                    <h4>Periodicidade</h4>
                    <span class="" id="detalheAtvPeriodicidade"></span>
                </div>

                <div class="span4">
                    <h4>Início da Atividade</h4>
                    <span class="text-center" id="detalheAtvIniAtividade"></span>
                </div>

                <div class="span4">
                    <h4>Término da Atividade</h4>
                    <span class="text-center" id="detalheAtvFimAtividade"></span>
                </div>
            </div>

            <div clas="row-fluid">
                <div class="span4">
                    <h4>Dia para execução</h4>
                    <span class="" class="text-center" id="detalheAtvDiaExecucao"></span>
                </div>
            </div>
        </div>
    </div> <!-- modal body -->
    <div class="modal-footer">
        <button id="btnEditar" coAtividade="" class="btn btn-small btn-primary"><i class="icon-edit"></i>Editar</button>
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<!-- modal editar atividade -->
<div style="width:760px;left:20%;margin:0 auto;top:4%" id="editar-atividade" class="modal hide" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h2 style="color: #010101" id="myModalLabel">Editar <?php __('Atividade'); ?></h2>
    </div>

    <div style="display:none" id="showEditar" class="modal-body">
        <div class="control-groups">
            <form id="formEditAtividade">
                <div class="row-fluid">
                    <div class="span12">
                        <?php
                        // if($this->Modulo->getHasNewFornecedores()) {
                        //     echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
                        // } ?>
                        <!-- vincular atividade -->
                        <label for="vincularAtv">Vincular a Atividade</label>
                        <select id="" class="editAtvVinc input-xxlarge">
                        </select>
                    </div>

                    <div class="control-row">
                        <!-- Responsável -->
                        <div class="required span6" id="id_co_responsavel">
                            <label for="editAtvResp">Nome do responsável</label>
                            <select id="editAtvResp" class="editAtvResp input-xlarge">
                            </select><br/>
                            <span class="messageErrorAtv message_co_responsavel_atividade"
                                  style="color: red;display:none"><b>Campo nome do responsável em branco </b>
                            </span>
                        </div>

                        <!-- Função -->
                        <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="required span6" id="">
                                <label>Função</label>
                                <select class="select editAtvFunc input-medium">
                                    <option value="Gestor">Gestor</option>
                                    <option value="Fiscal Requisitante">Fiscal Requisitante</option>
                                    <option value="Fiscal Técnico">Fiscal Técnico</option>
                                    <option value="Fiscal Administrativo">Fiscal Administrativo</option>
                                    <option value="Aux. Fiscal Técnico">Aux. Fiscal Técnico</option>
                                    <option value="Aux. Fiscal Administrativo">Aux. Fiscal Administrativo</option>
                                </select>
                                <p><span class="messageErrorAtv message_funcaoResponsavel"
                                         style="color: red;display:none">
                                <b>Campo função em branco </b>
                            </span></p>
                            </div>
                        <?php endif; ?>
                    </div>

                    <!-- Nome da atividade -->
                    <?php
                    if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="required" id="">
                            <label for="ditNome">Nome: </label>
                            <input id="editNome" name="nome" class="input-xxlarge" value=""/>
                            <p><span class="messageErrorAtv message_nome_atividade" style="color: red;display:none">
                            <b>Campo nome da atividade em branco</b>
                        </span></p>
                        </div>
                    <?php endif; ?>

                    <!-- Descrição -->
                    <div class="required" id="id_ds_atividade">
                        <label>Descrição da Atividade</label>
                        <textarea id="editDesc" class="input-xlarge" rows="3" maxlength="250">
                        </textarea>
                        <span class="messageErrorAtv message_atividade" style="color: red;display:none"><b>Campo atividade deve possuir entre 3 e 100 caracteres!</b>
                        </span><br/>
                        <span class="messageErrorAtv message_atividade2" style="color: red;display:none"><b>Campo descrição em branco</b>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- modal body -->
    <div class="modal-footer">
        <button id="editar-concluir" coAtividade="" class="btn btn-small btn-primary">Salvar</button>
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i>Fechar</button>
    </div>
</div>

<!-- modal add atividade -->
<div style="width:578px !important;top:0.5% !important;" id="add-atividade" class="modal hide" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Adicionar <?php __('Atividade'); ?></h4>
    </div>

    <div style="overflow-y:hidden;max-height:600px" class="modal-body">
        <form id="formAddAtividade" class="form-horizontal">
            <div class="control-groups">
                <div class="row-fluid">
                    <div class="span12">
                        <?php
                        if ($this->Modulo->getHasNewFornecedores()) {
                            echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
                        }

                        // vincular atividade
                        echo $this->Form->input('parent_id', array(
                            'id' => 'parent_id',
                            'label' => 'Vincular a ' . __('Atividade', true),
                            'type' => 'select',
                            'empty' => 'Selecione...',
                            'class' => 'parent_id input-xxlarge',
                            'options' => $atividades_pai
                        ));
                        ?>
                    </div>

                    <!-- Responsável -->
                    <div class="controls-row">
                        <div class="required span4" id="id_co_responsavel">
                            <?php
                            echo $this->Form->input('co_responsavel', array(
                                'id' => 'co_responsavel',
                                'label' => 'Nome do Responsável',
                                'class' => 'co_responsavel input-medium',
                                'type' => 'select',
                                'options' => $usuarios,
                                'empty' => 'Selecione...'
                            )); ?>
                            <span class="messageErrorAtv message_co_responsavel_atividade"
                                  style="color: red;display:none"><b>Campo nome do responsável em branco </b>
                            </span>
                        </div>

                        <!-- Função -->
                        <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="required span4" id="">
                                <?php echo $this->Form->input('funcao_responsavel', array(
                                    'id' => 'funcao_responsavel',
                                    'label' => 'Função',
                                    'type' => 'select',
                                    'class' => 'funcao_responsavel input-medium',
                                    'options' => array(
                                        'Gestor' => 'Gestor',
                                        'Fiscal Requisitante' => 'Fiscal Requisitante',
                                        'Fiscal Técnico' => 'Fiscal Técnico',
                                        'Fiscal Administrativo' => 'Fiscal Administrativo',
                                        'Aux. Fiscal Técnico' => 'Aux. Fiscal Técnico',
                                        'Aux. Fiscal Administrativo' => 'Aux. Fiscal Administrativo'
                                    ),
                                    'empty' => 'Selecione...',
                                    'selected' => false
                                ))
                                ?>
                                <span class="messageErrorAtv message_funcaoResponsavel" style="color: red;display:none">
                                <b>Campo função em branco </b>
                            </span>
                            </div>
                        <?php endif; ?>
                    </div>

                    <!-- Nome da atividade -->
                    <?php
                    if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="required" id="">
                            <?php echo $this->Form->input('nome', array(
                                'id' => 'nomeAtividade',
                                'label' => 'Nome da atividade',
                                'type' => 'text',
                                'class' => 'nomeAtividade input-xxlarge'
                            )) ?>
                            <span class="messageErrorAtv message_nome_atividade" style="color: red;display:none"><b>Campo nome da atividade em branco </b>
                        </span>
                        </div>
                    <?php endif; ?>

                    <!-- Descrição -->
                    <div class="required" id="id_ds_atividade">
                        <?php echo $this->Form->input('ds_atividade', array(
                            'id' => 'ds_atividade',
                            'label' => 'Descrição da Atividade',
                            'type' => 'textarea',
                            'rows' => 3,
                            'class' => 'ds_atividade input-xxlarge',
                            'maxlength' => 250
                        )); ?>
                        <span class="messageErrorAtv message_atividade" style="color: red;display:none"><b>Campo atividade deve possuir entre 3 e 100 caracteres!</b>
                        </span><br/>
                        <span class="messageErrorAtv message_atividade2" style="color: red;display:none"><b>Campo descrição em branco</b>
                        </span>
                    </div>

                    <!-- Inicio da atividade -->
                    <div style="" class="controls-row">
                        <div class="required span5">
                            <?php
                            echo $this->Form->input('dt_ini_planejado', array(
                                'id' => 'dt_ini_planejado',
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                'class' => 'dt_ini_planejado input-small',
                                'label' => 'Início da atividade',
                                'type' => 'text'));
                            ?>

                            <span class="messageErrorAtv" id="message_dt_inicio_atividade"
                                  style="color: red;display:none"><b>Campo inicio da atividade em branco</b>
                            </span><br/>

                            <span class="messageErrorAtv" id="message_dt_inicio_atividade2"
                                  style="color: red;display:none"><b>A data do inicio da atividade é maior que a data do fim do contrato</b>
                            </span>
                        </div>

                        <!-- fim da atividade -->
                        <div class="span5" id="_id_dt_fim_planejado">
                            <?php
                            echo $this->Form->input('dt_fim_planejado', array(
                                'id' => 'dt_fim_planejado',
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                'class' => 'dt_fim_planejado input-small',
                                'label' => 'Término da atividade',
                                'type' => 'text'));
                            ?>
                            <span class="messageErrorAtv" id="message_dt_fim_atividade0"
                                  style="color: red;display:none"><b>Campo fim da atividade em branco</b></span>
                            <span class="messageErrorAtv" id="message_dt_fim_atividade" style="color: red;display:none"><b>A data do término da atividade é maior que a data do fim do contrato</b><br/></span>
                            <span class="messageErrorAtv" id="message_dt_fim_atividade2"
                                  style="color: red;display:none"><b>A data do término da atividade é menor que a data do início da atividade</b></span>
                        </div>
                    </div>

                    <!-- Periodicidade -->
                    <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="control-rows">
                            <div class="required span5" id="">
                                <?php
                                echo $this->Form->input('periodicidade', array(
                                    'type' => 'select',
                                    'id' => 'periodicidade',
                                    'class' => 'input-large',
                                    'label' => 'Periodicidade',
                                    'empty' => 'Selecione...',
                                    'selected' => false,
                                    'options' => array(
                                        'Diariamente' => 'Diariamente',
                                        'Semanal' => 'Semanal',
                                        'Quinzenal' => 'Quinzenal',
                                        'Mensal' => 'Mensal',
                                        'Bimestral' => 'Bimestral',
                                        'Trimestral' => 'Trimestral',
                                        'Semestral' => 'Semestral',
                                        'Anual' => 'Anual',
                                        'Quando houver necessidade' => 'Quando houver necessidade',
                                        'Na solicitação' => 'Na solicitação'
                                    )
                                ));
                                ?>
                                <span class="messageErrorAtv" id="message_periodicidade_atividade"
                                      style="color: red;display:none"><b>Campo periodicidade em branco</b></span>
                            </div>

                            <!-- Dia para execução -->
                            <div class="required span6" style="">
                                <?php
                                echo $this->Form->input('dia_execucao', array(
                                    'type' => 'select',
                                    'id' => 'dia_execucao',
                                    'class' => 'input-large',
                                    'label' => 'Dia para execução',
                                    'empty' => 'Selecione...',
                                    'options' => array(
                                        'Segunda' => 'Segunda',
                                        'Terça' => 'Terça',
                                        'Quarta' => 'Quarta',
                                        'Quinta' => 'Quinta',
                                        'Sexta' => 'Sexta',
                                        'Quando Possível' => 'Quando Possível'
                                    )
                                ));
                                ?>

                                <span class="messageErrorAtv" id="message_dia_execucao_atividade"
                                      style="color: red;display:none"><b>Campo dia para execução em branco</b></span>
                            </div>

                        </div>
                    <?php endif ?>

                    <?php
                    if ($this->Modulo->getHasNewFornecedores()) {
                        echo '<div class="span6">';
                        echo $this->Form->input('ds_assunto_email', array('label' => 'Assunto (Para envio de email)'));
                        echo '</div>';
                    }
                    ?>

                </div>
            </div>
        </form>
    </div> <!-- modal body -->

    <div class="modal-footer">
        <button class="btn btn-small btn-primary" data-dismiss="modal" id="add_concluir"><i
                    class="icon-ok icon-white"></i> Concluir
        </button>
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<!-- modal editar porcentagem -->
<div style="width:580px" class="modal fade hide" id="editar-porcentagem" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Atividade</h4>
            </div>

            <div class="modal-body">
                <div class="row-fuid">
                    <label for="atvPorcentagem">Porcentagem</label>
                    <div class="">
                        <select class="input-small" id="atvPorcentagem">
                            <option value="0">0%</option>
                            <option value="5">5%</option>
                            <option value="10">10%</option>
                            <option value="15">15%</option>
                            <option value="20">20%</option>
                            <option value="25">25%</option>
                            <option value="30">30%</option>
                            <option value="35">35%</option>
                            <option value="40">40%</option>
                            <option value="45">45%</option>
                            <option value="50">50%</option>
                            <option value="55">55%</option>
                            <option value="60">60%</option>
                            <option value="65">65%</option>
                            <option value="70">70%</option>
                            <option value="75">75%</option>
                            <option value="80">80%</option>
                            <option value="85">85%</option>
                            <option value="90">90%</option>
                            <option value="95">95%</option>
                            <option value="100">100%</option>
                        </select>
                    </div>

                    <div class="required">
                        <?php echo $this->Form->input('pendencia', array(
                            'id' => 'atvPendencia',
                            'label' => 'Pendência',
                            'type' => 'textarea',
                            'rows' => 3,
                            'class' => 'input-xxlarge',
                            'maxlength' => 250
                        )); ?>
                        <span class="messageErrorAtv" id="message_pendencia_required"
                              style="color: red;display:none"><b>Pendência em branco</b>
                            </span>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-small" data-dismiss="modal">Fechar</button>
                <a id="ok-editar-porcentagem" class="btn btn-danger btn-ok btn-small">Salvar</a>
            </div>
        </div>
    </div>
</div></div>

<!-- modal iniciar atividade -->
<div style="top:12%" id="modalIniAtividade" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Iniciar Atividade</h3>
    </div>

    <div class="modal-body">
        <p style="font-size:14px">Deseja iniciar a atividade ?</p>
    </div>

    <div class="modal-footer">
        <a href="#" id="ok-ini-atividade" class="btn btn-primary btn-small">Sim</a>
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Não</a>
    </div>
</div>

<!-- modal alert periodicidade -->
<div style="top:15%" id="modalAlertPeriodicidade" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Alerta</h3>
    </div>

    <div class="modal-body">
        <p>A periodicidade é maior que a data prazo da atividade</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Fechar</a>
    </div>
</div>


<!-- modal alert inicio da atividade -->
<div style="top:15%" id="modalAlertDataInicio" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Alerta</h3>
    </div>

    <div class="modal-body">
        <p>A data de início da atividade é inferior a data de hoje</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Fechar</a>
    </div>
</div>

<script type="text/javascript">
    $('#tabelaExecucao tr.parent td.edit_descricao').on('click', function loadChildren(e) {
        var tr = $(e.target).closest('tr'),
            parentId = tr.attr('id'),
            tpAndamento = $('#selAtvs').val();

        if (!tr.hasClass('loaded')) {
            $(this).find('i').removeClass('icon-chevron-right');
            $(this).find('i').addClass('gifLoadAtv-small');
            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index'));?>/' + $('#co_contrato').val() + '/' + tpAndamento + '/' + parentId,
                method: 'GET',
                success: function (data) {
                    $('#collapseExec').html(data);
                    selectFiltro(tpAndamento);
                    $(this).find('i').removeClass('gifLoadAtv-small');
                    $('#' + parentId).addClass('loaded').treegrid('expand');
                }
            });
        }
    });

    function selectFiltro(tpAndamento) {
        switch (tpAndamento) {
            case 'F':
                $('#conc').prop('selected', true);
                break;
            case 'N':
                $('#fazer').prop('selected', true);
                break;
            case 'T':
                $('#todas').prop('selected', true);
                break;
        }
    }

    function loadAtividades(tpAndamento) {
        $.ajax({
            method: 'GET',
            url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index'));?>/' + $('#co_contrato').val() + '/' + tpAndamento,
            success: function (data) {
                $('#collapseExec').html(data);
                selectFiltro(tpAndamento);

        }, error:function (error) {
            console.err(error);
        }
    });

    }

    $(document).ready(function () {
        $('.alert-tooltip').tooltip();

        $('.v_atesto').click(function (event) {
            event.preventDefault();
            var self = $(this),
                coAtividade = self.data('id'),
                atestoId = '#atesto_' + coAtividade,
                atvAtestoId = '#atividade_atesto_' + coAtividade;

            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'atestarAtividade'));?>/' + coAtividade,
                method: 'POST',
                data: {}
            }).success(function (data) {
                $(atestoId).remove();
                $(atvAtestoId).append("<i class='icon-certificate alert-tooltip' title='Atividade Atestada' style='display: inline-block;'></i>");
            });
        });

        // editar percentual
        $("#tabelaExecucao .modalEditarPercentual").on('dblclick', function (e) {
            $('#editar-porcentagem').modal();
            var self = $(this),
                coAtividade = self.data('id'),
                target = $(e.target),
                porcentagem = $.trim($(e.target).html().toString()),
                porcentagem = porcentagem.substring(0, porcentagem.length - 1);

            if ($('#ini' + coAtividade + ' a > i').hasClass('ui-icon-control-play-blue')) {
                // $('#modalAlertaIniAtividade').modal('show');
            }

            if (porcentagem == 100) {
                $('#atvPendencia').prop('disabled', true);
            } else {
                $('#atvPendencia').prop('disabled', false);
            }

            $("#atvPorcentagem").on('change', function (e) {
                if ($(this).val() == 100) {
                    $('#atvPendencia').prop('disabled', true);
                } else {
                    $('#atvPendencia').prop('disabled', false);
                }
            });

            $('#atvPorcentagem option[value="' + porcentagem + '"]').prop('selected', true);
            $('#atvPendencia').val($(e.target).attr('pendencia'));

            $("#ok-editar-porcentagem").on('click', function () {
                if ($('#atvPorcentagem').val() < 100 && $('#atvPendencia').val() == '' || $('#atvPendencia').val() == ' ') {
                    $('#message_pendencia_required').show();
                    return;
                }

                $.ajax({
                    url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index'));?>/' + $('#co_contrato').val(),
                    method: 'POST',
                    data: {
                        "data[edit_percentual]": "1",
                        "data[co_atividade]": coAtividade,
                        "data[pc_executado]": $('#atvPorcentagem').val(),
                        "data[pendencia]": $('#atvPorcentagem').val() == 100 ? null : $('#atvPendencia').val()
                    }
                }).success(function (data) {
                    // data = JSON.parse(data);
                    $('#id_pg_execucao').html("");
                    $('#id_pg_execucao').append(data);
                    // $(e.target).html(porcentagem);
                    // $('#atvPendenciaTooltip' + coAtividade).attr('title', $('#atvPendencia').val());
                    // location.reload(location.href + '/atividade#collapseExec');
                });

                $('#editar-porcentagem').modal('hide');
            });
        });

        // treegrid
        $('.tree').treegrid({
            'initialState': 'collapsed',
            // initialState: 'expanded',
            'saveState': true,
            // onExpand: function () {
            //     console.log('ok');
            // },
            treeColumn: 0,
        });

        // datable tabela de atividades
        $('#tabelaExecucao').DataTable({
            ordering: false,
            sorting: false,
            pageLength: 10,
            info: true,
            paging: false,
            bLengthChange: false,
            // pagingType: 'simple',
            language: {
                // "lengthMenu": "Display _MENU_ records per page",
                zeroRecords: "Nenhum resultado encontrado",
                infoEmpty: "",
                infoFiltered: "",
                search: '<strong>Pesquisar </strong>:',
                // paginate: {
                //     previous: '‹‹ Anterior',
                //     next:     'Proximo ››'
                // },
                info: 'Mostrando _START_ - _END_ de _TOTAL_ registros'
            },
            fnDrawCallback: function () {
                $('#selAtvs').appendTo($('#tabelaExecucao_wrapper').find('div.row-fluid').first().find('.span6').first());
                $('#tabelaExecucao tr.parent td.edit_descricao').find('i').addClass('icon-chevron-right');
            }
        });
        // $('#tabelaExecucao').DataTable().column(6).search('^(?!100%)\(\).*$', true, true).draw();


        var trNodes = $('#tabelaExecucao');
        $('#tabelaExecucao_filter').css('text-align', 'right');
        $('#tabelaExecucao_filter').on('input', function () {
            if ($(this).find('input').val()) {
                setTimeout(function () {
                    trNodes.treegrid('expandAll');
                }, 0);
            } else {
                trNodes.treegrid('collapseAll');
            }
        });


        $('#selAtvs').on('change', function () {
            var trNodes = $('#tabelaExecucao');
            // trNodes.treegrid('expandAll');

            if ($(this).val() == 'N') {
                // $('#tabelaExecucao').DataTable().column(6).search('^(?!100%)\(\).*$', true, true).draw();
                loadAtividades('N');
            } else if ($(this).val() == 'F') {
                // $('#tabelaExecucao').DataTable().column(6).search('100%', false, false).draw();
                loadAtividades('F');
            } else if ($(this).val() == 'T') {
                // $('#tabelaExecucao').DataTable().column(6).search('').draw();
                loadAtividades('T');
            }
            // trNodes.treegrid('collapseAll');
        });

        // modal editar porcentagem
        $('#editar-porcentagem').on('hidden.bs.modal', function () {
            $('#message_pendencia_required').hide();
        });

        $('#parent_id').on('input', function () {
            if ($(this).val()) {
                $('#message_dia_execucao_atividade').hide();
                $('#message_periodicidade_atividade').hide();
                $("#dia_execucao").prop('disabled', true);
                $("#periodicidade").prop('disabled', true);
            } else {
                var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
                $("#periodicidade").prop('disabled', false);
                if (arr.indexOf($('#periodicidade').val()) > -1) {
                    $("#dia_execucao").prop('disabled', true);

                } else {
                    $("#dia_execucao").prop('disabled', false);
                }
            }

        });

        // periodicidade
        $("#periodicidade").on('input', function () {
            var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
            if (arr.indexOf($('#periodicidade').val()) != -1) {
                $('#message_dia_execucao_atividade').hide();
                $('#dia_execucao').prop('disabled', true);
                $('#dia_execucao option:first').prop('selected', true);
            } else {
                $('#dia_execucao').prop('disabled', false);
            }

            if ($(this).val() == '') {
                $('#message_periodicidade_atividade').show();
            } else {
                $('#message_periodicidade_atividade').hide();
            }
        });

        $('#dia_execucao').on('input', function () {
            if ($(this).val() == '') {
                $('#message_dia_execucao_atividade').show();
            } else {
                $('#message_dia_execucao_atividade').hide();
            }
        });

        // nome do responsável
        $('.co_responsavel').on('input', function () {
            if ($('.co_responsavel').val() == '' || $('.co_responsavel').val() == '') {
                $('.message_co_responsavel_atividade').show();
            } else {
                $('.message_co_responsavel_atividade').hide();
            }
        });

        // função
        $('.funcao_responsavel').on('input', function () {
            if ($('.funcao_responsavel').val() == '' || $('.funcao_responsavel').val() == '') {
                $('.add_concluir').attr('disabled', 'disabled');
                $('.message_funcaoResponsavel').show();
            } else {
                $('.message_funcaoResponsavel').hide();
            }
        });

        // nome atividade
        $('.nomeAtividade').on('input blur', function () {
            if ($('.nomeAtividade').val() == '' || $('.nomeAtividade').val() == '') {
                $('.message_nome_atividade').show();
            } else {
                $('.message_nome_atividade').hide();
            }
        });

        $('#add-atividade, #editar-atividade').on('hidden.bs.modal', function (e) {
            $(this)
                .find("input,textarea,select")
                .val('');
            $('#message_dt_inicio_atividade').hide();
            $('#message_dt_inicio_atividade2').hide();
            $('.message_co_responsavel_atividade').hide();
            $('.message_funcaoResponsavel').hide();
            $('.message_nome_atividade').hide();
            $('.message_atividade').hide();
            $('.message_atividade2').hide();
            $('#message_dt_fim_atividade2').hide();
            $('#message_dt_fim_atividade').hide();
            $('#message_dt_fim_atividade0').hide();
            $('#message_periodicidade_atividade').hide();
            $('#message_dia_execucao_atividade').hide();

        });

        // editar atividade
        $('#btnEditar').on('click', function () {
            $('#detalhar-atividade').modal('hide');
            $('#editar-atividade').modal();
            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'detalhar_atividade'));?>/' + $('#btnEditar').attr('coAtividade') + '/' + $('#co_contrato').val(),
                method: 'GET',
                dataType: 'json'
            })
                .done(function (resp) {
                    var optionResponsaveis = '', optionFuncoes = '';
                    $(".editAtvResp option").remove();
                    $(".editAtvVinc option").remove();

                    for (var coUsuario in resp.Usuarios) {
                        if (coUsuario == resp[0].Atividade.co_responsavel) {
                            optionResponsaveis = '<option selected value=' + coUsuario + '>' + resp.Usuarios[coUsuario] + '</option>';
                        } else {
                            optionResponsaveis = '<option value=' + coUsuario + '>' + resp.Usuarios[coUsuario] + '</option>';
                        }

                        $(".editAtvResp").append(optionResponsaveis);
                    }

                    var countVinc = false;
                    var optionVinc = '';

                    if ($('#tabelaExecucao .treegrid-' + $('#btnEditar').attr('coAtividade')).length < 1) {
                        $(".editAtvVinc").attr('disabled', false);
                        $(".editAtvVinc").toggleClass('disabled');

                        $(".editAtvVinc").append('<option value="0">Selecione...</option>');
                        for (var parentId in resp.AtividadesPais) {
                            if (resp[0].Atividade.parent_id == parentId) {
                                optionVinc = '<option  selected value=' + parentId + '>' + resp.AtividadesPais[parentId] + '</option>';
                                countVinc = true;
                            } else {
                                optionVinc = '<option  value=' + parentId + '>' + resp.AtividadesPais[parentId] + '</option>';
                            }

                            $(".editAtvVinc").append(optionVinc);
                        }

                        if (!countVinc) {
                            $(".editAtvVinc option:first").attr('selected', true)
                        }
                    } else {
                        $(".editAtvVinc").attr('disabled', true);
                        $(".editAtvVinc").toggleClass('disabled');
                    }

                    $('.editAtvFunc option').each(function (index, self) {
                        if (self.getAttribute('value') == resp[0].Atividade.funcao_responsavel) {
                            $(self).attr('selected', true);
                        }
                    });

                    $('#gifAtv').addClass('hideGif').hide();

                    $('#showEditar').show();

                    $('#editNome').val(resp[0].Atividade.nome);

                    $('#editDesc').val(resp[0].Atividade.ds_atividade);
                });

        });

        // detalhar atividade
        $(document).on('click', '.modal-detalharAtividade', function () {
            var self = $(this);
            var coAtividade = $(this).data('id');
            var coContrato = $('#co_contrato').val();

            $('#editar-concluir').attr('coAtividade', coAtividade);
            $('#btnEditar').attr('coAtividade', coAtividade);

            $('#gifAtv').hasClass('hideGif') ? $('#gifAtv').hide() : $('#gifAtv').show();

            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'detalhar_atividade'));?>/' + coAtividade + '/' + coContrato,
                method: 'GET',
                dataType: 'json'
            })
                .done(function (resp) {
                    $('#gifAtv').addClass('hideGif').hide();
                    $('#showDetalhes').show();
                    $('.detalheAtvResponsavel').html(resp[0].Usuario.ds_nome || '-');
                    $('.detalheAtvFuncao').html(resp[0].Atividade.funcao_responsavel || '-');
                    $('.detalheAtvNomeAtividade').html(resp[0].Atividade.nome);
                    $('.detalheAtvDescricao').html(resp[0].Atividade.ds_atividade);
                    $('#detalheAtvPeriodicidade').html(resp[0].Atividade.periodicidade || '-');
                    $('#detalheAtvIniAtividade').html(resp[0].Atividade.dt_ini_planejado || '-');
                    $('#detalheAtvFimAtividade').html(resp[0].Atividade.dt_fim_planejado || '-');
                    $('#detalheAtvDiaExecucao').html(resp[0].Atividade.dia_execucao || '-');
                });
        });

        $(document).on('click', '#bt-add-atividade', function () {
            var selectResponsaveis = $('#co_responsavel'),
                optionResponsaveis = $('#co_responsavel option');

            $.ajax({
                url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'responsaveis'));?>/' + $('#co_contrato').val(),
                method: 'get',
                dataType: 'json',
                success: function (data) {
                    $('#co_responsavel option').remove();
                    selectResponsaveis.append('<option value>Selecione</option>');
                    for (var coUsuario in data) {
                        selectResponsaveis.append('<option value="' + coUsuario + '">' + data[coUsuario] + '</option>');
                    }
                },
                error: function (error) {
                    $('#co_responsavel option').remove();
                    selectResponsaveis.append(optionResponsaveis);
                }
            });
        });

        // descrição da atividade
        $('.ds_atividade').on('input blur', function () {
            var tamanho = $('.ds_atividade').val().length;
            if (tamanho < 3 || tamanho > 100) {
                $('.message_atividade').show();
            } else {
                $('.message_atividade').hide();
                $('.message_atividade2').hide();
            }

            if ($('.ds_atividade').val() == '' || $('.ds_atividade').val() == ' ') {
                $('.message_atividade2').show();
            } else {
                $('.message_atividade2').hide();
            }
        });

        $('#dt_ini_planejado + span').on('click', function () {
            $('#dt_ini_planejado').focus();
        });

        // data de inicio da atividade
        $('#dt_ini_planejado').on('blur', function () {
            var data_ini = $('#dt_ini_planejado').val();
            if (data_ini == "" || data_ini == " " || data_ini == '__/__/____') {
                $('#message_dt_inicio_atividade').show();
                $('#add_concluir').prop('disabled', true);
            } else {
                $('#add_concluir').prop('disabled', false);
                $('#message_dt_inicio_atividade').hide();

                var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
                var dataInicioAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');
                var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');

                if (dataInicioAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                    $('#message_dt_inicio_atividade2').show();
                    $('#add_concluir').prop('disabled', true);

                    if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                        $('#add_concluir').prop('disabled', true);
                        $('#message_dt_fim_atividade').show();
                    }
                    return;
                } else {
                    $('#message_dt_inicio_atividade2').hide();
                    $('#add_concluir').prop('disabled', false);
                }

                if (dataFimAtividade.diff(dataInicioAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
                    $('#message_dt_fim_atividade2').show();
                    $('#add_concluir').prop('disabled', true);
                } else {
                    $('#add_concluir').prop('disabled', false);
                    $('#message_dt_fim_atividade2').hide();
                }
            }
        });

        $('#dt_fim_planejado + span').on('click', function () {
            $('#dt_fim_planejado').focus();
        });

        // data do fim da atividade
        $("#dt_fim_planejado").on('blur', function () {
            var data_fim = $('#dt_fim_planejado').val();

            // if (data_fim == "" || data_fim == " " || data_fim == '__/__/____')  {
            //       $('#message_dt_fim_atividade0').show();
            //       $('#add_concluir').prop('disabled', true);
            // } else {
            //       $('#add_concluir').prop('disabled', false);
            //       $('#message_dt_fim_atividade0').hide();
            // }

            if (data_fim != "" || data_fim != " " || data_fim != '__/__/____') {
                var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
                var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
                var dataIniAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');


                if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                    $('#message_dt_fim_atividade').show();
                    $('#add_concluir').prop('disabled', true);
                    return;
                } else {
                    $('#message_dt_fim_atividade').hide();
                    $('#add_concluir').prop('disabled', false);
                }

                if (dataFimAtividade.diff(dataIniAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
                    $('#message_dt_fim_atividade2').show();
                    $('#add_concluir').prop('disabled', true);
                } else {
                    $('#message_dt_fim_atividade2').hide();
                    $('#add_concluir').prop('disabled', false);
                }
            }
        });

        // update atividade
        $('#editar-concluir').on('click', function () {
            // $('#message_dt_inicio_atividade').hide();
            // $('.message_co_responsavel_atividade').hide();
            // $('.message_funcaoResponsavel').hide();
            // $('.message_nome_atividade').hide();
            // $('.message_atividade').hide();
            // $('.message_atividade2').hide();
            // $('#message_periodicidade_atividade').hide();
            // $('#message_dia_execucao_atividade').hide();
            // $('#message_dt_fim_atividade0').hide();

            var validacao = false;

            $('#co_responsavel option').remove();

            // responsável
            if ($('#editAtvResp').val() == '' || $('#editAtvResp').val() == '') {
                $('.message_co_responsavel_atividade').show();
                validacao = true;
            }

            // função
            if ($('.editAtvFunc').val() == '' || $('.editAtvFunc').val() == ' ') {
                $('.message_funcaoResponsavel').show();
                validacao = true;
            }

            // nome atividade
            if ($('#editNome').val() == '') {
                $('.message_nome_atividade').show();
                validacao = true;
            }

            // descrição
            if (!$.trim($("#editDesc").val())) {
                $('.message_atividade2').show();
                validacao = true;
            }

            if (validacao) {
                return false;
            } else {
                $.ajax({
                    type: "POST",
                    url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index'));?>/' + $('#co_contrato').val(),
                    data: {
                        "data[edit_atividade]": "1",
                        "data[edit_responsavel]": "1",
                        "data[co_atividade]": $('#btnEditar').attr('coAtividade'),
                        "data[co_contrato]": $('#co_contrato').val(),
                        "data[parent_id]": $('.editAtvVinc option:selected').val(),
                        "data[ds_atividade]": $('#editDesc').val(),
                        "data[co_responsavel]": $('.editAtvResp option:selected').val(),
                        "data[funcao_responsavel]": $('.editAtvFunc option:selected').val(),
                        "data[nome]": $("#editNome").val(),
                        // "data[co_privilegio]":$("#co_privilegio").val(),
                    },
                    success: function (result) {
                        $('#editar-atividade').modal('hide');
                        // console.log(result);return;
                        $('#id_pg_execucao').html("");
                        $('#id_pg_execucao').append(result);
                        // $('#tabelaExecucao i.treegrid-expanded').first().removeClass('icon-chevron-down');
                        $('#formEditAtividade').find("input,textarea,select").val('');
                    }
                });
            }
        });

        // salvar atividade
        $('#add_concluir').click(function () {
            alert('??????????????????????????');
            return;
            // $('#message_dt_inicio_atividade').hide();
            // $('.message_co_responsavel_atividade').hide();
            // $('.message_funcaoResponsavel').hide();
            // $('.message_nome_atividade').hide();
            // $('.message_atividade').hide();
            // $('.message_atividade2').hide();
            // $('#message_periodicidade_atividade').hide();
            // $('#message_dia_execucao_atividade').hide();
            // $('#message_dt_fim_atividade0').hide();

            var validacao = false;

            // responsável
            if ($('.co_responsavel').val() == '' || $('.co_responsavel').val() == '') {
                $('.message_co_responsavel_atividade').show();
                validacao = true;
            }

            // função
            if ($('.funcao_responsavel').val() == '' || $('.funcao_responsavel').val() == ' ') {
                $('.message_funcaoResponsavel').show();
                validacao = true;
            }

            // nome atividade
            if ($('.nomeAtividade').val() == '') {
                $('.message_nome_atividade').show();
                validacao = true;
            }

            // descrição
            if (!$.trim($(".ds_atividade").val())) {
                $('.message_atividade2').show();
                validacao = true;
            }

            // inicio atividade
            if ($('#dt_ini_planejado').val() == '') {
                $('#message_dt_inicio_atividade').show();
                validacao = true;
            }

            if ($('#message_dt_fim_atividade').is(":visible") || $('#message_dt_inicio_atividade').is(":visible") || $('#message_dt_inicio_atividade2').is(":visible") || $('#message_dt_fim_atividade2').is(':visible')) {
                validacao = true;
            }

            // periodicidade
            if ($('#periodicidade').val() == '' && $('#periodicidade').prop('disabled') == false && !$('#parent_id').val()) {
                $('#message_periodicidade_atividade').show();
                validacao = true;
            }

            // dia execucao
            if ($('#dia_execucao').val() == '' && ($('#dia_execucao').prop('disabled') == false) && !$('#parent_id').val()) {
                $('#message_dia_execucao_atividade').show();
                // $('#message_periodicidade_atividade').show();
                validacao = true;
            }

            var iniPlanejado = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');
            var fimPlanejado = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
            var hoje = new Date();
            hoje = moment(hoje.getDate() + '/' + (hoje.getMonth() + 1) + '/' + hoje.getFullYear(), 'DD/MM/YYYY');

            if ($('#dt_ini_planejado').val() && $('#dt_fim_planejado').val() && $('#periodicidade').val()) {
                var periodicidde;
                switch ($('#periodicidade').val().toLowerCase()) {
                    case 'diariamente':
                        periodicidade = 1;
                        break;
                    case 'semanal':
                        periodicidade = 7;
                        break;
                    case 'quinzenal':
                        periodicidade = 15;
                        break;
                    case 'mensal':
                        periodicidade = 30;
                        break;
                    case 'bimestral':
                        periodicidade = 60;
                        break;
                    case 'trimestral':
                        periodicidade = 90;
                        break;
                    case 'semestral':
                        periodicidade = 180;
                        break;
                    case 'anual':
                        periodicidade = 365;
                        break;
                }
                if ((fimPlanejado.diff(iniPlanejado, 'days') / periodicidade) < 2 && $('#periodicidade').prop('disabled') == false && $('#dia_execucao').prop('disabled') == false) {
                    validacao = true;
                    $('#modalAlertPeriodicidade').modal('show');
                }

                // vinculo atividade
                if ($('#vincularAtv').val()) {
                    // validacao = false;
                    $('#message_dia_execucao_atividade').hide();
                    $('#message_periodicidade_atividade').hide();
                }
            }

            // if ($('#dt_ini_planejado').val()) {
            //     if (hoje.diff(iniPlanejado, 'days') > 0) {
            //         validacao = true;
            //          $('#modalAlertDataInicio').modal('show');
            //     }
            // }

            // if($('#dt_fim_planejado').val() == '') {
            // $('#message_dt_fim_atividade0').show();
            //      validacao   = true;
            // }

            if (validacao) {
                return false;
            } else { // Gravar Atividade
                // $.ajax($('#base_url_atv').val() + "/atividades/verificaAtividade/" + $('#co_responsavel').val() + "/" + $('#dt_ini_planejado').val() + "/" + $('#dt_fim_planejado').val(), function(result){
                // if(result) alert(result);
                // });

                $(function () {
                    $("#dialog-confirm").dialog({
                        resizable: false,
                        height: 180,
                        modal: true,
                        buttons: {
                            Sim: function () {
                                $(this).dialog("close");
                            },
                            Nao: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
                //return false;
                var url_add = '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index'));?>/' + $('#co_contrato').val();
                var dataFimPlanejado = $('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '' ?
                    $('#dt_fim_vigencia_atividade').val() : $('#dt_fim_planejado').val();

                // console.log($('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '');
                // console.log($('#dt_fim_vigencia_atividade').val());
                // return;
                $('#formAddAtividade').find('select,input,textarea').val('');
                $.ajax({
                    type: "POST",
                    url: url_add,
                    data: {
                        "data[add_atividade]": "1",
                        "data[co_contrato]": $('#co_contrato').val(),
                        "data[parent_id]": $('#parent_id option:selected').val(),
                        "data[ds_atividade]": $('#ds_atividade').val(),
                        "data[co_responsavel]": $('#co_responsavel option:selected').val(),
                        "data[funcao_responsavel]": $('#funcao_responsavel option:selected').val(),
                        "data[dt_ini_planejado]": $('#dt_ini_planejado').val(),
                        "data[dt_fim_planejado]": dataFimPlanejado,
                        "data[tp_andamento]": "N",
                        "data[nome]": $("#nomeAtividade").val(),
                        "data[periodicidade]": $('#parent_id option:selected').val() ? null : $("#periodicidade").val(),
                        "data[dia_execucao]": $('#parent_id option:selected').val() ? null : $("#dia_execucao").val()
                    },
                    success: function (result) {
                        $('#collapseExec').css('height', 'auto');
                        $('#boxTableAtv').html(result);

                    }
                })
            }

        })
    });
</script>
