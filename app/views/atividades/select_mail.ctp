<?php echo $this->Form->create('Atividade', array('url' => array('controller' => 'atividades', 'action' => 'viewMail', $coAtividade))); ?>
            <div id="simResponsavel">
                <?php
                    echo $this->Form->input('destinatario', array('label' => 'E-mail do Destinatário: ', 'type' => 'select', 'empty' => 'Selecione', 'options' => $emails));
                ?>
            </div>
            <div id="naoResponsavel">
                <?php
                    echo $this->Form->input('email_destinatario', array('label' => 'E-mail do Destinatário: ', 'class' => 'input-xlarge'));
                    echo $this->Form->input('nome', array('label' => 'Nome do Destinatário: ', 'class' => 'input-xlarge'));
                ?>
            </div>

            <input type="checkbox" id="respNresp"> O Destinatário não é o Responsável pela Atividade

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Confirmar Email"> Confirmar</button>
<!--                <button class="myCloseModal" type="button" class="btn btn-small" data-dismiss="modal"> Fechar</button>-->
            </div>
        </div>

        <script type="text/javascript">
            $("#naoResponsavel").hide();
            $("#simResponsavel").show();

            $("#respNresp").click(function() {
                if(document.getElementById("respNresp").checked == true){
                    $("#simResponsavel").hide();
                    $("#naoResponsavel").show();
                } else {
                    $("#naoResponsavel").hide();
                    $("#simResponsavel").show();
                }
            });
        </script>
