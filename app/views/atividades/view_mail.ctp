<?php $usuario = $this->Session->read ('usuario'); ?>

<?php echo $this->Form->create('Atividade', array('url' => array('controller' => 'atividades', 'action' => 'sendMail', $coAtividade))); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1>Visualização do Email</h1></div>

    <h2>[GESCON] - <?php echo $atividade['Atividade']['nome']; ?></h2>

    <p>Olá <?php echo $atividade['Usuario']['ds_nome']; ?>, </p>
    <p>Voce deve realizar a atividade <?php echo $atividade['Atividade']['nome']; ?>
         no período de : <?php echo $atividade['Atividade']['dt_ini_planejado']; ?> à <?php echo $atividade['Atividade']['dt_fim_planejado']; ?>.</p>

    <p><?php echo $atividade['Atividade']['ds_atividade']; ?>.</p>

    <!--    <p>Para dúvidas e formulários, procurar a Seção de Credenciamentos do FUSEX.</p>-->
    <p><b>Este é um e-mail automático, pedimos a gentileza de não respondê-lo.</b></p>

    <?php
        if(!empty($data['Atividade']['destinatario'])){
            echo $this->Form->hidden('destinatario', array('value' => $data['Atividade']['destinatario']));
        } else {
            echo $this->Form->hidden('destinatario', array('value' => $data['Atividade']['email_destinatario']));
        }
    ?>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Enviar Email"> Enviar</button>
<!--            <button type="button" class="btn btn-small" data-dismiss="modal"> Cancelar</button>-->
        </div>
    </div>
</div>