<?php
    $input = '<input type="text" class="input-xlarge" disabled="disabled" value="%s"/>';
    $textArea = '<textarea rows="6" class="input-xlarge" disabled="disabled">%s</textarea>';
?>

<div class="form-horizontal">

    <div class="control-group">
        <label class="control-label">Usuário</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['username']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Data</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['timestamp']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Evento</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['audit_event_log']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">ServerHost</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['serverhost']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Host</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['host']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Connectionid</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['connectionid']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Queryid</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['queryid']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Categoria</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['operation']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Nome do Banco</label>
        <div class="controls controls-row">
            <?php echo sprintf($input, $row['AuditParsed']['database']); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Operação</label>
        <div class="controls controls-row">
            <?php echo sprintf($textArea, $row['AuditParsed']['object']); ?>
        </div>
    </div>
</div>
