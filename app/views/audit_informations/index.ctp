<!--<div class="row-fluid">-->
<!--    <div class="page-header position-relative">-->
<!--    </div>-->
<!--</div>-->


<div id="content-header">
    <h1><?php __('Log de Auditoria'); ?></h1>
</div>

<div class="container-fluid">
    <hr>
    <div class="container-fluid">
        <div class="quick-actions_homepage">
            <ul class="quick-actions">
                <li class="bg_lb span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_search')); ?>">
                        <i class="icon-search"></i> Log de Auditória
                    </a>
                </li>
                <li class="bg_lb span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_history')); ?>">
                        <i class="icon-book"></i> Histórico de Usuário
                    </a>
                </li>
                <li class="bg_lb span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_date_user_access')); ?>">
                        <i class="icon-calendar"></i> Log de Acesso por Usuário por Data
                    </a>
                </li>
                <li class="bg_lb span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_access')); ?>">
                        <i class="icon-user"></i> Log de Acesso por Usuário
                    </a>
                </li>
                <li class="bg_lg span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_operations')); ?>">
                        <i class="icon-tasks"></i> Operações Realizadas
                    </a>
                </li>
                <li class="bg_ls span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_insert')); ?>">
                        <i class="icon-plus"></i> Inclusão de Registro
                    </a>
                </li>
                <li class="bg_ly span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_update')); ?>">
                        <i class="icon-edit"></i> Alteração de Registro
                    </a>
                </li>
                <li class="bg_lo span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_delete')); ?>">
                        <i class="icon-remove"></i> Exclusão de Registro
                    </a>
                </li>
                <li class="bg_lv span4">
                    <a href="<?php echo $this->Html->url(array('controller' => 'audit_informations', 'action' => 'log_user_operation_database')); ?>">
                        <i class="icon-th"></i> Operação por Banco
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>