<div class="row-fluid">
    <div class="page-header position-relative">
        <h1><?php __('Log de Operações'); ?></h1>
    </div>
    <?php //echo $this->Form->create('Log'); ?>
</div>

<div class="row-fluid">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Qtde. Operações', 'occur'); ?></th>
            <th><?php echo $this->Paginator->sort('Operação', 'operation'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($glogs)): ?>
            <?php
            $i = 0;
            foreach ($glogs as $linha):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class; ?>>

                    <td><?php echo $linha['VwAuditOperationsOccur']['occur']; ?>&nbsp;</td>
                    <td><?php echo $linha['VwAuditOperationsOccur']['operation']; ?>&nbsp;</td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="3">Nenhum registro encontrado.</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" title="Voltar" class="btn btn-primary btn-small" id="Voltar"> Voltar</button>
    </div>
</div>