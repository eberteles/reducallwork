<?php echo $this->Html->script('inicia-datetimepicker'); ?>

<div class="row-fluid">
    <div class="page-header position-relative">
        <h1><?php __('Logs de Auditoria'); ?></h1>
    </div>
</div>
<?php echo $this->Form->create(); ?>
<div class="row-fluid">
    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('username', array('class' => 'input-medium', 'label' => 'Usuário', 'type' => 'text'));
            ?>
        </div>
    </div>

    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('dt_ini_log', array(
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small', 'label' => 'Data Inicial', 'type' => 'text'));
            ?>
        </div>
    </div>
    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('dt_fim_log', array(
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small', 'label' => 'Data Final', 'type' => 'text'));
            ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <div class="controls">
            <?php echo $this->Form->input('operation', array('class' => 'input-medium', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Categoria', 'options' => $categories)); ?>
        </div>
    </div>
    <div class="span5">
        <div class="controls">
            <?php echo $this->Form->input('object', array('class' => 'input-xlarge', 'label' => 'Operação', 'type' => 'text')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                title="Pesquisar Log de Auditoria"><i class="icon-search icon-white"></i> Pesquisar
        </button>
        <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="limpar"> Limpar
        </button>
        <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<div class="row-fluid">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Usuário', 'username'); ?></th>
            <th><?php echo $this->Paginator->sort('Host', 'host'); ?></th>
            <th><?php echo $this->Paginator->sort('Data', 'timestamp'); ?></th>
            <th><?php echo $this->Paginator->sort('Banco', 'database'); ?></th>
            <th><?php echo __("Operação") ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($glogs)): ?>
            <?php
            $i = 0;
            foreach ($glogs as $linha):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class; ?>>
                    <td><?php echo $linha['AuditParsed']['username']; ?>&nbsp;</td>
                    <td><?php echo $linha['AuditParsed']['host']; ?>&nbsp;</td>
                    <td><?php echo $linha['AuditParsed']['timestamp']; ?></td>
                    <td><?php echo $linha['AuditParsed']['database']; ?></td>
                    <td><a href="javascript:void(0);"
                           data-id="<?php echo $linha['AuditParsed']['id_audit_parsed']; ?>"
                           class="btn btn-info alert-tooltip detalhar"
                           title="Detalhar">
                            <i class="icon-search icon-white"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="4">Nenhum registro encontrado.</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>

<div id="modal-detalheLog" class="modal modal-medium hide fade" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="modal-title-detalheLog">Detalhamento do Registro</h3>
    </div>
    <div id="modal-detalhe-body" class="modal-body"></div>

    <div class="modal-footer">
        <a class="btn btn-small btn-primary" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
    </div>
</div>
<script type="application/javascript">

    var LogAudit = {
        init: function () {
            $('a.detalhar').on('click', LogAudit.handleClickLinkDetalhar);
            $('#limpar').on('click', function () {
                $('form#log_searchForm :input:not(:button)').val('');
            });
        },

        handleClickLinkDetalhar: function () {
            var id = parseInt($(this).data('id'));
            $.ajax({
                url: "<?php echo $this->base; ?>/audit_informations/detalhar/" + id,
                type: "POST",
                success: function (result) {
                    $('#modal-detalhe-body').html(result);
                    $('#modal-detalheLog').modal();
                }
            });
        }
    };
    $(LogAudit.init);
</script>