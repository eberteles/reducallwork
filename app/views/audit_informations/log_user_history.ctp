<?php echo $this->Html->script('inicia-datetimepicker'); ?>

<div class="row-fluid">
    <div class="page-header position-relative">
        <h1><?php __('Histórico de Usuário'); ?></h1>
    </div>
</div>
<?php echo $this->Form->create(); ?>
<div class="row-fluid">
    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('username', array('class' => 'input-medium', 'label' => 'Usuário', 'type' => 'text'));
            ?>
        </div>
    </div>

    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('dt_ini_log', array(
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small', 'label' => 'Data Inicial', 'type' => 'text'));
            ?>
        </div>
    </div>
    <div class="span2">
        <div class="controls">
            <?php
            echo $this->Form->input('dt_fim_log', array(
                'before' => '<div class="input-append date datetimepicker">',
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                'class' => 'input-small', 'label' => 'Data Final', 'type' => 'text'));
            ?>
        </div>
    </div>

    <div class="span2">
        <div class="controls">
            <?php echo $this->Form->input('limit', array(
                'class' => 'input-smal',
                'type' => 'select',
                'label' => 'Qtd. Registros', 'options' => array(10 => 10, 20 => 20, 50 => 50, 100 => 100, 500 => 500))); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                title="Pesquisar Log de Auditoria"><i class="icon-search icon-white"></i> Pesquisar
        </button>
        <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="limpar"> Limpar
        </button>
        <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<style>
    tr.logUsername > td {
        text-align: left !important;
        background-color: #f5f5f5;
        font-weight: bold;
    }

    tr.logColumnsName td {
        background-color: #f5f5f5;
    }
</style>

<div class="container-fluid">
    <?php if (count($glogs)): ?>

        <table cellpadding="0" cellspacing="0" style="background-color:white" class="table  table-bordered">

            <?php foreach ($glogs as $connectionid => $logs): ?>
                <?php $username = $logs[0]['username'] . ' ( connection_id = ' . $connectionid . ' )'; ?>
                <tr class="logUsername">
                    <td colspan="10"><? echo $username; ?></td>
                </tr>
                <tr class="logColumnsName">
                    <td>Operacao</td>
                    <td>Data</td>
                    <td>ServerHost</td>
                    <td>Host</td>
                    <td>QueryId</td>
                    <td>Database</td>
                    <td>Entidade</td>
                    <td>Objeto</td>
                </tr>
                <?php foreach ($logs as $log): ?>
                    <tr>
                        <td><?php echo $log['operation']; ?></td>
                        <td><?php echo $log['timestamp']; ?></td>
                        <td><?php echo $log['serverhost']; ?></td>
                        <td><?php echo $log['host']; ?></td>
                        <td><?php echo $log['queryid']; ?></td>
                        <td><?php echo $log['database']; ?></td>
                        <td><?php echo $log['entity']; ?></td>
                        <td>
                            <?php if ($log['object']): ?>
                                <a href="javascript:void(0);"
                                   data-id="<?php echo $log['id_audit_parsed']; ?>"
                                   data-object="<?php echo $log['object']; ?>"
                                   class="btn btn-info alert-tooltip detalhar"
                                   title="Detalhar">
                                    <i class="icon-search icon-white"></i>
                                </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </table>

    <?php elseif ($searched): ?>
        <h4>Nenhum registro encontrado.</h4>
    <?php endif; ?>
    <?php if ($searched): ?>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            </ul>
        </div>
    <?php endif; ?>
</div>

<div id="modal-detalheLog" class="modal modal-medium hide fade" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="modal-title-detalheLog">Objeto executado</h3>
    </div>
    <div id="modal-detalhe-body" class="modal-body">
        <textarea id="textarea-object" rows="6" class="input-xxlarge" disabled="disabled"></textarea>
    </div>

    <div class="modal-footer">
        <a class="btn btn-small btn-primary" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
    </div>
</div>

<script type="application/javascript">

    var LogAudit = {
        init: function () {
            $('a.detalhar').on('click', LogAudit.handleClickLinkDetalhar);

            $('#limpar').on('click', function () {
                $('form#log_user_historyForm :input:not(:button)').val('');
            });
        },

        handleClickLinkDetalhar: function () {
            $('#modal-detalhe-body').find('#textarea-object').val($(this).data('object'));
            $('#modal-detalheLog').modal();
        }
    };
    $(LogAudit.init);
</script>


