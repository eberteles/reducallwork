<?php echo $this->Form->create('Cargo', array('url' => array('controller' => 'cargos', 'action' => 'add', $modal))); ?>
    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Novo Cargo</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                            <?php
                            echo $this->Form->input('co_cargo');
                            echo $this->Form->input('ds_cargo', array('class' => 'input-xlarge', 'label' => 'Descrição'));
                            ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

