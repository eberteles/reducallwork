<?php echo $this->Form->create('Categoria', array('onsubmit' => "return confirm('Ao editar essa Categoria, você alterará todos os contratos e outros registros que possuem essa Categoria atrelada. Deseja prosseguir com essa alteração? (Sua alteração ficará registrada em log de alterações de Categorias)');")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Categoria'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_categoria');
                echo $this->Form->input('no_categoria', array('id' => 'noCategoria', 'class' => 'input-xlarge', 'label' => '*Descrição'));
                 echo '<div id="message-noCategoria" class="" style="display:none;width:285px">
                        <p class="error-message"><strong> A categoria já existe e será reativada</strong></p>
                    </div>';
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $().ready(function () {
        $('#noCategoria').on('blur', function (e) {
            if ($(this).val()) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '<?php echo $this->Html->url(array ('controller' => 'categorias', 'action' => 'checar_nome')); ?>/' + $(this).val() + '/1',
                    success: function (res) {
                        if (res.status) {
                            $('#message-noCategoria').show();
                        } else {
                            $('#message-noCcategoria').hide();
                        }
                    },
                    error: function (err) {
                        console.error(err);
                    } 
                });
            }
        });
    });

</script>
