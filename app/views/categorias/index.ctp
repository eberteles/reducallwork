<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Categorias'); ?></h1></div>
    <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/index') ) { ?>
    <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--></p>
    <div class="acoes-formulario-top clearfix">
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'categorias', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php echo __('Nova Categoria'); ?>"><i class="icon-plus icon-white"></i> <?php __('Nova Categoria'); ?></a>
        </div>
    </div>
    <?php } ?>
    <table cellpadding="0" cellspacing="0" style="background-clip: white" class="table table-hover table-bordered table-striped">
        <tr>
            <th><?php echo __('Código');?></th>
            <th><?php echo __('Descrição');?></th>
            <th class="actions"><?php __('Ações');?></th>
        </tr>
        <?php
        $i = 0;
        foreach($categorias as $categoria) :
            $class = null;
            if($i++ %2 == 0) :
                $class = 'altrow';
            endif;
        ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $categoria['Categoria']['co_categoria']; ?></td>
                <td><?php echo $categoria['Categoria']['no_categoria']; ?></td>
                <td class="actions">
                    <div class="btn-group acoes">
                        <?php echo $this->element('actions2', array('id' => $categoria['Categoria']['co_categoria'], 'class' => 'btn', 'local_acao' => 'categorias/index')); ?>
                    </div>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </table>
    <p><?php
    echo $this->Paginator->counter(array(
        'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
    ));
    ?></p>
    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
            <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
        </ul>
    </div>
</div>
