<div class="clausulas form">
<?php echo $this->Form->create('ClausulaContratual', array('url' => "/clausulas_contratuais/add/$coContrato/$modulo"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar uma Cláusula Contratual - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'clausulas_contratuais', 'action' => 'index', $coContrato, $modulo)); ?>" class="btn btn-small btn-primary" title="Listar Cláusulas">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
           <div class="widget-header widget-header-small"><h4>Nova Cláusula</h4></div>
            <div class="widget-body">
              <div class="widget-main">                        
                    <?php
                        echo $this->Form->hidden('id');
                        if($modulo == "ata" || $modulo == "clausula_ata") {
                            echo $this->Form->hidden('co_ata', array('value' => $coContrato));
                        } elseif($modulo == "evento") {
                            echo $this->Form->hidden('co_evento', array('value' => $coContrato));
                        } else {
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                        }

                        echo $this->Form->input('nu_clausula', array('label' => 'Número'));
                        echo $this->Form->input('ds_clausula', array('label'=>'Descrição', 'type'=>'textarea','type' => 'texarea',
                            'onKeyup'=>'$(this).limit("500","#charsLeft")', 
                            'after' => '<br><span id="charsLeft">500</span> caracteres restantes.'));
                        echo '<br />';
                    ?>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Cláusula"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>

         
    $('#Voltar').click(function(){
             
                  history.back();
                   
            
        });
<?php echo $this->Html->scriptEnd() ?>