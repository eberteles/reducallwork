<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="clausulas index">
<!-- h2>< ?php __('Históricos');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbClausulaContratual">
	<tr>
		<th><?php __('Número');?></th>
		<th><?php __( 'Descrição');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clausulas as $clausulas):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $clausulas['ClausulaContratual']['nu_clausula']; ?>&nbsp;</td>
		<td><?php echo $clausulas['ClausulaContratual']['ds_clausula']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $clausulas['ClausulaContratual']['id']; ?>

		<div class="btn-group acoes">
                    <!--a id="< ?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Histórico" style="display: inline-block;"></i></a-->
                    <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato.'/'.$modulo , 'class' => 'btn', 'local_acao' => 'clausulascontratuais/' ) ) ?>
                </div></td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'clausulascontratuais/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'clausulas_contratuais', 'action' => 'add', $coContrato, $modulo)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Cláusula Contratual</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php
    $modulo_h   = "clausulas";
    if($modulo == "ata") {
        $modulo_h   = $modulo_h . "_ata";
    }
    echo $this->Html->scriptStart();
?>

    $('.alert-tooltip').tooltip();

    $('#tbClausulaContratual tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato . "/" . $modulo_h; ?>/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
