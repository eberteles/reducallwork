<?php
echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Html->script( 'date' );

echo $this->Form->create('Cliente', array('url' => array('controller' => 'clientes', 'action' => 'add', $modal),  'inputDefaults' => array(
    'label' => false
)));
echo $this->Form->hidden('co_cliente');
//echo $form->hidden('tp_cliente', array('value' => 'j'));
?>

<b>Campos com * são obrigatórios.</b><br>
<div class="row-fluid">

    <?php if(!$this->Modulo->getHasNewClientes()) { ?>
    <div class="span6">
        <div class="widget-header widget-header-small"><h4><?php echo $this->Print->getLabelCliente($this->Modulo->isContratoExterno()); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                      <div class="span12">
                          <?php
                          echo $this->Form->input('tp_cliente', array('class' => 'input-xlarge', 'type' => 'select', 'label' => __('Tipo de Cliente', true), 'options' => $this->Modulo->getTiposFornecedores() ));
                          echo $this->Form->input('nu_cnpj', array('class' => 'input-xlarge', 'id' => 'nuCpf', 'mask' => '99.999.999/9999-99', 'onblur' => 'verificaCPF()', 'label' => __('CNPJ / CPF', true)));
                          ?>
                          <span id="isValidMessage"></span>
                          <?php
                          echo $this->Form->input('no_razao_social', array('class' => 'input-xlarge', 'label' => __('Razão Social / Nome', true), 'rows'=>'4', 'type' => 'textarea', 'maxlength' => '220'));
                          if($this->Modulo->isCamposCliente('no_nome_fantasia')) :
                              echo $this->Form->input('no_nome_fantasia', array('class' => 'input-xlarge', 'label' => 'Nome Fantasia'));
                          endif;
                          echo $this->Form->input('is_cliente_orgao', array('class' => 'input-xlarge', 'label' => 'É Cliente do Órgão?', 'type' => 'select', 'options' => array('S' => 'Sim', 'N' => 'Não')));

                          // Pedido da CODESP
                          if ($this->Modulo->isCamposCliente('ic_suspenso')) {
                            echo $this->Form->input('ic_suspenso', array(
                                'type' => 'checkbox',
                                'label' =>  'Empresa Suspensa',
                                'class' => 'input-xlarge',
                            ));
                          }

                          if ($this->Modulo->isCamposCliente('ds_penalidade_aplicada')) {
                            echo $this->Form->input('ds_penalidade_aplicada', array(
                                'type' => 'textarea',
                                'label' =>  'Penalidade Aplicada',
                                'class' => 'input-xlarge',
                                'rows' => 3,
                                'minlength' => 3,
                                'maxlength' => 100
                            ));
                          }

                          if ($this->Modulo->isCamposCliente('dt_ini_penalidade')) {

                           echo '<table><caption style="text-align:left">Período de Vigência da Penalidade</caption><tr><td>';

                            echo $this->Form->input('dt_ini_penalidade', array(
                                'id' => '',
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small',
                                'type' => 'text'
                                ));
                            echo '<td>&nbsp;&nbsp;Até&nbsp;&nbsp;</td><td>';
                            echo $this->Form->input('dt_fim_penalidade', array(
                                'id' => '',
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small',
                                'type' => 'text'
                                ));
                            echo '</td></tr></table>';
                          }
                          ?>
                      </div>
                      <div class="span12">
                          <?php if($this->Modulo->getHasNewClientes()) { // Comentário inútil
                              echo $this->Form->input('ocs_psa', array('class' => 'input-xlarge', 'label' => 'É uma OCS ou PSA?', 'type' => 'select', 'empty' => 'Selecione', 'options' => $combo));
                          } ?>
                      </div>
                  </div>
              </div>
              <div id="dialog-cliente" class="hide">
                    <div class="alert alert-info bigger-110">
                        Já existe um Cliente cadastrado com o número informado.<br>
                    <?php
                    if(!$modal) {
                    ?>
                        Deseja editar este Cliente?
                    <?php
                    }
                    ?>
                    </div>
              </div>
            </div>
    </div>
    <?php } else { ?>
        <div class="span12">
            <div class="widget-header widget-header-small"><h4><?php echo $this->Print->getLabelCliente($this->Modulo->isContratoExterno()); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span3">
                            <?php
                            echo $this->Form->input('tp_cliente', array('class' => 'input-xlarge', 'type' => 'select', 'label' => __('Tipo de Cliente', true), 'options' => $this->Modulo->getTiposClientes() ));
                            ?>
                        </div>
                        <div class="span3">
                            <?php
                                echo $this->Form->input('nu_cnpj', array('class' => 'input-xlarge', 'mask' => '99.999.999/9999-99', 'id' => 'nuCpf', 'onblur' => 'verificaCPF()', 'label' => __('CNPJ / CPF', true)));
                            ?>
                            <span id="isValidMessage"></span>
                        </div>
                        <div class="span3">
                            <?php
                                echo $this->Form->input('no_razao_social', array('class' => 'input-xlarge', 'label' => __('Razão Social / Nome', true), 'rows'=>'4', 'type' => 'textarea'));
                                if($this->Modulo->isCamposCliente('no_nome_fantasia')) :
                                    echo $this->Form->input('no_nome_fantasia', array('class' => 'input-xlarge', 'label' => 'Nome Fantasia'));
                                endif;
                            ?>
                        </div>
                        <div class="span3">
                            <?php if($this->Modulo->getHasNewClientes()) {
                                echo $this->Form->input('ocs_psa', array('class' => 'input-xlarge', 'label' => 'É uma OCS ou PSA?', 'type' => 'select', 'empty' => 'Selecione', 'options' => $combo));
                            } ?>
                        </div>
                    </div>
                </div>
                <div id="dialog-cliente" class="hide">
                    <div class="alert alert-info bigger-110">
                        Já existe um Cliente cadastrado com o número informado.<br>
                        <?php
                        if(!$modal) {
                            ?>
                            Deseja editar este Cliente?
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if(!$this->Modulo->getHasNewClientes()) { ?>
    <div class="span6">
        <div class="widget-header widget-header-small"><h4><?php __('Contato'); ?></h4></div>
        <div class="widget-body">
              <div class="widget-main">
                <?php
                echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail'));
                echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
                //echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone'));
                echo $this->Form->input('nu_fax', array('class' => 'input-xlarge', 'label' => __('Fax', true), 'mask' => '(99) 9999-9999?9'));
                ?>
            </div>
        </div>
    </div>
    <?php } ?>

</div><br>
<div class="row-fluid">
    <div class="span4 ">
        <div class="widget-header widget-header-small"><h4><?php __('Grupo Responsável'); ?></h4></div>
        <div class="widget-body">
              <div class="widget-main">
                <?php
                echo $this->Form->input('no_responsavel', array('class' => 'input-xlarge', 'label' => __('Responsável', true)));
                if($this->Modulo->isCamposCliente('nu_cpf_responsavel')) {
                    echo $this->Form->input('nu_cpf_responsavel', array('class' => 'input-xlarge', 'label' => 'CPF', 'mask' => '999.999.999-99', 'onblur' => 'verificaCPFGrupoResponsavel()'));
                }
                ?>
                  <span id="isValidMessageResp"></span>
                <?php
                if($this->Modulo->isCamposCliente('nu_rg_responsavel')) :
                    echo $this->Form->input('nu_rg_responsavel', array('class' => 'input-xlarge', 'label' => 'RG'));
                endif;
                if($this->Modulo->isCamposCliente('no_preposto')) {
                    echo $this->Form->input('no_preposto', array('class' => 'input-xlarge', 'label' => 'Preposto'));
                }
//echo $this->Form->hidden('dt_inclusao', array('label'=>'Data '));
                ?>
              </div>
        </div>
    </div>
    <div class="span8 ">
        <div class="widget-header widget-header-small"><h4><?php __('Endereço'); ?></h4></div>
        <div class="widget-body">
           <div class="widget-main">
             <div class="row-fluid">
                <div class="span6">
                        <?php
                        echo $this->Form->input('nu_cep', array('class' => 'input-xlarge', 'label' => 'Cep', 'mask' => '99999-999'));
                        echo $this->Form->input('ds_endereco', array('class' => 'input-xlarge', 'label' => 'Endereço'));
                        echo $this->Form->input('nu_endereco', array('class' => 'input-xlarge', 'label' => 'Número'));
                        ?>
                </div>
                <div class="span3">
                        <?php
                        echo $this->Form->input('sg_uf', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'UF', 'empty' => 'Selecione..', 'options' => $estados,
                        'onchange' => 'verificaTipoDeContrato(this.value)', 'style' => 'text-transform:uppercase'));
                        echo $this->Form->input('co_municipio', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Município', 'empty' => 'Selecione..', 'style' => 'text-transform:uppercase'));
                        echo $this->Form->input('bairro', array('class' => 'input-xlarge', 'type' => 'text', 'label' => 'Bairro', 'style' => 'text-transform:uppercase', 'maxlength' =>'120'));
                        ?>
                </div>
             </div>
          </div>
        </div>
    </div>
</div><br>

<div class="row-fluid">
     <div class="span4 ">
         <?php if($this->Modulo->isCamposCliente('dados_bancarios') == true) : ?>
        <div class="widget-header widget-header-small"><h4><?php __('Dados Bancários'); ?></h4></div>
        <div class="widget-body">
              <div class="widget-main">
                    <?php
                    echo $this->Form->input('co_banco', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Banco', 'options' => $bancos, 'default' => null));
                    echo $this->Form->input('nu_agencia', array('class' => 'input-xlarge', 'label' => 'Agência'));
                    echo $this->Form->input('nu_conta', array('class' => 'input-xlarge', 'label' => 'Conta'));
                    ?>
               </div>
        </div>
        <?php endif; ?>
     </div>

     <div class="span8 ">
        <div class="widget-header widget-header-small"><h4><?php __('Observações'); ?></h4></div>
        <div class="widget-body">
           <div class="widget-main">
             <div class="row-fluid">
                <?php
                if($this->Modulo->isCamposCliente('co_area') == true) {
                ?>
                <div class="span6">
                    <?php echo $this->Form->input('co_area',   array('class' => 'input-xlarge chosen-select', 'id' => 'coArea', 'type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Área', true), 'options' => $imprimirArea->getArrayAreas($areas), 'escape' => false, 'default' => null)); ?>
                </div>
                <?php } ?>
                <div class="span6">
                        <?php echo $this->Form->input('ds_observacao', array('class' => 'input-xlarge', 'label' => 'Observação (Tamanho máximo: 255 caracteres)', 'type' => 'texarea', 'cols' => '40', 'rows' => '4', 'maxLength' => '255' ));
                        ?>
                </div>
             </div>
           </div>
        </div>
     </div>
</div>

<?php if($this->Modulo->getHasNewClientes()): ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-header widget-header-small"><h4>Indicadores do Cliente</h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <p><strong>Os indicadores servem para mostrar de forma geral a importância do Cliente para a Instituição.</strong></p>
                        <div class="span3">
                            <?php echo $this->Form->input('indicador_valor', array('class' => 'input-xlarge', 'label' => 'Indicador de Valor', 'type' => 'select', 'empty' => 'Selecione', 'options' => $indValor, 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="1)Alto: Cliente com Alto valor contratual. 2)Médio: Cliente com Médio valor contratual. 3)Pequeno: Cliente com Pequeno valor contratual."><i class="icon-info-sign"></i></a>')); ?>
                        </div>
                        <div class="span3">
                            <?php echo $this->Form->input('indicador_importancia', array('class' => 'input-xlarge', 'label' => 'Indicador de Importância', 'type' => 'select', 'empty' => 'Selecione', 'options' => $indImportancia, 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="1) Imprescindível: Cliente de importância imprescindível. 2) Necessária: Cliente de importância necessária. 3) Comum: Cliente de importância comum;"><i class="icon-info-sign"></i></a>')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar" id="Salvar"> Salvar</button>
        <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
		<button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>



<script type="text/javascript">

    $("#isValidMessage").hide();

    $('#nuCpf').mask('99.999.999/9999-99');

    $('#ClienteNuAgencia').mask('?****-*', {autoclear: false, placeholder: "0", reverse: false});

    $('#ClienteNuConta').mask('******?*****-*', {autoclear: false, placeholder: "0", reverse: false});

    $('#ClienteNuTelefone')
        .mask("(99) 9999-9999?9")
        .on('focusout', function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if(phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
    });

    // alternar entre a mascara de cnpj e cnpj
    $('#ClienteTpCliente').on('change', function (e) {
        if ($(this).val() == 'F') {
            $('#nuCpf').mask('999.999.999-99');
        } else if ($(this).val() == 'J') {
            $('#nuCpf').mask('99.999.999/9999-99');
        }

    });

    $("#ClienteSgUf").change(function()
    {
     	$.getJSON("<?php echo $this->Html->url(array('controller' => 'municipios', 'action' => 'listar')) ?>" + "/" + $(this).val(), null, function (data) {
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
              });
            $("select#ClienteCoMunicipio").html(options);
        })
    });

     document.getElementById('ClienteCoMunicipio').disabled = true ;


    function verificaTipoDeContrato(tipo) {

        if(tipo == "") {
            document.getElementById('ClienteCoMunicipio').disabled = true ;
        } else {
            document.getElementById('ClienteCoMunicipio').disabled = false;
        }

    }


    function verificaCPF(){
        var reg = /[.-]/gi;
        var regNumber = /[0-9]/gi;
        var cpf = $("#nuCpf").val();
        cpf = cpf.replace(reg, '');
        if(regNumber.test(cpf)) {
            if( cpf.length > 13 ){
                cpf = cpf.replace('.','').replace('.','').replace('/','').replace('-','');
                $.getJSON("<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'verifyCNPJ')) ?>//" + "/" + cpf, function(data) {
                    //console.log(data);
                    document.getElementById('isValidMessage').style.color = data.color;
                    document.getElementById('isValidMessage').style.fontWeight = data.bold;
                    $("#isValidMessage").html(data.message).show();
                }).done(function(){
                })
            }else{
                cpf = cpf.replace('.','').replace('.','').replace('-','');
                $.getJSON("<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function(data) {
                    //console.log(data);
                    document.getElementById('isValidMessage').style.color = data.color;
                    document.getElementById('isValidMessage').style.fontWeight = data.bold;
                    $("#isValidMessage").html(data.message).show();
                }).done(function(){
                })
            }
        } else {
            document.getElementById('isValidMessage').style.color = "red";
            document.getElementById('isValidMessage').style.fontWeight = "bold";
            $("#isValidMessage").html("CPF/CNPJ em branco.").show();
        }
    }

    function verificaCPFGrupoResponsavel(){
        var cpf = $("#ClienteNuCpfResponsavel").val();

        if( (cpf.length) > 14 ){
            cpf = cpf.replace('.','').replace('.','').replace('/','').replace('-','');
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'verifyCNPJ')) ?>//" + "/" + cpf, function(data) {
                alert(data);
                $('#isValidMessageResp').attr('style', 'color: ' + data.color + '; fontWeight:' + data.bold );
                $("#isValidMessageResp").html(data.message).show();
            }).done(function(){
                console.log("Second success");
            })
        }else{
            cpf = cpf.replace('.','').replace('.','').replace('-','');
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function(data) {
                //console.log(data);
                $('#isValidMessageResp').attr('style', 'color: ' + data.color + '; fontWeight:' + data.bold );
                $("#isValidMessageResp").html(data.message).show();
            }).done(function(){
                console.log("Second success");
            })
        }
    }
</script>
