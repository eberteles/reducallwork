<?php $usuario = $this->Session->read('usuario'); ?>
<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Clientes'); ?></h1></div>

    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
        <div class="pull-right btn-group">
            <?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'clientes/add')): ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'add')); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Novo Cliente'); ?>"><i
                            class="icon-plus icon-white"></i> <?php __('Novo Cliente'); ?></a>
            <?php endif; ?>
        </div>
    </div>
    <?php echo $this->Form->create('Cliente'); ?>
    <div class="row-fluid">
        <div class="span2">
            <div class="controls">
                <?php echo $this->Form->input('tp_cliente', array('class' => 'input-large', 'type' => 'select', 'label' => __('Tipo de Cliente', true), 'empty' => 'Selecione..', 'options' => $this->Modulo->getTiposClientes())); ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php echo $this->Form->input('nu_cnpj', array('class' => 'input-xsmall', 'label' => __('CNPJ / CPF', true), 'maxlength' => 18)); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('no_razao_social', array('class' => 'input-xlarge', 'label' => __('Razão Social / Nome', true))); ?>
            </div>
        </div>
        <?php if ($this->Modulo->isCamposCliente('co_area') == true && $this->Modulo->getHasNewClientes() == false): ?>
            <div class="span3">
                <div class="controls">
                    <?php echo $this->Form->input('co_area', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Área', true), 'options' => $imprimirArea->getArrayAreas($areas), 'escape' => false)); ?>
                </div>
            </div>
        <?php elseif ($this->Modulo->getHasNewClientes()): ?>
            <div class="span2">
                <div class="controls">
                    <?php echo $this->Form->input('indicador_valor', array('class' => 'input-large', 'type' => 'select', 'empty' => 'Selecione', 'label' => 'Indicador de Valor', 'options' => $indValores)); ?>
                </div>
            </div>
            <div class="span2">
                <div class="controls">
                    <?php echo $this->Form->input('indicador_importancia', array('class' => 'input-large', 'type' => 'select', 'empty' => 'Selecione', 'label' => 'Indicador de Importância', 'options' => $indImportancia)); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                Limpar
            </button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>

            <th><?php echo __('CNPJ / CPF'); ?></th>
            <th><?php echo __('Razão Social / Nome'); ?></th>

            <?php if ($this->Modulo->isCamposCliente('co_area')) : ?>
                <th><?php echo __('Área'); ?></th>
            <?php endif; ?>

            <?php if ($this->Modulo->getHasNewClientes()): ?>
                <th>Indicador de Valor &nbsp;&nbsp;&nbsp;<a class="alert-tooltip"
                                                            title="1)Alto: Cliente com Alto valor contratual. 2)Médio: Cliente com Médio valor contratual. 3)Pequeno: Cliente com Pequeno valor contratual."><i
                                class="icon-info-sign"></i></a></th>
                <th>Indicador de Importância &nbsp;&nbsp;&nbsp;<a class="alert-tooltip"
                                                                  title="1) Imprescindível: Cliente de importância imprescindível. 2) Necessária: Cliente de importância necessária. 3) Comum: Cliente de importância comum;"><i
                                class="icon-info-sign"></i></a></th>
            <?php else: ?>
                <th><?php echo __('E-mail'); ?></th>
                <th><?php echo __('Telefone'); ?></th>
            <?php endif; ?>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($clientes as $cliente):
            ?>
            <tr>
                <?php if ($this->Modulo->getHasNewClientes()): ?>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'detalha' . '/' . $cliente['Cliente']['co_cliente'])); ?>"><?php echo $this->Print->cnpjCpf($cliente['Cliente']['nu_cnpj'], $cliente['Cliente']['tp_cliente']); ?>
                            &nbsp;</a></td>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'detalha' . '/' . $cliente['Cliente']['co_cliente'])); ?>"><?php echo $cliente['Cliente']['no_razao_social']; ?>
                            &nbsp;</a></td>
                <?php else: ?>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'mostrar' . '/' . $cliente['Cliente']['co_cliente'])); ?>"><?php echo $this->Print->cnpjCpf($cliente['Cliente']['nu_cnpj'], $cliente['Cliente']['tp_cliente']); ?>
                            &nbsp;</a></td>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'clientes', 'action' => 'mostrar' . '/' . $cliente['Cliente']['co_cliente'])); ?>"><?php echo $cliente['Cliente']['no_razao_social']; ?>
                            &nbsp;</a></td>
                <?php endif; ?>

                <?php if ($this->Modulo->isCamposCliente('co_area')) : ?>
                    <td><?php echo $cliente['Area']['ds_area']; ?>&nbsp;</td>
                <?php endif; ?>

                <?php if ($this->Modulo->getHasNewClientes()): ?>
                    <td><?php echo $cliente['Cliente']['indicador_valor']; ?></td>
                    <td><?php echo $cliente['Cliente']['indicador_importancia']; ?></td>
                <?php else: ?>
                    <td><?php echo $cliente['Cliente']['ds_email']; ?>&nbsp;</td>
                    <td><?php echo $this->Print->telefone($cliente['Cliente']['nu_telefone']); ?>&nbsp;</td>
                <?php endif; ?>


                <td class="actions"><?php $id = $cliente['Cliente']['co_cliente']; ?>
                    <div class="btn-group acoes">
                        <?php
                        if ($this->Modulo->getHasNewClientes()) {
                            echo $this->element('actions11', array('id' => $cliente['Cliente']['co_cliente'], 'class' => 'btn', 'local_acao' => 'clientes/index'));
                        } else {
                            echo $this->element('actions8', array('id' => $cliente['Cliente']['co_cliente'], 'class' => 'btn', 'local_acao' => 'clientes/index'));
                        }
                        ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#ClienteTpCliente').on('change', function () {
            $("#ClienteNuCnpj").unmask();
            var tpCliente = $(this).val();
            if (tpCliente == 'F') {
                $('#ClienteNuCnpj').mask('999.999.999-99');

            } else if (tpCliente == 'J') {
                $('#ClienteNuCnpj').mask('99.999.999/9999-99');
            }
        });
        $('#ClienteTpCliente').val('J').trigger('change');
    });
</script>
