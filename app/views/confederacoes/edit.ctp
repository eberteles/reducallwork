<?php echo $this->Form->create('Confederacao', array('url' => '/confederacoes/edit')); ?>


    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-header widget-header-small"><h4><?php __('Confederações / Federações'); ?></h4></div>
                    <div class="widget-body">
                      <div class="widget-main">
                        <?php
                            echo $this->Form->hidden('co_confederacao');

                            echo $this->Form->input('parent_id', array('class' => 'input-xxlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Vinculado a', true), 'options' => $imprimir->getArrayConfederacoes($confederacoes), 'escape' => false));
                                                        
                            echo $this->Form->input('nu_confederacao', array('class' => 'input-xlarge', 'label' => 'CNPJ', 'mask' => '99.999.999/9999-99'));

                            echo $this->Form->input('ds_confederacao', array('class' => 'input-xxlarge', 'label' => 'Nome'));
                            
                            echo 'Comitê que é vinculado:<br>';
                            echo '<div class="row-fluid"><div class="span4">';
                            echo 'COB: <br>' . $this->Form->checkbox('ic_cob', array('checked' => false));
                            echo '</div>';
                            echo '<div class="span4">';
                            echo 'CPB: <br>' . $this->Form->checkbox('ic_cpb', array('checked' => false));
                            echo '</div></div>';
                        ?>
                      </div>
                    </div>
            </div>
            <div class="span6">
                <div class="widget-header widget-header-small"><h4><?php __('Contato'); ?></h4></div>
                <div class="widget-body">
                      <div class="widget-main">
                        <?php
                        echo $this->Form->input('no_contrato', array('class' => 'input-xlarge', 'label' => 'Nome do Presidente'));
                        echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'Email','maxLength' => '30'));
                        echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'id' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
                        echo $this->Form->input('nu_celular', array('class' => 'input-xlarge', 'label' => 'Celular', 'id' => 'Celular', 'mask' => '(99) 9999-9999?9'));
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-header widget-header-small"><h4><?php __('Endereço'); ?></h4></div>
                <div class="widget-body">
                   <div class="widget-main">
                     <div class="row-fluid">
                        <div class="span6">
                                <?php
                                echo $this->Form->input('ds_endereco', array('class' => 'input-xlarge', 'label' => 'Endereço'));
                                echo $this->Form->input('ds_complemento', array('class' => 'input-xlarge', 'label' => 'Complemento'));
                                echo $this->Form->input('ds_bairro', array('class' => 'input-xlarge', 'label' => 'Bairro'));
                                ?>
                        </div>
                        <div class="span3">
                                <?php
                                echo $this->Form->input('sg_uf', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'UF', 'empty' => 'Selecione..', 'options' => $estados));
                                echo $this->Form->input('co_municipio', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Município', 'empty' => 'Selecione..', 'options' => $municipios));
                                echo $this->Form->input('nu_cep', array('class' => 'input-xlarge', 'label' => 'Cep', 'mask' => '99999-999'));
                                ?>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

<script type="text/javascript">
    <!--
    $("#ConfederacaoSgUf").change(function()
    {
        $.getJSON("<?php echo $this->Html->url(array('controller' => 'municipios', 'action' => 'listar')) ?>" + "/" + $(this).val(), null, function(data) {
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
              });
            $("select#ConfederacaoCoMunicipio").html(options);
            $("#ConfederacaoCoMunicipio").trigger("chosen:updated");
        })
    });
//-->
</script>
