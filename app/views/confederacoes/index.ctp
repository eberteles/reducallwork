
	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Confederações / Federações'); ?></h1></div>
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'confederacoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Unidade"><i class="icon-plus icon-white"></i> <?php __('Nova Confederação / Federação'); ?></a> 
          </div>
        </div>
	<?php echo $this->Form->create('Confederacao', array('url' => '/confederacoes/index'));?>
	
<div class="row-fluid">
    	<div class="span3">
    		<div class="controls">
    			<? echo $this->Form->input('ds_confederacao', array('class' => 'input-xlarge','label' => __('Confederação / Federação', true),'maxLength'=>'20')); ?>
    		</div>
    	</div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('sg_uf', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'UF', 'empty' => 'Selecione..', 'options' => $estados)); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php 
                    echo '<div class="row-fluid"><div class="span4">';
                    echo 'COB: <br>' . $this->Form->checkbox('ic_cob', array('checked' => false));
                    echo '</div>';
                    echo '<div class="span4">';
                    echo 'CPB: <br>' . $this->Form->checkbox('ic_cpb', array('checked' => false));
                    echo '</div></div>';
                ?>
            </div>
        </div>
</div>
 <div class="form-actions">
            <div class="btn-group">
              <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button> 
              <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button> 
              <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
          </div>
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tbConfederacao">
	<tr>
		<th><?php __('Nome');?></th>
		<th><?php __('COB');?></th>
		<th><?php __('CPB');?></th>
		<th><?php __('Email');?></th>
		<th><?php __('Telefone');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
        
	foreach ($confederacaos as $confederacao):
            $imprimir->listaTabelaConfederacao($confederacao, '', $this->Modulo);
        endforeach; 
        
        ?>
</table>
<p>
    <?php
//echo $this->Paginator->counter(array(
//	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
//));
?></p>

<!--<div class="pagination">
    <ul>
        < ?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        < ?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> < ?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>-->
</div>

<div id="view_modalidade" class="modal hide fade maior" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-header page-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
                <h1 id="titleModalModalidade">Modalidades da Confederação</h1>
	</div>
	<div class="modal-body-iframe" id="conf_modalidade"></div>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('#tbConfederacao tr td a.v_modalidade').click(function(){
        var title   = 'Modalidades - ' + $(this).attr('title-modalidade');
        var url_md  = "<?php echo $this->base; ?>/confederacoes/iframe_vincular/" + $(this).attr('id');
        $('#titleModalModalidade').html("");
        $('#titleModalModalidade').append( title );
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
                },
                success:function(result){
                    $('#conf_modalidade').html("");
                    $('#conf_modalidade').append(result);
                }
            })
    });

    $(document).ready(function() {
        $('.tree').treegrid({
            'initialState': 'collapsed',
            'saveState': true,
            treeColumn: 0
        });
    })

<?php echo $this->Html->scriptEnd() ?>