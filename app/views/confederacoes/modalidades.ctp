<div class="row-fluid">
    <?php echo $this->Form->create('ModalidadesConfederacao', array('url' => '/confederacoes/modalidades/' . $co_confederacao));?>
    
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">
        <div class="span8">
         <?php 
            echo $this->Form->hidden('co_confederacao', array('value'=>$co_confederacao));
            echo $this->Form->input('co_modalidade', array('class' => 'input-xxlarge chosen-select', 'type' => 'select', 'label' => 'Modalidades', 'empty' => 'Selecione..', 'options' => $listModalidades));
         ?>
        </div>
        <div class="span3"><br>
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Vincular Modalidade à Confederação">Vincular Modalidade</button>
        </div>
        </p>
    </div>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tbConfederacao">
	<tr>
            <th><?php __('Modalidade');?></th>
            <th><?php __('Tipo de Modalidade');?></th>
            <th><?php __('Tipo de Prova');?></th>
            <th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($modalidades as $modalidade):
            $id = $modalidade['Modalidade']['co_modalidade'];
        ?>
        <tr>
            <td><?php echo $modalidade['Modalidade']['ds_modalidade']; ?>&nbsp;</td>
            <td><?php if($modalidade['Modalidade']['tp_modalidade'] > 0) { echo $tiposModalidade[ $modalidade['Modalidade']['tp_modalidade'] ]; } ?>&nbsp;</td>
            <td><?php if($modalidade['Modalidade']['tp_prova'] > 0) { echo $tiposProva[ $modalidade['Modalidade']['tp_prova'] ]; } ?>&nbsp;</td>
            <td class="actions">
                <div class="btn-group acoes">
                    <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'desvincularModalidade', $id, $co_confederacao), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja desvincular # %s?', true), $id)); ?>
                </div>
            </td>
        </tr>
        <?php
        endforeach; 
        ?>
    </table>
</div>