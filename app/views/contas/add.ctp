<div class="contas form">
	<?php echo $this->Form->create('Conta', array('url' => "/contas/add/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Produto - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contas', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Contas">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Novo Produto</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('id');
                              echo $this->Form->hidden('contrato_id', array('value' => $coContrato));
                              
                              echo $this->Form->input('conta', array('label' => __('Código da Conta', true),
                                'between' => '<span class="input-icon"><i class="icon-file blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('cnpj', array('label' => __('CNPJ', true), 'mask' => '99.999.999/9999-99',
                                'between' => '<span class="input-icon"><i class="icon-building blue"></i>',
                                'after' => '</span>&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Utilizar CNPJ do Cliente" id="CnpjCliente" href="#cnpf_cliente" data-toggle="modal"><i class="blue icon-check bigger-150"></i></a>'));
                              echo $this->Form->input('operadora_id', array('class' => 'input-large chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Operadora', true), 'options' => $operadoras,
                                      'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Adicionar Operadora" id="AbrirNovaOperadora" href="#view_nova_operadora" data-toggle="modal"><i class="blue icon-edit bigger-150"></i></a>'));
                              echo $this->Form->input('telefone', array('class' => 'input-sm', 'label' => 'Telefone', 'mask' => '(99) 9999-9999?9',
                                'between' => '<span class="input-icon"><i class="icon-phone blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('login', array('label' => __('Login', true),
                                'between' => '<span class="input-icon"><i class="icon-user blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('senha', array('label' => __('Senha', true),
                                'between' => '<span class="input-icon"><i class="icon-key blue"></i>',
                                'after' => '</span>'));
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('gestor_id');
                              echo $this->Form->hidden('Gestor.id');
                              echo $this->Form->hidden('Gestor.contrato_id', array('value' => $coContrato));
                              echo $this->Form->input('Gestor.cpf', array('class' => 'input-large', 'id' => 'cpf', 'mask' => '999.999.999-99', 'onblur' => 'recuperaGestor()', 'label' => __('CPF do Gestor', true),
                                'between' => '<span class="input-icon"><i class="icon-building blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('Gestor.nome', array('label' => __('Nome do Gestor', true),
                                'between' => '<span class="input-icon"><i class="icon-briefcase blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('Gestor.rg', array('label' => __('RG do Gestor', true),
                                'between' => '<span class="input-icon"><i class="icon-building blue"></i>',
                                'after' => '</span>'));
                              echo $this->Form->input('Gestor.nascimento', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                'class' => 'input-small', 'label' => 'Nascimento do Gestor', 'type' => 'text'));
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Conta"> Salvar</button> 
          <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>
	<?php echo $this->Form->end(); ?>
</div>

<div id="view_nova_operadora" class="modal hide fade maior" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Nova Operadora</h3>
    </div>
        <div class="modal-body-iframe" id="add_operadora">
    </div>
</div>

<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<script>

    $("#AbrirNovaOperadora").bind('click', function (e) {
        var url_md = "<?php echo $this->base; ?>/operadoras/iframe/";
        $.ajax({
            type: "POST",
            url: url_md,
            data: {},
            success: function (result) {
                $('#add_operadora').html("");
                $('#add_operadora').append(result);
            }
        })
    });
    
    $("#CnpjCliente").bind('click', function (e) {
        $('#ContaCnpj').val(parent.$('#ClienteNuCnpj').val());
        $('#ContaCnpj').focus();
    });
        
    function atzComboOperadora(id) {
        $.getJSON("<?php echo $this->Html->url(array('controller' => 'operadoras', 'action' => 'listar'))?>", null, function (data) {
            var options = '<option value="">Selecione..</option>';
            $.each(data, function (index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContaOperadoraId").html(options);
            $("#ContaOperadoraId option[value=" + id + "]").attr("selected", true);
            $("#ContaOperadoraId").trigger("chosen:updated");
        });
    }
    
    function recuperaGestor(){
        var reg = /[.-]/gi;
        var regNumber = /[0-9]/gi;
        var cpf = $("#cpf").val();
        cpf = cpf.replace(reg, '');
        if(regNumber.test(cpf) && cpf.length == 11) {
            cpf = cpf.replace('.','').replace('.','').replace('-','');
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'contas', 'action' => 'find_gestor', $coContrato)) ?>/" + cpf, function(data) {
                $('#ContaGestorId').val( data['Gestor']['id'] );
                $('#GestorId').val( data['Gestor']['id'] );
                $('#GestorNome').val( data['Gestor']['nome'] );
                $('#GestorRg').val( data['Gestor']['rg'] );
                $('#GestorNascimento').val( data['Gestor']['nascimento'] );
            }).done(function(){
            })
        }
    }
        
</script>