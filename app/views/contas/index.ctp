<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="contas index">
<!-- h2>< ?php __('Contas');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbConta">
	<tr>
		<th><?php __('Código da Conta');?></th>
		<th><?php __('CNPJ');?></th>
		<th><?php __('Operadora');?></th>
		<th><?php __('Telefone');?></th>
		<th><?php __('Gestor');?></th>
		<th><?php __('CPF Gestor');?></th>
		<th><?php __('RG Gestor');?></th>
		<th><?php __('Nascimento Gestor');?></th>
		<th><?php __('Login');?></th>
		<th><?php __('Senha');?></th>

		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($contas as $conta):
	?>
	<tr>
		<td><?php echo $conta['Conta']['conta']; ?>&nbsp;</td>
                <td><?php echo $this->Print->cnpj($conta['Conta']['cnpj']); ?>&nbsp;</td>
		<td><?php echo $conta['Operadora']['nome']; ?>&nbsp;</td>
		<td><?php echo $conta['Conta']['telefone']; ?>&nbsp;</td>
		<td><?php echo $conta['Gestor']['nome']; ?>&nbsp;</td>
		<td><?php echo $this->Print->cpf($conta['Gestor']['cpf']); ?>&nbsp;</td>
		<td><?php echo $conta['Gestor']['rg']; ?>&nbsp;</td>
		<td><?php echo $conta['Gestor']['nascimento']; ?>&nbsp;</td>
		<td><?php echo $conta['Conta']['login']; ?>&nbsp;</td>
		<td><?php echo $conta['Conta']['senha']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $conta['Conta']['id']; ?>

                    <div class="btn-group acoes">
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Conta" style="display: inline-block;"></i></a>
                        <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'contas/' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contas/add', $hasPenalidade) ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contas', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Contas</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

    $('#tbConta tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/conta/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
