<?php echo $this->Form->create('Contato', array('url' => array('controller' => 'contatos', 'action' => 'add', $coFornecedor))); ?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Dados do Contato</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                      <div class="span6">
                          <?php
                              echo $this->Form->input('co_contato');
                              echo $this->Form->hidden('co_fornecedor', array('value' => $coFornecedor));
                              echo $this->Form->input('tipo_contato', array('class' => 'input-xlarge','label' => 'Tipo de Contato','maxlength' => '30', 'type' => 'select', 'options' => $tipos, 'empty' => 'Selecione'));
                              echo $this->Form->input('nome', array('class' => 'input-xlarge','label' => 'Nome', 'maxlength' => '50'));
                              echo $this->Form->input('telefone1', array('class' => 'input-xlarge','label' => 'Telefone 1', 'maxlength' => '11', 'mask' => '(99) 9999-9999?9'));
                              echo $this->Form->input('telefone2', array('class' => 'input-xlarge','label' => 'Telefone 2', 'maxlength' => '11', 'mask' => '(99) 9999-9999?9'));
                          ?>
                      </div>
                      <div class="span6">
                          <?php
                          echo $this->Form->input('email', array('class' => 'input-xlarge','label' => 'Email', 'maxlength' => '50'));
                          ?>
                      </div>
                  </div>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

