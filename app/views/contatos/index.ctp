<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
	    <div class="acoes-formulario-top clearfix" >
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contatos', 'action' => 'add/' . $coFornecedor)); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Contatos"><i class="icon-plus icon-white"></i> Novo Contato</a>
          </div>
        </div>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>TIPO DE CONTATO</th>
		<th>NOME</th>
		<th>TELEFONE 1</th>
		<th>TELEFONE 2</th>
		<th>EMAIL</th>
		<th class="actions">AÇÕES</th>
	</tr>
	<?php
	$i = 0;
	foreach ($contatos as $contato){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $contato['Contato']['tipo_contato']; ?></td>
        <td><?php echo $contato['Contato']['nome']; ?></td>
        <td><?php echo $contato['Contato']['telefone1']; ?></td>
        <td><?php echo $contato['Contato']['telefone2']; ?></td>
        <td><?php echo $contato['Contato']['email']; ?></td>

		<td class="actions">
			<div class="btn-group acoes">
				<?php echo $this->element( 'actions2', array( 'id' => $contato['Contato']['co_contato'] . '/' . $coFornecedor, 'class' => 'btn', 'local_acao' => 'contatos/index' ) ); ?>
			</div>
		</td>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>