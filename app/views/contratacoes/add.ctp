<?php echo $this->Form->create('Contratacao', array('url' => array('controller' => 'contratacoes', 'action' => 'add', $modal))); ?>


    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Modalidade de Contratação'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                            <?php
                            echo $this->Form->input('co_contratacao');
                            echo $this->Form->input('ds_contratacao', array('class' => 'input-xlarge', 'label' => 'Descrição'));
                            ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <?php if( !$modal ): ?>
                    <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                <?php endif; ?>
            </div>
        </div>
    </div>

