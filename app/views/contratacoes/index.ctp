<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
		<div class="page-header position-relative"><h1><?php __('Modalidade de Contratação'); ?></h1></div>
		<?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratacoes/add') ): ?>
			<div class="acoes-formulario-top clearfix" >
				<p class="requiredLegend pull-left"><span class="required" title="Required">Campos com * são obrigatórios</span> </p>
				<div class="pull-right btn-group">
					<a href="<?php echo $this->Html->url(array('controller' => 'contratacoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Usuário"><i class="icon-plus icon-white"></i> Nova <?php __('Modalidade de Contratação'); ?></a> 
				</div>
			</div>
	      	<?php endif; ?>
		
	        <?php echo $this->Form->create('Contratacao', array('url' => array('controller' => 'contratacoes', 'action' => 'index'))); ?>
	        <div class="row-fluid">
	            <div class="span3">
	                <div class="controls">
	                    <? echo $this->Form->input('ds_contratacao', array('class' => 'input-xlarge','label' => 'Descrição','maxLength'=>'60')); ?>
	                </div>
	            </div>
	        </div>
	        <div class="form-actions">
	            <div class="btn-group">
	                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
	                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
	                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
	            </div>
	        </div>
		<?php echo $this->Form->end(); ?>
		
	<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
		<tr>
			<th><?php echo $this->Paginator->sort('Código', 'co_contratacao');?></th>
			<th><?php echo $this->Paginator->sort('Descrição', 'ds_contratacao');?></th>
			<th class="actions"><?php __('Ações');?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($contratacoes as $contratacao):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
		?>
		<tr <?php echo $class;?>>
			<td><?php echo $contratacao['Contratacao']['co_contratacao']; ?>&nbsp;</td>
			<td><?php echo $contratacao['Contratacao']['ds_contratacao']; ?>&nbsp;</td>
			<td class="actions">
				<div class="btn-group acoes">	
					<?php
	                    echo $this->element(
	                        'actions2', array(
	                            'id' => $contratacao['Contratacao']['co_contratacao'],
	                            'class' => 'btn',
	                            'local_acao' => 'contratacoes/index'
	                        )
	                    )
					?>
				</div>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
		));
		?>
	</p>
	
	<div class="pagination">
	    <ul>
	        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
	        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
	    </ul>
	</div>
</div>


