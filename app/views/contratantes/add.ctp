<div class="contratantes form"><?php echo $this->Form->create('Contratante');?>
<fieldset><legend><?php __('Contratante'); ?></legend> <?php
echo $this->Form->hidden('co_contratante');
echo $this->Form->input('ds_contratante', array('label' => 'Contratante'));
echo $this->Form->input('reg_contratante', array('label' => 'Registro'));
echo $this->Form->input('nu_contato', array('label' => 'Contato', 'mask'=>'(99) 9999-9999?9'));
echo $this->Form->input('no_responsavel', array('label' => 'Responsável'));
echo $this->Form->input('ds_observacao', array('label' => 'Observação','maxLength' => '255','type' => 'texarea', 'cols'=>'40', 'rows'=>'4'));

?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Listar Contratantes', true), array('action' => 'index'));?></li>
</ul>
</div>
