<div class="contratantes index">
<!-- h2>< ?php __('Contratantes');?></h2-->

<fieldset>
	<legend><?php __('Contratantes'); ?></legend>
	<?php echo $this->Form->create('Contratante');?>

<table cellpadding="0" cellspacing="0">
	<tr>
    	<td width="150px"><?php echo $this->Form->input('ds_contratante', array('label' => 'Contratante')); ?></td>
    	<td width="50px"><?php echo $this->Form->end(__('Filtrar', true));?></td>
        <td>&nbsp;</td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('Contratante', 'ds_contratante');?></th>
		<th><?php echo $this->Paginator->sort('Registro', 'reg_contratante');?></th>
		<th><?php echo $this->Paginator->sort('Responsável', 'no_responsavel');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($contratantes as $contratante):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $contratante['Contratante']['ds_contratante']; ?>&nbsp;</td>
		<td><?php echo $contratante['Contratante']['reg_contratante']; ?>&nbsp;</td>
		<td><?php echo $contratante['Contratante']['no_responsavel']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $contratante['Contratante']['co_contratante']; ?>		
		<?php echo $this->Html->link($this->Html->image('ico_alterar.gif'), array('action' => 'edit', $id), array('escape' => false)); ?>
		<?php echo $this->Html->link($this->Html->image('ico_excluir.gif'), array('action' => 'delete', $id), array('escape' => false), sprintf(__('Tem certeza de que deseja excluir # %s?', true), $id)); ?>

		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</fieldset>
</div>

<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Contratante', true), array('action' => 'add')); ?></li>
</ul>
</div>
