<?php //if(isset($filtro)) { debug($filtro);die; }

    if($coFiltro == null) {
        echo '<div class="page-header">';
        echo $this->Form->input('tp_filtro_avan', array('id'=>'tp_filtro_avan','class' => 'input-xxlarge chosen-select','type' => 'select', 'empty' => 'Selecione um Filtro...', 'label'=> __('Filtros de Pesquisa', true), 'options' => $tpFiltros));
        echo '</div>';
    } else {
        switch ( $filtro['Filtro']['tp_filtro'] ) {
            case 'fixo' :
                echo '<div class="advanced-search-filter">';
                echo '<input name="data[Filtro][' . $filtro['Filtro']['no_filtro'] . ']" type="radio" value="1" checked="checked" /> ' . $filtro['Filtro']['ds_filtro'];
                echo '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>';
                echo '<br>';
                echo '</div>';
                break;

            case 'money' :
                echo '<div class="advanced-search-filter">';
                echo '</div>';
                break;

            case 'txt' :
                echo '<div class="advanced-search-filter">';
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'], array('id'=>$filtro['Filtro']['no_filtro'],'class' => 'input-xlarge', 'label'=>$filtro['Filtro']['ds_filtro'], 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>'));
                echo '<br>';
                echo '</div>';
                break;

            case 'int' :
                echo '<div class="advanced-search-filter">';
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'], array('id'=>$filtro['Filtro']['no_filtro'],'class' => 'input-small', 'label'=>$filtro['Filtro']['ds_filtro'], 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>'));
                echo '<br>';
                echo '</div>';
                break;
            
            case 'select' :
                echo '<div class="advanced-search-filter">';
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.0', array('id'=>$filtro['Filtro']['no_filtro'], 'class' => 'input-xxlarge chosen-select', 'label'=>$filtro['Filtro']['ds_filtro'], 'type' => 'select', 'empty' => 'Selecione...', 'options' => $dominio, 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>'));
                echo $this->Form->hidden('Filtro.' . $filtro['Filtro']['no_filtro'] . '.1', array('id'=>$filtro['Filtro']['no_filtro'] . '2'));
                echo '<br>';
                echo '</div>';
                break;
            
            case 'date' :
                echo '<div class="advanced-search-filter">';
                echo $this->Html->script( 'inicia-datetimepicker' );
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'], array(
                      'before' => '<div class="input-append date datetimepicker">', 
                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>',
                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999', 'id'=>$filtro['Filtro']['no_filtro'],
                      'class' => 'input-small alert-tooltip','label' => $filtro['Filtro']['ds_filtro'], 'type'=>'text', 'title'=>'Informe uma data.'));
                echo '<br>';
                echo '</div>';
                break;
            
            case 'daterange' :
                echo '<div class="advanced-search-filter">';
                echo $this->Html->script( 'inicia-datetimepicker' );

                echo '<div class="input text">';
                echo '  <label for="' . $filtro['Filtro']['no_filtro'] . '">' . $filtro['Filtro']['ds_filtro'] . '</label>';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.0', array(
                      'div' => false, 'label' => false, 'before' => '<div class="input-append date datetimepicker">', 
                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999', 'id'=>$filtro['Filtro']['no_filtro'],
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Data Início'));
                
                echo '&nbsp; - &nbsp;';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.1', array(
                      'div' => false, 'label' => false, 'before' => '<div class="input-append date datetimepicker">', 
                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>',
                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999', 'id'=>$filtro['Filtro']['no_filtro'].'1',
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Data Fim'));
                
                echo '</div>';
                echo '<br>';
                echo '</div>';
                break;
            
            case 'moneyrange' :
                echo '<div class="advanced-search-filter">';
                echo '<div class="input text">';
                echo '  <label for="' . $filtro['Filtro']['no_filtro'] . '">' . $filtro['Filtro']['ds_filtro'] . '</label>';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.0', array(
                      'div' => false, 'label' => false, 'id'=>$filtro['Filtro']['no_filtro'],
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Valor Inicial'));
                
                echo '&nbsp; - &nbsp;';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.1', array(
                      'div' => false, 'label' => false, 'id'=>$filtro['Filtro']['no_filtro'].'1',
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Valor Final'));
                
                echo '</div>';
                echo '<br>';
                echo '</div>';
                break;
            
            case 'intrangedate' :
                echo '<div class="advanced-search-filter">';
                echo '<div class="input text">';
                echo '  <label for="' . $filtro['Filtro']['no_filtro'] . '">' . $filtro['Filtro']['ds_filtro'] . '</label>';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.0', array(
                      'div' => false, 'label' => false, 'id'=>$filtro['Filtro']['no_filtro'],
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Dia Inicial'));
                
                echo '&nbsp; - &nbsp;';
                
                echo $this->Form->input('Filtro.' . $filtro['Filtro']['no_filtro'] . '.1', array(
                      'div' => false, 'label' => false, 'id'=>$filtro['Filtro']['no_filtro'].'1',
                      'class' => 'input-small alert-tooltip', 'type'=>'text', 'title'=>'Dia Final', 'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip btn-del-filter" style="cursor:pointer;" title="Excluir"><i class="icon-remove icon-2x red"></i></a>'));
                
                echo '</div>';
                echo '<br>';
                echo '</div>';
                break;
        }
    }
?>

<script type="text/javascript">
    $('.btn-del-filter').click(function () {
        $(this).closest('.advanced-search-filter').remove();
    });
</script>

<?php echo $this->Html->scriptStart() ?>

$(document).ready(function(){
    <?php if($coFiltro == null) { ?>
    $('#tp_filtro_avan').bind('change', function(e){
        
        var url_add_campo = "<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'add_campo')); ?>/" + $(this).val();
        $.ajax({
            type:"POST",
            url:url_add_campo,
            success:function(result){ // **** PAREI AQUI
                //$('#id_pg_execucao').html("");
                $('#camposPesquisa').append(result);
            }
        })
        
    });
    <?php } else {
        
            if($filtro['Filtro']['tp_filtro'] == 'select') {
    ?>
                $('#<?php echo $filtro['Filtro']['no_filtro']; ?>').bind('change', function(e){
                    $('#<?php echo $filtro['Filtro']['no_filtro']; ?>2').val( $('#<?php echo $filtro['Filtro']['no_filtro']; ?> :selected').text() );
                });
    <?php                
            }
            
            if($filtro['Filtro']['tp_filtro'] == 'money' || $filtro['Filtro']['tp_filtro'] == 'moneyrange') {
            ?>
                $("#<?php echo $filtro['Filtro']['no_filtro']; ?>").maskMoney({thousands:'.', decimal:','});
            <?php
            }
            if($filtro['Filtro']['tp_filtro'] == 'moneyrange') {
            ?>
                $("#<?php echo $filtro['Filtro']['no_filtro']; ?>1").maskMoney({thousands:'.', decimal:','});
            <?php
            }
            if($filtro['Filtro']['tp_filtro'] == 'int' || $filtro['Filtro']['tp_filtro'] == 'intrangedate') {
            ?>
                $("#<?php echo $filtro['Filtro']['no_filtro']; ?>").maskMoney({precision:0, allowZero:false, thousands:''});
            <?php
            }
            if($filtro['Filtro']['tp_filtro'] == 'intrangedate') {
            ?>
                $("#<?php echo $filtro['Filtro']['no_filtro']; ?>1").maskMoney({precision:0, allowZero:false, thousands:''});
            <?php
            }
            ?>
    
    <?php } ?>

    $('.alert-tooltip').tooltip();
    $(".chosen-select").chosen();

});

<?php echo $this->Html->scriptEnd() ?>