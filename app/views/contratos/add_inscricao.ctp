<?php 
        echo $this->Html->script( 'fuelux/fuelux.wizard.min' );
        //echo $this->Html->script( 'select2.min' );
        echo $this->Html->script( 'jquery.validate.min' );
        echo $this->Html->script( 'inicia-datetimepicker' ); 
?>

<!--?php echo $this->Form->create('Inscricao', array('url' => "/contratos/add_inscricao"));?-->
	
    <div class="row-fluid">

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar uma Inscrição<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'index')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Listar">Listagem</a> 
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
          </div>
        </div>
            
        <div class="row-fluid">
                        
            <div class="span12">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Inscrição</h4></div>

                <div class="widget-body">
                    <div class="widget-main">
                        
                        <div class="row-fluid">
                                <div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
                                        <ul class="wizard-steps">
                                                <li data-target="#step1" class="active">
                                                        <span class="step">1</span>
                                                        <span class="title">Dados do Candidado</span>
                                                </li>

                                                <li data-target="#step2">
                                                        <span class="step">2</span>
                                                        <span class="title">Inscrição</span>
                                                </li>

                                                <li data-target="#step3">
                                                        <span class="step">3</span>
                                                        <span class="title">Informações Adcionais</span>
                                                </li>
                                        </ul>
                                </div>

                                <hr />
                                <div class="step-content row-fluid position-relative" id="step-container">
                                    
                                    <?php echo $this->Form->create('Fornecedor', array('url' => array('controller' => 'contratos', 'action' => 'add_inscricao'), 'id' => 'form_inscricao'));?>
                                        <div class="step-pane active" id="step1">
                                          <div class="row-fluid">
                                            <div class="span12 ">
                                                <div class="widget-header widget-header-small"><h4><?php __('Dados Pessoais'); ?></h4></div>
                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                      <div class="row-fluid">
                                                        <div class="span4">
                                                    <?php
                                                       echo $this->Form->hidden('co_fornecedor');
                                                       echo $this->Form->hidden('tp_fornecedor', array('value'=>'F'));
                                                       echo $this->Form->input('nu_cnpj', array('class' => 'input-xlarge', 'label' => 'CPF','mask' =>'999.999.999-99'));
                                                       echo $this->Form->input('no_razao_social', array('class' => 'input-xlarge', 'label' => 'Nome'));
                                                       echo $this->Form->input('tp_sexo', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Sexo', 'options' => array('M' => 'Masculino', 'F' => 'Feminino')));
                                                       echo $this->Form->input('tp_raca', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Raça / Cor', 'options' => $racas));
                                                    ?>
                                                        </div>
                                                        <div class="span4">
                                                    <?php 
                                                       echo $this->Form->input('dt_nascimento', array(
                                                                        'before' => '<div class="input-append date datetimepicker">', 
                                                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                                                        'class' => 'input-small','label' => 'Data de Nascimento', 'type'=>'text'));
                                                        echo $this->Form->input('nu_identidade', array('class' => 'input-xlarge', 'label' => 'Identidade'));
                                                        echo $this->Form->input('ds_orgao', array('class' => 'input-xlarge', 'label' => 'Órgão Expedidor'));
                                                        echo $this->Form->input('dt_expedicao', array(
                                                                        'before' => '<div class="input-append date datetimepicker">', 
                                                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                                                        'class' => 'input-small','label' => 'Data de Expedição', 'type'=>'text'));
                                                    ?>
                                                        </div>
                                                        <div class="span4">
                                                    <?php 
                                                        echo $this->Form->input('tp_graduacao', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Escolaridade', 'options' => array('1' => '1 Grau', '2' => '2 Grau', '3' => 'Superior')));
                                                        echo $this->Form->input('sg_uf_origem', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Estado de Nascimento', 'empty' => 'Selecione...', 'options' => $estados));
                                                        echo $this->Form->input('co_municipio_origem', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Cidade de Nascimento', 'empty' => 'Selecione...'));
                                                        echo $this->Form->input('ic_patrocinio', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Patrocinado', 'options' => array('S' => 'Sim', 'N' => 'Não')));
                                                    ?>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                           </div>
                                            
                                           <div class="row-fluid">
                                            <div class="span8 ">
                                                <div class="widget-header widget-header-small"><h4><?php __('Endereço'); ?></h4></div>
                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                      <div class="row-fluid">
                                                        <div class="span6">
                                                        <?php
                                                        echo $this->Form->input('ds_endereco', array('class' => 'input-xlarge', 'label' => 'Endereço'));
                                                        echo $this->Form->input('nu_endereco', array('class' => 'input-xlarge', 'label' => 'Número'));
                                                        echo $this->Form->input('nu_cep', array('class' => 'input-xlarge', 'label' => 'Cep', 'mask' => '99999-999'));
                                                        ?>
                                                        </div>
                                                        <div class="span4">
                                                        <?php
                                                        echo $this->Form->input('sg_uf', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Estado', 'empty' => 'Selecione...', 'options' => $estados ));
                                                        echo $this->Form->input('co_municipio', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Cidade', 'empty' => 'Selecione...'));
                                                        ?>
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                               
                                            <div class="span4 ">
                                                <div class="widget-header widget-header-small"><h4><?php __('Contato'); ?></h4></div>
                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                        <?php
                                                        echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail'));
                                                        echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone'));
                                                        echo $this->Form->input('nu_fax', array('class' => 'input-xlarge', 'label' => 'Celular'));
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                    
                                        <div class="step-pane" id="step2">
                                            <div class="span4">
                                            <?php
                                            //echo $this->Form->input('co_contratacao', array('class' => 'input-xlarge', 'type' => 'select', 'label' => __('Modalidade de Contratação', true), 'options' => $tipos));
                                            echo $this->Form->input('co_confederacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Confederação do Atleta', 'options' => $confederacoes));
                                            echo $this->Form->input('co_federacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Federação do Atleta'));
                                            ?>
                                            </div>
                                            <div class="span4">
                                            <?php
                                            echo $this->Form->input('co_modalidade', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Modalidade'));
                                            echo $this->Form->input('co_prova', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Prova'));
                                            ?>
                                            </div>
                                            <div class="span4">
                                            <?php
                                            echo $this->Form->input('co_evento', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Evento ou Competição'));
                                            echo $this->Form->input('co_classificacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Classificação Obtida', 'options' => $classificacoes));
                                            ?>                                                
                                            </div>
                                        </div>
                                    <?php echo $form->end(); ?>

                                        <div class="step-pane" id="step3">
                                            <div class="row-fluid">
                                            <ol>
                                                    <li>Recomendamos a impressão da ficha de inscrição on-line e o acesso à Área Restrita no mínimo semanalmente.</li>
                                                    <li>A maior parte da comunicação entre o Atleta Candidato e o Programa Bolsa-Atleta é realizada por meio de e-mail, por isso, matenha seu endereço eletrônico atualizado em sua ficha de inscrição e, em caso de dúvida envie para o e-mail duvidasbolsa@esporte.gov.br.</li>
                                                    <li>
                                                            Você concluiu apenas a inscrição on-line, primeira etapa do pleito à Bolsa-Atleta. Para concorrer ao benefício você deve enviar os documentos exigidos pela legislação vigente, com modelos disponíveis na página Sitio Bolsa Atleta, no menu "Inscrições". São eles:<br>
                                                            <i class="icon-check"></i> Cópia de RG e CPF;<br>
                                                            <i class="icon-check"></i> Declaração da Instituição de Ensino - Somente categoria Estudantil;<br>
                                                            <i class="icon-check"></i> Declaração da Entidade de Prática (CLUBE);<br>
                                                            <i class="icon-check"></i> Declaração da Entidade Nacional (CONFEDERAÇÃO);<br>
                                                    </li>
                                                    <li>Os documentos devem ser postados ou protocolados no endereço abaixo, no máximo até 30 dias após o fim das incrições on-line, sob pena de indeferimento da inscrição.</li>
                                            </ol>
                                            <h6>ENDEREÇO PARA POSTAGEM OU PROTOCOLO DE DOCUMENTOS:</h6>
                                            <p>MINISTÉRIO DO ESPORTE</p>
                                            <p>SECRETÁRIA NACIONAL DE ESPORTE DE ALTO REDIMENTO</p>
                                            <p>BOLSA-ATLETA</p>
                                            <p>SAN - Quadra 03 Bloco A Ed. Núcleo dos Transportes - 1º Andar. Sala 12.49</p>
                                            <p>CEP: 70040-902 - Brasília - DF</p>
                                            </div>
                                        </div>
                                </div>

                                <hr />
                                <div class="row-fluid wizard-actions">
                                        <button class="btn btn-small btn-prev">
                                                <i class="icon-arrow-left"></i>
                                                Anterior
                                        </button>

                                        <button class="btn btn-small btn-success btn-next" data-last="Salvar ">
                                                Próxima
                                                <i class="icon-arrow-right icon-on-right"></i>
                                        </button>
                                </div>
                        </div>
                        
                    </div>
                </div>
              </div>
            </div>
              
        </div>
                  
</div>

        <div id="dialog-candidato" class="hide">
                <div class="alert alert-info bigger-110">
                    Já existe um Candidato cadastrado com o CPF informado.<br>
                    Deseja utilizar este cadastro?
                </div>
        </div>

        <div id="dialog-alerta" class="hide">
                <div class="alert alert-info bigger-110">
                    Favor preencher todos os campos para continuar.
                </div>
        </div>

<script type="text/javascript">
jQuery(function($) {
                            
    $("#FornecedorCoConfederacao").change(function()
        {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'confederacoes', 'action' => 'listar')) ?>" + "/" + $(this).val(), null, function(data) {
                var options = '<option value="">Selecione...</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                  });
                $("select#FornecedorCoFederacao").html(options);
                $("#FornecedorCoFederacao").trigger("chosen:updated");
            })
            
           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#FornecedorCoModalidade").html(options);
                   $("#FornecedorCoModalidade").trigger("chosen:updated");
           })
            
        });
        
    $('#FornecedorCoModalidade').change(function(){

        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar_provas') )?>" + "/" + $(this).val(), null, function(data){
               var options = '<option value="">Selecione...</option>';
               $.each(data, function(index, val) {
                        options += '<option value="' + index + '">' + val + '</option>';
                          });
               $("select#FornecedorCoProva").html(options);
               $("#FornecedorCoProva").trigger("chosen:updated");
        })

    });
    
    $('#FornecedorCoProva').change(function(){

        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'eventos', 'action' => 'listar') )?>" + "/" + $(this).val() + "/" + $('#FornecedorTpSexo').val(), null, function(data){
               var options = '<option value="">Selecione...</option>';
               $.each(data, function(index, val) {
                        options += '<option value="' + index + '">' + val + '</option>';
                          });
               $("select#FornecedorCoEvento").html(options);
               $("#FornecedorCoEvento").trigger("chosen:updated");
        })

    });
        
    $("#FornecedorSgUfOrigem").change(function()
        {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'municipios', 'action' => 'listar')) ?>" + "/" + $(this).val(), null, function(data) {
                var options = '<option value="">Selecione...</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                  });
                $("select#FornecedorCoMunicipioOrigem").html(options);
                $("#FornecedorCoMunicipioOrigem").trigger("chosen:updated");
            })
        });
        
        $("#FornecedorSgUf").change(function()
        {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'municipios', 'action' => 'listar')) ?>" + "/" + $(this).val(), null, function(data) {
                var options = '<option value="">Selecione...</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                  });
                $("select#FornecedorCoMunicipio").html(options);
                $("#FornecedorCoMunicipio").trigger("chosen:updated");
            })
        });
        
        if($("#FornecedorCoFornecedor").val() != "") {
            $( "#FornecedorNuCnpj" ).attr('readonly', true);
            $( "#FornecedorNoRazaoSocial" ).attr('readonly', true);
        }
        
        $( "#FornecedorNuCnpj" ).on('focusout', function(e) {
            if($(this).val() != '' && $("#FornecedorCoFornecedor").val() == "") {
                $.ajax({
                    type:"POST",
                    url:'<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'find_candidato') )?>',
                    data:{
                        "data[nu_cpf]":$(this).val()
                    },
                    success:function(result){
                        if(result > 0) {
                            e.preventDefault();

                            $( "#dialog-candidato" ).dialog({
                                    resizable: false,
                                    modal: true,
                                    title: 'Candidato já cadastrado!',
                                    title_html: true,
                                    buttons: [
                                            {
                                                html: "<i class='icon-ok bigger-110'></i>&nbsp; Confirma",
                                                "class" : "btn btn-success btn-mini",
                                                click: function() {
                                                        $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'add_inscricao') )?>/' + result);
                                                }
                                            }
                                            ,
                                            {
                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancela",
                                                "class" : "btn btn-mini",
                                                click: function() {
                                                        $( this ).dialog( "close" );
                                                }
                                            }
                                    ]
                            });
                        }
                    }
                })
            }
        });
			
        $('[data-rel=tooltip]').tooltip();

        $('#fuelux-wizard').ace_wizard().on('change' , function(e, info){
                if(info.step == 1) {
                    
                    $('#form_inscricao').validate({
                        errorClass: 'error-message',
                        rules: {
                            "data[Fornecedor][nu_cnpj]": {
                                required: true
                            },
                            "data[Fornecedor][no_razao_social]": {
                                required: true
                            },
                            "data[Fornecedor][tp_sexo]": {
                                required: true
                            },
                            "data[Fornecedor][tp_raca]": {
                                required: true
                            },
                            "data[Fornecedor][dt_nascimento]": {
                                required: true
                            },
                            "data[Fornecedor][nu_identidade]": {
                                required: true
                            },
                            "data[Fornecedor][ds_orgao]": {
                                required: true
                            },
                            "data[Fornecedor][dt_expedicao]": {
                                required: true
                            },
                            "data[Fornecedor][tp_graduacao]": {
                                required: true
                            },
                            "data[Fornecedor][sg_uf_origem]": {
                                required: true
                            },
                            "data[Fornecedor][co_municipio_origem]": {
                                required: true
                            },
                            "data[Fornecedor][ds_endereco]": {
                                required: true
                            },
                            "data[Fornecedor][nu_endereco]": {
                                required: true
                            },
                            "data[Fornecedor][nu_cep]": {
                                required: true
                            },
                            "data[Fornecedor][sg_uf]": {
                                required: true
                            },
                            "data[Fornecedor][co_municipio]": {
                                required: true
                            },
                            "data[Fornecedor][ds_email]": {
                                required: true,
                                email: true
                            },
                            "data[Fornecedor][nu_telefone]": {
                                required: true,
                            },
                            "data[Fornecedor][nu_fax]": {
                                required: true
                            }
                        },
                        invalidHandler: function (event, validator) { //display error alert on form submit   
                            $( "#dialog-alerta" ).dialog({
                                    resizable: true,
                                    modal: true,
                                    title: 'Alerta!',
                                    title_html: true,
                            });
                        }
                    });
                    
                    if(!$('#form_inscricao').valid()) { 
                        return false; 
                    }
                }
                
                if(info.step == 2) {
                    
                    $('#FornecedorCoConfederacao').rules( "add", {
                        required: true
                    });
                    $('#FornecedorCoFederacao').rules( "add", {
                        required: true
                    });
                    $('#FornecedorCoModalidade').rules( "add", {
                        required: true
                    });
                    $('#FornecedorCoProva').rules( "add", {
                        required: true
                    });
                    $('#FornecedorCoEvento').rules( "add", {
                        required: true
                    });
                    $('#FornecedorCoClassificacao').rules( "add", {
                        required: true
                    });
                    
                    if(!$('#form_inscricao').valid()) { 
                        return false; 
                    }
                }
        }).on('finished', function(e) {
            $('#form_inscricao').submit();
        }).on('changed', function(e){
                $("#FornecedorCoConfederacao").addClass("chosen-select");
                $("#FornecedorCoFederacao").addClass("chosen-select");
                $("#FornecedorCoModalidade").addClass("chosen-select");
                $("#FornecedorCoProva").addClass("chosen-select");
                $("#FornecedorCoEvento").addClass("chosen-select");
                $(".chosen-select").chosen();
        });
        
        $('#FornecedorNuTelefone').focusout(function(){
            var phone, element;
            element = $(this);
            element.unmask();
            phone = element.val().replace(/\D/g, '');
            if(phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        }).trigger('focusout');
        
        $('#FornecedorNuFax').focusout(function(){
            var phone, element;
            element = $(this);
            element.unmask();
            phone = element.val().replace(/\D/g, '');
            if(phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        }).trigger('focusout');

})
</script>