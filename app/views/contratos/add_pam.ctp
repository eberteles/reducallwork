<?php
$usuario = $this->Session->read('usuario');
?>
<?php echo $this->Form->create('Contrato', array('type' => 'file', 'url' => array('controller' => 'contratos', 'action' => 'add_pam'))); ?>

    <div class="row-fluid">
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">
                Preencha os campos abaixo para adicionar uma <?php __('PAM'); ?>
            </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'index')); ?>"
               data-toggle="modal" class="btn btn-small btn-primary" title="Listar">Listagem</a>
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span8">
            <div class="widget-box">
                <div class="widget-header widget-header-small"><h4><?php __('PAM'); ?></h4></div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span6">
                                <dl class="dl-horizontal">
                                    <?php
                                    if (isset($usuario['Usuario']['co_instituicoes'])) {
                                        echo $this->Form->hidden('co_instituicao', array('value' => $usuario['Usuario']['co_instituicoes']));
                                    }
//                                    echo $this->Form->input('nu_pam',
//                                        array(
//                                            'class' => 'input-xlarge',
//                                            'label' => 'Nº ' . __('PAM', true),
//                                        )
//                                    );

//                                    echo $this->Form->input('co_contratante',
//                                        array(
//                                            'class' => 'input-xlarge chosen-select',
//                                            'type' => 'select',
//                                            'empty' => 'Selecione...',
//                                            'label' => 'Unidade Solicitante',
//                                            // 'options' => array('1' => 'teste', '2' => 'outro'),
//                                            'options' => $imprimir->getArraySetores($contratantes),
//                                            'escape' => false)
//                                    );
                                    
                                        echo $this->Form->input('co_cliente', array(
                                                                    'class' => 'input-xlarge chosen-select',
                                                                    'type' => 'select',
                                                                    'empty' => 'Selecione...',
                                                                    'label' => 'Cliente',
                                                                    'options' => $clientes,
                                                                  ));
                                        
                                    echo $this->Form->input('co_parceiro', array('label' => 'Responsável / Parceiro', 'class' => 'input-xlarge chosen-select', 
                                        'type' => 'select', 'empty' => 'Selecione...', 'options' => $usuarios));
                                    
                                    echo $this->Form->input('co_gestor_atual', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Responsável Reducall', true), 'options' => $gestores, 'selected' => $usuario['Usuario']['co_usuario'], 'onchange' => 'validaGestorSuplente(this)'));
                                    
                                    echo $this->Form->input('vl_global', array('class' => 'input-xlarge', 'label' => __('Valor Estimado das Contas', true), 'maxlength' => 18));

                                    if (!$this->Modulo->isSuporteDocumental()) {
                                        if ($this->Modulo->isCamposContrato('tp_aquisicao')) {
                                            $sn = '';
                                            echo $this->Form->input('tp_aquisicao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Tipo de Aquisição', true), 'options' => array('S' => 'Serviços', 'N' => 'Materiais', 'SN' => 'Serviços e Materiais')));
                                        }

                                        if ($this->Modulo->isCamposContrato('ic_dispensa_licitacao')) {
                                            echo $this->Form->input('ic_dispensa_licitacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Dispensa Licitação', true), 'options' => array('S' => 'Sim', 'N' => 'Não')));
                                        }

                                        if ($this->Modulo->isCamposContrato('ic_ata_vigente')) {
                                            echo $this->Form->input('ic_ata_vigente', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Possui Ata Vigente', true), 'options' => array('S' => 'Sim', 'N' => 'Não')));
                                        }

                                        if ($this->Modulo->isPamFornecedor) {
                                            echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'options' => $fornecedores,
                                                'after' => $this->Print->getBtnEditCombo('Novo ' . $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'AbrirNovoFornecedor', '#view_novo_fornecedor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/add'))));
                                        }
                                    }

                                    echo '<div id="ic_ata_orgao" style="display:none;">';
                                    if ($this->Modulo->isCamposContrato('ic_ata_orgao')) {
                                        echo $this->Form->input('ic_ata_orgao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Ata Pertence ao Órgão', true), 'options' => array('S' => 'Sim', 'N' => 'Não')));
                                    }
                                    echo '</div>';

                                    ?>


                                </dl>
                            </div>
                            <div class="span5">
                                <dl class="dl-horizontal">

                                    <?php
                                    echo $this->Form->input('dt_autorizacao_pam', array(
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data Inicial', 'type' => 'text'));

//                                    echo $this->Form->input('ds_tipo_servico', array(
//                                            'class' => 'input-xlarge',
//                                            'label' => 'Tipo de Serviço'
//                                        )
//                                    );

//                                    echo $this->Form->input('ds_objeto', array('class' => 'input-xlarge', 'label' => 'Objeto', 'type' => 'texarea', 'cols' => '40', 'rows' => '4',
//                                        'onKeyup' => '$(this).limit("250","#charsLeft")',
//                                        'after' => '<br><span id="charsLeft">250</span> caracteres restantes.'));
//                                    echo '<br />';
                                    
                                    echo $this->Form->input('co_fase', array('label' => 'Andamento', 'class' => 'input-xlarge chosen-select', 
                                        'type' => 'select', 'empty' => 'Selecione...', 'value'=>1, 'options' => $fases));
                                    
                                    echo $this->Form->input('ds_andamento', array('class' => 'input-xlarge', 'label' => 'Observação', 'type' => 'texarea', 'cols' => '40', 'rows' => '4',
                                        'onKeyup' => '$(this).limit("250","#charsLeft")',
                                        'after' => '<br><span id="charsLeft">250</span> caracteres restantes.'));
                                    echo '<br />';

                                    echo $this->Form->input('anexo', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo da ' . __('PAM', true) . ' - 1º'));
                                    echo $this->Form->input('anexo2', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo da ' . __('PAM', true) . ' - 2º'));
                                    echo $this->Form->input('anexo3', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo da ' . __('PAM', true) . ' - 3º'));

                                    ?>

                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="form-actions">
		<div class="btn-group">
			<button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar <?php __('PAM'); ?>"> Salvar</button>
			<button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
			<button rel="tooltip" type="button" title="Voltar" class="btn btn-small"> Voltar</button>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<!-- APRESENTACAO UBN -->
<div id="view_form_memorando" class="modal hide fade maior"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <p class="pull-right">
            <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i>
                Fechar</a>
        </p>
        <h3 id="tituloAbaModelo">Memorando</h3>
    </div>
    <div id="conteudoModelo" style="padding: 10px;">

        <div class="row-fluid">
            <div class="widget-header widget-header-small">
                <h4>Dados do documento:</h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="input text required">
                        <label>Número:</label><input type="text" class="input-xlarge"
                                                     style="width: 100px;"/>
                    </div>
                    <div class="input text">
                        <label>Sigla:</label><input type="text" class="input-xlarge"
                                                    style="width: 100px;"/>
                    </div>
                    <div class="input text required">
                        <div class="input-append date datetimepicker">
                            <label>Data de Publicação</label> <input type="text"
                                                                     data-format="dd/MM/yyyy" mask="99/99/9999"
                                                                     class="input-small"/><span
                                    class="add-on"><i data-date-icon="icon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="input text required">
                        <label>Nome do Signatário:</label><input type="text"
                                                                 class="input-xlarge" style="width: 300px;"/>
                    </div>
                    <div class="input text required">
                        <label>Cargo do Signatário:</label><input type="text"
                                                                  class="input-xlarge" style="width: 300px;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="button"
                        class="btn btn-small btn-primary bt-pesquisar"
                        title="Gerar Documento"
                        onclick='$("#dl")[0].src="<?php echo $this->Html->url(array('controller' => 'modelos', 'action' => 'gerarMemorando'))?>"'>Gerar Documento
                </button>
            </div>
        </div>

    </div>
</div>
</div>

<iframe id="dl" style="display: none"></iframe>
<!-- FIM APRESENTACAO UNB -->

<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<?php echo $this->Html->scriptStart() ?>

$("#ContratoVlGlobal").maskMoney({thousands: '.', decimal: ','});

<?php if ($this->Modulo->isCamposContrato('ic_ata_orgao')) { ?>
    $("#ContratoIcAtaVigente").change( function() {
    if($(this).val() == 'S') {
    $('#ic_ata_orgao').css("display", "");
    } else {
    $('#ic_ata_orgao').css("display", "none");
    $("#ContratoIcAtaOrgao").val("");
    }
    });
<?php } ?>

$('.upload-arquivo').ace_file_input({
no_file:'Nenhum arquivo selecionado ...',
btn_choose:'Selecionar',
btn_change:'Alterar',
droppable:false,
// onchange:'C',
thumbnail:false //| true | large
//whitelist:'gif|png|jpg|jpeg'
//blacklist:'exe|php'
//onchange:''
//
});

$('a.remove').on('click', function(e) {
$(e.target).closest('div').find('input').ace_file_input('reset_input');
});

<?php echo $this->Html->scriptEnd() ?>
