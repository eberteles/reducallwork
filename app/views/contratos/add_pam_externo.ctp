<?php
//$usuario = $this->Session->read('usuario');
echo $this->Html->script('inicia-datetimepicker');
echo $this->Html->script( 'jquery.validate.min' );
?>
<?php echo $this->Form->create('Contrato', array('type' => 'file', 'url' => "/contratos/add_pam_externo/$tipoPam", 'id' => 'form_add_externo'));?>

    <div class="row-fluid">

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um <?php __('PAM'); ?><!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
          </div>
        </div>

        <div class="row-fluid">

            <div class="span6">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Informações Básicas</h4></div>

				<div class="widget-body">
					<div class="widget-main">
						<div class="row-fluid">
							<div class="span6">
								<dl class="dl-horizontal">
                              <?php
                                      if(isset($usuario['Usuario']['co_instituicoes'])){
                                           echo $this->Form->hidden('co_instituicao', array('value'=>$usuario['Usuario']['co_instituicoes']));
                                      }

                                      echo $this->Form->input('co_contratante',   array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Unidade Solicitante', 'options' => $imprimir->getArraySetores($contratantes), 'escape' => false));

                                      echo $this->Form->input('ds_observacao', array('class' => 'input-xlarge','label'=>'Observação','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                            'onKeyup'=>'$(this).limit("250","#charsLeft1")',
                                            'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'));
                                      echo '<br />';

                                      if($tipoPam == 2) {
                                        echo $this->Form->hidden('ic_ata_vigente', array('value'=>'S'));
                                        echo $this->Form->input('ic_ata_orgao',   array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Ata do HFA?', 'options' => array('S'=>'Sim', 'N'=>'Não')));
                                      }

                                    ?>
								</dl>
							</div>
							<div class="span6">
								<dl class="dl-horizontal">

                              <?php

                                      echo $this->Form->input('anexo', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo do '  . __('PAM', true) . ' - 1º') );
                                      echo $this->Form->input('anexo2', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo do '  . __('PAM', true) . ' - 2º') );
                                      echo $this->Form->input('anexo3', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo do '  . __('PAM', true) . ' - 3º') );

                                      if($tipoPam == 2) {
                                        echo '<div id="divGerenciadorAta" style="display: none;">';
                                        echo $this->Form->input('ds_gerenciador_ata', array('class' => 'input-xlarge', 'label'=>'Gerenciador da Ata'));
                                        echo '</div>';
                                      }
                              ?>

                              </dl>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
            </div>

            <div class="span6">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Informações do Requisitante</h4></div>

                    <div class="widget-body">
			<div class="widget-main">
                            <div class="row-fluid">
				<div class="span6">
                                    <dl class="dl-horizontal">
                              <?php
                                    echo $this->Form->input('ds_requisitante', array('class' => 'input-xlarge', 'label'=>'Nome Completo do Requisitante'));

                                    echo $this->Form->input('tl_requisitante', array('class' => 'input-xlarge', 'label' => 'Telefone do Requisitante', 'mask' => '(99) 9999-9999?9'));

                                    echo $this->Form->input('ds_assunto', array('class' => 'input-xlarge','label'=>'Assunto do Pedido','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("250","#charsLeft2")',
                                            'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'));

                                    ?>
                                    </dl>
				</div>
                                <div class="span6">
                                    <dl class="dl-horizontal">
                              <?php
                                    echo $this->Form->input('pt_requisitante', array('class' => 'input-xlarge', 'label'=>'Posto/Grad/Cat. Func. do Requisitante'));
                                    echo $this->Form->input('dt_solicitacao', array(
                                        'value'=> date('d/m/Y'),
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                        'class' => 'input-small','label' => 'PAM solicitado em:', 'type'=>'text'));

                              ?>
                                    </dl>
				</div>
                            </div>
			</div>
                    </div>
		</div>
            </div>

        </div>

    <?php if($tipoPam == 1 || $tipoPam == 3) { ?>
        <div class="row-fluid">

            <div class="span12">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Informações do Plano de Trabalho</h4></div>

                <?php if($tipoPam == 1) { ?>
                    <div class="widget-body">
			<div class="widget-main">
                            <div class="row-fluid">
				<div class="span4">
                              <?php
                                    echo $this->Form->input('ds_objeto_plano', array('class' => 'input-xlarge','label'=>'Objeto <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: Aquisição de scanner de mesa com base plana.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft3")',
                                            'after' => '<br><span id="charsLeft3"></span> caracteres restantes.'));
                                    echo '<BR>';
                                    echo $this->Form->input('ds_vantagem', array('class' => 'input-xlarge','label'=>'Vantagens e Economicidade para a Administração <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: A digitalização dos documentos, através dos equipamentos solicitados, permitirá economia de papel, espaço físico e contribuirá para a maior segurança no trâmite dos processos administrativos, já que os documentos tramitarão em meio digital.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft4")',
                                            'after' => '<br><span id="charsLeft4"></span> caracteres restantes.'));
                                    echo '<BR>';
                                    echo $this->Form->input('ds_criterio', array('class' => 'input-xlarge','label'=>'Critérios de Controles e Registros a Serem Adotados <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: A Secretaria Geral e a Divisão de Finanças estabelecerão critérios de controle e registro para utilização e manutenção dos equipamentos.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft5")',
                                            'after' => '<br><span id="charsLeft5"></span> caracteres restantes.'));
                                    ?>
                                    </dl>
				</div>
                                <div class="span3">
                                    <dl class="dl-horizontal">
                              <?php
                                    echo $this->Form->input('ds_justificativa', array('class' => 'input-xlarge','label'=>'Justificativa para Aquisição <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: Implantação de arquivo digital de documentos da Secretaria Geral do HFA e da Divisão de Finanças.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft6")',
                                            'after' => '<br><span id="charsLeft6"></span> caracteres restantes.'));
                                    echo '<BR>';
                                    echo $this->Form->input('ds_resultado', array('class' => 'input-xlarge','label'=>'Verificação dos Resultados <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: Os setores solicitantes executarão a verificação dos resultados.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft7")',
                                            'after' => '<br><span id="charsLeft7"></span> caracteres restantes.'));
                              ?>
                                    </dl>
				</div>
                                <div class="span5">
                                    <dl class="dl-horizontal">
                              <?php
                                    echo $this->Form->input('ds_demanda', array('class' => 'input-xlarge','label'=>'Demanda Prevista e Quantidade Bens e Serviços a serem Contratados <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: Serão necessárias duas unidades: uma para operação na Secretaria Geral e outra para a Divisão de Finanças. Não se aplica a estimativa de consumo mensal.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft8")',
                                            'after' => '<br><span id="charsLeft8"></span> caracteres restantes.'));
                                    echo '<BR>';
                                    echo $this->Form->input('ds_aproveitamento', array('class' => 'input-xlarge','label'=>'Aproveitamento de Servidores do Quadro, de Bens, de Equipamentos <span class="help-button" data-rel="popover" data-trigger="hover" data-content="Exemplo: Os servidores lotados na Secretaria Geral e na Divisão de Finanças realizarão a fiscalização dos equipamentos, desde o recebimento, que terá início com os Servidores lotados no Almoxarifado, até a plena execução dos trabalhos aos quais os equipamentos, citados no item 1, se destinam.">?</span>','type' => 'texarea', 'cols'=>'40', 'rows'=>'2',
                                            'onKeyup'=>'$(this).limit("500","#charsLeft9")',
                                            'after' => '<br><span id="charsLeft9"></span> caracteres restantes.'));
                              ?>
                                    </dl>
				</div>
                            </div>
			</div>
                    </div>
                <?php } ?>
                <?php if($tipoPam == 3) { ?>

                    <div class="widget-body">
			<div class="widget-main">
                            <div class="row-fluid">
				<div class="span4">
                                    <dl class="dl-horizontal">
                                        Download de modelo do Plano de Trabalho<br>
                                        <a href="<?php echo $this->base; ?>/plano_trabalho.docx">Baixar</a>
                                    </dl>
				</div>
                                <div class="span4">
                                    <dl class="dl-horizontal">
                                        <?php echo $this->Form->input('anexo4', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo do Plano de Trabalho') ); ?>
                                    </dl>
				</div>
                            </div>
                            <b>* Baixe  o modelo do Plano de Trabalho e após adequá-lo, anexe-o no campo ao lado.</b>
			</div>
                    </div>
                <?php } ?>

		</div>
            </div>
	</div>
    <?php } ?>

        <div style="display: none;">
            <?php echo $this->Form->input('co_fornecedor', array('id'=>'Fornecedor','type' => 'select', 'empty' => 'Selecione...', 'label'=>false, 'options' => $fornecedores, 'escape' => false, 'style'=>'display: none;')); ?>
        </div>

        <?php echo $this->Form->input('qtdGrupos', array('id'=> 'QtdGrupos','value' => '0', 'type'=>'hidden')); ?>
        <?php echo $this->Form->input('qtdTotalItens', array('id'=> 'QtdTotalItens','value' => '0', 'type'=>'hidden')); ?>
        <div class="row-fluid">
            <div class="span12">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Itens do Pedido de Aquisição</h4></div>
                    <div class="widget-body">
                        <div class="widget-main" id="grupos"></div>

                    <?php if($tipoPam == 2) { ?>
                        <div class="row-fluid">
                            <div class="span9">
                                <div class="novo_fornecedor">
                                </div>
                            </div>
                            <div class="span2">
                                <div class="input text"><label for="ContratoVlTotalGeral">Total Geral (R$)</label><input id="TtGeral" value="0,00" type="text" class="input-small real" readonly></div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
		        </div>
            </div>
	</div>

	<div class="form-actions">
		<div class="btn-group">
			<button type="submit"
				class="btn btn-small btn-primary bt-pesquisar alert-tooltip"
				title="Confirmar PAM" id="confirmarCadastro">Prosseguir</button>
			<button rel="tooltip" type="reset" title="Limpar dados preenchidos"
				class="btn btn-small" id="Limpar">Limpar</button>
			<button rel="tooltip" title="Voltar" class="btn btn-small voltarcss">
				Voltar</button>
		</div>
	</div>
</div>

<div id="dialog-alerta" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="alert alert-info bigger-110">
            AVISO!
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Favor preencher todos os campos para continuar.</b><br><br>&nbsp;
    <div class="modal-footer">
        <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    </div>
</div>

<div id="view_confirmacao" class="modal hide fade maior tela" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Confirmar Cadastro</h3>
    </div>
    <div class="modal-body tela">
        <div style="overflow: auto;">

            <div class="row-fluid">
                <div class="span12">
                  <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>Informações Básicas</h4></div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Unidade Solicitante</dt>
                                            <dd id="confirUnidadeSolicitante"></dd>
                                            <dt>Observação</dt>
                                            <dd id="confirObservacao"></dd>
                                        <?php
                                            if($tipoPam == 2) { ?>
                                            <dt>Ata do HFA?</dt>
                                            <dd id="confirAtaHFA"></dd>
                                        <?php
                                            }
                                        ?>
                                        </dl>
                                    </div>
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Anexo do <?php __('PAM'); ?> - 1º</dt>
                                            <dd id="confirAnexo1"></dd>
                                            <dt>Anexo do <?php __('PAM'); ?> - 2º</dt>
                                            <dd id="confirAnexo2"></dd>
                                            <dt>Anexo do <?php __('PAM'); ?> - 3º</dt>
                                            <dd id="confirAnexo3"></dd>
                                      <?php
                                            if($tipoPam == 2) { ?>
                                            <dt>Gerenciador da Ata</dt>
                                            <dd id="confirGerenciadorAta"></dd><?php
                                            }
                                      ?>

                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                  <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>Informações do Requisitante</h4></div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Nome Completo do Requisitante</dt>
                                            <dd id="confirRequisitante"></dd>
                                            <dt>Telefone do Requisitante</dt>
                                            <dd id="confirTelefone"></dd>
                                            <dt>Assunto do Pedido</dt>
                                            <dd id="confirAssunto"></dd>
                                        </dl>
                                    </div>
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Posto/Grad/Cat. Func. do Requisitante</dt>
                                            <dd id="confirPosto"></dd>
                                            <dt>PAM solicitado em</dt>
                                            <dd id="confirData"></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php if($tipoPam == 1 || $tipoPam == 3) { ?>
            <div class="row-fluid">

                <div class="span12">
                  <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>Informações do Plano de Trabalho</h4></div>

                    <?php if($tipoPam == 1) { ?>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <dl class="dl-horizontal">
                                            <dt>Objeto</dt>
                                            <dd id="confirObjeto"></dd>
                                            <dt>Vantagens e Economicidade para a Administração</dt>
                                            <dd id="confirVantagens"></dd>
                                            <dt>Critérios de Controles e Registros a Serem Adotados</dt>
                                            <dd id="confirCriterios"></dd>
                                        </dl>
                                    </div>
                                    <div class="span4">
                                        <dl class="dl-horizontal">
                                            <dt>Justificativa para Aquisição</dt>
                                            <dd id="confirJustificativaAquisicao"></dd>
                                            <dt>Verificação dos Resultados</dt>
                                            <dd id="confirVerificacao"></dd>
                                        </dl>
                                    </div>
                                    <div class="span4">
                                        <dl class="dl-horizontal">
                                            <dt>Demanda Prevista e Quantidade Bens e Serviços a serem Contratados</dt>
                                            <dd id="confirDemandaPrevista"></dd>
                                            <dt>Aproveitamento de Servidores do Quadro, de Bens, de Equipamentos</dt>
                                            <dd id="confirAproveitamento"></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if($tipoPam == 3) { ?>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row-fluid">
                                    <div class="span8">
                                        <dl class="dl-horizontal">
                                            <dt>Anexo do Plano de Trabalho</dt>
                                            <dd id="confirAnexo4"></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    </div>
                </div>
            </div>
        <?php } ?>

            <div class="row-fluid">
                <div class="span12">
                  <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>Itens do Pedido de Aquisição</h4></div>
                        <div class="widget-body">
                            <div class="widget-main" id="confirGrupos">
                            </div>

                        <?php if($tipoPam == 2) { ?>
                            <div class="row-fluid">
                                <div class="span9">
                                    <div class="">
                                    </div>
                                </div>
                                <div class="span2">
                                    <div class="input text"><label for="confirVlTotalGeral" id="confirVlTotalGeral"></label></div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
    <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    <button onclick="return false;" class="btn btn-primary btn-small" id="btnSalvar">
        Salvar
        <i class="icon-save icon-on-right bigger-110"></i>
    </button>

    </div>
</div>

<div id="view_novo_fornecedor" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Novo Fornecedor</h3>
    </div>
    <div class="modal-body-iframe" id="add_fornecedor_modal">
    </div>
</div>

<script type="text/javascript">

    // Inicia com os campos escondidos
    $(".novo_fornecedor").hide();

    $('.upload-arquivo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
    });

    $('#form_add_externo').validate({
        errorClass: 'error-message',
        rules: {
            "data[Contrato][co_contratante]": {
                required: true
            },
            "data[Contrato][ds_requisitante]": {
                required: true
            },
            "data[Contrato][tl_requisitante]": {
                required: true
            },
            "data[Contrato][ds_assunto]": {
                required: true
            },
            "data[Contrato][pt_requisitante]": {
                required: true
            },
            <?php if($tipoPam == 1) { ?>
            "data[Contrato][ds_objeto_plano]": {
                required: true
            },
            // validações retiradas para a demanda #819, CitSmart
            "data[Contrato][ds_vantagem]": {
                required: false
            },
            "data[Contrato][ds_criterio]": {
                required: false
            },
            "data[Contrato][ds_justificativa]": {
                required: false
            },
            "data[Contrato][ds_resultado]": {
                required: false
            },
            "data[Contrato][ds_demanda]": {
                required: false
            },
            "data[Contrato][ds_aproveitamento]": {
                required: false
            },
            <?php } ?>
            <?php if($tipoPam == 2) { ?>
            "data[Contrato][ic_ata_orgao]": {
                required: true
            },
            "CoFornecedor1": {
                required: true
            }
            <?php } ?>
        }
    });

    function createFornecedor() {
        var url_fn = "<?php echo $this->base; ?>/fornecedores/iframe/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
            },
            success:function(result){
                $('#add_fornecedor_modal').html("");
                $('#add_fornecedor_modal').append(result);
            }
        });
    }

    function atzComboFornecedor(co_fornecedor) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'fornecedores', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select[id^=CoFornecedor]:enabled").html(options);
            $("[id^=CoFornecedor]:enabled option[value=" + co_fornecedor + "]").attr("selected", true);
            $("[id^=CoFornecedor]:enabled").trigger("chosen:updated");
        });
    }

    maisGrupo(1);

    function maisGrupo(nuGrupo) {

        if(nuGrupo > 1) {
            $('#BtnNovoGrupo' + (nuGrupo - 1) ).hide();
        }

        $('#grupos').append(
            <?php if($tipoPam == 2) { ?>
                            '<div class="row-fluid">' +
                                '<div class="span6">' +
                                    '<input type="hidden" value="0" id="QtdItensGrupo' + nuGrupo + '">' +
                                    '<div class="input text required"><label for="ContratoCoFornecedor">' + nuGrupo + ' - Fornecedor</label><select name="CoFornecedor' + nuGrupo + '" id="CoFornecedor' + nuGrupo + '" class="input-xlarge chosen-select" ></select>&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" id="linkAdd' + nuGrupo + '" onclick="novoGrupo(' + nuGrupo + ')" title="Selecionar Fornecedor" href="#add_item_grupo" data-toggle="modal" aria-describedby="ui-tooltip-1"><i class="green icon-arrow-right bigger-150"></i></a>&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" id="AbrirNovoFornecedor' + nuGrupo + '" onclick="createFornecedor()" title="Adicionar Fornecedor" href="#view_novo_fornecedor" data-toggle="modal" aria-describedby="ui-tooltip-1"><i class="blue icon-edit bigger-150"></i></a></div>' +
                                '</div>' +
                            '</div>' +
            <?php } ?>

                            '<div id="itens' + nuGrupo + '"></div>' +

            <?php if($tipoPam == 2) { ?>
                            '<div class="row-fluid">' +
                                '<div class="span9">&nbsp;</div>' +
                                '<div class="span2">' +
                                    '<div class="input text"><label for="ContratoVlTotalFornecedor">Total Fornecedor (R$)</label><input id="TtFornecedor' + nuGrupo + '" type="text" value="0,00" class="input-small real" readonly></div>' +
                                '</div>' +
                            '</div>' +
            <?php } ?>

                            '<div class="row-fluid">' +
                                '<a href="#itens' + nuGrupo + '" class="btn btn-small btn-primary" id="BtnNovoItem' + nuGrupo + '" onclick="maisItem(' + nuGrupo + ')">Adicionar mais um item</a>&nbsp;&nbsp;&nbsp;' +
                            <?php if($tipoPam == 2) { ?>
                                '<a href="#itens' + nuGrupo + '" class="btn btn-small btn-primary" id="BtnNovoGrupo' + nuGrupo + '" onclick="maisGrupo(' + (nuGrupo + 1) + ')">Adicionar mais um Fornecedor</a>' +
                            <?php } ?>
                            '</div>');
        <?php if($tipoPam == 2) { ?>
        $('#Fornecedor').find('option').clone().appendTo('#CoFornecedor' + nuGrupo);

        $( "#BtnNovoItem" + nuGrupo ).hide();
        $( "#BtnNovoGrupo" + nuGrupo ).hide();
        $(".fornecedorExistente").hide();

        //$( "#CoFornecedor" + nuGrupo ).rules( "add", {
        //    required: true
        //})

        $(".chosen-select").chosen();
        $( '#CoFornecedor' + nuGrupo ).trigger("chosen:updated");
        <?php } else { ?>
        maisItem(nuGrupo);
        <?php } ?>
    }

    function novoGrupo(grupo) {
        if( $( "#CoFornecedor" + grupo ).val() > 0 ) {

            for( n=1; n <= $('#QtdGrupos').val(); n++ ) {
                if( $('#CoFornecedor' + n).val() == $( "#CoFornecedor" + grupo ).val() ) {
                    alert('Este Fornecedor ja foi selecionado, favor selecionar outro Fornecedor.');
                    return false;
                }
            }

            $( "#CoFornecedor" + grupo ).attr('disabled', true).trigger("chosen:updated");
            $( "#linkAdd" + grupo ).hide();
            $( "#AbrirNovoFornecedor" + grupo ).hide();

            $( "#QtdGrupos" ).val( parseInt($( "#QtdGrupos" ).val()) + 1 );

            $( "#BtnNovoItem" + grupo ).show();
            $( "#BtnNovoGrupo" + grupo ).show();

            maisItem( grupo );
        } else {
            alert('Selecione um Fornecedor');
        }
    }

    function maisItem(nuGrupo) {

        //recuperando o próximo numero da linha
        <?php if($tipoPam == 2) { ?>
        var nextGrupo   = parseInt($('#QtdItensGrupo' + nuGrupo).val()) + 1;
        <?php } ?>
        var nextGeral   = parseInt($('#QtdTotalItens').val()) + 1;

        //inserindo formulário
        $('#itens' + nuGrupo).append('<div class="row-fluid" id="divItem' + (nextGeral - 1) + '">' +
				'<div class="span1">' +
                                <?php if($tipoPam == 2) { ?>
                                    '<input type="hidden" name="data[ContratosItem]['+ (nextGeral - 1) +'][co_fornecedor]" value="' + $('#CoFornecedor' + nuGrupo).val() + '">' +
                                    '<div class="input text"><label for="ContratoNuOrdem">Ordem</label><input name="data[ContratosItem]['+ (nextGeral - 1) +'][nu_ordem]" type="text" value="' + nextGrupo + '" class="input-mini" readonly></div></div>' +
                                <?php } else { ?>
                                    '<div class="input text"><label for="ContratoNuOrdem">Ordem</label><input name="data[ContratosItem]['+ (nextGeral - 1) +'][nu_ordem]" type="text" value="' + nextGeral + '" class="input-mini" readonly></div></div>' +
                                <?php } ?>
                                <?php if($tipoPam == 2) { ?>
				'<div class="span1">' +
                                    '<div class="input text required alert-tooltip" title="Item do Pregão"><label for="ContratoNuItemPregao">Item Preg.</label><input id="ContratoNuItemPregao'+ (nextGeral - 1) +'" name="data[ContratosItem]['+ (nextGeral - 1) +'][nu_item_pregao]" type="text" class="input-mini numero"></div></div>' +
                                <?php } ?>
				'<div class="span1">' +
                                    '<div class="input text required alert-tooltip" title="Exemplo: Unidade, Litro, Peso."><label for="ContratoNuUnidade">Unidade</label><input id="ContratoNuUnidade'+ (nextGeral - 1) +'" name="data[ContratosItem]['+ (nextGeral - 1) +'][nu_unidade]" type="text" class="input-mini"></div></div>' +
				'<div class="span1">' +
                                    '<div class="input text required alert-tooltip" title="Quantidade Solicitada"><label for="ContratoQtSolicitada">Qtd Solicit.</label><input id="ContratoQtSolicitada'+ (nextGeral - 1) +'" name="data[ContratosItem]['+ (nextGeral - 1) +'][qt_solicitada]" type="text" class="input-mini numero"></div></div>' +
				'<div class="span3">' +
                                    '<div class="input texarea required"><label for="ContratoDsDemanda">Descrição completa do material/serviço</label><textarea name="data[ContratosItem]['+ (nextGeral - 1) +'][ds_demanda]" class="input-xlarge" cols="40" rows="2" onkeyup="$(this).limit(&quot;250&quot;,&quot;#charsLeft1' + nextGeral + '&quot;)" id="ContratoDsDemanda'+ (nextGeral - 1) +'"></textarea><br><span id="charsLeft1' + nextGeral + '"></span> caracteres restantes.</div></div>' +
                            <?php if($tipoPam == 1) { ?>
				'<div class="span2">' +
                                    '<div class="input text"><label for="ContratoCoSiasg">Código SIASG</label><input id="ContratoCoSiasg'+ (nextGeral - 1) +'" name="data[ContratosItem]['+ (nextGeral - 1) +'][co_siasg]" type="text" class="input-mini numero"></div></div>' +
                            <?php } ?>
                            <?php if($tipoPam == 2) { ?>
				'<div class="span2">' +
                                    '<div class="input text required"><label for="ContratoVlUnitario">Valor Unitário (R$)</label><input id="VlUnitario'+ (nextGeral - 1) +'" name="data[ContratosItem]['+ (nextGeral - 1) +'][vl_unitario]" type="text" class="input-small real"></div></div>' +
				'<div class="span2">' +
                                    '<div class="input text"><label for="ContratoTtItem">Total Item (R$)</label><input id="TtItem'+ (nextGeral - 1) +'" value="0,00" type="text" class="input-small real" readonly></div></div>' +
                            <?php } ?>
                            <?php if($tipoPam == 3) { ?>
				'<div class="span2">' +
                                    '<div class="input text"><label for="ContratoQtConsumo">Média Mensal Consumo</label><input name="data[ContratosItem]['+ (nextGeral - 1) +'][qt_consumo]" type="text" class="input-mini numero"></div></div>' +
				'<div class="span1">' +
                                    '<div class="input text"><label for="ContratoCoCat">CAT MAT / SV</label><input name="data[ContratosItem]['+ (nextGeral - 1) +'][co_cat]" type="text" class="input-mini"></div></div>' +
                            <?php } ?>
                                '<div class="span1"><br>' +
                                '<a class="alert-tooltip" onclick="delItem(' + (nextGeral - 1) + ', ' + nuGrupo + ')" title="Excluir o Item" href="#del_item_grupo" data-toggle="modal" aria-describedby="ui-tooltip-1"><i class="red icon-trash icon-white bigger-150"></i></a></div>' +
                            '</div>');

        $( "#ContratoNuUnidade" + (nextGeral - 1) ).rules( "add", {
            required: true
        });
        $( "#ContratoQtSolicitada" + (nextGeral - 1) ).rules( "add", {
            required: true
        });
        $( "#ContratoDsDemanda" + (nextGeral - 1) ).rules( "add", {
            required: true
        });
        <?php if($tipoPam == 1) { ?>
        $( "#ContratoCoSiasg" + (nextGeral - 1) ).rules( "add");
        <?php } ?>
        <?php if($tipoPam == 2) { ?>
        $( "#ContratoNuItemPregao" + (nextGeral - 1) ).rules( "add", {
            required: true
        });
        $( "#VlUnitario" + (nextGeral - 1) ).rules( "add", {
            required: true
        });
        <?php } ?>

        //armazenando a quantidade de linhas ou registros no elemento hidden
        <?php if($tipoPam == 2) { ?>
        $('#QtdItensGrupo' + nuGrupo).val(nextGrupo);
        <?php } ?>
        $('#QtdTotalItens').val(nextGeral);

        $(".real").maskMoney('destroy');
        $(".real").maskMoney({thousands:'.', decimal:','});

        $(".numero").maskMoney('destroy');
        $(".numero").maskMoney({precision:0, allowZero:false, thousands:'.'});

        $('.alert-tooltip').tooltip();

        <?php if($tipoPam == 2) { ?>
        addComportamentoItem(nextGeral - 1, nuGrupo);
        <?php } ?>

    }

    addComportamentoItem(0, 1);

    function addComportamentoItem(item, grupo) {
        $( "#VlUnitario" + item ).on('focusout', function(e) {
            var quantidade  = $("#ContratoQtSolicitada" + item).val();
            var valor = $(this).val().replace(/\./g, '').replace(/,/g, '.');

            var ttAntigo     = $("#TtItem" + item).val().replace(/\./g, '').replace(/,/g, '.');
            var ttFornecedor = $("#TtFornecedor" + grupo).val().replace(/\./g, '').replace(/,/g, '.') - ttAntigo;

            var ttGeral = $("#TtGeral").val().replace(/\./g, '').replace(/,/g, '.') - ttAntigo;

            var total        = valor * quantidade;

            $("#TtItem" + item).val( ( formatReal( ( total ).toFixed(2).replace('.', '') ) ) );

            $("#TtFornecedor" + grupo).val( ( formatReal( ( ttFornecedor + total ).toFixed(2).replace('.', '') ) ) );
            $("#TtGeral").val( ( formatReal( ( ttGeral + total ).toFixed(2).replace('.', '') ) ) );

        });
    }


    $("#ContratoIcAtaOrgao").change( function() {
        if($("#ContratoIcAtaOrgao").val() == 'N') {
            $("#divGerenciadorAta").show();

            $( "#ContratoDsGerenciadorAta" ).rules( "add", {
                required: true
            });
        } else {
            $("#divGerenciadorAta").hide();
            $( "#ContratoDsGerenciadorAta" ).rules( "remove" );
        }
    });

    $(".real").maskMoney({thousands:'.', decimal:','});
    $(".numero").maskMoney({precision:0, allowZero:false, thousands:'.'});

    function delItem(item, grupo) {
        $( "#ContratoNuUnidade" + item ).rules("remove");
        $( "#ContratoQtSolicitada" + item ).rules("remove");
        $( "#ContratoDsDemanda" + item ).rules("remove");
        <?php if($tipoPam == 1) { ?>
        $( "#ContratoCoSiasg" + item ).rules("remove");
        <?php } ?>
        <?php if($tipoPam == 2) { ?>
        $( "#ContratoNuItemPregao" + item ).rules("remove");
        $( "#VlUnitario" + item ).rules("remove");
        <?php } ?>

        <?php if($tipoPam == 2) { ?>

            var quantidade  = $("#ContratoQtSolicitada" + item).val();
            var valor = $("#VlUnitario" + item).val().replace(/\./g, '').replace(/,/g, '.');

            var ttAntigo     = $("#TtItem" + item).val().replace(/\./g, '').replace(/,/g, '.');
            var ttFornecedor = $("#TtFornecedor" + grupo).val().replace(/\./g, '').replace(/,/g, '.') - ttAntigo;

            var ttGeral = $("#TtGeral").val().replace(/\./g, '').replace(/,/g, '.') - ttAntigo;

            $("#TtFornecedor" + grupo).val( ( formatReal( ( ttFornecedor ).toFixed(2).replace('.', '') ) ) );
            $("#TtGeral").val( ( formatReal( ( ttGeral ).toFixed(2).replace('.', '') ) ) );

        <?php } ?>

        $("#divItem"+item).html('');
    }

    $('#confirmarCadastro').click(function(){

        if($('#form_add_externo').valid()) {
            $('#confirUnidadeSolicitante').html( $('#ContratoCoContratante option:selected').text().trim() );
            $('#confirObservacao').html( $('#ContratoDsObservacao').val() );
            <?php if($tipoPam == 2) { ?>
            $('#confirAtaHFA').html( $('#ContratoIcAtaOrgao option:selected').text() );
            $('#confirGerenciadorAta').html( $('#ContratoDsGerenciadorAta').val() );
            <?php } ?>
            $('#confirAnexo1').html( $('#ContratoAnexo').val() );
            $('#confirAnexo2').html( $('#ContratoAnexo2').val() );
            $('#confirAnexo3').html( $('#ContratoAnexo3').val() );
            $('#confirRequisitante').html( $('#ContratoDsRequisitante').val() );
            $('#confirTelefone').html( $('#ContratoTlRequisitante').val() );
            $('#confirAssunto').html( $('#ContratoDsAssunto').val() );
            $('#confirPosto').html( $('#ContratoPtRequisitante').val() );
            $('#confirData').html( $('#ContratoDtSolicitacao').val() );

            <?php if($tipoPam == 1) { ?>
            $('#confirObjeto').html( $('#ContratoDsObjetoPlano').val() );
            $('#confirVantagens').html( $('#ContratoDsVantagem').val() );
            $('#confirCriterios').html( $('#ContratoDsCriterio').val() );
            $('#confirJustificativaAquisicao').html( $('#ContratoDsJustificativa').val() );
            $('#confirVerificacao').html( $('#ContratoDsResultado').val() );
            $('#confirDemandaPrevista').html( $('#ContratoDsDemanda').val() );
            $('#confirAproveitamento').html( $('#ContratoDsAproveitamento').val() );
            <?php } ?>
            <?php if($tipoPam == 3) { ?>
            $('#confirAnexo4').html( $('#ContratoAnexo4').val() );
            <?php } ?>

            $('#confirGrupos').html( '' );

            <?php if($tipoPam == 2) { ?>
            var idFornecedor  = 0;
            var nvFornecedor  = 0;
            for( n=1; n <= $('#QtdGrupos').val(); n++ ) {
                idFornecedor = $('#CoFornecedor' + n).val();
                $('#confirGrupos').append('<b>' + n + ' - Fornercedor:</b> ' + $("#Fornecedor option[value=" + idFornecedor + "]").text() + ' - <b>Total Fornecedor (R$):</b> '+ $("#TtFornecedor" + n).val() +'<BR>');
                $('#confirGrupos').append('<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Item Preg.</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span2">Valor Unitário (R$)</div><div class="span2">Total Item (R$)</div></div>');
            <?php } ?>

                <?php if($tipoPam == 1) { ?>
                $('#confirGrupos').append('<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span1">Codigo SIASG</div></div>');
                <?php } if($tipoPam == 3) { ?>
                $('#confirGrupos').append('<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span2">Media Mensal Consumo</div><div class="span2">CAT MAT / SV</div></div>');
                <?php } ?>
                for( i=0; i < $('#QtdTotalItens').val(); i++ ) {

                    <?php if($tipoPam == 2) { ?>
                    nvFornecedor = $('input[name="data[ContratosItem][' + i + '][co_fornecedor]"]').val();
                    if(idFornecedor == nvFornecedor) {
                    <?php } ?>
                        if( !(typeof $('input[name="data[ContratosItem][' + i + '][nu_ordem]"]').val() === "undefined") ) {
                            $('#confirGrupos').append('<div class="row-fluid"><div class="span1">'+
                                $('input[name="data[ContratosItem][' + i + '][nu_ordem]"]').val() +'</div><div class="span1">'+
                                <?php if($tipoPam == 2) { ?>
                                $('input[name="data[ContratosItem][' + i + '][nu_item_pregao]"]').val() +'</div><div class="span1">'+
                                <?php } ?>
                                $('input[name="data[ContratosItem][' + i + '][nu_unidade]"]').val() +'</div><div class="span1">'+
                                $('input[name="data[ContratosItem][' + i + '][qt_solicitada]"]').val() +'</div><div class="span3">'+
                                $('#ContratoDsDemanda' + i).val() +'</div><div class="span2">'+
                                <?php if($tipoPam == 1) { ?>
                                $('input[name="data[ContratosItem][' + i + '][co_siasg]"]').val() +'</div><div class="span1">'+
                                <?php } ?>
                                <?php if($tipoPam == 3) { ?>
                                $('input[name="data[ContratosItem][' + i + '][qt_consumo]"]').val() +'</div><div class="span1">'+
                                <?php } ?>
                                <?php if($tipoPam == 3) { ?>
                                $('input[name="data[ContratosItem][' + i + '][co_cat]"]').val() +'</div><div class="span1">'+
                                <?php } ?>
                                <?php if($tipoPam == 2) { ?>
                                $('input[name="data[ContratosItem][' + i + '][vl_unitario]"]').val() +'</div><div class="span2">'+
                                $('#TtItem' + i).val() +'</div></div>'+
                                <?php } ?>
                                '');
                        }
                    <?php if($tipoPam == 2) { ?>
                    }<?php } ?>
                }

            <?php if($tipoPam == 2) { ?>
            }
            $('#confirVlTotalGeral').html( '<b>Total Geral (R$)</b> ' + $('#TtGeral').val() );
            <?php } ?>

            $('#view_confirmacao').modal();
        } else {
            $('#dialog-alerta').modal();
        }
        return false;
    });

    $('#btnSalvar').click(function(){
        $('#form_add_externo').submit();
    });

    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g,"");
    }


</script>
