<?php
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' );
?>

<?php echo $this->Form->create('Contrato', array('url' => array('controller' => 'contratos', 'action' => 'add_processo')) );?>

    <div class="row-fluid">

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Processo<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'index')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Listar">Listagem</a>
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
          </div>
        </div>

        <div class="row-fluid">

            <div class="span8">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>Processo</h4></div>

                <div class="widget-body">
                    <div class="widget-main">
                      <div class="row-fluid">
                        <div class="span6">
                           <dl class="dl-horizontal">
                              <?php
                                      echo $this->Form->hidden('co_contrato');
                                      if ($this->Modulo->isContratoExterno()) {
                                          $checkedI = 'checked="checked"';
                                          $checkedE = '';
                                          if(isset($this->data['Contrato']['ic_tipo_contrato']) && $this->data['Contrato']['ic_tipo_contrato'] == 'E') {
                                            $checkedE = 'checked="checked"';
                                            $checkedI = '';
                                          }
                             ?>
                                      Tipo de Visão <br>
                                      <input name="data[Contrato][ic_tipo_contrato]" type="radio" id="ContratoIcTipoContratoI" value="I" checked="checked"/> Interno &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input name="data[Contrato][ic_tipo_contrato]" type="radio" id="ContratoIcTipoContratoE" value="E"/> Externo<br><br>
                             <?php
                                      }
                                      if( $this->Modulo->isPam()) {
                                        echo $this->Form->input('nu_pam', array('class' => 'input-xlarge','label'=>'Nº '  . __('PAM', true)));
                                        echo $this->Form->input('dt_autorizacao_pam', array(
                                                'before' => '<div class="input-append date datetimepicker">',
                                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                                'class' => 'input-small','label' => 'Data de Autorização', 'type'=>'text'));
                                      }
                                      if(!$this->Modulo->isAutoNumeracao()) {
                                          echo $this->Form->input('nu_processo', array('class' => 'input-xlarge', 'label'=>'Processo', 'onblur'=>'completeComZeros()'));
                                      }
                                      //echo $this->Form->input('co_situacao',      array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...',    'label'=>'Andamento Processual', 'options' => $situacaos ));

                                      echo $this->Form->input('co_contratante',   array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Unidade Solicitante', 'options' => $imprimir->getArraySetores($contratantes), 'escape' => false));

                                      if($this->Modulo->isCamposContrato('co_executante') == true) {
                                        echo $this->Form->input('co_executante',   array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Unidade Executora', 'options' => $imprimir->getArraySetores($contratantes), 'escape' => false));
                                      }

                                      echo $this->Form->input('co_fornecedor',    array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...',     'label'=> __('Fornecedor', true), 'options' => $fornecedores,
                                          'after' => $this->Print->getBtnEditCombo('Novo ' .  __('Fornecedor', true), 'AbrirNovoFornecedor', '#view_novo_fornecedor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/add')) ) );

                                    echo $this->Form->input('ds_recurso_orcamentario', array('class' => 'input-xlarge','label'=>'Recurso Orçamentário','type' => 'texarea', 'cols'=>'30', 'rows'=>'3',
                                            'onKeyup'=>'$(this).limit("30","#charsLeft2")',
                                            'after' => '<br><span id="charsLeft2">30</span> caracteres restantes.'));
                              ?>
                            </dl>
                            <div id="dialog-pam" class="hide">
                                    <div class="alert alert-info bigger-110">
                                        Já existe um <?php __('PAM'); ?> cadastrado com o número informado.<br>
                                        Deseja vincular este <?php __('PAM'); ?> ao Processo?
                                    </div>
                            </div>
                         </div>
                         <div class="span3">
                               <dl class="dl-horizontal">

                              <?php
                                      echo $this->Form->input('ds_objeto', array('class' => 'input-xlarge','label'=>'Objeto','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                            'onKeyup'=>'$(this).limit("1500","#charsLeft")',
                                            'after' => '<br><span id="charsLeft">1500</span> caracteres restantes.'));

                                      echo '<br />';
                                      echo $this->Form->input('ds_observacao', array('class' => 'input-xlarge','label'=>'Observações','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                            'onKeyup'=>'$(this).limit("250","#charsLeft1")',
                                            'after' => '<br><span id="charsLeft1">250</span> caracteres restantes.'));
                              ?>

                              </dl>
                         </div>
                       </div>
                    </div>
                </div>
              </div>
            </div>

            <div class="span4">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4><?php __('Recursos Financeiros'); ?></h4></div>

                <div class="widget-body">
                    <div class="widget-main">
			<?php
                                echo $this->Form->input('tp_valor', array('class' => 'input-xlarge','label'=>'Tipo de Valor', 'options' => array('F' => 'Fixo', 'V' => 'Variável' ) ));
				echo $this->Form->input('vl_inicial', array('class' => 'input-xlarge','label'=>'Valor inicial (R$)', 'maxlength' => '13' ));
                                echo $this->Form->input('vl_mensal', array('class' => 'input-xlarge','label'=>'Valor mensal (R$)' , 'maxlength' => '13' ));

                            if($this->Modulo->isCamposContrato('vl_servico') == true) :
                                echo $this->Form->input('vl_servico', array('class' => 'input-xlarge', 'label' => 'Valor serviço (R$)', 'maxlength' => '13' ));
                            endif;
                            if($this->Modulo->isCamposContrato('vl_material') == true) :
                                echo $this->Form->input('vl_material', array('class' => 'input-xlarge', 'label' => 'Valor material (R$)', 'maxlength' => '13' ));
                            endif;

				echo $this->Form->input('vl_global', array('class' => 'input-xlarge','label'=>'Valor global (R$)' , 'maxlength' => '13' ));
			?>
                    </div>
                </div>
              </div>

            </div>

        </div>

        <div class="row-fluid">
            <div class="span8">
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Detalhamento'); ?></h4></div>
                    <div class="widget-body">
                      <div class="widget-main">
                      <div class="row-fluid">
                        <div class="span6">
                              <dl class="dl-horizontal">
                             <?php
                                    if($this->Modulo->isCamposContrato('st_repactuado') == true) :
                                        echo $this->Form->input('st_repactuado',   array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Prorrogável', true), 'options' => array( 'S' => 'Sim', 'N' => 'Não') ));
                                    endif;

                                    echo $this->Form->input('co_modalidade', array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Tipo', 'options' => $modalidades,
                                         'after' => $this->Print->getBtnEditCombo('Novo ' .  __('Tipo de Contrato', true), 'AbrirNovoTpContrato', '#view_novo_tp_contrato', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'modalidades/index')) ) );
                                     if($this->Modulo->isCamposContrato('co_servico') == true) {
                                        echo $this->Form->input('co_servico', array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Descrição do Serviço', 'options' => $servicos,
                                            'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Descrição do Serviço', true), 'AbrirNovoServico', '#view_novo_servico', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'servicos/index')) ) );
                                     }
                             ?>
                              </dl>
                        </div>
                        <div class="span6">
                              <dl class="dl-horizontal">
                             <?php
                             if($this->Modulo->isCamposContrato('co_situacao_processo')) :
                                echo $this->Form->input('tp_situacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Situação do Processo', true), 'options' => $situacaos));
                             endif;
                                     echo $this->Form->input('co_contratacao', array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Modalidade de Contratação', true), 'options' => $contratacoes,
                                         'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Modalidade de Contratação', true), 'AbrirNovaModalidade', '#view_nova_modalidade', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratatacoes/index')) ) );?>


                                    <?php
                                    // echo '<div class="">&nbsp;</div>';
                                    echo $this->Form->input('ic_cadastro_garantia', array(
                                        'id' => 'icGarantia',
                                        'class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Exigir cadastro de garantia ?', 'options' => array(1 => 'Sim', 0 => 'Não'),'escape' => false));

                                    // porcentagem da garantia
                                    echo $this->Form->input('pc_garantia', array(
                                        'id' => 'pctGarantia',
                                        'style' => '',
                                        'class' => 'input-mini',
                                        'maxlength' => '3',
                                        'type' => 'text', 'empty' => 'Selecione...', 'label'=>'Porcentagem da garantia ', 'escape' => false));

                                    ?>

                                    <?php
                                     if($this->Modulo->isCamposContrato('co_categoria') == true) {
                                        echo $this->Form->input('co_categoria', array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Categoria', true), 'options' => $categorias,
                                            'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Categoria', true), 'AbrirNovaCategoria', '#view_nova_categoria', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'categorias/index')) ) );
                                     } if($this->Modulo->isCamposContrato('co_subcategoria') == true) {
                                        echo $this->Form->input('co_subcategoria', array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Sub Categoria', true),
                                            'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Sub Categoria', true), 'AbrirNovaSubCategoria', '#view_nova_sub_categoria', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'subcategorias/index')) ) );
                                     }
                             ?>
                              </dl>
                        </div>
                      </div>
                   </div>
                </div>


              </div>
          </div>
			<?php if( $this->Modulo->isCamposContrato('dt_ini_processo')
					 || $this->Modulo->isCamposContrato('dt_fim_processo') ): ?>
            <div class="span4">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4><?php __('Datas'); ?></h4></div>

                <div class="widget-body">
                    <div class="widget-main">
                        <?php
                        	if( $this->Modulo->isCamposContrato('dt_ini_processo') ):
                            echo $this->Form->input('dt_ini_processo', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Início do Processo', 'type'=>'text'));
                            endif;

                            if( $this->Modulo->isCamposContrato('dt_fim_processo') ):
                            echo $this->Form->input('dt_fim_processo', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Fim do Processo', 'type'=>'text'));
                            endif;
						?>
                    </div>
                </div>
              </div>
            </div>
            <?php endif; ?>

        </div>

    <div id="view_novo_fornecedor" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Fornecedor'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_fornecedor">
        </div>
    </div>

    <div id="view_novo_tp_contrato" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Tipo de Contrato'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_tp_contrato">
        </div>
    </div>

    <div id="view_novo_servico" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Descrição do Serviço'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_servico">
        </div>
    </div>

    <div id="view_nova_modalidade" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Modalidade de Contratação'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_modalidade">
        </div>
    </div>

    <div id="view_nova_categoria" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Categoria'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_categoria">
        </div>
    </div>

    <div id="view_nova_sub_categoria" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Sub Categoria'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_sub_categoria">
        </div>
    </div>

<div class="form-actions">
   <div class="btn-group">
      <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar processo"> Salvar</button>
      <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
      <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
   </div>
</div>
</div>


<script type="text/javascript">
    function completeComZeros() {
        var stringProcess = String($('#ContratoNuProcesso').val());
        var newString = stringProcess;
        for(var i = 0; i < (stringProcess.split('_').length -1) ; i++ ){
            newString = newString.replace('_', '');
            newString = '0' + newString;
        }
        $('#ContratoNuProcesso').val(newString);
    }

    function atzComboFornecedor(co_fornecedor) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'fornecedores', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoFornecedor").html(options);
            $("#ContratoCoFornecedor option[value=" + co_fornecedor + "]").attr("selected", true);
            $("#ContratoCoFornecedor").trigger("chosen:updated");
        });
    }

    function atzComboModalidade(co_contratacao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'contratacoes', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoContratacao").html(options);
            $("#ContratoCoContratacao option[value=" + co_contratacao + "]").attr("selected", true);
            $("#ContratoCoContratacao").trigger("chosen:updated");
        });
    }

    function atzComboTpContrato(co_modalidade) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoModalidade").html(options);
            $("#ContratoCoModalidade option[value=" + co_modalidade + "]").attr("selected", true);
            $("#ContratoCoModalidade").trigger("chosen:updated");
        });
    }

    function atzComboServico(co_servico) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'servicos', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoServico").html(options);
            $("#ContratoCoServico option[value=" + co_servico + "]").attr("selected", true);
            $("#ContratoCoServico").trigger("chosen:updated");
        });
    }

    function atzComboCategoria(co_categoria) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'categorias', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoCategoria").html(options);
            $("#ContratoCoCategoria option[value=" + co_categoria + "]").attr("selected", true);
            $("#ContratoCoCategoria").trigger("chosen:updated");
            atzComboSubCategoria(co_categoria, 0);
        });
    }

    function atzComboSubCategoria(co_categoria, co_sub_categoria) {
        verificaDeContrato(co_categoria);
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'subcategorias', 'action' => 'listar') )?>" + "/" + co_categoria, null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoCoSubcategoria").html(options);
            if(co_sub_categoria > 0) {
                $("#ContratoCoSubcategoria option[value=" + co_sub_categoria + "]").attr("selected", true);
            }
            $("#ContratoCoSubcategoria").trigger("chosen:updated");
        });
    }

    $(function(){
        $('#pctGarantia').closest('div.input').hide();

        setMascaraCampo("#ContratoNuProcesso", "<?php echo FunctionsComponent::pegarFormato( 'processo' ); ?>");

        $("#ContratoVlInicial").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlMensal").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlGlobal").maskMoney({thousands:'.', decimal:','});

        $('#icGarantia').on('change', function () {
            if ($(this).val() == 1) {
                $('#pctGarantia').closest('div.input').show();
            } else {
                $('#pctGarantia').closest('div.input').hide();
            }
        });

        $( "#ContratoNuPam" ).on('focusout', function(e) {

            var co_contrato = 0;
            if($(this).val() != '') {
                $.ajax({
                    type:"POST",
                    url:'<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'find_pam') )?>',
                    data:{
                        "data[Contrato][nu_pam]":$(this).val()
                    },
                    success:function(result){
                        if(result > 0) {
                            e.preventDefault();

                            $( "#dialog-pam" ).dialog({
                                resizable: false,
                                modal: true,
                                title: 'PAM já cadastrado!',
                                title_html: true,
                                buttons: [
                                    {
                                        html: "<i class='icon-ok bigger-110'></i>&nbsp; Confirma",
                                        "class" : "btn btn-success btn-mini",
                                        click: function() {
                                            $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'edit') )?>/' + result + '/np');
                                        }
                                    }
                                    ,
                                    {
                                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancela",
                                        "class" : "btn btn-mini",
                                        click: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                ]
                            });
                        }
                    }
                })
            }
        });

    });

    $("#AbrirNovoFornecedor").bind('click', function(e) {
        var url_fn = "<?php echo $this->base; ?>/fornecedores/iframe/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
            },
            success:function(result){
                $('#add_fornecedor').html("");
                $('#add_fornecedor').append(result);
            }
        })
    });
    $("#AbrirNovaModalidade").bind('click', function(e) {
        var url_md = "<?php echo $this->base; ?>/contratacoes/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
            },
            success:function(result){
                $('#add_modalidade').html("");
                $('#add_modalidade').append(result);
            }
        })
    });
    $("#AbrirNovoTpContrato").bind('click', function(e) {
        var url_tm = "<?php echo $this->base; ?>/modalidades/iframe/";
        $.ajax({
            type:"POST",
            url:url_tm,
            data:{
            },
            success:function(result){
                $('#add_tp_contrato').html("");
                $('#add_tp_contrato').append(result);
            }
        })
    });
    $("#AbrirNovoServico").bind('click', function(e) {
        var url_sv = "<?php echo $this->base; ?>/servicos/iframe/";
        $.ajax({
            type:"POST",
            url:url_sv,
            data:{
            },
            success:function(result){
                $('#add_servico').html("");
                $('#add_servico').append(result);
            }
        })
    });
    $("#AbrirNovaCategoria").bind('click', function(e) {
        var url_ct = "<?php echo $this->base; ?>/categorias/iframe/";
        $.ajax({
            type:"POST",
            url:url_ct,
            data:{
            },
            success:function(result){
                $('#add_categoria').html("");
                $('#add_categoria').append(result);
            }
        })
    });
    $("#AbrirNovaSubCategoria").bind('click', function(e) {
        var url_sb = "<?php echo $this->base; ?>/subcategorias/iframe/";
        $.ajax({
            type:"POST",
            url:url_sb,
            data:{
            },
            success:function(result){
                $('#add_sub_categoria').html("");
                $('#add_sub_categoria').append(result);
            }
        })
    });


    <?php if($this->Modulo->isCamposContrato('co_subcategoria') == true) { ?>
    $("#ContratoCoCategoria").change( function()
    {
        atzComboSubCategoria($(this).val(), 0);
    });

    document.getElementById('ContratoCoSubcategoria').disabled = true ;
    <?php } ?>

    function verificaDeContrato(tipo) {


        if(tipo == "") {
            document.getElementById('ContratoCoSubcategoria').disabled = true ;
        } else {
            document.getElementById('ContratoCoSubcategoria').disabled = false;
        }



    }

</script>
