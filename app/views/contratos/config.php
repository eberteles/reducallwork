<?php
return array(
    'version' => '1.7.1',
    'language' => array(
        'locale' => 'pt_br'
    ),
    'api' => array(
        'atas' => array(
            'token' => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzeXN0ZW0iOiJOMk9USSIsImlhdCI6MTUxMDU3OTY2NywiZXhwIjoxNTQyMTM3MjY3fQ.iSLd4EYc_LBSKCtwbjh03hsJl_tP_JckoQoFOq2lGQKheESE2mSdaGry52AKHRATjhRX30Pv5i6Dd6U83xwNNW8uE9-TysFdTJztpo6s_jIAuMGg-gKIGIqenZFx_DTaArSvLVkZheGDBrhxhZF4H5xaCyyzU1uRCQ2lsRIuS7YGHBOml240bcauHweB8k6QYL2ZSlOVwDm8ZHAnPoP94D6TfUHrGMawdOYE8z6lEwcFDpCgoSeAgUU8IoWzVUDQnEL5JzkmLgGXicI_3FEiZLldkraEYhbFUKREusPAbwFbzQ1IM0mYfEqKa20cTwiH81QoWHZVxPdFKk83bYWqEw',
            'url' => 'http://build.reducall.intra:3000'
        ),
        'fornecedores' => array(
            'token' => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzeXN0ZW0iOiJOMk9USSIsImlhdCI6MTUxMDU3OTY2NywiZXhwIjoxNTQyMTM3MjY3fQ.iSLd4EYc_LBSKCtwbjh03hsJl_tP_JckoQoFOq2lGQKheESE2mSdaGry52AKHRATjhRX30Pv5i6Dd6U83xwNNW8uE9-TysFdTJztpo6s_jIAuMGg-gKIGIqenZFx_DTaArSvLVkZheGDBrhxhZF4H5xaCyyzU1uRCQ2lsRIuS7YGHBOml240bcauHweB8k6QYL2ZSlOVwDm8ZHAnPoP94D6TfUHrGMawdOYE8z6lEwcFDpCgoSeAgUU8IoWzVUDQnEL5JzkmLgGXicI_3FEiZLldkraEYhbFUKREusPAbwFbzQ1IM0mYfEqKa20cTwiH81QoWHZVxPdFKk83bYWqEw',
            'url' => 'http://build.reducall.intra:3000'
        )
    ),

    'resource' => array(
        'esqueciMinhaSenha' => true,
        'certificadoDigital' => false,
        'layout' => array(
            'logo_superior' => array(
                'direita' => array(
                    'path' => '/img/logo-blue.png',
                    'title' => 'Reduwork',
                    'link' => 'http://www.reducall.com',
                ),
                'esquerda' => array(
                    'path' => '/img/logo_sistema.png',
                    'title' => __('ReducallWork', true) . ' - Gestão Integrada de ' . __('Contratos', true) . ' e Processos.',
                    //'link' => 'http://www.reducall.com',
                )
            ),
            'img' => '/img/banner/banner01.png',
            'img_logo_topo' => '/img/logo_sistema_branco.png',
            'caption' => __('reduwork', true) . ' - Gestão Integrada de ' . __('Contratos', true) . ' e Processos.'
        ),
        'dashboard' => array(
            'vencimentos' => array(30, 60, 120, 180),
        ),
         'urls' => array(
             'roboComprasnet' => 'http://v1.reduwork.com.br/robo/siasg.php',
             'gestor' => 'http://gestor.com.br/',
         ),
         'licenca' => array(
             'bi' => 1,
             'nominal' => 10,
             'concorrente' => 1,
             'proporcaoConcorrente' => 5,
             'timeout' => array(
                 'nominal' => 120,
                 'concorrente' => 20,
             ),
         ),
         'available' => array(
             'fluxo' => true,
             'fiscal' => false,
             'apostilamento' => false,
             'siasg' => true,
             'relatorioExcel' => false,
         ),
         'uasg' => null,
         'contrato' => array(
             'camposPesquisa' => array(
                 'ctr' => array(1,3,6,25,8,9,10),
                 //'ctr' => array(1,2,3,6,7,8,9,10),
                 'cte' => array(16,15,18,20,21),
             )
         ),
    ),
    'database' => array(
        // conexões obrigatários
        'default' => array(
            'driver'        => 'mysql',
            'persistent'    => false,
            'host'          => 'mysql.reducallwork.com.br',
            'login'         => 'reducallwork',
            'password'      => 'sucesso2018',
            'database'      => 'reducallwork',
            'prefix'        => '',
            'encoding'      => 'utf8'
        ),
        'test' => array(
            'driver' => 'mysql',
            'persistent' => false,
            'host' => '10.12.0.11',
            'login' => 'usr_reduwork_merge',
            'password' => 'pwd_reduwork_merge',
            'database' => 'reduwork_teste',
            'prefix' => '',
            'encoding' => 'utf8'
        ),
        'logauditoria' => array(
            'driver' => 'mysql',
            'persistent' => false,
            'host' => '10.12.0.11',
            'login' => 'usr_reduwork_merge',
            'password' => 'pwd_reduwork_merge',
            'database' => 'audit_information',
            'prefix' => '',
        ),
        'userDatabaseManager' => array(
            'driver'        => 'mysql',
            'persistent'    => false,
            'host'          => '10.12.0.11',
            'login'         => 'usr_adm_users',
            'password'      => 'pwd_adm_users',
        ),
    ),
    'component' => array(
        /**
         * Configurações de envio de e-mail pelo PHPMailer
         */
        'email' => array(
            'SMTPDebug'     => 0,
            'fromName'      => 'ReducallWork',
            'from'          => 'reducallwork@gmail.com',
            'host'          => 'smtp.gmail.com',
            'port'          => 465,
            'username'      => 'reducallwork@gmail.com',
            'password'      => 'sucesso2019',
            'assunto'       => '[ReducallWork] - ',
            'SMTPAuth'      => true,
            'SMTPSecure'    => 'ssl',
            'AuthType'      => 'LOGIN',
            'encoding'      => 'UTF-8',
            'Realm'         => '',
            'Workstation'   => ''
        ),
        'barraGoverno' => array(
            'enabled' => false,
            'params' => array(
                /*
                 * Defina o número correto do órgão no SIORG. Acesse o SIORG e procure pelo seu órgão.
                 * http://siorg.planejamento.gov.br
                 */
                'unidadeOrganizacional' => null, // deve ser definido em cada cliente
            ),
        ),
        /*
         * Só deve ser habilitado se o modulo de auditoria (malop) for habilitado no banco mysql
         * e os usuario de banco tiverem sido criados
         *
         * Ao habilitar o modulo de auditoria deve-se impreterivelmente criar o usuário de banco para o administrador
         * user: ug-administrador
         * pass: 123-ug-administrador
         */
        'auditoria' => array(
            'enabled' => false,
            'params' => array(
                'prefix_login' => 'ug', // prefixo para criação do usuário de banco
                'salt' => '123',        // salt para senha criada para o usuário de banco
            ),
        ),
        /*
         * Define se o sistema será integrado com o SEI e qual versão.
         */
        'sei' => array(
            'enabled' => false,
            'params' => array(
                'version' => '2.6.0',
                'integrasConfig' => (object) array(
                    /* identificador do sistema que fará acesso ao SEI */
                    'system_external' => 'reduwork',

                    /* agrupador de serviços disponíveis para o sistema informado em 'external_external' */
                    'system_grouper' => 'reduwork',

                    /*
                     * devido ao fato que cada versão do sei pode ser conectado via banco ou webservice,
                     * esta propriedade define a forma padrão de conexão com os serviços
                     */
                    'system_default_connection' => 'webservice',

                    /*
                     * O número máximo de documentos por chamada é limitado através do parâmetro
                     * SEI_WS_NUM_MAX_DOCS (menu Infra/Parâmetros).
                     */
                    'sei_ws_num_max_docs' => 5,

                    /* versões do SEI disponíveis */
                    'version' => array(
                        /*
                         * Apenas uma chave de versão deve estar configurada
                         */
                        '2.6.0' => array(
                            'url_base' => 'http://10.12.0.23:10080/sei',
                            'connect' => array(
                                /* configuracao de acesso ao webservice do SEI */
                                'webservice' => array(
                                    'wsdl' => 'http://10.12.0.23:10080/sei/controlador_ws.php?servico=sei',
                                    'endpoint' => 'http://10.12.0.23:10080/sei/ws/SeiWS.php',
                                    'options' => array(
                                        'encoding' => 'ISO-8859-1',
                                        'soap_version' => SOAP_1_1,
                                    )
                                ),
                            ),

                            /*
                             * lista de serviços disponíveis pelo componente cada nome na lista a seguir deve corresponder
                             * a uma classe na pasta library/modules/integras/sei/version/services/SEI{version}{type} em que:
                             *
                             * {version}    corresponde a verão do SEI em uso e definida na entrada acima;
                             * {type}       corresponde ao tipo de acesso: database, webservice
                             **/
                            'services' => array(
                                'consultarDocumento',
                                'consultarProcedimento',
                                'gerarProcedimento',
                                'incluirDocumento',
                                'listarExtensoesPermitidas',
                                'listarSeries',
                                'listarTiposProcedimento',
                                'listarUnidades',
                                'listarUsuarios',

                            ),
                        ),
                        '3.0.0' => array(
                            'url_base' => 'http://10.15.0.58:2080/sei',
                            'connect' => array(
                                /* configuracao de acesso ao webservice do SEI */
                                'webservice' => array(
                                    'wsdl' => 'http://10.15.0.58:2080/sei/controlador_ws.php?servico=sei',
                                    /* parâmetro não obrigatório pode ser removido */
                                    'endpoint' => 'http://10.15.0.58:2080/sei/ws/SeiWS.php',
                                    'options' => array(
                                        'encoding' => 'ISO-8859-1',
                                        'soap_version' => SOAP_1_1,
                                        'cache_wsdl' => WSDL_CACHE_NONE,
                                    )
                                ),
                            ),

                            /*
                             * lista de serviços disponíveis pelo componente cada nome na lista a seguir deve corresponder
                             * a uma classe na pasta library/modules/integras/sei/version/services/SEI{version}{type} em que:
                             *
                             * {version}    corresponde a verão do SEI em uso e definida na entrada acima;
                             * {type}       corresponde ao tipo de acesso: database, webservice
                             **/
                            'services' => array(
                                'adicionarArquivo',
                                'adicionarConteudoArquivo',
                                'consultarDocumento',
                                'consultarProcedimento',
                                'listarAndamentos',
                                'listarUnidades',
                                'listarUsuarios',
                            ),
                        ),
                    ),
                ),
            ),
        ),
        'siafi' => array(
            'enabled' => false,
            'params' => array(
                'coGestaoOrgao' => 1,
                'ws_authToken' => 'f3f9e0641bd4f66700fe4ccaa130fcfe3178ec07',
                'ws_url' => null, // url deve ser seta no config de cada cliente
            ),
        ),
        /*'ldap' => array(
            'enabled' => false,
            'params' => array(
                'connection' => array( //vide demais parametros para Zend_Ldap
                    'host' => '10.12.0.22',
                    'port' => 389,
                    'useSsl' => false,
                    'useStartTls' => false,
                    'accountDomainName' => 'reducall.srvteste',
                    'accountDomainNameShort' => 'reducall0_SRVTESTE',
                    'accountCanonicalForm' => 3,
                    'username' => "Administrator",
                    'password' => 'Qaz123',
                    'baseDn' => "CN=Users,DC=reducall,DC=SRVTESTE",
                ),
                'filter' => '(&(cn=*%s*)(memberOf=CN=reduwork,CN=Users,DC=reducall,DC=SRVTESTE))',
                'user_keymap' => array(
                    'ds_nome' => 'displayname', //nome
                    'nu_cpf' => null, //numero do cpf
                    'no_usuario' => 'samaccountname',//login
                    'ds_email' => array(
                        'key' => 'userprincipalname',
                        'filters' => array('strtolower'),
                    ),
                ),
            ),
        ),*/
    ),
);
