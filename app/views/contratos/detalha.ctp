<script type="text/javascript">
    //usado em tabela_execucao.js
    var baseUrlAtividadesController = '<?php echo $this->Html->url(array('controller' => 'atividades')); ?>';
    var baseUrlPagamentosController = '<?php echo $this->Html->url(array('controller' => 'pagamentos')); ?>';
</script>
<?php
$usuario = $this->Session->read('usuario');
$hasPenalidade = $this->Session->read('hasPenalidade');
$nomeArquivo = '';
$link_nc = 'edit/' . $id . '/nc';
$link_np = 'edit/' . $id . '/np';
echo $this->Html->script('tabela_execucao');
echo $this->Html->script('inicia-datetimepicker');
echo $this->Html->script('moment/moment.js');

if (Configure::read('App.config.resource.certificadoDigital') && !empty($contrato["nu_pam"]) && empty($contrato["nu_processo"]) && empty($contrato["nu_contrato"])) {
    $auth = $this->Util->getRestPkiClient()->getAuthentication();
    $token = $auth->startWithWebPki(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
    $this->Util->setNoCacheHeaders();

    echo $this->Html->script('lacuna-web-pki-2.5.0');
    echo $this->Html->script('jquery.blockUI');
}

?>

<?php if (Configure::read('App.config.resource.certificadoDigital') && !empty($contrato["nu_pam"]) && empty($contrato["nu_processo"]) && empty($contrato["nu_contrato"])) { ?>
    <script type="text/javascript">

        var pki = new LacunaWebPKI();

        function initPki() {
            // Block the UI while we get things ready
            $.blockUI();

            pki.init({
                ready: loadCertificates, // as soon as the component is ready we'll load the certificates
                defaultError: onWebPkiError
            });
        }

        // -------------------------------------------------------------------------------------------------
        // Function called once the page is loaded
        // -------------------------------------------------------------------------------------------------
        function init() {

            // Wireup of button clicks
            $('#btEncaminharFase').click(signIn);
            $('#refreshButton').click(refresh);

            // Block the UI while we get things ready
            // $.blockUI();

            // Call the init() method on the LacunaWebPKI object, passing a callback for when
            // the component is ready to be used and another to be called when an error occurs
            // on any of the subsequent operations. For more information, see:
            // https://webpki.lacunasoftware.com/#/Documentation#coding-the-first-lines
            // http://webpki.lacunasoftware.com/Help/classes/LacunaWebPKI.html#method_init
//        pki.init({
//            ready: loadCertificates, // as soon as the component is ready we'll load the certificates
//            defaultError: onWebPkiError
//        });
        }

        // -------------------------------------------------------------------------------------------------
        // Function called when the user clicks the "Refresh" button
        // -------------------------------------------------------------------------------------------------
        function refresh() {
            // Block the UI while we load the certificates
            $.blockUI();
            // Invoke the loading of the certificates
            loadCertificates();
        }

        // -------------------------------------------------------------------------------------------------
        // Function that loads the certificates, either on startup or when the user
        // clicks the "Refresh" button. At this point, the UI is already blocked.
        // -------------------------------------------------------------------------------------------------
        function loadCertificates() {

            // Call the listCertificates() method to list the user's certificates
            pki.listCertificates({

                // specify that expired certificates should be ignored
                filter: pki.filters.isWithinValidity,

                // in order to list only certificates within validity period and having a CPF (ICP-Brasil), use this instead:
                //filter: pki.filters.all(pki.filters.hasPkiBrazilCpf, pki.filters.isWithinValidity),

                // id of the select to be populated with the certificates
                selectId: 'certificateSelect',

                // function that will be called to get the text that should be displayed for each option
                selectOptionFormatter: function (cert) {
                    return cert.subjectName + ' (issued by ' + cert.issuerName + ')';
                }

            }).success(function () {

                // once the certificates have been listed, unblock the UI
                $.unblockUI();

            });
        }

        // -------------------------------------------------------------------------------------------------
        // Function called when the user clicks the "Sign In" button
        // -------------------------------------------------------------------------------------------------
        function signIn() {
            if (!$("input[type='radio'][name='data[tp_mudanca]']").is(':checked')) {
                alert('Favor selecionar uma opção!');
                return false;
            }

            if ($("#TpMudancaD").is(":checked")) {
                if ($("#co_setor_mudar").val() == '') {
                    alert('Favor informar o Setor!');
                    return false;
                }
                if ($("#co_fase_mudar").val() == '') {
                    alert('Favor informar a Fase!');
                    return false;
                }
                if ($("#jus_fase").val() == '') {
                    alert('Favor informar a Justificativa para Desvio do Fluxo!');
                    return false;
                }
            }

            var tramitarSemCertificado = document.getElementById("TramitarSemCertificado");

            if (tramitarSemCertificado.checked) {
                $('#form_mudanca_fase').submit();
            } else {
                // Block the UI while we process the authentication
                $.blockUI();

                // Get the thumbprint of the selected certificate
                var selectedCertThumbprint = $('#certificateSelect').val();

                // Call signWithRestPki() on the Web PKI component passing the token received from REST PKI and the certificate
                // selected by the user. Although we're making an authentication, at the lower level we're actually signing
                // a cryptographic nonce (a random number generated by the REST PKI service), hence the name of the method.
                pki.signWithRestPki({
                    token: '<?php echo $token; ?>',
                    thumbprint: selectedCertThumbprint,
                }).success(function () {
                    // Once the operation is completed, we submit the form
                    $('#form_mudanca_fase').submit();
                });
            }
        }

        // -------------------------------------------------------------------------------------------------
        // Function called if an error occurs on the Web PKI component
        // -------------------------------------------------------------------------------------------------
        function onWebPkiError(message, error, origin) {
            // Unblock the UI
            $.unblockUI();
            // Log the error to the browser console (for debugging purposes)
            if (console) {
                console.log('An error has occurred on the signature browser component: ' + message, error);
            }
            // Show the message to the user. You might want to substitute the alert below with a more user-friendly UI
            // component to show the error.
            alert(message);
        }

        // Schedule the init function to be called once the page is loaded
        $(document).ready(init);

    </script>
<?php } else { ?>
    <script type="text/javascript">
        function init() {
            $('.mudarfase').click(function () {
//                if (!$("input[type='radio'][name='data[tp_mudanca]']").is(':checked')) {
//                    alert('Favor selecionar uma opção!');
//                    return false;
//                }
//
//                if ($("#TpMudancaD").is(":checked")) {
//                    if ($("#co_setor_mudar").val() == '') {
//                        alert('Favor informar o Setor!');
//                        return false;
//                    }
//                    if ($("#co_fase_mudar").val() == '') {
//                        alert('Favor informar a Fase!');
//                        return false;
//                    }
//                    if ($("#jus_fase").val() == '') {
//                        alert('Favor informar a Justificativa para Desvio do Fluxo!');
//                        return false;
//                    }
//                }

                $('#form_mudanca_fase').submit();
            });
        }

        $(document).ready(init);
    </script>
<?php } ?>

<div class="contratos form">
    <input type="hidden" id="base_url_atv" value="<?php echo $this->base; ?>"/>
    <!-- fim da vigência do contrato -->

    <input type="hidden" id="dt_fim_vigencia_atividade"
           value="<?php echo ($contrato['dt_fim_vigencia'] != null) ? $contrato['dt_fim_vigencia'] : $contrato['dt_fim_processo'] ?>"/>

    <!-- id do contrato -->
    <input id="idContrato" type="hidden" value="<?php echo $id ?>"/>
    <!-- fieldset id="fsContrato" class="ativo"-->

    <!--?php echo $this->Form->create('Contrato');?-->

    <?php
    if ($cadContrato != 'edit_pam' && $cadContrato != 'edit' && $cadContrato != 'detalha') {
        ?>
        <div class="acoes-formulario-top clearfix">
            <div class="span4">
                <div class="pull-left btn-group">
                    <input type="hidden" id="nu_contrato_detalhe" value="<?php echo $contrato['nu_contrato']; ?>">
                    <?php
                    if ($contrato['nu_contrato'] == "" && $contrato['nu_processo'] != "" && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/edit')) { ?>
                        <a href="<?php echo $this->Html->url($link_nc); ?>" data-toggle="modal"
                           class="btn btn-small btn-primary"
                           title="<?php echo __('Cadastrar Contrato'); ?>"><?php echo __('Cadastrar Contrato'); ?></a>
                    <?php } ?>
                    <?php
                    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/add')):
                        if ($contrato['nu_contrato'] != ""):
                            ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'add', $id)); ?>"
                               data-toggle="modal" class="btn btn-small btn-primary"
                               title="Criar novo contrato derivado"><i class="icon-pencil icon-white"></i> Criar novo
                                contrato derivado </a>
                        <?php endif; ?>

                        <?php if (!$hasPenalidade && $contrato['nu_pam'] == "") : ?>
                        <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'edit', $id)); ?>"
                           data-toggle="modal" class="btn btn-small btn-primary" title="Editar"
                           style="margin-right: 2px;"><i class="icon-pencil icon-white"></i> Editar</a>
                        <?php endif; ?>
                    
                        <?php if ($contrato['nu_pam'] != "") : ?>
                        <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'edit_pam', $id)); ?>"
                           data-toggle="modal" class="btn btn-small btn-primary" title="Editar"
                           style="margin-right: 2px;"><i class="icon-pencil icon-white"></i> Editar</a>
                        <?php endif; ?>

                    <?php endif; ?>
                    <!-- impressão -->
                    <a href="#view_impressao" class="v_impressao btn btn-small btn-primary" data-toggle="modal"><i
                                class="icon-print"></i> Imprimir</a>
                    <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
            </div>
            <?php if ($contrato["co_fase"] > 0 && Configure::read('App.config.resource.available.fluxo')) { ?>
                <div class="span4 center">
                    <span class="label label-large label-info arrowed-right arrowed"><b>Fase Atual:</b> <?php echo $this->Print->faseAtual($this->data['Fase']['ds_fase'], $this->data['SetorAtual']['ds_setor']); ?></span>
                    <?php if (isset($this->data['Andamento']['ds_fase_atual']) && $this->data['Andamento']['ds_fase_atual'] != "") { ?>
                        <span title="<?php echo $this->data['Andamento']['ds_fase_atual']; ?>" class="alert-tooltip">
                                                    <i class="orange icon-comments-alt bigger-150"></i>
                                                </span>
                    <?php } ?>
                    <?php if (isset($this->data['Andamento']['ds_justificativa_fluxo']) && $this->data['Andamento']['ds_justificativa_fluxo'] != "") { ?>
                        <span title="Fluxo Desviado. Justificativa: <?php echo $this->data['Andamento']['ds_justificativa_fluxo']; ?>"
                              class="alert-tooltip">
                                                    <i class="icon-share-alt red bigger-150"></i>
                                                </span>
                    <?php } ?>
                </div>
                <?php
                if ($this->data['Fluxo']['nu_sequencia_um'] != "" && $this->data['Fluxo']['nu_sequencia_um'] != null) {
                    $corAviso;
                    $mensagemAviso;
                    $tempoDecorrido;
                    $this->Print->setPrazoDecorridoDetalhaProcesso($this->data['Andamento']['dt_andamento'], $this->data['Andamento']['dt_fim'],
                        $this->data['Fase']['mm_duracao_fase'], $corAviso, $mensagemAviso, $tempoDecorrido);
                    ?>
                    <div class="span1 center alert-tooltip <?php echo $corAviso; ?>"
                         title="<?php echo $mensagemAviso; ?>">
                        <i class="icon-time bigger-110 <?php echo $corAviso; ?>"></i>
                        <b><?php echo $tempoDecorrido; ?></b>
                    </div>
                    <?php
                }
                ?>

                <?php if ($this->data['Fluxo']['nu_sequencia_um'] > 0 || $this->data['Fluxo']['nu_sequencia_dois'] > 0) { ?>
                    <div class="span3">
                        <div class="pull-right btn-group">
                            <?php
                            if (($usuario["Setor"]["ds_setor"] == 'Chefia de Divisão' && $usuario['Usuario']['co_setor'] == $this->data['Contrato']['co_setor'] && $contrato['co_assinatura_chefe'] == null && $this->data['Fase']['ds_fase'] == 'Encaminhar Pedido') || ($usuario["Setor"]["ds_setor"] == 'Ordenador de Despesas' && $usuario['Usuario']['co_setor'] == $this->data['Contrato']['co_setor'] && $contrato['co_assinatura_ordenador'] == null && $this->data['Fase']['ds_fase'] == 'Analisar e Autorizar')) {
                                ?>
                                <form style="height: 12px;" method="post"
                                      action="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'assinar_eletronicamente')); ?>"
                                      accept-charset="utf-8">
                                    <?php
                                    echo $this->Form->hidden('co_contrato', array('value' => $contrato['co_contrato']));
                                    echo $this->Form->hidden('co_usuario', array('value' => $usuario['Usuario']['co_usuario']));
                                    ?>
                                    <button type="submit" id="assinaturaEletronica" title="Assinatura Eletrônica"
                                            class="btn btn-small btn-primary">
                                        Assinatura Eletrônica
                                    </button>
                                </form>
                                <?php
                            }
                            ?>

                                <button id="encaminharFase" title="Encaminhar" class="btn btn-small btn-success"><i
                                            class="icon-share-alt"></i> Andamento
                                </button>

                            <?php
                            if ( $this->Modulo->isProcesso() && $contrato['nu_contrato'] == "" && $contrato['nu_processo'] == "" && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/add')) { ?>
                            <a href="<?php echo $this->Html->url($link_np); ?>" data-toggle="modal"
                               class="btn btn-small btn-primary" title="Cadastrar Processo">Cadastrar
                                    Processo</a><?php
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php
    }
    ?>
    <?php if ($contrato['nu_contrato'] != "" || !$this->Modulo->isInscricao()) { ?>
        <div id="accordion1" class="accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle"
                       id="identificacao">
                        <?php
                        if ($this->Modulo->isPam() && $contrato['nu_processo'] == "" && $contrato['nu_contrato'] == "") {
                            __($this->Modulo->isSuporteDocumental() ? 'Documento' : 'PAM');
                            echo ": " . $this->Print->pam($contrato['nu_pam']);
                            $nomeArquivo = __('PAM', true) . '_' . $this->Print->pam($contrato['nu_pam']);
                        } else if ($contrato['nu_contrato'] == "") {
                            __('Processo');
                            echo ": " . $this->Print->processo($contrato['nu_processo']);
                            $nomeArquivo = 'PROCESSO_' . $this->Print->processo($contrato['nu_processo']);
                        } else {
                            __('Dados Gerais');
                            //echo ": " . $this->Print->contrato($contrato['nu_contrato']);
                            //$nomeArquivo = 'CONTRATO_' . $this->Print->contrato($contrato['nu_contrato']);
                        }
                        ?>
                    </a>
                </div>

                <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                    <div class="accordion-inner">
                        <div class="row-fluid">

                            <div class="span12">
                                <?php if (isset($contrato['co_contrato_pai']) && $contrato['co_contrato_pai'] != null) { ?>
                                    <b>Este contrato é derivado do contrato <a
                                                href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'detalha', $contratoPai['Contrato']['co_contrato'])); ?>"><?php echo $this->Print->contrato($contratoPai['Contrato']['nu_contrato']); ?></a></b>
                                <?php } ?>

                                <div class="body-box" style="word-wrap: break-word; white-space:normal">
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <?php if ($contrato['nu_contrato'] != "") { ?>
                                                <dt>
                                                  Número de Contrato :
                                                </dt>
                                                <dd>
                                                  <?php echo $this->Print->contrato($contrato['nu_contrato']); ?>
                                                </dd>
                                            <?php } ?>

                                            <?php if ($this->Modulo->isPam() && $contrato['nu_processo'] != "" && $contrato['nu_pam'] != "") { ?>
                                                <dt><?php __($this->Modulo->isSuporteDocumental() ? 'Documento' : 'PAM') ?>
                                                    :
                                                </dt>

                                                <dd><?php echo $this->Print->pam($contrato['nu_pam']); ?></dd>
                                            <?php } ?>

                                            <?php if ($this->Modulo->isProcesso() && $contrato['nu_contrato'] != "") { ?>
                                                <dt>Processo:</dt>
                                                <dd><?php echo $this->Print->processo($contrato['nu_processo'], true); ?></dd>
                                            <?php } ?>
                                            <?php if ($this->Modulo->isPam() && $this->Modulo->isCamposContrato('co_contratante') && $contrato['nu_processo'] == "" && $contrato['nu_contrato'] == "") { ?>
                                                <dt>Unidade Administrativa:</dt>
                                                <dd><?php echo $this->data['Setor']['ds_setor'] ? $this->data['Setor']['ds_setor'] : '-' ?></dd>
                                            <?php } ?>
                                            <?php if ($contrato['nu_pam'] != "" || $contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "" || $this->Modulo->isPamFornecedor) { ?>
                                                    <dt>
                                                        <?php if ($this->Modulo->getNomeContratador() == true) {__('Contratador');} else{ echo 'Cliente'; } ?> :
                                                    </dt>

                                                    <dd>
                                                        <a href="<?php echo $this->base; ?>/clientes/mostrar/<?php echo $this->data['Cliente']['co_cliente']; ?>">
                                                            <?php echo $this->data['Cliente']['no_razao_social']; ?>
                                                        </a>
                                                    </dd>

                                                <?php 
                                                echo $this->Form->hidden('Cliente.nu_cnpj');
                                                if ($this->data['Cliente']['no_razao_social'] != '') { ?>
                                                    <dt>
                                                      Cliente E-mail :
                                                    </dt>
                                                    <dd>
                                                      <?php echo $this->data['Cliente']['ds_email'] == '' ? '---' : $this->data['Cliente']['ds_email']; ?>
                                                    </dd>
                                                <?php } ?>
                                                    
                                                <?php if ($this->data['Cliente']['no_razao_social'] != '') { ?>
                                                    <dt>
                                                      Cliente Telefone :
                                                    </dt>
                                                    <dd>
                                                      <?php echo $this->data['Cliente']['nu_telefone'] == '' ? '---' : $this->data['Cliente']['nu_telefone']; ?>
                                                    </dd>
                                                <?php } ?>
                                                
                                                <?php if ($this->data['Cliente']['no_responsavel'] != '') { ?>
                                                    <dt>
                                                      Cliente Responsável :
                                                    </dt>
                                                    <dd>
                                                      <?php echo $this->data['Cliente']['no_responsavel'] == '' ? '---' : $this->data['Cliente']['no_responsavel']; ?>
                                                    </dd>
                                                <?php } ?>
                                                    
                                                <?php if ($contrato['nu_pam'] != "") { ?>
                                                    <dt>
                                                      Responsável / Parceiro :
                                                    </dt>
                                                    <dd>
                                                      <?php echo $parceiro['Usuario']['ds_nome'] == '' ? '---' : $parceiro['Usuario']['ds_nome']; ?>
                                                    </dd>

                                                    <dt>
                                                      Responsável Reducall :
                                                    </dt>
                                                    <dd>
                                                      <?php echo $gestorAtual['ds_nome'] == '' ? '---' : $gestorAtual['ds_nome']; ?>
                                                    </dd>
                                                <?php } ?>
                                                    

                                                <?php if ($this->Modulo->isCamposContrato('ds_contratacao') == true) { ?>
                                                    <dt><?php __('Modalidade de Contratação'); ?>:</dt>
                                                    <dd><?php echo $this->data['Contratacao']['ds_contratacao'] ? $this->data['Contratacao']['ds_contratacao'] : '-' ?></dd>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($contrato['nu_contrato'] != "") { ?>
                                                <?php if ($this->Modulo->isCamposContrato('nu_licitacao') == true) { ?>
                                                    <dt>Nº Licitação:</dt>
                                                    <dd><?php echo $contrato['nu_licitacao'] ? $this->Print->licitacao($contrato['nu_licitacao']) : '-' ?></dd>
                                                <?php } ?>
                                                <?php if ($this->Modulo->isCamposContrato('dt_publicacao') == true) { ?>
                                                    <dt>Data de Publicação:</dt>
                                                    <dd><?php echo $contrato['dt_publicacao'] ? $contrato['dt_publicacao'] : '-' ?></dd>
                                                <?php } ?>
                                                <dt>Tipo do Serviço:</dt>
                                                <dd><?php echo $contrato['ds_tipo_servico'] ? $contrato['ds_tipo_servico'] : '-'; ?>
                                                    &nbsp;</dd>
                                            <?php } ?>


                                            <?php if (!$this->Modulo->isSuporteDocumental()) { ?>
                                                <?php if ($this->Modulo->isCamposContrato('tp_aquisicao') == true && $contrato['tp_aquisicao'] != "") { ?>
                                                    <dt><?php __('Tipo de Aquisição') ?>:</dt>
                                                    <dd><?php if ($contrato['tp_aquisicao'] == "S") echo "Serviço";
                                                        if ($contrato['tp_aquisicao'] == "N") echo "Produto";
                                                        if ($contrato['tp_aquisicao'] == "SN") echo "Serviços e Materiais"; ?></dd>
                                                <?php } ?>
                                                <?php if ($this->Modulo->isCamposContrato('ic_dispensa_licitacao') == true && $contrato['ic_dispensa_licitacao'] != "") { ?>
                                                    <dt><?php __('Dispensa Licitação') ?>:</dt>
                                                    <dd>
                                                        &nbsp;&nbsp;<?php if ($contrato['ic_dispensa_licitacao'] == "S") echo "Sim";
                                                        if ($contrato['ic_dispensa_licitacao'] == "N") echo "Não"; ?></dd>
                                                <?php } ?>
                                            <?php } ?>
                                        </dl>
                                        <?php if (isset($assRequisitante) || isset($assChefe) || isset($assOrdenador)) { ?>
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="widget-box">
                                                        <div class="widget-header widget-header-small"><h4>Requisitante
                                                                e Assinaturas</h4></div>
                                                        <div class="widget-body">
                                                            <div class="widget-main">
                                                                <div class="row-fluid">
                                                                    <div class="span12">
                                                                        <dl class="dl-horizontal">
                                                                            <?php if (isset($assRequisitante)) { ?>
                                                                                <dt>Requisitante:</dt>
                                                                                <dd><?php echo "Documento solicitado por " . $assRequisitante["AssinaturasRequisitante"]["nome_completo"] . ", " . $assRequisitante["AssinaturasRequisitante"]["cargo"] . ", em " . substr($assRequisitante["AssinaturasRequisitante"]["data_assinatura"], 0, 10) . " às " . substr($assRequisitante["AssinaturasRequisitante"]["data_assinatura"], 11, 5) . ", conforme horário oficial de Brasília."; ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (isset($assChefe)) { ?>
                                                                                <dt>Ass. do Chefe de Divisão:</dt>
                                                                                <dd><?php echo "Documento assinado eletrônicamente por " . $assChefe['Usuario']['ds_nome'] . ", " . $assChefe['Setor']['ds_setor'] . ", em " . substr($dtAssChefe, 0, 10) . " às " . substr($dtAssChefe, 11, 5) . ", conforme horário oficial de Brasília, com fundamento no § 1°, art. 6°, do Decreto n° 8.539 de 08/10/2015 da Presidência da República."; ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (isset($assOrdenador)) { ?>
                                                                                <dt>Ass. do Ordenador de Despesas:</dt>
                                                                                <dd><?php echo "Documento assinado eletrônicamente por " . $assOrdenador['Usuario']['ds_nome'] . ", " . $assOrdenador['Setor']['ds_setor'] . ", em " . substr($dtAssOrdenador, 0, 10) . " às " . substr($dtAssOrdenador, 11, 5) . ", conforme horário oficial de Brasília, com fundamento no § 1°, art. 6°, do Decreto n° 8.539 de 08/10/2015 da Presidência da República."; ?></dd>
                                                                            <?php } ?>
                                                                        </dl>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        </dl>
                                    </div>
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <?php if ($contrato['nu_pam'] != "") { ?>
                                                <dt>
                                                  Valor Estimado das Contas :
                                                </dt>
                                                <dd>
                                                  <?php echo $contrato['vl_global'] == '' ? '---' : $this->Formatacao->moeda($contrato['vl_global']); ?>
                                                </dd>

                                                <dt>
                                                  Data Inicial :
                                                </dt>
                                                <dd>
                                                  <?php echo $contrato['dt_autorizacao_pam'] == '' ? '---' : $contrato['dt_autorizacao_pam']; ?>
                                                </dd>
                                            <?php } ?>
                                                    
                                            <?php if ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "" || ($contrato['nu_pam'] != "" && $contrato['ds_observacao'] != "")) { ?>
                                                <dt>Observações:</dt>
                                                <dd>
                                                    <?php echo $contrato['ds_observacao'] ? $contrato['ds_observacao'] : '-' ?>
                                                </dd>
                                            <?php } ?>
                                            <?php if ($contrato['ds_objeto'] != "") { ?>
                                                <dt>Objeto:</dt>
                                                <dd>
                                                    <?php echo $contrato['ds_objeto'] ? $contrato['ds_objeto'] : '-' ?>
                                                </dd>
                                            <?php } ?>
                                            <?php if ($contrato['nu_contrato'] != "") { ?>
                                                <?php if ($this->Modulo->isCamposContrato('ds_fundamento_legal') == true) { ?>
                                                    <dt>Fundamento Legal:</dt>
                                                    <dd><?php echo $contrato['ds_fundamento_legal'] ? $contrato['ds_fundamento_legal'] : '-' ?></dd>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if (!$this->Modulo->isSuporteDocumental()) { ?>
                                                <?php if ($this->Modulo->isCamposContrato('ic_ata_vigente') == true && $contrato['ic_ata_vigente'] != "") { ?>
                                                    <dt><?php __('Possui Ata Vigente') ?>:</dt>
                                                    <dd>
                                                        &nbsp;&nbsp;<?php if ($contrato['ic_ata_vigente'] == "S") echo "Sim";
                                                        if ($contrato['ic_ata_vigente'] == "N") echo "Não"; ?></dd>
                                                <?php } ?>
                                                <?php if ($this->Modulo->isCamposContrato('ic_ata_orgao') == true && $contrato['ic_ata_orgao'] != "") { ?>
                                                    <dt><?php __('Ata Pertence ao Órgão') ?>:</dt>
                                                    <dd><?php if ($contrato['ic_ata_orgao'] == "S") echo "Sim";
                                                    if ($contrato['ic_ata_orgao'] == "N") echo "Não"; ?></dt>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($this->Modulo->isCamposContrato('dt_tais') == true) { ?>
                                                <dt><?php __('Data do TAIS') ?>:</dt>
                                                <dd><?php echo $contrato['dt_tais']; ?></dd>
                                            <?php } ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if (isset($contrato['ds_requisitante']) && $contrato['ds_requisitante'] != "") { ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-small"><h4>Informações do
                                                Requisitante</h4></div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <dl class="dl-horizontal">
                                                            <dt>Nome Completo do Requisitante</dt>
                                                            <dd id="confirRequisitante"><?php echo $contrato['ds_requisitante']; ?></dd>
                                                            <dt>Telefone do Requisitante</dt>
                                                            <dd id="confirTelefone"><?php echo $contrato['tl_requisitante']; ?></dd>
                                                            <dt>Assunto do Pedido</dt>
                                                            <dd id="confirAssunto"><?php echo $contrato['ds_assunto']; ?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="span6">
                                                        <dl class="dl-horizontal">
                                                            <dt>Posto/Grad/Cat. Func. do Requisitante</dt>
                                                            <dd id="confirPosto"><?php echo $contrato['pt_requisitante']; ?></dd>
                                                            <dt>PAM solicitado em</dt>
                                                            <dd id="confirData"><?php echo $contrato['dt_solicitacao']; ?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (isset($contrato['ds_vantagem']) && $contrato['ds_vantagem'] != "") { ?>
                            <div class="row-fluid">

                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-small"><h4>Informações do Plano de
                                                Trabalho</h4></div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Objeto</dt>
                                                            <dd id="confirObjeto"><?php echo $contrato['ds_objeto_plano']; ?></dd>
                                                            <dt>Vantagens e Economicidade para a Administração</dt>
                                                            <dd id="confirVantagens"><?php echo $contrato['ds_vantagem']; ?></dd>
                                                            <dt>Critérios de Controles e Registros a Serem Adotados</dt>
                                                            <dd id="confirCriterios"><?php echo $contrato['ds_criterio']; ?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="span4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Justificativa para Aquisição</dt>
                                                            <dd id="confirJustificativaAquisicao"><?php echo $contrato['ds_justificativa']; ?></dd>
                                                            <dt>Verificação dos Resultados</dt>
                                                            <dd id="confirVerificacao"><?php echo $contrato['ds_resultado']; ?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="span4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Demanda Prevista e Quantidade Bens e Serviços a serem
                                                                Contratados
                                                            </dt>
                                                            <dd id="confirDemandaPrevista"><?php echo $contrato['ds_demanda']; ?></dd>
                                                            <dt>Aproveitamento de Servidores do Quadro, de Bens, de
                                                                Equipamentos
                                                            </dt>
                                                            <dd id="confirAproveitamento"><?php echo $contrato['ds_aproveitamento']; ?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (isset($this->data['ContratosItem']) && is_array($this->data['ContratosItem']) && count($this->data['ContratosItem']) > 0) { ?>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-small"><h4>Itens do Pedido de
                                                Aquisição</h4></div>
                                        <div class="widget-body">
                                            <div class="widget-main" id="grupos">
                                                <?php $contador = 0;
                                                $vltotal = 0 ?>
                                                <?php
                                                foreach ($this->data['ContratosItem'] as $itens) {
                                                    if ($contador == 0) {
                                                        if ($itens['co_siasg'] > 0) {
                                                            echo '<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span1">Codigo SIASG</div></div>';
                                                        } elseif ($itens['vl_unitario'] > 0) {
                                                            echo 'Fornecedor: ' . $itens['Fornecedor']['no_razao_social'];
                                                            echo '<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Item Preg.</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span2">Valor Unitário (R$)</div><div class="span2">Total Item (R$)</div></div>';
                                                        } else {
                                                            echo '<div class="row-fluid"><div class="span1">Ordem</div><div class="span1">Unidade</div><div class="span1">Qtd Solicit.</div><div class="span3">Descrição completa do material/serviço</div><div class="span2">Media Mensal Consumo</div><div class="span2">CAT MAT / SV</div></div>';
                                                        }
                                                    }
                                                    $contador++;

                                                    echo '<div class="row-fluid">';
                                                    echo '  <div class="span1">' . $itens['nu_ordem'] . '</div>';
                                                    if ($itens['nu_item_pregao'] != '') {
                                                        echo '  <div class="span1">' . $itens['nu_item_pregao'] . '</div>';
                                                    }
                                                    echo '  <div class="span1">' . $itens['nu_unidade'] . '</div>';
                                                    echo '  <div class="span1">' . $itens['qt_solicitada'] . '</div>';
                                                    echo '  <div class="span3">' . $itens['ds_demanda'] . '</div>';
                                                    if ($itens['co_siasg'] != '') {
                                                        echo '  <div class="span1">' . $itens['co_siasg'] . '</div>';
                                                    }
                                                    if ($itens['qt_consumo'] != '') {
                                                        echo '  <div class="span2">' . $itens['qt_consumo'] . '</div>';
                                                    }
                                                    if ($itens['co_cat'] != '') {
                                                        echo '  <div class="span2">' . $itens['co_cat'] . '</div>';
                                                    }
                                                    if ($itens['vl_unitario'] != '') {
                                                        $vltotal += $itens['qt_solicitada'] * $itens['vl_unitario'];
                                                        echo '  <div class="span2">' . $this->Formatacao->moeda($itens['vl_unitario']) . '</div>';
                                                        echo '  <div class="span2">' . $this->Formatacao->moeda($itens['qt_solicitada'] * $itens['vl_unitario']) . '</div>';
                                                    }
                                                    echo '</div>';
                                                    ?>

                                                    <?php
                                                }
                                                if ($vltotal > 0) { ?>
                                                    <div class="row-fluid">
                                                        <div class="span9">&nbsp;</div>
                                                        <div class="span2">
                                                            <div class="input text"><label for="confirVlTotalGeral"
                                                                                           id="confirVlTotalGeral"><?php echo '<b>Total Geral (R$)</b> ' . $this->Formatacao->moeda($vltotal); ?></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($contrato['nu_contrato'] == '' && $this->Modulo->isInscricao()) { ?>
        <div id="accordion1" class="accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle"
                       id="identificacao">
                        <?php __('Processo');
                        echo ": " . $this->Print->processo($contrato['nu_processo']); ?>
                    </a>
                </div>

                <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                    <div class="accordion-inner">

                        <div class="row-fluid">

                            <div class="span12 ">

                                <div class="body-box">
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Atleta:</dt>
                                            <dd><a><?php echo $this->data['Fornecedor']['no_razao_social']; ?></a></dd>
                                            <dt>CPF:</dt>
                                            <dd><?php echo $this->Print->cpf($this->data['Fornecedor']['nu_cnpj']); ?></dd>
                                            <dt>Tipo:</dt>
                                            <dd><?php
                                                if ($this->data['Contrato']['co_contratacao'] > 0) {
                                                    echo $contratacao['ds_contratacao'];
                                                } else {
                                                    echo '--';
                                                }
                                                ?></dd>
                                            <dt>Confederação do Atleta:</dt>
                                            <dd><?php echo $this->data['Confederacao']['ds_confederacao']; ?></dd>
                                            <dt>Federação do Atleta:</dt>
                                            <dd><?php echo $this->data['Federacao']['ds_confederacao']; ?></dd>
                                        </dl>
                                    </div>
                                    <div class="span6">
                                        <dl class="dl-horizontal">
                                            <dt>Modalidade:</dt>
                                            <dd><?php echo $this->data['Modalidade']['ds_modalidade']; ?></dd>
                                            <dt>Prova:</dt>
                                            <dd><?php echo $this->data['Prova']['ds_prova']; ?></dd>
                                            <dt>Evento ou Competição:</dt>
                                            <dd><?php echo $this->data['Evento']['ds_evento']; ?></dd>
                                            <dt>Classificação Obitida:</dt>
                                            <dd><?php
                                                if ($this->data['Contrato']['co_classificacao'] > 0) {
                                                    echo $classificacoes[$this->data['Contrato']['co_classificacao']];
                                                } else {
                                                    echo '--';
                                                }
                                                ?></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($contrato['nu_contrato'] != "" || (!$this->Modulo->isInscricao() && $contrato['nu_processo'] != "")) { ?>
        <div id="accordion2" class="accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseInfBs" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle"
                       id="inf_basicas">
                        Informações Básicas
                    </a>
                </div>

                <div class="accordion-body in collapse" id="collapseInfBs" style="height: auto;">
                    <div class="accordion-inner">

                        <div class="row-fluid">
                            <div class="span4">
                                <div class="widget-header widget-header-small"><h4>
                                        <font><?php __('Detalhamento'); ?></font></h4></div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <dl class="dl-horizontal">
                                                <dt>Responsável Araras:</dt>
                                                <dd><?php
                                                //var_dump($this->data['Contrato']);

                                                if ($this->data["GestorSuplente"]["ds_nome"] == '' || $this->data["GestorSuplente"]["ds_nome"] == NULL){
                                                  echo "---";
                                                }else{
                                                  echo $this->data["GestorSuplente"]["ds_nome"];
                                                }

                                                 ?></dd>
                                                <dt>Parceiro:</dt>
                                                <dd><?php
                                                //var_dump($this->data['Contrato']);

                                                if ($parceiro['Usuario']['ds_nome'] == '' || $parceiro['Usuario']['ds_nome'] == NULL){
                                                  echo "---";
                                                }else{
                                                  echo $parceiro['Usuario']['ds_nome'];
                                                }

                                                 ?></dd>
                                            <?php if ($this->Modulo->isCamposContrato('co_contratante') == true) { ?>
                                                <dt>Unidade Administrativa:</dt>
                                                <dd><?php echo $this->data['Setor']['ds_setor']; ?>&nbsp;</dd>
                                            <?php } ?>
                                            <?php if ($this->Modulo->isCamposContrato('co_executante') == true) { ?>
                                                <dt>Unidade Executora:</dt>
                                                <dd><?php echo $this->data['Executante']['ds_setor']; ?>&nbsp;</dd>
                                            <?php } ?>
                                            <?php if ($gestorAtual['ds_nome'] != "") { ?>
                                                <dt><?php echo __('Responsável Brasília'); ?>:</dt>
                                                <dd><?php echo $gestorAtual['ds_nome']; ?>&nbsp;</dd>
                                            <?php } ?>
                                            <?php if ($fiscalAtual['ds_nome'] != "") { ?>
                                                <?php if (isset($fiscais)) { ?>
                                                    <dt><?php __('Fiscal'); ?>:</dt>
                                                    <?php foreach ($fiscais as $fiscal) { ?>
                                                        <dd><?php echo $fiscal['Usuario']['ds_nome']; ?></dd>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($this->Modulo->isCamposContrato('co_modalidade') == true) { ?>
                                                <dt>Tipo de <?php __('Contrato'); ?>:</dt>
                                                <dd><?php echo $this->data['Modalidade']['ds_modalidade']; ?>&nbsp;</dd>
                                            <?php } ?>

                                            <?php if ($this->Modulo->isCamposContrato('co_servico') == true) { ?>
                                                <dt>Descrição do Serviço:</dt>
                                                <dd><?php echo $this->data['Servico']['ds_servico']; ?>&nbsp;</dd>
                                            <?php }
                                            if ($this->Modulo->isCamposContrato('co_categoria') == true) {
                                                ?>
                                                <dt><?php __('Categoria'); ?>:</dt>
                                                <dd><?php echo $this->data['Categoria']['no_categoria']; ?>&nbsp;</dd>
                                            <?php }
                                            if ($this->Modulo->isCamposContrato('co_subcategoria') == true) {
                                                ?>
                                                <dt><?php __('Sub Categoria'); ?>:</dt>
                                                <dd><?php echo $this->data['Subcategoria']['no_subcategoria']; ?>
                                                    &nbsp;</dd>
                                            <?php } ?>
                                            <?php if ($this->Modulo->isCamposContrato('co_situacao') && $contrato['co_situacao'] != "") { ?>
                                                <dt><?php __('Situação') ?>:</dt>
                                                <dd><?php
                                                    echo $this->data['Situacao']['ds_situacao'];
                                                    if ($contrato['fg_financeiro'] == 'D') {
                                                        echo "<font color=\"#FF0000\"> - Devolvido pelo Financeiro</font>";
                                                    }
                                                    ?>&nbsp;
                                                </dd>
                                            <?php } ?>
                                            <?php if ($this->Modulo->isCamposContrato('st_repactuado') && isset($contrato['st_repactuado']) && $contrato['st_repactuado'] != '') { ?>
                                                <dt>Prorrogável:</dt>
                                                <dd><?php
                                                    $repactuados = array('S' => 'Sim', 'N' => 'Não');
                                                    if ($contrato['st_repactuado'] != null && $contrato['st_repactuado'] != '') {
                                                        echo $repactuados[$contrato['st_repactuado']];
                                                    }
                                                    ?>&nbsp;
                                                </dd>
                                            <?php } ?>
                                            <?php if ($contrato['nu_contrato'] != "") { ?>
                                                <input type="hidden" id="dt_fim_vigencia_contrato"
                                                       value="<?php echo $this->Print->proximoDia($contrato['dt_fim_vigencia']); ?>">
                                                <?php
                                                if (empty($contrato['dt_fim_vigencia_inicio'])) {
                                                    $contrato['dt_fim_vigencia_inicio'] = $contrato['dt_fim_vigencia'];
                                                }
                                                $diferenca = $this->Print->difEmDias($contrato['dt_ini_vigencia'], $contrato['dt_fim_vigencia']);
                                                $diferenca = 0;
                                                if ($diferenca > 0) {
                                                    ?>
                                                    <dt width="80">Período Inicial:</dt>
                                                    <dd><?php echo $contrato['dt_ini_vigencia']; ?>
                                                        à <?php echo $contrato['dt_fim_vigencia_inicio'] . " ($diferenca dias)" ?></dd>

                                                    <dt width="80">Período Aditivado:</dt>
                                                <?php } else { ?>
                                                    <dt width="80">Período:</dt>
                                                <?php } ?>
                                                <!-- FOI MODIFICADO O ID, PORQUE O ID MUDA O VALOR DA DATA FIM DE VIGÊNCIA -->
                                                <dd id="sid_periodo_contrato"><?php echo $contrato['dt_ini_vigencia'] . "  à  " . $contrato['dt_fim_vigencia'] ?> </dd>

                                                <?php if ($this->Modulo->isCamposContrato('dt_assinatura')) { ?>
                                                    <dt width="80">Data de Assinatura:</dt>
                                                    <dd><?php echo $contrato['dt_assinatura']; ?></dd>
                                                <?php } ?>
                                                <?php if ($this->Modulo->isCamposContrato('dt_fim_suporte')) { ?>
                                                    <dt width="80">Fim do suporte:</dt>
                                                    <dd><?php echo $contrato['dt_fim_suporte']; ?>&nbsp;</dd>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <dt width="80">Período do Processo:</dt>
                                                <dd><?php echo $contrato['dt_ini_processo']; ?> &nbsp;&nbsp;à&nbsp;&nbsp; <?php echo $contrato['dt_fim_processo']; ?></dd>
                                                <?php if (!empty($contrato['dt_fim_garantia'])) { ?>
                                                    <dt width="80">Termino da Garantia:</dt>
                                                    <dd><?php echo $contrato['dt_fim_garantia']; ?></dd>
                                                <?php }
                                            } ?>

                                            <?php if ($this->Modulo->isCamposContrato('nu_qtd_licensas')) { ?>
                                                <dt>Quantidade de Licensas</dt>
                                                <dd><?php echo $this->data['Contrato']['nu_qtd_licensas']; ?>&nbsp;</dd>
                                            <?php } ?>
                                        </dl>
                                    </div>


                                </div>
                            </div>
                            <div class="span4">
                                <div class="widget-header widget-header-small"><h4>
                                        <font><?php __('Recursos Financeiros'); ?></font></h4></div>
                                <div class="widget-body">
                                    <div class="widget-main" id="totalizaValores">
                                    </div>
                                </div>
                            </div>

                            <div class="span4 ">
                                <div class="widget-header widget-header-small"><h4>
                                        <?php __('Percentuais'); ?></h4>
                                </div>
                                <div class="widget-body">
                                    <div class="infobox-container" id="totalizaValores2">

                                    </div>
                                </div>
                                <br />
                                <div class="widget-header widget-header-small"><h4>
                                        <?php __('Projeções de Recebimento Reducall'); ?></h4>
                                </div>
                                <div class="widget-body">
                                  <div class="widget-main" id="totalizaValores">

                                      <dl class="dl-horizontal">
                                      <dt>12 Meses: </dt>
                                      <dd>
                                          <?php echo $this->Formatacao->moeda( ($this->data['Contrato']['vl_global'] - $this->data['Contrato']['vl_inicial']) * 12 ); ?>
                                      </dd>
                                      <dt>24 Meses: </dt>
                                      <dd>
                                          <?php echo $this->Formatacao->moeda( ($this->data['Contrato']['vl_global'] - $this->data['Contrato']['vl_inicial']) * 24 ); ?>
                                      </dd>
                                      <dt>36 Meses: </dt>
                                      <dd>
                                          <?php
                                            echo $this->Formatacao->moeda( ($this->data['Contrato']['vl_global'] - $this->data['Contrato']['vl_inicial']) * 36 );

                                            ?>
                                      </dd>

                                        </dl>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    $inativoall = true;
    $abaAtiva = null;

    $abas = array();

    if ($this->Modulo->isInscricao() && $contrato['nu_contrato'] == '') {
        $abas[] = array('fsAvaliacao', 'Avaliação', true, $this->Html->url(array('controller' => 'historicos', 'action' => 'iframe', $id)));
    }

    if (Configure::read('App.config.resource.available.fiscal') && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos_fiscais/iframe') && $contrato['nu_contrato'] != "") {
        $abas[] = array('fsFiscais', __('Fiscalização', true), true, $this->Html->url(array('controller' => 'contratos_fiscais', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isGarantia() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'garantias/iframe') && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsGarantias', __('Garantias', true), true, $this->Html->url(array('controller' => 'garantias', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isGarantiaSuporte() && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsGarantiasSuporte', __('Garantia de Suporte', true), true, $this->Html->url(array('controller' => 'garantias_suporte', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isProduto() && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsProdutos', __('Produtos', true), true, $this->Html->url(array('controller' => 'produtos', 'action' => 'iframe', $id)));
    }
    
    if ($this->Modulo->isContas() && $contrato['nu_contrato'] != "") {
        $abas[] = array('fsContas', __('Produtos', true), true, $this->Html->url(array('controller' => 'contas', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isOficio() && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsOficios', __('Ofícios', true), true, $this->Html->url(array('controller' => 'oficios', 'action' => 'iframe', $id)));
    }

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'aditivos/iframe') && $this->Modulo->isAditivo() && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'aditivos/iframe') && $this->Modulo->isAditivo() && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsAditivos', 'Aditivos', true, $this->Html->url(array('controller' => 'aditivos', 'action' => 'iframe', $id)));
    }

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'apostilamentos/iframe') && Configure::read('App.config.resource.available.apostilamento') && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'apostilamentos/iframe') && Configure::read('App.config.resource.available.apostilamento') && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsApostilamentos', __('Apostilamentos', true), true, $this->Html->url(array('controller' => 'apostilamentos', 'action' => 'iframe', $id)));
    }

    if ((!$this->Modulo->isInscricao() && $this->Modulo->isPenalidade() && $contrato['nu_processo'] != "") || $this->Modulo->isPenalidade() &&
        $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsPenalidades', 'Penalidades', true, $this->Html->url(array('controller' => 'penalidades', 'action' => 'iframe', $id)));
    }

    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pendencias/iframe')) {
        $abas[] = array('fsPendencias', 'Pendências', true, $this->Html->url(array('controller' => 'pendencias', 'action' => 'iframe', $id)));
        $abaAtiva = count($abas) - 1;
    }

    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'historicos/iframe')) {
        $abas[] = array('fsHistoricos', __('Histórico', true), true, $this->Html->url(array('controller' => 'historicos', 'action' => 'iframe', $id)));
    }

    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'clausulascontratuais/iframe') && $this->Modulo->isClausulaContratual() && $contrato['nu_contrato'] != "") {
        $abas[] = array('fsClausulas', __('Cláusulas Contratuais', true), true, $this->Html->url(array('controller' => 'clausulas_contratuais', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isAndamento() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'andamentos/iframe')) {
        $abas[] = array('fsAndamentos', __('Andamento', true), true, $this->Html->url(array('controller' => 'andamentos', 'action' => 'iframe', $id)));
    }


    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/iframe')) {
        $abas[] = array('fsAnexos', __('Suporte Documental', true), true, $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isContato() && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsContatos', __('Contatos', true), true, $this->Html->url(array('controller' => 'contratos_contatos', 'action' => 'iframe', $id)));
    }

    if ($this->Sei->isEnabled()) {
        $abas[] = array('fsSeiProcessos', __('SEI-Processos', true), true, $this->Html->url(array('controller' => 'sei', 'action' => 'iframe_processos', $id)));
    }

    if (count($abas) > 0) {
        ?>

        <div id="accordion5" class="accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseAba" data-parent="#accordion5" data-toggle="collapse" class="accordion-toggle"
                       id="complemento">
                        Complemento
                    </a>
                </div>

                <div class="accordion-body in collapse" id="collapseAba" style="height: auto;">
                    <div class="accordion-inner">

                        <div class="row-fluid">

                            <div class="box-tabs-white">
                                <ul class="nav nav-tabs" id="myTab">
                                    <?php echo $aba->render($abas, $inativoall, $abaAtiva); ?>
                                </ul>
                                <div class="tab-content" id="myTabContent"></div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    $inativoall = true;
    $abaAtiva = null;

    $abas = array();

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pre_empenhos/iframe') &&
            $this->Modulo->isPreEmpenho() && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pre_empenhos/iframe') && $this->Modulo->isPreEmpenho() && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsPreEmpenhos', 'Pré-Empenhos', true, $this->Html->url(array('controller' => 'pre_empenhos', 'action' => 'iframe', $id)));
    }


    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/iframe') && $this->Modulo->isEmpenho() && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/iframe') && $this->Modulo->isEmpenho() && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsEmpenhos', __('Empenhos', true), true, $this->Html->url(array('controller' => 'empenhos', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isAbaComplemento('ordemDeFornecimento')) {
        if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'ordem_fornecimento_servico/iframe')) {
            $abas[] = array('fsGarantias', __('Ordem de Fornecimento/Serviço', true), true, $this->Html->url(array('controller' => 'ordem_fornecimento_servico', 'action' => 'iframe', $id)));
        }
    }

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'notas_fiscais/iframe') && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'notas_fiscais/iframe') && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsNotas', __('Notas / Atestos', true), true, $this->Html->url(array('controller' => 'notas_fiscais', 'action' => 'iframe', $id)));
    }

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'liquidacoes/iframe') && $this->Modulo->isLiquidacao() && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'liquidacoes/iframe') && $this->Modulo->isLiquidacao() && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsLiquidacoes', 'Liquidações', true, $this->Html->url(array('controller' => 'liquidacoes', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isPagamento() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pagamentos/iframe') && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsPagamentos', $this->Print->getLabelPagamentos($contrato['ic_tipo_contrato']), true, $this->Html->url(array('controller' => 'pagamentos', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isEntrega() && ($contrato['nu_processo'] != "" || $contrato['nu_contrato'] != "")) {
        $abas[] = array('fsEntregas', __('Entregas', true), true, $this->Html->url(array('controller' => 'entregas', 'action' => 'iframe', $id)));
    }

    if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'ordem_bancaria/iframe') && $this->Modulo->isOrdemBancaria() && $contrato['nu_processo'] != "") ||
        $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'ctr_emp_c') && $this->Modulo->isOrdemBancaria() && $contrato['nu_contrato'] != ""
    ) {
        $abas[] = array('fsOb', __('Ordens Bancárias', true), true, $this->Html->url(array('controller' => 'ordem_bancaria', 'action' => 'iframe', $id)));
    }

    if ($this->Modulo->isCronogramaFinanceiroDesembolso() && $contrato['nu_contrato'] != "") {
        if ((!$this->Modulo->isInscricao() && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'cronograma_financeiro_desembolso/iframe') && $contrato['nu_processo'] != "") ||
            $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'cronograma_financeiro_desembolso/iframe') && $this->Modulo->isOrdemBancaria() && $contrato['nu_contrato'] != ""
        ) {
            $abas[] = array('fsOb', 'Cronograma Financeiro/Desembolso', true, $this->Html->url(array('controller' => 'cronograma_financeiro_desembolso', 'action' => 'iframe', $id)));
        }
    }

    if ($this->Modulo->ata == true && ($contrato['nu_contrato'] != "" && $contrato['nu_processo'] != "")) {
        $abas[] = array('fsAta', 'Atas', true, $this->Html->url(array('controller' => 'atas', 'action' => 'index', $id)));
    };

    if (count($abas) > 0) {
        ?>
        <!-- financeiro  -->
        <div id="accordion4" class="accordion">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a href="#collapseAbaFin" data-parent="#accordion4" data-toggle="collapse" class="collapsed accordion-toggle"
                       id="financeiro">
                        Financeiro
                    </a>
                </div>

                <div class="accordion-body collapse" id="collapseAbaFin" style="height: 0px;">
                    <div class="accordion-inner">

                        <div class="row-fluid">


                            <div class="box-tabs-white">
                                <ul class="nav nav-tabs" id="myTabFin">
                                    <?php echo $aba->render($abas, $inativoall, $abaAtiva, 'carregarAbaFin'); ?>
                                </ul>
                                <div class="tab-content" id="myTabContentFin">

                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    ?>

    <!-- Cronograma de Execução -->
    <?php if ($this->Modulo->isExecucao() && !$this->Modulo->isInscricao() && $contrato['nu_contrato'] || $contrato['nu_processo']) { ?>
    <div id="accordion3" class="accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseExec" data-parent="#accordion3" data-toggle="collapse"
                   class="collapsed accordion-toggle" id="col_execucao" style="height:auto">
                    Cronograma de Execução
                </a>
            </div>
            <div class="accordion-body collapse" id="collapseExec">
                <!--<div style="" id="gifLoadAtv"></div>-->
                <table id="tabelaExecucao" class="table display table-hover table-condensed">
                    <div id="filtro" style="height:20px" class="padding">
                        <label for="selAtvs">
                            <select style="position:relative;top:0px;height:25px;margin-left:75px;width:120px" class=""
                                    id="selAtvs">
                                <option id="fazer" value="N">A fazer</option>
                                <option id="conc" value="F">Concluídas</option>
                                <option id="enc" value="C">Encerradas</option>
                                <option id="todas" value="T">Todas</option>
                            </select>
                        </label>
                        <div style="display:none" class="gifLoadAtv-small"></div>
                    </div>

                    <thead>
                    <tr sstyle="background-color: #f9f9f9;">
                        <th colspan="1"><?php __('Atividade'); ?></th>
                        <?php if ($this->Modulo->getAtividadesAvancado()) { ?>
                            <th>Periodicidade</th>
                        <?php } else {
                            echo '<th></th>';
                        } ?>
                        <th>Responsável</th>
                        <th colspan="1">Início Planejado</th>
                        <th colspan="1">Fim Planejado</th>
                        <th colspan="1">Início Executado</th>
                        <th colspan="1">Fim Executado</th>
                        <th>Término</th>
                        <th>%</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <div class="clearfix"></div>
                <div class="acoes-formulario-top clearfix">
                    <p class="requiredLegend pull-left">
                        Selecione uma Ação<!--span class="required" title="Required">*</span-->
                    </p>

                    <div class="pull-right btn-group">
                        <?php if (!$hasPenalidade) : ?>
                            <a href="#add-atividade" id="bt-add-atividade" data-toggle="modal"
                               title="Adicionar <?php __('Atividade'); ?>" class="btn btn-small btn-primary">
                                <i class="icon-plus icon-white"></i>
                                <?php __('Atividade'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>

    </div>
    <!-- modal impressão -->
    <div id="view_impressao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Configuração de Impressão</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="imp_contrato" value="<?php echo $contrato['co_contrato']; ?>">
            <?php echo $this->Form->checkbox('imp_basicas', array('checked' => 'checked')); ?> Informações Básicas<br>
            <?php if (Configure::read('App.config.resource.available.fiscal') && $contrato['nu_contrato'] != "") {
                echo $this->Form->checkbox('imp_fiscais', array('checked' => 'checked')) . ' ' . __('Fiscalização', true) . '<br>';
            } ?>
            <?php if ($this->Modulo->isAditivo() && $contrato['nu_contrato'] != "") {
                echo $this->Form->checkbox('imp_adtivos', array('checked' => 'checked')) . ' Aditivos<br>';
            } ?>
            <?php if ($this->Modulo->isGarantia() && $contrato['nu_contrato'] != "") {
                echo $this->Form->checkbox('imp_garantias', array('checked' => 'checked')) . ' Garantias<br>';
            } ?>
            <?php if ($this->Modulo->isEmpenho() && $contrato['nu_processo'] != "") {
                echo $this->Form->checkbox('imp_empenhos', array('checked' => 'checked')) . ' Empenhos<br>';
            } ?>
            <?php if ($this->Modulo->isPagamento() && $contrato['nu_processo'] != "") echo $this->Form->checkbox('imp_pagamentos', array('checked' => 'checked')); ?> <?php if ($this->Modulo->isPagamento() && $contrato['nu_processo'] != "") echo $this->Print->getLabelPagamentos($contrato['ic_tipo_contrato']) . '<br>'; ?>
            <?php echo $this->Form->checkbox('imp_historico', array('checked' => 'checked')); ?> <?php echo __('Histórico'); ?>
            <br>
            <?php echo $this->Form->checkbox('imp_pendencias', array('checked' => 'checked')); ?> Pendências<br>
            <?php if ($this->Modulo->isAndamento()) {
                echo $this->Form->checkbox('imp_andamento', array('checked' => 'checked')) . ' Andamento<br>';
            } ?>
            <?php if ($this->Modulo->isExecucao() && $contrato['nu_processo'] != "") {
                echo $this->Form->checkbox('imp_execucao', array('checked' => 'checked')) . ' Execução<br>';
            } ?>
            <?php if ($this->Modulo->isEmpenho() && $contrato['nu_processo'] != "") {
                echo $this->Form->checkbox('imp_apostilamento', array('checked' => 'checked')) . ' Apostilamentos<br>';
            } ?>
            <?php if ($contrato['nu_processo'] != "") echo $this->Form->checkbox('imp_penalidades', array('checked' => 'checked')) . ' Penalidades<br>'; ?>
            <?php if ($contrato['nu_processo'] != "") echo $this->Form->checkbox('imp_notas', array('checked' => 'checked')) . ' Notas Fiscais<br>'; ?>
            <!--      --><?php //if($contrato['nu_pam'] != "") { ?><!---->
            <?php //echo $this->Form->checkbox('imp_ass', array('checked' => 'checked')); ?><!-- Assinaturas<br>--><?php //} ?>
        </div>
        <div class="modal-footer">
            <a id="btImpContrato" href="#view_imp_contrato" class="btn btn-small btn-primary"><i class="icon-print"></i>
                Imprimir</a>
            <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
        </div>
    </div>

    <?php if ($contrato["co_fase"] > 0) { ?>
        <div id="mudar_fase" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="tituloAbaFase">Mudar de Fase</h3>
            </div>
            <div class="modal-body">
                <?php if ($alertaProximaFase != '') { ?>
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="icon-remove"></i>
                        </button>
                        <strong>Alerta: </strong>
                        <?php echo $alertaProximaFase; ?>
                    </div>
                <?php } ?>
                <form id="form_mudanca_fase" method="post"
                      action="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'mudar_fase')); ?>"
                      accept-charset="utf-8">
                    <?php
                    //echo $this->Form->input('tp_mudanca', array('label' => 'Encaminhar para', 'type' => 'radio', 'options' => $opcoesFluxo));
                    echo $this->Form->hidden('tp_mudanca', array('value' => 'P'));
                    echo $this->Form->hidden('co_contrato', array('value' => $contrato['co_contrato']));
                    echo $this->Form->hidden('nu_sequencia', array('value' => $this->data['Fluxo']['nu_sequencia']));
                    echo $this->Form->hidden('co_setor_atual', array('value' => $this->data['Contrato']['co_setor']));
                    echo $this->Form->hidden('co_fase_atual', array('value' => $this->data['Contrato']['co_fase']));
                    echo $this->Form->hidden('nu_sequencia_um', array('value' => $this->data['Fluxo']['nu_sequencia_um']));
                    echo $this->Form->hidden('nu_sequencia_dois', array('value' => $this->data['Fluxo']['nu_sequencia_dois']));
                    echo $this->Form->input('co_fase_mudar', array('label' => __('Andamento <font color="red">*</font>', true), 'type' => 'select', 'empty' => 'Selecione...', 'value'=>$this->data['Contrato']['co_fase']+1, 'options' => $fases));
//                    echo '<div id="comboDesviar" style="display: none;">';
//                    echo $this->Form->input('co_setor_mudar', array('label' => __('Setor <font color="red">*</font>', true), 'type' => 'select', 'empty' => 'Selecione...', 'options' => $setoresFluxo));
//                    echo $this->Form->input('co_fase_mudar', array('label' => __('Fase <font color="red">*</font>', true), 'type' => 'select', 'empty' => 'Selecione...'));
//                    echo $this->Form->input('jus_fase', array('class' => 'input-xxlarge', 'label' => 'Justificativa <font color="red">*</font>', 'type' => 'texarea', 'rows' => '2',
//                        'onKeyup' => '$(this).limit("255","#charsLeftJus")',
//                        'after' => '<br><span id="charsLeftJus"></span> caracteres restantes.'));
//                    echo '<br></div>';
                    echo $this->Form->input('obs_fase', array('class' => 'input-xxlarge', 'label' => 'Observações', 'type' => 'texarea', 'rows' => '2',
                        'onKeyup' => '$(this).limit("255","#charsLeftObs")',
                        'after' => '<br><span id="charsLeftObs"></span> caracteres restantes.'));

                    if (Configure::read('App.config.resource.certificadoDigital')) { ?>
                        <div class="input checkbox">
                            <input id="isCertificate" value="1" type="checkbox">
                            <label for="TramitarComCertificado">Tramitar com Certificado Digital</label>
                        </div>
                        <div id="useCertificate">
                            <?php echo $this->Form->hidden('token', array('value' => $token)); ?>

                            <div class="form-group">
                                <label for="certificateSelect">Escolha um Certificado</label>
                                <select id="certificateSelect" class="form-control"></select>
                                <?php echo $this->Form->input('tramitar_sem_certificado', array('type' => 'checkbox', 'label' => 'Tramitar sem Certificado Digital', 'id' => 'TramitarSemCertificado')); ?>
                            </div>
                        </div>
                    <?php } //        echo $this->element( 'applet_assinar_documento', array('rd2signer_documento0'=>'PAM: ' . $contrato['nu_pam'] . ', Fluxo Processual - ' . date('d/m/Y H:i:s'), 'formIdx'=>3) ); ?>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btEncaminharFase" type="button" title="Encaminhar"
                        class="btn btn-small btn-success mudarfase"><i class="icon-share-alt"></i> Encaminhar
                </button>
                <button data-dismiss="modal" title="Fechar" class="btn btn-small"> Fechar <i class="icon-remove"></i>
                </button>
            </div>
        </div>
    <?php } ?>

    <div id="view_imp_contrato" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <p class="pull-right">
                <a id="imprimir_pdf" href="#imprimir_pdf" class="btn btn-small btn-primary"><i class="icon-print"></i>
                    Imprimir</a>
                <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
            </p>
            <h3 id="tituloAbaPendencia">Impressão de <?php __('Contrato/Processo/Pam'); ?></h3>
        </div>
        <div class="modal-body maior" id="impressao">
        </div>
    </div>

    <div id="view_dominio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="tituloAbaDominio"></h3>
        </div>
        <div class="modal-body maior" id="list_dominio">
        </div>
    </div>


    <!-- ######### Modais de atividades ######## -->


    <!-- modal encerrar atividade -->
    <div style="top:12%" id="modalEncAtividade" class="modal hide fade">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Encerrar Atividade</h3>
        </div>

        <div class="modal-body">
            <p style="font-size:14px">Deseja encerrar a atividade ?</p>
            <p class="hasParent"></p>
        </div>

        <div class="modal-footer">
            <a href="#" id="ok-encerrar-atividade" class="btn btn-primary btn-small">Sim</a>
            <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Não</a>
        </div>
    </div>

    <!-- modal financeiro -->
    <div style="top:3%" id="view_financeiro" class="modal hide maior" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Orçamentário/Financeiro</h3>
        </div>
        <div class="modal-body" id="atv_financeiro">
        </div>
    </div>

    <!-- modal email -->
    <div style="top:3%" id="view_email" class="modal hide maior" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Informações do Envio de E-mail</h3>
        </div>
        <div class="modal-body" id="add_email">
        </div>
    </div>

    <!-- modal anexo -->
    <div style="top:3%" id="view_anexo" class="modal hide maior" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Suporte Documental</h3>
        </div>
        <div class="modal-body" id="atv_anexo">
        </div>
    </div>

    <!-- modal loader -->
    <div style="top:30%;z-index:9999" id="modalLoader" class="modal hide fade">
        <!---<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Alerta</h3>
        </div>-->

        <div class="modal-body">
            <h3><span id="gifLoadAtv"></span> Aguarde ...</h3>
        </div>

        <!--<div class="modal-footer">
            <a href="#" class="btn btn-default btn-small" data-dismiss="modal" >Fechar</a>
        </div>-->
    </div>

    <!-- modal detalhar atvidade -->
    <div style="width:760px;left:20%;margin:0 auto;top:4%" id="detalhar-atividade" class="modal hide" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 style="color: #010101" id="myModalLabel">Detalhes <?php __('Atividade'); ?></h2>
        </div>

        <div id="modalBodyDetalharEditar" class="modal-body">
            <div id="gifAtv"></div>

            <div style="display:none;" id="showDetalhes">
                <div class="row-fluid">

                    <div class="span6">
                        <h4>Responsável</h4>
                        <span class="center detalheAtvResponsavel" id=""><span>
                    </div>

                    <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="span6">
                            <h4>Função</h4>
                            <span class="detalheAtvFuncao" id=""></span>
                        </div>
                    <?php endif ?>
                </div>


                <div class="row-fluid">
                    <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="span12">
                            <h4>Nome da Atividade</h4>
                            <span class="detalheAtvNomeAtividade" id=""></span>
                        </div>
                    <?php endif ?>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <h4>Descrição da Atividade</h4>
                        <span class="detalheAtvDescricao" id=""></span>
                    </div>
                </div>

                <div class="row-fluid">
                    <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                        <div class="span4">
                            <h4>Periodicidade</h4>
                            <span class="" id="detalheAtvPeriodicidade"></span>
                        </div>
                    <?php endif ?>

                    <div class="span4">
                        <h4>Início da Atividade</h4>
                        <span class="text-center" id="detalheAtvIniAtividade"></span>
                    </div>

                    <div class="span4">
                        <h4>Término da Atividade</h4>
                        <span class="text-center" id="detalheAtvFimAtividade"></span>
                    </div>
                </div>

                <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                    <div clas="row-fluid">
                        <div class="span4">
                            <h4>Dia para execução</h4>
                            <span class="" class="text-center" id="detalheAtvDiaExecucao"></span>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div> <!-- modal body -->
        <div class="modal-footer">
            <button id="btnEditar" coAtividade="" class="btn btn-small btn-primary"><i class="icon-edit"></i>Editar
            </button>
            <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
        </div>
    </div>

    <!-- modal editar atividade -->
    <div style="width:760px;left:20%;margin:0 auto;top:4%" id="editar-atividade" class="modal hide" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 style="color: #010101" id="myModalLabel">Editar <?php __('Atividade'); ?></h2>
        </div>

        <div style="display:none" id="showEditar" class="modal-body">
            <div class="control-groups">
                <form id="formEditAtividade">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php
                            // if($this->Modulo->getHasNewFornecedores()) {
                            //     echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
                            // } ?>
                            <!-- vincular atividade -->
                            <label for="vincularAtv">Vincular a Atividade</label>
                            <select id="" class="editAtvVinc input-xxlarge">
                            </select>
                        </div>

                        <div class="control-row">
                            <!-- Responsável -->
                            <div class="required span6" id="id_co_responsavel">
                                <label for="editAtvResp">Nome do responsável</label>
                                <select id="editAtvResp" class="editAtvResp input-xlarge">
                                </select><br/>
                                <span class="messageErrorAtv message_co_responsavel_atividade"
                                      style="color: red;display:none"><b>Campo nome do responsável em branco </b>
                            </span>
                            </div>

                            <!-- Função -->
                            <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                                <div class="required span6" id="">
                                    <label>Função</label>
                                    <select class="select editAtvFunc input-medium">
                                        <option value="Gestor">Gestor</option>
                                        <option value="Fiscal Requisitante">Fiscal Requisitante</option>
                                        <option value="Fiscal Técnico">Fiscal Técnico</option>
                                        <option value="Fiscal Administrativo">Fiscal Administrativo</option>
                                        <option value="Aux. Fiscal Técnico">Aux. Fiscal Técnico</option>
                                        <option value="Aux. Fiscal Administrativo">Aux. Fiscal Administrativo</option>
                                    </select>
                                    <p><span class="messageErrorAtv message_funcaoResponsavel"
                                             style="color: red;display:none">
                                <b>Campo função em branco </b>
                            </span></p>
                                </div>
                            <?php endif; ?>
                        </div>

                        <!-- Nome da atividade -->
                        <?php
                        if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="required" id="">
                                <label for="ditNome">Nome: </label>
                                <input id="editNome" name="nome" class="input-xxlarge" value=""/>
                                <p><span class="messageErrorAtv message_nome_atividade" style="color: red;display:none">
                            <b>Campo nome da atividade em branco</b>
                        </span></p>
                            </div>
                        <?php endif; ?>

                        <!-- Descrição -->
                        <div class="required" id="id_ds_atividade">
                            <label>Descrição da Atividade</label>
                            <textarea id="editDesc" class="input-xlarge" rows="1" maxlength="250">
                        </textarea>
                            <span class="messageErrorAtv message_atividade" style="color: red;display:none"><b>Campo atividade deve possuir entre 3 e 100 caracteres!</b>
                        </span><br/>
                            <span class="messageErrorAtv message_atividade2" style="color: red;display:none"><b>Campo descrição em branco</b>
                        </span>
                        </div>

                        <div class="controls-row">
                            <!-- Periodicidade -->
                            <div class="required span5" id="id_periodicidade">
                                <label for="selectPeridiocidade">Periodicidade</label>
                                <select id="selectPeridiocidade" class="select editPeriodicidade input-xlarge"></select>
                            </div>

                            <div class="required span5" id="id_dia_execucao">
                                <label for="selectDiaExecucao">Dia para execução</label>
                                <select id="selectDiaExecucao" class="select editDiaExecucao input-xlarge"></select>
                            </div>
                        </div>

                        <div style="" class="controls-row">
                            <div class="required span5">
                                <div class="input text">
                                    <div class="input-append date datetimepicker"><label for="edit_dt_ini">Início da
                                            atividade</label><input name="dt_ini_planejado" type="text" id="edit_dt_ini"
                                                                    data-format="dd/MM/yyyy" mask="99/99/9999"
                                                                    class="edit_dt_ini input-small"><span
                                                class="add-on"><i data-date-icon="icon-calendar"
                                                                  class="icon-calendar"></i></span></div>
                                </div>
                            </div>

                            <!-- fim da atividade -->
                            <div class="span5">
                                <div class="input text">
                                    <div class="input-append date datetimepicker"><label for="edit_dt_fim">Término da
                                            atividade</label><input name="dt_fim_planejado" type="text" id="edit_dt_fim"
                                                                    data-format="dd/MM/yyyy" mask="99/99/9999"
                                                                    class="edit_dt_fim input-small"><span
                                                class="add-on"><i data-date-icon="icon-calendar"
                                                                  class="icon-calendar"></i></span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div> <!-- modal body -->
        <div class="modal-footer">
            <button id="editar-concluir" coAtividade="" class="btn btn-small btn-primary">Salvar</button>
            <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i>Fechar</button>
        </div>
    </div>

    <!-- modal add atividade -->
    <div style="margin-left:-328px;width:630px !important;top:0.5% !important;overflow: scroll;height: 100%;"
         id="add-atividade" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel">Adicionar <?php __('Atividade'); ?></h4>
        </div>

        <div style="overflow-y:hidden;max-height:600px" class="modal-body">
            <form id="formAddAtividade" class="form-horizontal">
                <div class="control-groups">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php
                            if ($this->Modulo->getHasNewFornecedores()) {
                                echo $this->Form->input('co_tipo_inspecao', array('label' => 'Tipo de Atividade / Inspeção', 'type' => 'select', 'options' => $inspecoes, 'empty' => 'Selecione'));
                            }

                            // vincular atividade
                            echo $this->Form->input('parent_id', array(
                                'id' => 'parent_id',
                                'label' => 'Vincular a ' . __('Atividade', true),
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'class' => 'parent_id input-xxlarge',
                                // 'options' => ''
                            ));
                            ?>
                        </div>

                        <!-- Responsável -->
                        <div class="controls-row">
                            <div class="required span4" id="id_co_responsavel">
                                <?php
                                echo $this->Form->input('co_responsavel', array(
                                    'id' => 'co_responsavel',
                                    'label' => 'Nome do Responsável',
                                    'class' => 'co_responsavel input-medium',
                                    'type' => 'select',
                                    'options' => '',
                                    'empty' => 'Selecione...'
                                )); ?>
                                <span class="messageErrorAtv message_co_responsavel_atividade"
                                      style="color: red;display:none"><b>Campo nome do responsável em branco </b>
                            </span>
                            </div>

                            <!-- Função -->
                            <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                                <div class="required span4" id="">
                                    <?php echo $this->Form->input('funcao_responsavel', array(
                                        'id' => 'funcao_responsavel',
                                        'label' => 'Função',
                                        'type' => 'select',
                                        'class' => 'funcao_responsavel input-medium',
                                        'options' => array(
                                            'Gestor' => 'Gestor',
                                            'Fiscal Requisitante' => 'Fiscal Requisitante',
                                            'Fiscal Técnico' => 'Fiscal Técnico',
                                            'Fiscal Administrativo' => 'Fiscal Administrativo',
                                            'Aux. Fiscal Técnico' => 'Aux. Fiscal Técnico',
                                            'Aux. Fiscal Administrativo' => 'Aux. Fiscal Administrativo'
                                        ),
//                                        'options' => '',
//                                        'readonly' => true,
                                        'empty' => 'Selecione...',
                                        'selected' => false
                                    ))
                                    ?>
                                    <span class="messageErrorAtv message_funcaoResponsavel"
                                          style="color: red;display:none">
                                <b>Campo função em branco </b>
                            </span>
                                </div>
                            <?php endif; ?>
                        </div>

                        <!-- Nome da atividade -->
                        <?php
                        if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="required" id=>
                                <?php echo $this->Form->input('nome', array(
                                    'id' => 'nomeAtividade',
                                    'label' => 'Nome da atividade',
                                    'type' => 'text',
                                    'class' => 'nomeAtividade input-xxlarge'
                                )) ?>
                                <span class="messageErrorAtv message_nome_atividade" style="color: red;display:none"><b>Campo nome da atividade em branco </b>
                        </span>
                            </div>
                        <?php endif; ?>

                        <!-- Descrição -->
                        <div class="required" id="id_ds_atividade">
                            <?php echo $this->Form->input('ds_atividade', array(
                                'id' => 'ds_atividade',
                                'label' => 'Descrição da Atividade',
                                'type' => 'textarea',
                                'rows' => 3,
                                'class' => 'ds_atividade input-xxlarge',
                                'maxlength' => 250
                            )); ?>
                            <span class="messageErrorAtv message_atividade" style="color: red;display:none"><b>Campo atividade deve possuir entre 3 e 100 caracteres!</b>
                        </span><br/>
                            <span class="messageErrorAtv message_atividade2" style="color: red;display:none"><b>Campo descrição em branco</b>
                        </span>
                        </div>

                        <!-- Inicio da atividade -->
                        <div style="" class="controls-row">
                            <div class="required span5">
                                <?php
                                echo $this->Form->input('dt_ini_planejado', array(
                                    'id' => 'dt_ini_planejado',
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'dt_ini_planejado input-small',
                                    'label' => 'Início da atividade',
                                    'type' => 'text'));
                                ?>

                                <span class="messageErrorAtv" id="message_dt_inicio_atividade"
                                      style="color: red;display:none"><b>Campo inicio da atividade em branco</b>
                            </span><br/>

                                <span class="messageErrorAtv" id="message_dt_inicio_atividade2"
                                      style="color: red;display:none"><b>A data do inicio da atividade é maior que a data do fim do contrato</b>
                            </span>
                            </div>

                            <!-- fim da atividade -->
                            <div class="span5" id="_id_dt_fim_planejado">
                                <?php
                                echo $this->Form->input('dt_fim_planejado', array(
                                    'id' => 'dt_fim_planejado',
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'dt_fim_planejado input-small',
                                    'label' => 'Término da atividade',
                                    'type' => 'text'));
                                ?>
                                <span class="messageErrorAtv" id="message_dt_fim_atividade0"
                                      style="color: red;display:none"><b>Campo fim da atividade em branco</b></span>
                                <span class="messageErrorAtv" id="message_dt_fim_atividade"
                                      style="color: red;display:none"><b>A data do término da atividade é maior que a data do fim do contrato</b><br/></span>
                                <span class="messageErrorAtv" id="message_dt_fim_atividade2"
                                      style="color: red;display:none"><b>A data do término da atividade é menor que a data do início da atividade</b></span>
                            </div>
                        </div>

                        <!-- Periodicidade -->
                        <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="control-rows">
                                <div class="required span5" id="">
                                    <?php
                                    echo $this->Form->input('periodicidade', array(
                                        'type' => 'select',
                                        'id' => 'periodicidade',
                                        'class' => 'input-large',
                                        'label' => 'Periodicidade',
                                        'empty' => 'Selecione...',
                                        'selected' => false,
                                        'options' => array(
                                            'Diariamente' => 'Diariamente',
                                            'Semanal' => 'Semanal',
                                            'Quinzenal' => 'Quinzenal',
                                            'Mensal' => 'Mensal',
                                            'Bimestral' => 'Bimestral',
                                            'Trimestral' => 'Trimestral',
                                            'Semestral' => 'Semestral',
                                            'Anual' => 'Anual',
                                            'Quando houver necessidade' => 'Quando houver necessidade',
                                            'Na solicitação' => 'Na solicitação'
                                        )
                                    ));
                                    ?>
                                    <span class="messageErrorAtv" id="message_periodicidade_atividade"
                                          style="color: red;display:none"><b>Campo periodicidade em branco</b></span>
                                </div>

                                <!-- Dia para execução -->
                                <div class="required span6" style="">
                                    <?php
                                    echo $this->Form->input('dia_execucao', array(
                                        'type' => 'select',
                                        'id' => 'dia_execucao',
                                        'class' => 'input-large',
                                        'label' => 'Dia para execução',
                                        'empty' => 'Selecione...',
                                        'options' => array(
                                            'Segunda' => 'Segunda',
                                            'Terça' => 'Terça',
                                            'Quarta' => 'Quarta',
                                            'Quinta' => 'Quinta',
                                            'Sexta' => 'Sexta',
                                            'Quando Possível' => 'Quando Possível'
                                        )
                                    ));
                                    ?>

                                    <span class="messageErrorAtv" id="message_dia_execucao_atividade"
                                          style="color: red;display:none"><b>Campo dia para execução em branco</b></span>
                                </div>

                            </div>
                        <?php endif ?>

                        <?php
                        if ($this->Modulo->getHasNewFornecedores()) {
                            echo '<div class="span6">';
                            echo $this->Form->input('ds_assunto_email', array('label' => 'Assunto (Para envio de email)'));
                            echo '</div>';
                        }
                        ?>

                    </div>
                </div>
            </form>
        </div> <!-- modal body -->

        <div class="modal-footer">
            <div style="display:none" class="gifLoadAtv-modal"></div>
            <button class="btn btn-small btn-primary" id="add_concluir"><i class="icon-ok icon-white"></i> Concluir
            </button>
            <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
        </div>
    </div>

    <!-- modal editar porcentagem -->
    <div style="width:580px" class="modal fade hide" id="editar-porcentagem" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Atividade</h4>
                </div>

                <div class="modal-body">
                    <div class="row-fuid">
                        <label for="atvPorcentagem">Porcentagem</label>
                        <div class="">
                            <select class="input-small" id="atvPorcentagem">
                                <option value="0">0%</option>
                                <option value="5">5%</option>
                                <option value="10">10%</option>
                                <option value="15">15%</option>
                                <option value="20">20%</option>
                                <option value="25">25%</option>
                                <option value="30">30%</option>
                                <option value="35">35%</option>
                                <option value="40">40%</option>
                                <option value="45">45%</option>
                                <option value="50">50%</option>
                                <option value="55">55%</option>
                                <option value="60">60%</option>
                                <option value="65">65%</option>
                                <option value="70">70%</option>
                                <option value="75">75%</option>
                                <option value="80">80%</option>
                                <option value="85">85%</option>
                                <option value="90">90%</option>
                                <option value="95">95%</option>
                                <option value="100">100%</option>
                            </select>
                        </div>

                        <?php if ($this->Modulo->getAtividadesAvancado()) : ?>
                            <div class="required">
                            <?php echo $this->Form->input('pendencia', array(
                                'id' => 'atvPendencia',
                                'label' => 'Pendência',
                                'type' => 'textarea',
                                'rows' => 3,
                                'class' => 'input-xxlarge',
                                'maxlength' => 250
                            )); ?>
                            <span id="message_pendencia_required"
                                  style="color: red;display:none"><b>Pendência em branco</b>
                            </span>
                            </div><?php endif ?>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-small" data-dismiss="modal">Fechar</button>
                    <a id="ok-editar-porcentagem" class="btn btn-danger btn-ok btn-small">Salvar</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal iniciar atividade -->
<div style="top:12%" id="modalIniAtividade" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Iniciar Atividade</h3>
    </div>

    <div class="modal-body">
        <p style="font-size:14px">Deseja iniciar a atividade ?</p>
    </div>

    <div class="modal-footer">
        <a href="#" id="ok-ini-atividade" class="btn btn-primary btn-small">Sim</a>
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Não</a>
    </div>
</div>

<!-- modal deletar atividade -->
<div style="top:12%" id="modalDelAtividade" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Deletar Atividade</h3>
    </div>

    <div class="modal-body">
        <p style="font-size:14px">Deseja deletar a atividade ?</p>
        <p style="color: red;" class="parentMsg">Esta atividade possui vínculos.</p>
    </div>

    <div class="modal-footer">
        <a href="#" id="ok-del-atividade" class="btn btn-primary btn-small">Sim</a>
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Não</a>
    </div>
</div>

<!-- modal alert periodicidade -->
<div style="top:15%" id="modalAlertPeriodicidade" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Alerta</h3>
    </div>

    <div class="modal-body">
        <p>A periodicidade é maior que a data prazo da atividade</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Fechar</a>
    </div>
</div>

<!-- modal alert inicio da atividade -->
<div style="top:15%" id="modalAlertDataInicio" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Alerta</h3>
    </div>

    <div class="modal-body">
        <p>A data de início da atividade é inferior a data de hoje</p>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn btn-default btn-small" data-dismiss="modal">Fechar</a>
    </div>
</div>

<!-- modal alerta inicio atividade -->
<div style="top:3%" id="modalAlertaIniAtividade" class="modal hide" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Alerta</h3>
    </div>
    <div class="modal-body">
        <p style="font-size:14px">É necessário iniciar a atividade primeiro.</p>
    </div>
</div>

<!-- ############ Fim dos modais de atividades ########## -->

<script>
    <?php
    if( !($cadContrato != 'edi_pamt' && $cadContrato != 'edit' && $cadContrato != 'detalha') ) {
    ?>
    if ($('#tituloAbaDominio', parent.document).html() == 'Editar') {
        $('#tituloAbaDominio', parent.document).html('Detalhar');
    }
    <?php
    }
    ?>

    $('#isCertificate').click(function () {
        if (this.checked) {
            $("#useCertificate").show();
            initPki();
        } else {
            $("#useCertificate").hide();
        }
    });

    $('#encaminharFase').click(function () {
        $('#tituloAbaFase').html("");
        $('#tituloAbaFase').append("Mudança de Andamento");
        $('#mudar_fase').modal();
        $('.modal-body').css({height: "400px", overflow: "auto"});
    });

    $('#TpMudancaC').click(function () {
        $('#comboDesviar').hide();
    });
    $('#TpMudancaR').click(function () {
        $('#comboDesviar').hide();
    });
    $('#TpMudancaD').click(function () {
        $('#comboDesviar').show();
    });

    $('#co_setor_mudar').change(function () {

        $.getJSON("<?php echo $this->Html->url(array('controller' => 'fases', 'action' => 'listar_fases_fluxo'))?>" + "/" + $(this).val(), null, function (data) {
            var options = '<option value="">Selecione...</option>';
            $.each(data, function (index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#co_fase_mudar").html(options);
        })

    });


    // get impressão
    $('#btImpContrato').click(function () {
        $('#view_impressao').modal('hide');
        $('#view_imp_contrato').modal();
        var url_imp = "../iframe_imprimir/" + $('#imp_contrato').val() + "/" + $('#imp_basicas:checked').length + $('#imp_fiscais:checked').length + $('#imp_adtivos:checked').length + $('#imp_garantias:checked').length + $('#imp_empenhos:checked').length + $('#imp_pagamentos:checked').length + $('#imp_historico:checked').length + $('#imp_pendencias:checked').length + $('#imp_andamento:checked').length + $('#imp_execucao:checked').length + $('#imp_apostilamento:checked').length + $('#imp_notas:checked').length + $('#imp_ass:checked').length + '/<?php echo $nomeArquivo; ?>';
        $.ajax({
            type: "POST",
            url: url_imp,
            data: {},
            success: function (result) {
                $('#impressao').html("");
                $('#impressao').append(result);
            }
        })
    })

    $('#imprimir_pdf').click(function () {
        window.frames["iframe_imp_contrato"].focus();
        window.frames["iframe_imp_contrato"].print();
    })

    function carregarFiscais() {
        <?php echo $this->JqueryEngine->request("/contratos_fiscais/iframe/$id", array('update' => '#fsFiscais')); ?>
    }
    function carregarAditivos() {
        <?php echo $this->JqueryEngine->request("/aditivos/iframe/$id", array('update' => '#fsAditivos')); ?>
    }
    function carregarGarantias() {
        <?php echo $this->JqueryEngine->request("/garantias/iframe/$id", array('update' => '#fsGarantias')); ?>
    }
    function carregarGarantiasSuporte() {
        <?php echo $this->JqueryEngine->request("/garantias_suporte/iframe/$id", array('update' => '#fsGarantiasSuporte')); ?>
    }
    function carregarProdutos() {
        <?php echo $this->JqueryEngine->request("/produtos/iframe/$id", array('update' => '#fsProdutos')); ?>
    }
    function carregarContas() {
        <?php echo $this->JqueryEngine->request("/contas/iframe/$id", array('update' => '#fsContas')); ?>
    }
    function carregarOficios() {
        <?php echo $this->JqueryEngine->request("/oficios/iframe/$id", array('update' => '#fsOficios')); ?>
    }
    <?php if( $this->Modulo->isEmpenho() ) { ?>
    function carregarEmpenhos() {
        <?php echo $this->JqueryEngine->request("/empenhos/iframe/$id", array('update' => '#fsEmpenhos')); ?>
    }
    <?php } ?>
    function carregarLiquidacoes() {
        <?php echo $this->JqueryEngine->request("/liquidacoes/iframe/$id", array('update' => '#fsLiquidacoes')); ?>
    }
    function carregarPenalidades() {
        <?php echo $this->JqueryEngine->request("/penalidades/iframe/$id", array('update' => '#fsPenalidades')); ?>
    }
    function carregarNotas() {
        <?php echo $this->JqueryEngine->request("/notas/iframe/$id", array('update' => '#fsNotas')); ?>
    }
    function carregarPagamentos() {
        <?php echo $this->JqueryEngine->request("/pagamentos/iframe/$id", array('update' => '#fsPagamentos')); ?>
    }
    function carregarEntregas() {
        <?php echo $this->JqueryEngine->request("/entregas/iframe/$id", array('update' => '#fsEntregas')); ?>
    }
    function carregarPendencias() {
        <?php echo $this->JqueryEngine->request("/pendencias/iframe/$id", array('update' => '#fsPendencias')); ?>
    }
    function carregarHistoricos() {
        <?php echo $this->JqueryEngine->request("/historicos/iframe/$id", array('update' => '#fsHistoricos')); ?>
    }
    function carregarClausulas() {
        <?php echo $this->JqueryEngine->request("/clausulas_contratuais/iframe/$id", array('update' => '#fsClausulas')); ?>
    }
    function carregarAvaliacoes() {
        <?php echo $this->JqueryEngine->request("/historicos/iframe/$id", array('update' => '#fsAvaliacao')); ?>
    }
    function carregarAnexos() {
        <?php echo $this->JqueryEngine->request("/anexos/iframe/$id", array('update' => '#fsAnexos')); ?>
    }
    function carregarAndamentos() {
        <?php echo $this->JqueryEngine->request("/andamentos/index/$id", array('update' => '#fsAndamentos')); ?>
    }

    function carregarApostilamentos() {
        <?php echo $this->JqueryEngine->request("/apostilamentos/iframe/$id", array('update' => '#fsApostilamentos')); ?>
    }

    function atualizaTotalizaValores() {
        <?php echo $this->JqueryEngine->request("/contratos/valores/$id", array('update' => '#totalizaValores')); ?>
    }

    function atualizaTotalizaPercentuais() {
        <?php echo $this->JqueryEngine->request("/contratos/percentuais/$id", array('update' => '#totalizaValores2')); ?>
    }

    function atualizaExecucao() {
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller' => 'atividades', 'action' => 'index')); ?>/' + $('#idContrato').val() + '/N',
            method: 'GET',
            cache: false,
            success: function (data) {
                $('#collapseExec').html(data);
            }
        });
    }

    $(document).ready(function () {
        setTimeout(function () {
            $('.alert-block').hide('slow');
        }, 5000);
        atualizaTotalizaValores();
        $("#myTab > li > a").first(function () {
            $(this).click();
        });
    });
    function carregarAba(element, aba) {
        $("#myTab > li").each(function () {
            $(this).removeClass('active');
        });
        element.parent().addClass('active');
        $("#myTabContent").load(element.attr('req'));
    }
    function carregarAbaFin(element, aba) {
        $("#myTabFin > li").each(function () {
            $(this).removeClass('active');
        });
        element.parent().addClass('active');
        $("#myTabContentFin").load(element.attr('req'));
    }

    $().ready(function () {
        atualizaTotalizaPercentuais();
        <?php if ($this->Modulo->isAditivo()) {
        echo "carregarAditivos();";
    } ?>
        <?php if (Configure::read('App.config.resource.available.fiscal')) {
        echo "carregarFiscais();";
    } ?>
        <?php if ($this->Modulo->isGarantia()) {
        echo "carregarGarantias();";
    } ?>
        <?php if ($this->Modulo->isGarantiaSuporte()) {
        echo "carregarGarantiasSuporte();";
    } ?>
        <?php if ($this->Modulo->isEmpenho()) {
        echo "carregarEmpenhos();";
    } ?>
        <?php if ($this->Modulo->isPenalidade()) {
        echo "carregarPenalidades();";
    } ?>
        carregarNotas();
        <?php if ($this->Modulo->isPagamento()) {
        echo "carregarPagamentos();";
    } ?>
        <?php if ($this->Modulo->isEntrega()) {
        echo "carregarEntregas();";
    } ?>
        carregarPendencias();
        carregarHistoricos();
        <?php if ($this->Modulo->isClausulaContratual()) {
        echo "carregarClausulas();";
    } ?>
        carregarAvaliacoes();
        carregarAnexos();
        carregarAndamentos();
        carregarApostilamentos();
    });

    function abrirAnexo(coAnexo) {
        var url_abrir = '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'lerArquivo')); ?>/' + coAnexo + "/1";
        $('#view_dominio').modal();
        $('#list_dominio').html("");
        $('#tituloAbaDominio').html('Abrir Documento');
        $.ajax({
            type: "POST",
            url: '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe_abrir', 510)); ?>',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data: {
                "data[url]": url_abrir
            },
            success: function (result) {
                $('#list_dominio').html("Caso o arquivo não abra nesta Janela, verifique a área de Downloads do seu Navegador.");
                $('#list_dominio').append(result);
            }
        })
    }

    function carregarAtvConcluidas(aba) {
        <?php echo $this->JqueryEngine->request(
        $this->Html->url(array('controller' => 'atividades', 'action' => 'index')) . "/$id/concluidas",
        array('update' => '#atvConcluidas')
    ); ?>

        aba.tab('show');
    }

    $('#myTab a').click(function (e) {
        e.preventDefault();

        if ($(this).attr('href') == '#atvConcluidas') {

            if (!$(this).hasClass('loaded')) {
                carregarAtvConcluidas($(this));
                $(this).addClass('loaded');

            } else {
                $(this).tab('show');
            }
            return;
        }
        $(this).tab('show');
    });


</script>
