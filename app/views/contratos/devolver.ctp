<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="contratos form">
	
	<!-- fieldset id="fsContrato" class="ativo"-->
        
		<fieldset>
		<?php echo $this->Form->create('Contrato');?>
			<legend>
                        <?php 
                            if($contrato['nu_contrato'] == "") { 
                                __('Processo');
                            } else {
                                __('Contrato: '); echo $this->Print->contrato($contrato['nu_contrato']); 
                            }
                        ?></legend>
                        
                        <table>
                            <tr>
                                <td width="80">Processo: </td>
                                <td width="270"><?php echo $this->Print->processo($contrato['nu_processo']); ?></td>
                                <td><?php if($contrato['nu_contrato'] != "") { __('Fornecedor').":"; } ?></td>
                                <td><?php if($contrato['co_fornecedor'] > 0 && $contrato['nu_contrato'] != "") { echo $fornecedores[$contrato['co_fornecedor']]; } ?></td>
                            </tr>
                            <tr>
                                <td>Unidade Solicitante: </td>
                                <td><?php echo $contratantes[$contrato['co_contratante']]; ?></td>
                                <td>Objeto: </td>
                                <td><?php echo $contrato['ds_objeto']; ?></td>
                            </tr>
                            <tr>
                                <td width="80">Vigência:</td>
                                <td><?php echo $contrato['dt_ini_vigencia']; ?> &nbsp;&nbsp;à&nbsp;&nbsp; <?php echo $contrato['dt_fim_vigencia']; ?></td>
                                <td>Expira em:</td>
                                <td>
                                    <?php 
                                    $diasExpira = $this->Print->expira($contrato['dt_fim_vigencia']);
                                    if ($diasExpira > 0 ) {
                                        echo $diasExpira . ' dias';
                                    } else {
                                        if( !empty( $contrato['dt_ini_vigencia'] ) && !empty( $contrato['dt_fim_vigencia'] ) ) {
                                            echo "<font color=\"#FF0000\">Finalizado</font>";
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="80">Tipo de <?php __('Contrato'); ?>:</td>
                                <td><?php if ( !empty( $contrato['co_modalidade'] ) ) { echo $modalidades[$contrato['co_modalidade']]; } ?></td>
                                <td>Andamento Processual:</td>
                                <td><?php echo $situacaos[$contrato['co_situacao']]; ?>
                                </td>
                            </tr>
                        </table>
            <?php 
                echo $this->Form->input('ds_justificativa', array('label'=>'Justificativa para Devolução', 'type'=>'textarea', 'cols'=>'70', 'rows'=>'4'));
                echo $this->Form->end(__('Devolver', true));
            ?>
	</fieldset>
	
</div>


<div class="actions">
	<h3><?php __('Ações'); ?></h3>
	<ul>
            <li><?php echo $this->Html->link(__('Voltar', true), array('action' => 'index'));?></li>
	</ul>
</div>