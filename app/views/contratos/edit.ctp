<?php
echo $this->Html->script('inicia-datetimepicker');
echo $this->Html->script('date');

$url = "/contratos/edit/$id";
if ($cadContrato != "") {
    $url .= "/$cadContrato";
}
?>
<?php echo $this->Form->create('Contrato', array('url' => $url)); ?>

<div class="row-fluid">
    <?php
    if ($cadContrato != 'edit' && $cadContrato != 'detalha') {
        ?>
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha os campos
                abaixo para editar.</p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'index')); ?>"
                   class="btn btn-small btn-primary" title="Listagem">Listagem</a>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="row-fluid">

        <div class="span8 ">
            <div class="widget-box">
                <div class="widget-header widget-header-small"><h4>
                        <?php
                        if ($this->Modulo->isPam() && $contrato['nu_processo'] == "" && $contrato['nu_contrato'] == "" && $cadContrato != "np") {
                            __('PAM/s');
                        } else if ($contrato['nu_contrato'] == "" && $cadContrato != "nc") {
                            __('Processo');
                        } else {
                            __('Contrato');
                            if ($cadContrato != "nc") {
                                echo ': ' . $this->Print->contrato($contrato['nu_contrato']);
                            }
                        }
                        ?>
                    </h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row-fluid">
                                <div class="span6">
                                    <dl>
                                        <?php
                                        echo $this->Form->hidden('co_contrato');
                                        echo $this->Form->hidden('co_instituicao');
                                        if ($temPai) {
                                            echo $this->Form->hidden('co_contrato_pai', array('value' => $idContratoPai));
                                        }

                                        echo $this->Form->hidden('co_contrato');
                                        if ($this->Modulo->isContratoExterno()) {
                                            $checkedI = 'checked="checked"';
                                            $checkedE = '';
                                            if (isset($this->data['Contrato']['ic_tipo_contrato']) && $this->data['Contrato']['ic_tipo_contrato'] == 'E') {
                                                $checkedE = 'checked="checked"';
                                                $checkedI = '';
                                            }
                                            ?>
                                            Tipo de Visão <br>
                                            <input name="data[Contrato][ic_tipo_contrato]" type="radio"
                                                   id="ContratoIcTipoContratoI"
                                                   value="I" <?php echo $checkedI; ?>/> Interno &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="data[Contrato][ic_tipo_contrato]" type="radio"
                                                   id="ContratoIcTipoContratoE" value="E" <?php echo $checkedE; ?>/> Externo
                                            <br><br>
                                            <?php
                                        }
                                        if ($this->Modulo->isProcesso()) {
                                            echo $this->Form->input('nu_processo', array('class' => 'input-xlarge', 'label' => 'Nº Processo', 'onblur' => 'completeComZeros()'));
                                        }
                                        if (!$this->Modulo->isAutoNumeracao()) {
                                            echo $this->Form->input('nu_contrato', array('class' => 'input-xlarge', 'label' => 'Nº ' . __('Contrato', true), 'mask' => '9999/9999'));//FunctionsComponent::pegarFormato('contrato')
                                        }

                                        echo $this->Form->input('ds_tipo_servico', array(
                                            'class' => 'input-xlarge',
                                            'label' => 'Tipo do Serviço',
                                            'id' => 'tipoServico',
                                            'type' => 'select',
                                            'options' => array (
                                              'Diagnóstico' => 'Diagnóstico',
                                              'Redução Mensal' => 'Redução Mensal',
                                              'Ressarcimento' => 'Ressarcimento',
                                              'Todos' => 'Todos'
                                            )
                                        ));

                                        if ($this->Modulo->isCamposContrato('ds_contratacao') == true) :
                                            echo $this->Form->input('co_contratacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Modalidade de Contratação', true), 'options' => $contratacoes,
                                                'after' => $this->Print->getBtnEditCombo('Nova ' . __('Modalidade de Contratação', true), 'AbrirNovaModalidade', '#view_nova_modalidade', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratacoes/index'))));
                                        endif;

                                        if ($this->Modulo->isCamposContrato('nu_licitacao') == true) :
                                            echo $this->Form->input('nu_licitacao', array('class' => 'input-xlarge', 'label' => 'Nº Licitação', 'mask' => FunctionsComponent::pegarFormato('licitacao')));
                                        endif;

                                        if ($this->Modulo->isCamposContrato('dt_publicacao') == true) :
                                            echo $this->Form->input('dt_publicacao', array(
                                                'before' => '<div class="input-append date datetimepicker">',
                                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                                'class' => 'input-small', 'label' => 'Data de Publicação', 'type' => 'text'));
                                        endif;

                                        if ($this->Modulo->isCamposContrato('dt_tais')) {
                                            echo $this->Form->input('dt_tais', array(
                                                'before' => '<div class="input-append date datetimepicker">',
                                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                                'class' => 'input-small', 'label' => 'Data do TAIS', 'type' => 'text'));
                                        }
                                        /*echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'options' => $fornecedores,
                                            'after' => $this->Print->getBtnEditCombo('Novo ' . $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'AbrirNovoFornecedor', '#view_novo_fornecedor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/add'))));*/
                                            echo $this->Form->input('co_cliente', array(
                                                                        'class' => 'input-xlarge chosen-select',
                                                                        'type' => 'select',
                                                                        'empty' => 'Selecione...',
                                                                        'label' => 'Cliente',
                                                                        'options' => $clientes,
                                                                      ));
                                            
//                                            echo $this->Form->input('Cliente', array(
//                                                                        'class' => 'input-xlarge chosen-select',
//                                                                        'type' => 'select',
//                                                                        'empty' => 'Selecione...',
//                                                                        'label' => 'Clientes',
//                                                                        'options' => $clientes,
//                                                                        'selected' => $html->value('Contrato.Cliente'),
//                                                                        'multiple' => true
//                                                                      ));
                                        ?>
                                        <br/>
                                        <?php
                                        echo $this->Form->input('co_parceiro', array('label' => 'Parceiro', 'class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $usuarios,
                                            'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Adicionar ao Grupo Auxiliar" id="AbrirNovoAuxiliar" href="#view_novo_auxiliar" data-toggle="modal"><i class="blue icon-edit bigger-150"></i></a>'));

                                        /*echo $this->Form->input('ds_recurso_orcamentario', array('class' => 'input-xlarge', 'label' => 'Recurso Orçamentário', 'type' => 'texarea', 'cols' => '30', 'rows' => '3',
                                            'onKeyup' => '$(this).limit("1500","#charsLeft2");',
                                            'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'));*/
                                        ?>
                                    </dl>

                                    <div id="dialog-processo" class="hide">
                                        <div class="alert alert-info bigger-110">
                                            Já existe um Processo cadastrado com o número informado.<br>
                                            Deseja vincular este Processo ao <?php __('Contrato'); ?>?
                                        </div>
                                    </div>

                                    <div id="dialog-processo-modalidade" class="hide">
                                        <div class="alert alert-info bigger-110">
                                            Já existe um processo com esse número e modalidade
                                        </div>
                                    </div>

                                </div>
                                <div class="span6">
                                    <dl class="dl-horizontal">


                                        <?php
                                        echo $this->Form->input('ds_objeto', array('class' => 'input-xlarge', 'label' => 'Objeto', 'type' => 'textarea', 'rows' => '4',
                                            'onKeyup' => '$(this).limit("1500","#charsLeft")',
                                            'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                                        echo '<br />';
                                        if ($this->Modulo->isCamposContrato('ds_fundamento_legal') == true) {
                                            echo $this->Form->input('ds_fundamento_legal', array('class' => 'input-xlarge', 'label' => 'Fundamento Legal', 'type' => 'textarea', 'rows' => '4', 'cols' => '42',
                                                'onKeyup' => '$(this).limit("500","#charsLeft1")',
                                                'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'));
                                            echo '<br />';
                                        }
                                        echo $this->Form->input('ds_observacao', array('class' => 'input-xlarge', 'label' => 'Observação', 'type' => 'texarea', 'cols' => '42', 'rows' => '4',
                                            'onKeyup' => '$(this).limit("1500","#charsLeft3")',
                                            'after' => '<br><span id="charsLeft3"></span> caracteres restantes.'));
                                        ?>

                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Detalhamento'); ?></h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row-fluid">

                                <div class="span6">
                                    <dl class="dl-horizontal">
                                        <?php
                                        if ($this->Modulo->isCamposContrato('co_contratante') == true) {
                                            echo $this->Form->input('co_contratante', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Unidade Solicitante', true), 'options' => $imprimir->getArraySetores($contratantes), 'escape' => false));
                                        }
                                        if ($this->Modulo->isCamposContrato('co_executante') == true) {
                                            echo $this->Form->input('co_executante', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Unidade Executora', true), 'options' => $imprimir->getArraySetores($contratantes), 'escape' => false));
                                        }
                                        $selectedGestor = 0;
                                        if ($usuario['UsuarioPerfil']['co_perfil'] == 3) {
                                            $selectedGestor = $usuario['Usuario']['co_usuario'];
                                        }
                                        echo $this->Form->input('co_gestor_atual', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Responsável Reducall', true), 'options' => $gestores, 'selected' => $selectedGestor, 'onchange' => 'validaGestorSuplente(this)'));

                                        echo $this->Form->input('co_gestor_suplente', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Responsável Araras', true), 'options' => $fiscais, 'selected' => $selectedGestor, 'id' => 'gestorSuplente'));

                                        if ($this->Modulo->isCamposContrato('co_modalidade') == true) {
      //                                    echo $this->Form->input('co_modalidade', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label'=>'Tipo de ' . __('Contrato', true), 'options' => $modalidades,
      //                                        'after' => $this->Print->getBtnEditCombo('Novo ' .  __('Tipo de Contrato', true), 'AbrirNovoTpContrato', '#view_novo_tp_contrato', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratacoes/index')) ) );

                                            echo $this->Form->input('co_modalidade', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Tipo', 'options' => $modalidades,
                                                'after' => $this->Print->getBtnEditCombo('Novo ' . __('Tipo de Contrato', true), 'AbrirNovoTpContrato', '#view_novo_tp_contrato', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratacoes/index'))));
                                        }
                                        if ($this->Modulo->isCamposContrato('co_servico') == true) {
                                            echo $this->Form->input('co_servico', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Descrição do Serviço', true), 'options' => $servicos,
                                                'after' => $this->Print->getBtnEditCombo('Nova ' . __('Descrição do Serviço', true), 'AbrirNovoServico', '#view_novo_servico', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratacoes/index'))));
                                        }
                                        ?>
                                    </dl>
                                </div>
                                <div class="span6">
                                    <dl class="dl-horizontal">
                                        <?php
                                        if ($this->Modulo->isCamposContrato('co_situacao')) {
                                            echo $this->Form->input('co_situacao', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Situação do Contrato', true), 'options' => $situacaos,
                                                'after' => $this->Print->getBtnEditCombo('Nova ' . __('Situação do Contrato', true), 'AbrirNovaSituacao', '#view_nova_situacao', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'situacoes/index'))));
                                        }

                                        // echo '<div class="">&nbsp;</div>';
                                      //  echo $this->Form->input('ic_cadastro_garantia', array(
                                        //    'id' => 'icGarantia',
                                        //    'class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Exigir cadastro de garantia ?', 'options' => array(1 => 'Sim', 0 => 'Não'), 'escape' => false));;

                                        // porcentagem da garantia
                                        echo $this->Form->input('pc_garantia', array(
                                            'id' => 'pctGarantia',
                                            'style' => '',
                                            'class' => 'input-mini',
                                            'maxlength' => '3',
                                            'type' => 'text', 'empty' => 'Selecione...', 'label' => 'Porcentagem da garantia ', 'escape' => false));;


                                        if ($this->Modulo->isCamposContrato('co_categoria') == true) {
                                            echo $this->Form->input('co_categoria', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Categoria', true), 'options' => $categorias,
                                                'after' => $this->Print->getBtnEditCombo('Nova ' . __('Categoria', true), 'AbrirNovaCategoria', '#view_nova_categoria', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'categorias/index'))));
                                        }
                                        if ($this->Modulo->isCamposContrato('co_subcategoria') == true) {
                                            echo $this->Form->input('co_subcategoria', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Sub Categoria', true),
                                                'after' => $this->Print->getBtnEditCombo('Nova ' . __('Sub Categoria', true), 'AbrirNovaSubCategoria', '#view_nova_sub_categoria', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'subcategorias/index'))));
                                        }
                                        if ($this->Modulo->isCamposContrato('st_repactuado')) {
                                            echo $this->Form->input('st_repactuado', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Prorrogável', true), 'options' => array('S' => 'Sim', 'N' => 'Não')));
                                        }
                                        ?>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Recursos Financeiros'); ?></h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <?php
                            echo $this->Form->input('vl_global', array('class' => 'input-xlarge', 'label' => __('Valor da Conta do Cliente (Mensal)', true), 'maxlength' => 18, 'value'=>$this->Formatacao->moeda($contrato['vl_global'], array('before' => '')) ));

                            if ($this->Modulo->isCamposContrato('tp_valor') == true) {
                                echo $this->Form->input('tp_valor', array('class' => 'input-xlarge', 'label' => 'Tipo de Valor', 'options' => array('F' => 'Fixo', 'V' => 'Variável')));
                            }
                            if ($this->Modulo->isCamposContrato('vl_inicial')) {
                                echo $this->Form->input('vl_inicial', array('class' => 'input-xlarge', 'label' => 'Valor da Conta Mensal (Com Redução)', 'maxlength' => 18, 'value'=>$this->Formatacao->moeda($contrato['vl_inicial'], array('before' => '')) ));
                            }
                            if ($this->Modulo->isCamposContrato('nu_qtd_parcelas')) {
                                echo $this->Form->input('nu_qtd_parcelas', array('class' => 'input-xlarge', 'label' => 'Número de parcelas'));
                            }

                            //echo $this->Form->input('vl_mensal', array('class' => 'input-xlarge', 'label' => __('Valor mensal (R$)', true), 'maxlength' => 18));

                            if ($this->Modulo->isCamposContrato('vl_servico') == true) :
                                echo $this->Form->input('vl_servico', array('class' => 'input-xlarge', 'label' => 'Valor serviço (R$)', 'maxlength' => 18));
                            endif;
                            if ($this->Modulo->isCamposContrato('vl_material') == true) :
                                echo $this->Form->input('vl_material', array('class' => 'input-xlarge', 'label' => 'Valor material (R$)', 'maxlength' => 18));
                            endif;

                            echo $this->Form->input('pct_reducao',
                                              array(
                                                'class' => 'input-xlarge',
                                                'label' => 'Percentual de Redução',
                                                'maxlength' => 3
                                              )
                                            );
                            ?>
                        </div>
                    </div>
                </div>

                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Redução Mensal'); ?></h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                          <?php



                          echo $this->Form->input('vl_reducall',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Reducall',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_reducall'], array('before' => ''))
                                            )
                                          );

                          echo $this->Form->input('vl_araras',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Araras',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_araras'], array('before' => ''))
                                            )
                                          );

                          echo $this->Form->input('vl_parceiro',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Parceiro',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_parceiro'], array('before' => ''))
                                            )
                                          );

                           ?>
                        </div>
                      </div>
                    </div>

                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Ressarcimento'); ?></h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                          <?php

                          echo $this->Form->input('vl_ressarcimento',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Total do Ressarcimento (p/ Cliente)',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_ressarcimento'], array('before' => ''))
                                            )
                                          );

                          echo $this->Form->input('vl_ressarcimento_reducall',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Ressarcimento Reducall',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_ressarcimento_reducall'], array('before' => ''))
                                            )
                                          );

                          echo $this->Form->input('vl_ressarcimento_araras',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Ressarcimento Araras',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_ressarcimento_araras'], array('before' => ''))
                                            )
                                          );

                          echo $this->Form->input('vl_ressarcimento_parceiro',
                                            array(
                                              'class' => 'input-xlarge',
                                              'label' => 'Valor Ressarcimento Parceiro',
                                              'maxlength' => 18,
                                              'value'=>$this->Formatacao->moeda($contrato['vl_ressarcimento_parceiro'], array('before' => ''))
                                            )
                                          );

                           ?>
                        </div>
                      </div>
                    </div>

            </div>
        </div>

        <div class="row-fluid">

            <div class="span8 ">

            </div>

            <div class="span4 ">
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4><?php __('Datas do Contrato'); ?></h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <?php
                            //                                        echo $this->Form->input('dt_ini_vigencia', array('class' => 'input-xlarge','label'=>'Vigência Início', 'type'=>'text','id' => 'data' ));
                            //                                        echo $this->Form->input('dt_fim_vigencia', array('class' => 'input-xlarge','label'=>'Vigência Final', 'type'=>'text','id' => 'data2'));
                            $classDatepicker = 'datetimepicker';
                            if ($this->Modulo->isCamposContrato('nu_qtd_parcelas')) {
                                $classDatepicker = 'datetimepickerDtInicial';
                            }
                            echo $this->Form->input('dt_ini_vigencia', array(
                                //'id' => 'dtIniVigencia',
                                'before' => '<div class="input-append date ' . $classDatepicker . '">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar" id="teste"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                'class' => 'input-small', 'label' => __('Início da Vigência', true), 'type' => 'text'));
                            ?>
                            <table>
                                <tr>
                                    <td>
                                        <?php
                                        echo $this->Form->input('dt_fim_vigencia', array(
                                            // 'id' => 'dtFimVigencia',
                                            'before' => '<div class="input-append date datetimepicker">',
                                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                            'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                            'class' => 'input-small', 'label' => __('Fim da Vigência', true), 'type' => 'text'));
                                        ?>
                                    </td>
                                    <td>&nbsp;&nbsp;<b> ou </b>&nbsp;&nbsp;</td>
                                    <td>
                                        <?php
                                        echo $this->Form->input('nu_fim_vigencia', array(
                                            'id' => 'dtNuFimVigencia',
                                            'class' => 'input-mini alert-tooltip', 'title' => 'Preencher caso deseje informar o final da Vigência em dias.', 'label' => __('Fim da Vigência', true) . ' (em dias)', 'type' => 'text'));
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <?php
                            if ($this->Modulo->isCamposContrato('dt_assinatura')) {
                                echo $this->Form->input('dt_assinatura', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de Assinatura', 'type' => 'text'));
                            }
                            if ($this->Modulo->isCamposContrato('dt_fim_suporte')) {
                                echo $this->Form->input('dt_fim_suporte', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Fim do suporte', 'type' => 'text'));
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="view_novo_fornecedor" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">
                    Novo <?php echo $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_fornecedor">
            </div>
        </div>

        <div id="view_novo_auxiliar" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Novo Grupo Auxiliar</h3>
            </div>
            <div class="modal-body-iframe" id="add_auxiliar">
            </div>
        </div>

        <div id="view_nova_modalidade" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Nova <?php __('Modalidade de Contratação'); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_modalidade">
            </div>
        </div>

        <div id="view_novo_tp_contrato" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Novo <?php __('Tipo de Contrato', true); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_tp_contrato">
            </div>
        </div>

        <div id="view_nova_situacao" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Nova <?php __('Situação do Contrato'); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_situacao">
            </div>
        </div>

        <div id="view_nova_categoria" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Nova <?php __('Categoria'); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_categoria">
            </div>
        </div>

        <div id="view_nova_sub_categoria" class="modal hide fade maior" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Nova <?php __('Sub Categoria'); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_sub_categoria">
            </div>
        </div>

        <div id="view_novo_servico" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Nova <?php __('Descrição do Serviço'); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_servico">
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar processo"
                        id="salvar_contrato">Salvar
                </button>
                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                    Limpar
                </button>
                <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
            </div>
        </div>
      </div>


      <script type="text/javascript">
        function completeComZeros() {
            var stringProcess = String($('#ContratoNuProcesso').val());
            if (stringProcess != "_____/__-__") {
                var newString = stringProcess;
                for (var i = 0; i < (stringProcess.split('_').length - 1); i++) {
                    newString = newString.replace('_', '');
                    newString = '0' + newString;
                }
                $('#ContratoNuProcesso').val(newString)
            }
        }

        function atzComboFiscais(co_grupo_auxiliar) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoUsuarioFiscal").html(options);
                $("#ContratoCoUsuarioFiscal option[value=" + co_grupo_auxiliar + "]").attr("selected", true);
                $("#ContratoCoUsuarioFiscal").trigger("chosen:updated");
            });
        }

        function atzComboFornecedor(co_fornecedor) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoFornecedor").html(options);
                $("#ContratoCoFornecedor option[value=" + co_fornecedor + "]").attr("selected", true);
                $("#ContratoCoFornecedor").trigger("chosen:updated");
            });
        }

        function atzComboSolicitante(co_usuario) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoSolicitante").html(options);
                $("#ContratoCoSolicitante option[value=" + co_solicitante + "]").attr("selected", true);
                $("#ContratoCoSolicitante").trigger("chosen:updated");
            });
        }

        function atzComboModalidade(co_contratacao) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'contratacoes', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoContratacao").html(options);
                $("#ContratoCoContratacao option[value=" + co_contratacao + "]").attr("selected", true);
                $("#ContratoCoContratacao").trigger("chosen:updated");
            });
        }

        function atzComboTpContrato(co_modalidade) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'modalidades', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoModalidade").html(options);
                $("#ContratoCoModalidade option[value=" + co_modalidade + "]").attr("selected", true);
                $("#ContratoCoModalidade").trigger("chosen:updated");
            });
        }

        function atzComboSituacao(co_situacao) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'situacoes', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoSituacao").html(options);
                $("#ContratoCoSituacao option[value=" + co_situacao + "]").attr("selected", true);
                $("#ContratoCoSituacao").trigger("chosen:updated");
            });
        }

        function atzComboServico(co_servico) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'servicos', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoServico").html(options);
                $("#ContratoCoServico option[value=" + co_servico + "]").attr("selected", true);
                $("#ContratoCoServico").trigger("chosen:updated");
            });
        }

        function atzComboCategoria(co_categoria) {
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'categorias', 'action' => 'listar'))?>", null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoCategoria").html(options);
                $("#ContratoCoCategoria option[value=" + co_categoria + "]").attr("selected", true);
                $("#ContratoCoCategoria").trigger("chosen:updated");
                atzComboSubCategoria(co_categoria, 0);
            });
        }

        function atzComboSubCategoria(co_categoria, co_sub_categoria) {
            verificaDeContrato(co_categoria);
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'subcategorias', 'action' => 'listar'))?>" + "/" + co_categoria, null, function (data) {
                var options = '<option value="">Selecione..</option>';
                $.each(data, function (index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#ContratoCoSubcategoria").html(options);
                if (co_sub_categoria > 0) {
                    $("#ContratoCoSubcategoria option[value=" + co_sub_categoria + "]").attr("selected", true);
                }
                $("#ContratoCoSubcategoria").trigger("chosen:updated");
            });
        }

        $(function () {
            $('#pctGarantia').closest('div.input').hide();

            setMascaraCampo("#ContratoNuProcesso", "<?php echo FunctionsComponent::pegarFormato('processo'); ?>");
            $("#Vl").maskMoney({thousands: '.', decimal: ','});
            $("#V1").maskMoney({thousands: '.', decimal: ','});
            $("#V2").maskMoney({thousands: '.', decimal: ','});
            $("#V3").maskMoney({thousands: '.', decimal: ','});
            $("#V4").maskMoney({thousands: '.', decimal: ','});

            $("#ContratoVlInicial").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlMensal").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlGlobal").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoNuFimVigencia").maskMoney({precision: 0, allowZero: false, thousands: '.'});
            // porcentagem da garantia
            // $("#pctGarantia").maskMoney({thousands:'.', decimal:','});

            $("#ContratoPctReducao").maskMoney({precision: 0, allowZero: false, thousands: '.'});
            $("#ContratoVlReducall").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlAraras").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlParceiro").maskMoney({thousands: '.', decimal: ','});

            $("#ContratoVlRessarcimento").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlRessarcimentoReducall").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlRessarcimentoAraras").maskMoney({thousands: '.', decimal: ','});
            $("#ContratoVlRessarcimentoParceiro").maskMoney({thousands: '.', decimal: ','});

            $('#icGarantia').on('change', function () {
                if ($(this).val() == 1) {
                    $('#pctGarantia').closest('div.input').show();
                } else {
                    $('#pctGarantia').closest('div.input').hide();
                }
            }).change();

            <?php if($this->Modulo->isCamposContrato('nu_qtd_parcelas')){ ?>

            $("#ContratoNuQtdParcelas").maskMoney({precision: 0, allowZero: false, thousands: '.'});

            $("#ContratoNuQtdParcelas").on('focusout', function (e) {
                var valor = $("#ContratoVlGlobal").val().replace(/\./g, '').replace(/,/g, '.');

                $("#ContratoVlMensal").val(( formatReal((valor / $("#ContratoNuQtdParcelas").val()).toFixed(2).replace('.', '')) ));
                if ($("#ContratoDtIniVigencia").val() != '') {
                    calculaDtFimContrato($("#ContratoDtIniVigencia").val());
                }
            });

            $("#ContratoDtIniVigencia").on('focusout', function (e) {
                calculaDtFimContrato($(this).val());
            });

            <?php } ?>

            $("#ContratoNuProcesso").on('focusout', function (e) {

                var co_contrato = 0;
                if ($(this).val() != '') {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'find_processo')); ?>',
                        data: {
                            "data[Contrato][nu_processo]": $(this).val()
                        },
                        success: function (result) {
                            $('body').append(result);
                            if (result > 0) {
                                e.preventDefault();
                                $("#dialog-processo").dialog({
                                    resizable: false,
                                    modal: true,
                                    title: 'Processo já cadastrado!',
                                    title_html: true,
                                    buttons: [
                                        {
                                            html: "<i class='icon-ok bigger-110'></i>&nbsp; Confirma",
                                            "class": "btn btn-success btn-mini",
                                            click: function () {
                                                $(location).attr('href', '<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'edit'))?>/' + result + '/nc');
                                            }
                                        }
                                        ,
                                        {
                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancela",
                                            "class": "btn btn-mini",
                                            click: function () {
                                                $(this).dialog("close");
                                            }
                                        }
                                    ]
                                });
                            }
                        }
                    })
                }
            });
        });

        $("#AbrirNovoFornecedor").bind('click', function (e) {
            var url_fn = "<?php echo $this->base; ?>/fornecedores/iframe/";
            $.ajax({
                type: "POST",
                url: url_fn,
                data: {},
                success: function (result) {
                    $('#add_fornecedor').html("");
                    $('#add_fornecedor').append(result);
                }
            })
        });
        $("#AbrirNovaModalidade").bind('click', function (e) {
            var url_md = "<?php echo $this->base; ?>/contratacoes/iframe/";
            $.ajax({
                type: "POST",
                url: url_md,
                data: {},
                success: function (result) {
                    console.log(result);
                    $('#add_modalidade').html("");
                    $('#add_modalidade').append(result);
                }
            })
        });
        $("#AbrirNovoTpContrato").bind('click', function (e) {
            var url_tm = "<?php echo $this->base; ?>/modalidades/iframe/";
            $.ajax({
                type: "POST",
                url: url_tm,
                data: {},
                success: function (result) {
                    $('#add_tp_contrato').html("");
                    $('#add_tp_contrato').append(result);
                }
            })
        });
        $("#AbrirNovoServico").bind('click', function (e) {
            var url_sv = "<?php echo $this->base; ?>/servicos/iframe/";
            $.ajax({
                type: "POST",
                url: url_sv,
                data: {},
                success: function (result) {
                    $('#add_servico').html("");
                    $('#add_servico').append(result);
                }
            })
        });
        $("#AbrirNovaSituacao").bind('click', function (e) {
            var url_st = "<?php echo $this->base; ?>/situacoes/iframe/";
            $.ajax({
                type: "POST",
                url: url_st,
                data: {},
                success: function (result) {
                    $('#add_situacao').html("");
                    $('#add_situacao').append(result);
                }
            })
        });
        $("#AbrirNovaCategoria").bind('click', function (e) {
            var url_ct = "<?php echo $this->base; ?>/categorias/iframe/";
            $.ajax({
                type: "POST",
                url: url_ct,
                data: {},
                success: function (result) {
                    $('#add_categoria').html("");
                    $('#add_categoria').append(result);
                }
            })
        });
        $("#AbrirNovaSubCategoria").bind('click', function (e) {
            var url_sb = "<?php echo $this->base; ?>/subcategorias/iframe/";
            $.ajax({
                type: "POST",
                url: url_sb,
                data: {},
                success: function (result) {
                    $('#add_sub_categoria').html("");
                    $('#add_sub_categoria').append(result);
                }
            })
        });

        $("#ContratoCoCategoria").change(function () {
            atzComboSubCategoria($(this).val(), 0);
        });
        <?php if($this->Modulo->isCamposContrato('co_subcategoria') == true) { ?>
        // document.getElementById('ContratoCoSubcategoria').disabled = true ;
        <?php } ?>


        function verificaDeContrato(tipo) {


            if (tipo == "") {
                document.getElementById('ContratoCoSubcategoria').disabled = true;
            } else {
                document.getElementById('ContratoCoSubcategoria').disabled = false;
            }


        }
        $("#AbrirNovoAuxiliar").bind('click', function (e) {
            var url_md = "<?php echo $this->base; ?>/grupo_auxiliares/iframe/";
            $.ajax({
                type: "POST",
                url: url_md,
                data: {},
                success: function (result) {
                    $('#add_auxiliar').html("");
                    $('#add_auxiliar').append(result);
                }
            })
        });

        $(document).ready(function () {
            var _fill = {<?php foreach ($_POST as $k => $v) if (substr($k, 0, 6) == '_fill_') echo substr($k, 6) . ':' . '"' . str_replace('"', "", $v) . '",'; ?>};
            for (id in _fill) $('#' + id).val(_fill[id]);


      //        $("#dtNuFimVigencia").prop("disabled", true);
      //        $("#dtFimVigencia").prop("disabled", true);

            $("#dtNuFimVigencia").keydown(function () {

                if ($('#dtIniVigencia').val() == '' || $('#dtIniVigencia').val() == '' || $('#dtIniVigencia').val() == null) {
                    $('#dtIniVigencia').val(moment().format('DD/MM/YYYY'));
                }

            });
        });

        function validaGestorSuplente(gestor) {
            var op = document.getElementById("gestorSuplente");
            for (var i = 0; i < gestor.length; i++) {
                if (gestor[gestor.selectedIndex].value == op[i].value) {
                    if (gestor.selectedIndex == op.selectedIndex) {
                        $("#gestorSuplente").val(0);
                    }
                    op[i].disabled = true;
                    $("#gestorSuplente").trigger("chosen:updated");
                } else {
                    op[i].disabled = false;
                }
            }
        }

      </script>
