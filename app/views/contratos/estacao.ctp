<button class="btn btn-small btn-sm">Teste</button>
                <div class="infobox infobox-blue2">
                        <div class="infobox-progress">
                                <div class="easy-pie-chart percentage" data-percent="50" data-size="46">
                                        <span class="percent">50</span>
                                        %
                                </div>
                        </div>

                        <div class="infobox-data">
                                <span class="infobox-text">Realizado</span>

                                <div class="infobox-content">
                                        48% Restante
                                </div>
                        </div>
                </div>

    <script type="text/javascript">
        
        $(function() {
            $('.easy-pie-chart.percentage').each(function(){
                    var $box = $(this).closest('.infobox');
                    var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
                    var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
                    var size = parseInt($(this).data('size')) || 50;
                    $(this).easyPieChart({
                            barColor: barColor,
                            trackColor: trackColor,
                            scaleColor: false,
                            lineCap: 'butt',
                            lineWidth: parseInt(size/10),
                            animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
                            size: size
                    });
            })
        });


    </script>