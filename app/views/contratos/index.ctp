<?php

$moduloConfig = 'ctr';
$usuario = $this->Session->read('usuario');
if (isset($usuario['ic_tipo_contrato']) && $usuario['ic_tipo_contrato'] == 'E') {
    $moduloConfig = 'cte';
}
echo $this->element('configurador_campos_pesquisa', array('moduloConfig' => $moduloConfig));

if (isset($usuario['palavra_chave'])) {
    $palavra_chave = $usuario['palavra_chave'];
}

$papel = ($papel != null) ? "?papel=$papel" : '';

?>

<div class="row-fluid">

    <div class="page-header position-relative">
        <div class="row-fluid">
            <h1>
                <?php
                $nome_link_contratos = __('Contratos', true);
                if ($this->Modulo->isProcesso()) {
                    $nome_link_contratos .= ' / ' . __('Processos', true);
                }
                echo $nome_link_contratos;
                ?>
            </h1>
        </div>
        <div class="row-fluid">
            <?php echo $this->Form->create('Filter', array('url' => array('controller' => 'contratos', 'action' => $papel))); ?>
            <div class="span3">
                <?php echo $this->Form->input('filter_type', array('label' => 'Filtro', 'class' => 'input-xlarge', 'type' => 'select', 'options' => array('Contratos' => 'Contratos em Execução', /*'Processos' => 'Processos',*/ 'PAMs' => __('Prospecção', true)), 'empty' => 'Todos')); ?>
            </div>
            <div class="span3">
                <?php //echo $this->Form->input('filter_uasg', array('label' => 'UASG', 'class' => 'input-xlarge', 'type' => 'select', 'options' => array($uasgsArray), 'empty' => 'Todos')); ?>
            </div>

            <div class="span3">
                <?php echo $this->Form->input('palavra_chave', array('label' => 'Palavra-chave', 'class' => 'input-xlarge', 'type' => 'text', 'value' => $palavra_chave)); ?>
            </div>
            <div class="span1 pull-right">
                <input type="button" name="Limpar" value="Limpar" class="btn btn-primary pull-right" style="bottom: -15px;" onclick="window.location='/contratos';">
            </div>
            <div class="span1 pull-right">
                <button class="btn btn-primary pull-right" type="submit" style="bottom: -15px;">Filtrar</button>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <!-- /.page-header -->
    <?php
    if ($this->Modulo->isContratoExterno()) {
    ?>
    <div class="box-tabs-white">
        <ul class="nav nav-tabs" id="myTab">
            <?php
            $abas = array();

            $abas[] = array('fsInterno', 'Contratos Internos', true, '');
            $abas[] = array('fsExterno', 'Contratos Externos', true, '');

            $idAtivo = 0;
            $fieldset1 = 'id="fsInterno" class="ativo"';
            $fieldset2 = 'id="fsExterno" class="inativo" style="display: none"';
            if (isset($usuario['ic_tipo_contrato']) && $usuario['ic_tipo_contrato'] == 'E') {
                $idAtivo = 1;
                $fieldset1 = 'id="fsExterno" class="ativo"';
                $fieldset2 = 'id="fsInterno" class="inativo" style="display: none"';
            }

            echo $aba->render($abas, null, $idAtivo, 'carregaContrato');

            ?>
        </ul>
        <div class="tab-content" id="myTabContent">

            <fieldset <?php echo $fieldset1 ?>>

                <?php
                }
                ?>
                <table style="table-layout:auto;" cellpadding="0" cellspacing="0"
                       class="table table-hover table-bordered table-striped"
                       id="list_contratos">
                    <!-- INIT TR -->
                    <tr>
                        <th class="actions">&nbsp;</th>
                        <?php
                        foreach ($colunasResultadoPesquisa as $colunaResultadoPesquisa) :
                            if ($colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_nome'] == 'Objeto') {
                                echo '<th style="width:40%">' . $colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_nome'] . '</th>';
                            } else {
                                if ($colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_nome'] == 'PAM')
                                {
                                  echo '<th>Prospecções</th>';
                                }else{
                                  echo '<th>' . $colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_nome'] . '</th>';
                                }

                            }
                        endforeach;
                        ?>
                    </tr>
                    <!-- END TR -->
                    <?php
                    $i = 0;
                    foreach ($contratos as $contrato):
                        ?>
                        <tr>
                            <td class="actions">
                                <?php echo $this->element('menu_contrato', array('contrato' => $contrato)); ?>
                            </td>
                            <?php
                            foreach ($colunasResultadoPesquisa as $colunaResultadoPesquisa) :
                                if ($colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio'] == 'ds_objeto') {
                                    if (($contrato['Contrato']['ds_objeto'] == null) && ($contrato['Contrato']['ds_objeto_plano'] != null)) {
                                        $contrato['Contrato']['ds_objeto'] = '[PLANO DE OBJETO] - ' . $contrato['Contrato']['ds_objeto_plano'];
                                    }
                                    $colunaObjeto = $this->element('funcoes_exibe_campos_pesquisa',
                                            array('colunasResultadoPesquisa' => $colunaResultadoPesquisa, 'registro' => $contrato));
                                    echo '<td ><div class="text-justify" style="word-wrap: break-word; word-break:break-all; white-space:normal">'. $colunaObjeto . '</div></td>';
                                } else {
                                    $str = $this->element('funcoes_exibe_campos_pesquisa', array('colunasResultadoPesquisa' => $colunaResultadoPesquisa, 'registro' => $contrato));
                                    if ($colunaResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio'] == 'no_razao_social') {
                                        $str = '<div class="text-justify" style="word-wrap: break-word; word-break:break-all; white-space:normal">'. $str . '</div>';
                                    }
                                    echo '<td>' .$str. '</td>';
                                }
                            endforeach;
                            ?>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    if ($i == 0) {
                        echo '<tr><td colspan="8">Não existem registros a serem exibidos.</td></tr>';
                    }
                    ?>
                </table>

                <p>
                    <?php
                    echo $this->Paginator->counter(
                        array('format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
                        ));
                    if (isset($this->params['url']['papel'])) {
                        $this->Paginator->options(array('url' => $this->params['url']['papel']));
                    }
                    ?>
                </p>
                <div class="pagination">
                    <ul>
                        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>
                        <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                    </ul>
                </div>

                <?php
                if ($this->Modulo->isContratoExterno()) {
                ?>
            </fieldset>
            <fieldset <?php echo $fieldset1 ?>>
            </fieldset>
            <br><br><br><br><br>
        </div>
        <?php
        }
        echo $this->element('script_menu_contrato');
        ?>

        <?php
        if ($this->Modulo->isContratoExterno()) {
        ?>
    </div>
<?php
}
?>

</div>

<script type="text/javascript">
    function carregaContrato(objeto, aba) {
        if (aba == 'fsInterno') {
            $("#ContratoIcTipoContrato").val("I");
        } else {
            $("#ContratoIcTipoContrato").val("E");
        }
        $("#searchContrato").submit();
    }
</script>
