﻿<?php
    $usuario = $this->Session->read ('usuario');
?>
<div class="row-fluid">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped" id="list_contratos">
	<thead>
    <tr>
        <th class="actions" style="width: 25px;">&nbsp;</th>
		<th><?php __('Contrato'); ?></th>
            <?php if($this->Modulo->isProcesso()) { ?>
                <th><?php __('Processo'); ?></th>
            <?php } ?>
		<th>Situação</th>
		<th>Objeto</th>
		<th><?php __('Fornecedor'); ?></th>
	</tr>
    </thead>
	<?php
	$i = 0;
	foreach ($contratos as $contrato):
        $i++;
        $link_contrato  = $this->Html->url(array('controller' => 'contratos', 'action' => 'detalha', $contrato['Contrato']['co_contrato']));
	?>
    <tbody>
	<tr>
            
		<td class="actions">
                    <?php echo $this->element( 'menu_contrato', array( 'contrato' => $contrato ) ); ?>
		</td>
		
            <?php if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ) { ?>
		<td><?php echo $this->Html->link(__($this->Print->contrato( $contrato['Contrato']['nu_contrato'] ), true), $link_contrato); ?>&nbsp;</td>
            <?php } else { ?>
		<td><?php echo $this->Print->contrato( $contrato['Contrato']['nu_contrato'] ); ?>&nbsp;</td>
            <?php } ?>
                
            <?php if($this->Modulo->isProcesso()) { ?>
                <td><?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>&nbsp;</td>
            <?php } ?>

		<td><?php echo $contrato['Situacao']['ds_situacao'] ? $contrato['Situacao']['ds_situacao'] : '-' ?>&nbsp;</td>
                
        <!--<td>--><?php //echo $this->Print->printHelp( $contrato['Contrato']['ds_objeto'] , $contrato['Contrato']['ds_objeto'], 50); ?><!--&nbsp;</td>-->
        <td><?php echo $this->Print->printHelp( $contrato['Contrato']['ds_objeto'] ); ?>&nbsp;</td>

        <td><?php echo $contrato['Fornecedor']['no_razao_social']; ?>&nbsp;</td>
	</tr>
	<?php
        endforeach;
        echo $this->element( 'script_menu_contrato' );
        if(count($contratos) == 0){
            echo '<tr><td colspan="6">Não existem registros a serem exibidos.</td></tr>';
        }
        ?>
    </table>
    <?php if($i > 0){echo '<BR>';} ?>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

<?php echo $this->Html->scriptEnd() ?>
