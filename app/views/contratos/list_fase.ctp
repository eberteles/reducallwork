<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
﻿<?php
    $usuario = $this->Session->read ('usuario');
?>
<div class="row-fluid">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped" id="list_processos">
	<tr>
                <th style="width: 60px"><?php __('Processo'); ?></th>
		<th width="10%">Prazo</th>
             <?php
             if(count($situacoes) > 0) {
                $width  = 70 / count($situacoes);
                foreach ($situacoes as $situacao):
             ?>
		<th width="<?php echo $width; ?>%"><?php echo $situacao['Situacao']['ds_situacao']; ?></th>
             <?php
                endforeach;
             }
             ?>
                <th style="width: 40px">&nbsp;</th>
	</tr>
	<?php
	$i = 0;
	foreach ($contratos as $contrato):
            $class = null;
            if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
            }

            $link_contrato  = $this->Html->url(array('controller' => 'contratos', 'action' => 'detalha', $contrato['Contrato']['co_contrato']));
	?>
	<tr <?php echo $class;?>>
            
            <?php if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ): ?>
                <td><?php echo $this->Print->printHelp( $this->Html->link($this->Print->processo( $contrato['Contrato']['nu_processo']), $link_contrato) , $contrato['Contrato']['ds_objeto'], 200); ?></td>
            <?php else: ?>
                <td><?php echo $this->Print->printHelp( $this->Print->processo( $contrato['Contrato']['nu_processo'] ) , $contrato['Contrato']['ds_objeto'], 200); ?></td>
            <?php endif; ?>                
                <td><?php echo $this->Print->prazoAtividade('', $contrato['Contrato']['dt_prazo_processo'] ); ?></td>                
            <?php
            foreach ($situacoes as $situacao):
            ?>
                <td <?php if ( $contrato['Contrato']['co_situacao'] == $situacao['Situacao']['co_situacao'] ) echo 'style="background-color: wheat"'; ?>>
                  <?php
                    for($i = 0; $i < count( $contrato['Pendencia'] ); $i++) {
                        if(!isset($contrato['Pendencia'][$i]['co_pendencia'])) {
                            unset($contrato['Pendencia'][$i]);
                        } else {
                            if($contrato['Pendencia'][$i]['co_situacao'] <= 0) {
                                unset($contrato['Pendencia'][$i]);
                            }
                            else {
                                if($contrato['Pendencia'][$i]['co_situacao'] == $situacao['Situacao']['co_situacao'] && $contrato['Pendencia'][$i]['st_pendencia'] == 'P') {
                          ?>
                            <i style="cursor: pointer" class="red icon-info-sign bigger-110" data-rel="popover" data-title="Pendência - <?php echo $contrato['Pendencia'][$i]['dt_inicio'] ?>" data-trigger="hover" data-placement="bottom" data-content="<?php echo $this->Print->getHelpPendencia($contrato['Pendencia'][$i]);?>"></i> &nbsp;
                          <?php
                                    unset($contrato['Pendencia'][$i]);
                                }
                            }
                        }
                    }

                    for($i = 0; $i < count( $contrato['Historico'] ); $i++) {
                        if(!isset($contrato['Historico'][$i]['co_historico'])) {
                            unset($contrato['Historico'][$i]);
                        } else {
                            if($contrato['Historico'][$i]['co_situacao'] <= 0) {
                                unset($contrato['Historico'][$i]);
                            }
                            else {
                                if($contrato['Historico'][$i]['co_situacao'] == $situacao['Situacao']['co_situacao']) {
                          ?>
                            <i style="cursor: pointer" class="blue icon-film bigger-110" data-rel="popover" data-title="<?php echo $contrato['Historico'][$i]['dt_historico'] ?> - <?php echo $contrato['Historico'][$i]['no_assunto'] ?>" data-trigger="hover" data-placement="bottom" data-content="<?php echo $contrato['Historico'][$i]['ds_observacao'] ?>"></i> &nbsp;
                          <?php
                                    unset($contrato['Historico'][$i]);
                                }
                            }
                        }
                    }
                  ?>
                </td>
            <?php
            endforeach;
            ?>  
		<td>
                    
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-info btn-small dropdown-toggle alert-tooltip" title="Mais opções">
                                <i class="icon-caret-down"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-info pull-right">
                                <li>
                                        <a class="v_detalhar" href="#view_dominio" data-toggle="modal" titulo-aba="<?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>" id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="icon-list"></i> &nbsp; Detalhar Processo</a>
                                </li>
                                
                                <li>
                                        <a class="v_editar" href="#view_dominio" data-toggle="modal" titulo-aba="<?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>" id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="icon-pencil"></i> &nbsp; Editar Processo</a>
                                </li>

                                <li>
                                        <a class="v_pendencias" href="#view_dominio" data-toggle="modal" titulo-aba="<?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>" id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="red icon-info-sign bigger-110"></i> &nbsp; Pendências</a>
                                </li>

                                <li>
                                        <a class="v_historicos" href="#view_dominio" data-toggle="modal" titulo-aba="<?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>" id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="blue icon-film bigger-110"></i> &nbsp; Históricos</a>
                                </li>

                                <li>
                                        <a class="upd_prazo" href="#upd-prazo" data-toggle="modal" titulo-aba="<?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>" id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="icon-time"></i> &nbsp; Denifir Prazo</a>
                                </li>
                        </ul>
                    </div>
		</td>
	</tr>
	<?php endforeach;
        if($i == 0){
            echo '<tr><td colspan=4>Não existem registros a serem exibidos.</td></tr>';
        }
        ?>
    </table>
    <?php if($i > 0){echo '<BR><BR><BR><BR><BR><BR><BR>';} ?>
</div>

<div id="upd-prazo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="tituloDefinirPrazo">Denifir Prazo</h4>
  </div>
  <div class="modal-body">
      <div class="control-group">
              <?php 
                        echo $this->Form->create('Contrato', array('id'=>'form_upd_prazo_processo'));
                        echo $this->Form->hidden('co_contrato_upd_prazo');
               ?>
          <div class="required" id="id_dt_prazo_processo">
              <?php 
                        echo $this->Form->input('Contrato.dt_prazo_processo', array(
                                    'before' => '<div class="input-append date datetimepicker">', 
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                    'data-format' => 'dd/MM/yyyy', 'data-mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Parzo para Conclusão', 'type'=>'text'));
              ?>
          </div>
              <?php 
                        echo $this->Form->end();
              ?>
      </div>
  </div>
  <div class="modal-footer">
  <button class="btn btn-small btn-primary" data-dismiss="modal" id="concluir_upd_prazo"><i class="icon-ok icon-white"></i> Concluir</button>
  <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {

    function abrirFrame(titulo, url_frame) {
    
        $('#tituloAbaDominio').html("");
        $('#tituloAbaDominio').append(titulo);
        $.ajax({
            type:"POST",
            url:url_frame,
            data:{
                },
                success:function(result){
                    $('#list_dominio').html("");
                    $('#list_dominio').append(result);
                }
            });
    
    }

    $('#list_processos tr td a.v_editar').click(function(){
        abrirFrame("Processo: " + $(this).attr('titulo-aba'), "<?php echo $this->base; ?>/contratos/iframe/" + $(this).attr('id') + "/edit");
    });
    
    $('#list_processos tr td a.v_detalhar').click(function(){
        abrirFrame("Processo: " + $(this).attr('titulo-aba'), "<?php echo $this->base; ?>/contratos/iframe/" + $(this).attr('id') + "/detalha");
    });
    
    $('#list_processos tr td a.v_pendencias').click(function(){
        abrirFrame("Pendências: " + $(this).attr('titulo-aba'), "<?php echo $this->base; ?>/pendencias/iframe/" + $(this).attr('id'));
    });
    
    $('#list_processos tr td a.v_historicos').click(function(){
        abrirFrame("Históricos: " + $(this).attr('titulo-aba'), "<?php echo $this->base; ?>/historicos/iframe/" + $(this).attr('id'));
    });
    
    $('#list_processos tr td a.upd_prazo').click(function(){
        $('#tituloDefinirPrazo').html("");
        $('#tituloDefinirPrazo').append("Processo: " + $(this).attr('titulo-aba') );
        
        $('#ContratoCoContratoUpdPrazo').val($(this).attr('id'));
        $('#ContratoDtPrazoProcesso').val('');
    });
    
    $('#concluir_upd_prazo').click(function(){
        $('#id_error_dt_prazo_processo').remove();
        var validacao   = false;
        if($('#ContratoDtPrazoProcesso').val() == '') {
            $('#id_dt_prazo_processo').append('<div class="error-message" id="id_error_dt_prazo_processo">Campo Data em branco.</div>');
            validacao   = true;
        }
        if(validacao) {
            return false;
        } else { // Gravar Prazo
            var url_upd = "<?php echo $this->base; ?>/contratos/upd_prazo/" + $('#ContratoCoContratoUpdPrazo').val();
            $.ajax({
                type:"POST",
                url:url_upd,
                data:{
                    "data[dt_prazo_processo]":$('#ContratoDtPrazoProcesso').val()
                },
                success:function(result){
                    atualizaListProcessos();
                }
            })
        }

    })

    $('.alert-tooltip').tooltip();

    $('[data-rel=popover]').popover({html:true});
    
});

<?php echo $this->Html->scriptEnd() ?>