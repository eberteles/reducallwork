﻿<?php
    $usuario = $this->Session->read ('usuario');
?>
<div class="row-fluid">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped" id="list_processos">
	<tr>
                <th>Recebimento</th>
                <th>Prazo</th>
                <?php if( $this->Modulo->isPam() ) { ?>
		<th><?php __($this->Modulo->isSuporteDocumental() ? 'DOC' : __('PAM')); ?></th>
                <?php } ?>
                <th><?php __('Processo'); ?></th>
		<th>Observações</th>
		<th>Fase</th>
                <th style="width: 40px">&nbsp;</th>
	</tr>
	<?php
	$i = 0;
	foreach ($contratos as $contrato):
            $class = null;
            if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
            }

            $andamentoAtual = null;
            foreach ($contrato['Andamento'] as $andamento):
                if($andamento['nu_sequencia'] == $contrato['Contrato']['nu_sequencia'] || $andamento['co_setor'] == $contrato['Contrato']['co_setor'] ) {
                    $andamentoAtual = $andamento;
                }
            endforeach;

            $link_contrato  = $this->Html->url(array('controller' => 'contratos', 'action' => 'detalha', $contrato['Contrato']['co_contrato']));
	?>
	<tr <?php echo $class;?>>
            
                <td><?php echo $this->Print->printHelp( $andamentoAtual['dt_andamento'] , 'Prazo Decorrido na Fase Atual: ' . $this->Print->getPrazoDecorrido($andamentoAtual['dt_andamento'], $andamentoAtual['dt_fim']) ); ?>&nbsp;</td>
                <td>
                    <?php
                        $corAviso; $mensagemAviso; $tempoDecorrido;
                        $this->Print->setPrazoDecorridoDetalhaProcesso($andamentoAtual['dt_andamento'], $andamentoAtual['dt_fim'], 
                                $contrato['Fase']['mm_duracao_fase'], $corAviso, $mensagemAviso, $tempoDecorrido);
                    ?>
                    <div class="alert-tooltip <?php echo $corAviso; ?>" title="<?php echo $mensagemAviso; ?>">
                        <i class="icon-time bigger-110 <?php echo $corAviso; ?>"></i>
                        <b><?php echo $tempoDecorrido; ?></b>
                    </div>
                </td>
            
            <?php if( $this->Modulo->isPam() ) { ?>
              <?php if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ) { ?>
		<td><?php echo $this->Html->link(__($this->Print->pam( $contrato['Contrato']['nu_pam'] ), true), $link_contrato); ?>&nbsp;</td>
              <?php } else { ?>
                <td><?php echo $this->Print->pam( $contrato['Contrato']['nu_pam'] ); ?>&nbsp;</td>
              <?php } ?>
            <?php } ?>
                
            <?php if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ) { ?>
                <td><?php echo $this->Html->link(__($this->Print->processo( $contrato['Contrato']['nu_processo'] ), true), $link_contrato); ?>&nbsp;</td>
            <?php } else { ?>
                <td><?php echo $this->Print->processo( $contrato['Contrato']['nu_processo'] ); ?>&nbsp;</td>
            <?php } ?>

		<td><?php echo $this->Print->printHelp( $contrato['Contrato']['ds_observacao'] , $contrato['Contrato']['ds_observacao'], 50); ?>&nbsp;</td>
                
		<td><?php echo $contrato['Fase']['ds_fase']; ?>&nbsp;</td>

		<td>
                    <?php
                        if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/edit') ) {
                            echo $this->Html->link( '<i class="icon-pencil"></i>', 
                                    array( 'controller' => 'contratos', 'action' => 'edit', 
				
                                           $contrato['Contrato']['co_contrato']), 
                                    array( 'escape' => false,
                                           'class' => 'btn btn-small btn-success' )
                            );
                        }                        
                    ?>
		</td>
	</tr>
	<?php endforeach;
        if($i == 0){
            echo '<tr><td colspan=4>Não existem registros a serem exibidos.</td></tr>';
        }
        ?>
    </table>
    <?php if($i > 0){echo '<BR>';} ?>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

<?php echo $this->Html->scriptEnd() ?>