<div class="row-fluid">
    <div class="page-header position-relative"><h1>Solicitar NOVO <?php __('PAM'); ?></h1></div>

    <div class="row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <div class="span6">
                    <?php echo $this->Form->input('tp_pam', array('class' => 'input-large', 'type' => 'select', 'label' => __('Selecione o Tipo de ' . __('PAM', true) . ' desejado:', true), 'empty' => 'Selecione..', 'options' => array(1=>'01 - Material e Serviço', 2=>'02 - Material e Serviço Constante com Ata', 3=>'03 - Seção de Licitações') )); ?>
                </div>
                <div class="span6">
                    <br>
                    <button id="addPamExterno" rel="tooltip" type="submit" class="btn btn-small btn-primary btn-next" title="Prosseguir">Prosseguir <i class="icon-arrow-right icon-on-right"></i></button>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="widget-header widget-header-small"><h4><font>Ajuda - Descrição dos Tipos de PAM</font></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <b>01 - Material e Serviço</b><br>
                  Modelo geral de pedido.<br><br>
                  <b>02 - Material e Serviço Constante com Ata de Registro de Preço Gerenciada</b><br>
                  Modelo de pedido baseado em adesão à ata de registro de preços (do HFA ou de terceiros).<br><br>
                  <b>03 - Seção de Licitações</b><br>
                  Modelo de pedido para pregão na Seção de Licitações.
              </div>
            </div>
        </div>
    </div>

    <div class="page-header position-relative"><h1>PAM's Solicitados</h1></div>
    <div class="row-fluid">
        <div class="span5">
            <div class="widget-header widget-header-small"><h4><font>Últimos PAM's Solicitados</font></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($pams as $pam):
                        $i++;
                    ?>
                        <tr>
                            <td><?php echo $this->Print->pam($pam['Contrato']['nu_pam']); ?></td>
                            <td><?php echo $pam['Contrato']['dt_cadastro_pam']; ?></td>
                            <td><?php echo isset($pam['Setor']['ds_setor']) ? $pam['Setor']['ds_setor'] : '---'; ?></td>
                        </tr>
                    <?php
                    endforeach;
                    if($i == 0) {
                        echo 'Nenhum ' . __('PAM', true) . ' foi solicitado até o momento.';
                    }
                    ?>
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->scriptStart()?>

    $('#addPamExterno').click(function(){
        //alert( $("#tp_pam option:selected" ).text() );
        if($('#tp_pam').val() > 0) {
            $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'add_pam_externo') )?>/' + $('#tp_pam').val());
        } else {
            alert('Selecione o Tipo de <?php __('PAM'); ?> desejado.');
        }

    })

<?php echo $this->Html->scriptEnd() ?>