<br>

<?php
if (floatval($contrato['vl_global']) != 0) {
    $percent_empenho = (floatval($total_empenhado) * 100) / floatval($contrato['vl_global'] + $vl_total_apostilamento + $vl_total_aditivo);
} else {
    $percent_empenho = 0;
}
// echo 'pc_empenhado' . $percent_empenho;exit;
?>
  <?php if($this->Modulo->isEmpenho()) { ?>
<div class="infobox infobox-blue" style="height: 39px">

    <div class="infobox-progress">
        <div class="easy-pie-chart percentage easyPieChart" data-percent="<?php echo $percent_empenho; ?>"
             data-size="39" style="width: 55px; height: 39px; line-height: 55px;">
            <span class="percent"><?php echo $this->Formatacao->porcentagem($percent_empenho, 0); ?></span>
            <canvas width="55" height="39"></canvas>
        </div>
    </div>

    <div class="infobox-data">
        <div class="infobox-content"><font><font class="">
                    &nbsp;&nbsp;&nbsp;<?php echo __('Empenhos', true); ?></font></font></div>
        <div class="infobox-content"><font><font class="goog-text-highlight">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor <?php echo __('empenhado', true); ?>.</font></font></div>
    </div>
</div>

<br>
  <?php } ?>

<?php
$pc_pagamento = 0;
$pc_aditivo = 0;
$pc_restante = 0;
$valor_total = $contrato['vl_global'] + $vl_total_aditivo;
$valor_restante = $valor_total - $vl_total_pagamento;
if ($contrato['vl_global'] > 0) {
    $pc_pagamento = ($vl_total_pagamento * 100) / $valor_total;
    $pc_aditivo = ($vl_total_aditivo * 100) / $contrato['vl_global'];
    $pc_restante = ($valor_restante * 100) / $valor_total;
}
$dt_fim_vigencia_contrato = $this->Print->proximoDia($contrato['dt_fim_vigencia']);
$id_periodo_contrato = $contrato['dt_ini_vigencia'] . '&nbsp;&nbsp;à&nbsp;&nbsp;' . $contrato['dt_fim_vigencia'];

if ($this->Modulo->isPagamento()) {
    ?>
    <br>
    <div class="infobox infobox-green infobox-small infobox-dark">
        <div class="infobox-progress">
            <div class="easy-pie-chart percentage easyPieChart" data-percent="<?php echo $pc_pagamento; ?>"
                 data-size="39" style="width: 39px; height: 39px; line-height: 39px;">
                <span class="percent"><font><font><?php echo $this->Formatacao->porcentagem($pc_pagamento, 0); ?></font></font></span><font><font>
                    </font></font>
                <canvas width="39" height="39"></canvas>
            </div>
        </div>

        <div class="infobox-data">
            <div class="infobox-content"><font><font
                            class=""><?php $this->Print->getLabelPagamento($contrato['ic_tipo_contrato']); ?></font></font>
            </div>
            <div class="infobox-content"><font><font class="goog-text-highlight">Realizado</font></font></div>
        </div>
    </div>
<?php }
$cor_aditivo = 'infobox-blue';
if ($pc_aditivo >= 25) {
    $cor_aditivo = 'infobox-red';
} ?>
<?php if ($this->Modulo->isAditivo()) { ?>
    <div class="infobox <?php echo $cor_aditivo; ?> infobox-small infobox-dark">
        <div class="infobox-progress">
            <div class="easy-pie-chart percentage easyPieChart" data-percent="<?php echo $pc_aditivo; ?>" data-size="39"
                 style="width: 39px; height: 39px; line-height: 39px;">
                <span class="percent"><font><font><?php echo $this->Formatacao->porcentagem($pc_aditivo, 0); ?></font></font></span><font><font>
                    </font></font>
                <canvas width="39" height="39"></canvas>
            </div>
        </div>

        <div class="infobox-data">
            <div class="infobox-content"><font><font class="">Aditivos</font></font></div>
            <div class="infobox-content"><font><font class="goog-text-highlight">Total</font></font></div>
        </div>
    </div>
<?php } ?>
<br><br>

<?php
$pc_decorrido = 0;
$titulo = '';
$descricao = '';
if ($contrato['nu_contrato'] != "" && $contrato['dt_ini_vigencia'] != "" && $contrato['dt_fim_vigencia'] != "") {
    $diasExpira = $this->Print->expira($contrato['dt_fim_vigencia']);
    $titulo = 'Prazo Decorrido';
    if ($diasExpira > -1) {
        if ($diasExpira == 0) {
            $descricao = 'Expira hoje';
        } else if ($diasExpira == 1) {
            $descricao = 'Expira amanhã';
        } else {
            $descricao = 'Expira em ' . $diasExpira . ' dias';
        }
        $tempoTotal = $this->Print->difEmDias($contrato['dt_ini_vigencia'], $contrato['dt_fim_vigencia']);
        $pc_decorrido = (($tempoTotal - $diasExpira) * 100) / $tempoTotal;
    } else {
        $pc_decorrido = 100;
        $descricao = '<font color="#FF0000">Finalizado</font>';
    }
}
if ($contrato['nu_processo'] != "" && $contrato['dt_ini_processo'] != "" && $contrato['dt_fim_processo'] != "") {
    $diasExpira = $this->Print->expira($contrato['dt_fim_processo']);
    $titulo = 'Prazo Decorrido';
    if ($diasExpira > -1) {
        if ($diasExpira == 0) {
            $descricao = 'Termina hoje';
        } else if ($diasExpira == 1) {
            $descricao = 'Termina amanhã';
        } else {
            $descricao = 'Termino em ' . $diasExpira . ' dias';
        }
        $tempoTotal = $this->Print->difEmDias($contrato['dt_ini_processo'], $contrato['dt_fim_processo']);
        $pc_decorrido = (($tempoTotal - $diasExpira) * 100) / $tempoTotal;
    } else {
        $pc_decorrido = 100;
        $descricao = '<font color="#FF0000">Finalizado</font>';
    }
}
if ($titulo != '' && $descricao != '') {
    ?>

    <div class="infobox infobox-blue" style="height: 39px">
        <div class="infobox-progress">
            <div class="easy-pie-chart percentage easyPieChart" data-percent="<?php echo $pc_decorrido; ?>"
                 data-size="39" style="width: 55px; height: 39px; line-height: 55px;">
                <span class="percent"><?php echo $this->Formatacao->porcentagem($pc_decorrido, 0); ?></span>
                <canvas width="55" height="39"></canvas>
            </div>
        </div>

        <div class="infobox-data">
            <div class="infobox-content"><font><font class="">&nbsp;&nbsp;&nbsp;<?php echo $titulo; ?></font></font>
            </div>
            <div class="infobox-content"><font><font class="goog-text-highlight">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $descricao; ?>.</font></font></div>
        </div>
    </div>
    <?php
}
?>

<br><br>

<script type="text/javascript">

    $(function () {
        <?php if($contrato['nu_contrato'] != "") { ?>
        $('#id_periodo_contrato').html("");
        $('#id_periodo_contrato').append("<?php echo $id_periodo_contrato; ?>");
        $('#dt_fim_vigencia_contrato').val("<?php echo $dt_fim_vigencia_contrato; ?>");
        <?php } ?>

        $('.easy-pie-chart.percentage').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
            var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
            var size = parseInt($(this).data('size')) || 50;
            $(this).easyPieChart({
                barColor: barColor,
                trackColor: trackColor,
                scaleColor: false,
                lineCap: 'butt',
                lineWidth: parseInt(size / 10),
                animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
                size: size
            });
        })
    });


</script>
