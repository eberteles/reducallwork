<div>
    <?php
        echo $this->Form->input('Contrato.dt_prazo_processo', array(
                'before' => '<div class="input-append date datetimepicker">', 
                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                'class' => 'input-small', 'label' => 'Parzo para Conclusão', 'type'=>'text'));
    ?>
</div>