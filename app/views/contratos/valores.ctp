                <?php
                    $pc_pagamento = 0; $pc_aditivo = 0; $pc_restante = 0;
                    $valor_restante = $vl_total_empenhado - $vl_total_pagamento;
                    if( $contrato['vl_global'] > 0 ) {
                        $pc_pagamento   = ($vl_total_pagamento * 100) / $valor_total;
                        $pc_aditivo     = ($vl_total_aditivo * 100) / $contrato['vl_global'];
                        $pc_apostilamento = ($vl_total_apostilamento * 100) / $contrato['vl_global'];
                        $pc_restante    = ($valor_restante * 100) / $valor_total;
                    }
                ?>

                <dl class="dl-horizontal">
                    <?php if($this->Modulo->isCamposContrato('tp_valor') == true) { ?>
                        <?php if( $this->Modulo->getNomeTipoPagamentoRF() == true ) { ?>
                                <dt width="120">Tipo de <?php echo __('Pagamento'); ?>:</dt>
                        <?php }else{ ?>
                                <dt width="120">Tipo de Valor:</dt>
                        <?php } ?>
                                <dd class="right"><?php echo $this->Print->tpValor($contrato['tp_valor']); ?></dd>

                    <?php } ?>
                                <dt>Vlr da Conta (Mensal):</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda( $valor_total ); ?></dd>
                                <dt><?php echo __('Vlr da Conta (Com Redução)'); ?>:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda( $valor_total ); ?></dd>
                                <dt><?php echo __('Percentual de Redução'); ?>:</dt>
                                <dd class="right"><?php echo $contrato['pct_reducao']; ?>%</dd>
<!--                                <dd class="right">--><?php //echo $this->Formatacao->moeda($contrato['vl_global']); ?><!--</dd>-->
                            <?php if($this->Modulo->isEmpenho()) { ?>
                                <dt><?php echo __('Total Empenhado', true); ?>:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($vl_total_empenhado); ?></dd>
                            <?php } ?>
                            <?php if($this->Modulo->isPagamento()) { ?>
                                <dt>Total Honorário Recebido:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($vl_total_pagamento); ?></dd>
                            <?php } ?>
                            <?php if($this->Modulo->isCamposContrato('nu_qtd_parcelas')) { ?>
                                <dt>Número de parcelas:</dt>
                                <dd class="right"><?php echo $contrato['nu_qtd_parcelas']; ?></dd>
                            <?php } ?>
                                <!-- <dt><?php __('Valor mensal (R$)'); ?>:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_mensal']); ?></dd>-->
                            <?php if($this->Modulo->isCamposContrato('vl_inicial')) { ?>
                                <!-- <dt>Valor inicial:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_inicial']); ?></dd>-->
                            <?php } ?>
                            <?php if(false) : ?>
                                <dt>Total aditivos:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($vl_total_aditivo); ?></dd>
                            <?php endif; ?>
                            <?php if(Configure::read('App.config.resource.available.apostilamento')) : ?>
                                <dt>Total apostilamentos:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($vl_total_apostilamento); ?></dd>
                            <?php endif; ?>
                            <?php if($this->Modulo->isPagamento()) { ?>
                                <!-- <dt><?php __('Valor Restante') ?>:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda( $valor_restante ); ?></dd>-->
                            <?php } ?>
                            <?php if($this->Modulo->isPagamento()) { ?>
                                <!-- <dt>Total Liquidado:</dt>
                                <dd class="right"><?php echo $this->Formatacao->moeda($vl_total_liquidado); ?></dd>-->
                            <?php } ?>
                            <br /><dt><b style="color:red"><?php __('Redução Mensal'); ?></b></dt><br />
                            <dt>Valor Reducall:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_reducall']); ?></dd>
                            <dt>Valor Araras:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_araras']); ?></dd>
                            <dt>Valor Parceiro:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_parceiro']); ?></dd>
                            <br /><dt><b style="color:red">Ressarcimento</b></dt><br />
                            <dt>Valor do Ressarcimento:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_ressarcimento']); ?></dd>
                            <dt>Ressarcimento Reducall:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_ressarcimento_reducall']); ?></dd>
                            <dt>Ressarcimento Araras:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_ressarcimento_araras']); ?></dd>
                            <dt>Ressarcimento Parceiro:</dt>
                            <dd class="right"><?php echo $this->Formatacao->moeda($contrato['vl_ressarcimento_parceiro']); ?></dd>
                    </dl>
