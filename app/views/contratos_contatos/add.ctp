<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="produtos form">
<?php echo $this->Form->create('ContratosContato', array('url' => "/contratos_contatos/add/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Contato - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos_contatos', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Contatos">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Novo Contato</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('id');
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                              
                              echo $this->Form->input('nome', array('label' => __('Nome', true)) );
                              echo $this->Form->input('cargo', array('label' => __('Cargo', true)) );
                              echo $this->Form->input('departamento', array('label' => __('Departamento', true)) );
                              echo $this->Form->input('email', array('label' => __('Email', true)) );
                              
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('telefone_um', array('class' => 'input-xlarge', 'label' => 'Telefone Um', 'mask' => '(99) 9999-9999?9'));
                              echo $this->Form->input('telefone_dois', array('class' => 'input-xlarge', 'label' => 'Telefone Dois', 'mask' => '(99) 9999-9999?9'));
                              echo $this->Form->input('endereco', array('label'=>'Endereço','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("500","#charsLeft2")', 
                                'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'));
                              echo '<br />';
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Contato"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>