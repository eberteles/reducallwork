<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="contatos index">
<!-- h2>< ?php __('ContratosContatos');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbContratosContato">
	<tr>
		<th><?php __('Nome');?></th>
		<th><?php __('Cargo');?></th>
		<th><?php __('Departamento');?></th>
		<th><?php __('Email');?></th>
                <th><?php __('Telefones');?></th>
                <th><?php __('Endereço');?></th>

		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($contatos as $contato): 
	?>
	<tr>
		<td><?php echo $contato['ContratosContato']['nome']; ?>&nbsp;</td>
		<td><?php echo $contato['ContratosContato']['cargo']; ?>&nbsp;</td>
		<td><?php echo $contato['ContratosContato']['departamento']; ?>&nbsp;</td>
		<td><?php echo $contato['ContratosContato']['email']; ?>&nbsp;</td>
		<td><?php echo $contato['ContratosContato']['telefone_um']; ?> - <?php echo $contato['ContratosContato']['telefone_dois']; ?></td>
		<td><?php echo $contato['ContratosContato']['endereco']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $contato['ContratosContato']['id']; ?> 

                    <div class="btn-group acoes">
                        <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'contratos_contatos/index' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos_contatos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos_contatos', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a> 
          </div>
        </div>

</div>
<?php } ?>


<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();
    
<?php echo $this->Html->scriptEnd() ?>