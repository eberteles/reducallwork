<div class="contratosFiscais form">
    <?php echo $this->Form->create('ContratoFiscal', array('url' => "/contratos_fiscais/add/$coContrato") );?>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um <?php __('Fiscal'); ?> - <b>Campos com * são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'contratos_fiscais', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Executores">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-header widget-header-small"><h4>Novo <?php __('Fiscal'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">

                            <?php
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                            echo $this->Form->input('co_usuario_fiscal', array('label' => __('Fiscal', true), 'class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $fiscais,
                                'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Adicionar ao Grupo Auxiliar" id="AbrirNovoAuxiliar" href="#view_novo_auxiliar" data-toggle="modal"><i class="blue icon-edit bigger-150"></i></a>' ));

                            echo '<br>';
                            echo $this->Form->input('co_fiscais_tipos', array('label' => __('Tipo Fiscal', true), 'class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'id' => 'selecao', 'options' => $Tiposfiscais));


                            echo $this->Form->input('nu_portaria_nomeacao', array('label' => 'Número da portaria de nomeação'));
                            echo $this->Form->input('nu_boletim_especial', array('label' => 'Número do boletim especial'));
                            ?>
                        </div>
                        <div class="span6">
                            <?php
                            echo $this->Form->input('dt_inicio', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data de Nomeação do Fiscal', 'type'=>'text'));
                            echo $this->Form->input('dt_fim', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data de Exoneração do Fiscal', 'type'=>'text'));
                            echo $this->Form->input('dt_boletim_especial', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data do boletim especial', 'type'=>'text'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Fiscal"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <div id="view_novo_auxiliar" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo Grupo Auxiliar</h3>
        </div>
        <div class="modal-body-iframe" id="add_auxiliar">
        </div>
    </div>
</div>

<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<script type="text/javascript">
    $(document).ready(function() {
        // TODO: colocar isso num helper
        $("#ContratoFiscalNuPortariaNomeacao").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $("#ContratoFiscalNuBoletimEspecial").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });

    $("#AbrirNovoAuxiliar").bind('click', function(e) {
        var url_md = "<?php echo $this->base; ?>/grupo_auxiliares/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
            },
            success:function(result){
                $('#add_auxiliar').html("");
                $('#add_auxiliar').append(result);
            }
        })
    });

    function atzComboFiscais(co_grupo_auxiliar) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'usuarios', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#ContratoFiscalCoUsuarioFiscal").html(options);
            $("#ContratoFiscalCoUsuarioFiscal option[value=" + co_grupo_auxiliar + "]").attr("selected", true);
            $("#ContratoFiscalCoUsuarioFiscal").trigger("chosen:updated");
        });
    }
</script>
