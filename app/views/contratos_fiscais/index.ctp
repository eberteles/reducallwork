<?php $usuario = $this->Session->read('usuario'); ?>
<div class="contratosFiscais index">
    <!-- h2>< ?php __('Fiscais');?></h2 -->
    <input type="hidden" id="coContrato" value="<?php echo $coContrato; ?>"/>
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped" id="tbFiscal">
        <tr>
            <th><?php __('CPF'); ?> / Matrícula</th>
            <th><?php __('Nome'); ?></th>
            <th><?php __('Email'); ?></th>
            <th>Tipo de Fiscal</th>
            <th>Data de Nomeação do Fiscal</th>
            <th>Data de Exoneração do Fiscal</th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php foreach ($contratosFiscais as $contratoFiscal): ?>
            <tr>
                <td><?php echo $this->Print->cpf($contratoFiscal['Usuario']['nu_cpf']); ?>&nbsp;</td>
                <td><?php echo $contratoFiscal['Usuario']['ds_nome']; ?>&nbsp;</td>
                <td><?php echo $contratoFiscal['Usuario']['ds_email']; ?>&nbsp;</td>
                <td><?php echo $contratoFiscal['FiscalTipo']["no_fiscais_tipos"]; ?>&nbsp;</td>
                <td><?php echo $contratoFiscal['ContratoFiscal']['dt_inicio']; ?>&nbsp;</td>
                <td><?php echo $contratoFiscal['ContratoFiscal']['dt_fim']; ?>&nbsp;</td>
                <td class="actions"><?php $id = $contratoFiscal['ContratoFiscal']['co_contrato_fiscal']; ?>
                    <div class="btn-group acoes">
                        <a id="<?php echo $id; ?>" class="v_historico btn" href="#view_historico" data-toggle="modal"><i
                                    class="icon-film alert-tooltip" title="<?php __('Histórico do Fiscal'); ?>"
                                    style="display: inline-block;"></i></a>
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i
                                    class="silk-icon-folder-table-btn alert-tooltip"
                                    title="Anexar Documento de Nomeação" style="display: inline-block;"></i></a>
                        <?php echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'contratos_fiscais/')) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?>
    </p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
<?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos_fiscais/add')): ?>
    <div class="actions">
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">Selecione uma Ação
                <!--span class="required" title="Required">*</span--></p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'contratos_fiscais', 'action' => 'add', $coContrato)); ?>"
                   class="btn btn-small btn-primary">Adicionar</a>
            </div>
        </div>
    </div>
<?php endif; ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental - Anexar Documento de Nomeação</h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo">
    </div>
</div>

<div id="view_historico" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php __('Fiscalização - Histórico do Fiscal'); ?></h3>
    </div>
    <div class="modal-body-iframe" id="fis_hist">
    </div>
</div>

<script type="text/javascript">

    $('.alert-tooltip').tooltip();

    $('#tbFiscal tr td a.v_anexo').click(function () {
        var url_an = '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $coContrato)); ?>/fiscal/' + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_an,
            data: {},
            success: function (result) {
                $('#fis_anexo').html("");
                $('#fis_anexo').append(result);
            }
        })
    });

    $('#tbFiscal tr td a.v_historico').click(function () {
        var url_ht = '<?php echo $this->Html->url(array('controller' => 'contratos_fiscais', 'action' => 'list_historico')); ?>/' + $(this).attr('id') + "/" + $("#coContrato").val();
        $.ajax({
            type: "POST",
            url: url_ht,
            data: {},
            success: function (result) {
                $('#fis_hist').html("");
                $('#fis_hist').append(result);
            }
        })
    });
</script>
