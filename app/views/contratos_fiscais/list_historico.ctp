<div class="fiscais index">

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th><?php __('Data');?></th>
		<th><?php __('CPF');?></th>
		<th><?php __('Nome');?></th>
		<th><?php __('Email');?></th>
		<th><?php __('Operação');?></th>
		<th><?php __('Funcionalidade');?></th>
	</tr>
	<?php if( count($fiscais) ): ?>
		<?php
		$i = 0;
		foreach ($fiscais as $fiscal):
	        $class = null;
			if ($i++ % 2 == 0):
				$class = ' class="altrow"';
	        endif;
		?>
		<tr <?php echo $class;?>>
			<td><?php echo $fiscal['Log']['dt_log']; ?>&nbsp;</td>
			<td><?php echo $this->Print->cpf( $fiscal['Usuario']['nu_cpf'] ); ?>&nbsp;</td>
			<td><?php echo $fiscal['Usuario']['ds_nome']; ?>&nbsp;</td>
			<td><?php echo $fiscal['Usuario']['ds_email']; ?>&nbsp;</td>
			<td><?php echo isset($tpAcao[$fiscal['Log']['tp_acao']]) ? $tpAcao[$fiscal['Log']['tp_acao']] : " - "; ?>&nbsp;</td>	
			<td><?php echo $fiscal['Log']['ds_tabela']; ?>&nbsp;</td>
		</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan="5">Nenhum registro encontrado.</td>
		</tr>
	<?php endif; ?>
</table>


</div>