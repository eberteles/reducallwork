<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="CronogramaFinanceiroDesembolso form">
<?php echo $this->Form->create('CronogramaFinanceiroDesembolso', array('url' => "/cronograma_financeiro_desembolso/edit/$id/$coContrato"));?>

	<div class="acoes-formulario-top clearfix">
		<p class="requiredLegend pull-left">
			Preencha os campos abaixo para adicionar um Cronograma
			Financeiro/Desembolso - <b>Campos com * são obrigatórios.</b>
		</p>
		<div class="pull-right btn-group">
			<a href="cronograma_financeiro_desembolso/index/"
				<?php echo $coContrato; ?>"
				class="btn btn-small btn-primary"
				title="Listar Empenhos">Listagem</a>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widget-header widget-header-small">
			<h4>Novo Cronograma Financeiro/Desembolso</h4>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row-fluid">
					<div class="span4">
						<dl class="dl-horizontal">
                      <?php
                    echo $this->Form->hidden('co_cronograma_financeiro_desembolso');
                    echo $this->Form->hidden('co_contrato', array(
                        'value' => $coContrato
                    ));
                    
                    echo $this->Form->input('descricao', array(
                        'class' => 'input-xlarge',
                        'label' => 'Descrição',
                        'type' => 'textarea',
                        'rows' => '4'
                    ));
                    echo $this->Form->input('mes', array(
                        'label' => 'Mês',
                        'type' => 'select',
                        'empty' => 'Selecione...',
                        'options' => $meses
                    ));
                    
                    ?>
                       </dl>
					</div>
					<div class="span4">
						<dl class="dl-horizontal">                  
                      <?php
                    echo $this->Form->input('dt_inicio', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy',
                        'mask' => '99/99/9999',
                        'class' => 'input-small',
                        'label' => 'Data Inícial',
                        'type' => 'text'
                    ));
                    
                    echo $this->Form->input('dt_data_final', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy',
                        'mask' => '99/99/9999',
                        'class' => 'input-small',
                        'label' => 'Data Final',
                        'type' => 'text'
                    ));
                    
                    ?>
                       </dl>
					</div>
					<div class="span4">
						<dl class="dl-horizontal">                  
                      <?php
                    echo $this->Form->input('ds_valor', array(
                        'label' => 'Valor (R$)'
                    ));

                    if($this->Modulo->cronogramaFinanceiroPercentual){
                        echo $this->Form->input('percentual', array(
                            'label' => 'Percentual (%)',
                            'mask' => '999,99',
                        ));
                    }
                    ?>
                       </dl>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<div class="btn-group">
			<button rel="tooltip" type="submit"
				class="btn btn-small btn-primary bt-pesquisar"
				title="Salvar Garantia">Salvar</button>
			<button rel="tooltip" type="reset" title="Limpar dados preenchidos"
				class="btn btn-small" id="Limpar">Limpar</button>
			<button rel="tooltip" title="Voltar" class="btn btn-small"
				id="Voltar">Voltar</button>
		</div>
	</div>

</div>
<?php echo $this->Html->scriptStart()?>

$(document).ready(function() {
    $("#CronogramaFinanceiroDesembolsoDsValor").maskMoney({thousands:'.', decimal:','});
});

<?php echo $this->Html->scriptEnd() ?>