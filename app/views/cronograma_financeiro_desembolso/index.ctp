<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="ordens index">
	<!-- h2>< ?php __('CronogramaFinanceiroDesembolso');?></h2-->

	<table cellpadding="0" cellspacing="0" style="background-color: white"
		class="table table-hover table-bordered table-striped" id="tbEmpenho">
		<tr>
			<th><?php echo $this->Paginator->sort('Descricao', 'descricao');?></th>
			<th><?php echo $this->Paginator->sort('Mês', 'mes');?></th>
			<th><?php echo $this->Paginator->sort('Data Início', 'dt_inicio');?></th>
			<th><?php echo $this->Paginator->sort('Data Final', 'dt_data_final');?></th>
            <?php
                if($this->Modulo->cronogramaFinanceiroPercentual) {
                    echo "<th>";
                    echo $this->Paginator->sort('Percentual', 'percentual');
                    echo "<th>";
                }
            ?>
			<th><?php echo $this->Paginator->sort('Valor', 'ds_valor');?></th>
			<th class="actions"><?php __('Ações');?></th>
		</tr>
	<?php
$i = 0;
foreach ($cronograma_financeiro_desembolso as $cronograma) :
    $class = null;
    if ($i ++ % 2 == 0) {
        $class = ' class="altrow"';
    }
    ?>
	<tr <?php echo $class;?>>
			<td><?php echo $cronograma['CronogramaFinanceiroDesembolso']['descricao']; ?>&nbsp;</td>
			<td><?php echo $this->Formatacao->pegaMes($cronograma['CronogramaFinanceiroDesembolso']['mes']); ?>&nbsp;</td>
			<td><?php echo $cronograma['CronogramaFinanceiroDesembolso']['dt_inicio']; ?>&nbsp;</td>
			<td><?php echo $cronograma['CronogramaFinanceiroDesembolso']['dt_data_final']; ?>&nbsp;</td>
            <?php
                if($this->Modulo->cronogramaFinanceiroPercentual) {
                    echo "<td>";
                    echo $cronograma['CronogramaFinanceiroDesembolso']['percentual'] . "%";
                    echo "<td>";
                }
            ?>&nbsp;
			<td><?php echo $this->Formatacao->moeda($cronograma['CronogramaFinanceiroDesembolso']['ds_valor']); ?>&nbsp;</td>
			<td class="actions"><?php $id = $cronograma['CronogramaFinanceiroDesembolso']['co_cronograma_financeiro_desembolso']; ?>
                    <div class="btn-group acoes">	
                        <?php echo $this->Html->link('<i class="icon-pencil"></i>', array( 'action' => 'edit', $id, $coContrato), array('escape' => false, 'class' => 'btn')); ?>
                        <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array( 'action' => 'delete', $id, $coContrato), array('escape' => false,'class' => 'btn btn-danger'), sprintf(__('Tem certeza de que deseja excluir esse registro ?', true), $id)); ?>
                    </div></td>
		</tr>
	<?php endforeach; ?>
</table>
	<p><?php
echo $this->Paginator->counter(array(
    'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

	<div class="pagination">
		<ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
	</div>

</div>

<?php
if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add')) {
    ?>
<div class="actions">
	<div class="acoes-formulario-top clearfix">
		<p class="requiredLegend pull-left">
			Selecione uma Ação
			<!--span class="required" title="Required">*</span-->
		</p>
		<div class="pull-right btn-group">
			<a
				href="<?php echo $this->Html->url(array('controller' => 'cronograma_financeiro_desembolso', 'action' => 'add', $coContrato)); ?>"
				class="btn btn-small btn-primary">Adicionar</a>
		</div>
	</div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
		<h3 id="myModalLabel">Suporte Documental - Anexar Empenho</h3>
	</div>
	<div class="modal-body-iframe" id="fis_anexo"></div>
</div>

<?php echo $this->Html->scriptStart()?>
        
    $('.alert-tooltip').tooltip();

    $('#tbEmpenho tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/empenho/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });
    
<?php echo $this->Html->scriptEnd() ?>