    <?php
    $usuario = $this->Session->read('usuario');
    // echo debug($usuario);die;
    echo $this->Html->script('tablesorter/jquery-latest');
    echo $this->Html->script('highcharts/js/highcharts.js');
    echo $this->Html->script('highcharts/js/highcharts-more.js');
    echo $this->Html->script('highcharts/js/highcharts-3d.js');
    echo $this->Html->script('highcharts/js/modules/exporting');
    echo $this->Html->script('highcharts/js/modules/solid-gauge');
    echo $this->Html->script('charts-governador');
    echo $this->Html->script('tablesorter/jquery.tablesorter.js');
    echo $this->Html->script('datatable/js/jquery.dataTables');
    echo $this->Html->script('datatable/js/shCore');
    ?>
    <br />
    <div class="row-fluid page-header position-relative">
        <div class="span6">
            <h1>
                <?php echo __('Dashboard'); ?>
            </h1>
        </div>
        <div class="span6">
            <?php echo $this->Form->create('Dashboard', array('url' => "/dashboard", 'class' => 'form-inline pull-right'));?>
            <div class="control-group inline">
                <div class="controls">
                    <span class="help-inline">Tipo de Dashboard</span>
                    <?php echo $this->Form->select('tipo_dashboard', $opcoesDashboard, null, array('empty' => 'Selecione...')); ?>
                    <button class="btn btn-small btn-primary" type="submit">Trocar</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>

<div class="row-fluid">

	<!-- GRÁFICO DE BARRAS -->
	<div class="span12 widget-container-span">
		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5>
					<i class="icon-bar-chart"></i><font><font class=""> Financeiro </font></font>
				</h5>
				<span class="widget-toolbar"> <a href="#"> <i class="icon-move"></i>
				</a> <a href="#" data-action="collapse"> <i class="icon-chevron-up"></i>
				</a> <a href="#" data-action="close"> <i class="icon-remove"></i>
				</a>
				</span>
			</div>
			<div class="widget-body">
				<div class="widget-main" style="height: 435px;">
					<div class="tabbable">
						<!-- Only required for left/right tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">À Pagar</a></li>
							<li><a href="#tab2" data-toggle="tab">Pago</a></li>
							<li><a href="#tab3" data-toggle="tab">Trimestre</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1">
								<div id="bars" style="height: 300px; width: 100%;"></div>
								<h3 id="ValorTotalAPagar"></h3>
							</div>
							<div class="tab-pane" id="tab2">
								<div id="bars2" style="height: 300px; width: 94%;"></div>
								<h3 id="ValorTotalPago"></h3>
							</div>
							<div class="tab-pane" id="tab3">
								<div id="bars3" style="height: 300px; width: 94%;"></div>
								<h3 id="vta"></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row-fluid">

	<!-- GRÁFICO DE PIZZA -->
	<div class="span4 widget-container-span">

		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5>
					<i class="icon-dashboard"></i><font><font class=""> <?php __('Contratos'); ?>
							Ativos </font></font>
				</h5>

				<span class="widget-toolbar"> <a href="#"> <i class="icon-move"></i>
				</a> <a href="#" data-action="collapse"> <i class="icon-chevron-up"></i>
				</a> <a href="#" data-action="close"> <i class="icon-remove"></i>
				</a>
				</span>
			</div>

			<div class="widget-body">
				<div class="widget-main" style="height: 505px;">
					<div class="row-fluid">
						<div id="container"></div>
					</div>
					<h3 id="totalDeContratos"></h3>
				</div>
			</div>
		</div>
	</div>

	<div class="span4 widget-container-span">

		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5>
					<i class="icon-bar-chart"></i><font><font class="">
                                        <?php echo __('Pagamento'); ?> em Atraso
                                </font></font>
				</h5>

				<span class="widget-toolbar"> <a href="#"> <i class="icon-move"></i>
				</a> <a href="#" data-action="collapse"> <i class="icon-chevron-up"></i>
				</a> <a href="#" data-action="close"> <i class="icon-remove"></i>
				</a>
				</span>

			</div>

			<div class="widget-body">
				<div class="widget-main" style="cursor: pointer; height: 504px;">
					<div id="pagamento-atraso"></div>
				</div>
<!-- 				<h3 id="totalDeAtrasosENaoAtraso"></h3> -->
			</div>
		</div>
	</div>

	<div class="span4 widget-container-span">

		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5>
					<i class="icon-bar-chart"></i><font><font class="">
                                        <?php __('Contratos'); ?> Vigentes com Pendências
                                </font></font>
				</h5>

				<span class="widget-toolbar"> <a href="#"> <i class="icon-move"></i>
				</a> <a href="#" data-action="collapse"> <i class="icon-chevron-up"></i>
				</a> <a href="#" data-action="close"> <i class="icon-remove"></i>
				</a>
				</span>

			</div>

			<div class="widget-body">
				<div class="widget-main" style="cursor: pointer; height: 504px;">
					<div id="contratos-pendentes"></div>
				</div>
				<!-- 				<h3 id="totalPendencia"></h3> -->
			</div>
		</div>
	</div>
</div>

<div class="row-fluid">
	<!-- GRÁFICO DE GAUGE | VENCIMENTO DOS CONTRATOS -->
	<div class="span12 widget-container-span">

		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5>
					<i class="icon-dashboard"></i><font><font class="">
                                        Vencimento dos <?php __('Contratos'); ?>
                                </font></font>
				</h5>
				<span class="widget-toolbar"> <a href="#"> <i class="icon-move"></i>
				</a> <a href="#" data-action="collapse"> <i class="icon-chevron-up"></i>
				</a> <a href="#" data-action="close"> <i class="icon-remove"></i>
				</a>
				</span>
			</div>

			<div class="widget-body" style="height: 152px;">
				<div class="widget-main">
					<div class="row-fluid">

						<div class="span3">
							<div id="gauge"></div>
						</div>
						<div class="span3">
							<div id="gauge2"></div>
						</div>

						<div class="span3">
							<div id="gauge3"></div>
						</div>
						<div class="span3">
							<div id="gauge4"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 2º NÍvel -->
<div id="view_imp_relatorio" class="modal hide fade maior" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="display: none; top: 13%; bottom: 13%;">
	<div class="modal-header">
		<p class="pull-right">
			<!--  <a id="imprimir_pdf" href="#imprimir_pdf" class="btn btn-small btn-primary" ><i class="icon-print"></i> Imprimir</a> -->
			<a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i>
				Fechar</a>
		</p>
		<h3 id="tituloAbaImpressoa"></h3>
	</div>
	<div id="content-innerCt"></div>
	<div id="tabela" style="width: 97.5%; height: 30%; margin: 0 auto;"></div>
</div>
<!-- End 2º NÍvel -->
<script type="text/javascript">

            $(document).ready(function() {
                $("#contratosAtivosPorSetor").hide();
                contratosAtivosPorSetor("<?=$this->base; ?>", <?=$contratosAtivosPorSetor; ?>);
                contratos_A_pagar("<?=$this->base; ?>", <?=$contratosAPagarValor; ?>, <?=$contratosAPagarPercentual; ?>);
                contratosPagos("<?=$this->base; ?>", <?=$contratosPagos_total_valores; ?>, <?=$contratosPagos_total_percentual; ?>);
                trimestre(<?=$contratosPagosPorTrimetre; ?>, <?=$contratosApagarPorTrimetre; ?>);
                vencimentosContratos1(<?=$contratosAVencer30; ?>);
                vencimentosContratos2(<?=$contratosAVencer60; ?>);
                vencimentosContratos3(<?=$contratosAVencer90; ?>);
                vencimentosContratos4(<?=$contratosAVencer120; ?>);
                contratosComPagamentoAtrasado("<?=$this->base; ?>", <?=$contratosComPagamentoAtrasado; ?>);
                contratosComPendencias(<?=$contratosComPendencias; ?>);
            });

</script>
