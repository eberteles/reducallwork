<?php
$usuario = $this->Session->read('usuario');

echo $this->Html->script('jquery.slimscroll.min');
echo $this->Html->script('flot/jquery.flot.min');
echo $this->Html->script('flot/jquery.flot.pie.min');
echo $this->Html->script('flot/jquery.flot.resize.min');
?>

<!--[if lt IE 9]><?php echo $this->Html->css('excanvas');?><![endif]-->
<?php
    echo $this->Html->css('../bower_components/jqPlot/src/jquery.jqplot');
    echo $this->Html->script('../bower_components/jqPlot/src/jquery.jqplot');
    echo $this->Html->script('../bower_components/jqPlot/src/plugins/jqplot.meterGaugeRenderer');
?>

        <br />
        <div class="row-fluid page-header position-relative">
            <div class="span6">
                <h1>
                    <?php echo __('Consolidado'); ?>
                </h1>
            </div>
            <div class="span6">
                <?php echo $this->Form->create('Dashboard', array('url' => "/dashboard", 'class' => 'form-inline pull-right'));?>
                <div class="control-group inline">
                    <div class="controls">
                        <span class="help-inline">Tipo de Dashboard</span>
                        <?php echo $this->Form->select('tipo_dashboard', $opcoesDashboard, null, array('empty' => 'Selecione...')); ?>
                        <button class="btn btn-small btn-primary" type="submit">Trocar</button>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>

        <div class="row-fluid">

            <div class="span4 widget-container-span">

                <div class="widget-box">
                        <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                        <i class="icon-dashboard"></i><font><font class="">
                                        Vencimento dos <?php __('Contratos'); ?>
                                </font></font></h5>

                            <span class="widget-toolbar">
                                    <a href="#">
                                            <i class="icon-move"></i>
                                    </a>

                                    <a href="#" data-action="collapse">
                                            <i class="icon-chevron-up"></i>
                                    </a>

                                    <a href="#" data-action="close">
                                            <i class="icon-remove"></i>
                                    </a>
                            </span>
                        </div>

                        <div class="widget-body">
                             <div class="widget-main" style="padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; height: 249px;">
                                 <div class="row-fluid">
                                     <?php foreach ($vencimentosDashboard as $vencimentoDashboard): ?>
                                         <div id="chart<?php echo $vencimentoDashboard; ?>" class="plot jqplot-target" style="cursor:pointer; width: 190px; height: 120px; position: relative; float:left;"></div>
                                     <?php endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="span4 widget-container-span">

                <div class="widget-box">
                        <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                        <i class="icon-bar-chart"></i><font><font class="">
                                        <?php __('Contratos'); ?> com <?php echo __('Pagamento'); ?> em Atraso
                                </font></font></h5>

                            <span class="widget-toolbar">
                                    <a href="#">
                                            <i class="icon-move"></i>
                                    </a>

                                    <a href="#" data-action="collapse">
                                            <i class="icon-chevron-up"></i>
                                    </a>

                                    <a href="#" data-action="close">
                                            <i class="icon-remove"></i>
                                    </a>
                            </span>

                        </div>

                        <div class="widget-body"><br>
                            <div class="widget-main" style="height: 207px;">
                                <div id="contrato-pgt-atraso" style="cursor:pointer;"></div>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="span4 widget-container-span">

                <div class="widget-box">
                        <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                        <i class="icon-bar-chart"></i><font><font class="">
                                        <?php __('Contratos'); ?> Vigentes com Pendências
                                </font></font></h5>

                            <span class="widget-toolbar">
                                    <a href="#">
                                            <i class="icon-move"></i>
                                    </a>

                                    <a href="#" data-action="collapse">
                                            <i class="icon-chevron-up"></i>
                                    </a>

                                    <a href="#" data-action="close">
                                            <i class="icon-remove"></i>
                                    </a>
                            </span>

                        </div>

                        <div class="widget-body"><br>
                            <div class="widget-main" style="height: 207px; cursor:pointer;">
                                 <div id="contratos-pendencia"></div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

    <?php
        $divisao_padrao = 8;
        if(!$this->Modulo->isExecucao() && !$this->Modulo->meus_processos_dashboard) {
            $divisao_padrao = 12;
        }
        if($this->Modulo->meus_processos_dashboard) {
            $divisao_padrao = 6;
        }
    ?>
        <div class="row-fluid">

            <div class="span<?php echo $divisao_padrao; ?> widget-container-span">

                <div class="widget-box">
                        <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                        <i class="icon-list orange"></i><font><font class="">
                                        Meus <?php __('Registros'); ?>
                                </font></font></h5>

                                <div class="widget-toolbar">
                                    <a href="#">
                                            <i class="icon-move"></i>
                                    </a>

                                    <a href="#" data-action="collapse">
                                            <i class="icon-chevron-up"></i>
                                    </a>

                                    <a href="#" data-action="close">
                                            <i class="icon-remove"></i>
                                    </a>
                                </div>

<!--                                <div class="widget-toolbar">
                                    <button type="button" class="btn btn-default" id="btAplicaFiltros">Aplicar Filtros</button>
                                </div>-->

                                <div class="widget-toolbar no-border">
                                    &nbsp;
                                        <button class="btn dropdown-toggle" data-toggle="dropdown" id="bntFiltroPapel">
                                                Listar Todos os <?php __('Contratos'); ?>
                                                <i class="icon-angle-down icon-on-right"></i>
                                        </button>

                                        <ul class="dropdown-menu pull-right dropdown-caret dropdown-close">
                                                <li>
                                                        <a href="#lt_contratos" id="listaTodos">Listar Todos os <?php __('Contratos'); ?></a>
                                                </li>

                                                <li>
                                                        <a href="#lt_contratos" id="listaGestor">Listar <?php __('Contratos'); ?> que sou Gestor</a>
                                                </li>

                                                <li>
                                                        <a href="#lt_contratos" id="listaFiscal">Listar <?php __('Contratos'); ?> que sou Fiscal</a>
                                                </li>

                                        </ul>
                                </div>

                                <div class="widget-toolbar no-border">
                                    &nbsp;
                                        <button class="btn dropdown-toggle" data-toggle="dropdown" id="bntFiltro">
                                                Contratos Ativos
                                                <i class="icon-angle-down icon-on-right"></i>
                                        </button>

                                        <ul class="dropdown-menu pull-right dropdown-caret dropdown-close">
                                                <li>
                                                        <a href="#lt_contratos" id="listAtivos"><?php __('Contratos'); ?> Ativos</a>
                                                </li>

                                                <li>
                                                        <a href="#lt_contratos" id="listPa"><?php __('Contratos'); ?> com <?php echo __('Pagamento'); ?> em Atraso</a>
                                                </li>

                                                <li>
                                                        <a href="#lt_contratos" id="listPd"><?php __('Contratos'); ?> Vigentes com Pendências</a>
                                                </li>

                                                <?php foreach ($vencimentosDashboard as $vencimentoDashboard): ?>
                                                    <li><a href="#lt_contratos" id="listV<?php echo $vencimentoDashboard; ?>"><?php __('Contratos'); ?> Encerrando nos Próximos <?php echo $vencimentoDashboard; ?> dias</a></li>
                                                <?php endforeach; ?>
                                        </ul>
                                </div>

                        </div>

                        <div class="widget-body">
                             <div class="widget-main no-padding">
                                 <div class="dialogs" id="id_list_contratos">
                                 </div>
                            </div>
                        </div>
                    </div>
            </div>
            <?php if($this->Modulo->meus_processos_dashboard): ?>
                <div class="span6 widget-container-span">
                    <div class="widget-box">
                        <div class="widget-header widget-header-flat widget-header-small">
                            <h5>
                                <i class="icon-list orange"></i>
                                <font>
                                    <font class="">Meus Processos
                                    </font>
                                </font>
                            </h5>
                            <div class="widget-toolbar">
                                <a href="#">
                                    <i class="icon-move"></i>
                                </a>
                                <a href="#" data-action="collapse">
                                    <i class="icon-chevron-up"></i>
                                </a>
                                <a href="#" data-action="close">
                                    <i class="icon-remove"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                             <div class="widget-main no-padding">
                                 <div class="dialogs" id="id_list_processos">
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
            <?php if($this->Modulo->isExecucao()): ?>
                <div class="span4 widget-container-span">
                    <div class="widget-box">
                            <div class="widget-header widget-header-flat widget-header-small">
                                    <h5>
                                            <i class="icon-tasks"></i><font><font class="">
                                            Minhas <?php
                                                echo __('Atividades');
                                            ?>
                                    </font></font></h5>

                                <span class="widget-toolbar">
                                        <a href="#">
                                                <i class="icon-move"></i>
                                        </a>

                                        <a href="#" data-action="collapse">
                                                <i class="icon-chevron-up"></i>
                                        </a>

                                        <a href="#" data-action="close">
                                                <i class="icon-remove"></i>
                                        </a>
                                </span>

                                <div class="widget-toolbar no-border">
                                    &nbsp;
                                    <button class="btn btn-mini bigger dropdown-toggle" data-toggle="dropdown" id="bntFiltroAtv">
                                        Filtro
                                        <i class="icon-angle-down icon-on-right"></i>
                                    </button>

                                    <ul class="dropdown-menu pull-right dropdown-caret dropdown-close">
                                        <li>
                                            <a href="#lt_atividades" id="listTodasAtv">Todas</a>
                                        </li>

                                        <li>
                                            <a href="#lt_atividades" id="listPendentesAtv">Pendentes</a>
                                        </li>

                                        <li>
                                            <a href="#lt_atividades" id="listConcluidasAtv">Concluídas</a>
                                        </li>

                                    </ul>
                                </div>

                            </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <div class="dialogs" id="id_list_atividades">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div id="view_dominio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-header">
        <button id="atualizar_lista_processo" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="tituloAbaDominio"></h3>
      </div>
      <div class="modal-body" id="list_dominio">
      </div>
    </div>

<script type="text/javascript">
    var tipoFiltro = 'default';
    var papelFiltro = '';

    function atualizaListAtividades(tipo){
        var url_list = "./atividades/list_atividades/" + tipo;
        $.ajax({
            type: "POST",
            url: url_list,
            success:function(result){
                $('#id_list_atividades').html("");
                $('#id_list_atividades').append(result);
            }
        });
    }
    atualizaListAtividades('td');

    function mudaTxtBtnAtv(texto) {
        $('#bntFiltroAtv').html("");
        $('#bntFiltroAtv').append(texto + " <i class='icon-angle-down icon-on-right'></i>");
    }

    $('#listTodasAtv').click(function(){
        mudaTxtBtnAtv( $(this).text() );
        atualizaListAtividades('td');
    });

    $('#listPendentesAtv').click(function(){
        mudaTxtBtnAtv( $(this).text() );
        atualizaListAtividades('pd');
    });

    $('#listConcluidasAtv').click(function(){
        mudaTxtBtnAtv( $(this).text() );
        atualizaListAtividades('cc');
    });




    $('#atualizar_lista_processo').click(function() {
        atualizaListProcessos();
    });

        function atualizaListContratos(tipo, papel){
            var url_list = "./contratos/list_contratos/" + tipo + "/" + papelFiltro;
            $.ajax({
                type:"POST",
                url:url_list,
                success:function(result){
                    $('#id_list_contratos').html("");
                    $('#id_list_contratos').append(result);
                },
                error: function (err) {
                     console.log('erro');
                }
            });
        }
        atualizaListContratos(tipoFiltro, papelFiltro);

        function mudaTxtBtn(texto) {
            $('#bntFiltro').html("");
            $('#bntFiltro').append(texto + " <i class='icon-angle-down icon-on-right'></i>");
        }

        function mudaTxtBtnPapel(texto) {
            $('#bntFiltroPapel').html("");
            $('#bntFiltroPapel').append(texto + " <i class='icon-angle-down icon-on-right'></i>");
        }

        $('#btAplicaFiltros').click(function(){
            atualizaListContratos(tipoFiltro, papelFiltro);
        });
        
        $('#listaTodos').click(function(){
            mudaTxtBtnPapel( $(this).text() );
            papelFiltro = '';
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#listaGestor').click(function(){
            mudaTxtBtnPapel( $(this).text() );
            papelFiltro = 'gestor';
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#listaFiscal').click(function(){
            mudaTxtBtnPapel( $(this).text() );
            papelFiltro = 'fiscal';
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#listAtivos').click(function(){
            mudaTxtBtn( $(this).text() );
            tipoFiltro = '';
          //  atualizaListContratos('');
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#listPa').click(function(){
            mudaTxtBtn( $(this).text() );
            tipoFiltro = 'pa';
          //  atualizaListContratos('pa');
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#contrato-pgt-atraso').click(function(){
            mudaTxtBtn( $('#listPa').text() );
            tipoFiltro = 'pa';
          //  atualizaListContratos('pa');
        });

        $('#listPd').click(function(){
            mudaTxtBtn( $(this).text() );
            tipoFiltro = 'pd';
          //  atualizaListContratos('pd');
          atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#contratos-pendencia').click(function(){
            mudaTxtBtn( $('#listPd').text() );
            tipoFiltro = 'pd';
          //  atualizaListContratos('pd');
        });

        <?php foreach ($vencimentosDashboard as $vencimentoDashboard): ?>
        $('#listV<?php echo $vencimentoDashboard; ?>').click(function(){
            mudaTxtBtn( $(this).text());
            tipoFiltro = 'v<?php echo $vencimentoDashboard; ?>';
            atualizaListContratos(tipoFiltro, papelFiltro);
        });

        $('#chart<?php echo $vencimentoDashboard; ?>').click(function(){
            mudaTxtBtn( $('#listV<?php echo $vencimentoDashboard; ?>').text() );
            tipoFiltro = 'v<?php echo $vencimentoDashboard; ?>';
        });
        <?php endforeach; ?>

    <?php
        if($this->Modulo->meus_processos_dashboard == true) {
    ?>
        function atualizaListProcessos(){
                <?php echo $this->JqueryEngine->request( "/contratos/list_processos/list_fase", array( 'update' => '#id_list_processos' ) ); ?>
        }
        atualizaListProcessos();
    <?php
        }
    ?>

    $(document).ready(function(){
        s30 = [<?php echo $this->Print->precisao($pctVencendo30, 0); ?>];
        n30 = [<?php echo $this->Print->precisao($ttVencendo30, 0); ?>];
        s60 = [<?php echo $this->Print->precisao($pctVencendo60, 0); ?>];
        n60 = [<?php echo $this->Print->precisao($ttVencendo60, 0); ?>];
        s90 = [<?php echo $this->Print->precisao($pctVencendo90, 0); ?>];
        n90 = [<?php echo $this->Print->precisao($ttVencendo90, 0); ?>];
        s120 = [<?php echo $this->Print->precisao($pctVencendo120, 0); ?>];
        n120 = [<?php echo $this->Print->precisao($ttVencendo120, 0); ?>];
        s180 = [<?php echo $this->Print->precisao($pctVencendo180, 0); ?>];
        n180 = [<?php echo $this->Print->precisao($ttVencendo180, 0); ?>];

        <?php foreach ($vencimentosDashboard as $vencimentoDashboard): ?>
            plot<?php echo $vencimentoDashboard; ?> = $.jqplot('chart<?php echo $vencimentoDashboard; ?>', [s<?php echo $vencimentoDashboard; ?>], {
                seriesDefaults: {
                    renderer: $.jqplot.MeterGaugeRenderer,
                    rendererOptions: {
                        min: 0,
                        max: 50,
                        intervals:[15, 35, 50],
                        intervalColors:['#93b75f', '#E7E658', '#cc6666'],
                        label: '<a>' + n<?php echo $vencimentoDashboard; ?> + ' <?php __('Contratos'); ?> / <?php echo $vencimentoDashboard; ?> dias </a>',
                        labelPosition: 'bottom'
                    }
                }
            });
        <?php endforeach; ?>
    });

    jQuery(function($) {

            $('.dialogs,.comments').slimScroll({
                        height: '250px'
            });

                // Portlets (boxes)
            $('.widget-container-span').sortable({
                connectWith: '.widget-container-span',
                        items:'> .widget-box',
                        opacity:0.8,
                        revert:true,
                        forceHelperSize:true,
                        placeholder: 'widget-placeholder',
                        forcePlaceholderSize:true,
                        tolerance:'pointer'
            });

              /** Contratos com Pendências */
              var placeholderCtrPend = $('#contratos-pendencia').css({'width':'90%' , 'min-height':'140px'});
              var dataCtrPend        = [
                    { label: "<?php echo $this->Formatacao->porcentagem($pctCtPendencia, 0); ?> - Com Pendências (<?php echo $ttCtPendencia; ?>)",  data: <?php echo $this->Print->precisao($pctCtPendencia, 0);?>, color: "#faa732"},
                    { label: "<?php echo $this->Formatacao->porcentagem(100 - $pctCtPendencia, 0); ?> - Sem Pendências (<?php echo $ttGrPendencia - $ttCtPendencia; ?>)",  data: <?php echo 100 - $pctCtPendencia;?>, color: "#dadada"}
              ];
              drawPieChart(placeholderCtrPend, dataCtrPend, "", 0.5);

              /** Pagamentos em Atraso */
              var placeholderPgtAtr = $('#contrato-pgt-atraso').css({'width':'90%' , 'min-height':'140px'});
              var dataPgtAtr = [
                    { label: "<?php echo $this->Formatacao->porcentagem(100 - $pctPgAtraso, 0); ?> - Em dia",  data: <?php echo 100 - $pctPgAtraso;?>, color: "#68BC31"},
                    { label: "<?php echo $this->Formatacao->porcentagem($pctPgAtraso, 0); ?> - Em atraso",  data: <?php echo $this->Print->precisao($pctPgAtraso,0);?>, color: "#DA5430"}
              ];
              drawPieChart(placeholderPgtAtr, dataPgtAtr, "se");

              function drawPieChart(placeholder, data, position, innerRadius) {
                      $.plot(placeholder, data, {
                            series: {
                                    pie: {
                                            innerRadius: innerRadius || 0,
                                            show: true,
                                            tilt:0.8,
                                            highlight: {
                                                    opacity: 0.25
                                            },
                                            stroke: {
                                                    color: '#fff',
                                                    width: 2
                                            },
                                            startAngle: 2
                                    }
                            },
                            legend: {
                                    show: true,
                                    position: position || "ne",
                                    labelBoxBorderColor: null,
                                    margin:[-30,15]
                            }
                            ,
                            grid: {
                                    hoverable: true,
                                    clickable: true
                            }
                     });
             }

    });

</script>
