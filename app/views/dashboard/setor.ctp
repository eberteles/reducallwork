<?php $usuario = $this->Session->read ('usuario'); ?>

<?php
    echo $this->Html->script( 'jquery.slimscroll.min' );
    echo $this->Html->script( 'flot/jquery.flot.min' );
    echo $this->Html->script( 'flot/jquery.flot.pie.min' );
    echo $this->Html->script( 'flot/jquery.flot.resize.min' );
    
    echo $this->Html->script( 'jquery-1.6.4.min' );
?>
        <?php echo $this->Html->scriptStart() ?>
            var jq164   = jQuery.noConflict(true);
        <?php echo $this->Html->scriptEnd() ?>
            
<!--[if lt IE 9]><?php echo $this->Html->css( 'excanvas' );?><![endif]-->
<?php
    echo $this->Html->css( 'jquery.jqplot' );
    echo $this->Html->script( 'jquery.jqplot' );
    echo $this->Html->script( 'jqplot.meterGaugeRenderer' );
?>

<!--     <div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-mini btn-warning ace-settings-btn" id="ace-settings-btn">
                    <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                Relação com os possíveis gráficos<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
                Teste<br>
            </div>
    </div>-->

    <div class="page-header position-relative">
            <h1>
                    Dashboard
            </h1>
    </div>
    <div class="row-fluid">
        
        <div class="span8 widget-container-span">

            <div class="widget-box">
                    <div class="widget-header widget-header-flat widget-header-small">
                            <h5>
                                    <i class="icon-list orange"></i><font><font class="">
                                    Caixa de Entrada - <?php echo$usuario['Setor']['ds_setor']; ?>
                            </font></font></h5>
                        
                            <div class="widget-toolbar">
                                <a href="#">
                                        <i class="icon-move"></i>
                                </a>

                                <a href="#" data-action="collapse">
                                        <i class="icon-chevron-up"></i>
                                </a>

                                <a href="#" data-action="close">
                                        <i class="icon-remove"></i>
                                </a>
                            </div>
                        
<!--                            <div class="widget-toolbar no-border">
                                &nbsp;
                                    <button class="btn btn-mini bigger btn-yellow dropdown-toggle" data-toggle="dropdown">
                                            Filtro
                                            <i class="icon-angle-down icon-on-right"></i>
                                    </button>

                                    <ul class="dropdown-menu pull-right dropdown-caret dropdown-close">
                                            <li>
                                                    <a href="#">Contratos Ativos</a>
                                            </li>

                                            <li>
                                                    <a href="#">Contratos com Pagamento em Atraso</a>
                                            </li>

                                            <li>
                                                    <a href="#">Contratos com Pendências</a>
                                            </li>
                                            
                                            <li>
                                                    <a href="#">Contratos Encerrando nos Próximos 30 dias</a>
                                            </li>
                                            
                                            <li>
                                                    <a href="#">Contratos Encerrando nos Próximos 60 dias</a>
                                            </li>
                                            
                                            <li>
                                                    <a href="#">Contratos Encerrando nos Próximos 90 dias</a>
                                            </li>

                                    </ul>
                            </div>-->
                        
                    </div>

                    <div class="widget-body">
                         <div class="widget-main no-padding">
                             <div class="list_processos" id="id_list_contratos">
                             </div>
                        </div>
                    </div>
                </div>
        </div>
        
        <div class="span4 widget-container-span">

            <div class="widget-box">
                    <div class="widget-header widget-header-flat widget-header-small">
                            <h5>
                                    <i class="icon-bar-chart"></i><font><font class="">
                               <?php __('Processos'); ?> com Pendências
                            </font></font></h5>
                        
                        <span class="widget-toolbar">
                                <a href="#">
                                        <i class="icon-move"></i>
                                </a>

                                <a href="#" data-action="collapse">
                                        <i class="icon-chevron-up"></i>
                                </a>

                                <a href="#" data-action="close">
                                        <i class="icon-remove"></i>
                                </a>
                        </span>
                        
                    </div>

                    <div class="widget-body"><br>
                        <div class="widget-main" style="height: 207px;">
                             <div id="contratos-pendencia"></div>
                        </div>
                    </div>
                </div>
        </div>
        
        <div class="span4 widget-container-span">

            <div class="widget-box">
                    <div class="widget-header widget-header-flat widget-header-small">
                            <h5>
                                    <i class="icon-tasks"></i><font><font class="">
                                    Minhas Atividades
                            </font></font></h5>
                        
                        <span class="widget-toolbar">
                                <a href="#">
                                        <i class="icon-move"></i>
                                </a>

                                <a href="#" data-action="collapse">
                                        <i class="icon-chevron-up"></i>
                                </a>

                                <a href="#" data-action="close">
                                        <i class="icon-remove"></i>
                                </a>
                        </span>
                        
                    </div>

                    <div class="widget-body">
                         <div class="widget-main no-padding">
                           <div class="list_atividades">
	<?php
        $i = 0;
	foreach ($atividades as $atividade):
            $i++;
            $link_contrato  = $this->Html->url(array('controller' => 'contratos', 'action' => 'detalha', $atividade['Atividade']['co_contrato'])) . '/atividade#collapseExec';
            $html_atividade = '
                                <div class="clearfix">
                                        <span class="pull-left">' . $atividade['Atividade']['ds_atividade'] . '</span>
                                        <span class="pull-right">' . $atividade['Atividade']['pc_executado'] . '%</span>
                                </div>

                                <div class="progress progress-mini progress-' . $this->Print->alertAtividadeDashboard($atividade) . ' progress-striped active">
                                        <div style="width:' . $atividade['Atividade']['pc_executado'] . '%" class="bar"></div>
                                </div>';
            if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ||
                 $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') ||
                 $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha')) {
                echo $this->Html->link($html_atividade, $link_contrato, array('escape' => false));
            } else {
                echo $html_atividade;
            }
        endforeach;
        if($i == 0){
            echo 'Não existem registros a serem exibidos.';
        }else {echo '<BR>';} 
        ?>
                           </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    

<?php echo $this->Html->scriptStart() ?>

    function atualizaListContratos(){
            <?php echo $this->JqueryEngine->request( $this->Html->url(array('controller' => 'contratos', 'action' => 'list_processos')), array( 'update' => '#id_list_contratos' ) ); ?>
    }

jQuery(function($) {

        atualizaListContratos();

        $('.list_atividades').slimScroll({
                    height: '250px'
        });
        
        $('.list_processos').slimScroll({
                    height: '542px'
        });
        
            // Portlets (boxes)
        $('.widget-container-span').sortable({
            connectWith: '.widget-container-span',
                    items:'> .widget-box',
                    opacity:0.8,
                    revert:true,
                    forceHelperSize:true,
                    placeholder: 'widget-placeholder',
                    forcePlaceholderSize:true,
                    tolerance:'pointer'
        });
        
          /** Contratos com Pendências */
          var placeholderCtrPend = $('#contratos-pendencia').css({'width':'90%' , 'min-height':'140px'});
          var dataCtrPend        = [
                { label: "<?php echo $this->Formatacao->porcentagem($pctCtPendencia, 0); ?> - Com Pendências (<?php echo $ttCtPendencia; ?>)",  data: <?php echo $this->Print->precisao($pctCtPendencia, 0);?>, color: "#faa732"},
                { label: "<?php echo $this->Formatacao->porcentagem(100 - $pctCtPendencia, 0); ?> - Sem Pendências (<?php echo $ttGrPendencia - $ttCtPendencia; ?>)",  data: <?php echo 100 - $pctCtPendencia;?>, color: "#dadada"}
          ]
          drawPieChart(placeholderCtrPend, dataCtrPend, "", 0.5);
        
          /** Pagamentos em Atraso */
          var placeholderPgtAtr = $('#contrato-pgt-atraso').css({'width':'90%' , 'min-height':'140px'});
          var dataPgtAtr = [
                { label: "<?php echo $this->Formatacao->porcentagem(100 - $pctPgAtraso, 0); ?> - Em dia",  data: <?php echo 100 - $pctPgAtraso;?>, color: "#68BC31"},
                { label: "<?php echo $this->Formatacao->porcentagem($pctPgAtraso, 0); ?> - Em atraso",  data: <?php echo $this->Print->precisao($pctPgAtraso,0);?>, color: "#DA5430"}
          ]
          drawPieChart(placeholderPgtAtr, dataPgtAtr, "se");
          
          function drawPieChart(placeholder, data, position, innerRadius) {
                  $.plot(placeholder, data, {
                        series: {
                                pie: {
                                        innerRadius: innerRadius || 0,
                                        show: true,
                                        tilt:0.8,
                                        highlight: {
                                                opacity: 0.25
                                        },
                                        stroke: {
                                                color: '#fff',
                                                width: 2
                                        },
                                        startAngle: 2
                                }
                        },
                        legend: {
                                show: true,
                                position: position || "ne", 
                                labelBoxBorderColor: null,
                                margin:[-30,15]
                        }
                        ,
                        grid: {
                                hoverable: true,
                                clickable: true
                        }
                 })
         }
                         
});

<?php echo $this->Html->scriptEnd() ?>