<?php echo $this->Form->create('DspCamposAuxiliares', array('url' => array('controller' => 'dsp_campos_auxiliares', 'action' => 'add'))); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Campos Auxiliares'); ?></h1></div>
    <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Cadastro de Campos Auxiliares'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span3">
                        <div class="controls">
                            <?php
                                echo $this->Form->input('tipo_campo', array('class' => 'input-xlarge', 'label' => 'Tipo de Campo', 'type' => 'select', 'options' => array( 1 => 'Unidade de Lotação', 2 => 'Cargo', 3 => 'Função', 4 => 'Meio de Transporte', 5 => 'Categoria de Passagem' )));
                            ?>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="controls">
                            <?php
                                echo $this->Form->input('descricao_campo', array('class' => 'input-xlarge', 'label' => 'Descrição do Campo'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button> 
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

