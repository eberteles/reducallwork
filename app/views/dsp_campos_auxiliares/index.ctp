<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Campos Auxiliares de Despesas com Diárias e Passagens'); ?></h1></div>
            <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'dsp_campos_auxiliares/add') ) { ?>
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><span class="required" title="Required">Informe a opção em Tipo de Campo, para visualizar a lista desejada.</span> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'dsp_campos_auxiliares', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Despesa"><i class="icon-plus icon-white"></i> Novo <?php __('Campo Auxiliar'); ?></a>
          </div>
        </div>
      <?php } ?>

        <?php echo $this->Form->create('CamposAuxiliares', array('url' => array('controller' => 'dsp_campos_auxiliares', 'action' => 'index')));?>

        <div class="row-fluid">
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('tipo_campo', array('class' => 'input-xlarge','label' => __('Tipo de Campo', true), 'id' => 'TipoCampo', 'type' => 'select', 'options' => array( 1 => 'Unidade de Lotação', 2 => 'Cargo', 3 => 'Função', 4 => 'Meio de Transporte', 5 => 'Categoria de Passagem' ) ));
                    ?>
                </div>
            </div>
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('descricao_campo', array('class' => 'input-xlarge','label' => __('Descrição do Campo', true)));
                    ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
                <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
        <th>Código</th>
		<th>Tipo de Campo</th>
		<th>Descrição do Campo</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
<?php
    $i = 0;
    $counter = 1;
    foreach ($campos_auxiliares as $campo):
        $class = null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
        ?>
        <tr <?php echo $class;?>>
                <?php if( $banco == 'unidade' ){ ?>
                    <td><?php echo $counter; ?></td>
                    <td>Unidade de Lotação</td>
                    <td><?php echo $campo['DspUnidadeLotacao']['nome_unidade'] ?></td>
                    <td class="actions" >
                        <div class="btn-group acoes" >
                            <?php
                                echo $this->element(
                                    'actions3', array(
                                        'id' => $campo['DspUnidadeLotacao']['co_unidade'],
                                        'controller' => 'dsp_unidade_lotacao',
                                        'class' => 'btn',
                                        'local_acao' => 'dsp_campos_auxiliares/index'
                                    )
                                )
                            ?>
                        </div>
                    </td>
                <?php }elseif( $banco == 'cargo' ){ ?>
                    <td><?php echo $counter; ?></td>
                    <td>Cargo</td>
                    <td><?php echo $campo['DspCargos']['nome_cargo'] ?></td>
                    <td class="actions" >
                        <div class="btn-group acoes" >
                            <?php
                            echo $this->element(
                                'actions3', array(
                                    'id' => $campo['DspCargos']['co_cargo'],
                                    'controller' => 'dsp_cargos',
                                    'class' => 'btn',
                                    'local_acao' => 'dsp_campos_auxiliares/index'
                                )
                            )
                            ?>
                        </div>
                    </td>
                <?php }elseif( $banco == 'funcao' ){ ?>
                    <td><?php echo $counter; ?></td>
                    <td>Função</td>
                    <td><?php echo $campo['DspFuncao']['nome_funcao'] ?></td>
                    <td class="actions" >
                        <div class="btn-group acoes" >
                            <?php
                            echo $this->element(
                                'actions3', array(
                                    'id' => $campo['DspFuncao']['co_funcao'],
                                    'controller' => 'dsp_funcao',
                                    'class' => 'btn',
                                    'local_acao' => 'dsp_campos_auxiliares/index'
                                )
                            )
                            ?>
                        </div>
                    </td>
                <?php }elseif( $banco == 'meio_transporte' ){ ?>
                    <td><?php echo $counter; ?></td>
                    <td>Meio de Transporte</td>
                    <td><?php echo $campo['DspMeioTransporte']['nome_meio_transporte'] ?></td>
                    <td class="actions" >
                        <div class="btn-group acoes" >
                            <?php
                            echo $this->element(
                                'actions3', array(
                                    'id' => $campo['DspMeioTransporte']['co_meio_transporte'],
                                    'controller' => 'dsp_meio_transporte',
                                    'class' => 'btn',
                                    'local_acao' => 'dsp_campos_auxiliares/index'
                                )
                            )
                            ?>
                        </div>
                    </td>
                <?php }elseif( $banco == 'categoria_passagem' ){ ?>
                    <td><?php echo $counter; ?></td>
                    <td>Categoria de Passagem</td>
                    <td><?php echo $campo['DspCategoriaPassagem']['nome_categoria_passagem'] ?></td>
                    <td class="actions" >
                        <div class="btn-group acoes" >
                            <?php
                            echo $this->element(
                                'actions3', array(
                                    'id' => $campo['DspCategoriaPassagem']['co_categoria_passagem'],
                                    'controller' => 'dsp_categoria_passagem',
                                    'class' => 'btn',
                                    'local_acao' => 'dsp_campos_auxiliares/index'
                                )
                            )
                            ?>
                        </div>
                    </td>
                <?php } ?>
        </tr>
        <?php $counter++; ?>
    <?php endforeach; ?>
</table>
<p>
    <?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>


