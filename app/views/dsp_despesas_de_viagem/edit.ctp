<?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' );

    //debug($this->data);die;
?>

<?php echo $this->Form->create('DspDespesasDeViagem', array('url' => array('controller' => 'dsp_despesas_de_viagem', 'action' => 'edit', $id), 'id' => 'DespesasForm') );?>
	
	<div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Edição de Despesas com Diárias e Passagens'); ?></h1></div>
        <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>

        <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Campos com * são obrigatórios</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'index')); ?>" class="btn btn-small btn-primary" title="Listar <?php __('Despesas'); ?>">Listagem</a>
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
          </div>
        </div>
        
          <div class="row-fluid">
                        
            <div class="span5">
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>
                        <?php __('Processo'); ?>
                    </h4></div>
             
                <div class="widget-body">
                    <div class="widget-main">
                      <div class="row-fluid">
                        <div class="span6">

                        <dl>
			                <?php
                              echo $this->Form->input('numero_processo', array('class' => 'input-xlarge', 'label'=>__('Número do Processo', true), 'value' => $this->data['DspDespesasDeViagem']['numero_processo'], 'readonly' => true ));
                              echo $this->Form->input('motivo_viagem', array('class' => 'input-xlarge', 'label'=>'Motivo da Viagem', 'type' => 'textarea', 'style' => 'margin: 0px 0px 10px; width: 420px; height: 120px; max-width: 420px; max-height: 120px;', 'value' => $this->data['DspDespesasDeViagem']['motivo_viagem']));
			                ?>
                        </dl>

                     </div>
                       </div>
                    </div>
                </div>
              </div>
            </div>

              <div class="span7">
                  <div class="widget-box">
                      <div class="widget-header widget-header-small"><h4>
                              <?php __('Funcionário'); ?>
                          </h4></div>

                      <div class="widget-body">
                          <div class="widget-main">
                              <div class="row-fluid">
                                  <div class="span6">

                                      <dl>
                                          <?php
                                              echo $this->Form->input('co_funcionario', array('label' => __('Funcionário', true), 'class' => 'input-xxlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $funcionarios, 'disabled' => 'disabled' ));
                                              echo $this->Form->input('cod_cargo', array('class' => 'input-xlarge ', 'id' => 'combo_cargos', 'type' => 'select', 'options' => array_map('strtoupper', $cargos), 'empty' => 'SELECIONE...', 'label'=>'Cargo', 'after' => $this->Print->getBtnEditCombo('Novo ' .  __('Cargo', true), 'AbrirNovoCargo', '#view_novo_cargo', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_for_i')), 'value' => $this->data['DspDespesasDeViagem']['cod_cargo']));
                                              echo $this->Form->input('cod_unidade_lotacao', array('class' => 'input-xlarge ', 'id' => 'combo_unidade', 'type' => 'select', 'options' => array_map('strtoupper', $unidade), 'empty' => 'SELECIONE...', 'label'=>'Unidade de Lotação', 'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Unidade de Lotação', true), 'AbrirNovaUnidade', '#view_nova_unidade', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_for_i')), 'value' => $this->data['DspDespesasDeViagem']['cod_unidade_lotacao']));
                                          ?>
                                      </dl>


                                  </div>
                                  <div class="span6">
                                      <dl class="dl-horizontal">
                                          <br><br><br>
                                          <?php
                                            echo $this->Form->input('cod_funcao', array('class' => 'input-xlarge ', 'id' => 'combo_funcao', 'type' => 'select', 'options' => array_map('strtoupper', $funcao), 'empty' => 'SELECIONE...', 'label'=>'Função', 'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Função', true), 'AbrirNovaFuncao', '#view_nova_funcao', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_for_i')), 'value' => $this->data['DspDespesasDeViagem']['cod_funcao']));
                                          ?>
                                      </dl>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
        </div>
            
        <div class="row-fluid">

            <div class="span7 ">
                <div class="widget-box">
                    <div class="widget-header widget-header-small"><h4>
                            <?php __('Dados da Viagem'); ?>
                        </h4></div>

                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row-fluid">

                                <div class="span6">
                                    <dl class="dl-horizontal">
                                        <?php
                                        echo $this->Form->input('origem', array('class' => 'input-xlarge', 'label'=>'Cidade Origem'));
                                        ?>
                                        <div id="OriExt">
                                            <?php
                                            echo $this->Form->input('origem_exterior', array('type' => 'checkbox', 'id' => 'OrigemExterior', 'label' => 'A origem é do exterior', 'onchange' => "showFieldOrigem()"));
                                            ?>
                                        </div>
                                        <div id="OriUf">
                                            <?php
                                            echo $this->Form->input('uf_origem', array('class' => 'input-xlarge', 'style' => 'width: 80%;', 'id' => 'UfOrigem', 'label'=>'UF de Origem', 'type' => 'select', 'empty' => 'SELECIONE...', 'options' => array_map('strtoupper', $ufs), 'value' => $this->data['DspDespesasDeViagem']['uf_origem']));
                                            ?>
                                        </div>
                                        <div id="OriPais">
                                            <?php
                                            echo $this->Form->input('pais_origem', array('class' => 'input-xlarge', 'id' => 'PaisOrigem', 'label'=>'País de Origem', 'type' => 'text', 'value' => $this->data['DspDespesasDeViagem']['pais_origem'] ));
                                            ?>
                                        </div>
                                        <?php
                                        echo $this->Form->input('destino', array('class' => 'input-xlarge', 'label'=>'Cidade de Destino'));
                                        ?>
                                        <div id="DstExt">
                                            <?php
                                            echo $this->Form->input('destino_exterior', array('type' => 'checkbox', 'id' => 'DestinoExterior', 'label' => 'O destino é do exterior', 'onchange' => 'showFieldDestino()'));
                                            ?>
                                        </div>
                                        <div id="DstUf">
                                            <?php
                                            echo $this->Form->input('uf_destino', array('class' => 'input-xlarge', 'style' => 'width: 80%;', 'id' => 'UfDestino', 'label'=>'UF de Destino', 'type' => 'select', 'empty' => 'SELECIONE...', 'options' => array_map('strtoupper', $ufs), 'value' => $this->data['DspDespesasDeViagem']['uf_destino']));
                                            ?>
                                        </div>
                                        <div id="DstPais">
                                            <?php
                                            echo $this->Form->input('pais_destino', array('class' => 'input-xlarge', 'id' => 'PaisDestino', 'label'=>'País de Destino', 'type' => 'text', 'value' => $this->data['DspDespesasDeViagem']['pais_destino']));
                                            ?>
                                        </div>
                                    </dl>
                                </div>
                                <div class="span6">
                                    <dl class="dl-horizontal">
                                        <div class="row-fluid">
                                            <div class="span5">
                                                <?php
                                                echo $this->Form->input('dt_inicio', array(
                                                        'before' => '<div class="input-append date datetimepicker">',
                                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                                        'class' => 'input-small','label' => 'Início (Período)', 'type'=>'text', 'id' => 'data_inicio')
                                                );
                                                ?>
                                            </div>
                                            <div class="span6">
                                                <?php
                                                echo $this->Form->input('dt_final', array(
                                                    'before' => '<div class="input-append date datetimepicker" onselect="verificaDatas()">',
                                                    'after' => '<span class="add-on" onselect="verificaDatas()"><i data-date-icon="icon-calendar"></i></span></div>',
                                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                                    'class' => 'input-small','label' => 'Fim (Período)', 'type'=>'text', 'id' => 'data_final', 'onselect' => 'verificaDatas()'));
                                                ?>
                                                <span class="msg_date"><b style="color: red;">A data final deve ser maior que a data inicial</b></span>
                                            </div>
                                        </div>
                                        <?php
                                        echo $this->Form->input('cod_meio_transporte', array('class' => 'input-xlarge', 'id' => 'combo_meio_transporte', 'type' => 'select', 'empty' => 'SELECIONE...', 'options' => array_map('strtoupper', $meio_transporte), 'label'=>'Meio de Transporte', 'after' => $this->Print->getBtnEditCombo('Novo ' .  __('Meio de Transporte', true), 'AbrirNovoMeioTransporte', '#view_novo_meio_transporte', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_drp_i'))));
                                        echo $this->Form->input('cod_categoria_passagem', array('class' => 'input-xlarge', 'id' => 'combo_categoria_passagem', 'type' => 'select', 'empty' => 'SELECIONE...', 'options' => array_map('strtoupper', $categoria_passagem), 'label'=>'Categoria da Passagem', 'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Categoria de Passagem', true), 'AbrirNovaCategoriaPassagem', '#view_nova_categoria_passagem', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_drp_i'))));
                                        ?>
                                    </dl>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                  
           <div class="span5 ">
              <div class="widget-box">
                <div class="widget-header widget-header-small"><h4><?php __('Valores'); ?></h4></div>
                
                <div class="widget-body">
                    <div class="widget-main">
                        <?php
                            echo $this->Form->input('valor_passagem', array('class' => 'input-xlarge', 'id' => 'ValorPassagem', 'label'=>'Valor da Passagem (R$)', 'onblur' => 'updateTotal()' , 'value' => formatMoney($this->data['DspDespesasDeViagem']['valor_passagem'])));
                            echo $this->Form->input('numero_diarias', array('class' => 'input-xlarge', 'id' => 'NumeroDiarias', 'label'=>'Número de Diárias', 'value' => $this->data['DspDespesasDeViagem']['numero_diarias'], 'mask' => '9,?9', 'onblur' => 'changeMask()'));
                            echo $this->Form->input('valor_total_diarias', array('class' => 'input-xlarge', 'id' => 'ValorTotalDiarias', 'label'=>'Valor Total das Diárias (R$)', 'onblur' => 'updateTotal()', 'value' => formatMoney($this->data['DspDespesasDeViagem']['valor_total_diarias'])));
                            echo $this->Form->input('valor_total_viagem', array('class' => 'input-xlarge', 'id' => 'ValorTotalViagem', 'label'=>'Valor Total da Viagem (R$)', 'readonly' => true , 'value' => formatMoney($this->data['DspDespesasDeViagem']['valor_total_viagem'])));
                        ?>
                    </div>
                </div>
              </div>
           </div>
         </div>

    <div id="view_novo_cargo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Cargo'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_cargo">
        </div>
    </div>

    <div id="view_nova_unidade" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Unidade de Lotação'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_unidade">
        </div>
    </div>
            
    <div id="view_nova_funcao" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Função'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_funcao">
        </div>
    </div>
		
    <div id="view_novo_meio_transporte" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Meio de Transporte'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_meio_transporte">
        </div>
    </div>
		
    <div id="view_nova_categoria_passagem" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Categoria de Passagem'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_categoria_passagem">
        </div>
    </div>
		
<div class="form-actions">
   <div class="btn-group">
      <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" onclick="verificaDatas()" title="Salvar processo" id="salvar_contrato">Salvar</button>
      <button rel="tooltip" type="reset" title="Resetar dados preechidos" class="btn btn-small" id="Reset"> Reset</button>
      <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
   </div>
</div>
	</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".msg_date").hide();
        updateTotal();
        changeMask();

        var ori_pais = "<?php echo $this->data['DspDespesasDeViagem']['pais_origem']; ?>";
        var ori_uf   = "<?php echo $this->data['DspDespesasDeViagem']['uf_origem']; ?>";
        var dst_pais = "<?php echo $this->data['DspDespesasDeViagem']['pais_destino']; ?>";
        var dst_uf   = "<?php echo $this->data['DspDespesasDeViagem']['uf_destino']; ?>";

        console.log("Pais de Origem: " + ori_pais + ", Uf de Origem: " + ori_uf + ", Pais de Destino: " + dst_pais + ", Uf de Destino: " + dst_uf);

        if( ori_pais != "" ){
            document.getElementById("OrigemExterior").checked = true;
            $("#OriPais").show();
            $("#OriUf").hide();
        }else{
            $("#OriPais").hide();
            $("#OriUf").show();
        }
        if( dst_pais != "" ){
            document.getElementById("DestinoExterior").checked = true;
            $("#DstPais").show();
            $("#DstUf").hide();
        }else{
            $("#DstPais").hide();
            $("#DstUf").show();
        }
    });

    function showFieldOrigem()
    {
        if( document.getElementById('OrigemExterior').checked ){
            $("#UfOrigem").val("");
            $("#OriUf").hide();
            $("#OriPais").show();
        }else{
            $("#PaisOrigem").val("");
            $("#OriPais").hide();
            $("#OriUf").show();
        }
    }

    function showFieldDestino()
    {
        if( document.getElementById('DestinoExterior').checked ){
            $("#UfDestino").val("");
            $("#DstUf").hide();
            $("#DstPais").show();
        }else{
            $("#PaisDestino").val("");
            $("#DstPais").hide();
            $("#DstUf").show();
        }
    }

    function changeMask(){
        var num = $("#NumeroDiarias").val();
        num = num.replace(',','.');
        num = parseFloat(num);
        if( num % 1 != 0 ){
            var numero = num.toString();
            numero = numero.replace('.',',');
            console.log(numero);
            $("#NumeroDiarias").val(numero);
        }else{
            num = num.toString();
            num = num + ',0';
            console.log(num);
            $("#NumeroDiarias").val(num);
        }
    }

    function loadComboCargos( cod_cargo ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_cargos', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione...</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#combo_cargos").html(options);
            $("#combo_cargos option[value=" + cod_cargo + "]").attr("selected", true);
            $("#combo_cargos").trigger("chosen:updated");
        });
    }

    function loadComboFuncao( cod_funcao ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_funcao', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione...</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#combo_funcao").html(options);
            $("#combo_funcao option[value=" + cod_funcao + "]").attr("selected", true);
            $("#combo_funcao").trigger("chosen:updated");
        });
    }

    function loadComboUnidade( cod_unidade ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_unidade_lotacao', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione...</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#combo_unidade").html(options);
            $("#combo_unidade option[value=" + cod_unidade + "]").attr("selected", true);
            $("#combo_unidade").trigger("chosen:updated");
        });
    }

    function loadComboMeioTransporte( cod_meio_transporte ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_meio_transporte', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione...</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#combo_meio_transporte").html(options);
            $("#combo_meio_transporte option[value=" + cod_meio_transporte + "]").attr("selected", true);
            $("#combo_meio_transporte").trigger("chosen:updated");
        });
    }

    function loadComboCategoriaPassagem( cod_categoria_passagem ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_categoria_passagem', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione...</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#combo_categoria_passagem").html(options);
            $("#combo_categoria_passagem option[value=" + cod_categoria_passagem + "]").attr("selected", true);
            $("#combo_categoria_passagem").trigger("chosen:updated");
        });
    }

    function updateTotal(){
        $("#ValorTotalViagem").value = '';
        var valorPassagem = $("#ValorPassagem").val();
        var valorTotalDiarias = $("#ValorTotalDiarias").val();

        valorPassagem = valorPassagem.replace(/[^\d,]+/g,'');
        valorPassagem = valorPassagem.replace(',','.');
        valorTotalDiarias = valorTotalDiarias.replace(/[^\d,]+/g,'');
        valorTotalDiarias = valorTotalDiarias.replace(',','.');

        if( (valorPassagem.length == 0 || valorPassagem == '0.00' || valorPassagem == '') && (valorTotalDiarias.length == 0 || valorTotalDiarias == '0.00' || valorTotalDiarias == '') )
        {
            var total = parseFloat(0);
        }else{
            if( valorPassagem == '' && valorTotalDiarias != '' )
            {
                var total = parseFloat(valorTotalDiarias);
            }else{
                if( valorTotalDiarias == '' && valorPassagem != '' ){
                    var total = parseFloat(valorPassagem);
                }else{
                    var total = parseFloat(valorPassagem) + parseFloat(valorTotalDiarias);
                }
            }
        }

        var valorTotal = 0.00;

        $.ajax({
            type:"POST",
            url: "<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'formatMoney')); ?>",
            data:{
                'total': total
            },
            success: function( data ){
                valorTotal = data.replace("\"","").replace("\"","");
                $("#ValorTotalViagem").val(valorTotal);
            }
        });
    }
</script>

<?php echo $this->Html->scriptStart() ?>

    function verificaDatas(){
        console.log("Teste");
        var inicio = $("#DataInicio").val();
        var final = $("#DataFinal").val();
        console.log(inicio);
        console.log(final);
        if( inicio > final ){
            console.log("Deveria cancelar o submit");
        }
    }

    $("#ValorPassagem").maskMoney({thousands:'.', decimal:','});
    $("#ValorTotalDiarias").maskMoney({thousands:'.', decimal:','});
    $("#ValorTotalViagem").maskMoney({thousands:'.', decimal:','});

    function atzComboCargos(cod_cargo) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_cargos', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">SELECIONE...</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#Cargos").html(options);
               $("#Cargos option[value=" + cod_cargo + "]").attr("selected", true);
               $("#Cargos").trigger("chosen:updated");
        });
    }
    
    function atzComboModalidade(co_contratacao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'contratacoes', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoContratacao").html(options);
               $("#ContratoCoContratacao option[value=" + co_contratacao + "]").attr("selected", true);
               $("#ContratoCoContratacao").trigger("chosen:updated");
        });
    }
    
    function atzComboTpContrato(co_modalidade) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoModalidade").html(options);
               $("#ContratoCoModalidade option[value=" + co_modalidade + "]").attr("selected", true);
               $("#ContratoCoModalidade").trigger("chosen:updated");
        });
    }
    
    function atzComboSituacao(co_situacao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'situacoes', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoSituacao").html(options);
               $("#ContratoCoSituacao option[value=" + co_situacao + "]").attr("selected", true);
               $("#ContratoCoSituacao").trigger("chosen:updated");
        });
    }
    
    function atzComboServico(co_servico) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'servicos', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoServico").html(options);
               $("#ContratoCoServico option[value=" + co_servico + "]").attr("selected", true);
               $("#ContratoCoServico").trigger("chosen:updated");
        });
    }
    
    function atzComboCategoria(co_categoria) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'categorias', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoCategoria").html(options);
               $("#ContratoCoCategoria option[value=" + co_categoria + "]").attr("selected", true);
               $("#ContratoCoCategoria").trigger("chosen:updated");
               atzComboSubCategoria(co_categoria, 0);
        });
    }

    function atzComboSubCategoria(co_categoria, co_sub_categoria) {
        verificaDeContrato(co_categoria);
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'subcategorias', 'action' => 'listar') )?>" + "/" + co_categoria, null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoSubcategoria").html(options);
               if(co_sub_categoria > 0) {
                    $("#ContratoCoSubcategoria option[value=" + co_sub_categoria + "]").attr("selected", true);
               }
               $("#ContratoCoSubcategoria").trigger("chosen:updated");
        });
    }

    $(function(){
    
        setMascaraCampo("#ContratoNuProcesso", "<?php echo FunctionsComponent::pegarFormato( 'processo' ); ?>");
        $("#Vl").maskMoney({thousands:'.', decimal:','});
        $("#V1").maskMoney({thousands:'.', decimal:','});
        $("#V2").maskMoney({thousands:'.', decimal:','});
        $("#V3").maskMoney({thousands:'.', decimal:','});
        $("#V4").maskMoney({thousands:'.', decimal:','});
    
        $("#ContratoVlInicial").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlMensal").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlGlobal").maskMoney({thousands:'.', decimal:','});
        $("#ContratoNuFimVigencia").maskMoney({precision:0, allowZero:false, thousands:''});
        
        $( "#ContratoNuProcesso" ).on('focusout', function(e) {
            var co_contrato = 0;
            if($(this).val() != '') {
                $.ajax({
                    type:"POST",
                    url: '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'find_processo') ); ?>',
                    data:{
                        "data[Contrato][nu_processo]":$(this).val()
                    },
                    success:function(result){

                        $('body').append(result);
                        if(result > 0) {
                            e.preventDefault();

                            $( "#dialog-processo" ).dialog({
                                    resizable: false,
                                    modal: true,
                                    title: 'Processo já cadastrado!',
                                    title_html: true,
                                    buttons: [
                                            {
                                                html: "<i class='icon-ok bigger-110'></i>&nbsp; Confirma",
                                                "class" : "btn btn-success btn-mini",
                                                click: function() {
                                                        $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'edit') )?>/' + result + '/nc');
                                                }
                                            }
                                            ,
                                            {
                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancela",
                                                "class" : "btn btn-mini",
                                                click: function() {
                                                        $( this ).dialog("close");
                                                }
                                            }
                                    ]
                            });
                        }
                    }
                })
            }
        });
  });



    $("#AbrirNovoCargo").bind('click', function(e) {
        var url_fn = "<?php echo $this->base; ?>/dsp_cargos/iframe/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                },
                success:function(result){
                    $('#add_cargo').html("");
                    $('#add_cargo').append(result);
                }
            })
    });
    $("#AbrirNovaUnidade").bind('click', function(e) {
        var url_md = "<?php echo $this->base; ?>/dsp_unidade_lotacao/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
                },
                success:function(result){
                    console.log(result);
                    $('#add_unidade').html("");
                    $('#add_unidade').append(result);
                }
            })
    });
    $("#AbrirNovaFuncao").bind('click', function(e) {
        var url_tm = "<?php echo $this->base; ?>/dsp_funcao/iframe/";
        $.ajax({
            type:"POST",
            url:url_tm,
            data:{
                },
                success:function(result){
                    $('#add_funcao').html("");
                    $('#add_funcao').append(result);
                }
            })
    });
    $("#AbrirNovoMeioTransporte").bind('click', function(e) {
        var url_sv = "<?php echo $this->base; ?>/dsp_meio_transporte/iframe/";
        $.ajax({
            type:"POST",
            url:url_sv,
            data:{
                },
                success:function(result){
                    $('#add_meio_transporte').html("");
                    $('#add_meio_transporte').append(result);
                }
            })
    });
    $("#AbrirNovaCategoriaPassagem").bind('click', function(e) {
        var url_st = "<?php echo $this->base; ?>/dsp_categoria_passagem/iframe/";
        $.ajax({
            type:"POST",
            url:url_st,
            data:{
                },
                success:function(result){
                    $('#add_categoria_passagem').html("");
                    $('#add_categoria_passagem').append(result);
                }
            })
    });
    
    $(document).ready(function(){
    	var _fill = {<? foreach ($_POST as $k=>$v) if (substr($k,0,6) == '_fill_') echo substr($k,6).':'.'"'.str_replace('"',"",$v).'",'; ?>};
    	for (id in _fill) $('#'+id).val(_fill[id]);
    });
    
<?php echo $this->Html->scriptEnd() ?>
