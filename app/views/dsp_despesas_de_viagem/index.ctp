<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Diárias e Passagens'); ?></h1></div>
            <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'drp_man_i') ) { ?>
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><span class="required" title="Required"></span> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'preImprimir')); ?>" data-toggle="modal" class="btn btn-small btn-info" title="Imprimir Relatório"><i class="icon-print icon-white"></i><?php __('Relatório Geral'); ?></a>
            <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Despesa"><i class="icon-plus icon-white"></i> Nova <?php __('Despesa'); ?></a>
          </div>
        </div>
      <?php } ?>

        <?php echo $this->Form->create('Despesa', array('url' => '/dsp_despesas_de_viagem/index'));?>

        <div class="row-fluid">
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('numero_processo', array('class' => 'input-xlarge','label' => __('Número do Processo', true),'maxLength'=>'50'));
                    ?>
                </div>
            </div>
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('nome_funcionario', array('class' => 'input-xlarge','label' => __('Nome do Funcionário', true)));
                    ?>
                </div>
            </div>
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('matricula_funcionario', array('class' => 'input-xlarge','label' => __('Matrícula do Funcionário', true)));
                    ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
                <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>

        <?php
            if( isset($despesas_viagem) ) {
        ?>
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th>Número do Processo</th>
		<th>Nome do Funcionário</th>
		<th>Matrícula do Funcionário</th>
		<th>Motivo</th>
		<th>Início(Período)</th>
		<th>Fim(Período)</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
        foreach ($despesas_viagem as $despesa):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }

            $dt_ini = $despesa['DspDespesasDeViagem']['dt_inicio'] ?: '---';
            $dt_fim = $despesa['DspDespesasDeViagem']['dt_final']  ?: '---';

            ?>
            <tr <?php echo $class;?>>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'mostrar' . '/' . $despesa['DspDespesasDeViagem']['co_despesas_viagem'])); ?>"><?php echo $despesa['DspDespesasDeViagem']['numero_processo']; ?>
                        &nbsp;</a></td>
                <td>
                    <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'mostrar' . '/' . $despesa['DspDespesasDeViagem']['co_despesas_viagem'])); ?>"><?php echo $despesa['GrupoAuxiliar']['ds_nome']; ?>
                        &nbsp;</a></td>
                <td><?php echo $despesa['GrupoAuxiliar']['nu_cpf']; ?>&nbsp;</td>
                <td><?php echo $despesa['DspDespesasDeViagem']['motivo_viagem']; ?>&nbsp;</td>
                <td><?php echo $dt_ini; ?>&nbsp;</td>
                <td><?php echo $dt_fim; ?>&nbsp;</td>
                <td class="actions">
                    <div class="btn-group acoes">
                        <?php
                        echo $this->element(
                            'actions4', array(
                                'id' => $despesa['DspDespesasDeViagem']['co_despesas_viagem'],
                                'class' => 'btn',
                                'local_acao' => 'dsp_despesas_de_viagem/index'
                            )
                        )
                        ?>
                    </div>
                </td>
            </tr>
        <?php
        endforeach;
    ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
        <?php } ?>
</div>


