<?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' );

    $cargo = array_values($cargos);
    $unidade = array_values($unidade);
    $funcao = array_values($funcao);
    $meio_transporte = array_values($meio_transporte);
    $categoria_passagem = array_values($categoria_passagem);
?>

<?php echo $this->Form->create('DespesasDeViagem', array('url' => "/dsp_despesas_de_viagem/imprimirViagem/" . $despesa[0]['DspDespesasDeViagem']['co_despesas_viagem']) );?>
	
	<div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Dados da Despesa com Diárias e Passagem'); ?></h1></div>
        <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'index')); ?>" class="btn btn-small btn-primary" title="Listar <?php __('Despesas'); ?>">Listagem</a>
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
          </div>
        </div>

        <div style="margin-left: 15%; margin-right: 15%;">
        <table class="table table-bordered">
            <tr style="background-color: #F6F6F6;">
                <th colspan="2">PROCESSO</th>
            </tr>
            <tr style="background-color: #F6F6F6;">
                <th>Número do Processo</th>
                <th><b style="margin-right: 26%;">Motivo da Viagem</b></th>
            </tr>
            <tr>
                <td width="20%"><?php echo $despesa[0]['DspDespesasDeViagem']['numero_processo']; ?></td>
                <td width="80%"><?php echo $despesa[0]['DspDespesasDeViagem']['motivo_viagem']; ?></td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr style="background-color: #F6F6F6;">
                <th colspan="5">FUNCIONÁRIO</th>
            </tr>
            <tr style="background-color: #F6F6F6;">
                <th>Unidade de Lotação</th>
                <th>Nome do Funcionário</th>
                <th>Matrícula do Funcionário</th>
                <th>Cargo</th>
                <th>Função</th>
            </tr>
            <tr>
                <td width="20%"><?php echo $unidade[0]; ?></td>
                <td width="20%"><?php echo $despesa[0]['GrupoAuxiliar']['ds_nome']; ?></td>
                <td width="20%"><?php echo $despesa[0]['GrupoAuxiliar']['nu_cpf']; ?></td>
                <td width="20%"><?php echo $cargo[0]; ?></td>
                <td width="20%"><?php echo isset($funcao[0]) ? $funcao[0] : '---'; ?></td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr style="background-color: #F6F6F6;">
                <th colspan="6">DADOS DA VIAGEM</th>
            </tr>
            <tr style="background-color: #F6F6F6;">
                <th>Origem</th>
                <th>Destino</th>
                <th>Data Início</th>
                <th>Data Final</th>
                <th>Meio de Transporte</th>
                <th>Categoria de Passagem</th>
            </tr>
            <tr>
                <?php $origem  = $despesa[0]['DspDespesasDeViagem']['uf_origem'] ?: $despesa[0]['DspDespesasDeViagem']['pais_origem']; ?>
                <?php $destino = $despesa[0]['DspDespesasDeViagem']['uf_destino'] ?: $despesa[0]['DspDespesasDeViagem']['pais_destino']; ?>
                <td width="10%"><?php echo $despesa[0]['DspDespesasDeViagem']['origem'] . ' - ' . $origem; ?></td>
                <td width="10%"><?php echo $despesa[0]['DspDespesasDeViagem']['destino'] . ' - ' . $destino; ?></td>
                <td width="20%"><?php echo (isset($despesa[0]['DspDespesasDeViagem']['dt_inicio'])) ? $despesa[0]['DspDespesasDeViagem']['dt_inicio'] : '---'; ?></td>
                <td width="20%"><?php echo (isset($despesa[0]['DspDespesasDeViagem']['dt_final']))  ? $despesa[0]['DspDespesasDeViagem']['dt_final']  : '---'; ?></td>
                <td width="20%"><?php echo (isset($meio_transporte[0])) ? $meio_transporte[0] : '---'; ?></td>
                <td width="20%"><?php echo (isset($categoria_passagem[0])) ? $categoria_passagem[0] : '---'; ?></td>
            </tr>
        </table>

        <table class="table table-bordered">
            <tr style="background-color: #F6F6F6;">
                <th colspan="6">VALORES</th>
            </tr>
            <tr style="background-color: #F6F6F6;">
                <th>Valor da Passagem</th>
                <th>Número de Diárias</th>
                <th>Valor Total das Diárias</th>
                <th>Valor Total da Viagem</th>
            </tr>
            <tr>
                <td width="25%"><?php echo isset($despesa[0]['DspDespesasDeViagem']['valor_passagem']) ? 'R$ ' . $despesa[0]['DspDespesasDeViagem']['valor_passagem'] : '---'; ?></td>
                <td width="25%"><?php echo isset($despesa[0]['DspDespesasDeViagem']['numero_diarias']) ? $despesa[0]['DspDespesasDeViagem']['numero_diarias'] : '---'; ?></td>
                <td width="25%"><?php echo isset($despesa[0]['DspDespesasDeViagem']['valor_total_diarias']) ? 'R$ ' . $despesa[0]['DspDespesasDeViagem']['valor_total_diarias'] : '---'; ?></td>
                <td width="25%"><?php echo isset($despesa[0]['DspDespesasDeViagem']['valor_total_viagem']) ? 'R$ ' . $despesa[0]['DspDespesasDeViagem']['valor_total_viagem'] : '---'; ?></td>
            </tr>
        </table>
        </div>

		
<div class="form-actions">
   <div class="btn-group">
      <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'drp_man_a') ) { ?>
       <button rel="tooltip" type="button" class="btn btn-small btn-primary bt-pesquisar" onclick="location.href = '<?php echo $this->Html->url(array('controller' => 'dsp_despesas_de_viagem', 'action' => 'edit' . '/' . $despesa[0]['DspDespesasDeViagem']['co_despesas_viagem'])); ?>'" title="Editar Registro" id="editar_registro">Editar</button>
      <?php } ?>
      <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Imprimir Viagem" id="imprimir_viagem">Imprimir PDF</button>
      <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
   </div>
</div>
	</div>

<?php echo $this->Html->scriptStart() ?>

    $("#ValorPassagem").maskMoney({thousands:'.', decimal:','});
    $("#ValorTotalDiarias").maskMoney({thousands:'.', decimal:','});
    $("#ValorTotalViagem").maskMoney({thousands:'.', decimal:','});

    function updateTotal(){
        $("#ValorTotalViagem").value = '';
        var valorPassagem = $("#ValorPassagem").val();
        var valorTotalDiarias = $("#ValorTotalDiarias").val();
        valorPassagem = valorPassagem.replace(',','.');
        valorTotalDiarias = valorTotalDiarias.replace(',','.');
        if( valorPassagem == '' ){
            var total = parseFloat(valorTotalDiarias);
        }else{
            if( valorTotalDiarias == '' ){
                var total = parseFloat(valorPassagem);
            }else{
                var total = parseFloat(valorPassagem) + parseFloat(valorTotalDiarias);
            }
        }
        total = total.toFixed(2);
        total = total.replace('.',',');
        console.log(parseInt(valorPassagem));
        console.log(parseInt(valorTotalDiarias));
        console.log(total);
        $("#ValorTotalViagem").val(total);
    }

    function atzComboCargos(cod_cargo) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_cargos', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione...</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#Cargos").html(options);
               $("#Cargos option[value=" + cod_cargo + "]").attr("selected", true);
               $("#Cargos").trigger("chosen:updated");
        });
    }
    
    function atzComboModalidade(co_contratacao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'contratacoes', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoContratacao").html(options);
               $("#ContratoCoContratacao option[value=" + co_contratacao + "]").attr("selected", true);
               $("#ContratoCoContratacao").trigger("chosen:updated");
        });
    }
    
    function atzComboTpContrato(co_modalidade) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoModalidade").html(options);
               $("#ContratoCoModalidade option[value=" + co_modalidade + "]").attr("selected", true);
               $("#ContratoCoModalidade").trigger("chosen:updated");
        });
    }
    
    function atzComboSituacao(co_situacao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'situacoes', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoSituacao").html(options);
               $("#ContratoCoSituacao option[value=" + co_situacao + "]").attr("selected", true);
               $("#ContratoCoSituacao").trigger("chosen:updated");
        });
    }
    
    function atzComboServico(co_servico) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'servicos', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoServico").html(options);
               $("#ContratoCoServico option[value=" + co_servico + "]").attr("selected", true);
               $("#ContratoCoServico").trigger("chosen:updated");
        });
    }
    
    function atzComboCategoria(co_categoria) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'categorias', 'action' => 'listar') )?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoCategoria").html(options);
               $("#ContratoCoCategoria option[value=" + co_categoria + "]").attr("selected", true);
               $("#ContratoCoCategoria").trigger("chosen:updated");
               atzComboSubCategoria(co_categoria, 0);
        });
    }

    function atzComboSubCategoria(co_categoria, co_sub_categoria) {
        verificaDeContrato(co_categoria);
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'subcategorias', 'action' => 'listar') )?>" + "/" + co_categoria, null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#ContratoCoSubcategoria").html(options);
               if(co_sub_categoria > 0) {
                    $("#ContratoCoSubcategoria option[value=" + co_sub_categoria + "]").attr("selected", true);
               }
               $("#ContratoCoSubcategoria").trigger("chosen:updated");
        });
    }

    $(function(){
    
        setMascaraCampo("#ContratoNuProcesso", "<?php echo FunctionsComponent::pegarFormato( 'processo' ); ?>");
        $("#Vl").maskMoney({thousands:'.', decimal:','});
        $("#V1").maskMoney({thousands:'.', decimal:','});
        $("#V2").maskMoney({thousands:'.', decimal:','});
        $("#V3").maskMoney({thousands:'.', decimal:','});
        $("#V4").maskMoney({thousands:'.', decimal:','});
    
        $("#ContratoVlInicial").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlMensal").maskMoney({thousands:'.', decimal:','});
        $("#ContratoVlGlobal").maskMoney({thousands:'.', decimal:','});
        $("#ContratoNuFimVigencia").maskMoney({precision:0, allowZero:false, thousands:''});
        
        $( "#ContratoNuProcesso" ).on('focusout', function(e) {
        
            var co_contrato = 0;
            if($(this).val() != '') {
                $.ajax({
                    type:"POST",
                    url: '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'find_processo') ); ?>',
                    data:{
                        "data[Contrato][nu_processo]":$(this).val()
                    },
                    success:function(result){

                        $('body').append(result);
                        if(result > 0) {
                            e.preventDefault();

                            $( "#dialog-processo" ).dialog({
                                    resizable: false,
                                    modal: true,
                                    title: 'Processo já cadastrado!',
                                    title_html: true,
                                    buttons: [
                                            {
                                                html: "<i class='icon-ok bigger-110'></i>&nbsp; Confirma",
                                                "class" : "btn btn-success btn-mini",
                                                click: function() {
                                                        $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'contratos', 'action' => 'edit') )?>/' + result + '/nc');
                                                }
                                            }
                                            ,
                                            {
                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancela",
                                                "class" : "btn btn-mini",
                                                click: function() {
                                                        $( this ).dialog("close");
                                                }
                                            }
                                    ]
                            });
                        }
                    }
                })
            }
        });
  });

   $("#AbrirNovoCargo").bind('click', function(e) {
       var url_fn = "<?php echo $this->base; ?>/dsp_cargos/iframe/";
       $.ajax({
           type:"POST",
           url:url_fn,
           data:{},
           success:function(result){
               $('#add_cargo').html("");
               $('#add_cargo').append(result);
           }
       })
   });
   $("#AbrirNovaUnidade").bind('click', function(e) {
        var url_md = "<?php echo $this->base; ?>/dsp_unidade_lotacao/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{},
            success:function(result){
                console.log(result);
                $('#add_unidade').html("");
                $('#add_unidade').append(result);
            }
        })
   });
   $("#AbrirNovaFuncao").bind('click', function(e) {
        var url_tm = "<?php echo $this->base; ?>/dsp_funcao/iframe/";
        $.ajax({
            type:"POST",
            url:url_tm,
            data:{},
            success:function(result){
                $('#add_funcao').html("");
                $('#add_funcao').append(result);
            }
        })
   });
    $("#AbrirNovoMeioTransporte").bind('click', function(e) {
        var url_sv = "<?php echo $this->base; ?>/dsp_meio_transporte/iframe/";
        $.ajax({
            type:"POST",
            url:url_sv,
            data:{},
            success:function(result){
                $('#add_meio_transporte').html("");
                $('#add_meio_transporte').append(result);
            }
        })
    });
    $("#AbrirNovaCategoriaPassagem").bind('click', function(e) {
        var url_st = "<?php echo $this->base; ?>/dsp_categoria_passagem/iframe/";
        $.ajax({
            type:"POST",
            url:url_st,
            data:{},
            success:function(result){
                $('#add_categoria_passagem').html("");
                $('#add_categoria_passagem').append(result);
            }
        })
    });
    
    $(document).ready(function(){
        var _fill = {<? foreach ($_POST as $k=>$v) if (substr($k,0,6) == '_fill_') echo substr($k,6).':'.'"'.str_replace('"',"",$v).'",'; ?>};
        for (id in _fill) $('#'+id).val(_fill[id]);
    });
    
<?php echo $this->Html->scriptEnd() ?>
