<?php echo $this->Form->create('PreImprimir', array('url' => "/dsp_despesas_de_viagem/imprimirRelatorioGeral/")); ?>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Relatório de Despesas com Diárias e Passagens'); ?></h1></div>
    <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    <br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Para visualizar o relatório de Despesas, informe as datas de Início e Fim'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span2">
                        <div class="controls">
                            <?php
                            echo $this->Form->input('dt_inicio', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Início (Período)', 'type'=>'text')
                            );
                            ?>
                        </div>
                    </div>
                    <div class="span2">
                        <div class="controls">
                            <?php
                            echo $this->Form->input('dt_final', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Fim (Período)', 'type'=>'text'));
                            ?>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="controls">
                            <?php
                                echo $this->Form->input('nome_funcionario',array(
                                    'class'   => 'input-xlarge',
                                    'type'    => 'select',
                                    'empty'   => 'SELECIONE...',
                                    'options' => $funcionarios
                                ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Gerar </button>
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

