<?php echo $this->Form->create('DspMeioTransporte', array('url' => array('controller' => 'dsp_meio_transporte', 'action' => 'edit', $id))); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Campos Auxiliares'); ?></h1></div>
    <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Meio de Transporte'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('descricao', array('class' => 'input-xlarge', 'label' => 'Descrição do Meio de Transporte', 'value' => $this->data['DspMeioTransporte']['nome_meio_transporte']));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

