<?php echo $this->Form->create('DspUnidadeLotacao', array('url' => array('controller' => 'dsp_unidade_lotacao', 'action' => 'add', $modal))); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Unidade de Lotação'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                    echo $this->Form->input('nome_unidade', array('class' => 'input-xlarge', 'label' => 'Nome da Unidade de Lotação'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
        </div>
    </div>
</div>

