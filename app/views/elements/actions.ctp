<?php

$usuario = $this->Session->read ('usuario');
$hasPenalidade = $this->Session->read ('hasPenalidade');
?>

<?php if ( !isset( $class ) ) $class = null ?>

<?php if ( isset( $ajax ) ) : ?>

    <?php
    echo $this->Html->link( '', array(
        'action' => 'edit',
        $id
    ), array(
        'escape' => false,
        'class' => 'modal '
    ) );
    ?>

    <?php
    echo $this->Html->link( $this->Html->image( 'ico_excluir.gif' ), array(
        'action' => 'delete',
        $id
    ), array(
        'escape' => false,
        'onclick' => "if(confirm('Tem certeza de que deseja excluir # $id?')) {
            $.ajax( {url: this.href, complete: function() { $callback } } );
        } return false"
    ) );
    ?>


<?php else : ?>

    <?php
    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao . 'edit', $hasPenalidade)) {
        echo $this->Html->link('<i class="icon-pencil"></i>' , array(
            'action' => 'edit',
            $id
        ), array(
            'escape' => false,
            'class' => $class . ' alert-tooltip',
            'title' => "Editar"
        ) );
    }

    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao .'delete', $hasPenalidade) ) {
        echo $this->Html->link( '<i class="icon-trash icon-white"></i>' , array(
            'action' => 'delete',
            $id
        ), array(
            'escape' => false,
            'class' => $class.' btn-danger alert-tooltip',
            'title' => "Excluir"
        ), sprintf( __( 'Tem certeza de que deseja excluir este registro? Esta operação será registrada nos logs e este item será removido permanentemente', true ), $id ) );
    }
    ?>


<?php endif; ?>
