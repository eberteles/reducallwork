<?php
    $usuario = $this->Session->read ('usuario');
?>

<?php if ( !isset( $class ) ) $class = null ?>

<?php if ( isset( $ajax ) ) : ?>

    <?php
        echo $this->Html->link( $this->Html->image( 'ico_print.gif' ) , array(
            'action' => 'mostrar',
            $id
        ), array(
            'escape' => false,
            'class' => 'modal '
        ) );
    ?>

	<?php
		echo $this->Html->link( '', array(
			'action' => 'edit',
			$id
		), array(
			'escape' => false,
			'class' => 'modal '
		) );
        //echo "<a href=\"/gescon2/pendencias/edit/$id\" class=\"modal\"><img src=\"/gescon2/img/ico_alterar.gif\"></a>";
	?>

	<?php
		echo $this->Html->link( $this->Html->image( 'ico_excluir.gif' ), array(
			'action' => 'logicDelete',
			$id
		), array(
			'escape' => false,
			'onclick' => "if( confirm( 'Tem certeza de que deseja excluir # $id?' ) ){
							$.ajax( {url: this.href, complete: function() { $callback } } );
						} return false"
		) );
	?>


<?php else : ?>

	<?php

        echo $this->Html->link( '<i class="icon-print"></i>' , array(
            'action' => 'mostrar',
            $id
        ), array(
            'escape' => false,
            'class' => $class . ' alert-tooltip btn-info',
            'title' => 'Imprimir'
        ) );

            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao) ) {
		echo $this->Html->link('<i class="icon-pencil"></i>' , array(
			'action' => 'edit',
			$id
		), array(
			'escape' => false,
			'class' => $class . ' alert-tooltip',
			'title' => "Editar"
		) );
            }

            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao) ) {
		echo $this->Html->link( '<i class="icon-trash icon-white"></i>' , array(
			'action' => 'logicDelete',
			$id
		), array(
			'escape' => false,
			'class' => $class.' btn-danger alert-tooltip',
                        'title' => "Excluir"
		), sprintf( __( 'Tem certeza de que deseja excluir este registro? Essa exclusão é lógica e será registrada nos logs. Os registros que já possuem esse item atrelado não sofrerão alterações e os novos registros não conseguirão mais associar este item', true ), $id ) );
            }
	?>


<?php endif; ?>