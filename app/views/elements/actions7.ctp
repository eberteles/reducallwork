<?php
    $usuario = $this->Session->read ('usuario');
?>

<?php if ( !isset( $class ) ) $class = null ?>

<?php if ( isset( $ajax ) ) : ?>

	<?php
		echo $this->Html->link( '', array(
			'action' => 'edit',
			$id
		), array(
			'escape' => false,
			'class' => 'modal '
		) );
        //echo "<a href=\"/gescon2/pendencias/edit/$id\" class=\"modal\"><img src=\"/gescon2/img/ico_alterar.gif\"></a>";
	?>
	
	<?php
		echo $this->Html->link( '', array(
			'action' => 'desbloquear',
			$id
		) );
	?>
	

<?php else : ?>

	<?php

            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao) ) {
		echo $this->Html->link('<i class="icon-pencil"></i>' , array(
			'action' => 'edit',
			$id
		), array(
			'escape' => false,
			'class' => $class . ' alert-tooltip',
			'title' => "Editar"
		) );
            }

            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], $local_acao) ) {
		echo $this->Html->link( '<i class="icon-ok icon-white"></i>' , array(
			'action' => 'desbloquear',
			$id
		), array(
			'escape' => false,
			'class' => $class.' btn-success alert-tooltip',
                        'title' => "Desbloquear"
		));
            }
	?>


<?php endif; ?>