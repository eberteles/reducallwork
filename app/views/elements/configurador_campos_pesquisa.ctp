<!--    <div class="ace-settings-container" id="ace-settings-container">-->
<!--            <div class="btn btn-app btn-mini btn-warning ace-settings-btn" id="ace-settings-btn">-->
<!--                    <i class="icon-cog bigger-150 alert-tooltip" title="Configuração dos campos a serem exibidos no resultado da Pesquisa."></i>-->
<!--            </div>-->
<!---->
<!--            <div class="ace-settings-box" id="ace-settings-box"  style="height: 450px;">-->
<!--                <div>-->
<!--                    --><?php
//                          echo $this->Form->input('list_campos_pesquisa', array('type' => 'select', 'empty' => 'Selecione...', 'label' => 'Colunas Disponíveis:',
//                              'after' => $this->Print->getBtnEditCombo('Aplicar as configurações de Colunas', 'mudarColunasPesquisa', '#', true, 'btn btn-small btn-success icon-ok') ) );
//                          echo $this->Form->hidden('co_novos_campos', array('value' => ''));
//                    ?>
<!--                    <label class="lbl"><b>Colunas a serem exibidas no resultado:</b></label>-->
<!--                    <ul class="unstyled spaced" id="list_campos_habilitados">-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--    </div>-->

<script type="text/javascript">

$(document).ready(function() {
    
    carregarComboCampos();
    carregarListagemCampos();
    
    // Atualiza a combo de campos disponíveis para configuração
    function carregarComboCampos() {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'colunas_resultado_pesquisa', 'action' => 'listar') )?>" + "/<?php echo $moduloConfig; ?>", null, function(data){
               var options = '<option value="">Selecione..</option>';
               $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
               });
               $("select#list_campos_pesquisa").html(options);
               $("#list_campos_pesquisa").trigger("chosen:updated");
        });
    }
    
    // Atualiza a listagem de campos selecionados
    function carregarListagemCampos() {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'colunas_resultado_pesquisa', 'action' => 'listar')); ?>" + "/<?php echo $moduloConfig; ?>/2", null, function(data){
               var options = '';
               $.each(data, function(index, val) {
                    options += '<li id="'+ val['codigo'] +'"><i class="icon-remove red alert-tooltip" title="Remove coluna do Resultado da Pesquisa." style="cursor: pointer"></i> ' + val['coluna'] + '</li>';
               });
               $("ul#list_campos_habilitados").html(options);
               $('#list_campos_habilitados li').on('click', 'i.icon-remove', clikRemoveCampo);
               $('.alert-tooltip').tooltip();
        });
    }
    
    // Remove Campo da lista <li>
    function clikRemoveCampo(e) {
        var id  = $(this).parent().attr('id');
        //$("#" + id).remove();
        delCampoPesqUsu(id);
    }
    
    // Adiciona Coluna à configuração do Usuário
    function addCampoPesqUsu(campo) {
        var url_add = "<?php echo $this->base; ?>/colunas_resultado_pesquisa/add_campo/<?php echo $moduloConfig; ?>/" + campo;
        $.ajax({
            type:"POST",
            url:url_add,
            success:function(result){
                carregarComboCampos();
            }
        })
    }
    
    // Remove Coluna da configuração do Usuário
    function delCampoPesqUsu(campo) {
        var url_del = "<?php echo $this->base; ?>/colunas_resultado_pesquisa/del_campo/<?php echo $moduloConfig; ?>/" + campo;
        $.ajax({
            type:"POST",
            url:url_del,
            success:function(result){
                carregarComboCampos();
                carregarListagemCampos();
            }
        })
    }
    
    $("#list_campos_pesquisa").change(function(){
        var options = $("ul#list_campos_habilitados").html();
        var nameSelected = $(this).find(":selected").text();
        options += '<li id="'+ $(this).val() +'"><i class="icon-remove red alert-tooltip" title="Remove coluna do Resultado da Pesquisa." style="cursor: pointer"></i> ' + nameSelected + '</li>';
        $("ul#list_campos_habilitados").html(options);
        $('#list_campos_habilitados li').on('click', 'i.icon-remove', clikRemoveCampo);
        $('.alert-tooltip').tooltip();

        addCampoPesqUsu($(this).val());
    });
    
    $('#list_campos_habilitados li').on('click', 'i.icon-remove', clikRemoveCampo);
    
    $('#mudarColunasPesquisa').click(function(){
        location.reload();
    });
    
});

</script>