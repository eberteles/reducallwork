<?php

    $usuario = $this->Session->read ('usuario');

    switch ($colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_funcao_exibe']) {
        case 'exibePam':
            $link_contrato  = 'detalha/' . $registro['Contrato']['co_contrato'];
            $link_pam       = $this->Print->pam( $registro['Contrato']['nu_pam'] );
            if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') && $registro['Contrato']['ic_ativo'] == 1 ) {
                $link_pam   = $this->Html->link($link_pam, $link_contrato);
            }
            echo $link_pam;
            break;

        case 'exibeProcesso':
            $link_contrato  = 'detalha/' . $registro['Contrato']['co_contrato'];
            $link_pro       = $this->Print->processo( $registro['Contrato']['nu_processo'] );
            if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') && $registro['Contrato']['ic_ativo'] == 1 ) {
                $link_pro   = $this->Html->link($link_pro, $link_contrato);
            }
            echo $link_pro;
            break;
        case 'exibeContrato':
            $link_contrato  = 'detalha/' . $registro['Contrato']['co_contrato'];
            isset($registro['Modalidade']) ?
                $link_ctr = $this->Print->contrato( $registro['Contrato']['nu_contrato'], $registro['Modalidade']['nu_modalidade'] ) :
                $link_ctr = $this->Print->contrato( $registro['Contrato']['nu_contrato']) ;
            if ( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/detalha') && $registro['Contrato']['ic_ativo'] == 1 ) {
                $link_ctr   = $this->Html->link($link_ctr, $link_contrato);
            }
            echo $link_ctr;
            break;
        case 'exibeFaseAtual':
            if(isset($registro['Fase']) && isset($registro['SetorAtual']) && isset($registro['Situacao'])){
                echo $this->Print->faseAtual( $registro['Fase']['ds_fase'],
                    $registro['SetorAtual']['ds_setor'],
                    $registro['Situacao']['ds_situacao'] );
            }
            break;
        case 'exibeFimDaVigencia':
            echo $this->Print->fimDaVigencia($registro);
//            echo $this->Print->fimDaVigencia($registro['Contrato']);
            break;

        case 'exibePendencia':
            if ($registro['Contrato']['nu_pendencias'] > 0 ) {
                echo '<a class="v_pendencia" titulo-aba="' . $this->Print->processo( $registro['Contrato']['nu_processo'] ) . ' - ' . $this->Print->contrato( $registro['Contrato']['nu_contrato'], $registro['Modalidade']['nu_modalidade'] ) . '" id="' . $registro['Contrato']['co_contrato'] . '" href="#view_dominio" data-toggle="modal">' . $registro['Contrato']['nu_pendencias'] . '</a>';
            } else {
                echo "Nada Consta";
            }
            break;

        case 'exibeGarantia':
            $primeiro = true;

            foreach ($registro['GarantiaSuporte'] as $garantia) :
                if(!$primeiro) {
                    echo ' ----------------<BR>';
                }
                $primeiro = false;
                echo '<span class="alert-tooltip" title="' . __('Nº de Série', true) . ': ' . $garantia['nu_serie'] . '"><center>' . $garantia['dt_inicio'] . ' à ' . $garantia['dt_fim'] . '</center></span>';
            endforeach;
            break;

        case 'exibeNomeFornecedor':
            if(isset($registro['Contrato']['co_fornecedor']) && $registro['Fornecedor']['no_razao_social'] != null){
                echo "<a href=\"/fornecedores/mostrar/{$registro['Contrato']['co_fornecedor']}\" >" . $registro['Fornecedor']['no_razao_social'] . "</a>";
            } else {
                echo '---';
            }
            break;
        case 'exibeNomeCliente':
            if(isset($registro['Contrato']['co_cliente']) && $registro['Cliente']['no_razao_social'] != null){
                echo "<a href=\"/clientes/mostrar/{$registro['Contrato']['co_cliente']}\" >" . $registro['Cliente']['no_razao_social'] . "</a>";
            } else {
                echo '---';
            }
            break;
        case 'exibeNomeGestor':
            // TODO: ajustar esta lasca de código para trazer os dados junto com contrato, usando o recursive
            App::import('Model', 'Usuario');
            $usuarioModel = new Usuario();
            if(isset($registro['Contrato']['co_gestor_atual'])){
                $usuario = $usuarioModel->findByCoUsuario($registro['Contrato']['co_gestor_atual']);

                if($usuario['Usuario']['ds_nome'] != null){
                    echo $usuario['Usuario']['ds_nomes'];
                }
            } else {
                echo '---';
            }
            break;
        default:
//            debug($registro[ $colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_dominio'] ][$colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio']]);
            if( isset( $registro[ $colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_dominio'] ][$colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio']] ) &&
                    $registro[ $colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_dominio'] ][$colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio']] != NULL &&
                    $registro[ $colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_dominio'] ][$colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio']] != "" ) {
                echo $registro[ $colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_dominio'] ][$colunasResultadoPesquisa['ColunaResultadoPesquisa']['ds_coluna_dominio']];
            } else {
                echo '---';
            }
    }
?>
