<?php
    $texto_atesto    = "";
    if( $anexo['Anexo']['tp_documento'] == 1 ) {
        $texto_atesto = "Atestado: " . $this->Print->tpIndicador($anexo['Anexo']['ic_atesto']) . " / Observações: " . $anexo['Anexo']['ds_observacao'];
    }

    echo "'b" . $anexo['Anexo']['co_anexo'] . "' : {name: '" . 
            $this->Imprimir->getMenuDocumento($anexo, $permissaoEditar, $permissaoExcluir, Configure::read('App.config.resource.certificadoDigital')) . ' &nbsp;&nbsp; ' .
            $this->Imprimir->getDescricaoDocumento($anexo, $this->Print->printHelp( $tpAnexo[ $anexo['Anexo']['tp_documento'] ], $texto_atesto), $this->base, $this->Modulo->isExecucao(), Configure::read('App.config.resource.certificadoDigital')) . "', type: 'item'} , ";
?>