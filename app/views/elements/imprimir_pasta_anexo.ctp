<?php
    echo "'a" . $pasta['AnexoPasta']['co_anexo_pasta'] . "' : {name: '" . $pasta['AnexoPasta']['ds_anexo_pasta'] . "', type: 'folder', 'icon-class':'orange', 'additionalParameters': { 'children' : { ";

    foreach ($pasta['children'] as $pastaFilha):
        echo $this->element( 'imprimir_pasta_anexo', array( 'pasta'=>$pastaFilha, 'nomePastaPai'=>$nomePastaPai, 'permissaoEditar'=>$permissaoEditar, 'permissaoExcluir'=>$permissaoExcluir ) );
    endforeach;
    
    foreach ($pasta['Anexo'] as $anexoPuro):
        $anexo  = array( 'Anexo' => $anexoPuro, 'Atividade' => $anexoPuro['Atividade'] );
        echo $this->element( 'imprimir_item_anexo', array( 'anexo'=>$anexo, 'permissaoEditar'=>$permissaoEditar, 'permissaoExcluir'=>$permissaoExcluir ) );
    endforeach;

    echo " } } }, ";
    
?>