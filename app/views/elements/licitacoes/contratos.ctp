<?php //debug($contratos) ?>
<table id="tableLicitacoes" cellpadding="0" cellspacing="0" style="background-color: white" class="table-relatorio table table-hover table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo 'Nº Contrato';?></th>
				<th><?php echo 'Nº Processo';?></th>
				<th><?php echo 'Data de publicação';?></th>
			    <th><?php echo 'Modalidade de Contratação';?></th>
				<th><?php echo 'Fundamento Legal';?></th>
                <th><?php echo 'CNPJ';?></th>
                <th><?php echo 'Fornecedor';?></th>
                <th><?php echo 'Objeto';?></th>
                <th><?php echo 'Período da Vigência';?></th>
                <th><?php echo 'Situação';?></th>
                <th><?php echo 'Valor Global';?></th>
                <th><?php echo 'Nota de Empenho';?></th>
                <th><?php echo 'Aditivo';?></th>
                <th><?php echo 'Data do Aditivo';?></th>
			</tr>
		<thead>
    	<tbody>
            <?php //if (count($contratos) > 0) {
                foreach ($contratos as $index => $contrato) : ?>
                <tr>
                    <td><?php echo $this->Print->contrato($contrato['Contrato']['nu_contrato']) ?></td>
                    <td><?php echo $this->Print->processo($contrato['Contrato']['nu_processo']) ?></td>
                    <td><?php echo $contrato[0]['dt_publicacao'] ?></td>
                    <td><?php echo $contrato['Contratacao']['ds_contratacao'] ?></td>
                    <td><?php echo $this->Print->printHelp($contrato['Contrato']['ds_fundamento_legal'], $contrato['Contrato']['ds_fundamento_legal'], 50) ?></td>
                    <td><?php echo $this->Print->cnpjCpf($contrato['Fornecedor']['nu_cnpj'], $contrato['Fornecedor']['tp_fornecedor']) ?></td>
                    <td><?php echo $contrato['Fornecedor']['no_razao_social'] ?></td>
                    <td><?php echo $this->Print->printHelp($contrato['Contrato']['ds_objeto'], $contrato['Contrato']['ds_objeto'], 50) ?></td>
                    <td><?php echo "{$contrato[0]['dt_ini_vigencia']} - {$contrato[0]['dt_fim_vigencia']}" ?></td>
                    <td><?php echo $contrato['Situacao']['ds_situacao'] ?></td>
                    <td><?php echo $this->Formatacao->moeda($contrato['Contrato']['vl_global']) ?></td>
                    <td><?php echo $contrato['NotaFiscal']['nu_nota'] ?></td>
                    <td><?php echo $this->Print->printHelp($contrato['Aditivo']['no_aditivo'], $contrato['Aditivo']['no_aditivo'], 50) ?></td>
                    <td><?php echo $contrato[0]['dt_aditivo'] ?></td>
                    <!--<td class="actions"><?php //$id = $licitacao['Licitacao']['co_licitacao']; ?>
                        <div class="btn-group acoes">	
                            <-<a class="btn btn-primary" href="#view_pastas"><i class="icon-upload"></i></a>
                            <a class="modalAnexoLicitacao btn btn-primary" href="#view_anexos" data-toggle="modal" titulo-aba="Suporte Documental" data-target="#view_anexos" data-id="<?php //echo $licitacao['Licitacao']['co_licitacao']; ?>"><i class="yellow icon-folder-open"></i></a>
                            <?php
                                //echo $this->element('actions2', array('id' => $licitacao['Licitacao']['co_licitacao'], 'class' => 'btn', 'local_acao' => 'adm_for_'));
                            ?>
                                
                        </div>
                    </td>-->
                </tr>
                
            <?php endforeach;
                // } else {
                //      echo '<tr><td colspan=14>Não existem registros a serem exibidos.</td></tr>';
                // } ?>
    	</tbody>
    </table>