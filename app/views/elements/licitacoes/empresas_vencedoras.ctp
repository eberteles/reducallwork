<?php //debug($empresas) ?>

<table id="tableLicitacoes" cellpadding="0" cellspacing="0" style="background-color: white" class="table-relatorio table table-hover table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo 'Nome da Empresa';?></th>
				<th><?php echo 'CNPJ';?></th>
				<th><?php echo 'Penalidade Aplicada';?></th>
			    <th><?php echo 'Período da Penalidade';?></th>
				<!--<th class="actions"><?php// __('Ações');?></th>-->
			</tr>
		<thead>
    	<tbody>
            <?php //if (count($empresas) > 0) {
                foreach ($empresas as $index => $empresa) : ?>
                <tr>
                    <td><?php echo $empresa['Fornecedor']['no_razao_social'] ?></td>
                    <td><?php echo $this->Print->cnpjCpf($empresa['Fornecedor']['nu_cnpj'], $empresa['Fornecedor']['tp_fornecedor']) ?></td>
                    <td><?php echo $empresa['Fornecedor']['ds_penalidade_aplicada'] ?></td>
                    <td><?php echo "{$empresa['Fornecedor']['dt_ini_penalidade']} - {$empresa['Fornecedor']['dt_fim_penalidade']}" ?></td>
                    
                    <!--<td class="actions"><?php //$id = $licitacao['Licitacao']['co_licitacao']; ?>
                        <div class="btn-group acoes">	
                            <-<a class="btn btn-primary" href="#view_pastas"><i class="icon-upload"></i></a>
                            <a class="modalAnexoLicitacao btn btn-primary" href="#view_anexos" data-toggle="modal" titulo-aba="Suporte Documental" data-target="#view_anexos" data-id="<?php //echo $licitacao['Licitacao']['co_licitacao']; ?>"><i class="yellow icon-folder-open"></i></a>
                            <?php
                                //echo $this->element('actions2', array('id' => $licitacao['Licitacao']['co_licitacao'], 'class' => 'btn', 'local_acao' => 'adm_for_'));
                            ?>
                                
                        </div>
                    </td>-->
                </tr>
                
            <?php endforeach;
                // } else {
                //      echo '<tr><td colspan=14>Não existem registros a serem exibidos.</td></tr>';
                // } ?>
    	</tbody>
    </table>