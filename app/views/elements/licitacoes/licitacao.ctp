<table id="tableLicitacoes" cellpadding="0" cellspacing="0" style="background-color: white" class="table-relatorio table table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th><?php echo 'Data de Publicação';?></th>
        <th><?php echo 'Data da Abertura';?></th>
        <th><?php echo 'N°  da Licitação';?></th>
        <th><?php echo 'Modalidade de Contratação';?></th>
        <th><?php echo 'N° Processo';?></th>
        <th><?php echo 'Fundamento Legal';?></th>
        <th><?php echo 'Objeto';?></th>
        <th><?php echo 'Situação';?></th>
        <th><?php echo 'Arquivos';?></th>
    </tr>
    <thead>

    <tbody>
    <?php // if (count($licitacoes) > 0) { 
        foreach ($licitacoes as $licitacao) : ?>
            <tr>
                <td><?php echo $licitacao['Licitacao']['dt_publicacao'] ?></td>
                <td><?php echo $licitacao['Licitacao']['dt_abertura'] ?></td>
                <td><?php echo $this->Print->licitacao($licitacao['Licitacao']['nu_licitacao']) ?></td>
                <td><?php echo $licitacao['Contratacao']['ds_contratacao'] ?></td>
                <td><?php echo $licitacao['Licitacao']['nu_processo'] ?></td>
                <td><?php echo "Fundamento Legal" ?></td>
                <td><?php echo $licitacao['Licitacao']['ds_objeto'] ?></td>
                <td><?php echo $licitacao['Situacao']['ds_situacao'] ?></td>
                <td>
                    <a href="/licitacoes/arquivosDownload/<?echo $licitacao['Licitacao']['co_licitacao'] ?>" class="btImpUrlSimples"
                       >Arquivos Licitação <?php echo $this->Print->licitacao($licitacao['Licitacao']['nu_licitacao'])?></a>

                </td>
            </tr>

        <?php endforeach;
    // } else {
    //     echo '<tr>;<td colspan=12>Não existem registros a serem exibidos.</td></tr>';
    // } ?>
    </tbody>
</table>