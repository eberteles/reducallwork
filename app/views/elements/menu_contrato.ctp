<?php $usuario = $this->Session->read('usuario'); ?>

<div class="btn-group">
    <button data-toggle="dropdown"
            class="btn btn-info btn-small dropdown-toggle alert-tooltip"
            title="Menu de Opções">
        <i class="icon-caret-down"></i>
    </button>

    <ul class="dropdown-menu dropdown-info pull-left">
        <?php
        if (true === Configure::read('App.config.component.sei.enabled')): ?>
            <li>
                <div style="min-height: 16px;"><?php echo $this->Print->IconeProcessoSEI($contrato['Contrato']['nu_processo']); ?></div>
            </li>
        <?php endif; ?>

        <li>
            <a class="v_detalhar" href="#view_dominio" data-toggle="modal"
               titulo-aba="<?php echo $this->Print->processo($contrato['Contrato']['nu_processo'], false); ?>"
               id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="icon-list"></i> &nbsp; Detalhar</a>
        </li>

        <li>
            <a class="v_impressao" href="#view_impressao" data-toggle="modal"
               id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="blue icon-print"></i> &nbsp; Imprimir</a>
        </li>

        <?php if ( $contrato['Contrato']['nu_pam'] == '' && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/edit') && $contrato['Contrato']['ic_ativo'] == 1): ?>
            <li>
                <a class="v_editar" href="#view_dominio" data-toggle="modal"
                   id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="green icon-pencil"></i> &nbsp;
                    Editar</a>
            </li>
        <?php endif; ?>
            
        <?php if ( $contrato['Contrato']['nu_pam'] != '' && $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'contratos/edit') && $contrato['Contrato']['ic_ativo'] == 1): ?>
            <li>
                <a class="v_editar_pam" href="#view_dominio" data-toggle="modal"
                   id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="green icon-pencil"></i> &nbsp;
                    Editar</a>
            </li>
        <?php endif; ?>

        <li>
            <a class="v_pendencia" href="#view_dominio" data-toggle="modal"
               titulo-aba="<?php echo $this->Print->processo($contrato['Contrato']['nu_processo']) . ' - ' . $this->Print->contrato($contrato['Contrato']['nu_contrato'], $contrato['Modalidade']['nu_modalidade']); ?>"
               id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="red icon-info-sign"></i> &nbsp;
                Pendências</a>
        </li>

        <li>
            <a class="v_andamento" href="#view_dominio" data-toggle="modal"
               titulo-aba="<?php echo $this->Print->processo($contrato['Contrato']['nu_processo']) . ' - ' . $this->Print->contrato($contrato['Contrato']['nu_contrato'], $contrato['Modalidade']['nu_modalidade']); ?>"
               id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="grey icon-comments-alt"></i> &nbsp;
                Andamentos</a>
        </li>

        <li>
            <a class="v_anexo" href="#view_dominio" data-toggle="modal"
               titulo-aba="<?php echo $this->Print->processo($contrato['Contrato']['nu_processo']) . ' - ' . $this->Print->contrato($contrato['Contrato']['nu_contrato'], $contrato['Modalidade']['nu_modalidade']); ?>"
               id="<?php echo $contrato['Contrato']['co_contrato']; ?>"><i class="pink icon-folder-open"></i> &nbsp;
                Suporte Documental</a>
        </li>

        <?php
        if ($usuario['UsuarioPerfil']['co_perfil'] == 1) { // Ativar ou Desativar Conrato
            ?>
            <li>
                <?php
                if ($contrato['Contrato']['ic_ativo'] == 1) {
                    ?>
                    <a href="#" co_contrato="<?php echo $contrato['Contrato']['co_contrato'] ?>"
                       name="excluir_contrato"><i class="red icon-trash"></i> &nbsp; Excluir</a>
                    <?php
                } else {
                    ?>
                    <a href="#" co_contrato="<?php echo $contrato['Contrato']['co_contrato'] ?>" name="ativar_contrato"><i
                                class="green icon-ok"></i> &nbsp; Desfazer Exclusão</a>
                    <?php
                }
                ?>
            </li>
            <?php
        }
        ?>
    </ul>
</div>