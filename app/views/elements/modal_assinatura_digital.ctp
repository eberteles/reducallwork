<div id="modal-login" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Assinatura Digital</h3>
    </div>
    <div class="modal-body">
        <script>
            anexos = {
                selected: -1
            }
            nocache = new Date().getTime();
            var attributes = {
                codebase: '/',
                code: 'com.n2oti.gescon.applet.TokenSignApplet.class',
                archive: 'GesconApplet.jar?nocache=' + nocache,
                width: '350', 
                height: '100',
                id: 'signApplet'
            };
            var parameters = {
                type: '-Xmx256m'
            }; // customize per your needs
            var version = '1.6'; // JDK version
            deployJava.runApplet(attributes, parameters, version);
            
            function appletCallback(str){
                res = eval(str);
                $('#appletMessagePlaceholder').text(res.message);
                $('#appletMessagePlaceholder').show();
            }
            
            function getDocumentBase64(){
                return anexos.conteudo;
            }
            
            function getSignatureBase64(){
                return anexos.assinatura;
            }
            
            function getPublicKeyBase64(){
                return anexos.chave;
            }
            
            function setSignature(signature) {
                anexos.assinatura = signature;
            }
            
            function setPublicKey(publicKey) {
                anexos.publicKey = publicKey;
            }
            
            function verifySignature(str) {
                console.log(str);
            }
            
            function getAppletType () {
                // A - Assinar; V - Verificar
                return anexos.type;
            }
            
            function setOwner (owner) {
                anexos.ds_usuario_token = owner;
            }
            
            function saveSignedDocument(){
                $.ajax({
                    url: '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'assinar')); ?>',
                    type: 'POST',
                    data: {
                        assinatura: anexos.assinatura,
                        publicKey: anexos.publicKey,
                        co_anexo: anexos.selected,
                        ds_usuario_token: anexos.ds_usuario_token
                    },
                    success: function (data, textStatus, jqXHR){
                        $('#modal-login').modal('hide');
                    }
                });
            }
            
            function showSignModal(id, type) {
                anexos.selected = id;
                anexos.type = type
                $.ajax({
                    url: '<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'getDocumentBase64')); ?>',
                    type: 'POST',
                    data: {
                        co_anexo: anexos.selected
                    },
                    success: function (data, textStatus, jqXHR){
                        res = eval(data);
                        anexos.conteudo = res.document;
                        anexos.assinatura = res.assinatura;
                        anexos.chave = res.chave;
                        anexos.co_usuario = res.co_usuario;
                        anexos.ds_nome = res.ds_nome;
                        anexos.hash = res.hash;
                        anexos.ds_usuario_token = res.ds_usuario_token;
                    }
                });
                $('#appletMessagePlaceholder').hide();
                $('#signatureFormPlaceholder').hide();
                $('#modal-login').modal('show');
            }
            
            function verifyCallback(appletAnswer) {
                res = eval(appletAnswer);
                if (res.verified == 'true') {
                    $('#signatureStatusImagePlaceholder').attr("src", '/images/greenball.png');
                    $('#signatureStatusTextPlaceholder').text('Verificado');
                } else {
                    $('#signatureStatusImagePlaceholder').attr("src", '/images/redball.png');
                    $('#signatureStatusTextPlaceholder').text('Assinatura inválida. O arquivo foi alterado depois de assinado.');
                }
                $('#signatureFormPlaceholder').show();
                $('#signatureNamePlaceholder').text(anexos.ds_nome);
                $('#signatureSignaturePlaceholder').text(anexos.hash);
                $('#signatureTokenOwnerPlaceholder').text(anexos.ds_usuario_token);
            }
        </script>
        <br/>
        <div style="color:red; font-weight: bold" id="appletMessagePlaceholder"></div>
        <div id="signatureFormPlaceholder" style="display: none">
            <div class="control-group">
                <label for="input01" class="control-label">
                    <span class="required"></span> Estado da assinatura:
                </label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on">
                            <img id="signatureStatusImagePlaceholder" class="alert-tooltip" src="/images/greenball.png"/>
                            <b id="signatureStatusTextPlaceholder"> Verificado</b>
                        </span>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="input01" class="control-label">
                    <span class="required"></span> Usuário logado durante a assinatura:
                </label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i> <b id="signatureNamePlaceholder">Eduardo Leonne</b></span>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="input01" class="control-label">
                    <span class="required"></span> Assinado por (proprietário do token):
                </label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i> <b id="signatureTokenOwnerPlaceholder">Eduardo Leonne</b></span>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="input01" class="control-label">
                    <span class="required"></span> Assinatura:
                </label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-bitbucket-sign"></i> <b id="signatureSignaturePlaceholder">Eduardo Leonne</b></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>