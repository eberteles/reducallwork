<div id="view_impressao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>Configuração de Impressão</h3>
    </div>
    <div class="modal-body">
        <input type="hidden" id="imp_contrato">
        <?php echo $this->Form->checkbox('imp_basicas', array('checked' => 'checked', 'class' => 'ace')); ?> Informações
        Básicas<br>
        <?php echo $this->Form->checkbox('imp_fiscais', array('checked' => 'checked')); ?> <?php __('Fiscalização'); ?>
        <br>
        <?php echo $this->Form->checkbox('imp_adtivos', array('checked' => 'checked')); ?> Aditivos<br>
        <?php echo $this->Form->checkbox('imp_garantias', array('checked' => 'checked')); ?> Garantias<br>
        <?php echo $this->Form->checkbox('imp_empenhos', array('checked' => 'checked')); ?> Empenhos<br>
        <?php echo $this->Form->checkbox('imp_pagamentos', array('checked' => 'checked')); ?> <?php echo __('Pagamentos'); ?>
        <br>
        <?php echo $this->Form->checkbox('imp_historico', array('checked' => 'checked')); ?> Histórico<br>
        <?php echo $this->Form->checkbox('imp_pendencias', array('checked' => 'checked')); ?> Pendências<br>
        <?php echo $this->Form->checkbox('imp_andamento', array('checked' => 'checked')); ?> Andamento<br>
        <?php echo $this->Form->checkbox('imp_execucao', array('checked' => 'checked')); ?> Execução<br>
        <?php echo $this->Form->checkbox('imp_apostilamento', array('checked' => 'checked')); ?> Apostilamentos<br>
    </div>
    <div class="modal-footer">
        <a id="btImpContrato" href="#view_imp_contrato" class="btn btn-small btn-primary"><i class="icon-print"></i>
            Imprimir</a>
        <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
    </div>
</div>

<div id="view_imp_contrato" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <p class="pull-right">
            <a id="imprimir_pdf" href="#imprimir_pdf" class="btn btn-small btn-primary"><i class="icon-print"></i>
                Imprimir</a>
            <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
        </p>
        <h3 id="tituloAbaContrato">Impressão de <?php __('Contrato/Processo'); ?></h3>
    </div>
    <div class="modal-body maior" id="impressao">
    </div>
</div>


<!-- Modal -->
<div id="myModalExclusao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalExclusaoLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalExclusaoLabel">Motivo da exclusão</h3>
    </div>
    <div class="modal-body">
        <p>
            <textarea id="motivo_exclusao" rows="4"
                      style="margin: 0px 0px 10px; width: 516px; height: 80px;"></textarea>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
        <a id="confirmar_excluir_contrato" class="btn btn-primary" class="btn btn-default">Desabilitar contrato </a>
    </div>
</div>

<!-- Modal -->
<div id="myModalAtivar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalAtivarLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalAtivarLabel">Motivo da reativação</h3>
    </div>
    <div class="modal-body">
        <p>
            <textarea id="motivo_ativacao" rows="4"
                      style="margin: 0px 0px 10px; width: 516px; height: 80px;"></textarea>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
        <a id="confirmar_ativar_contrato" class="btn btn-primary" class="btn btn-default">Reabilitar contrato</a>
    </div>
</div>

<div id="view_dominio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="tituloAbaDominio"></h3>
    </div>
    <div class="modal-body" id="list_dominio">
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        function abrirFrame(titulo, url_frame) {
            $('#tituloAbaDominio').html("");
            $('#tituloAbaDominio').append(titulo);
            $.ajax({
                type: "POST",
                url: url_frame,
                data: {},
                success: function (result) {
                    $('#list_dominio').html("");
                    $('#list_dominio').append(result);
                }
            });
        }

        var urlContratoIframe = "<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'iframe')); ?>/" ; 
 
        $('#list_contratos tr td a.v_detalhar').click(function () {
            abrirFrame("Detalhar", urlContratoIframe + this.id + "/detalha");
        });
        $('#list_contratos tr td a.v_editar').click(function () {
            abrirFrame("Editar", urlContratoIframe + this.id + "/edit"); 
        });
        $('#list_contratos tr td a.v_editar_pam').click(function () {
            abrirFrame("Editar", urlContratoIframe + this.id + "/edit_pam"); 
        });

        $("a[name='excluir_contrato']").click(function () {
            var co_contrato = $(this).attr('co_contrato');
            $('#confirmar_excluir_contrato').attr('co_contrato', co_contrato);
            $('#myModalExclusao').modal('show');
            return false;

        });

        $("a[name='ativar_contrato']").click(function () {
            var co_contrato = $(this).attr('co_contrato');
            $('#confirmar_ativar_contrato').attr('co_contrato', co_contrato);
            $('#myModalAtivar').modal('show');
            return false;
        });

        $('#confirmar_excluir_contrato').click(function () {
            $.ajax({
                type: "GET",
                url: "<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'delete')); ?>/" + ($(this).attr('co_contrato')) + "/" + ($('#motivo_exclusao').val()),
                dataType: 'json',
                success: function (r) {
                    $('#myModalExclusao').modal('hide');
                    alert(r.msg);
                    if (!r.error) {
                        window.location.href = "/contratos";
                    }
                }
            });
        });

        $('#confirmar_ativar_contrato').click(function () {
            var co_contrato = ($(this).attr('co_contrato'));
            var motivo = ($('#motivo_ativacao').val());
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'ativar')); ?>",
                data: {
                    'motivo': $('#motivo').val(),
                    'co_contrato': co_contrato
                }
            });
            window.location.replace("/contratos");
        });

        $('#list_contratos tr td a.v_pendencia').click(function () {
            abrirFrame("Pendências " + $(this).attr('titulo-aba'), "<?php echo $this->Html->url(array('controller' => 'pendencias', 'action' => 'iframe')); ?>/" + $(this).attr('id'));
        });

        $('#list_contratos tr td a.v_andamento').click(function () {
            abrirFrame("Andamentos: " + $(this).attr('titulo-aba'), "<?php echo $this->Html->url(array('controller' => 'andamentos', 'action' => 'iframe')); ?>/" + $(this).attr('id'));
        });

        $('#list_contratos tr td a.v_anexo').click(function () {
            abrirFrame("Suporte Documental: " + $(this).attr('titulo-aba'), "<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe')); ?>/" + $(this).attr('id'));
        });

        $('#list_contratos tr td a.v_impressao').click(function () {
            $('#imp_contrato').val($(this).attr('id'));
        });

        $('#btImpContrato').click(function () {
            $('#view_impressao').modal('hide');
            $('#view_imp_contrato').modal();

            var parametrosImpressao = "";
            $('.modal-body > input').each(function (index, object) {

                if ($(object).attr('type') != 'hidden' && index > 1) {
                    if ($(object).prop('checked')) {
                        parametrosImpressao = parametrosImpressao + "1";
                    } else {
                        parametrosImpressao = parametrosImpressao + "0";
                    }
                }
            });

            var url_imp = "<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'iframe_imprimir')); ?>/" + $('#imp_contrato').val() + "/" + parametrosImpressao
            $.ajax({
                type: "POST",
                url: url_imp,
                data: {},
                success: function (result) {
                    $('#impressao').html("");
                    $('#impressao').append(result);
                }
            })
        });

        $('#imprimir_pdf').click(function () {
            window.frames["iframe_imp_contrato"].focus();
            window.frames["iframe_imp_contrato"].print();
        });
    });

</script>
