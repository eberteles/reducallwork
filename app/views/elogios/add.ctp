<?php echo $this->Form->create('Elogio', array('url' => array('controller' => 'elogios', 'action' => 'add'))); ?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Dados do Elogio</h4></div>
            <div class="widget-body">
              <div class="widget-main">
              <?php
                  echo $this->Form->input('co_elogio');
                  echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select','label' => 'Fornecedor Elogiado','id' => 'fornecedorElogiado','type' => 'select', 'options' => $fornecedores, 'empty' => 'Selecione'));
                  echo $this->Form->input('co_contrato', array('class' => 'input-xlarge','label' => 'Contrato Referente','type' => 'select', 'empty' => 'Selecione','id' => 'contratoReferente', 'readonly' => true));
                  echo $this->Form->input('ds_elogio', array('class' => 'input-xlarge','type' => 'textarea','label' => 'Descrição do Elogio','maxlength' => '300'));
              ?>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>


<script type="text/javascript">
    $("#nuElogio").mask("9999/9999");

    $("#fornecedorElogiado").change(function() {
        var fornecedor = $("#fornecedorElogiado").val();
        console.log(fornecedor);
        $.ajax({
            method: "POST",
            dataType: "json",
            url: '<?php $this->Html->url(array('controller' => 'elogios', 'action' => 'listContratos')); ?>',
            data: {
                co_fornecedor: fornecedor
            }
        }).success(function(data) {
            var i;
            var selectContrato = document.getElementById("contratoReferente");
            if (selectContrato.length > 0) {
                selectContrato.remove(selectContrato.length-1);
            }
            for(i = 0; i < data.length; i++) {
                $("#contratoReferente").removeAttr("readonly");
                var nuContrato = data[i].Contrato.nu_contrato.substr(0,4) + '/' + data[i].Contrato.nu_contrato.substr(5,8);
                $("#contratoReferente").append('<option value="' + data[i].Contrato.co_contrato + '">' + nuContrato + '</option>');
            }
//            $.each(data, function(data.Contrato.co_contrato, data.Contrato.nu_contrato) {
//                console.log(data.Contrato.co_contrato);
//                $("#contratoReferente").append('<option value=""></option>');
//            })
        });
    });
</script>