<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">

    <?php if(isset($coFornecedor)) { ?>
        <p>Para cadastrar um Elogio, selecione Estação de Trabalho => Elogios</p>
    <?php } ?>

    <?php if(!isset($coFornecedor)){ ?>
        <div class="page-header position-relative"><h1>Lista de Elogios</h1></div>

	    <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha um dos campos para fazer a pesquisa</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'elogios', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Elogio"><i class="icon-plus icon-white"></i> Novo Elogio</a>
          </div>
        </div>
         
        <?php echo $this->Form->create('Elogio', array('url' => array('controller' => 'elogios', 'action' => 'index')));?>
            
<div class="row-fluid">
    	<div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('num_elogio', array('class' => 'input-xlarge','label' => 'Número do Elogio')); ?>
            </div>
    	</div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('num_fornecedor', array('class' => 'input-xlarge chosen-select','label' => 'Fornecedor Elogiado', 'type' => 'select', 'empty' => 'Selecione', 'options' => $fornecedores)); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('num_contrato', array('class' => 'input-xlarge chosen-select','label' => 'Contrato Referente', 'type' => 'select', 'empty' => 'Selecione', 'options' => $contratos)); ?>
            </div>
        </div>
</div>
<div class="form-actions">
    <div class="btn-group">
      <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
      <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
    </div>
</div>

    <?php } ?>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>NÚMERO DO ELOGIO</th>
		<th>FORNECEDOR ELOGIADO</th>
		<th>CONTRATO REFERENTE</th>
		<th>DATA DE CADASTRO</th>
		<th>DESCRIÇÃO DO ELOGIO</th>
        <?php if(!isset($coFornecedor)){ ?>
		    <th class="actions">AÇÕES</th>
        <?php } ?>
	</tr>
	<?php
	$i = 0;
	foreach ($elogios as $elogio){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $elogio['Elogio']['nu_prefix'] . '/' . $elogio['Elogio']['nu_year']; ?></td>
        <td><?php echo $elogio['Fornecedor']['no_razao_social']; ?></td>
        <td><a target="_blank" href="<?php echo $this->base; ?>/contratos/detalha/<?php echo $elogio['Contrato']['co_contrato']; ?>"><?php echo substr($elogio['Contrato']['nu_contrato'],0,5) . '/' . substr($elogio['Contrato']['nu_contrato'],5,4); ?></a></td>
        <td><?php echo $elogio['Elogio']['dt_elogio']; ?></td>
        <td><?php echo $elogio['Elogio']['ds_elogio']; ?></td>

        <?php if(!isset($coFornecedor)){ ?>
            <td class="actions">
                <div class="btn-group acoes">
                    <?php echo $this->element( 'actions2', array( 'id' => $elogio['Elogio']['co_elogio'], 'class' => 'btn', 'local_acao' => 'elogios/index' ) ); ?>
                </div>
            </td>
        <?php } ?>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>