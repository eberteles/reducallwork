<?php
    $usuario = $this->Session->read ('usuario');
?>
<br>
    <div class="row">
        <div class="col-xs-12">
            <div class="tabbable">
                    <ul id="inbox-tabs" class="inbox-tabs nav nav-tabs padding-16 tab-size-bigger tab-space-1">
                            <li class="li-new-mail pull-right active">
                                    <a href="#" class="btn-new-mail">
                                            <span class="btn bt1n-small btn-purple no-border">
                                                    <i class=" icon-envelope bigger-130"></i>
                                                    <span class="bigger-110">Escrever</span>
                                            </span>
                                    </a>
                            </li><!-- ./li-new-mail -->

                            <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'index', 1)); ?>">
                                            <i class="blue icon-inbox bigger-130"></i>
                                            <span class="bigger-110">Entrada</span>
                                    </a>
                            </li>

                            <li>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'index', 2)); ?>">
                                            <i class="orange icon-location-arrow bigger-130 "></i>
                                            <span class="bigger-110">Enviados</span>
                                    </a>
                            </li>
                    </ul>

            </div><!-- /.tabbable -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <br>
<?php echo $this->Form->create('EmailSave', array('url' => array('controller' => 'emails', 'action' => 'add', $coContrato)) );?>

<?php
    echo $this->Form->input('assunto', array('label' => 'Assunto'));
    echo $this->Form->hidden('co_remetente', array('value'=>$usuario['Usuario']['co_usuario']));
    //echo $this->Form->input('co_destinatario',   array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Para', true) , 'options' => $emails, 'escape' => false));
    echo $this->Form->input('destinatario', array('label' => 'Nome Destinatário', 'class' => 'input-xlarge', 'id'=>'nomeDestinatario'));
    echo $this->Form->input('email', array('label' => 'E-mail Destinatário', 'class' => 'input-xlarge', 'id'=>'emailDestinatario'));
    echo '<br>';
    echo $this->Form->input('mensagem', array('label' => 'Mensagem', 'class' => 'input-xlarge', 'type'=>'textarea', 'rows'=>'8', 'cols'=>'60'));
?>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Enviar"> Enviar</button> 
            </div>
        </div>
    
<script type="text/javascript">
  $( function() {
    function selecionar( email ) {
        $("#emailDestinatario").val(email);
    }
 
    var pesquisa = new Array();
    $( "#nomeDestinatario" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo $this->Html->url(array ('controller' => 'emails', 'action' => 'pesquisar') )?>",
          type:"POST",
          data: {
            "data[nome]": request.term
          },
          success: function( data ) {
            pesquisa = new Array();
            $.each(JSON.parse(data), function(index, val) {
              var item = { id: index, label: val };
              pesquisa.push(item);
            });
            response( pesquisa );
          }
        } );
      },
      minLength: 2,
      select: function( event, ui ) {
        selecionar(ui.item.id );
      }
    } );
  } );
</script>
