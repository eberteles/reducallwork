<br>
    <div class="row">
        <div class="col-xs-12">
            <div class="tabbable">
                    <ul id="inbox-tabs" class="inbox-tabs nav nav-tabs padding-16 tab-size-bigger tab-space-1">
                            <li class="li-new-mail pull-right">
                                    <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'add')); ?>" class="btn-new-mail">
                                            <span class="btn bt1n-small btn-purple no-border">
                                                    <i class=" icon-envelope bigger-130"></i>
                                                    <span class="bigger-110">Escrever</span>
                                            </span>
                                    </a>
                            </li><!-- ./li-new-mail -->

                            <li <?php if($tipo==1){ echo 'class="active"'; } ?>>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'index', 1)); ?>">
                                            <i class="blue icon-inbox bigger-130"></i>
                                            <span class="bigger-110">Entrada</span>
                                    </a>
                            </li>

                            <li <?php if($tipo==2){ echo 'class="active"'; } ?>>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'index', 2)); ?>">
                                            <i class="orange icon-location-arrow bigger-130 "></i>
                                            <span class="bigger-110">Enviados</span>
                                    </a>
                            </li>
                    </ul>

                    <div class="tab-content no-border no-padding">
                            <div class="tab-pane in active">
                                    <div class="message-container">

                                            <div class="message-list-container">
                                                    <div class="message-list" id="message-list">
                                                        <?php
                                                            foreach ($emails as $email):
                                                        ?>
                                                            <div class="message-item <?php if($email['EmailSave']['visualizado'] == 0) { echo 'message-unread';} ?>">
                                                                <?php
                                                                    $destinatario   = 'Sistema';
                                                                    if($tipo==1 && $email['EmailSave']['co_remetente'] > 0) {
                                                                        $destinatario   = $email['Remetente']['ds_nome'];
                                                                    }
                                                                    if($tipo==2) {
                                                                        $destinatario   = $email['EmailSave']['destinatario'];
                                                                    }
                                                                ?>
                                                                <span class="sender" title="<?php echo $destinatario; ?>"><a href="#" onclick="abrirMensagem('<?php echo $email['EmailSave']['id']; ?>', '<?php echo $email['EmailSave']['assunto']; ?>');"><?php if($tipo==2){echo 'Para: ';} echo $destinatario; ?></a></span>
                                                                    <?php if($email['EmailSave']['envio']): ?>
                                                                    <span class="time" style="min-width:135px;"><?php echo $email['EmailSave']['created']; ?></span>
                                                                    <?php else: ?>
                                                                    <span class="time"></span>
                                                                    <?php endif; ?>

                                                                    <span class="summary">
                                                                            <span class="text">
                                                                                <a href="#" onclick="abrirMensagem('<?php echo $email['EmailSave']['id']; ?>', '<?php echo $email['EmailSave']['assunto']; ?>');">
                                                                                <?php echo $email['EmailSave']['assunto']; ?>
                                                                                </a>
                                                                            </span>
                                                                    </span>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                            </div><!-- /.message-list-container -->

                                            <div class="message-footer clearfix">
                                                    <div class="pull-left">
                                                        <?php
                                                            echo $this->Paginator->counter(array(
                                                                    'format' => __('%count% mensagens no total', true)
                                                            ));
                                                        ?>
                                                    </div>

                                                    <div class="pull-right">
                                                        <div class="inline middle">
                                                            <?php
                                                                echo $this->Paginator->counter(array(
                                                                        'format' => __('Página %page% de %pages%', true)
                                                                ));
                                                            ?>
                                                        </div>

                                                        &nbsp; &nbsp;
                                                        <div class="pagination">
                                                            <ul>
                                                                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
                                                                <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div><!-- /.message-container -->
                            </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
            </div><!-- /.tabbable -->
        </div><!-- /.col -->
    </div><!-- /.row -->
