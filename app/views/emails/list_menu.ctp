<?php
    $registros  = $this->Paginator->counter(array('format' => '%count%'));
?>
    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <?php
            $class1  = '';
            $class2  = 'badge-transparent';
            if($registros > 0) {
                $class1 = 'icon-animated-vertical';
                $class  = 'badge-success';
            }
        ?>
            <i class="icon-envelope <?php echo $class1; ?>"></i>
            <span class="badge <?php echo $class2; ?>"><?php echo $registros; ?></span>
    </a>

    <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
                    <i class="icon-envelope-alt"></i>
                    <?php
                        if($registros == '0') {
                            echo 'Não existem novas mensagens.';
                        } elseif ($registros == '1') {
                            echo '1 Mensagem';
                        } else {
                            echo $registros . ' Mensagens';
                        }
                    ?>
            </li>

            <?php
                foreach ($emails as $email):
            ?>
            <li>
                    <a href="#" class="emailsMsg" id="<?php echo $email['EmailSave']['id'];?>" onclick="abrirMensagem('<?php echo $email['EmailSave']['id']; ?>', '<?php echo $email['EmailSave']['assunto']; ?>');">
                            <span class="msg-body abrir">
                                    <span class="msg-title">
                                        <span class="blue" id="titulo-<?php echo $email['EmailSave']['id']; ?>" titulo-mensagem="<?php echo $email['EmailSave']['assunto']; ?>">Assunto:</span> <?php echo $email['EmailSave']['assunto']; ?>
                                    </span>

                                    <span class="msg-time">
                                            <i class="icon-time"></i>
                                            <span>&nbsp;<?php if($email['EmailSave']['created'] == ''){echo 'agora';} else{echo $email['EmailSave']['created'];} ?></span>
                                    </span>
                            </span>
                    </a>
            </li>
            <?php
                endforeach;
            ?>
            <li>
                <a href="<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'index')); ?>">
                    Ver todas as mensagens
                    <i class="icon-arrow-right"></i>
                </a>
            </li>
    </ul>

<script>
    <?php
        if (count($emails)) { ?>
            $().ready(function () {
                $('#list_emails').find('li').last().find('a').find('i').prependTo($('#list_emails').find('li').last().find('a').css('display', 'inline'));
                $('#list_emails').find('li').last().append('<i class="icon-remove"></i><span style="color:#4f99c6;cursor:pointer" id="limparText">Limpar</a>');

                $('#list_emails').find('ul').css('width', '280px');

                $('#limparText').on('mouseover', function () {
                    $(this).css('textDecoration', 'underline');
                });

                $('#limparText').on('mouseleave', function () {
                    $(this).css('textDecoration', 'none');
                });

                $('#limparText').on('click', function (e1) {
                    var idsEmails = [];
                    $('a.emailsMsg').each(function (index, self) {
                        idsEmails.push($(self).attr('id'));
                    });

                    $.ajax({
                        url: '<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'limpar'))?>',
                        type: 'POST',
                        data: {
                            "data[Email]": idsEmails
                        },
                        success: function (data) {
                            atualizarMenuEmail();
                        },
                        error: function (err) {
                            console.error(err);
                        }
                    });
                });

                if ($('#list_emails').find('ul').height() > 260) {
                    $('#list_emails').find('ul').css({ width: '280px', height: '261px', overflowY: 'scroll'});
                    // $('#list_emails').find('li').last().append('<i class="icon-remove"></i><span style="color:#4f99c6;cursor:pointer" id="limparText">Limpar</a>');

                }
            });

        <?php }?>
</script>