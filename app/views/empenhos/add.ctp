<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<input id="dataFimVigenciaContrato" type="hidden" value="<?php echo $dataFimVigenciaContrato ?>"/>
<input id="dataIniVigenciaContrato" type="hidden" value="<?php echo $dataIniVigenciaContrato ?>"/>
<div class="empenhos form">
    <?php echo $this->Form->create('Empenho', array('id' => 'formAddEmpenho', 'url' => "/empenhos/add/$coContrato")); ?>

	<div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">
                Preencha os campos abaixo para adicionar <?php __("umEmpenho"); ?> - <b>Campos com * são obrigatórios.</b>
        </p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar <?php __("Empenhos"); ?>">Listagem
            </a>
        </div>
    </div>

       <div class="row-fluid">
           <div class="widget-header widget-header-small"><h4><?php echo __('Novo Empenho', true) ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <?php
                            echo $this->Form->hidden('co_empenho');

                            echo $this->Form->hidden('co_contrato', array('id' => 'coContrato', 'value' => $coContrato));

                            if ($this->Modulo->processo_pagamento) {
                                echo $this->Form->input('nu_processo_pagamento', array('label' => 'Processo Pagamento', 'mask' => FunctionsComponent::pegarFormato('processo')));
                            }

                            if (!$this->Modulo->isAutoNumeracaoEmpenho()) :
                                echo $this->Form->input('nu_empenho', array('label' => __('Empenho', true), 'mask' => FunctionsComponent::pegarFormato('empenho')));
                            endif;

                            if ($this->Modulo->isPreEmpenho()) {
                                echo $this->Form->input('co_pre_empenho', array('label' => 'Existe um Pré-empenho para este Empenho?', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $preEmpenhos, 'onblur' => 'comparaValoresEmpenho()'));
                            }

                            echo $this->Form->input('tp_empenho', array('label' => 'Tipo', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $tipos, 'onchange' => 'showEmpenhos()'));

                            echo '<div class="EmpenhoReforcoOriginario">';
                            echo $this->Form->input('empenho_reforco_originario', array(
                                'id' => 'EmpenhoReforcoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'label' => __('Vincular empenho a:', true) ,
                                'options' => $empenhos_reforco
                              ));
                            echo '</div>';
 
                            echo '<div class="EmpenhoCancelamentoOriginario">';
                            echo $this->Form->input('empenho_cancelamento_originario', array(
                                'id' => 'EmpenhoCancelamentoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'label' => __('Vincular empenho a:', true) ,
                                'options' => $empenhos_reforco
                              ));
                            echo '</div>';

                            echo '<div class="EmpenhoAnulacaoOriginario">';
                            echo $this->Form->input('empenho_anulacao_originario', array(
                                'id' => 'EmpenhoAnulacaoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'label' => __('Vincular empenho a:', true),
                                'options' => $empenhos_anulacao
                            ));
                            echo '</div>';

                            echo '<div class="EmpenhoEstornoDeAnulacaoOriginario">';
                            echo $this->Form->input('empenho_estorno_de_anulacao_originario', array(
                                'id' => 'EmpenhoEstornoDeAnulacaoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'label' => __('Vincular empenho a:', true),
                                'options' => $empenhos_de_estorno_de_anulacao
                            ));
                            echo '</div>';

                            echo $this->Form->input('ds_empenho', array('class' => 'input-xlarge', 'label' => 'Descrição', 'type' => 'textarea', 'rows' => '4',
                                'onKeyup' => '$(this).limit("1500","#charsLeft")',
                                'after' => '<br><span id="charsLeft">1500</span> caracteres restantes.'));
                            echo '<br />';

                            ?>
                        </dl>
                    </div>
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <?php
                            if ($this->Modulo->isCamposEmpenho('co_plano_interno')) {
                                echo $this->Form->input('co_plano_interno', array('label' => __('Plano Interno', true)));
                            }

                            if ($this->Modulo->isPreEmpenho()) {
                                echo $this->Form->input('vl_empenho', array('label' => 'Valor (R$)', 'onblur' => 'comparaValoresEmpenho()', 'maxLength' => '13'));
                                ?>
                                <span style="color: red;font-weight: bold;" id="MsgPreEmpenho">Valor do <?php __('empenho'); ?> ultrapassou o valor restante do pré-empenho!</span>
                                <span style="color: red;font-weight: bold;" id="MsgAnulacao">Valor da anulação ultrapassou o valor restante do empenho original!</span>
                                <?php
                            } else {
                                echo $this->Form->input('vl_empenho', array('label' => 'Valor (R$)', 'onblur' => 'comparaValoresEmpenho()', 'maxLength' => '13'));
                                ?>
                                <span style="color: red;font-weight: bold;" id="MsgAnulacao">Valor da anulação ultrapassou o valor restante do <?php __('empenho'); ?> original!</span>
                                <?php
                            }

                            echo $this->Form->input('ds_fonte_recurso', array('label' => 'Fonte de Recurso'));

                            ?>
                        </dl>
                    </div>
                    <!-- data do empenho -->
                    <div class="span4">
                       <dl class="dl-horizontal">
                      <?php
                          echo $this->Form->input('dt_empenho', array(
                                'id' => 'dataEmpenho',
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data', 'type'=>'text'));

                            echo '<span id="dataEmpenhoError" style="display:none;color: red;font-weight: bold;">A data do ' . __('empenho', true) . ' deve estar entre a vigência do contrato!</span>';

                            echo $this->Form->input('nu_natureza_despesa', array('label' => 'Natureza de Despesa'));

                            if ($this->Modulo->isCamposEmpenho('nu_ptres')) {
                                echo $this->Form->input('nu_ptres', array('label' => __('PTRES', true)));
                            }
                            ?>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar <?php __("Empenho"); ?>" id="salvarEmpenho"> Salvar</button>
          <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm"> Limpar</button>
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>
$(document).ready(function() {
<?php if (strpos(__('mask_nu_ptres', true), '9') === false) { ?>
    $("#EmpenhoNuPtres").maskMoney({precision:0, allowZero:false, thousands:''});
<?php } else { ?>
    setMascaraCampo("#EmpenhoNuPtres", "<?php echo __('mask_nu_ptres'); ?>");
<?php } ?>

<?php if (strpos(__('mask_natureza_despesa', true), '9') === false) { ?>
    $("#EmpenhoNuNaturezaDespesa").maskMoney({precision:0, allowZero:false, thousands:''});
<?php } else { ?>
    setMascaraCampo("#EmpenhoNuNaturezaDespesa", "<?php echo __('mask_natureza_despesa'); ?>");
<?php } ?>
});
<?php echo $this->Html->scriptEnd() ?>

<script>
    $(document).ready(function () {
        showEmpenhos();
        $('#LimparForm').click(function () {
            $('#EmpenhoNuEmpenho').val('');
            $('#EmpenhoDsEmpenho').val('');
            $('#EmpenhoVlEmpenho').val('');
            $('#EmpenhoDsFonteRecurso').val('');
            $('#EmpenhoDtEmpenho').val('');
            $('#EmpenhoNuNaturezaDespesa').val('');
            $('#EmpenhoNuPtres').val('');
            $('#EmpenhoTpEmpenho').val('');
        });

        $('#dataEmpenho + span').on('click', function () {
            $('#dataEmpenho').focus();
        });

        $("#MsgPreEmpenho").hide();
        $("#MsgAnulacao").hide();

        $("#EmpenhoVlEmpenho").maskMoney({thousands: '.', decimal: ','});
    });

    // funções
    function comparaValoresEmpenho() {
        var vlEmpenho = $("#EmpenhoVlEmpenho").val();
        var preEmpenho = $("#EmpenhoCoPreEmpenho").val();
        var tpEmpenho = $("#EmpenhoTpEmpenho").val();
        var empenhoOriginario = $("#selectAnulacao").val();
        var coContrato = $("#EmpenhoCoContrato").val();

        if (preEmpenho && vlEmpenho) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?php echo $this->Html->url(array('controller' => 'pre_empenhos', 'action' => 'comparaValoresEmpenho')); ?>',
                data: {
                    "vlEmpenho": vlEmpenho,
                    "preEmpenho": preEmpenho
                },
                success: function (result) {
                    if (result.msg == 'false') {
                        $("#MsgPreEmpenho").show();
                        var valor = result.restante * (-1);
                        alert("O valor do <?php __('empenho', true); ?> ultrapassou em R$" + valor + " o valor do pré-empenho!");
                        $("#salvarEmpenho").prop("disabled", true);
                    } else {
                        $("#salvarEmpenho").prop("disabled", false);
                        $("#MsgPreEmpenho").hide();
                    }
                }
            })
        }

        if (tpEmpenho == 'A') {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'comparaValoresAnulacao')); ?>',
                data: {
                    "vlEmpenho": vlEmpenho,
                    "empenhoOriginario": empenhoOriginario
                },
                success: function (result) {
                    if (result.msg == 'false') {
                        $("#MsgAnulacao").show();
                        $("#salvarEmpenho").prop("disabled", true);
                    } else {
                        $("#salvarEmpenho").prop("disabled", false);
                        $("#MsgAnulacao").hide();
                    }
                }
            })
        }
    }

    function showEmpenhos() {
        var tpEmpenho = $("#EmpenhoTpEmpenho").val();

        switch (tpEmpenho) {
            case '<?php echo Empenho::TIPO_REFORCO; ?>':
                $('.EmpenhoReforcoOriginario').show();
                $('.EmpenhoEstornoDeAnulacaoOriginario, .EmpenhoCancelamentoOriginario, .EmpenhoAnulacaoOriginario').hide();
            break;
            case '<?php echo Empenho::TIPO_ANULACAO; ?>':
                $('.EmpenhoAnulacaoOriginario').show();
                $('.EmpenhoEstornoDeAnulacaoOriginario, .EmpenhoCancelamentoOriginario, .EmpenhoReforcoOriginario').hide();
            break;
            case '<?php echo Empenho::TIPO_CANCELAMENTO; ?>':
                $('.EmpenhoCancelamentoOriginario').show();
                $('.EmpenhoEstornoDeAnulacaoOriginario, .EmpenhoAnulacaoOriginario, .EmpenhoReforcoOriginario').hide();
            break;
            case '<?php echo Empenho::TIPO_ESTORNO_DE_ANULACAO; ?>':
                $('.EmpenhoEstornoDeAnulacaoOriginario').show();
                $('.EmpenhoCancelamentoOriginario, .EmpenhoAnulacaoOriginario, .EmpenhoReforcoOriginario').hide();
            break;
            default:
                $('.EmpenhoReforcoOriginario, .EmpenhoAnulacaoOriginario, .EmpenhoCancelamentoOriginario, .EmpenhoEstornoDeAnulacaoOriginario').hide();
            break;
        }
    }
</script>

