<?php 
if($acao == 'S') {
    echo $this->Html->scriptStart();
?>
    $(document).ready(function() {
        parent.document.location.reload();
    });
<?php 
    echo $this->Html->scriptEnd();
} else {
?>

<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="empenhos form">
<?php echo $this->Form->create('Empenho', array('url' => "/empenhos/anular/$coContrato/$id"));?>


	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para anular o <?php __('Empenho'); ?> - <b>Campos com * são obrigatórios.</b></p>
        </div>

          <div class="row-fluid">
            <div class="span4">
               <dl class="dl-horizontal">
            <?php
                    echo $this->Form->hidden('co_empenho');
                    echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                    echo $this->Form->hidden('acao');
                    echo $this->Form->hidden('vl_original', array('value' => $this->Print->real( $empenho['vl_original'] )));
                    echo $this->Form->hidden('vl_empenho', array('value' => $this->Print->real( $empenho['vl_empenho'] )));
                    
                    if($empenho['tp_anulacao'] != '' && isset($empenho['nu_empenho']) ) {
                        echo '<dt>Tipo de Anulação:</dt><dd>';
                        if($empenho['tp_anulacao'] == 'T') {echo 'Total';}  else {echo 'Parcial';}
                        echo '</dd>';
                        
                        echo '<dt>Valor da Anulação (R$):</dt><dd>' . $this->Print->real( $empenho['vl_anulacao'] ) . '</dd>';
                    } else {
                        echo $this->Form->input('tp_anulacao', array('label'=>'Tipo de Anulação', 'type' => 'select', 'empty' => 'Selecione...', 'options' => array('T'=>'Total', 'P'=>'Parcial')));
                        echo $this->Form->input('vl_anulacao', array('label' => 'Valor da Anulação (R$)' ));
                    }
                    
            ?>
                </dl>
              </div>
             <div class="span4">
                <dl class="dl-horizontal">                  
               <?php
                    if($empenho['tp_anulacao'] != '' && isset($empenho['nu_empenho']) ) {
                        echo '<dt>Data da Anulação:</dt><dd>' . $empenho['dt_anulacao'] . '</dd>';
                    } else {
                       echo $this->Form->input('dt_anulacao', array(
                               'before' => '<div class="input-append date datetimepicker">', 
                               'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                               'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                               'class' => 'input-small','label' => 'Data da Anulação', 'type'=>'text'));
                    }
               ?>
                </dl>
              </div>
          </div>
    
    <div class="form-actions">
       <div class="btn-group">
       <?php
            if($empenho['tp_anulacao'] != '' && isset($empenho['nu_empenho']) ) {
       ?>
           <button id="desfazer_anulacao" title="Desfazer Anulação" class="btn btn-small btn-danger"> Desfazer Anulação <i class="icon-trash"></i></button>
       <?php
            } else {
       ?>
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Anular Empenho"> Anular</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
       <?php
            }
       ?>
       </div>
    </div>

</div>    
<?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {
    $("#EmpenhoVlAnulacao").maskMoney({thousands:'.', decimal:','});
    $('#desfazer_anulacao').click(function(){
        if( confirm('Tem certeza de que deseja desfazer a Anulação do Empenho?') ) {
            $("#EmpenhoAcao").val('D');
        } else {
            return false;
        }        
    })
    
    $("#EmpenhoTpAnulacao").change( function() {
        if($(this).val() == 'T') {
            $('#EmpenhoVlAnulacao').val( $('#EmpenhoVlEmpenho').val() );
        }
     });
});

<?php echo $this->Html->scriptEnd() ?>

<?php } ?>