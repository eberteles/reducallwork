<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<div class="empenhos form">
    <?php echo $this->Form->create('Empenho', array('id' => 'formEditEmpenho', 'url' => "/empenhos/edit/$id/$coContrato")); ?>

    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar <?php __('oEmpenho'); ?> - <b>Campos
                com * são
                obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar <?php __('Empenhos'); ?>">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Alterar <?php __('Empenho'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <input id="dataFimVigenciaContrato" type="hidden"
                                   value="<?php echo $dataFimVigenciaContrato ?>"/>
                            <input id="dataIniVigenciaContrato" type="hidden"
                                   value="<?php echo $dataIniVigenciaContrato ?>"/>
                            <?php
                            if ($empenho['tp_empenho'] == 'A') {
                                echo $this->Form->hidden('empenho_anulado', array('value' => $empenho_anulado));
                            }
                            echo $this->Form->hidden('co_empenho');
                            echo $this->Form->hidden('vl_empenho_atual', array('value' => $this->Print->real($empenho['vl_empenho'])));
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                            if ($this->Modulo->processo_pagamento) {
                                echo $this->Form->input('nu_processo_pagamento', array('label' => 'Processo Pagamento', 'mask' => FunctionsComponent::pegarFormato('processo')));
                            }

                            if ($this->Modulo->isAutoNumeracaoEmpenho()) {
                                echo $this->Form->hidden('nu_empenho');
                            }

                            if (!$this->Modulo->isAutoNumeracaoEmpenho()) {
                                echo $this->Form->input('nu_empenho', array(
                                    'label' => __('Empenho', true),
                                    'mask' => FunctionsComponent::pegarFormato('empenho', 3),
                                    'readonly' => true
                                ));
                            }

                            if ($this->Modulo->isPreEmpenho()) {
                                echo $this->Form->input('co_pre_empenho', array('label' => 'Existe um Pré-empenho para este Empenho?', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $preEmpenhos, 'onblur' => 'comparaValoresEmpenho()'));
                            }

                            echo $this->Form->hidden('tp_empenho', array('options' => $tipos));
                            echo $this->Form->input('tp_empenho', array(
                                'label' => 'Tipo',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'options' => $tipos,
                                'onchange' => 'showEmpenhos()',
                                'disabled' => true
                            ));

                            echo '<div class="EmpenhoReforcoOriginario">';
                            echo $this->Form->input('empenho_reforco_originario', array(
                                'id' => 'EmpenhoReforcoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'disabled' => 'disabled',
                                'label' => __('Vincular empenho a:', true),
                                'options' => $empenhos_reforco
                            ));
                            echo '</div>';
                            echo '<div style="display:none" id="errorVincularReforcoOriginario" class="error-message">Campo vínculo em branco!</div>';

                            echo '<div class="EmpenhoAnulacaoOriginario">';
                            echo $this->Form->input('empenho_anulacao_originario', array(
                                'id' => 'EmpenhoAnulacaoOriginario',
                                'class' => 'input-xlarge chosen-select',
                                'type' => 'select',
                                'empty' => 'Selecione...',
                                'disabled' => 'disabled',
                                'label' => __('Vincular empenho a:', true),
                                'options' => $empenhos_anulacao
                            ));
                            echo '</div>';
                            echo '<div style="display:none" id="errorVincularReforcoOriginario" class="error-message">Campo vínculo em branco!</div>';
                            ?>

                            <?php
                            echo $this->Form->input('ds_empenho', array('class' => 'input-xlarge', 'label' => 'Descrição', 'type' => 'textarea', 'rows' => '4',
                                'onKeyup' => '$(this).limit("1500","#charsLeft")',
                                'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                            echo '<br />';
                            ?>
                        </dl>
                    </div>
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <?php
                            if ($this->Modulo->isCamposEmpenho('co_plano_interno')) {
                                echo $this->Form->input('co_plano_interno', array('label' => __('Plano Interno', true)));
                            }

                            $arrConfigInputVlEmpenho = array(
                                'label' => 'Valor (R$)',
                                'onblur' => 'comparaValoresEmpenho()',
                                'value' => $this->Print->real($empenho['vl_empenho']),
                                'maxLength' => '13',
                            );

                            $blockUnputs = false;
                            if (isset($empenho['ic_importacao_siafi']) && $empenho['ic_importacao_siafi'] === 'S') {
                                $blockUnputs = true;
                                $msgTooltipInfoSiafi = 'A edição do campo é bloqueada quando esse é preenchido automaticamento com informações do SIAFI';
                                $arrConfigInputVlEmpenho['before'] = '<div class="input-append">';
                                $arrConfigInputVlEmpenho['after'] = '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>';
                                $arrConfigInputVlEmpenho['disabled'] = 'disabled';
                            }

                            if ($this->Modulo->isPreEmpenho()) {
                                if ($blockUnputs) {
                                    echo $this->Form->input('vl_empenhox', $arrConfigInputVlEmpenho);
                                    echo $this->Form->hidden('vl_empenho');

                                } else {
                                    echo $this->Form->input('vl_empenho', $arrConfigInputVlEmpenho);
                                }
                                ?>
                                <span style="color: red;font-weight: bold;"
                                      id="MsgPreEmpenho">Valor <?php __('doEmpenho'); ?> ultrapassou o valor restante do pré-<?php __('empenho'); ?>
                                    !</span>
                                <span style="color: red;font-weight: bold;" id="MsgAnulacao">Valor da anulação ultrapassou o valor restante do <?php __('empenho'); ?>
                                    original!</span>
                                <?php
                            } else {
                                if ($blockUnputs) {
                                    echo $this->Form->input('vl_empenhox', $arrConfigInputVlEmpenho);
                                    echo $this->Form->hidden('vl_empenho', array('value' => $this->Print->real($empenho['vl_empenho'])));

                                } else {
                                    echo $this->Form->input('vl_empenho', $arrConfigInputVlEmpenho);
                                }
                                ?>
                                <span style="color: red;font-weight: bold;" id="MsgAnulacao">Valor da anulação ultrapassou o valor restante <?php __('doEmpenho'); ?>
                                    original!</span>
                                <?php
                            }

                            echo $this->Form->input('ds_fonte_recurso', array('label' => 'Fonte de Recurso'));

                            ?>
                        </dl>
                    </div>
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <?php
                            if ($blockUnputs) {
                                echo $this->Form->input('dt_empenhox', array(
                                    'id' => 'dataEmpenhox',
                                    'disabled' => 'disabled',
                                    'before' => '<div class="input-append date">',
                                    'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'value' => $empenho['dt_empenho'],
                                    'class' => 'input-small', 'label' => 'Data', 'type' => 'text'));

                                echo $this->Form->hidden('dt_empenho');
                            } else {
                                echo $this->Form->input('dt_empenho', array(
                                    'id' => 'dataEmpenho',
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data', 'type' => 'text'));
                            }

                            echo $this->Form->input('nu_natureza_despesa', array('label' => 'Natureza de Despesa'));

                            if ($this->Modulo->isCamposEmpenho('nu_ptres')) {
                                if ($blockUnputs) {
                                    echo $this->Form->input('nu_ptresx', array(
                                            'label' => __('PTRES', true),
                                            'before' => '<div class="input-append">',
                                            'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                            'disabled' => 'disabled',
                                            'value' => $empenho['nu_ptres'],
                                        )
                                    );
                                    echo $this->Form->hidden('nu_ptres');
                                } else {
                                    echo $this->Form->input('nu_ptres', array('label' => __('PTRES', true)));
                                }
                            }
                            ?>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Salvar <?php __('Empenho'); ?>">
                Salvar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm">
                Limpar
            </button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

</div>

<script type="text/javascript">
    var vlEmpenhoAtual = 0.00;

    $(document).ready(function () {
        var tpEmpenho = $("#EmpenhoTpEmpenho").val();

        showEmpenhos();
        $('#LimparForm').click(function () {
            $('#EmpenhoNuEmpenho').val('');
            $('#EmpenhoDsEmpenho').val('');
            $('#EmpenhoVlEmpenho').val('');
            $('#EmpenhoDsFonteRecurso').val('');
            $('#EmpenhoDtEmpenho').val('');
            $('#EmpenhoNuNaturezaDespesa').val('');
            $('#EmpenhoNuPtres').val('');
            $('#EmpenhoTpEmpenho').val('');
        });

        if (tpEmpenho != 'R') {
            $(".EmpenhoReforcoOriginario").hide();
        }

        if (tpEmpenho != 'A') {
            $(".EmpenhoAnulacaoOriginario").hide();
        }

        $("#MsgPreEmpenho").hide();
        $("#MsgAnulacao").hide();

        $("#EmpenhoVlEmpenho").maskMoney({thousands: '.', decimal: ','});
        $("#EmpenhoVlRestante").maskMoney({thousands: '.', decimal: ','});

        <?php if(strpos(__('mask_nu_ptres', true), '9') === false): ?>
        $("#EmpenhoNuPtres").maskMoney({precision: 0, allowZero: false, thousands: ''});
        <?php else: ?>
        setMascaraCampo("#EmpenhoNuPtres", "<?php echo __('mask_nu_ptres'); ?>");
        <?php endif; ?>

        <?php if(strpos(__('mask_natureza_despesa', true), '9') === false): ?>
        $("#EmpenhoNuNaturezaDespesa").maskMoney({precision: 0, allowZero: false, thousands: ''});
        <?php else: ?>
        setMascaraCampo("#EmpenhoNuNaturezaDespesa", "<?php echo __('mask_natureza_despesa'); ?>");
        <?php endif; ?>
    });

    // funcções
    function comparaValoresEmpenho() {
        return;
        var vlEmpenho = $("#EmpenhoVlEmpenho").val();
        var preEmpenho = $("#EmpenhoCoPreEmpenho").val();
        var tpEmpenho = $("#EmpenhoTpEmpenho").val();
        var empenhoOriginario = $("#selectAnulacao").val();
        var coContrato = $("#EmpenhoCoContrato").val();

        if (preEmpenho && vlEmpenho) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?php echo $this->Html->url(array('controller' => 'pre_empenhos', 'action' => 'comparaValoresEmpenho')); ?>',
                data: {
                    "vlEmpenho": vlEmpenho,
                    "preEmpenho": preEmpenho
                },
                success: function (result) {
                    if (result.msg == 'false') {
                        $("#MsgPreEmpenho").show();
                        var valor = result.restante * (-1);
                        alert("O valor <?php __('doEmpenho'); ?> ultrapassou em R$" + valor + " o valor do pré-empenho!");
                        $("#salvarEmpenho").prop("disabled", true);
                    } else {
                        $("#salvarEmpenho").prop("disabled", false);
                        $("#MsgPreEmpenho").hide();
                    }
                }
            });
        }

        if (tpEmpenho == 'A') {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'comparaValoresAnulacao')); ?>',
                data: {
                    "vlEmpenho": vlEmpenho,
                    "empenhoOriginario": empenhoOriginario
                },
                success: function (result) {
                    if (result.msg == 'false') {
                        $("#MsgAnulacao").show();
                        $("#salvarEmpenho").prop("disabled", true);
                    } else {
                        $("#salvarEmpenho").prop("disabled", false);
                        $("#MsgAnulacao").hide();
                    }
                }
            })
        }
    }

    function showEmpenhos() {
        var tpEmpenho = $("#EmpenhoTpEmpenho").val();
        if (tpEmpenho == 'R') {
            if ($('#EmpenhoReforcoOriginario').val() == 0 && $('.EmpenhoReforcoOriginario').is(':visible')) {
                $('#errorVincularReforcoOriginario').show();
            } else {
                $('#errorVincularReforcoOriginario').hide();
            }

            $(".EmpenhoAnulacaoOriginario").hide();
            $(".EmpenhoReforcoOriginario").show();
            $(".EmpenhoReforcoOriginario").addClass('required');
        } else {
            if (tpEmpenho == 'A') {
                $(".EmpenhoReforcoOriginario").hide();
                $(".EmpenhoReforcoOriginario").removeClass('required');
                $(".EmpenhoAnulacaoOriginario").show();
            } else {
                $(".EmpenhoReforcoOriginario").hide();
                $(".EmpenhoReforcoOriginario").removeClass('required');
                $(".EmpenhoAnulacaoOriginario").hide();
            }
        }
    }
</script>
