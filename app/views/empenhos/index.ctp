<?php echo $this->Html->scriptStart(); ?>

	$( function() {
		parent.atualizaTotalizaValores();
		//parent.atualizaTotalizaPercentuais();
	});

<?php echo $this->Html->scriptEnd(); ?>

<?php
    $usuario = $this->Session->read ('usuario');
    $hasPenalidade = $this->Session->read ('hasPenalidade');
?>

<div class="empenhos index">

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbEmpenho">

    <tr>
		<th><?php echo $this->Paginator->sort(__('Empenho', true), 'nu_empenho');?></th>

        <?php if($this->Modulo->processo_pagamento) { ?>
            <th><?php echo $this->Paginator->sort('Processo Pgt.', 'nu_processo_pagamento');?></th>
        <?php } ?>

		<th><?php echo $this->Paginator->sort('Tipo', 'tp_empenho');?></th>

        <?php if( $this->Modulo->isCamposEmpenho('co_plano_interno')) { ?>
            <th><?php echo $this->Paginator->sort(__('Plano Interno', true), 'co_plano_interno');?></th>
        <?php } ?>

		<th><?php echo $this->Paginator->sort('Data', 'dt_empenho');?></th>
		<th><?php echo $this->Paginator->sort(__('Valor do Empenho', true), 'vl_empenho');?></th>
		<th><?php echo $this->Print->printHelp('Valor Restante', 'Valor ' . __('doEmpenho', true) . ' menos o Total de Notas emitidas para ' . __('oEmpenho', true) . '.'); ?></th>

        <?php if($this->Modulo->isCamposEmpenho('co_plano_interno')){ ?>
            <th><?php echo $this->Paginator->sort('Fonte de recurso', 'ds_fonte_recurso');?></th>
            <th><?php echo $this->Paginator->sort('Natureza de despesa', 'nu_natureza_despesa');?></th>
        <?php } ?>

        <?php if($this->Modulo->siafi == true) { ?>
		    <th><?php echo $this->Paginator->sort('PTRES', 'nu_ptres');?></th>
        <?php } ?>

        <th><?php echo __('Empenho de referência', true) ?></th>
            <th class="actions"><?php __('Ações');?></th>
	</tr>

	<?php
	$i = 0;

	foreach ($empenhos as $empenho){
        $class = null;
	?>

	<tr <?php echo $class;?>>
            <td><?php echo $empenho['Empenho']['nu_empenho']; ?>&nbsp;</td>
        <?php if($this->Modulo->processo_pagamento) { ?>
            <td><?php echo $empenho['Empenho']['nu_processo_pagamento']; ?>&nbsp;</td>
        <?php } ?>
        <td><?php echo $this->Print->type( $empenho['Empenho']['tp_empenho'], $tipos ); ?>&nbsp;</td>
        <?php if( $this->Modulo->isCamposEmpenho('co_plano_interno')) { ?>
            <td><?php echo $empenho['Empenho']['co_plano_interno']; ?>&nbsp;</td>
        <?php } ?>
            <td><?php echo $empenho['Empenho']['dt_empenho']; ?>&nbsp;</td>
            <td>
            <?php
                echo $this->Formatacao->moeda( $empenho['Empenho']['vl_empenho'] );
                $somaReforco = $somaAnulacao = $somaDeEstornoDeAnulacao = 0.0;
                foreach ($empenhos as $emp) {
                    $idDoEmpenhoDaVez = $empenho["Empenho"]["co_empenho"];
                    $valorDoEmpenho = $emp["Empenho"]["vl_empenho"];
                    if($emp["Empenho"]["empenho_anulacao_originario"] == $idDoEmpenhoDaVez){
                        $somaAnulacao += $valorDoEmpenho;
                    } elseif($emp["Empenho"]["empenho_reforco_originario"] == $idDoEmpenhoDaVez) {
                        $somaReforco += $valorDoEmpenho;
                    } elseif($emp["Empenho"]["empenho_estorno_de_anulacao_originario"] == $idDoEmpenhoDaVez) {
                        $somaDeEstornoDeAnulacao += $valorDoEmpenho;
                    }
                }
                if ($somaReforco) {
                    echo '<br> <b>' . __('Empenho', true) . ' com Reforço: ' . $this->Formatacao->moeda($somaReforco) . '</b>';
                }
                if ($somaAnulacao) {
                    echo '<br> <b>' . __('Empenho', true) . ' com Anulação: ' . $this->Formatacao->moeda($somaAnulacao) . '</b>';
                }
                if ($somaDeEstornoDeAnulacao) {
                    echo '<br> <b>' . __('Empenho', true) . ' com Estorno De Anulação: ' . $this->Formatacao->moeda($somaDeEstornoDeAnulacao) . '</b>';
                }
            ?>
            &nbsp;
        </td>

        <td>
            <?php
                if($empenho['Empenho']['tp_empenho'] == 'O') {
                    $mensagemPgto   = 'Não existem notas fiscais para este ' . __('Empenho', true) . '.';
                    $ttNotaFiscal   = 0;

                    if(is_array($empenho['NotaFiscal'])) {
                        foreach ($empenho['NotaFiscal'] as $notaFiscal) {
                            if($coContrato == $notaFiscal['co_contrato']){
                                $ttNotaFiscal += $notaFiscal['vl_nota'];
                            }
                        }
                    }

                    if($ttNotaFiscal > 0) {
                        $mensagemPgto   = 'Valor Total das Notas deste ' . __('Empenho', true) . ': ' . $this->Formatacao->moeda( $ttNotaFiscal ) . '.';
                    }

                    $vlRestante = $empenho['Empenho']['vl_restante'] - $ttNotaFiscal;
                    if($vlRestante < 0) {
                        $vlRestante = 0;
                    }
                    echo $this->Print->printHelp($this->Formatacao->moeda( $vlRestante ), $mensagemPgto);
                } else {
                    echo " - - - ";
                }
            ?>
            &nbsp;
        </td>

        <?php if($this->Modulo->siafi == true) { ?>
		    <td><?php echo $empenho['Empenho']['nu_ptres']; ?>&nbsp;</td>
        <?php } ?>

        <?php
        $numeroEmpenhoOriginal = '- - -';
        $tipoDeEmpenho = $empenho['Empenho']['tp_empenho'];
        if (in_array($tipoDeEmpenho, array(Empenho::TIPO_ANULACAO, Empenho::TIPO_REFORCO, Empenho::TIPO_CANCELAMENTO, Empenho::TIPO_ESTORNO_DE_ANULACAO))) {
            $colunaDoBanco = array(
                Empenho::TIPO_ANULACAO => 'empenho_anulacao_originario',
                Empenho::TIPO_REFORCO => 'empenho_reforco_originario',
                Empenho::TIPO_CANCELAMENTO => 'empenho_cancelamento_originario',
                Empenho::TIPO_ESTORNO_DE_ANULACAO => 'empenho_estorno_de_anulacao_originario',
            );
            App::import('Model', 'Empenhos');
            $empenhosModel = new Empenho();
            $empenhoOriginario = $empenhosModel->find(array('co_empenho' => $empenho['Empenho'][$colunaDoBanco[$tipoDeEmpenho]]));
            $numeroEmpenhoOriginal = $empenhoOriginario['Empenho']['nu_empenho'];
        }
        ?>
            <td><?php echo $numeroEmpenhoOriginal; ?></td>
            <td class="actions">
                    <div class="btn-group acoes">
            <a id="<?php echo $empenho['Empenho']['co_empenho']; ?>" class="v_notas btn alert-tooltip" href="#view_notas" data-toggle="modal" title="Notas Fiscais vinculadas a <?php __("Empenho"); ?>"><i class="icon-file-text" style="display: inline-block;"></i></a>
            <a id="<?php echo $empenho['Empenho']['co_empenho']; ?>" class="v_anexo btn alert-tooltip" href="#view_anexo" data-toggle="modal" title="Anexar <?php __("Empenho"); ?>"><i class="silk-icon-folder-table-btn" style="display: inline-block;"></i></a>
            <?php
                echo $this->element( 'actions', array( 'id' => $empenho['Empenho']['co_empenho'].'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'empenhos/' ) );
            ?>
                    </div>
            </td>
	</tr>
	<?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</div>

<?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add', $hasPenalidade) ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary alert-tooltip" title="Adicionar Nova <?php __("Empenho"); ?>">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar <?php __('Empenho'); ?></h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<div id="view_notas" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><?php __('Notas / Atestos'); ?> - Vinculados <?php __('aoEmpenho'); ?></h3>
  </div>
  <div class="modal-body-iframe" id="vinc_notas">
  </div>
</div>

<div id="view_anular" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="title_anular_empenho">Anular <?php __('Empenho'); ?></h3>
  </div>
  <div class="modal-body-iframe" id="anul_empenho">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

    $('#tbEmpenho tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/empenho/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

    $('#tbEmpenho tr td a.v_notas').click(function(){
        var url_nt = "<?php echo $this->base; ?>/notas_fiscais/iframe/<?php echo $coContrato; ?>/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_nt,
            data:{
                },
                success:function(result){
                    $('#vinc_notas').html("");
                    $('#vinc_notas').append(result);
                }
            })
    });

    $('#tbEmpenho tr td a.v_anular').click(function(){
        var name_modal  = $(this).attr('name-modal');
        var url_ar      = "<?php echo $this->base; ?>/empenhos_anulacoes/iframe/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_ar,
            data:{
                },
                success:function(result){
                    $('#title_anular_empenho').html("");
                    $('#title_anular_empenho').append("Anular <?php __('Empenho'); ?> - " + name_modal);

                    $('#anul_empenho').html("");
                    $('#anul_empenho').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
