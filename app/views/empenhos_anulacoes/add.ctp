<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="empenhos form">
<?php echo $this->Form->create('EmpenhoAnulacao', array('url' => "/empenhos_anulacoes/add/$coEmpenho"));?>
    
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para Anular o Empenho - <b>Campos com * são obrigatórios.</b></p>
        </div>
    
          <div class="row-fluid">
            <div class="span4">
               <dl class="dl-horizontal">
            <?php
                    echo $this->Form->hidden('co_empenho', array('value' => $coEmpenho));
                    echo $this->Form->hidden('vl_empenho', array('value' => $this->Print->real( $empenho['Empenho']['vl_empenho'] )));

                    echo $this->Form->input('nu_empenho', array('label'=>'Empenho', 'mask'=>FunctionsComponent::pegarFormato( 'empenho' )));
                    
                    echo $this->Form->input('tp_anulacao', array('label'=>'Tipo de Anulação', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $tipos ));
                    echo $this->Form->input('vl_anulacao', array('label' => 'Valor da Anulação (R$)' ));
                    
            ?>
                </dl>
              </div>
             <div class="span4">
                <dl class="dl-horizontal">                  
               <?php
                   echo $this->Form->input('dt_anulacao', array(
                           'before' => '<div class="input-append date datetimepicker">', 
                           'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                           'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                           'class' => 'input-small','label' => 'Data da Anulação', 'type'=>'text'));
               ?>
                </dl>
              </div>
          </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Anular Empenho"> Anular</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button> 

       </div>
    </div>

</div>    
<?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {
    $("#EmpenhoAnulacaoVlAnulacao").maskMoney({thousands:'.', decimal:','});
    
    $("#EmpenhoAnulacaoTpAnulacao").change( function() {
        if($(this).val() == 'T') {
            $('#EmpenhoAnulacaoVlAnulacao').val( $('#EmpenhoAnulacaoVlEmpenho').val() );
        }
     });
});

<?php echo $this->Html->scriptEnd() ?>