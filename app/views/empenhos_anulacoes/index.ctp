<?php 
if($acao == 'S') {
    echo $this->Html->scriptStart();
?>
    $(document).ready(function() {
        parent.document.location.reload();
    });
<?php 
    echo $this->Html->scriptEnd();
}
?>

<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="anulacoes index">
<!-- h2>< ?php __('EmpenhoAnulacaos');?></h2-->

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbEmpenhoAnulacao">
       <tr>
		<th>Número do Empenho</th>
		<th>Data</th>
		<th>Valor</th>
		<th>Tipo</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($anulacoes as $anulacao):
	?>
	<tr>
		<td><?php echo $anulacao['EmpenhoAnulacao']['nu_empenho']; ?>&nbsp;</td>
		<td><?php echo $anulacao['EmpenhoAnulacao']['dt_anulacao']; ?>&nbsp;</td>
		<td><?php echo $this->Formatacao->moeda($anulacao['EmpenhoAnulacao']['vl_anulacao']); ?>&nbsp;</td>
		<td><?php echo $this->Print->type( $anulacao['EmpenhoAnulacao']['tp_anulacao'], $tipos ); ?>&nbsp;</td>
                
		<td class="actions">
			<div class="btn-group acoes">	
                        <?php 
                            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add') ) {
                                
                                $id = $anulacao['EmpenhoAnulacao']['co_empenho_anulacao'];
                                echo $this->Html->link( '<i class="icon-trash icon-white"></i>' , array(
                                        'action' => 'delete',
                                        $id, $coEmpenho
                                ), array(
                                        'escape' => false,
                                        'class' => 'btn btn-danger alert-tooltip',
                                        'title' => "Cancelar a Anulação"
                                ), sprintf( __( 'Tem certeza de que deseja cancelar a Anulação este registro?', true ), $id ) );
                                
                           }
                       ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
          <?php
            if( $empenho['Empenho']['vl_empenho'] > 0 ) {
          ?>
            <a href="<?php echo $this->Html->url(array('controller' => 'empenhos_anulacoes', 'action' => 'add', $coEmpenho)); ?>" class="btn btn-small btn-primary alert-tooltip" title="Adicionar Nova Anulação">Adicionar</a> 
          <?php
            }
          ?>
          </div>
        </div>

</div>
<?php } ?>

<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

<?php echo $this->Html->scriptEnd() ?>