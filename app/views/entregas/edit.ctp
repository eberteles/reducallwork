<?php 
    echo $this->Html->script( 'inicia-datetimepicker' );
    echo $this->Html->script( 'date' );
?>
<div class="entregas form">
<?php echo $this->Form->create('Entrega', array('url' => "/entregas/edit/$id/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar uma Entrega - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'entregas', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Entregas">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Alterar Entrega</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('co_entrega');
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                              echo $this->Form->input('dt_entrega', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Data da entrega', 'type'=>'text'));
                              echo $this->Form->input('dt_entrega_oficial', array(
                                          'before' => '<div class="input-append date datetimepickerDtEntregaOficial">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Data oficial da entrega', 'type'=>'text'));
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('pz_recebimento', array('label' => 'Prazo de recebimento (dias)'));
                              echo $this->Form->input('dt_recebimento', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Data recebimento', 'type'=>'text'));
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Entrega"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>
<?php echo $this->Html->scriptStart() ?>

    $("#EntregaPzRecebimento").maskMoney({precision:0, allowZero:false, thousands:''});
    
    $( "#EntregaPzRecebimento" ).on('focusout', function(e) {
        calculaDtRecebimentoEntrega( $("#EntregaDtEntregaOficial").val(), $("#EntregaPzRecebimento").val() );
    });
    
    $( "#EntregaDtEntregaOficial" ).on('focusout', function(e) {
        calculaDtRecebimentoEntrega( $("#EntregaDtEntregaOficial").val(), $("#EntregaPzRecebimento").val() );
    });

<?php echo $this->Html->scriptEnd() ?>