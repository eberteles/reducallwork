<?php
echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Form->create('Evento');
echo $this->Form->hidden('co_evento');
?>

<b>Campos com * são obrigatórios.</b><br>
<div class="row-fluid">
    <div class="span6">
        <div class="widget-header widget-header-small"><h4>Dados Básicos do <?php __('Evento'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span6">
                    <div class="row-fluid">
                        <?php
                        echo $this->Form->input('ic_evento', array('class' => 'input-xlarge', 'type' => 'select', 'label' => 'Evento', 'options' => $icEvento));
                        echo $this->Form->input('nu_ano', array('class' => 'input-mini','label' => 'Ano', 'maxlength' => 4));
                        ?>
                        <div class="row-fluid">
                            <div class="span6">
                        <?php
                        echo $this->Form->input('dt_inicio', array(
                                'before' => '<div class="input-append date datetimepicker">', 
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Início do Evento', 'type'=>'text'));
                        ?>
                            </div>
                            <div class="span6">
                        <?php
                        echo $this->Form->input('dt_fim', array(
                                'before' => '<div class="input-append date datetimepicker">', 
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Fim do Evento', 'type'=>'text'));
                        ?>
                            </div>
                        </div>
                        <?php
                        echo $this->Form->input('co_categoria', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Tipo de Evento', 'empty' => 'Selecione...', 'options' => $tipos));
                        ?>
                    </div>
                  </div>
                  <div class="span6">
                    <?php
                    echo $this->Form->input('co_confederacao', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Confederação', 'empty' => 'Selecione...', 'options' => $confederacoes));
                    echo '<br />';
                    echo $this->Form->input('ds_evento', array('class' => 'input-xlarge', 'label' => 'Nome do Evento', 'rows'=>'2', 'type' => 'textarea',
                        'onKeyup'=>'$(this).limit("500","#charsLeft")', 
                        'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                    echo '<br />';
                    echo $this->Form->input('ds_local', array('class' => 'input-xlarge', 'label' => 'Local', 'rows'=>'2', 'type' => 'textarea',
                        'onKeyup'=>'$(this).limit("500","#charsLeft1")', 
                        'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'));
                    ?>
                  </div>
                </div>
              </div>
            </div>
    </div>
    <div class="span6">
        <div class="widget-header widget-header-small"><h4><?php __('Modalidade / Prova'); ?></h4></div>
        <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                    <div class="span6">
                    <?php
                        echo $this->Form->input('co_modalidade', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Modalidade', 'empty' => 'Selecione...', 'options' => $modalidades));
                        echo '<br />';
                        echo $this->Form->input('co_prova_categoria', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Categoria / Classe', 'empty' => 'Selecione...', 'options' => $categoriasProva));
                        echo '<br />';
                        echo $this->Form->input('ic_sexo', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Sexo', 'empty' => 'Selecione...', 'options' => array( 'F' => 'FEMININO', 'M' => 'MASCULINO', 'A' => 'MISTO' )));
                        echo '<div id="divUfsEvento">';
                        echo '<br />';
                        echo $this->Form->input('ufs_evento', array('multiple'=>true,'class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Estado(s) Participante(s) do Evento', 'options'=> $ufs));
                        echo '</div>';
                        echo '<div id="divPaisesEvento">';
                        echo '<br />';
                        echo $this->Form->input('paises_evento', array('multiple'=>true,'class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'País(es) Participante(s) do Evento', 'options'=> $paises));
                        echo '</div>';
                    ?>
                    </div>
                    <div class="span6">
                    <?php
                        echo $this->Form->input('co_prova', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Prova', 'empty' => 'Selecione...', 'options' => $provas));
                        echo '<br />';
                        echo $this->Form->input('co_prova_subcategoria', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Sub Categoria', 'empty' => 'Selecione...', 'options' => $subcategoriasProva));
                        echo '<br />';
                        echo $this->Form->input('co_prova_classificacao', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Classificação Funcional', 'empty' => 'Selecione...', 'options' => $classificacaoProva));
                        echo '<div id="divUfsProva">';
                        echo '<br />';
                        echo $this->Form->input('ufs_prova', array('multiple'=>true,'class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'Estados Participantes na Prova', 'options'=> $ufs));
                        echo '</div>';
                        echo '<div id="divPaisesProva">';
                        echo '<br />';
                        echo $this->Form->input('paises_prova', array('multiple'=>true,'class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => 'País(es) Participante(s) na Prova', 'options'=> $paises));
                        echo '</div>';
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar" id="Salvar"> Salvar</button> 
        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
        <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>



<script type="text/javascript">
    
    $(function(){
        
        function mostraUfs() {
            $('#divPaisesEvento').hide();
            $('#divPaisesProva').hide();
            
            $('#divUfsEvento').show();
            $('#divUfsProva').show();
        }
        
        function mostraPaises() {
            $('#divPaisesEvento').show();
            $('#divPaisesProva').show();
            
            $('#divUfsEvento').hide();
            $('#divUfsProva').hide();
        }
        
        if( $("#EventoIcEvento").val() == 'N' ) {
            mostraUfs();
        } else {
            mostraPaises();
        }
        
        $("#EventoIcEvento").change(function(){
            if( $(this).val() == 'N' ) {
                mostraUfs();
            } else {
                mostraPaises();
            }
        });
        
        $('#EventoCoConfederacao').change(function(){

           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#EventoCoModalidade").html(options);
                   $("#EventoCoModalidade").trigger("chosen:updated");
           })

        });
        
        $('#EventoCoModalidade').change(function(){

           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'listar_provas') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#EventoCoProva").html(options);
                   $("#EventoCoProva").trigger("chosen:updated");
           })

        });
        
        $('#EventoCoProva').change(function(){

           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'provas_categorias', 'action' => 'listar') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#EventoCoProvaCategoria").html(options);
                   $("#EventoCoProvaCategoria").trigger("chosen:updated");
           })
           
           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'provas_subcategorias', 'action' => 'listar') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#EventoCoProvaSubcategoria").html(options);
                   $("#EventoCoProvaSubcategoria").trigger("chosen:updated");
           })
           
           $.getJSON("<?php echo $this->Html->url(array ('controller' => 'provas_classificacoes', 'action' => 'listar') )?>" + "/" + $(this).val(), null, function(data){
                   var options = '<option value="">Selecione...</option>';
                   $.each(data, function(index, val) {
                            options += '<option value="' + index + '">' + val + '</option>';
                              });
                   $("select#EventoCoProvaClassificacao").html(options);
                   $("#EventoCoProvaClassificacao").trigger("chosen:updated");
           })
           
           //$.getJSON("<?php echo $this->Html->url(array ('controller' => 'modalidades', 'action' => 'get_is_sexo_prova') )?>" + "/" + $(this).val(), null, function(data){
           //    if(data == "1") {
           //        var options = '<option value="">Selecione...</option>';
           //        options += '<option value="M">Masculino</option>';
           //        options += '<option value="F">Feminino</option>';
           //        $("select#EventoIcSexo").html(options);
           //        $("#EventoIcSexo").trigger("chosen:updated");
           //    }
           //})

        });

    });

</script>
