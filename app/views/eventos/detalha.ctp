﻿<?php
    $usuario = $this->Session->read ('usuario');
?>

<div class="eventos form">
	                        
  <div class="acoes-formulario-top clearfix" >
            <div class="span4">
                <div class="pull-left btn-group">
                    <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_man_a') ) { ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'eventos', 'action' => 'edit' , $id)); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Editar"><i class="icon-pencil icon-white"></i> Editar</a> 
                    <?php } ?>
                            <!-- a href="#view_impressao" class="v_impressao btn btn-small btn-primary" data-toggle="modal"><i class="icon-print"></i> Imprimir</a--> 
                            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
             </div>
    </div>
    
    <div id="accordion1" class="acordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" id="identificacao">
                    <?php __('Evento'); echo ": " . $evento['Evento']['ds_evento']; ?>
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                <div class="accordion-inner">

              <div class="row-fluid">

                <div class="span12 ">

                  <div class="body-box">
                      <div class="span6">
                         <dl class="dl-horizontal">
                            <dt><?php __('Evento') ?>: </dt>
                            <dd><?php echo $icEvento[ $evento['Evento']['ic_evento'] ]; ?></dd>
                            
                            <dt>Ano: </dt>
                            <dd><?php echo $evento['Evento']['nu_ano']; ?></dd>
                            
                            <dt>Data do Evento: </dt>
                            <dd><?php echo $evento['Evento']['dt_inicio']; ?> &nbsp;&nbsp;à&nbsp;&nbsp; <?php echo $evento['Evento']['dt_fim']; ?></dd>
                            <br>
                            <dt>Modalidade: </dt>
                            <dd><?php echo $evento['Modalidade']['ds_modalidade'] . ' - ' . $tiposModalidade[$evento['Modalidade']['tp_modalidade']]; ?>&nbsp;</dd>
                            
                            <dt>Categoria / Classe: </dt>
                            <dd><?php echo $evento['ProvaCategoria']['ds_prova_categoria']; ?>&nbsp;</dd>
                            
                            <dt>Sexo: </dt>
                            <dd class="right"><?php if($evento['Evento']['ic_sexo'] != '') {echo $sexos[$evento['Evento']['ic_sexo']];} ?>&nbsp;</dd>
                            
                            <dt>Participante(s) do Evento: </dt>
                            <dd class="right">
                                <?php
                                $competidoresEvento = '';
                                foreach ($this->data['EventoCompetidor'] as $competidorEvento):
                                    if( $competidoresEvento != '' ) {
                                        $competidoresEvento .= ', ';
                                    }
                                    if($competidorEvento['sg_uf'] != '') {
                                        $competidoresEvento .= $competidorEvento['Uf']['no_uf'];
                                    } else {
                                        $competidoresEvento .= $competidorEvento['Pais']['no_pais'];
                                    }
                                endforeach;
                                echo $competidoresEvento;
                                ?>
                            </dd>
                            
                         </dl>
                      </div>
                      <div class="span6">
                         <dl class="dl-horizontal">
                            <dt>Confederação: </dt>
                            <dd><?php echo $evento['Confederacao']['ds_confederacao']; ?>&nbsp;</dd>
                            
                            <dt>Local: </dt>
                            <dd><?php echo $evento['Evento']['ds_local']; ?>&nbsp;</dd>
                            
                            <dt>Tipo de Evento: </dt>
                            <dd><?php echo $evento['TipoEvento']['no_categoria']; ?></dd>
                            <br>
                            
                                                        <dt>Prova: </dt>
                            <dd><?php echo $evento['Prova']['ds_prova']; ?>&nbsp;</dd>
                            
                            <dt>Sub Categoria: </dt>
                            <dd><?php echo $evento['ProvaSubCategoria']['ds_prova_subcategoria']; ?>&nbsp;</dd>
                            
                            <dt>Classificação Funcional: </dt>
                            <dd class="right"><?php echo $evento['ProvaClassificacao']['ds_prova_classificacao']; ?>&nbsp;</dd>
                            
                            <dt>Participante(s) na Prova: </dt>
                            <dd class="right">
                                <?php
                                $participantesEvento = '';
                                foreach ($this->data['EventoParticipante'] as $participanteEvento):
                                    if( $participantesEvento != '' ) {
                                        $participantesEvento .= ', ';
                                    }
                                    if($participanteEvento['sg_uf'] != '') {
                                        $participantesEvento .= $participanteEvento['Uf']['no_uf'];
                                    } else {
                                        $participantesEvento .= $participanteEvento['Pais']['no_pais'];
                                    }
                                endforeach;
                                echo $participantesEvento;
                                ?>
                            </dd>
                         </dl>
                      </div>
                   </div>
                 </div>
               </div>

             </div>
           </div>
        </div>
    </div><br>

<?php
    
    $inativoall = true;
    $abaAtiva    = null;

    $abas = array();
    
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_rst_c') ) {
        $abas[] = array( 'fsResultados', 'Resultados', true, $this->Html->url(array('controller' => 'eventos_resultados', 'action' => 'iframe', $id)) );
    }
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_anx_c') ) {
        $abas[] = array( 'fsAnexos', __('Suporte Documental', true), true, $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $id, 'evento')) );
    }
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_htr_c') ) {
        $abas[] = array( 'fsHistoricos', 'Avaliação', true,$this->Html->url(array('controller' => 'historicos', 'action' => 'iframe', $id, 'evento')) );
    }
    
    if(count($abas) > 0) {
?>
                
    <div id="accordion4" class="accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseAba" data-parent="#accordion4" data-toggle="collapse" class="accordion-toggle" id="complemento">
                  Complemento
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseAba" style="height: auto;">
                <div class="accordion-inner">

                  <div class="row-fluid">  


                    <div class="box-tabs-white">
                    <ul class="nav nav-tabs" id="myTab">
                    <?php echo $aba->render( $abas ); ?>
                     </ul>
                     <div class="tab-content" id="myTabContent">

                    </div>

                    </div>

                  </div>


                </div>
            </div>
        </div>
    </div>
<?php
    }
?>

                  
</div>


<?php echo $this->Html->scriptStart() ?>
        
	function carregarAba(element,aba){
		$("#myTab > li").each(function(){
			$(this).removeClass('active');
		});
		element.parent().addClass('active');
		$("#myTabContent").load(element.attr('req'));
	}
	
	$( function() {                
                $("#myTabContent").load( $("#li_fsResultados > a").attr('req') );
	});
        
	
<?php echo $this->Html->scriptEnd() ?>