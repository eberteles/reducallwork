
	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Eventos'); ?></h1></div>
         <?php
            $usuario = $this->Session->read ('usuario');
         
            if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_man_i') ) {
         ?>
            <div class="acoes-formulario-top clearfix" >
                <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
                <div class="pull-right btn-group">
                  <a href="<?php echo $this->Html->url(array('controller' => 'eventos', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Novo Evento'); ?>"><i class="icon-plus icon-white"></i> <?php __('Novo Evento'); ?></a>
                </div>
            </div>
         <?php
            }
         ?>
		<?php echo $this->Form->create('Evento');?>

<div class="row-fluid">
		<div class="span1">
                    <div class="controls">
    			<?php echo $this->Form->input('nu_ano', array('class' => 'input-mini','label' => 'Ano', 'maxlength' => 4)); ?>
                    </div>
                </div>
		<div class="span2">
                    <div class="controls">
    			<?php echo $this->Form->input('co_confederacao', array('class' => 'input-large chosen-select', 'type' => 'select', 'label' => 'Confederação', 'empty' => 'Selecione...', 'options' => $confederacoes)); ?>
                    </div>
                </div>
   		<div class="span3">
                    <div class="controls">
    			<?php echo $this->Form->input('ds_evento', array('class' => 'input-large','label' => 'Evento')); ?>
                    </div>
                </div>
   		<div class="span3">
                    <div class="controls">
    			<?php echo $this->Form->input('ds_local', array('class' => 'input-large','label' => 'Local')); ?>
                    </div>
                </div>
		<div class="span2">
                    <div class="controls">
    			<?php echo $this->Form->input('co_modalidade', array('class' => 'input-large chosen-select', 'type' => 'select', 'label' => 'Modalidade', 'empty' => 'Selecione...', 'options' => $modalidades)); ?>
                    </div>
                </div>
</div>
<div class="form-actions">
     <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar Evento"><i class="icon-search icon-white"></i> Pesquisar</button> 
        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
        <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
     </div>
</div>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('Evento');?></th>
			<th><?php echo __('Ano');?></th>
			<th><?php echo __('Confederação');?></th>
                        <th><?php echo __('Data');?></th>
			<th><?php echo __('Tipo de Evento');?></th>
			<th><?php echo __('Modalidade');?></th>
			<th><?php echo __('Prova');?></th>
			
			<th class="actions"><?php __('Ações');?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = 0;
	foreach ($eventos as $evento):
            $id = $evento['Evento']['co_evento'];
	?>
	<tr>	
                <td><?php echo $this->Html->link($evento['Evento']['ds_evento'], 'detalha/' . $id); ?>&nbsp;</td>
		<td><?php echo $evento['Evento']['nu_ano']; ?>&nbsp;</td>
		<td><?php echo $evento['Confederacao']['ds_confederacao']; ?>&nbsp;</td>
                <td><?php echo $evento['Evento']['dt_inicio']; ?> &nbsp;&nbsp;à&nbsp;&nbsp; <?php echo $evento['Evento']['dt_fim']; ?>&nbsp;</td>
		<td><?php echo $evento['TipoEvento']['no_categoria']; ?>&nbsp;</td>
		<td><?php echo $evento['Modalidade']['ds_modalidade']; ?>&nbsp;</td>
		<td><?php echo $evento['Prova']['ds_prova']; ?>&nbsp;</td>

		<td class="actions">
			<div class="btn-group acoes">	
                            <?php echo $this->element( 'actions', array( 'id' => $id, 'class' => 'btn', 'local_acao' => 'eventos/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

