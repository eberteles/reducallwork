<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="resultados index">
<!-- h2>< ?php __('Históricos');?></h2 -->

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_rst_i') ) { 
        echo $this->Form->create('EventoResultado', array('url' => array('controller' => 'eventos_resultados', 'action' => 'add', $coEvento)));
?>
<div class="actions">
    <div class="acoes-formulario-top clearfix" >
        <div class="span2">
         <?php 
            echo $this->Form->hidden('co_evento', array('value'=>$coEvento));
            echo $this->Form->hidden('co_fornecedor');
            echo $this->Form->input('nu_classificacao', array('class' => 'input-mini','label' => __('Classificação', true), 'maxlength' => 3));
         ?>
        </div>
        <div class="span3">
         <?php 
            echo $this->Form->input('nu_cpf', array('class' => 'input-xsmall','label' => __('CPF', true), 'maxlength' => 18));
         ?>
        </div>
        <div class="span3">
         <?php 
            echo $this->Form->input('no_atleta', array('class' => 'input-xlarge', 'label' => 'Nome do Atleta'));
         ?>
        </div>
        <div class="span3">
            <br>
            <button type="submit" id="addAtleta" class="btn btn-small btn-primary bt-pesquisar alert-tooltip" title="Adicionar Resultado ao Evento">Adicionar Resultado</button>
        </div>
    </div>

</div>
<?php 
        echo $this->Form->end();
    } 
?>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbResultado">
	<tr>
		<th><?php __('Classificação');?></th>
		<th><?php __('Nome do Atleta');?></th>
		<th><?php __('CPF');?></th>
		<th class="actions"><!--?php __('Ações');?--></th>
	</tr>
	<?php
	$i = 0;
	foreach ($resultados as $resulltado):
	?>
	<tr>
		<td><?php echo $resulltado['EventoResultado']['nu_classificacao']; ?>&nbsp;</td>
		<td><?php echo $resulltado['Fornecedor']['no_razao_social']; ?>&nbsp;</td>
		<td><?php echo $this->Print->cnpjCpf( $resulltado['Fornecedor']['nu_cnpj'], $resulltado['Fornecedor']['tp_fornecedor'] ); ?>&nbsp;</td>
		<td class="actions"><?php $id = $resulltado['EventoResultado']['co_evento_resultado']; ?> 
        
		<div class="btn-group acoes">
                    <?php
                        if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'evt_rst_e') ) {
                            echo $this->Html->link( '<i class="icon-trash icon-white"></i>' , array(
                                    'action' => 'delete',
                                    $id, $coEvento
                            ), array(
                                    'escape' => false,
                                    'class' => 'btn btn-danger alert-tooltip',
                                    'title' => "Excluir"
                            ), sprintf( __( 'Tem certeza de que deseja excluir este registro?', true ), $id ) );
                        }
                    ?>
                </div></td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>


<?php echo $this->Html->scriptStart(); ?>

    $('.alert-tooltip').tooltip();
    
    $("#EventoResultadoNuCpf").mask("999.999.999-99");
    $("#EventoResultadoNuClassificacao").maskMoney({precision:0, allowZero:false, thousands:''});
    
    $( "#EventoResultadoNuCpf" ).on('focusout', function(e) {

        var co_fornecedor   = 0;
        if($(this).val() != '') {
            $.getJSON("<?php echo $this->Html->url(array ('controller' => 'fornecedores', 'action' => 'find') ); ?>/" + $(this).val(), null, function(data){
                if( !$.isEmptyObject(data) ) {
                    $("#EventoResultadoCoFornecedor").val(data["Fornecedor"]["co_fornecedor"]);
                    $("#EventoResultadoNoAtleta").val(data["Fornecedor"]["no_razao_social"]);
                    $("#EventoResultadoNoAtleta").attr('readonly', true);
                } else {
                    $("#EventoResultadoCoFornecedor").val('');
                    $("#EventoResultadoNoAtleta").attr('readonly', false);
                }
            });
        }
    });
    
    $("#addAtleta").bind('click', function(e) {
        if($("#EventoResultadoNuClassificacao").val() <= 0 || $("#EventoResultadoNuCpf").val() == '' || $("#EventoResultadoNoAtleta").val() == '' ) {
            alert('Favor informar todos os dados do Resultado.');
            return false;
        }
    });
    
<?php echo $this->Html->scriptEnd() ?>