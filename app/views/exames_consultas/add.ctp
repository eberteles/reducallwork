<?php echo $this->Form->create('ExameConsulta', array('url' => "/exames_consultas/add")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Dados do Exame/Consulta</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('id');
                echo $this->Form->input('co_especialidade', array('class' => 'input-xlarge','label' => 'Especialidade do Exame/Consulta', 'type' => 'select', 'options' => $especialidades, 'empty' => 'Selecione'));
                echo $this->Form->input('codigo', array('class' => 'input-xlarge', 'label' => 'Código do Exame/Consulta', 'maxlength' => 15, 'mask' => '9?99999999999999'));
                echo $this->Form->input('nome', array('class' => 'input-xlarge', 'label' => 'Nome do Exame/Consulta', 'maxlength' => 50));
                echo $this->Form->input('observacoes', array('class' => 'input-xlarge', 'label' => 'Observações', 'maxlength' => 150));
                ?>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preechidos" class="btn btn-small" id="Voltar"> Voltar</button>

        </div>
    </div>
</div>

