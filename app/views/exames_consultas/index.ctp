<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1>Lista de Exames/Consultas</h1></div>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha um dos campos para fazer a pesquisa</p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'exames_consultas', 'action' => 'add')); ?>" class="btn btn-small btn-primary" title="Novo Exame/Consulta"><i class="icon-plus icon-white"></i> Novo Exame/Consulta</a>
        </div>
    </div>

    <?php echo $this->Form->create('ExameConsulta', array('url' => '/exames_consultas/index'));?>

    <div class="row-fluid">
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('codigo_ex', array('class' => 'input-xlarge','label' => 'Código do Exame/Consulta', 'maxlength' => 15)); ?>
                <?php echo $this->Form->input('nome_ex', array('class' => 'input-xlarge','label' => 'Nome Exame/Consulta', 'maxlength' => 50)); ?>
                <?php echo $this->Form->input('especialidade', array('class' => 'input-xlarge chosen-select', 'label' => 'Especialidade', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $especialidades)); ?>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
            <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
        </div>
    </div>

    <table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
        <tr>
            <th>ESPECIALIDADE</th>
            <th>CÓDIGO</th>
            <th>EXAME/CONSULTA</th>
            <th class="actions">AÇÕES</th>
        </tr>
        <?php
        $i = 0;
        foreach ($exames as $exame){
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class;?>>
                <td><?php echo $exame['Especialidade']['ds_especialidade']; ?></td>
                <td><?php echo $exame['ExameConsulta']['codigo']; ?></td>
                <td><?php echo $exame['ExameConsulta']['nome']; ?></td>

                <td class="actions">
                    <div class="btn-group acoes">
                        <a href="<?php echo "/exames_consultas/edit/" . $exame['ExameConsulta']['id']; ?>" class="btn alert-tooltip" title="Editar"><i class="icon-pencil"></i></a>
                        <a href="<?php echo "/exames_consultas/delete/" . $exame['ExameConsulta']['id']; ?>" class="btn btn-danger alert-tooltip" title="Excluir" onclick="return confirm('Tem certeza de que deseja excluir #'+ <?php echo $exame['ExameConsulta']['nome']; ?> +'?');"><i class="icon-trash icon-white"></i></a>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
            <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
        </ul>
    </div>
</div>