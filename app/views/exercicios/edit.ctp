<div class="exercicios form"><?php echo $this->Form->create('Exercicio');?>
<fieldset><legend><?php __('Exercicio'); ?></legend> <?php
echo $this->Form->input('co_exercicio');
echo $this->Form->input('co_contrato');
echo $this->Form->input('an_exercicio');
echo $this->Form->input('ds_natureza_despesa');
echo $this->Form->input('nu_lei');
echo $this->Form->input('vl_exercicio');
echo $this->Form->input('ds_observacao');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('Exercicio.id')), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $this->Form->value('Exercicio.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Exercicios', true), array('action' => 'index'));?></li>
</ul>
</div>
