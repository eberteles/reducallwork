<div class="exercicios index">
    <div class="page-header position-relative"><h1><?php __('Exercicios');?></h1></div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('co_exercicio');?></th>
		<th><?php echo $this->Paginator->sort('co_contrato');?></th>
		<th><?php echo $this->Paginator->sort('an_exercicio');?></th>
		<th><?php echo $this->Paginator->sort('ds_natureza_despesa');?></th>
		<th><?php echo $this->Paginator->sort('nu_lei');?></th>
		<th><?php echo $this->Paginator->sort('vl_exercicio');?></th>
		<th><?php echo $this->Paginator->sort('ds_observacao');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($exercicios as $exercicio):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $exercicio['Exercicio']['co_exercicio']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['co_contrato']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['an_exercicio']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['ds_natureza_despesa']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['nu_lei']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['vl_exercicio']; ?>&nbsp;</td>
		<td><?php echo $exercicio['Exercicio']['ds_observacao']; ?>&nbsp;</td>
		<td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $exercicio['Exercicio']['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $exercicio['Exercicio']['id'])); ?>
		<?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $exercicio['Exercicio']['id']), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $exercicio['Exercicio']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Exercicio', true), array('action' => 'add')); ?></li>
</ul>
</div>
