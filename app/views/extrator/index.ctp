<div class="consolidados index">
		<?php echo $this->Form->create('consolidado', array('url' => "/extrator")); ?>
			<?php echo $this->Form->input('anos', array('label' => __('Ano:', true) . '<b><font color="red"> *</font></b>', 'multiple'=>false, 'data-placeholder'=>'Selecione um ano.', 'class' => '', 'type' => 'select', 'options' => $anos)); ?>	
			<?php echo $this->Form->input('co_grupos', array('label' => __('Ano:', true) . '<b><font color="red"> *</font></b>', 'multiple'=>false, 'data-placeholder'=>'Selecione um grupo.', 'class' => '', 'type' => 'select', 'options' => $grupos)); ?>		
    	<?php echo $this->Form->end(); ?>
	<table cellpadding="0" cellspacing="0" style="background-color: white" class="table table-hover table-bordered table-striped" id="tbConsolidado">
		<tr>
			<th><?php echo __("Ano");?></th>
			<th><?php echo __("Valor");?></th>
		</tr>
		<?php if( count($consolidados) > 0 ): ?>
			<?php foreach( $consolidados as $consolidado ): ?>
			<tr>
				<td><?php echo $consolidado->ano; ?></td>
				<td><?php echo number_format($consolidado->valor, 2, ",", "."); ?></td>
			</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="2">
					Nenhum registro encontrado.
				</td>
			</tr>
		<?php endif; ?>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#consolidadoCoGrupos,#consolidadoAnos").change(function(){
			console.log($(this).val());
			$("form").submit();
		});
	});	
</script>