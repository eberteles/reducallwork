<?php echo $this->Form->create('Fase', array('onsubmit' => "return confirm('Ao editar essa Fase, você alterará todos os contratos e outros registros que possuem essa Fase atrelada. Deseja prosseguir com essa alteração? (Sua alteração ficará registrada em log de alterações de Fases)');"));?>

    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Fase de Tramitação'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                           <?php
                echo $this->Form->hidden('co_fase');
                echo $this->Form->input('ds_fase', array('class' => 'input-xlarge','label' => 'Descrição'));
                echo $this->Form->input('nu_dias', array('id' => 'nu_dias', 'class' => 'input-mini','label' => 'Duração da Fase em dias'));
                echo $this->Form->input('nu_horas', array('id' => 'nu_horas', 'class' => 'input-mini','label' => 'Duração da Fase em horas / minutos', 'mask'=>'99:99'));
?>
               </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

 <?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {
    $("#nu_dias").maskMoney({precision:0, allowZero:false, thousands:''});    
});

<?php echo $this->Html->scriptEnd() ?>