<?php $usuario = $this->Session->read('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Fases de Tramitação'); ?></h1></div>
    <?php
    if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fases/add')) { ?>
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left"><span class="required"
                                                      title="Required">Campos com * são obrigatórios</span></p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'fases', 'action' => 'add')); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary" title="Nova Fase"><i
                            class="icon-plus icon-white"></i> Nova Fase</a>
            </div>
        </div>
    <?php } ?>

    <?php echo $this->Form->create('Fases', array('url' => array('controller' => 'fases', 'action' => 'index'))); ?>

    <div class="row-fluid">
        <div class="span2">
            <div class="controls">
                <?php echo $this->Form->input('ds_fase', array('class' => 'input-large', 'type' => 'text', 'label' => __('Descrição da Fase', true))); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
            </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                Limpar
            </button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <tr id="titulo">
            <th><?php __('Fase'); ?></th>
            <th>Conclusão da Fase</th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        foreach ($fases as $fase):
            ?>
            <tr>
                <td><?php echo $fase['Fase']['ds_fase']; ?>&nbsp;</td>
                <td><?php echo $this->Print->getMinutosToData($fase['Fase']['mm_duracao_fase']); ?>&nbsp;</td>
                <td class="actions">
                    <div class="btn-group">
                        <?php
                        echo $this->element('actions', array('id' => $fase['Fase']['co_fase'], 'class' => 'btn btn-small', 'local_acao' => 'fases/')) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>


<?php echo $this->Html->scriptStart() ?>

$(function(){
//função que faz a linha descer na tabela
$(".descer").click(function(){

var objLinha    = $(this).parent().parent().parent();
var seq_atual   = $(objLinha).attr('nu_sequencia');
var co_atual    = $(objLinha).attr('co_fase');
var seq_prox    = $(objLinha).next().attr('nu_sequencia');
var co_prox     = $(objLinha).next().attr('co_fase');

if(seq_prox > seq_atual) {

var url_resp = "<?php echo $this->Html->url(array('controller' => 'fases', 'action' => 'trocar')); ?>/" + co_atual + "/" + seq_prox + "/" + co_prox + "/" + seq_atual;
$.ajax({
type:"GET",
url:url_resp,
success:function(result){
$('body').append(result);
$(objLinha).attr('nu_sequencia', seq_prox);
$(objLinha).next().attr('nu_sequencia', seq_atual);
$(objLinha).next().after(objLinha); //Abaixo da linha clicada, insere a linha clicada
}
})
}

});

//função que faz a linha subir na tabela
$(".subir").click(function(){

var objLinha    = $(this).parent().parent().parent();
var seq_atual   = $(objLinha).attr('nu_sequencia');
var co_atual    = $(objLinha).attr('co_fase');
var seq_ant     = $(objLinha).prev().attr('nu_sequencia');
var co_ant      = $(objLinha).prev().attr('co_fase');

if(seq_atual > seq_ant) {

var url_resp = "<?php echo $this->Html->url(array('controller' => 'fases', 'action' => 'trocar')); ?>/" + co_atual + "/" + seq_ant + "/" + co_ant + "/" + seq_atual;
$.ajax({
type:"GET",
url:url_resp,
success:function(result){
$('body').append(result);
$(objLinha).attr('nu_sequencia', seq_ant);
$(objLinha).prev().attr('nu_sequencia', seq_atual);
$(objLinha).prev().before(objLinha); //Acima da linha clicada, insere a linha clicada
}
})
}

});

});

<?php echo $this->Html->scriptEnd() ?>
