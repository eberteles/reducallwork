<div class="financeiros form"><?php echo $this->Form->create('Financeiro');?>
<fieldset><legend><?php __('Financeiro'); ?></legend> <?php
echo $this->Form->input('co_financeiro');
echo $this->Form->input('co_contrato');
echo $this->Form->input('vl_atual');
echo $this->Form->input('vl_mensal');
echo $this->Form->input('ds_observacao');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('Financeiro.id')), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $this->Form->value('Financeiro.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Financeiros', true), array('action' => 'index'));?></li>
</ul>
</div>
