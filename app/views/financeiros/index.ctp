<div class="financeiros index">
<!-- h2>< ?php __('Financeiros');?></h2 -->
<fieldset>
	<legend>Fornecedor: <?php echo $this->Print->cnpj($fornecedor['nu_cnpj']) . " - " . $fornecedor['no_razao_social']; ?></legend>
        <?php echo $this->Form->create('Financeiro', array('url' => "/financeiros/index/$co_contrato"));?>
        
<table cellpadding="0" cellspacing="0">
    <tr>
        <td width="120px"><label for="FinanceiroFgNotaFiscal">Nota Fiscal</label><?php echo $this->Form->checkbox('fg_notafiscal'); ?></td>
        <td width="150px"><?php echo $this->Form->input('tp_quitacao', array('type' => 'select', 'empty' => 'Selecione...', 'label'=>'Situação', 'options' => $tpSituacao)); ?></td>
    	<td width="50px"><?php echo $this->Form->end(__('Filtrar', true));?></td>
        <td>&nbsp;</td>
    </tr>
</table>
        
<table cellpadding="0" cellspacing="0">
	<tr>
		<th>NF - Série</th>
		<th>Dt Quitação</th>
		<th>Dt Vencimento</th>
		<th>Situação</th>
		<th>Vl Nota</th>
		<th>Vl Parcela</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($financeiros as $financeiro):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $this->Print->notaFiscal($financeiro['Financeiro']['num_nf'], $financeiro['Financeiro']['nr_serie']); ?>&nbsp;</td>
		<td align="center"><?php echo $financeiro['Financeiro']['dt_quitacao']; ?>&nbsp;</td>
		<td align="center"><?php echo $financeiro['Financeiro']['dt_vencimento']; ?>&nbsp;</td>
		<td><?php echo $this->Print->tpQuitacao( $financeiro['Financeiro']['tp_quitacao']); ?>&nbsp;</td>
		<td align="right"><?php echo $this->Print->real( $financeiro['Financeiro']['vl_nf'], true ); ?>&nbsp;</td>
		<td align="right"><?php echo $this->Print->real( $financeiro['Financeiro']['vl_parcela'], true ); ?>&nbsp;</td>
                <td class="actions"><?php echo $this->Html->link($this->Html->image('novo.gif', 
                        array( 'title' => 'Novo ' . __('Pagamento', true) )), "javascript: utilizar('" . 
                            $financeiro['Financeiro']['num_nf'] . "', '" . $financeiro['Financeiro']['nr_serie'] . 
                            "', '" . $financeiro['Financeiro']['dt_quitacao'] . 
                            "', '" . $financeiro['Financeiro']['dt_vencimento'] . 
                            "', '" . $this->Print->real( $financeiro['Financeiro']['vl_nf'], true ) . 
                            "', '" . $this->Print->real( $financeiro['Financeiro']['vl_parcela'], true ) . "');", array('escape' => false)); ?></td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</fieldset>
</div>

<?php echo $this->Html->scriptStart() ?>

        function utilizar(nf, serie, quitacao, vencimento, vlnota, vlparcela) {
            parent.document.getElementById('PagamentoNuNotaFiscal').value = nf;
            parent.document.getElementById('PagamentoNuSerieNf').value = serie;
            parent.document.getElementById('PagamentoDtPagamento').value = quitacao;
            parent.document.getElementById('PagamentoDtVencimento').value = vencimento;
            parent.document.getElementById('PagamentoVlPagamento').value = vlnota;
            var dtVencimento = vencimento.split("/");
            parent.document.getElementById('PagamentoNuMesPagamento').value = dtVencimento[1];
            parent.document.getElementById('PagamentoNuAnoPagameto').value = dtVencimento[2];
            if(quitacao.length == 10) {
                parent.document.getElementById('PagamentoVlPago').value = vlparcela;
                parent.document.getElementById('jaRealizadoSim').checked = 1;
                parent.document.getElementById('PagamentoDtPagamento').disabled = false;
            } else {
                parent.document.getElementById('jaRealizadoNao').checked = 1;
                parent.document.getElementById('PagamentoDtPagamento').disabled = true;
            }
            parent.$("#financeiro").dialog( "close" );
        }

<?php echo $this->Html->scriptEnd() ?>