
<?php echo $this->Form->create('Fiscal'); ?>

    <div class="row-fluid">
 <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">

            <div class="span12 ">
                <h3><?php __('Fiscal'); ?></h3>

                <div class="body-box">
                    <div class="span4">
                        <dl class="dl-horizontal">
                            <?php
                            echo $this->Form->input('co_fiscal');
                            echo $this->Form->input('nu_cpf_fiscal', array('class' => 'input-xlarge', 'label' => '*CPF', 'mask' => '999.999.999-99', 'id' => 'CPF'));
                            echo $this->Form->input('no_fiscal', array('class' => 'input-xlarge', 'label' => '*Nome'));
                            ?>
                            <?php
                            echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => '*E-mail', 'id' => 'Email'));
                            echo $this->Form->input('co_setor', array('class' => 'input-xlarge', 'label' => '*Setor', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $setores));
                           
                            ?>
                        </dl>
                    </div>
                    <div class="span4">
                        <dl class="dl-horizontal">

                            
                            <?php
                             echo $this->Form->input('nu_siape', array('class' => 'input-xlarge', 'label' => '*Matrícula'));
//echo $this->Form->input('nu_portaria', array('label' => 'Portaria'));
                            echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => '*Telefone', 'mask' => '(99) 9999-9999?9'));
                            echo $this->Form->input('st_fiscal', array('class' => 'input-xlarge', 'label' => '*Situação', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $situacoes));
                            echo $this->Form->input('ds_observacao', array('class' => 'input-xlarge', 'label' => '*Observação', 'maxLength' => '255', 'type' => 'texarea', 'cols' => '40', 'rows' => '4'));
                            ?>
                        </dl>
                    </div>
                    </div>
            </div>

        </div>

      <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar" id="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>
    </div>

