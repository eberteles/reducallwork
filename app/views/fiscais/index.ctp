
	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Fiscais'); ?></h1></div>
		
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'fiscais', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Usuário"><i class="icon-plus icon-white"></i> Novo <?php __('Fiscal'); ?></a> 
          </div>
        </div>
		
	<?php echo $this->Form->create('Fiscal');?>
	
<div class="row-fluid">
		<div class="span3">
    		<div class="controls">
    			<?php echo $this->Form->input('no_fiscal', array('class' => 'input-xlarge','label' => __('Fiscal', true))); ?>
    		</div>
    	</div>
    </tr>
</div>
<div class="form-actions">
     <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button> 
        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
        <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
     </div>
</div>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th><?php echo $this->Paginator->sort('Setor', 'Setor.ds_setor');?></th>
		<th><?php echo $this->Paginator->sort('CPF', 'nu_cpf_fiscal');?></th>
		<th><?php echo $this->Paginator->sort('Nome', 'no_fiscal');?></th>
		<th><?php echo $this->Paginator->sort('Email', 'ds_email');?></th>
		<th><?php echo $this->Paginator->sort('Matrícula', 'nu_siape');?></th>
		<!-- th>< ?php echo $this->Paginator->sort('Portaria', 'nu_portaria');?></th-->
		<th><?php echo $this->Paginator->sort('Telefone', 'nu_telefone');?></th>
		<th><?php echo $this->Paginator->sort('Situação', 'st_fiscal');?></th>

		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($fiscais as $fiscal):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $fiscal['Setor']['ds_setor']; ?>&nbsp;</td>
		<td><?php echo $this->Print->cpf( $fiscal['Fiscal']['nu_cpf_fiscal'] ); ?>&nbsp;</td>
		<td><?php echo $fiscal['Fiscal']['no_fiscal']; ?>&nbsp;</td>
		<td><?php echo $fiscal['Fiscal']['ds_email']; ?>&nbsp;</td>
		<td><?php echo $fiscal['Fiscal']['nu_siape']; ?>&nbsp;</td>
		<!-- td>< ?php echo $fiscal['Fiscal']['nu_portaria']; ?>&nbsp;</td-->
		<td><?php echo $this->Print->telefone( $fiscal['Fiscal']['nu_telefone'] ); ?>&nbsp;</td>
		<td><?php echo $this->Print->type( $fiscal['Fiscal']['st_fiscal'], $situacoes); ?>&nbsp;</td>

                <td class="actions"><?php $id = $fiscal['Fiscal']['co_fiscal']; ?>
                    <div class="btn-group acoes">	
                        <?php echo $this->Html->link('<i class="icon-pencil"></i>', array( 'action' => 'edit', $id), array('escape' => false, 'class' => 'btn')); ?>
                        <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array( 'action' => 'delete', $id), array('escape' => false,'class' => 'btn btn-danger'), sprintf(__('Tem certeza de que deseja excluir este registro?', true), $id)); ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>


