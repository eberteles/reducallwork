<?php echo $this->Form->create('FiscalTipo', array('url' => array('controller' => 'fiscais_tipos', 'action' => 'add'))); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Tipo de Fiscal'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_fiscais_tipos');
                echo $this->Form->input('no_fiscais_tipos', array('type' => 'text', 'class' => 'input-xlarge', 'label' => 'Nome'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>
