<?php echo $this->Form->create('Fluxo');?>

    <div class="row-fluid">
<b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Editar Passo '); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
            <?php
                echo $this->Form->input('nu_sequencia');
                echo $this->Form->input('co_fase', array('class' => 'chosen-select input-xxlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Fase', true), 'options' => $fases));
                echo $this->Form->input('co_setor', array('class' => 'chosen-select input-xxlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Setor', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false));
                echo $this->Form->input('tp_operador', array('class' => 'chosen-select input-xxlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Verificação', true), 'options' => $tpOperadores));
                echo $this->Form->input('nu_sequencia_um', array('class' => 'chosen-select input-xxlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Sim / Verificação 1', true), 'options' => $fluxos));
                echo $this->Form->input('nu_sequencia_dois', array('class' => 'chosen-select input-xxlarge','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Não / Verificação 2', true), 'options' => $fluxos));
            ?>
              </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

<?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {
    $("#FluxoNuSequencia").maskMoney({precision:0, allowZero:false, thousands:''});    
});

<?php echo $this->Html->scriptEnd() ?>