<?php 
    $usuario = $this->Session->read ('usuario'); 
    //echo $this->Html->script( 'jsPlumb-2.0.4' );
?>
<style>
    .anchor{
      display: block;
      height: 100px; /*same height as header*/
      margin-top: -100px; /*same height as header*/
      visibility: hidden;
    }
</style>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Workflow Processual'); ?></h1></div>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fluxos/add') ) { ?>
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><span class="required" title="Required">Campos com * são obrigatórios</span> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'fluxos', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Passo"><i class="icon-plus icon-white"></i> Novo Passo</a> 
          </div>
        </div>
      <?php } ?>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr id="titulo">
		<th>Passo</th>
		<th>Verificação</th>
		<th>Fase</th>
		<th>Setor</th>
                <th colspan="2">Passos Seguintes</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php 
        foreach ($fluxos as $fluxo):
	?>
	<tr>
                <td><span class="anchor" id="<?php echo $fluxo['Fluxo']['nu_sequencia']; ?>"></span><?php echo $fluxo['Fluxo']['nu_sequencia']; ?></td>
                <td><?php if($fluxo['Fluxo']['tp_operador'] > 0) {
                    if (isset($tpOperadores[$fluxo['Fluxo']['tp_operador']])) {
                        echo $tpOperadores[$fluxo['Fluxo']['tp_operador']]['nome'];
                    } 
                }?>&nbsp;
                </td>
                <td><?php echo $fluxo['Fase']['ds_fase']; ?>&nbsp;</td>
                <td><?php echo $fluxo['Setor']['ds_setor']; ?>&nbsp;</td>
                <td><?php 
                    if($fluxo['Fluxo']['nu_sequencia_um'] > 0) {
                        echo $fluxo['Fluxo']['nu_sequencia_um'];
                    } else {
                        echo 'Fim do Fluxo.';
                    }
                    ?>&nbsp;
                </td>
                <td><?php 
                    if($fluxo['Fluxo']['nu_sequencia_dois'] > 0) {
                        echo $fluxo['Fluxo']['nu_sequencia_dois'];
                    }
                    ?>&nbsp;
                </td>
		<td class="actions">
                    <div class="btn-group">
                        <?php echo $this->element( 'actions', array( 'id' => $fluxo['Fluxo']['nu_sequencia'], 'class' => 'btn btn-small', 'local_acao' => 'fluxos/' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>

            
</div>


<?php echo $this->Html->scriptStart() ?>
    $('a[href*=#]').on('click', function(event){     
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
    });
<?php echo $this->Html->scriptEnd() ?>
