<div class="contratos form">
    
    <div class="acoes-formulario-top clearfix">
        <div class="span4">
            <div class="pull-left btn-group">
                <input type="hidden" id="co_fornecedor_detalhe" value="<?php echo $fornecedor["Fornecedor"]["co_fornecedor"]; ?>">
                <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'edit' , $id)); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary" title="Editar"><i class="icon-pencil icon-white"></i>
                    Editar
                </a>
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>
    </div>

    <div id="accordion1" class="accordion">
        <div class="accordion-group">

            <div class="accordion-heading">
                <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" id="identificacao">
                    FORNECEDOR: <?php echo up($fornecedor["Fornecedor"]["no_razao_social"]); ?>
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="span12 ">
                            <div class="body-box">
                                <div class="span6">
                                    <dl class="dl-horizontal">

                                        <dt>Tipo de Fornecedor: </dt>
                                        <dd><?php if($fornecedor["Fornecedor"]["tp_fornecedor"] == 'J' || $fornecedor["Fornecedor"]["tp_fornecedor"] == 'j'){ echo "Pessoa Jurídica"; } else { echo "Pessoa Física"; } ?></dd>

                                        <dt>Razão Social / Nome: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["no_razao_social"])) { echo $fornecedor["Fornecedor"]["no_razao_social"]; } else { echo '---'; } ?></dd>

                                        <dt>CNPJ / CPF: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["nu_cnpj"])) { echo $this->Print->cnpj($fornecedor["Fornecedor"]["nu_cnpj"]); } else { echo '---'; } ?></dd>

                                        <dt>CEP: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["nu_cep"])) { echo $fornecedor["Fornecedor"]["nu_cep"]; } else { echo '---'; } ?></dd>

                                        <dt>Município: </dt>
                                        <dd><?php if(!empty($fornecedor["Municipio"]["ds_municipio"])) { echo $fornecedor["Municipio"]["ds_municipio"]; } else { echo '---'; } ?></dd>

                                        <dt>Endereço: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["ds_endereco"])) { echo $fornecedor["Fornecedor"]["ds_endereco"]; } else { echo '---'; } ?></dd>

                                    </dl>
                                </div>
                                <div class="span6">
                                    <dl class="dl-horizontal">

                                        <dt>Indicador de Valor: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["indicador_valor"])) { echo $fornecedor["Fornecedor"]["indicador_valor"]; } else { echo '---'; } ?></dd>

                                        <dt>Indicador de Importância: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["indicador_importancia"])) { echo $fornecedor["Fornecedor"]["indicador_importancia"]; } else { echo '---'; } ?></dd>

                                        <dt>É uma OCS ou PSA? </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["ocs_psa"])) { echo $fornecedor["Fornecedor"]["ocs_psa"]; } else { echo '---'; } ?></dd>

                                        <dt>UF: </dt>
                                        <dd><?php if(!empty($fornecedor["Fornecedor"]["sg_uf"])) { echo $fornecedor["Fornecedor"]["sg_uf"]; } else { echo '---'; } ?></dd>

<!--                                        <dt>Número: </dt>-->
<!--                                        <dd>--><?php //echo $fornecedor["Fornecedor"][""]; ?><!--</dd>-->

                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php
        $inativoall = true;
        $abaAtiva    = null;

        $abas = array();

        $abas[] = array( 'fsContatos', 'Contatos', true, $this->Html->url(array('controller' => 'contatos', 'action' => 'iframe', $id)) );
        $abas[] = array( 'fsLocaisAtendimento', 'Locais de Atendimento', true, $this->Html->url(array('controller' => 'locais_atendimento', 'action' => 'iframe', $id)) );
        $abas[] = array( 'fsReclamacoes', 'Reclamações', true, $this->Html->url(array('controller' => 'reclamacoes', 'action' => 'iframe', $id)) );
        $abas[] = array( 'fsElogios', 'Elogios', true, $this->Html->url(array('controller' => 'elogios', 'action' => 'iframe', $id)) );
    ?>

    <div id="accordion2" class="accordion">
        <div class="accordion-group">

            <div class="accordion-heading">
                <a href="#collapseComp" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle" id="complemento">
                    Complemento
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseComp" style="height: auto;">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="box-tabs-white">
                            <ul class="nav nav-tabs" id="myTab">
                                <?php echo $aba->render( $abas, $inativoall, $abaAtiva, 'carregarAba' ); ?>
                            </ul>
                            <div class="tab-content" id="myTabContent"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<script type="text/javascript">
    function carregarAba(element,aba){
        $("#myTab > li").each(function(){
            $(this).removeClass('active');
        });
        element.parent().addClass('active');
        $("#myTabContent").load(element.attr('req'));
    }

    $(document).ready(function(){
        $("#myTab > li > a").first(function(){
            $(this).click();
        });
    });

    $( function() {
        carregarElogios();
        carregarReclamacoes();
        carregarContatos();
        carregarLocaisAtendimento();
    });

    function carregarElogios(){
        <?php echo $this->JqueryEngine->request( "/elogios/iframe/$id", array( 'update' => '#fsElogios' ) ); ?>
    }

    function carregarReclamacoes(){
        <?php echo $this->JqueryEngine->request( "/reclamacoes/iframe/$id", array( 'update' => '#fsReclamacoes' ) ); ?>
    }

    function carregarContatos(){
        <?php echo $this->JqueryEngine->request( "/contatos/iframe/$id", array( 'update' => '#fsContatos' ) ); ?>
    }

    function carregarLocaisAtendimento(){
        <?php echo $this->JqueryEngine->request( "/locais_atendimento/iframe/$id", array( 'update' => '#fsLocaisAtendimento' ) ); ?>
    }
</script>
