<?php $usuario = $this->Session->read('usuario'); ?>
<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Fornecedores'); ?></h1></div>

    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
        <div class="pull-right btn-group">
            <?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/add')): ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'add')); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Novo Fornecedor'); ?>"><i
                            class="icon-plus icon-white"></i> <?php __('Novo Fornecedor'); ?></a>
            <?php endif; ?>
        </div>
    </div>
    <?php echo $this->Form->create('Fornecedor'); ?>
    <div class="row-fluid">
        <div class="span2">
            <div class="controls">
                <?php echo $this->Form->input('tp_fornecedor', array('class' => 'input-large', 'type' => 'select', 'label' => __('Tipo de Fornecedor', true), 'empty' => 'Selecione..', 'options' => $this->Modulo->getTiposFornecedores())); ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php echo $this->Form->input('nu_cnpj', array('class' => 'input-xsmall', 'label' => __('CNPJ / CPF', true), 'maxlength' => 18)); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('no_razao_social', array('class' => 'input-xlarge', 'label' => __('Razão Social / Nome', true))); ?>
            </div>
        </div>
        <?php if ($this->Modulo->isCamposFornecedor('co_area') == true && $this->Modulo->getHasNewFornecedores() == false): ?>
            <div class="span3">
                <div class="controls">
                    <?php echo $this->Form->input('co_area', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Área', true), 'options' => $imprimirArea->getArrayAreas($areas), 'escape' => false)); ?>
                </div>
            </div>
        <?php elseif ($this->Modulo->getHasNewFornecedores()): ?>
            <div class="span2">
                <div class="controls">
                    <?php echo $this->Form->input('indicador_valor', array('class' => 'input-large', 'type' => 'select', 'empty' => 'Selecione', 'label' => 'Indicador de Valor', 'options' => $indValores)); ?>
                </div>
            </div>
            <div class="span2">
                <div class="controls">
                    <?php echo $this->Form->input('indicador_importancia', array('class' => 'input-large', 'type' => 'select', 'empty' => 'Selecione', 'label' => 'Indicador de Importância', 'options' => $indImportancia)); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                Limpar
            </button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>

            <th><?php echo __('CNPJ / CPF'); ?></th>
            <th><?php echo __('Razão Social / Nome'); ?></th>

            <?php if ($this->Modulo->isCamposFornecedor('co_area')) : ?>
                <th><?php echo __('Área'); ?></th>
            <?php endif; ?>

            <?php if ($this->Modulo->getHasNewFornecedores()): ?>
                <th>Indicador de Valor &nbsp;&nbsp;&nbsp;<a class="alert-tooltip"
                                                            title="1)Alto: Fornecedor com Alto valor contratual. 2)Médio: Fornecedor com Médio valor contratual. 3)Pequeno: Fornecedor com Pequeno valor contratual."><i
                                class="icon-info-sign"></i></a></th>
                <th>Indicador de Importância &nbsp;&nbsp;&nbsp;<a class="alert-tooltip"
                                                                  title="1) Imprescindível: Fornecedor de importância imprescindível. 2) Necessária: Fornecedor de importância necessária. 3) Comum: Fornecedor de importância comum;"><i
                                class="icon-info-sign"></i></a></th>
            <?php else: ?>
                <th><?php echo __('E-mail'); ?></th>
                <th><?php echo __('Telefone'); ?></th>
            <?php endif; ?>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($fornecedores as $fornecedor):
            ?>
            <tr>
                <?php if ($this->Modulo->getHasNewFornecedores()): ?>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'detalha' . '/' . $fornecedor['Fornecedor']['co_fornecedor'])); ?>"><?php echo $this->Print->cnpjCpf($fornecedor['Fornecedor']['nu_cnpj'], $fornecedor['Fornecedor']['tp_fornecedor']); ?>
                            &nbsp;</a></td>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'detalha' . '/' . $fornecedor['Fornecedor']['co_fornecedor'])); ?>"><?php echo $fornecedor['Fornecedor']['no_razao_social']; ?>
                            &nbsp;</a></td>
                <?php else: ?>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'mostrar' . '/' . $fornecedor['Fornecedor']['co_fornecedor'])); ?>"><?php echo $this->Print->cnpjCpf($fornecedor['Fornecedor']['nu_cnpj'], $fornecedor['Fornecedor']['tp_fornecedor']); ?>
                            &nbsp;</a></td>
                    <td>
                        <a href="<?php echo $this->Html->url(array('controller' => 'fornecedores', 'action' => 'mostrar' . '/' . $fornecedor['Fornecedor']['co_fornecedor'])); ?>"><?php echo $fornecedor['Fornecedor']['no_razao_social']; ?>
                            &nbsp;</a></td>
                <?php endif; ?>

                <?php if ($this->Modulo->isCamposFornecedor('co_area')) : ?>
                    <td><?php echo $fornecedor['Area']['ds_area']; ?>&nbsp;</td>
                <?php endif; ?>

                <?php if ($this->Modulo->getHasNewFornecedores()): ?>
                    <td><?php echo $fornecedor['Fornecedor']['indicador_valor']; ?></td>
                    <td><?php echo $fornecedor['Fornecedor']['indicador_importancia']; ?></td>
                <?php else: ?>
                    <td><?php echo $fornecedor['Fornecedor']['ds_email']; ?>&nbsp;</td>
                    <td><?php echo $this->Print->telefone($fornecedor['Fornecedor']['nu_telefone']); ?>&nbsp;</td>
                <?php endif; ?>


                <td class="actions"><?php $id = $fornecedor['Fornecedor']['co_fornecedor']; ?>
                    <div class="btn-group acoes">
                        <?php
                        if ($this->Modulo->getHasNewFornecedores()) {
                            echo $this->element('actions11', array('id' => $fornecedor['Fornecedor']['co_fornecedor'], 'class' => 'btn', 'local_acao' => 'fornecedores/index'));
                        } else {
                            echo $this->element('actions8', array('id' => $fornecedor['Fornecedor']['co_fornecedor'], 'class' => 'btn', 'local_acao' => 'fornecedores/index'));
                        }
                        ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#FornecedorTpFornecedor').on('change', function () {
            $("#FornecedorNuCnpj").unmask();
            var tpFornecedor = $(this).val();
            if (tpFornecedor == 'F') {
                $('#FornecedorNuCnpj').mask('999.999.999-99');

            } else if (tpFornecedor == 'J') {
                $('#FornecedorNuCnpj').mask('99.999.999/9999-99');
            }
        });
        $('#FornecedorTpFornecedor').val('J').trigger('change');
    });
</script>
