<div class="garantias form">
    <?php  echo $this->Form->create('GarantiaModalidade', array('url' => "/garantiasModalidade/add")); ?>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar <?php __('uma Garantia'); ?> - <b>Campos com * são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'garantiasModalidade')); ?>" class="btn btn-small btn-primary" title="Listar Modalidades de Garantia">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-header widget-header-small"><h4>Modalidade de Garantia</h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                          <?php echo $this->Form->input('ds_modalidade_garantia', array('class' => 'input-xlarge', 'label' => 'Descrição')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button
                id="btnSalvarModalidadeGarantia"
                rel="tooltip"
                type="submit"
                class="btn btn-small btn-primary bt-pesquisar"
                title="Salvar modalidade de garantia">
                Salvar
            </button>
            <button id="LimparForm" rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small">Limpar</button>
            <button id="Voltar" rel="tooltip" title="Voltar" class="btn btn-small">Voltar</button>
        </div>
    </div>
</div>