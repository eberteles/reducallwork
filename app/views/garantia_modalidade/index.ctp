<?php $usuario = $this->Session->read('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1>Modalidades de Garantia</h1></div>
    <?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'garantiasModalidade/add')) : ?>
        <div class="acoes-formulario-top clearfix">
            <div class="pull-right btn-group">
                <a
                    href="<?php echo $this->Html->url(array('controller' => 'garantiasModalidade', 'action' => 'add')); ?>"
                    data-toggle="modal"
                    class="btn btn-small btn-primary"
                    title="Nova Modalidade De Garantia"><i class="icon-plus icon-white"></i>Nova Modalidade De Garantia</a>
            </div>
        </div>
    <?php endif; ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped">
        <tr>
            <th>Código</th>
            <th>Descrição</th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        foreach ($modalidades as $modalidade) : ?>
            <tr>
                <td><?php echo $modalidade['GarantiaModalidade']['co_modalidade_garantia']; ?></td>
                <td><?php echo $modalidade['GarantiaModalidade']['ds_modalidade_garantia']; ?></td>
                <td>
                    <div class="btn-group acoes">
                        <?php
                        echo $this->element('actions', array(
                                'id' => $modalidade['GarantiaModalidade']['co_modalidade_garantia'],
                                'class' => 'btn',
                                'local_acao' => 'garantiasModalidade/'
                            )
                        );
                        ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?>
    </p>
    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => ''));  echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
