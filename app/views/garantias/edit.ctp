<?php

echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="garantias form">
<?php
echo $this->Form->create('Garantia', array('url' => "/garantias/edit/$coContrato/$id"));
if ($this->Modulo->isCadastroGarantia()) :
    echo $this->Form->input('valorGarantiaPermitido', array(
        'type' => 'hidden',
        'id' => 'valorGarantiaPermitido',
        'value' => $contrato[0]['valor_garantia_permitido']
    ));
endif;
?>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar <?php __('a Garantia'); ?> <b>Campos com * são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array ('controller' => 'garantias', 'action' => 'index', $garantia, $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar <?php __('Garantias'); ?>">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-header widget-header-small"><h4>Alterar <?php __('Garantia'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                    <?php
                        echo $this->Form->hidden('co_garantia');
                        echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                        echo $this->Form->input(
                                'co_modalidade_garantia',
                                array(
                                    'class' => 'input-xlarge',
                                    'type' => 'select',
                                    'label' => 'Modalidade',
                                    'options' => $modalidades
                                )
                        );
                        if($this->Modulo->isCamposGarantia('vl_garantia')){
                            echo $this->Form->input('vl_garantia', array(
                                'label' => 'Valor (R$)',
                                'value' => $this->Print->real($garantia['vl_garantia']),
                                'error' => array('escape' => false)
                            ));
                        }
                        echo $this->Form->input('ds_observacao', array('label'=>'Observação','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                            'onKeyup'=>'$(this).limit("250","#charsLeft")',
                            'after' => '<br><span id="charsLeft">250</span> caracteres restantes.'));
                        echo '<br />';
                    ?>
                        </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('dt_inicio', array(
                                          'before' => '<div class="input-append date datetimepicker">',
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Início', 'type'=>'text'));
                              echo $this->Form->input('dt_fim', array(
                                          'before' => '<div class="input-append date datetimepicker">',
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Fim', 'type'=>'text'));
                          ?>
                            </dl>
                        </div>
                        <div class="span4">
                          <?php
                            if($this->Modulo->isCamposGarantia('no_seguradora')){
                              echo $this->Form->input('no_seguradora', array('label' => 'Seguradora'));
                            }
                            if($this->Modulo->isCamposGarantia('nu_apolice')){
                              echo $this->Form->input('nu_apolice', array('label' => 'Apólice'));
                            }
                            if($this->Modulo->isCamposGarantia('ds_endereco')){
                              echo $this->Form->input('ds_endereco', array('label' => 'Endereço','type' => 'texarea', 'rows'=>'2'));
                            }
                            if($this->Modulo->isCamposGarantia('nu_telefone')){
                              echo $this->Form->input('nu_telefone', array('label' => 'Telefone', 'mask'=>'(99) 9999-9999?9'));
                            }
                            if($this->Modulo->isCamposGarantia('nu_registro_siafi')){
                              echo $this->Form->input('nu_registro_siafi', array('label' => 'Número do registro no SIAFI'));
                            }
                          ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button id="btnSalvarGarantia" rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar <?php __('Garantia'); ?>"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>       
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#GarantiaNuRegistroSiafi").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        if ($("#GarantiaVlGarantia").val() == '0,00') {
            $("#GarantiaVlGarantia").val('');
        }
    });

    $("#GarantiaVlGarantia").maskMoney({thousands: '.', decimal: ','});

    $('#GarantiaVlGarantia').on('change', function () {
        var limitePermitido = parseFloat($('#valorGarantiaPermitido').val()),
                valorGarantia = $(this).val();

        if (!valorGarantia) {
            $('#error-valor-garantia').hide();
            return;
        }

        valorGarantia = parseFloat(valorGarantia.replace(/[^0-9,]/, '').replace(/,/, '.'));

        if (valorGarantia < limitePermitido) {
            $('#error-valor-garantia').show();
        } else {
            $('#error-valor-garantia').hide();
        }
    });

    $('#btnSalvarGarantia').on('click', function (e) {
        if ($('#error-valor-garantia').is(':visible')) {
            e.preventDefault();
            return false;
        }
    });
</script>
