<?php 
$usuario = $this->Session->read ('usuario'); 
$hasPenalidade = $this->Session->read ('hasPenalidade');
?>
<div class="garantias index">
<!-- h2>< ?php __('Garantias');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbGarantia">
	<tr>
	<?php
	//gambiarra da braba ahead. inicialize a variável gambira como qualquer outra coisa para que os campos voltem a aparecer.
    	$gambira = 1;
    	?>

		<th><?php __('Modalidade');?></th>
            <?php if($this->Modulo->isCamposGarantia('vl_garantia')){ ?>
		<th><?php __('Valor');?></th>
            <?php } ?>
		<th><?php __('Data Início');?></th>
		<th><?php __('Data Fim');?></th>
            <?php if($this->Modulo->isCamposGarantia('no_seguradora') && $gambira !== 1){ ?>
		<th><?php __('Seguradora');?></th>
            <?php } ?>
            <?php if($this->Modulo->isCamposGarantia('nu_apolice') && $gambira !== 1){ ?>
		<th><?php __('Apólice');?></th>
            <?php } ?>
            <?php if($this->Modulo->isCamposGarantia('nu_telefone') && $gambira !== 1){ ?>
		<th><?php __('Telefone');?></th>
            <?php } ?>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;



	foreach ($garantias as $garantia):

	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}

	?>
	<tr <?php echo $class;?>>
            <td><a href="/garantias/edit/<?php echo $garantia['Garantia']['co_garantia']; echo '/' . $coContrato; ?>/readonly"><?php echo $garantia['GarantiaModalidade']['ds_modalidade_garantia']; ?>&nbsp;</a></td>
            <?php if($this->Modulo->isCamposGarantia('vl_garantia') ){ ?>
		<td><?php echo $this->Formatacao->moeda( $garantia['Garantia']['vl_garantia'] ); ?>&nbsp;</td>
            <?php } ?>
		<td><?php echo $garantia['Garantia']['dt_inicio']; ?>&nbsp;</td>
		<td><?php echo $garantia['Garantia']['dt_fim']; ?>&nbsp;</td>
            <?php if($this->Modulo->isCamposGarantia('no_seguradora') && $gambira !== 1){ ?>
		<td><?php echo $garantia['Garantia']['no_seguradora']; ?>&nbsp;</td>
            <?php } ?>
            <?php if($this->Modulo->isCamposGarantia('nu_apolice') && $gambira !== 1){ ?>
		<td><?php echo $garantia['Garantia']['nu_apolice']; ?>&nbsp;</td>
            <?php } ?>
            <?php if($this->Modulo->isCamposGarantia('nu_telefone') && $gambira !== 1){ ?>
		<td><?php echo $garantia['Garantia']['nu_telefone']; ?>&nbsp;</td>
            <?php } ?>
		<td class="actions"><?php $id = $garantia['Garantia']['co_garantia']; ?>

			<div class="btn-group acoes">
                            <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Garantia" style="display: inline-block;"></i></a>
                            <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'garantias/' ) ) ?>



			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'garantias/add', $hasPenalidade) ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a class="btn btn-small btn-primary btn-add-garantia">Adicionar</a>
<!--            <a href="--><?php //echo $this->Html->url(array('controller' => 'garantias', 'action' => 'add', $coContrato)); ?><!--" class="btn btn-small btn-primary">Adicionar</a>-->
          </div>
        </div>

</div>
<?php  } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar <?php __('Garantia'); ?></h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<script>
    $(document).ready(function () {
        $('.btn-add-garantia').click(function () {
            var garantia = '<?php echo $pcGarantia; ?>';
            if (garantia === "0" ) {
                alert('Este contrato não permite garantias. \n Verificar item no contrato.');
            } else {
                window.location.href = '<?php echo $this->Html->url(array('controller' => 'garantias', 'action' => 'add', $coContrato)); ?>';
            }
        });
    });
</script>
<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

    $('#tbGarantia tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/garantia/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
