<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="garantias_suporte form">
<?php echo $this->Form->create('GarantiaSuporte', array('url' => "/garantias_suporte/edit/$id/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar uma Garantia de Suporte - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'garantias_suporte', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Garantias de Suporte">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Alterar Garantia de Suporte</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('co_garantia_suporte');
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                              echo $this->Form->input('nu_serie', array('label' => __('Nº de Série', true)) );
                              echo $this->Form->input('ds_observacao', array('label'=>'Observação','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("250","#charsLeft")', 
                                'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                              echo '<br />';
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('dt_inicio', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Início', 'type'=>'text'));
                              echo $this->Form->input('dt_fim', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Fim', 'type'=>'text'));
                              echo $this->Form->input('ds_garantia_suporte', array('label'=>'Descrição','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("250","#charsLeft2")', 
                                'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'));
                              echo '<br />';
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Garantia de Suporte"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>