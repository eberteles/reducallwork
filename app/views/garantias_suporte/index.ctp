<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="garantias_suporte index">
<!-- h2>< ?php __('GarantiaSuportes');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbGarantiaSuporte">
	<tr>
		<th><?php __('Nº de Série');?></th>
		<th><?php __('Data Início');?></th>
		<th><?php __('Data Fim');?></th>
		<th><?php __('Observação');?></th>
		<th><?php __('Descrição');?></th>

		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($garantias_suporte as $garantia):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $garantia['GarantiaSuporte']['nu_serie']; ?>&nbsp;</td>
		<td><?php echo $garantia['GarantiaSuporte']['dt_inicio']; ?>&nbsp;</td>
		<td><?php echo $garantia['GarantiaSuporte']['dt_fim']; ?>&nbsp;</td>
		<td><?php echo $garantia['GarantiaSuporte']['ds_observacao']; ?>&nbsp;</td>
		<td><?php echo $garantia['GarantiaSuporte']['ds_garantia_suporte']; ?>&nbsp;</td>
		<td class="actions"><?php $id = $garantia['GarantiaSuporte']['co_garantia_suporte']; ?> 

                    <div class="btn-group acoes">	
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Garantia de Suporte" style="display: inline-block;"></i></a>
                        <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'garantias_suporte/index' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'garantias_suporte/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'garantias_suporte', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a> 
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Garantias de Suporte</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

    $('#tbGarantiaSuporte tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/garantia_suporte/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });
    
<?php echo $this->Html->scriptEnd() ?>