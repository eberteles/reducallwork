<?php echo $this->Form->create('Graduacao', array('url' => "/graduacao/edit"));?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Dados do Posto / Graduação</h4></div>
            <div class="widget-body">
              <div class="widget-main">
              <?php
                echo $this->Form->input('co_graduacao');
                echo $this->Form->input('ds_graduacao', array('class' => 'input-xlarge','label' => 'Descrição do Posto / Graduação','maxLength' => '50'));
              ?>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

