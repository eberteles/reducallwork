<!-- Vigência de contratos -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
	["Element", "Density", { role: "style" } ],
	["Serviços", 10, "green"],
	["Farmácia", 30, "orange"],
	["Escritório", 20, "red"],
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
	title: "Vigência dos <?php __('Contratos'); ?>",
	width: 500,
	height: 300,
	bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
      chart.draw(view, options);
  }
  </script>

 
<!-- Relógios -->
 <script type='text/javascript'>
      google.load('visualization', '1', {packages:['gauge']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['10 Dias', 10],
          ['20 Dias', 25],
          ['30 Dias', 50]
        ]);

        var options = {
          width: 370, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

<!-- Relógio específico 1 -->
 <script type='text/javascript'>
      google.load('visualization', '1', {packages:['gauge']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Serviço 1', 10],
          ['Serviço 2', 80],
          ['Serviço 3', 20]
        ]);

        var options = {
          width: 370, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('relogio1_div'));
        chart.draw(data, options);
      }
    </script>

<!-- Empenhos -->
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['2013', 'Empenhado', 'A_empenhar'],
          ['Jan',  1000,      400],
          ['Fev',  1170,      460],
          ['Mar',  660,       1120],
          ['Abr',  1000,       2500],
          ['Mai',  200,       2000],
          ['Jun',  500,       1120],
          ['Jul',  500,       800],
          ['Ago',  1000,       1225],
          ['Set',  2000,       2000],
          ['Out',  800,       1000],
          ['Nov',  1100,       1120],
          ['Dez',  1030,      540]
        ]);

        var options = {
          title: 'Exercício Janeiro - Dezembro',
          hAxis: {title: '2013', titleTextStyle: {color: 'Blue'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('empenho1_div'));
        chart.draw(data, options);
      }
    </script>

<!-- Empenhos pagos -->
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['2013', 'Empenhados', 'Pagos'],
          ['Jan',  800,      1000],
          ['Fev',  2170,      460],
          ['Mar',  1160,       2120],
          ['Abr',  500,       1500],
          ['Mai',  1200,       3000],
          ['Jun',  2500,       120],
          ['Jul',  1500,       1800],
          ['Ago',  1500,       2225],
          ['Set',  800,       3000],
          ['Out',  1800,       2000],
          ['Nov',  1100,       820],
          ['Dez',  1030,      1540]
        ]);

        var options = {
          title: 'Exercício Janeiro - Dezembro',
          hAxis: {title: '2013', titleTextStyle: {color: 'Blue'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('empenho2_div'));
        chart.draw(data, options);
      }
    </script>

<!-- Pagamentos -->
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['2013', 'Total', 'Atrasados'],
          ['Jan',  1800,      100],
          ['Fev',  1400,      460],
          ['Mar',  1110,       120],
          ['Abr',  1500,       100],
          ['Mai',  4010,       1000],
          ['Jun',  1200,       120],
          ['Jul',  2100,       800],
          ['Ago',  2100,       225],
          ['Set',  2800,       0],
          ['Out',  2100,       200],
          ['Nov',  1100,       820],
          ['Dez',  1030,      150]
        ]);

        var options = {
          title: 'Vida financeira',
          hAxis: {title: '2013',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('pagamento0_div'));
        chart.draw(data, options);
      }
    </script>


<!-- Pagamentos em atraso -->
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Atrasos',     11],
          ['Pendências',      20]
        ]);

        var options = {
          title: '<?php echo __('Pagamentos'); ?> em atraso (no exercício)'
        };

        var chart = new google.visualization.PieChart(document.getElementById('pagamento1_div'));
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Pago',     11],
          ['Não pago',  5]
        ]);

        var options = {
          title: 'Saldo de <?php echo __('Pagamento'); ?> (no exercício)'
        };

        var chart = new google.visualization.PieChart(document.getElementById('pagamento2_div'));
        chart.draw(data, options);
      }
    </script>



  </head>

  <body>

    <h4> Vigência de <?php __('Contratos'); ?> </h4>
    <!--Div vigencia dos contratos-->
    <div id="barchart_values" class="span6"></div>
    
    <div class="span5">
            <p>Serviços:</p>
            <div id='relogio1_div'></div>
    </div>

    <!-- Relógios -->
    <div class="span5">
        <p><?php __('Contratos'); ?> vencendo em:</p>
        <div id='chart_div'></div>
    </div>

    
    <div class="clearfix"></div>

    <h4> Empenho </h4>
    <div class="span11">
        <div id='empenho1_div'></div>
    </div>

    <div class="span11">
        <select name="select" id="select">
                <option>2013</option>
                <option>2012</option>
                <option>2011</option>
                <option>2010</option>
                <option>2009</option>
        </select>
    </div>

    <div class="span11">
        <div id='empenho2_div'></div>
    </div>

    <div class="clearfix"></div>

    <h4> <?php echo __('Pagamentos'); ?> </h4>

    <div class="span11">
        <div id='pagamento0_div'></div>
    </div>

    <div class="span6">
        <div id='pagamento1_div'></div>
    </div>
    <div class="span5">
        <div id='pagamento2_div'></div>
    </div>
    <div class="clearfix"></div>

  </body>