﻿<?php
    $usuario = $this->Session->read ('usuario');
?>

<div class="grupo_auxiliares form">
	                        
  <div class="acoes-formulario-top clearfix" >
      Lei de Acesso à Informação - LAI, Lei Federal n° 12.527 de 18 de novembro de 2011.
            <div class="span4">
                <div class="pull-left btn-group">
                    <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_gax_e') ) { ?>
                            <a href="<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'edit' , $id)); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Editar"><i class="icon-pencil icon-white"></i> Editar</a> 
                    <?php } ?>
                            <!-- a href="#view_impressao" class="v_impressao btn btn-small btn-primary" data-toggle="modal"><i class="icon-print"></i> Imprimir</a--> 
                            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
             </div>
    </div>
    
    <div id="accordion1" class="acordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseIdent" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" id="identificacao">
                    Dados do Funcionário
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseIdent" style="height: auto;">
                <div class="accordion-inner">

              <div class="row-fluid">

                <div class="span12 ">

                  <div class="body-box">
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Nome') ?>: </dt>
                            <dd><?php echo $grupoAuxiliar['GrupoAuxiliar']['ds_nome']; ?></dd>
                            
                            <dt><?php __('Cargo') ?>: </dt>
                            <?php if(isset($grupoAuxiliar['Cargo'])) { ?>
                                <dd><?php echo $grupoAuxiliar['Cargo']['ds_cargo']; ?></dd>
                            <?php } else { ?>
                                <dd>N/A</dd>
                            <?php } ?>
                         </dl>
                      </div>
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Matrícula') ?>: </dt>
                            <dd><?php echo $grupoAuxiliar['GrupoAuxiliar']['nu_cpf']; ?>&nbsp;</dd>
                            
                            <dt><?php __('Função') ?>: </dt>
                            <?php if(isset($grupoAuxiliar['Funcao'])) { ?>
                                <dd><?php echo $grupoAuxiliar['Funcao']['ds_funcao']; ?></dd>
                            <?php } else { ?>
                                <dd>N/A</dd>
                            <?php } ?>
                         </dl>
                      </div>
                      <div class="span4">
                         <dl class="dl-horizontal">
                            <dt><?php __('Unidade Administrativa') ?>: </dt>
                            <dd><?php echo $grupoAuxiliar['Setor']['ds_setor']; ?></dd>
                         </dl>
                      </div>
                   </div>
                 </div>
               </div>

             </div>
           </div>
        </div>
    </div><br>

<?php
    
    $inativoall = true;
    $abaAtiva    = null;

    $abas = array();
    
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'grupo_auxiliares/add') ) {
            $abas[] = array( 'fsTransportes', __('Indenizações de Transporte', true), true, $this->Html->url(array('controller' => 'indenizacoes', 'action' => 'iframe', $id)) );
    }
    
    if(count($abas) > 0) {
?>
                
    <div id="accordion4" class="accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseAba" data-parent="#accordion4" data-toggle="collapse" class="accordion-toggle" id="complemento">
                  Complemento
                </a>
            </div>

            <div class="accordion-body in collapse" id="collapseAba" style="height: auto;">
                <div class="accordion-inner">

                  <div class="row-fluid">  


                    <div class="box-tabs-white">
                    <ul class="nav nav-tabs" id="myTab">
                    <?php echo $aba->render( $abas ); ?>
                     </ul>
                     <div class="tab-content" id="myTabContent">

                    </div>

                    </div>

                  </div>


                </div>
            </div>
        </div>
    </div>
<?php
    }
?>

                  
</div>


<?php echo $this->Html->scriptStart() ?>

	$(document).ready(function(){
		//atualizaTotalizaValores();
		$("#myTab > li > a").first(function(){
			$(this).click();
		});		
	});
	function carregarAba(element,aba){
		$("#myTab > li").each(function(){
			$(this).removeClass('active');
		});
		element.parent().addClass('active');
		$("#myTabContent").load(element.attr('req'));
	}
	
	$( function() {                
                $("#myTabContent").load( $("#li_fsTransportes > a").attr('req') );
	});
        
	
<?php echo $this->Html->scriptEnd() ?>

<script type="text/javascript">
    window.despair = function ()
    {
        alert('aeho');
    }