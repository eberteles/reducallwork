<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
  <?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Form->create('GrupoAuxiliar'); 
  ?>

    <div class="row-fluid">
        <div class="span8 ">
            <div class="widget-header widget-header-small"><h4><?php __('Dados Básicos'); ?>:</h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                           <?php
                                echo $this->Form->hidden('co_usuario');
                                if($this->Modulo->isCamposGrupoAuxiliar('nu_cpf')) {
                                    echo $this->Form->input('nu_cpf', array('class' => 'input-xlarge', 'id' => 'nuCpf', 'label' => 'CPF','mask' =>'999.999.999-99', 'onblur' => 'verificaCPF()'));
                                } else {
                                    echo $this->Form->input('nu_cpf', array('class' => 'input-xlarge', 'label' => 'Matrícula'));
                                }
                           ?>
                        <span id="isValidMessage"></span>
                           <?php
                                echo $this->Form->input('ds_nome', array('class' => 'input-xlarge', 'label' => 'Nome'));
                                echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail'));
                                echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
                                echo $this->Form->input('UsuarioPerfil.co_perfil', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label'=> 'Central de Alertas<br>Receber notificações encaminhadas para o perfil:', 'options' => $perfis));
                           ?>
                        </div>
                        <div class="span6">
                           <?php
                                if($this->Modulo->isCamposGrupoAuxiliar('co_setor')) {
                                    // unidade administrativa
                                    //echo $this->Form->input('co_setor', array('id' => 'coSetorUsuario', 'class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false) );
                                    echo $this->Form->input('co_setor', array('id' => 'coSetorUsuario', 'class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false,
                                        'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Unidade Administrativa', true), 'AbrirNovoSetor', '#view_novo_setor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'setores/index')) ) );
                                }
                           ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="view_novo_setor" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Unidade Administrativa'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_setor">
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    
    <?php echo $this->Form->end();  ?>

<script>
    $(document).ready(function () {
        $("#AbrirNovoSetor").bind('click', function(e) {
            var url_st = "<?php echo $this->base; ?>/setores/iframe/";
            $.ajax({
                type:"POST",
                url:url_st,
                data:{
                },
                success:function(result){
                    $('#add_setor').html("");
                    $('#add_setor').append(result);
                }
            })
        });
    });

    function atzComboSetor(co_setor) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'setores', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data.setores, function(key, value) {
                options += '<option value="' + value.co_setor + '">' + value.ds_setor + '</option>';
            });
            $("select#coSetorUsuario").html(options);
            $("#coSetorUsuario option[value=" + co_setor + "]").attr("selected", true);
            $("#coSetorUsuario").trigger("chosen:updated");
        });
    }
</script>

