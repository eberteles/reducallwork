<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<?php
    $usuario = $this->Session->read ('usuario');

?>
<?php echo $this->Form->create('GrupoAuxiliar', array('id'=>'fiscalForm' , 'url' => array('controller' => 'grupo_auxiliares', 'action' => 'fiscal', $cadAuxiliar))); ?>

<div class="row-fluid">
    <div class="span8 ">
        <div class="widget-header widget-header-small"><h4><?php __('Dados Básicos'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span6">
                        <?php
                    // echo $this->Form->hidden('co_usuario');
                        if($this->Modulo->isCamposGrupoAuxiliar('nu_cpf')) {
                        echo $this->Form->input('nu_cpf', array('class' => 'input-xlarge', 'id' => 'nuCpf', 'label' => 'CPF','mask' =>'999.999.999-99', 'onfocusout' => 'verificaCPF()'));
                        } else {
                        echo $this->Form->input('nu_cpf', array('class' => 'input-xlarge', 'label' => 'Matrícula'));
                        }
                        ?>
                        <span id="isValidMessage"></span>
                        <?php
                    echo $this->Form->input('ds_nome', array('class' => 'input-xlarge', 'label' => 'Nome'));
                        echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail'));
                        echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
                        echo $this->Form->input('UsuarioPerfil.co_perfil', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Perfil', true), 'options' => $perfis));
                        ?>
                    </div>
                    <div class="span6">
                        <?php
                    if($this->Modulo->isCamposGrupoAuxiliar('co_setor')) {
                        echo $this->Form->input('co_setor', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false,
	                        'after' => $this->Print->getBtnEditCombo('Nova ' .  __('Unidade Administrativa', true), 'AbrirNovoSetor', '#view_novo_setor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'setores/add')) ) );
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="view_novo_setor" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Nova <?php __('Unidade Administrativa'); ?></h3>
    </div>
    <div class="modal-body-iframe" id="add_setor">
    </div>
</div>

<div id="view_novo_cargo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Novo Cargo</h3>
    </div>
    <div class="modal-body-iframe" id="add_cargo">
    </div>
</div>

<div id="view_nova_funcao" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Nova Função</h3>
    </div>
    <div class="modal-body-iframe" id="add_funcao">
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar" onclick="ResetFormValues()"> Limpar</button>
        <button rel="tooltip" type="submit" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>

<script type="text/javascript">
    $("#isValidMessage").hide();

    function verificaCPF(){
        var cpf = $("#nuCpf").val();

        if (cpf != '___________' || cpf != '___.___.___-__' || cpf != '' || cpf != ' ') {
            cpf = cpf.replace('.','').replace('.','').replace('-','');
            $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function(data) {
                $('#isValidMessage').css('color', data.color);
                $('#isValidMessage').css('fontWeight', data.bold);
                $("#isValidMessage").html(data.message).show();
            }).done(function(){
                console.log("Second success");
            })
            //$("#isValidMessage").html("Teste").show();
        }
    }

    $("#AbrirNovoSetor").bind('click', function(e) {
        var url_st = "<?php echo $this->base; ?>/setores/iframe/";
        $.ajax({
            type:"POST",
            url:url_st,
            data:{
            },
            success:function(result){
                $('#add_setor').html("");
                $('#add_setor').append(result);
            }
        })
    });

    function atzComboSetor(co_setor) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'setores', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data.setores, function(key, value) {
                options += '<option value="' + value.co_setor + '">' + value.ds_setor + '</option>';
            });
            $("select#GrupoAuxiliarCoSetor").html(options);
            $("#GrupoAuxiliarCoSetor option[value=" + co_setor + "]").attr("selected", true);
            $("#GrupoAuxiliarCoSetor").trigger("chosen:updated");
        });
    }

    $("#AbrirNovoCargo").bind('click', function(e) {
        var url_cg = "<?php echo $this->base; ?>/cargos/iframe/";
        $.ajax({
            type:"POST",
            url:url_cg,
            data:{
            },
            success:function(result){
                $('#add_cargo').html("");
                $('#add_cargo').append(result);
            }
        })
    });

    function atzComboCargo(co_cargo) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'cargos', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#GrupoAuxiliarCoCargo").html(options);
            $("#GrupoAuxiliarCoCargo option[value=" + co_cargo + "]").attr("selected", true);
            $("#GrupoAuxiliarCoCargo").trigger("chosen:updated");
        });
    }

    $("#AbrirNovaFuncao").bind('click', function(e) {
        var url_fc = "<?php echo $this->base; ?>/funcoes/iframe/";
        $.ajax({
            type:"POST",
            url:url_fc,
            data:{
            },
            success:function(result){
                $('#add_funcao').html("");
                $('#add_funcao').append(result);
            }
        })
    });

    function atzComboFuncao(co_funcao) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'funcoes', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#GrupoAuxiliarCoFuncao").html(options);
            $("#GrupoAuxiliarCoFuncao option[value=" + co_funcao + "]").attr("selected", true);
            $("#GrupoAuxiliarCoFuncao").trigger("chosen:updated");
        });
    }

</script>
