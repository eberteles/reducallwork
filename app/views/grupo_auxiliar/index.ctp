<?php
    $usuario = $this->Session->read ('usuario');
 ?>

    <div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Grupo Auxiliar'); ?></h1></div>
            <?php if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'grupo_auxiliares/add') ): ?>
                <div class="acoes-formulario-top clearfix" >
                    <p class="requiredLegend pull-left"> Preencha um dos campos para fazer a pesquisa</p>
                    <div class="pull-right btn-group">
                        <a href="<?php echo $this->Html->url(array('controller' => 'grupo_auxiliares', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Usuário"><i class="icon-plus icon-white"></i> <?php echo __("Novo Parceiro"); ?></a>
                    </div>
                </div>
            <?php endif; ?>
	<?php echo $this->Form->create('GrupoAuxiliar', array('type' => 'GET'));?>

	<div class="row-fluid">
    	<div class="span3">
    		<div class="controls">
    		    <?php echo $this->Form->input('ds_nome_auxiliar', array('class' => 'input-xlarge','label' => 'Nome')); ?>
    		</div>
    	</div>
        <?php
        if(!$this->Modulo->isCamposGrupoAuxiliar('nu_cpf')):
        ?>
    	<div class="span3">
    		<div class="controls">
    		<?php echo $this->Form->input('ds_matricula', array('class' => 'input-xlarge','label' => 'Matrícula')); ?>
    		</div>
    	</div>
        <?php endif; if($this->Modulo->isCamposGrupoAuxiliar('co_setor')): ?>
        <div class="span3">
                <div class="controls">
                <?php echo $this->Form->input('co_setor_pes', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false)); ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="span3">
            <div class="controls">
                <?php
                    echo $this->Form->input('ic_ativo', array(
                        'type' => 'select',
                        'class' => 'input-xlarge',
                        'empty' => 'Selecione...',
                        'label' => 'Situação',
                        'options' => array(
                            0 => 'Inativo',
                            1 => 'Ativo'
                        )
                    ));
                ?>
            </div>
        </div>
    </div>
    <div class="form-actions">
		<div class="btn-group">
			<button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
			<button rel="tooltip" type="button" title="Limpar dados preechidos" class="btn btn-small" id="Limpar"> Limpar</button>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
<table cellpadding="0" cellspacing="0"  style="background-color:white" class="table table-hover table-bordered table-striped">
	<thead>
		<tr>
			<th><?php __('Nome');?></th>
            <th><?php if($this->Modulo->isCamposGrupoAuxiliar('nu_cpf')) { __('CPF'); }else{ __('Matrícula'); } ?></th>
            <th><?php __('Unidade Administrativa');?></th>
			<th><?php __('Email');?></th>
			<th><?php __('Telefone');?></th>
			<th class="actions"><?php __('Ações');?></th>
		</tr>
	<thead>
	<tbody>
	<?php
	$i = 0;
	foreach ($usuarios as $usuario):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>

	<tr <?php echo $class;?>>
		<td>
            <?php
                if( $this->Modulo->isIndenizacaoTransporte() ) {
                    echo $this->Html->link($usuario['GrupoAuxiliar']['ds_nome'] , 'detalha/'.$usuario['GrupoAuxiliar']['co_usuario']);
                } else {
                    echo $usuario['GrupoAuxiliar']['ds_nome'];
                }
            ?>
            &nbsp;
        </td>
        <td>
            <?php
                if($this->Modulo->isCamposGrupoAuxiliar('nu_cpf')) {
                    echo $this->Print->cpf($usuario['GrupoAuxiliar']['nu_cpf']);
                } else {
                    echo $usuario['GrupoAuxiliar']['nu_cpf'];
                }
            ?>
            &nbsp;
        </td>
        <td><?php echo $usuario['Setor']['ds_setor']; ?>&nbsp;</td>
		<td><?php echo $usuario['GrupoAuxiliar']['ds_email']; ?>&nbsp;</td>
		<td><?php echo $usuario['GrupoAuxiliar']['nu_telefone']; ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group acoes">

            <?php
            if( $usuario['GrupoAuxiliar']['ic_ativo'] == 1 ) {
                echo $this->element('actions6', array('id' => $usuario['GrupoAuxiliar']['co_usuario'], 'class' => 'btn', 'local_acao' => 'grupo_auxiliares/index'));
            } elseif ( $usuario['GrupoAuxiliar']['ic_ativo'] == 0 ){
                echo $this->element('actions7', array('id' => $usuario['GrupoAuxiliar']['co_usuario'], 'class' => 'btn', 'local_acao' => 'grupo_auxiliares/index'));
            }
            ?>

			</div>
		</td>
	</tr>

	<?php endforeach; ?>
	</tbody>
</table>
<p>
    <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
    ?>
</p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true),
            array('tag'=>'li', 'separator'=>'',
                'url' => array(
                    '?' => array(
                        'ds_nome_auxiliar' => $this->params['url']['ds_nome_auxiliar'],
                        'co_setor_pes' => $this->params['url']['co_setor_pes'],
                        'ic_ativo' => $this->params['url']['ic_ativo']
                        ))),
            null,
            array('class'=>'disabled'));
            ?>

            <?php echo $this->Paginator->numbers(
                array('tag'=>'li', 'separator'=>'',
                    'url' => array(
                        '?' => array(
                            'ds_nome_auxiliar' => $this->params['url']['ds_nome_auxiliar'],
                            'co_setor_pes' => $this->params['url']['co_setor_pes'],
                            'ic_ativo' => $this->params['url']['ic_ativo']
                            ))
                    ));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>',
            array('tag'=>'li', 'separator'=>'',
                'url' => array(
                    '?' => array(
                        'ds_nome_auxiliar' => $this->params['url']['ds_nome_auxiliar'],
                        'co_setor_pes' => $this->params['url']['co_setor_pes'],
                        'ic_ativo' => $this->params['url']['ic_ativo']
                        ))),
            null,
            array('class' => 'disabled'));
            ?>
    </ul>
</div>
</div>
