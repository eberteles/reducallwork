<?php
/**
 * Helper for print tab navegation.
 *
 * PHP versions 4 and 5
 *
 * @filesource
 * @copyright        Copyright 2010
 * @package            app
 * @subpackage        app.view.helpers
 * @version            $Revision: 0.1 $
 * @lastmodified    $Date: 2010-11-29 23:19:05
 */

/**
 * AbaHelper library.
 *
 * @package        app
 * @subpackage    app.view.helpers
 */
class AbaHelper extends Helper
{
    var $helpers = array('Javascript');

    function render($aValues, $inativoall = null, $abaAtiva = null, $carregarAba = 'carregarAba')
    {
        $oAba = "oAba" . rand();
        $xhtml = "";
        // echo '<pre>';print_r($aValues);exit;
        foreach ($aValues as $key => $val) {
            if ($val [2]) {
                if ($inativoall) {
                    $class = "inativo";
                } else if ($abaAtiva > 0) {
                    ($key == $abaAtiva) ? ($class = "ativo active") : ($class = "inativo");
                } else {
                    ($key == 0) ? ($class = "ativo active") : ($class = "inativo");
                }
                $xhtml .= "<li class='{$class}' id='li_{$val[0]}'>";
                $xhtml .= "<a data-toggle=\"tab\" onclick=\"$carregarAba($(this), '{$val[0]}' ); \" href='#{$val[0]}' req='" . $val[3] . "'>{$this->getIcone($val[0])}<b>{$val[1]}</b></a></li>\n";
                // $xhtml .= "<a data-toggle=\"tab\" onclick=\"$carregarAba($(this), '{$val[0]}' ); \" href='#{$val[0]}'>{$this->getIcone($val[0])}<b>{$val[1]}</b></a></li>\n";
            } else {
                $xhtml .= "<li class='disabled' id='li_{$val[0]}'>";
                $xhtml .= "<a>{$val[1]}</a></li>\n";
            }

            $elements [] = $val [0];
        }

        $xhtml .= "";

        $aElements = "Array( '" . implode("','", $elements) . "' )";

        $script = $this->Javascript->codeBlock("
		{$oAba} = new Aba( {$aElements} );
		");

        $return = $xhtml . $script;

        return $return;
    }

    function getIcone($icone)
    {
        $html_icone = '';
        if ($icone == "fsFiscais") {
            $html_icone = '<i class="blue icon-group bigger-110"></i> &nbsp;';
        }
        if ($icone == "fsAditivos") {
            $html_icone = '<i class="grey icon-briefcase bigger-110"></i> ';
        }
        if ($icone == "fsGarantias") {
            $html_icone = '<i class="green icon-hdd bigger-110"></i> ';
        }
        if ($icone == "fsGarantiasSuporte") {
            $html_icone = '<i class="blue icon-beaker bigger-110"></i> ';
        }
        if ($icone == "fsProdutos") {
            $html_icone = '<i class="purple icon-laptop bigger-110"></i> ';
        }
        if ($icone == "fsContas") {
            $html_icone = '<i class="purple icon-phone-sign bigger-110"></i> ';
        }
        if ($icone == "fsContatos") {
            $html_icone = '<i class="purple icon-group bigger-110"></i> ';
        }
        if ($icone == "fsOficios") {
            $html_icone = '<i class="green icon-list bigger-110"></i> ';
        }
        if ($icone == "fsPreEmpenhos") {
            $html_icone = '<i class="blue icon-legal bigger-110"></i> ';
        }
        if ($icone == "fsEmpenhos") {
            $html_icone = '<i class="pink icon-legal bigger-110"></i> ';
        }
        if ($icone == "fsLiquidacoes") {
            $html_icone = '<i class="green icon-hdd bigger-110"></i> ';
        }
        if ($icone == "fsOb") {
            $html_icone = '<i class="yellow icon-credit-card bigger-110"></i> ';
        }
        if ($icone == "fsPenalidades") {
            $html_icone = '<i class="orange icon-exclamation-sign bigger-110"></i> ';
        }
        if ($icone == "fsNotas") {
            $html_icone = '<i class="grey icon-file-text bigger-110"></i> ';
        }
        if ($icone == "fsPagamentos") {
            $html_icone = '<i class="green icon-credit-card bigger-110"></i> ';
        }
        if ($icone == "fsEntregas") {
            $html_icone = '<i class="green icon-credit-card bigger-110"></i> ';
        }
        if ($icone == "fsHistoricos") {
            $html_icone = '<i class="blue icon-film bigger-110"></i> ';
        }
        if ($icone == "fsClausulas") {
            $html_icone = '<i class="pink2 icon-list bigger-110"></i> ';
        }
        if ($icone == "fsPendencias") {
            $html_icone = '<i class="red icon-info-sign bigger-110"></i> ';
        }
        if ($icone == "fsAndamentos") {
            $html_icone = '<i class="grey icon-comments-alt bigger-110"></i> ';
        }
        if ($icone == "fsAnexos") {
            $html_icone = '<i class="pink icon-folder-open bigger-110"></i> ';
        }
        if ($icone == "fsApostilamentos") {
            $html_icone = '<i class="purple icon-file bigger-110"></i>';
        }
        if ($icone == "fsItens") {
            $html_icone = '<i class="grey icon-list bigger-110"></i> &nbsp;';
        }
        if ($icone == "fsReservas") {
            $html_icone = '<i class="red icon-bookmark bigger-110"></i>';
        }
        if ($icone == "fsPedidos") {
            $html_icone = '<i class="green icon-flag bigger-110"></i>';
        }
        if ($icone == "fsResultados") {
            $html_icone = '<i class="green icon-flag bigger-110"></i> &nbsp;';
        }
        if ($icone == "fsTransportes") {
            $html_icone = '<i class="green icon-credit-card bigger-110"></i> &nbsp;';
        }
        if($icone == "fsSeiProcessos") {
            $html_icone = '<i class="green icon-file-text bigger-110"></i> &nbsp;';
        }
        if($icone == "fsSeiDocumentos") {
            $html_icone = '<i class="green icon-cogs bigger-110"></i> &nbsp;';
        }
        if($icone == "fsSeiAndamentos") {
            $html_icone = '<i class="green icon-road bigger-110"></i> &nbsp;';
        }
        return $html_icone;
    }
}

