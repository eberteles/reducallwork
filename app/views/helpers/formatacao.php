<?php

/**
 * Helper para formata��o de dados no padr�o brasileiro
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @author        Juan Basso <jrbasso@gmail.com>
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Formata��o Helper
 *
 * @link http://wiki.github.com/jrbasso/cake_ptbr/helper-formatao
 */
class FormatacaoHelper extends AppHelper
{

    /**
     * Helpers auxiliares
     *
     * @var array
     * @access public
     */
    var $helpers = array(
        'Time',
        'Number'
    );

    /**
     * Formata a data
     *
     * @param integer $data
     *            Data em timestamp ou null para atual
     * @param array $opcoes
     *            � poss�vel definir o valor de 'invalid' e 'userOffset' que ser�o usados pelo helper Time
     * @return string Data no formato dd/mm/aaaa
     * @access public
     */
    function data($data = null, $opcoes = array())
    {
        $padrao = array(
            'invalid' => '31/12/1969',
            'userOffset' => null
        );
        $config = array_merge($padrao, $opcoes);
        
        $data = $this->_ajustaDataHora($data);
        return $this->Time->format('d/m/Y', $data, $config['invalid'], $config['userOffset']);
    }

    /**
     * Formata a data e hora
     *
     * @param integer $dataHora
     *            Data e hora em timestamp ou null para atual
     * @param boolean $segundos
     *            Mostrar os segundos
     * @param array $opcoes
     *            � poss�vel definir o valor de 'invalid' e 'userOffset' que ser�o usados pelo helper Time
     * @return string Data no formato dd/mm/aaaa hh:mm:ss
     * @access public
     */
    function dataHora($dataHora = null, $segundos = true, $opcoes = array())
    {
        $padrao = array(
            'invalid' => '31/12/1969',
            'userOffset' => null
        );
        $config = array_merge($padrao, $opcoes);
        
        $dataHora = $this->_ajustaDataHora($dataHora);
        if ($segundos) {
            return $this->Time->format('d/m/Y H:i:s', $dataHora, $config['invalid'], $config['userOffset']);
        }
        return $this->Time->format('d/m/Y H:i', $dataHora, $config['invalid'], $config['userOffset']);
    }

    /**
     * Mostrar a data completa
     *
     * @param integer $dataHora
     *            Data e hora em timestamp ou null para atual
     * @return string Descri��o da data no estilo "Sexta-feira", 01 de Janeiro de 2010, 00:00:00"
     * @access public
     */
    function dataCompleta($dataHora = null)
    {
        $_diasDaSemana = array(
            'Domingo',
            'Segunda-feira',
            'Ter�a-feira',
            'Quarta-feira',
            'Quinta-feira',
            'Sexta-feira',
            'S�bado'
        );
        $_meses = array(
            'Janeiro',
            'Fevereiro',
            'Mar�o',
            'Abril',
            'Maio',
            'Junho',
            'Julho',
            'Agosto',
            'Setembro',
            'Outubro',
            'Novembro',
            'Dezembro'
        );
        
        $dataHora = $this->_ajustaDataHora($dataHora);
        $w = date('w', $dataHora);
        $n = date('n', $dataHora) - 1;
        
        return sprintf('%s, %02d de %s de %04d, %s', $_diasDaSemana[$w], date('d', $dataHora), $_meses[$n], date('Y', $dataHora), date('H:i:s', $dataHora));
    }

    /**
     * Se a data for nula, usa data atual
     *
     * @param mixed $data            
     * @return integer Se null, retorna a data/hora atual
     * @access protected
     */
    function _ajustaDataHora($data)
    {
        if (is_null($data)) {
            return time();
        }
        if (is_integer($data) || ctype_digit($data)) {
            return (int) $data;
        }
        return strtotime((string) $data);
    }

    /**
     * Mostrar uma data em tempo
     *
     * @param integer $dataHora
     *            Data e hora em timestamp, dd/mm/YYYY ou null para atual
     * @param string $limite
     *            null, caso n�o haja expira��o ou ent�o, forne�a um tempo usando o formato ingl�s para strtotime: Ex: 1 year
     * @return string Descri��o da data em tempo ex.: a 1 minuto, a 1 semana
     * @access public
     */
    function tempo($dataHora = null, $limite = '30 days')
    {
        if (! $dataHora) {
            $dataHora = time();
        }
        
        if (strpos($dataHora, '/') !== false) {
            $_dataHora = str_replace('/', '-', $dataHora);
            $_dataHora = date('ymdHi', strtotime($_dataHora));
        } elseif (is_string($dataHora)) {
            $_dataHora = date('ymdHi', strtotime($dataHora));
        } else {
            $_dataHora = date('ymdHi', $dataHora);
        }
        
        if ($limite !== null) {
            if ($_dataHora > date('ymdHi', strtotime('+ ' . $limite))) {
                return $this->dataHora($dataHora);
            }
        }
        
        $_dataHora = date('ymdHi') - $_dataHora;
        if ($_dataHora > 88697640 && $_dataHora < 100000000) {
            $_dataHora -= 88697640;
        }
        
        switch ($_dataHora) {
            case 0:
                return 'menos de 1 minuto';
            case ($_dataHora < 99):
                if ($_dataHora === 1) {
                    return '1 minuto';
                } elseif ($_dataHora > 59) {
                    return ($_dataHora - 40) . ' minutos';
                }
                return $_dataHora . ' minutos';
            case ($_dataHora > 99 && $_dataHora < 2359):
                $flr = floor($_dataHora * 0.01);
                return $flr == 1 ? '1 hora' : $flr . ' horas';
            
            case ($_dataHora > 2359 && $_dataHora < 310000):
                $flr = floor($_dataHora * 0.0001);
                return $flr == 1 ? '1 dia' : $flr . ' dias';
            
            case ($_dataHora > 310001 && $_dataHora < 12320000):
                $flr = floor($_dataHora * 0.000001);
                return $flr == 1 ? '1 mes' : $flr . ' meses';
            
            case ($_dataHora > 100000000):
            default:
                $flr = floor($_dataHora * 0.00000001);
                return $flr == 1 ? '1 ano' : $flr . ' anos';
        }
    }

    /**
     * N�mero float com ponto ao inv�s de v�rgula
     *
     * @param float $numero
     *            N�mero
     * @param integer $casasDecimais
     *            N�mero de casas decimais
     * @return string N�mero formatado
     * @access public
     */
    function precisao($numero, $casasDecimais = 3)
    {
        return number_format($numero, $casasDecimais, ',', '.');
    }

    /**
     * Valor formatado com s�mbolo de %
     *
     * @param float $numero
     *            N�mero
     * @param integer $casasDecimais
     *            N�mero de casas decimais
     * @return string N�mero formatado com %
     * @access public
     */
    function porcentagem($numero, $casasDecimais = 2)
    {
        $percentual = round($this->precisao($numero, $casasDecimais));
        return $percentual . '%';
    }

    public function pegaMes($month)
    {
        switch ($month) {
            case "1":
                $month = "Janeiro";
                break;
            case "2":
                $month = "Fevereiro";
                break;
            case "3":
                $month = "Março";
                break;
            case "4":
                $month = "Abril";
                break;
            case "5":
                $month = "Maio";
                break;
            case "6":
                $month = "Junho";
                break;
            case "7":
                $month = "Julho";
                break;
            case "8":
                $month = "Agosto";
                break;
            case "9":
                $month = "Setembro";
                break;
            case "10":
                $month = "Outubro";
                break;
            case "11":
                $month = "Novembro";
                break;
            case "12":
                $month = "Dezembro";
                break;
            default:
                $month = "---";
                break;
        }
        return $month;
    }

    /**
     * Formata um valor para reais
     *
     * @param float $valor
     *            Valor
     * @param array $opcoes
     *            Mesmas op��es de Number::currency()
     * @return string Valor formatado em reais
     * @access public
     */
    function moeda($valor, $opcoes = array())
    {
        $padrao = array(
            'before' => 'R$ ',
            'after' => '',
            'zero' => 'R$ 0,00',
            'places' => 2,
            'thousands' => '.',
            'decimals' => ',',
            'negative' => '-',
            'escape' => true
        );
        $config = array_merge($padrao, $opcoes);
        if ($valor > - 1 && $valor < 1) {
            $before = $config['before'];
            $config['before'] = '';
            $formatado = $this->Number->format(abs($valor), $config);
            if ($valor < 0) {
                if ($config['negative'] == '()') {
                    return '(' . $before . $formatado . ')';
                } else if (trim($config['negative']) == '-') {
                    return trim($config['negative']) . $before . $formatado;
                } else {
                    return $before . $config['negative'] . $formatado;
                }
            }
            return $before . $formatado;
        }
        return $this->Number->currency($valor, null, $config);
    }

    /**
     * Valor por extenso em reais
     *
     * @param float $numero            
     * @return string Valor em reais por extenso
     * @access public
     * @link http://forum.imasters.uol.com.br/index.php?showtopic=125375
     */
    function moedaPorExtenso($numero)
    {
        $singular = array(
            'centavo',
            'real',
            'mil',
            'milh�o',
            'bilh�o',
            'trilh�o',
            'quatrilh�o'
        );
        $plural = array(
            'centavos',
            'reais',
            'mil',
            'milh�es',
            'bilh�es',
            'trilh�es',
            'quatrilh�es'
        );
        
        $c = array(
            '',
            'cem',
            'duzentos',
            'trezentos',
            'quatrocentos',
            'quinhentos',
            'seiscentos',
            'setecentos',
            'oitocentos',
            'novecentos'
        );
        $d = array(
            '',
            'dez',
            'vinte',
            'trinta',
            'quarenta',
            'cinquenta',
            'sessenta',
            'setenta',
            'oitenta',
            'noventa'
        );
        $d10 = array(
            'dez',
            'onze',
            'doze',
            'treze',
            'quatorze',
            'quinze',
            'dezesseis',
            'dezesete',
            'dezoito',
            'dezenove'
        );
        $u = array(
            '',
            'um',
            'dois',
            'tr�s',
            'quatro',
            'cinco',
            'seis',
            'sete',
            'oito',
            'nove'
        );
        
        $z = 0;
        $rt = '';
        
        $valor = number_format($numero, 2, '.', '.');
        $inteiro = explode('.', $valor);
        $tamInteiro = count($inteiro);
        
        // Normalizandos os valores para ficarem com 3 digitos
        $inteiro[0] = sprintf('%03d', $inteiro[0]);
        $inteiro[$tamInteiro - 1] = sprintf('%03d', $inteiro[$tamInteiro - 1]);
        
        $fim = $tamInteiro - 1;
        if ($inteiro[$tamInteiro - 1] <= 0) {
            $fim --;
        }
        foreach ($inteiro as $i => $valor) {
            $rc = $c[$valor{0}];
            if ($valor > 100 && $valor < 200) {
                $rc = 'cento';
            }
            $rd = '';
            if ($valor{1} > 1) {
                $rd = $d[$valor{1}];
            }
            $ru = '';
            if ($valor > 0) {
                if ($valor{1} == 1) {
                    $ru = $d10[$valor{2}];
                } else {
                    $ru = $u[$valor{2}];
                }
            }
            
            $r = $rc;
            if ($rc && ($rd || $ru)) {
                $r .= ' e ';
            }
            $r .= $rd;
            if ($rd && $ru) {
                $r .= ' e ';
            }
            $r .= $ru;
            $t = $tamInteiro - 1 - $i;
            if (! empty($r)) {
                $r .= ' ';
                if ($valor > 1) {
                    $r .= $plural[$t];
                } else {
                    $r .= $singular[$t];
                }
            }
            if ($valor == '000') {
                $z ++;
            } elseif ($z > 0) {
                $z --;
            }
            if ($t == 1 && $z > 0 && $inteiro[0] > 0) {
                if ($z > 1) {
                    $r .= ' de ';
                }
                $r .= $plural[$t];
            }
            if (! empty($r)) {
                if ($i > 0 && $i < $fim && $inteiro[0] > 0 && $z < 1) {
                    if ($i < $fim) {
                        $rt .= ', ';
                    } else {
                        $rt .= ' e ';
                    }
                } elseif ($t == 0 && $inteiro[0] > 0) {
                    $rt .= ' e ';
                } else {
                    $rt .= ' ';
                }
                $rt .= $r;
            }
        }
        
        if (empty($rt)) {
            return 'zero';
        }
        return trim(str_replace('  ', ' ', $rt));
    }

    //FORMATA COMO TIMESTAMP
    /*Esta função é bem simples, e foi criada somente para nos ajudar a 
    formatar a data já em formato  TimeStamp facilitando nossa soma de dias 
    para uma data qualquer.*/
    function dataToTimestamp($data){
        $dia = substr($data, 6,4);
        $mes = substr($data, 3,2);
        $ano = substr($data, 0,2);
        return mktime(0, 0, 0, $mes, $dia, $ano);  
    } 
}

?>