<?php

class ImprimirHelper extends AppHelper {
    
	var $helpers = array(
		'Html', 'Print'
		);
        var $arraySetores       = array();
        var $arrayPastas        = array();
        var $arrayConfederacoes = array();
        
        function listaTabelaSetor($setor, $id_pai = '') {
            
            $class_tree     = "treegrid-" . $setor['Setor']['co_setor'];
            $class_tree_pai = '';
            if($id_pai != '') {
                $class_tree_pai = "treegrid-parent-" . $id_pai;
            }
            
            ?>
            <tr class="<?php echo $class_tree . ' ' . $class_tree_pai;?>">
                <td>&nbsp;&nbsp;<?php echo $setor['Setor']['ds_setor']; ?>&nbsp;</td>
                <td><?php echo $setor['Setor']['ds_email']; ?>&nbsp;</td>
                <td><?php echo $setor['Setor']['nu_telefone']; ?>&nbsp;</td>
                <td><?php echo $setor['Usuario']['ds_nome']; ?>&nbsp;</td>
                <td class="actions"><?php $id = $setor['Setor']['co_setor']; ?>
                    <div class="btn-group acoes">
                        <?php $nome_setor = addslashes($setor['Setor']['ds_setor']); ?>
                        <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $id), array('escape' => false,'class' => 'btn ')); ?>
                        <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'logicDelete', $id), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja excluir \'%s\'?', true), $nome_setor)); ?>
                    </div>
                </td>
            </tr>
            <?php
            
			if (array_key_exists('children', $setor)) {
				foreach ($setor['children'] as $filho):
					$this->listaTabelaSetor($filho, $setor['Setor']['co_setor']);                
				endforeach;
            }
        }
        
        function selectTabelaSetor($setor, $id_selected, $nivel = 0) {
            
            $selected   = '';
            if($setor['Setor']['co_setor'] == $id_selected) {
                $selected   = 'selected';
            }
            $espacos    = $this->Print->getEspacos($nivel);
            echo '<option value="' . $setor['Setor']['co_setor'] . '" ' . $selected . '>' . $espacos . $setor['Setor']['ds_setor'] . '</option>';
                        
            foreach ($setor['children'] as $filho):
                $novo_nivel = $nivel + 1;
                $this->selectTabelaSetor($filho, $id_selected, $novo_nivel);
            endforeach;
            
        }
        
        /**
         * @param array $item
         * @param array $configs
         */
        function setArrayNested($item, $configs) 
        {        	
        	extract($configs);
        	
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            
            $espacos = $print->getEspacos($nivel);
            
            $this->arrayMultinivel[ $item[$table][$primary] ] = $espacos . $item[$table][$label];
                        
            foreach ($item['children'] as $filho):
                $novo_nivel = $nivel + 1;
            	$configs['nivel'] = $novo_nivel;
                $this->setArrayNested($filho, $configs);
            endforeach;
            
        }
        
        /**
         * @param array $itens
         * @param array $configs
         */
        function getArrayNested($itens, $configs = array())
        {
        	extract($configs);
        	
            $this->arrayMultinivel = array();
            foreach ($itens as $item):
                $this->setArrayNested($item, $configs);
            endforeach;
            
            return $this->arrayMultinivel;
        }
        
        function setArraySetores($setor, $nivel = 0, $tipo = 0) {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            
            $espacos    = $print->getEspacos($nivel);
            if($tipo == 1) {
                $this->arraySetores['setores'][]   = array( 'co_setor'=> $setor['Setor']['co_setor'], 'ds_setor'=> $espacos . $setor['Setor']['ds_setor']);
            } else {
                $this->arraySetores[ $setor['Setor']['co_setor'] ] = $espacos . $setor['Setor']['ds_setor'];
            }
                        
            foreach ($setor['children'] as $filho):
                $novo_nivel = $nivel + 1;
                $this->setArraySetores($filho, $novo_nivel, $tipo);
            endforeach;
            
        }
                
        function getArraySetores($setores, $tipo = 0){
            
            $this->arraySetores = array();
            foreach ($setores as $setor):
                $this->setArraySetores($setor, 0, $tipo);
            endforeach;
            
            return $this->arraySetores;
        }

        function getArrayAnexosPastas($pastas, $tipo = 0){
            
            $this->arrayPastas  = array();
            foreach ($pastas as $pasta):
                $this->setArrayPastas($pasta, 0, $tipo);
            endforeach;
            
            return $this->arrayPastas;
        }
        
        function listaTabelaAnexoPasta($anexo_pasta, $id_pai = '', $idModulo = 0, $modulo = "padrao") {

            $class_tree     = "treegrid-" . $anexo_pasta['AnexoPasta']['co_anexo_pasta'];
            $class_tree_pai = '';
            if($id_pai != '') {
                $class_tree_pai = "treegrid-parent-" . $id_pai;
            }

            ?>
            <tr class="<?php echo $class_tree . ' ' . $class_tree_pai;?>">
                <td>&nbsp;&nbsp;<?php echo stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']); ?>&nbsp;</td>
                <td class="actions"><?php $id = $anexo_pasta['AnexoPasta']['co_anexo_pasta']; ?>
                    <div class="btn-group acoes">
                         <?php $nome_pasta = stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']); ?>
                        <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $id, $idModulo, $modulo), array('escape' => false,'class' => 'btn ')); ?>
                        <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $id, $idModulo, $modulo), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja excluir a pasta \'%s\' e seu conteúdo?', true), $nome_pasta)); ?>
                    </div>
                </td>
            </tr>
            <?php
            
            foreach ($anexo_pasta['children'] as $filho):
                $this->listaTabelaPasta($filho, $anexo_pasta['AnexoPasta']['co_anexo_pasta'], $idModulo);
            endforeach;

        }
        
        function listaTabelaPasta($anexo_pasta, $id_pai = '', $idModulo = 0, $modulo = "padrao") {
            
            $class_tree     = "treegrid-" . $anexo_pasta['AnexoPasta']['co_anexo_pasta'];
            $class_tree_pai = '';
            if($id_pai != '') {
                $class_tree_pai = "treegrid-parent-" . $id_pai;
            }
            
            ?>
            <tr class="<?php echo $class_tree . ' ' . $class_tree_pai;?>">
                <td>&nbsp;&nbsp;<?php echo stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']) ?>&nbsp;</td>
                <td class="actions"><?php $id = $anexo_pasta['AnexoPasta']['co_anexo_pasta']; ?>
                    <div class="btn-group acoes">
                        <?php $nome_pasta = stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']) ?>
                        <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $id, $idModulo, $modulo), array('escape' => false,'class' => 'btn ')); ?>
                        <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $id, $idModulo, $modulo), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja excluir a pasta \'%s\' e seu conteúdo?', true), $nome_pasta)); ?>
                    </div>
                </td>
            </tr>
            <?php
            
            foreach ($anexo_pasta['children'] as $filho):
                $this->listaTabelaAnexoPasta($filho, $anexo_pasta['AnexoPasta']['co_anexo_pasta'], $idModulo);
            endforeach;
            
        }
        
        function setArrayPastas($anexo_pasta, $nivel = 0, $tipo = 0) {
            App::import('Helper', 'Print');
            $print = new PrintHelper();
            
            $espacos    = $print->getEspacos($nivel);
            if($tipo == 1) {
                $this->arrayPastas['anexos_pastas'][]   = array( 'co_anexo_pasta'=> $anexo_pasta['AnexoPasta']['co_anexo_pasta'], 'ds_anexo_pasta'=> $espacos . stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']));
            } else {
                $this->arrayPastas[ $anexo_pasta['AnexoPasta']['co_anexo_pasta'] ] = $espacos . stripslashes($anexo_pasta['AnexoPasta']['ds_anexo_pasta']);
            }
                        
            foreach ($anexo_pasta['children'] as $filho):
                $novo_nivel = $nivel + 1;
                $this->setArrayPastas($filho, $novo_nivel, $tipo);
            endforeach;
            
        }

        function listaTabelaConfederacao($confederacao, $id_pai = '', $modulo = null) {
            
            $class_tree     = "treegrid-" . $confederacao['Confederacao']['co_confederacao'];
            $class_tree_pai = '';
            if($id_pai != '') {
                $class_tree_pai = "treegrid-parent-" . $id_pai;
            }
            
            ?>
            <tr class="<?php echo $class_tree . ' ' . $class_tree_pai;?>">
                    <td>&nbsp;&nbsp;<?php echo $confederacao['Confederacao']['ds_confederacao']; ?>&nbsp;</td>
                    <td><?php echo $this->Print->tpIndicador($confederacao['Confederacao']['ic_cob']); ?>&nbsp;</td>
                    <td><?php echo $this->Print->tpIndicador($confederacao['Confederacao']['ic_cpb']); ?>&nbsp;</td>
                    <td><?php echo $confederacao['Confederacao']['ds_email']; ?>&nbsp;</td>
                    <td><?php echo $this->Print->telefone($confederacao['Confederacao']['nu_telefone']); ?>&nbsp;</td>
                    <td class="actions"><?php $id = $confederacao['Confederacao']['co_confederacao']; ?> 
                        <div class="btn-group acoes">
                            <?php if($id_pai == ''): ?>
                                <a id="<?php echo $id; ?>" title-modalidade="<?php echo $confederacao['Confederacao']['ds_confederacao']; ?>" class="v_modalidade btn" href="#view_modalidade"
                                        data-toggle="modal"><i
                                        class="silk-icon-fa-users alert-tooltip"
                                        title="Modalidades da Confederação" style="display: inline-block;"></i></a>
                            <?php endif; ?>
                            <?php $nome_confederacao = addslashes($confederacao['Confederacao']['ds_confederacao']); ?>
                            <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $id), array('escape' => false,'class' => 'btn ')); ?>
                            <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $id), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja excluir \'%s\'?', true), $nome_confederacao)); ?>
                        </div>
                    </td>
            </tr>
            <?php
            
            foreach ($confederacao['children'] as $filho):
                $this->listaTabelaConfederacao($filho, $confederacao['Confederacao']['co_confederacao'], $modulo);
            endforeach;
            
        }
        
        function selectTabelaConfederacao($confederacao, $id_selected, $nivel = 0) {
            
            $selected   = '';
            if($confederacao['Confederacao']['co_confederacao'] == $id_selected) {
                $selected   = 'selected';
            }
            $espacos    = $this->Print->getEspacos($nivel);
            echo '<option value="' . $confederacao['Confederacao']['co_confederacao'] . '" ' . $selected . '>' . $espacos . $confederacao['Confederacao']['ds_confederacao'] . '</option>';
                        
            foreach ($confederacao['children'] as $filho):
                $novo_nivel = $nivel + 1;
                $this->selectTabelaConfederacao($filho, $id_selected, $novo_nivel);
            endforeach;
            
        }
        
        function setArrayConfederacoes($confederacao, $nivel = 0) {
            
            $espacos    = $this->Print->getEspacos($nivel);
            $this->arrayConfederacoes[ $confederacao['Confederacao']['co_confederacao'] ] = $espacos . $confederacao['Confederacao']['ds_confederacao'];
                        
            foreach ($confederacao['children'] as $filho):
                $novo_nivel = $nivel + 1;
                $this->setArrayConfederacoes($filho, $novo_nivel);
            endforeach;
            
        }
                
        function getArrayConfederacoes($confederacoes){
            
            $this->arrayConfederacoes = array();
            foreach ($confederacoes as $confederacao):
                $this->setArrayConfederacoes($confederacao);
            endforeach;
            
            return $this->arrayConfederacoes;
        }
        
        function getMenuDocumento($anexo, $permissaoEditar = false, $permissaoExcluir = false, $isCertificadoDigital = false) {
        	$coAnexo = $anexo['Anexo']['co_anexo'];
            $menu = '<div class="btn-group">' . 
                    '<button data-toggle="dropdown" class="btn btn-info btn-minier dropdown-toggle alert-tooltip" title="Menu de Opções"><i class="icon-caret-down"></i></button>' . 
                    '<ul class="dropdown-menu dropdown-info pull-left">' . 
                        '<li><a href="javascript:void(0);" onclick="abrirAnexo(\\\'' . $coAnexo . '\\\');"><i class="icon-folder-open"></i> &nbsp; Abrir Documento</a></li>';
            if($permissaoEditar) {
                $menu .= '<li><a href="javascript:void(0);" onclick="editarAnexo(\\\'' . $coAnexo . '\\\');"><i class="icon-pencil"></i> &nbsp; Editar</a></li>';
            }
            if($permissaoExcluir) {
            	$menu .= '<li><a href="javascript:void(0);" onclick="excluirAnexo(\\\'' . $coAnexo . '\\\', \\\'' . $anexo['Anexo']['ds_anexo'] . '\\\');"><i class="red icon-trash"></i> &nbsp; Excluir</a></li>';
            }
            if($isCertificadoDigital) {
//                $menu .= '<li><a href="javascript:void(0);" onclick="showSignModal(\\\'' . $coAnexo . '\\\', \\\'A\\\');"><i class="icon-certificate"></i> &nbsp; Assinar documento</a></li>';
                $menu .= '<li><a href="javascript:void(0);" onclick="alertRotinaNaoHabilitada()"><i class="icon-certificate"></i> &nbsp; Assinar documento</a></li>';
//                $menu .= '<li><a href="javascript:void(0);" onclick="showSignModal(\\\'' . $coAnexo . '\\\', \\\'V\\\');"><i class="icon-bitbucket-sign"></i> &nbsp; Verificar assinatura</a></li>';
                $menu .= '<li><a href="javascript:void(0);" onclick="alertRotinaNaoHabilitada()"><i class="icon-bitbucket-sign"></i> &nbsp; Verificar assinatura</a></li>';
            }
            $menu .= '</ul></div>';
            return $menu;
        }
        
        function getImgExtensao($extensao, $base) {
            return '<img height="14" width="14" alt="' . $extensao . '" src="' . $base . '/img/extensao/' . $extensao . '.png">';
        }
        
        function getDescricaoDocumento($anexo, $tpDocumento, $base, $isExecucao = false, $isCertificadoDigital = false) {

            if ($tpDocumento == 'EMPENHO') {
                $tpDocumento = __('EMPENHO', true);
            }

            $texto  = '';
            if($isExecucao && isset($anexo['Atividade']['ds_atividade']) && $anexo['Atividade']['ds_atividade'] != '') {
                $texto  .= '<b>' . __('Atividade', true) . '</b>: ' . $anexo['Atividade']['ds_atividade'] . '<BR>';
            }
            $texto  .= '<b>' . __('Tipo de Documento', true) . '</b>: ' . $tpDocumento . '<BR>';
            $texto  .= '<b>' . __('Data', true) . '</b>: ' . $anexo['Anexo']['dt_anexo'] . '<BR>';
            if($isCertificadoDigital) {
                $texto  .= '<b>' . __('Assinatura Digital', true) . '</b>: ' . ((empty($anexo['Anexo']['assinatura'])) ? 'Não' : 'Sim') . '<BR>';
            }
            $texto  .= '<b>' . __('Extensão', true) . '</b>: .' . $anexo['Anexo']['ds_extensao'] . '<BR>';
            return '<span data-rel="popover" data-trigger="hover" data-placement="right" data-content="' . $texto . '" data-html="true">' . $this->getImgExtensao($anexo['Anexo']['ds_extensao'], $base) . ' &nbsp;&nbsp; ' . $anexo['Anexo']['ds_anexo'] . '</span>';
        }
        
}

?>
