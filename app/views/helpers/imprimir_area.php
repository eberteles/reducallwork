<?php

class ImprimirAreaHelper extends AppHelper
{
    var $helpers    = array('Html', 'Print');
    var $arrayAreas = array();
        
    function listaTabelaArea($area, $id_pai = '')
    {
        $class_tree     = "treegrid-" . $area['Area']['co_area'];
        $class_tree_pai = '';
        if($id_pai != '') {
            $class_tree_pai = "treegrid-parent-" . $id_pai;
        }
        ?>
        <tr class="<?php echo $class_tree . ' ' . $class_tree_pai;?>">
            <td>&nbsp;&nbsp;<?php echo $area['Area']['ds_area']; ?>&nbsp;</td>
            <td class="actions"><?php $id = $area['Area']['co_area']; ?> 
                <div class="btn-group acoes">	
                    <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', $id), array('escape' => false,'class' => 'btn ')); ?>
                    <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $id), array('escape' => false,'class' => 'btn  btn-danger'), sprintf(__('Tem certeza de que deseja excluir # %s?', true), $id)); ?>
                </div>
            </td>
        </tr>
        <?php
        foreach ($area['children'] as $filho):
            $this->listaTabelaArea($filho, $area['Area']['co_area']);                
        endforeach;
    }

    function selectTabelaArea($area, $id_selected, $nivel = 0)
    {
        $selected   = '';
        if($area['Area']['co_area'] == $id_selected) {
            $selected   = 'selected';
        }
        $espacos    = $this->Print->getEspacos($nivel);
        echo '<option value="' . $area['Area']['co_area'] . '" ' . $selected . '>' . $espacos . $area['Area']['ds_area'] . '</option>';

        foreach ($area['children'] as $filho):
            $novo_nivel = $nivel + 1;
            $this->selectTabelaArea($filho, $id_selected, $novo_nivel);
        endforeach;
    }

    function setArrayAreas($area, $nivel = 0)
    {
        $espacos = $this->Print->getEspacos($nivel);
        $this->arrayAreas[ $area['Area']['co_area'] ] = $espacos . $area['Area']['ds_area'];

        foreach ($area['children'] as $filho):
            $novo_nivel = $nivel + 1;
            $this->setArrayAreas($filho, $novo_nivel);
        endforeach;            
    }

    function getArrayAreas($areas)
    {
        $this->arrayAreas = array();
        foreach ($areas as $area):
            $this->setArrayAreas($area);
        endforeach;

        return $this->arrayAreas;
    }
}
?>
