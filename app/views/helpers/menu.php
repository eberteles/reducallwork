<?php
/**
 * Helper for print tab navegation.
 *
 * PHP versions 4 and 5
 *
 * @filesource
 * @copyright		Copyright 2010
 * @package			app
 * @subpackage		app.view.helpers
 * @version			$Revision: 0.1 $
 */

/**
 * AbaHelper library.
 *
 * @package		app
 * @subpackage	app.view.helpers
 * @author RaulM
 *
 */
class MenuHelper extends AppHelper
{
	var $helpers = array(
		'Html'
		);

		/**
		 *
		 * @param $links
		 * @param $htmlAttributes
		 * @param $type
		 * @return unknown_type
		 */
		function menu( $links = array(), $htmlAttributes = array(), $type = 'li' )
		{

				$this->tags[ 'ul' ] = '<ul%s>%s</ul>';
				$this->tags[ 'ol' ] = '<ol%s>%s</ol>';
				$this->tags[ 'li' ] = '<li%s>%s</li>';
				$out = array();
				foreach ( $links as $title => $link ) {
					if(isset($link['multi'])){
						$ls = array();
						foreach($link['links'] as $ti => $lp){
                                                    if($lp == 'divider') {
                                                        $ls[] = '<li class="divider"></li>';
                                                    }else {
							$ls[] = sprintf('<li>%s</li>',$this->Html->link( $ti, $lp, array('escape' => false) ), $ti);
                                                    }
						}
						$out[] = sprintf('<li class="dropdown">
                							<a href="#" data-toggle="dropdown" class="dropdown-toggle">%s <b class="caret"></b></a>
												<ul class="dropdown-menu">
													%s
												</ul>
										</li>',$title, implode('',$ls));
					} else {
						if ( $this->url( $link ) == substr( $this->here, 0, -1 ) ) {
							$out[] = sprintf( $this->tags[ 'li' ], ' class="active"', $this->Html->link( $title, $link ) );
						} else {
							$out[] = sprintf( $this->tags[ 'li' ], '', $this->Html->link( $title, $link ) );
						}
					}

				}
				$tmp = join( "\n", $out );
				return $this->output( sprintf( $this->tags[ $type ], $this->_parseAttributes( $htmlAttributes ), $tmp ) );

		}
}
?>