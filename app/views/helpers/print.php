<?php

use N2oti\Integras\Sei\Exception\IntegrasSEIVersionNotSupportedException;
use N2oti\Integras\Sei\Version\Services\Exception\IntegrasServicesArgumentException;
use N2oti\Integras\Sei\Version\Services\Exception\IntegrasServicesRetrieveException;

/**
 * PrintHelper library.
 *
 * @package cake
 * @subpackage cake.app.view.helpers
 */
class PrintHelper extends Helper
{

    /**
     * Atributo responsavel pela data em timestamp
     */
    var $inputValue = NULL;

    var $mesExtenso = array(
        'Jan' => 'Janeiro',
        'Feb' => 'Fevereiro',
        'Mar' => 'Marco',
        'Apr' => 'Abril',
        'May' => 'Maio',
        'Jun' => 'Junho',
        'Jul' => 'Julho',
        'Aug' => 'Agosto',
        'Nov' => 'Novembro',
        'Sep' => 'Setembro',
        'Oct' => 'Outubro',
        'Dec' => 'Dezembro'
    );

    var $mesNuExtenso = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Marco',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
    );

    var $mesNuResumido = array(
        '01' => 'Jan',
        '02' => 'Fev',
        '03' => 'Mar',
        '04' => 'Abr',
        '05' => 'Mai',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Ago',
        '09' => 'Set',
        '10' => 'Out',
        '11' => 'Nov',
        '12' => 'Dez'
    );

    /**
     * Retorna o valor no formato especificado por parametro.
     *
     * O 0(zero) � o elemento chave da formata��o
     *
     * Ex: 000.000.000-00
     *
     * @param string $$strFormat
     * @return string
     */
    function _get($sFormat = NULL)
    {
        if ($this->inputValue == NULL) {
            return NULL;
        }

        if ($sFormat == NULL) {
            return $this->inputValue;
        }

        $sData = $this->inputValue;

        for ($i = 0; $i < strlen($sFormat); $i++) {
            if (substr($sFormat, $i, 1) != "9") {
                $sData = substr($sData, 0, $i) . substr($sFormat, $i, 1) . substr($sData, $i, strlen($sData));
            }
        }
        return $sData;
    }

    /**
     * Tira todos os caracteres que n�o s�o num�ricos
     *
     * @param string $$sInput
     * @return void
     */
    function _set($sInput)
    {
        if (!isset($sInput)) {
            $this->inputValue = NULL;
        } else {
            for ($i = 0; $i < strlen($sInput); $i++) {
                if (!is_numeric(substr($sInput, $i, 1))) {
                    $sInput = str_replace(substr($sInput, $i, 1), "", $sInput);
                }
            }
            $this->inputValue = $sInput;
        }
    }

    /**
     * Retorna a informacao com a mascara informada
     *
     * @param string $sInput
     *            String que ira receber a mascara
     * @param string $sMask
     *            Mascara para formatar a string
     */
    function mask($sInput, $sMask)
    {
        $this->_set($sInput);
        return $this->_get($sMask);
    }

    public function tpAditivo($tpAditivo)
    {
        switch ($tpAditivo) {
            case 1:
                return __('Aditivo de Valor', true);
                break;

            case 2:
                return 'Aditivo de Prazo';
                break;

            case 3:
                return 'Aditivo de Valor e Prazo';
                break;

            case 4:
                return 'Aditivo de Supressão';
                break;

            case 5:
                return 'Aditivo (Outros)';
                break;

            case 6:
                return 'Aditivo de Retificação';
                break;
            case 7:
                return 'Aditivo de Prazo e Supressão';
                break;
            case 8:
                return 'Aditivo de Reajuste';
                break;
        }
    }

    function statusAta($tpStatus, $dt_vigencia_fim)
    {
        if ($this->difEmDias(date('d/m/Y'), $dt_vigencia_fim) < 0) {
            return 'Expirada';
        }
        if ($tpStatus == 'V') {
            return 'Vigente';
        }
        if ($tpStatus == 'E') {
            return 'Expirada';
        }
        if ($tpStatus == 'C') {
            return 'Cancelada';
        }
    }

    /**
     * Retorna o numero com o formato de dinheiro brasileiro
     */
    function real($sInput, $force = false)
    {
        if ($sInput == "") {
            return "0,00";
        } else {
            if ($force) {
                $sInput = ln($sInput);
            }
            if (strpos($sInput, ',')) { // Já formatado
                return $sInput;
            } else {
                return number_format($sInput, 2, ",", ".");
            }
        }
    }

    /**
     * Retorna o numero inteiro com separador de milhar
     */
    function milhar($sInput)
    {
        return number_format($sInput, 0, "", ".");
    }

    /**
     * Retora o valor que tem referencia no array
     */
    function type($sInput, $aOptions)
    {
        foreach ($aOptions as $key => $val) {
            if ($sInput == $key) {
                return $val;
            }
        }
        return;
    }

    /**
     * Retorna a data no formato brasileiro
     */
    function data($sInput)
    {
        return $sInput;
    }

    /**
     * Retorna a data e hora no formato brasileiro
     */
    function dataHora($sInput)
    {
        return $sInput;
    }

    /**
     * Retorna um cep mascarado
     *
     * @param string $sInput
     *            String que ira receber a mascara
     */
    function cep($sInput)
    {
        return FunctionsComponent::mascara($sInput, 'cep');
    }

    /**
     * Retorna um cpf mascarado
     *
     * @param string $sInput
     *            String que ira receber a mascara
     */
    function cpf($sInput)
    {
        if (strlen($sInput) == 11) {
            return FunctionsComponent::mascara($sInput, 'cpf');
        } else {
            return $sInput;
        }
    }

    /**
     * Retorna um cnpj mascarado
     *
     * @param string $sInput
     *            String que ira receber a mascara
     */
    function cnpj($sInput)
    {
        return FunctionsComponent::mascara($sInput, 'cnpj');
    }

    function cnpjCpf($sInput, $tipo)
    {
        if (up($tipo) == 'J') {
            return FunctionsComponent::mascara($sInput, 'cnpj');
        } else {
            return FunctionsComponent::mascara($sInput, 'cpf');
        }
    }

    /**
     * Retorna um cnpj mascarado
     *
     * @param string $sInput
     *            String que ira receber a mascara
     */
    function telefone($sInput)
    {
        return FunctionsComponent::mascara($sInput, 'telefone');
    }

    /**
     * Retorna um contrato mascarado
     *
     * @param string $sInput
     *            String que ira receber a mascara
     */
    function contrato($sInput, $tpContrato = null)
    {
        $modulo = new ModuloHelper();
        if ($sInput == NULL || $sInput == "") {
            return "---";
        } else {
            return FunctionsComponent::mascara($sInput, 'contrato');
        }
    }

    function numeroDeOrdem($sInput, $tp = null)
    {
        if ($sInput == NULL || $sInput == "") {
            return "---";
        } else {
            return FunctionsComponent::mascara($sInput, 'ordem');
        }
    }

    function pam($sInput)
    {
        if ($sInput == NULL || $sInput == "") {
            return "---";
        } else {
            return FunctionsComponent::mascara($sInput, 'pam');
        }
    }

    function tpValor($tpValor)
    {
        if ($tpValor == "F") {
            return "FIXO";
        } else
            if ($tpValor == "V") {
                return "VARIÁVEL";
            } else {
                return "NÃO INFORMADO";
            }
    }

    function tpAnulacao($tpValor)
    {
        if ($tpValor == "T") {
            return "Total";
        } else
            if ($tpValor == "P") {
                return "Parcial";
            } else {
                return "--";
            }
    }

    function getEspacos($nivel)
    {
        if ($nivel == 0) {
            return "";
        } else {
            $nivel = $nivel * 3;
            $espacos = '';
            for ($i = 0; $i < $nivel; $i++) {
                $espacos .= "&nbsp; ";
            }
            return $espacos;
        }
    }

    function getCrumbs($controller, $path, $data = null)
    {
        $divisao = '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>';
        $caminho = '<ul class="breadcrumb"><li><i class="icon-home home-icon"></i><a href="' . $path . '/">Início</a>' . $divisao . '</li>';

        if ($controller['action'] == 'index' || $controller['url']['url'] == 'contratos/pesquisa_avancada' || $controller['url']['url'] == 'pages/siafi') {
            $caminho .= '<li class="active">' . $this->getNomeModulo($controller, $data) . '</li>';
        } else {
            $caminho .= '<li><a href="' . $path . '/' . $controller['controller'] . '">' . $this->getNomeModulo($controller, $data) . '</a>' . $divisao . '</li>';
            $caminho .= '<li class="active">' . $this->getNomeAction($controller['action']) . '</li>';
        }

        $caminho .= '</ul>';
        return $caminho;
    }

    function getNomeModulo($controller, $data = null)
    {
        $modulo = new ModuloHelper();

        if ($controller['controller'] == 'dashboard') {
            return __('Dashboard', true);
        }

        if ($controller['controller'] == 'projetos') {
            return 'Projetos';
        }

        if ($controller['controller'] == 'contratos') {
            if ($controller['url']['url'] == 'contratos/index' || $controller['url']['url'] == 'contratos') {
                if ($modulo->isProcesso()) {
                    return __('Processos', true) . ' / ' . __('Contratos', true);
                } else {
                    return __('Contratos', true);
                }
            }
            if ($controller['url']['url'] == 'contratos/pesquisa_avancada') {
                return 'Pesquisa Avançada';
            }
            if (substr_count($controller['url']['url'], 'add_inscricao') > 0) {
                return 'Inscrições';
            }
            if ($controller['url']['url'] == 'contratos/add_processo') {
                return 'Processos';
            }
            if ($controller['url']['url'] == 'contratos/add_pam') {
                return __('PAM/s', true);
            }
            if (isset($data['Contrato'])) {
                if ($data['Contrato']['nu_contrato'] > 0) {
                    return __('Contratos', true);
                }
                if ($data['Contrato']['nu_processo'] > 0) {
                    return __('Processos', true);
                }
                if (isset($data['Contrato']['nu_pam']) && $data['Contrato']['nu_pam'] > 0) {
                    return __('PAM/s', true);
                }
            }
            return __('Contratos', true);
        }

        if ($controller['controller'] == 'fornecedores') {
            return __('Fornecedores', true);
        }
        if ($controller['controller'] == 'setores') {
            return __('Unidades Administrativas', true);
        }
        if ($controller['controller'] == 'modalidades') {
            return __('Tipo de Contrato', true);
        }
        if ($controller['controller'] == 'contratacoes') {
            return __('Modalidade de Contratação', true);
        }
        if ($controller['controller'] == 'situacoes') {
            if ($modulo->isCamposContrato('co_situacao_processo')) :
                return __('Situações do Processo / Contrato', true);
            else :
                return __('Situações do Contrato', true);
            endif;
        }
        if ($controller['controller'] == 'fases') {
            return 'Fases de Tramitação Processual';
        }
        if ($controller['controller'] == 'servicos') {
            return 'Descrição de Serviço';
        }
        if ($controller['controller'] == 'usuarios') {
            return 'Usuários';
        }
        if ($controller['controller'] == 'logs') {
            return 'Log de Acesso';
        }
        if ($controller['controller'] == 'alertas') {
            return 'Central de Alertas';
        }
        if ($controller['controller'] == 'relatorios') {
            return 'Relatórios';
        }
        if ($controller['controller'] == 'grupo_auxiliares') {
            return __('Grupo Auxiliar', true);
        }
        if ($controller['controller'] == 'siafis') {
            return 'SIAFI';
        }
        if ($controller['controller'] == 'siasg') {
            return 'SIASG';
        }
        if ($controller['controller'] == 'atas') {
            return 'Atas';
        }
        if ($controller['controller'] == 'produtos_servicos') {
            return __('Produtos / Serviços', true);
        }
        if ($controller['controller'] == 'atas_categorias') {
            return __('Atas - Categorias', true);
        }
        if ($controller['controller'] == 'areas') {
            return __('Áreas / Sub Áreas de Fornecedores', true);
        }
        if ($controller['controller'] == 'categorias') {
            return __('Categorias', true);
        }
        if ($controller['controller'] == 'subcategorias') {
            return __('Sub Categorias', true);
        }
        if ($controller['controller'] == 'fiscais') {
            return __('Fiscais', true);
        }
        if ($controller['controller'] == 'eventos') {
            return __('Eventos', true);
        }
        if ($controller['controller'] == 'dsp_despesas_de_viagem') {
            return __('Diárias e Passagens', true);
        }
        if ($controller['controller'] == 'dsp_cargos') {
            return __('Cargos', true);
        }
        if ($controller['controller'] == 'dsp_unidade_lotacao') {
            return __('Unidade de Lotação', true);
        }
        if ($controller['controller'] == 'dsp_funcao') {
            return __('Função', true);
        }
        if ($controller['controller'] == 'dsp_meio_transporte') {
            return __('Unidade de Lotação', true);
        }
        if ($controller['controller'] == 'dsp_categoria_passagem') {
            return __('Categoria de Passagem', true);
        }
        if ($controller['controller'] == 'dsp_campos_auxiliares') {
            return __('Campos Auxiliares', true);
        }
        if ($controller['controller'] == 'lct_licitacoes') {
            return __('Licitações', true);
        }
        if ($controller['controller'] == 'importacao') {
            return __('Importação', true);
        }
        if ($controller['controller'] == 'anexos') {
            return __('Suporte Documental', true);
        }
        if ($controller['controller'] == 'atividades') {
            return __('Atividades', true);
        }
        if ($controller['controller'] == 'emails') {
            return __('Mensagens', true);
        }
        if ($controller['controller'] == 'modelos') {
            return __('Modelos de Documento', true);
        }

        $controller['controller'] = ucfirst($controller['controller']);

        return __($controller['controller'], true);
    }

    function getNomeAction($action)
    {
        if ($action == 'add' || $action == 'add_processo' || $action == 'add_pam' || $action == 'add_inscricao') {
            return 'Adicionar';
        }
        if ($action == 'edit') {
            return 'Editar';
        }
        if ($action == 'detalha') {
            return 'Detalhar';
        }
        if ($action == 'pesquisar' || $action == 'atividades_gerais') {
            return 'Pesquisar';
        }
    }

    function tpIndicador($tpIndicador)
    {
        if ($tpIndicador == "1" || $tpIndicador == "S") {
            return "Sim";
        } elseif ($tpIndicador == "0" || $tpIndicador == "N") {
            return "Não";
        } else {
            return "--";
        }
    }

    function getHelpPendencia($pendencia)
    {
        $texto = $pendencia['ds_pendencia'];
        if ($pendencia['dt_fim'] != '') {
            $texto .= '<br>Dt Fim: ' . $pendencia['dt_fim'];
        }
        if ($pendencia['ds_observacao'] != '') {
            $texto .= '<br>Observação: ' . $pendencia['ds_observacao'];
        }
        return $texto;
    }

    function printHelp($campo, $help = '', $tamanho = 0)
    {
        if ($help != "") {
            if ($tamanho > 0 && strlen($campo) > $tamanho) {
                $campo = substr($campo, 0, $tamanho) . ' ...';
            }
            return '<div class="alert-tooltip" title="' . $help . '">' . $campo . '</div>';
        } else {
            return $campo . '<br>';
        }
    }

    function tpQuitacao($tpQuitacao)
    {
        if ($tpQuitacao == "Q") {
            return "Quitado";
        } else if ($tpQuitacao == "C") {
            return "Comprometido";
        } else if ($tpQuitacao == "V") {
            return "Vencido";
        } else {
            return "";
        }
    }

    function saudacao()
    {
        $hora = date("H");

        if ($hora >= 5 && $hora < 12) {
            return "Bom Dia";
        } elseif ($hora >= 12 && $hora < 19) {
            return "Boa Tarde";
        } elseif ($hora >= 19 || $hora < 5) {
            return "Boa Noite";
        }
    }

    function notaFiscal($nf, $serie = '')
    {
        if (is_array($nf)) {
            if (isset($nf['nu_nota'])) {
                $nota = $nf['nu_nota'];
                if (isset($nf['nu_serie']) && $nf['nu_serie'] != '') {
                    $nota = $nota . ' - ' . $nf['nu_serie'];
                }
                if (isset($nf['vl_nota'])) {
                    $nota = $nota . ' / R$' . $this->real($nf['vl_nota']);
                }
                return $nota;
            } else {
                return "--";
            }
        } else {
            if (strstr($nf, 'PRV')) {
                return $nf;
            } else {
                return $nf . " - " . $serie;
            }
        }
    }

    function iconeProcessoSEI($sInput)
    {
        if ($sInput == NULL || $sInput == "") {
            return "";
        } else {
            $icon = '';
            App::import('Helper', 'Sei');
            $seiHelper = new SeiHelper();

            if ($seiHelper->isEnabled()) {
                try {
                    $seiHelper->consultarProcesso($sInput);
                    $icon = ' <a href="javascript:void(0);"><i class="icone-sei"></i></a> ';
                } catch (IntegrasSEIVersionNotSupportedException $exception) {
                    throw $exception;
                } catch (IntegrasServicesArgumentException $exception) {
                    throw $exception;
                } catch (IntegrasServicesRetrieveException $exception) {
                    $mensagem = preg_replace("/.*\[([^\]]+)\]/", '$1', $exception->getMessage());
                    $icon = '<a href="javascript:void(0)"><i class="icone-sei-disable" title="' . $mensagem . '"></i></a>';
                } catch (\Exception $exception) {
                    error_log("[{$sInput}] -- " . $exception);
                    throw $exception;
                }
            }
            return $icon;
        }
    }


    /**
     * Retorna um processo mascarado
     *
     * @param $sInput String que ira receber a mascara
     * @param bool $checkSei
     * @return string|unknown_type
     */
    function processo($sInput, $checkSei = false)
    {
        $modulo = new ModuloHelper();
        if ($sInput == NULL || $sInput == "") {
            return "---";
        } else {

            $numeroFormatado = '';

            if (strlen($sInput) == 17) {
                $numeroFormatado = FunctionsComponent::mascara($sInput, 'processo', 17);
            } else {
                $numeroFormatado = FunctionsComponent::mascara($sInput, 'processo');
            }

            if ($checkSei) {
                $numeroFormatado = $numeroFormatado . $this->iconeProcessoSEI($sInput);
            }

            return $numeroFormatado;
        }
    }

    function licitacao($sInput)
    {
        return FunctionsComponent::mascara($sInput, 'licitacao');
    }

    function ata($sInput)
    {
        return FunctionsComponent::mascara($sInput, 'ata');
    }

    function faseAtual($fase, $setor, $situacao = null)
    {
        if ($fase == '') {
            if ($situacao != null && $situacao != '') {
                return $situacao;
            }
            return '--';
        } else {
            if($setor != '') {
                return $setor . ' / ' . $fase;
            } else {
                return $fase;
            }
        }
    }

    /**
     * Retorna a quantidade de dias para expirar o contrato
     *
     * @param $dtFim String com a data de fim
     * @param string $formato
     * @return int
     */
    function expira($dtFim, $formato = "PT")
    {
        if ($formato == "US") {
            $dtFim = FunctionsComponent::data($dtFim);
        }
        if (!empty($dtFim)) {
            $time_inicial = new \DateTime('now');
            $time_final = new \DateTime($this->nData($dtFim));

            /** @var Contrato $contratoMdl */
            $contratoMdl = ClassRegistry::init('Contrato');
            $diferenca = $contratoMdl->getDiferencaEmDias(
                $time_inicial->format('Y-m-d'),
                $time_final->format('Y-m-d')
            );

            return ($diferenca < 0) ? -1 : $diferenca;
        } else {
            return -1;
        }
    }

    public function nData($data)
    {
        $objDateTime = DateTime::createFromFormat('d/m/Y', $data);
        if (!$objDateTime) {
            return false;
        }
        return $objDateTime->format('Y-m-d');
    }

    function getMenuAtivo($controller, $controllerAtual, $actionAtual = 'todos')
    {
        if ($controller['controller'] == $controllerAtual && $controller['action'] == $actionAtual) {
            return ' class = "active" ';
        } else
            if ($actionAtual == 'todos' && $controller['controller'] == $controllerAtual) {
                return ' class = "active" ';
            } else {
                return '';
            }
    }

    function getMenuAtivoFilho($controller, $paginasFilho, $verificarSoController = false)
    {
        foreach ($paginasFilho as $pagina) :
            if ($verificarSoController) {
                $url = $controller['controller'];
            } else {
                $url = $controller['controller'] . '/' . $controller['action'];
            }
            if ($url == $pagina) {
                return ' class = "active open" ';
            }
        endforeach;
        return '';
    }

    function fimDaVigencia($contrato)
    {
        if ($contrato['Contrato']['dt_fim_processo'] == "" && $contrato['Contrato']['dt_fim_vigencia'] == "") {
            return '---';
        } else if ($contrato['Penalidade'] && $contrato['Penalidade'][0]['ic_situacao'] == 2) {
            return $this->printHelp('<font color="#FF0000">Encerrado</font>');
        } else {
            if ($contrato['Contrato']['dt_fim_vigencia'] != "") {
                $diasExpira = $this->expira($contrato['Contrato']['dt_fim_vigencia']);
                if ($diasExpira > -1) {
                    if ($diasExpira == 0) {
                        $descricao = 'Hoje';
                    } else if ($diasExpira == 1) {
                        $descricao = 'Amanhã';
                    } else {
                        $descricao = $diasExpira . ' dias';
                    }
                    return $this->printHelp($descricao, 'Período: ' . $contrato['Contrato']['dt_ini_vigencia'] . ' à ' . $contrato['Contrato']['dt_fim_vigencia']);
                } else {
                    return $this->printHelp('<font color="#FF0000">Finalizado</font>', 'Período: ' . $contrato['Contrato']['dt_ini_vigencia'] . ' à ' . $contrato['Contrato']['dt_fim_vigencia']);
                }
            } else {
                if ($contrato['Contrato']['dt_fim_processo'] != "" && $contrato['Contrato']['nu_contrato'] == "") {
                    $diasExpira = $this->expira($contrato['Contrato']['dt_fim_processo']);
                    if ($diasExpira > 0) {
                        return $this->printHelp($diasExpira . ' dias', 'Período: ' . $contrato['Contrato']['dt_ini_processo'] . ' à ' . $contrato['Contrato']['dt_fim_processo']);
                    } else {
                        return $this->printHelp('<font color="#FF0000">Finalizado</font>', 'Período: ' . $contrato['Contrato']['dt_ini_processo'] . ' à ' . $contrato['Contrato']['dt_fim_processo']);
                    }
                } else {
                    return '---';
                }
            }
        }
    }

    /**
     * Retorna a quantidade de dias entre duas datas
     *
     * @param string $dtFim
     *            String com a data de fim
     */
    function difEmDias($dtIni, $dtFim, $formato = "PT")
    {
        if ($formato == "US") {
            $dtIni = FunctionsComponent::data($dtIni);
            $dtFim = FunctionsComponent::data($dtFim);
        }
        if (!empty($dtFim)) {
            $time_inicial = FunctionsComponent::geraTimestamp($dtIni);
            $time_final = FunctionsComponent::geraTimestamp($dtFim);

            $diferenca = $time_final - $time_inicial; // segundos

            return (int)floor($diferenca / (60 * 60 * 24));
        } else {
            return -1;
        }
    }

    function proximoDia($data, $days = 1)
    {
        return date('d/m/Y', strtotime(dtDb($data) . ' + ' . $days . ' days'));
    }

    function prazoAtividade($tp_andamento, $dtFim)
    {
        if ($tp_andamento == "F") {
            return "Concluído";
        } else if ($tp_andamento == "N" || ($dtFim == null || $dtFim == '')) {
            return "-";
        } else {
            $dias = $this->expira($dtFim);
            if ($dias == 0) {
                return "Hoje";
            } else if ($dias > 0) {
                return $dias . " dias";
            } else {
                $dias = $dias * -1;
                if ($dias == 1) {
                    return "1 dia atrasado";
                } else {
                    return $dias . " dias atrasado";
                }
            }
        }
    }

    function iconeAtividade($tp_andamento)
    {
        $icone = '<i class="alert-tooltip ';
        $titulo = ' title="' . $this->tituloAtividade($tp_andamento) . '" style="display: inline-block;"></i>';
        if ($tp_andamento == "N") {
            $icone .= 'ui-icon-control-play-blue"' . $titulo;
            $icone = '<a class="ini_atividade" href="#i_atividade">' . $icone . '</a>';
        }
        if ($tp_andamento == "E") {
            $icone .= 'ui-icon-hourglass"' . $titulo;
        }
        if ($tp_andamento == "P") {
            $icone .= 'silk-icon-flag-red"' . $titulo;
        }
        if ($tp_andamento == "F") {
            $icone .= 'silk-icon-accept"' . $titulo;
        }
        return $icone;
    }

    function getIconeAtividade($tp_andamento)
    {
        $class_icone = "";
        if ($tp_andamento == "N") {
            $class_icone .= "ui-icon-control-play-blue";
        }
        if ($tp_andamento == "E") {
            $class_icone .= "ui-icon-hourglass";
        }
        if ($tp_andamento == "P") {
            $class_icone .= "silk-icon-flag-red";
        }
        if ($tp_andamento == "F") {
            $class_icone .= "ui-icon silk-icon-accept";
        }
        return $class_icone;
    }

    function alertAtividadeDashboard($atividade)
    {
        $alert = $this->alertAtividade($atividade);
        if ($alert == 'error') {
            return 'danger';
        } else {
            return $alert;
        }
    }

    function alertAtividade($atividade)
    {
        if ($atividade['Atividade']['tp_andamento'] == "F") {
            return "success";
        }

        if ($this->expira($atividade['Atividade']['dt_ini_planejado']) >= 0) {
            return "success";
        }

        $dias = $this->expira($atividade['Atividade']['dt_fim_planejado']);
        if ($dias < 0) {
            return "error";
        }

        $planejado = $this->difEmDias($atividade['Atividade']['dt_ini_planejado'], $atividade['Atividade']['dt_fim_planejado']);
        $diasAteHj = $this->difEmDias($atividade['Atividade']['dt_ini_planejado'], date('d/m/Y'));
        $percEsperado = 100 * $diasAteHj / $planejado;
        $percRealizado = 0;
        if ($percEsperado < 1) {
            return "success";
        }
        if ($percEsperado > 0) {
            $percRealizado = 100 * $atividade['Atividade']['pc_executado'] / $percEsperado;
        }
        if ($percRealizado < 75) {
            return "error";
        } else
            if ($percRealizado >= 75 && $percRealizado < 100) {
                return "warning";
            } else {
                return "success";
            }
    }

    function tituloAtividade($tp_andamento)
    {
        if ($tp_andamento == "N") {
            return "Não Iniciada - Clique para iniciar a Atividade!";
        }
        if ($tp_andamento == "E") {
            return "Em Andamento";
        }
        if ($tp_andamento == "P") {
            return "Pendente";
        }
        if ($tp_andamento == "F") {
            return "Finalizada";
        }
    }

    /**
     * Valor formatado com símbolo de %
     *
     * @param float $numero
     *            Número
     * @param integer $casasDecimais
     *            Número de casas decimais
     * @return string Número formatado com %
     * @access public
     */
    function porcentagem($numero, $casasDecimais = 2)
    {
        return $this->precisao($numero, $casasDecimais) . '%';
    }

    /**
     * Número float com ponto ao invés de vírgula
     *
     * @param float $numero
     *            Número
     * @param integer $casasDecimais
     *            Número de casas decimais
     * @return string Número formatado
     * @access public
     */
    function precisao($numero, $casasDecimais = 3)
    {
        return number_format($numero, $casasDecimais, ',', '.');
    }

    function getBtnEditCombo($titulo, $idBtn, $modal, $permissao, $class_icone = 'blue icon-edit bigger-150')
    {
        if ($permissao) {
            return '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="' . $titulo . '" id="' . $idBtn . '" href="' . $modal . '" data-toggle="modal"><i class="' . $class_icone . '"></i></a>';
        } else {
            return '';
        }
    }

    function checkPrivilegio($coPerfil, $rota)
    {
        if (Configure::read('App.customAcl')) {
            $filehash = "rs" . md5($coPerfil);

            if (($recursos_privados = Cache::read($filehash)) !== false) {
                if (in_array($rota, $recursos_privados)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    function checkPermissao($coPerfil, $rota, $penalidade = false)
    {
        if (Configure::read('App.customAcl')) {
            $filehash = "rs" . md5($coPerfil);

            if (!$penalidade) {
                if (($recursos_privados = Cache::read($filehash)) !== false) {
                    if (in_array($rota, $recursos_privados)) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    function tpApostilamento($tpApostilamento)
    {
        $str = '';
        switch ($tpApostilamento) {
            case Apostilamento::TP_APOSTILAMENTO_VALOR:
                $str = Apostilamento::getTipo(Apostilamento::TP_APOSTILAMENTO_VALOR);
                break;
            case Apostilamento::TP_APOSTILAMENTO_PRAZO:
                $str = Apostilamento::getTipo(Apostilamento::TP_APOSTILAMENTO_PRAZO);
                break;
            case Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO:
                $str = Apostilamento::getTipo(Apostilamento::TP_APOSTILAMENTO_VALOR_PRAZO);
                break;
            case Apostilamento::TP_APOSTILAMENTO_SUPRESSAO:
                $str = Apostilamento::getTipo(Apostilamento::TP_APOSTILAMENTO_SUPRESSAO);
                break;
            case Apostilamento::TP_APOSTILAMENTO_REPACTUACAO:
                $str = Apostilamento::getTipo(Apostilamento::TP_APOSTILAMENTO_REPACTUACAO);
                break;
            default;
                $str = 'Apostilamento (Outros)';
        }
        return $str;
    }

    function getMinutosToData($minutos)
    {
        $dias = '';

        if ($minutos >= 1440) {
            $dias = (int)($minutos / 1440);
            $minutos -= $dias * 1440;
            $dias = $dias . ' dia(s) ';
        }
        $horas = 0;
        if ($minutos < 1400 && $minutos >= 60) {
            $horas = (int)($minutos / 60);
            $minutos -= $horas * 60;
        }

        if ($horas > 0 || $minutos > 0) {
            return $dias . date("H:i", strtotime("$horas:$minutos"));
        } else {
            return $dias;
        }
    }

    function getDiferencaDataHoraEmSegundos($prazo_inicial, $prazo_final)
    {
        $dtFim = FunctionsComponent::geraTimestamp(date('d/m/Y H:i:s'), true);
        if ($prazo_final != '') {
            $dtFim = FunctionsComponent::geraTimestamp($prazo_final, true);
        }
        return (int)floor($dtFim - FunctionsComponent::geraTimestamp($prazo_inicial, true)); // segundos
    }

    function getPrazoDecorrido($prazo_inicial, $prazo_final)
    {
        $segundos = $this->getDiferencaDataHoraEmSegundos($prazo_inicial, $prazo_final);

        if ($segundos < 0) {
            return '--';
        } else
            if ($segundos > 86400) {
                return (int)floor($segundos / (60 * 60 * 24)) . ' dia(s)';
            } else {
                $horas = floor($segundos / 3600);
                $segundos -= $horas * 3600;
                $minutos = floor($segundos / 60);
                $segundos -= $minutos * 60;
                return date("H:i:s", strtotime("$horas:$minutos:$segundos"));
            }
    }

    function setPrazoDecorridoDetalhaProcesso($prazo_inicial, $prazo_final, $prazo_fase, &$corAviso, &$mensagemAviso, &$tempoDecorrido)
    {
        if ($prazo_fase > 0) {
            $decorridoEmMinutos = floor($this->getDiferencaDataHoraEmSegundos($prazo_inicial, $prazo_final) / 60);
            if ($decorridoEmMinutos > $prazo_fase) {
                $corAviso = 'red';
                $mensagemAviso = 'Fase atrasada! Esta fase deveria ter sido concluída em: ' . date('d/m/Y H:i:s', FunctionsComponent::geraTimestamp($prazo_inicial, true) + ($prazo_fase * 60));
                $tempoDecorrido = $this->getPrazoDecorrido($prazo_inicial, $prazo_final);
            } else {
                $corAviso = 'green';
                $mensagemAviso = 'Prazo restante para Conclusão da Fase Atual.';
                $prazo_final = date('d/m/Y H:i:s', FunctionsComponent::geraTimestamp($prazo_inicial, true) + ($prazo_fase * 60));
                $tempoDecorrido = $this->getPrazoDecorrido(date('d/m/Y H:i:s'), $prazo_final);
            }
        } else {
            $corAviso = 'green';
            $mensagemAviso = 'Prazo decorrido na Fase Atual.';
            $tempoDecorrido = $this->getPrazoDecorrido($prazo_inicial, $prazo_final);
        }
    }

    function getTempoDecorrido($prazo_inicial, $prazo_final, $prazo_fase)
    {
        if ($prazo_fase > 0) {
            $decorridoEmMinutos = floor($this->getDiferencaDataHoraEmSegundos($prazo_inicial, $prazo_final) / 60);
            if ($decorridoEmMinutos > $prazo_fase) {
                $tempoDecorrido = $this->getPrazoDecorrido($prazo_inicial, $prazo_final);
            } else {
                $prazo_final = date('d/m/Y H:i:s', FunctionsComponent::geraTimestamp($prazo_inicial, true) + ($prazo_fase * 60));
                $tempoDecorrido = $this->getPrazoDecorrido(date('d/m/Y H:i:s'), $prazo_final);
            }
        } else {
            $tempoDecorrido = $this->getPrazoDecorrido($prazo_inicial, $prazo_final);
        }

        return $tempoDecorrido;
    }

    function checkPrazoDecorridoAndamento($prazo_inicial, $prazo_final, $prazo_fase)
    {
        if ($prazo_fase > 0) {
            $decorridoEmMinutos = floor($this->getDiferencaDataHoraEmSegundos($prazo_inicial, $prazo_final) / 60);
            if ($decorridoEmMinutos > $prazo_fase) {
                return ' <i class="icon-warning-sign orange bigger-130 alert-tooltip" title="O tempo planejado para esta Fase, ' . $this->getMinutosToData($prazo_fase) . ', foi excedido!"></i>';
            }
        }
    }

    function getIcTipoContrato($usuario)
    {
        if (isset($usuario['ic_tipo_contrato'])) {
            return $usuario['ic_tipo_contrato'];
        } else {
            return '';
        }
    }

    function getLabelFornecedor($contratoExterno, $icTipoContrato = '')
    {
        if ($contratoExterno) {
            if ($icTipoContrato == 'E') {
                return 'Cliente';
            }
            if ($icTipoContrato == 'I') {
                return __('Fornecedor', true);
            }
            return 'Fornecedor/Cliente';
        } else {
            return __('Fornecedor', true);
        }
    }

    function getLabelPagamento($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            echo 'Recebimento';
        } else {
            __('Pagamento');
        }
    }

    function getLabelDtPagamento($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            return 'Dt. Recebimento';
        } else {
            return __('Dt. Pagamento', true);
        }
    }

    function getLabelVlPagamento($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            return 'Vl. Recebimento';
        } else {
            return __('Vl. Pagamento', true);
        }
    }

    function getLabelPagamentos($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            return 'Cronograma de Recebimento';
        } else {
            return __('Pagamentos', true);
        }
    }

    function getLabelTtPago($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            echo 'Total Recebido';
        } else {
            __('Total Pago');
        }
    }

    function getLabelVlPago($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            return 'Valor Recebido (R$)';
        } else {
            return __('Valor Pago (R$)', true);
        }
    }

    function getLabelVlRestante($icTipoContrato = '')
    {
        if ($icTipoContrato == 'E') {
            echo 'Valor a Receber';
        } else {
            __('Valor Restante');
        }
    }

    function diffDate($date1, $date2)
    {
        $data1 = new DateTime($this->dateSql($date1));
        $data2 = new DateTime($this->dateSql($date2));
        return $data1->diff($data2)->days;
    }

    public function dateSql($dateSql)
    {
        $ano = substr($dateSql, 6, 4);
        $mes = substr($dateSql, 3, 2);
        $dia = substr($dateSql, 0, 2);
        return $ano . "-" . $mes . "-" . $dia;
    }

    public function dateEmail($data)
    {
        $hj = new DateTime();
        $dtEmail = new DateTime($this->dateSql($data) . ' ' . substr($data, 11, 8));
        $diferenca = $dtEmail->diff($hj);
        if ($diferenca->d == 0) {
            return $dtEmail->format('H:m');
        } else {
            return $dtEmail->format('d') . ' de ' . $this->mesNuResumido[$dtEmail->format('m')];
        }
    }
}
