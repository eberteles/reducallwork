<?php

use N2oti\Integras\Sei\IntegrasSEI;
use N2oti\Integras\Sei\IntegrasConfigParamenterSEI;

/**
 * SeiHelper library.
 *
 * @package cake
 * @subpackage cake.app.view.helpers
 */
class SeiHelper extends Helper
{

    private $configs = array();

    public function __construct()
    {
        $this->configs = Configure::read('App.config.component.sei');
    }

    public function isEnabled()
    {
        return $this->configs['enabled'];
    }

    public function getConfigParam($key)
    {
        return $this->hasConfigParam($key)
            ? $this->configs['params'][$key]
            : null;
    }

    public function hasConfigParam($key)
    {
        return isset($this->configs['params'][$key]);
    }

    public function consultarProcesso($sInput, $idUnidade = null)
    {
        if ($sInput == NULL || $sInput == "") {
            return null;
        } else {
            $result = null;
            if ($this->isEnabled()) {
                try {
                    $integrasSeiClient = IntegrasSEI::factory(
                        new IntegrasConfigParamenterSEI(
                            $this->getConfigParam('integrasConfig'),
                            $this->getConfigParam('version')
                        )
                    );
                    $result = $integrasSeiClient->consultarProcedimento($sInput, $idUnidade);

                } catch (\Exception $e) {
                    error_log("[{$sInput}][{$idUnidade}] -- " . $e);
                    throw $e;
                }
            }
            return $result;
        }
    }

    public function formataProcessosDoSeiParaJsonDaTreeView($processosDoSei)
    {
        $processos = array();
        $link = '<a onclick="%s">%s</a>';
        foreach ($processosDoSei as $processo) {
            $detalharAndamento = $processo->ProcedimentoFormatado;
            if ($processo->Historicos) {
                $detalhamentosArray = array_map(function($andamento, $index) {
                    $andamento['descricao'] = utf8_encode($andamento['descricao']);
                    return $andamento;
                }, $processo->Historicos
                );
                $detalhamentos = json_encode($detalhamentosArray);
                $detalharAndamento = sprintf("<a href='javascript:void(0);' onclick='detalharAndamento(this);' data-historico='%s'>%s (Detalhar Andamento)</a>", $detalhamentos, $processo->ProcedimentoFormatado);
            }
            $processos[] = array(
                'id' => $processo->id,
                'text' => $detalharAndamento,
                'state' => (object) array('disabled' => !$processo->achouNoSei),
                'expandIcon' => $processo->achouNoSei ? 'glyphicon glyphicon-plus' : null,
                'tags' => $processo->achouNoSei ? array(count($processo->Documentos)) : null,
                'nodes' => array_map(function ($documento) use ($link) {
                            $nomeDocumento = sprintf('%s (%s)', utf8_encode($documento['tipo']), $documento['documento']);
                            $url = $documento['link'] ? preg_replace("/.*'([^']+)'.*/", '$1', $documento['link']) : '';
                            return array(
                                'text' => $documento['link'] ? sprintf($link, "carregaDocumento('{$url}');", $nomeDocumento) : $nomeDocumento,
                                'tags' => $documento['parent'] ? array(count($documento['parent'])) : null,
                                'nodes' => array_map(function ($documento) use($link) {
                                            $nomeDocumento = sprintf('%s (%s)', utf8_encode($documento['tipo']), $documento['documento']);
                                            return array(
                                                'text' => $documento['link'] ? sprintf($link, $documento['link'], $nomeDocumento) : $nomeDocumento,
                                            );
                                        }, $documento['parent']),
                            );
                        }, $processo->Documentos),
            );
        }
        return json_encode($processos);
    }

}
