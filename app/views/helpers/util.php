<?php

require_once 'RestPkiLegacy.php';

use Lacuna\RestPkiClient;


class UtilHelper extends AppHelper
{
    function getRestPkiClient() {

        // -----------------------------------------------------------------------------------------------------------
        // PASTE YOUR ACCESS TOKEN BELOW
        $restPkiAccessToken = 't3uLcAPCRYVamDTGhHwZKkeI62Bcn0rhmCwrIix6ZgV5DWVk79aJpdPW1B-WkVi5yoF4V7fWkyZWWhcMRDUdJRNgaM77-_lMbR5pzsDtGtjVJondcVksj5c2Q2bCQZPF6r2rzHeSePIcVFuhX7CCdKaJYOiaSsAXB_8v1o2drvTy9y7gj4ImDmDiel1_4AdzNrbctir1p3R-auWM2WM0hUPNBG_NXvQ4I4fyiH_yKG69JBzK63P-xl5-zlORzRtYb74_J_ZAbMVaVjtuCM5182t2HX4yEoTcQKFqL3RI3sCAIQNyn8NXqFKbkWb9_biskzG8UqZNjHv-pSAczRX2Xqg0QNf8tTeKUxv8kxZ3JZ1jxv954PEbOPBgqPpOoWn67OsZd2NQ_eSeBWIq1OyfieZVOy7PIjN-RjUymqkXbHw9bMgV46e99isaKr99fINNLzv3K8j0hvVe0xMd3BIW64WdAH9oAFg4YFfao7Q48pdNzUNj4D9gqwk-Pl9a3_Yt-MBG7Q';
        //                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        // -----------------------------------------------------------------------------------------------------------

        // Throw exception if token is not set (this check is here just for the sake of newcomers, you can remove it)
        if (strpos($restPkiAccessToken, ' API ') !== false) {
            throw new \Exception('The API access token was not set! Hint: to run this sample you must generate an API access token on the REST PKI website and paste it on the file api/util.php');
        }

        // -----------------------------------------------------------------------------------------------------------
        // IMPORTANT NOTICE: in production code, you should use HTTPS to communicate with REST PKI, otherwise your API
        // access token, as well as the documents you sign, will be sent to REST PKI unencrypted.
        // -----------------------------------------------------------------------------------------------------------
        $restPkiUrl = 'http://pki.rest/';
        //$restPkiUrl = 'https://pki.rest/'; // <--- USE THIS IN PRODUCTION!

	    return new RestPkiClient($restPkiUrl, $restPkiAccessToken);
    }

    function setNoCacheHeaders() {
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() - 3600) . ' GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
    }

    function createAppData() {
        $appDataPath = "app-data";
        if (!file_exists($appDataPath)) {
            mkdir($appDataPath);
        }
    }

}