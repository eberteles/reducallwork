<div class="historicos form">
<?php echo $this->Form->create('Historico', array('url' => "/historicos/edit/$id/$coContrato/$modulo"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar o Histórico - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'historicos', 'action' => 'index', $coContrato, $modulo)); ?>" class="btn btn-small btn-primary" title="Listar Históricos">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
           <div class="widget-header widget-header-small"><h4>Alterar Histórico</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                    <?php
                        echo $this->Form->hidden('co_historico');
                        echo $this->Form->hidden('co_situacao');
                        if($modulo == "ata" || $modulo == "historico_ata") {
                            echo $this->Form->hidden('co_ata', array('value' => $coContrato));
                        } elseif($modulo == "evento") {
                            echo $this->Form->hidden('co_evento', array('value' => $coContrato));
                        } else {
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                        }

                        if($this->Modulo->getTipoNoHistorico()) {
                            echo $this->Form->input('tipo', array('class' => 'chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Tipo', 'options' => $tipos, 'escape' => false));
                        }
                        echo $this->Form->input('no_assunto', array('label' => 'Assunto', 'size' => '60'));
                        //echo $this->Form->input('ds_historico', array('label' => 'Descrição', 'size' => '60'));
                        echo $this->Form->hidden('dt_historico', array('value' => date('d/m/Y H:i:s') ));
                        echo $this->Form->input('ds_observacao', array('label'=>'Observação','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                            'onKeyup'=>'$(this).limit("500","#charsLeft")', 
                            'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                        echo '<br />';
                    ?>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Histórico"> Salvar</button> 
          <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>