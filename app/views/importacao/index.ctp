<?php echo $this->Form->create('Importacao', array('type' => 'file', 'url' => array('controller' => 'importacao', 'action' => 'index')));?>
<br>
<div class="row-fluid">
  <div class="span6">
<?php
    echo $this->Form->input('conteudo', array('type' => 'file', 'label' => 'Selecione um Arquivo no formato .CSV com as colunas separadas por ";"') );
?>
      <b>Passos para Importação:</b><br>
      1) <?php echo $this->Html->link('Baixe a planilha', '/modelo_importacao.xlsx', array('target' => '_blank')); ?> de exemplo e grave os dados nela utilizando o LibreOffice.<br>
      OBS.: A utilização deve aplicativo é nessária pois o Excel não dá suporte há algumas opções necessárias para exportação do CSV.<br>
      2) Após editar o arquivo acesse no LibreOffice Calc a opção: Arquivo -> Salvar como...<br>
      3) Abrirá uma tela para escolher o destino do arquivo. <br>
      &nbsp;&nbsp;&nbsp; 3.1) Na coluna "Tipo:" selecione "Texto CSV";<br>
      &nbsp;&nbsp;&nbsp; 3.2) Marque a opção "Editar as configurações do filtro";<br>
      &nbsp;&nbsp;&nbsp; 3.3) Clique em "Salvar" e se aparecer um alerta clique em "Utilizar o formato Texto CSV".<br>
      4) Na Tela "Exportar arquivo de texto" utilize as seguintes opções:<br>
      &nbsp;&nbsp;&nbsp; 4.1) Conjunto de Caracteres: Europa ocidental (ISO-8859-1)<br>
      &nbsp;&nbsp;&nbsp; 4.2) Delimitador de campo: ";"<br>
      &nbsp;&nbsp;&nbsp; 4.3) Clique em "Ok".<br>
      5) Agora, no Gescon, selecione o arquivo criado e envie via o formulário de Importação.
  </div>
</div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Enviar"> Enviar</button> 
            </div>
        </div>

<script>

    $('#ImportacaoConteudo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            icon_remove:null,
            droppable:true,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
    });
</script>