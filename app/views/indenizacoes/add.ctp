<?php 
    echo $this->Html->script( 'inicia-datetimepicker' ); 
?>
<div class="indenizacoes form">
<?php echo $this->Form->create('Indenizacao', array('url' => "/indenizacoes/add/$coFuncionario"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar uma Indenização - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'indenizacoes', 'action' => 'index', $coFuncionario)); ?>" class="btn btn-small btn-primary" title="Listar Indenizações">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
        <div class="span12 ">
           <div class="widget-header widget-header-small"><h4>Nova Indenização</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                <?php
                    echo $this->Form->hidden('co_indenizacao');
                    echo $this->Form->hidden('co_funcionario', array('value'=> $coFuncionario));
                ?>
                  <div class="row-fluid">
                      <div class="span3">
                        <?php
                                echo $this->Form->input('dt_indenizacao', array(
                                    'before' => '<div class="input-append date datetimepicker">', 
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Data', 'type'=>'text'));
                            echo $this->Form->input('ds_carro', array('label' => 'Carro'));
                        ?>
                      </div>
                      <div class="span2">
                        <?php
                            echo $this->Form->input('qt_viagem', array('label' => 'N° de Viagens', 'class' => 'input-small'));
                            echo $this->Form->input('ds_placa', array('label' => 'Placa', 'class' => 'input-small'));
                        ?>
                      </div>
                      <div class="span3">
                        <?php
                            echo $this->Form->input('nu_distancia', array('label' => 'KM da Viagem', 'class' => 'input-small'));
                            echo $this->Form->input('nu_ano_carro', array('label' => 'Ano do automóvel', 'class' => 'input-small'));
                        ?>
                      </div>
                  </div>
                  <div class="row-fluid">
                      <div class="span12">
                        <?php
                            echo $this->Form->input('ds_servico', array('label'=>'Obra/Serviço', 'class' => 'input-xxlarge', 'type'=>'textarea', 'type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("1000","#charsLeft")', 
                                'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                        ?>
                      </div>
                  </div>
	      </div>
                  
            </div>
              
        </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Indenização"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>

    $('#Voltar').click(function(){
             
        history.back();
        
    });
    
    $("#IndenizacaoQtViagem").maskMoney({precision:0, allowZero:false, thousands:''});
    $("#IndenizacaoNuDistancia").maskMoney({precision:0, allowZero:false, thousands:'.'});
    $("#IndenizacaoNuAnoCarro").maskMoney({precision:0, allowZero:false, thousands:''});
    
<?php echo $this->Html->scriptEnd() ?>