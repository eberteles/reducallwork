<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="indenizacoes index">
<!-- h2>< ?php __('Históricos');?></h2 -->

<?php
    echo $this->Form->input('anos', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label'=> __('Selecione o ano', true), 'options' => $anos, 'value'=>$ano));
?>
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbIndenizacao">
	<tr>
		<th><?php __('Mês');?></th>
		<th><?php __('Dia');?></th>
		<th><?php __('Obra / Serviço');?></th>
		<th><?php __('N° de Viagens');?></th>
		<th><?php __('KM da Viagem');?></th>
		<th class="actions"><!--?php __('Ações');?--></th>
	</tr>
	<?php
	foreach ($indenizacoes as $indenizacao):
	?>
	<tr>
		<td><?php echo $this->Print->mesExtenso[date('M', strtotime(dtDb($indenizacao['Indenizacao']['dt_indenizacao'])))]; ?>&nbsp;</td>
		<td><?php echo date('d', strtotime(dtDb($indenizacao['Indenizacao']['dt_indenizacao']))); ?>&nbsp;</td>
		<td><?php echo $indenizacao['Indenizacao']['ds_servico']; ?>&nbsp;</td>
		<td><?php echo $indenizacao['Indenizacao']['qt_viagem']; ?>&nbsp;</td>
		<td><?php echo $indenizacao['Indenizacao']['nu_distancia']; ?> Km&nbsp;</td>
		<td class="actions"><?php $id = $indenizacao['Indenizacao']['co_indenizacao']; ?> 
        
		<div class="btn-group acoes">	
                    <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coFuncionario, 'class' => 'btn', 'local_acao' => 'indenizacoes/index' ) ) ?>
                </div></td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'grupo_auxiliares/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'indenizacoes', 'action' => 'add', $coFuncionario)); ?>" class="btn btn-small btn-primary">Adicionar</a> 
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Histórico</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart(); ?>

    $('.alert-tooltip').tooltip();
    
    $("#anos").change( function() {
        $(location).attr('href', '<?php echo $this->Html->url(array ('controller' => 'indenizacoes', 'action' => 'index', $coFuncionario) )?>/' + $(this).val());
    });

<?php echo $this->Html->scriptEnd() ?>