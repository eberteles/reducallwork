<?php echo $this->Form->create('Inspecao', array('url' => array ('controller' => 'inspecao', 'action' => 'add'))); ?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Dados do Tipo da Atividade/Inspeção</h4></div>
            <div class="widget-body">
              <div class="widget-main">
              <?php
                  echo $this->Form->input('co_inspecao');
                  echo $this->Form->input('ds_inspecao', array('class' => 'input-xlarge','label' => 'Descrição do Tipo da Atividade/Inspeção','maxlength' => '50'));
              ?>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

