<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
        <div class="page-header position-relative"><h1>Lista de Inspeção</h1></div>

	    <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha um dos campos para fazer a pesquisa</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'inspecao', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Tipo de Atividade/Inspeção"><i class="icon-plus icon-white"></i> Novo Tipo de Atividade/Inspeção</a>
          </div>
        </div>
         
        <?php echo $this->Form->create('Inspecao', array('url' => '/inspecao/index'));?>
            
<div class="row-fluid">
    	<div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('ds_inspecao', array('class' => 'input-xlarge','label' => 'Descrição do Tipo da Inspeção', 'maxlength' => '50')); ?>
            </div>
    	</div>
</div>
<div class="form-actions">
    <div class="btn-group">
      <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
      <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
    </div>
</div>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>ITEM</th>
		<th>TIPO DE ATIVIDADE / INSPEÇÃO</th>
		<th class="actions">AÇÕES</th>
	</tr>
	<?php
	$i = 0;
	foreach ($inspecoes as $inspecao){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $inspecao['Inspecao']['co_inspecao']; ?></td>
        <td><?php echo $inspecao['Inspecao']['ds_inspecao']; ?></td>

		<td class="actions">
			<div class="btn-group acoes">
				<?php echo $this->element( 'actions2', array( 'id' => $inspecao['Inspecao']['co_inspecao'], 'class' => 'btn', 'local_acao' => 'adm_ctb_' ) ); ?>
			</div>
		</td>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>