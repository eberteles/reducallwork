<?php echo $this->Form->create('Instituicao');?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Instituição'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
				<?php
					echo $this->Form->input('co_instituicao_pai', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Nenhuma...', 'label' => 'Instituição Gestora', 'options' => $instituicoes));
					echo $this->Form->input('nu_instituicao', array('class' => 'input-xlarge','label' => 'Código UASG','maxLength' => '6'));
					echo $this->Form->input('ds_instituicao', array('class' => 'input-xlarge','label' => 'Descrição','maxLength' => '200'));
				?>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

