<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Instituições'); ?></h1></div>
      <?php 
//           if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_ctb_i') ) {
	  ?>        
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'instituicoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Instituição"><i class="icon-plus icon-white"></i> Nova Instituição</a> 
          </div>
        </div>
      <?php
// 		   }
	  ?>
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th style="width:90px;text-align:left;"><?php echo $this->Paginator->sort('Código', 'co_instituicao');?></th>
		<th style="width:120px;text-align:left;"><?php echo $this->Paginator->sort('Código UASG', 'nu_instituicao');?></th>
		<th style="text-align:left;"><?php echo $this->Paginator->sort('Descrição', 'ds_instituicao');?></th>
		<th style="text-align:left;"><?php echo $this->Paginator->sort('Instituição Gestora', 'co_instituicao_pai');?></th>
		<th class="actions" style="width:90px;text-align:left;"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($instituicoes as $instituicao):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $instituicao['Instituicao']['co_instituicao']; ?>&nbsp;</td>
		<td><?php echo $instituicao['Instituicao']['nu_instituicao']; ?>&nbsp;</td>
		<td><?php echo $instituicao['Instituicao']['ds_instituicao']; ?>&nbsp;</td>
		<td><?php echo $instituicao['InstituicaoPai']['ds_instituicao']; ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group acoes">	
				<?php echo $this->element( 'actions', array( 'id' => $instituicao['Instituicao']['co_instituicao'], 'class' => 'btn', 'local_acao' => 'instituicoes/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
