<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)https://sistemas.novacap.df.gov.br/sisprot/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.: Protocolo | Novacap :.</title>
    <link rel="shortcut icon" href="/img/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
        echo $this->Html->meta( 'icon' );

        echo $this->Html->css( 'bootstrap' );
        echo $this->Html->css( 'style' );
        echo $this->Html->css( 'fonts' );
        echo $this->Html->css( 'msg' );
        echo $this->Html->css( 'table' );
        echo $this->Html->css( 'estiloGeral' );
        echo $this->Html->css( 'nav-h' );
        echo $this->Html->css( 'alertas' );
        echo $this->Html->css( 'bootstrap-responsive.min' );
        echo $this->Html->css( 'font-awesome.min.css' );
        echo $this->Html->css( 'ace-fonts.css' );
        echo $this->Html->css( 'ace.min.css' );
        echo $this->Html->css( 'ace-responsive.min.css' );
        echo $this->Html->css( 'ace-skins.min.css' );
        echo $this->Html->css( 'chosen' );
        echo $this->Html->css( 'jquery.treegrid' );
        echo $this->Html->css( 'jquery-silk-icons' );
        echo $this->Html->css( 'bootstrap-datetimepicker.min' );

        echo $this->Html->script( 'jquery-1.4.2.min' );
        echo $this->Html->script( 'bootstrap-datetimepicker.min' );

    ?>

</head>

<body style="z-index:0;">

<div class="header" style="margin-top: 0px;">
    <div class="headerLogoNovacap" style="position: relative; bottom: -20px;">
        <span class="siglaSis"><?php echo $html->image('logo_sistema_branco.png'); ?></span><br>
        <span class="descricaoSis">CONTRATOS</span><br>
    </div>
    <div class="headerLogoNovacap2">
    </div>
</div>

<div class="wrapper" style="margin-top: -17px;">
    <div class="barraMenu">
        <p style="position: relative;bottom: -10px;">Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    </div>

    <br />

    <?php echo $this->Form->create('PreImprimir', array('url' => array('controller' => 'contratos_relatorio', 'action' => 'imprimirRelatorioGeralHtml'))); ?>
    
    <fieldset style="border-style: radius; border-radius: 5px; border: 1px solid #F0F0F0; width: 100%; height: 30px;">
        <p style="margin-top: 0.5%;">Selecione as datas abaixo referente aos períodos dos Contratos ou deixe em branco para listar todos os Contratos.</p>
    </fieldset>

    <br />

    <div class="tabs-container">
        <input type="radio" name="tabs" class="tabs" id="tab1" checked>
        <label for="tab1">Geral Ou Por Número de Contrato</label>
        <div>
            <br />

            <?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

            <div class="row-fluid">
                <div class="span3">
                    <label for="dt_inicio" style="position: relative; bottom: -18px;" >Selecione o Início dos Contratos</label>
                    <?php
                    echo $this->Form->input('dt_inicio', array(
                            'before' => '<div class="input-append date datetimepicker">',
                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                            'class' => 'input-small','label' => 'Início (Período)', 'type'=>'text', 'style' => 'width: 80%;')
                    );
                    ?>
                </div>

                <div class="span3">
                    <label style="position: relative; bottom: -18px;" >Selecione o Fim dos Contratos</label>
                    <?php
                    echo $this->Form->input('dt_final', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                        'class' => 'input-small','label' => 'Fim (Período)', 'type'=>'text', 'style' => 'width: 80%;'));
                    ?>
                </div>

                <span class="span3" style="position: relative; bottom: -18px;">
                    <?php
                        echo $this->Form->input('modalidade', array('type' => 'select', 'empty' => 'Selecione...', 'options' => $situacoes));
                    ?>
                </span>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="btn-group">
                        <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Gerar </button>
                        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="footer"><br>
    <center>Copyright © Novacap 2015 - Todos direitos reservados</center>
</div>

<script type="text/javascript">
</script>

</body>
</html>