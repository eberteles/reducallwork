<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)https://sistemas.novacap.df.gov.br/sisprot/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.: Protocolo | Novacap :.</title>
    <link rel="shortcut icon" href="/img/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
    echo $this->Html->meta( 'icon' );

    echo $this->Html->css( 'bootstrap' );
    echo $this->Html->css( 'style' );
    echo $this->Html->css( 'fonts' );
    echo $this->Html->css( 'msg' );
    echo $this->Html->css( 'table' );
    echo $this->Html->css( 'estiloGeral' );
    echo $this->Html->css( 'nav-h' );
    echo $this->Html->css( 'alertas' );
    echo $this->Html->css( 'bootstrap-responsive.min' );
    echo $this->Html->css( 'font-awesome.min.css' );
    echo $this->Html->css( 'ace-fonts.css' );
    echo $this->Html->css( 'ace.min.css' );
    echo $this->Html->css( 'ace-responsive.min.css' );
    echo $this->Html->css( 'ace-skins.min.css' );
    echo $this->Html->css( 'chosen' );
    echo $this->Html->css( 'jquery.treegrid' );
    echo $this->Html->css( 'jquery-silk-icons' );
    echo $this->Html->css( 'bootstrap-datetimepicker.min' );

    echo $this->Html->script( 'jquery-1.4.2.min' );
    echo $this->Html->script( 'bootstrap-datetimepicker.min' );

    ?>

</head>

<body style="z-index:0; width: 90%; margin: 0 auto; margin-top: 2%;">

    <div class="row-fluid">
        <div class="span12">
<!--            --><?php //if( isset( $dt_inicio ) && isset( $dt_final ) ){ ?>
<!--            <fieldset style="border: 2px solid #F0F0F0;height: 50px;border-style: radius; border-radius: 5px;" >-->
<!--                <a href="--><?php //echo $this->Html->url(array ('controller' => 'contratos_relatorio', 'action' => 'imprimirRelatorioGeral' . '/' . dtDb($dt_inicio) . '/' . dtDb($dt_final) ) )?><!--"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>-->
<!--            </fieldset>-->
<!--            --><?php //}elseif( isset( $contratos ) ){ ?>
<!--                <fieldset style="border: 2px solid #F0F0F0;height: 50px;border-style: radius; border-radius: 5px;" >-->
<!--                    <a href="--><?php //echo $this->Html->url(array ('controller' => 'contratos_relatorio', 'action' => 'imprimirRelatorioGeral') )?><!--"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>-->
<!--                </fieldset>-->
<!--            --><?php //} ?>
            <fieldset style="border: 2px solid #F0F0F0;height: 50px;border-style: radius; border-radius: 5px;" >
                <a class="btn btn-primary" id="btnPdf" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</a>
            </fieldset>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span2">
            <?php echo $html->image('logo_novacap.png'); ?>
        </div>

        <div class="span8" style='font-size: 30px; font-family: Arial; text-align: center;'>
            TABELA DE CONTRATOS
            <br />
            <p style='font-size: 12px;'>(Lei de Acesso à Informação - LAI, Lei Federal nº 12.527, de 18 de novembro de 2011)</p>
        </div>

        <div class="span2">
            <?php echo $html->image('logo_gdf.png'); ?>
        </div>
    </div>

    <div id="contratosReport">
        <div class="row-fluid" >
            <div class="span12">
                <table style="max-width: 100%; font-size: 10px;" class="table table-bordered table-striped">
                    <thead>
                        <th><b>NÚMERO DO CONTRATO</b></th>
                        <th><b>PROCESSO</b></th>
                        <th><b>EMPRESA</b></th>
                        <th><b>CNPJ</b></th>
                        <th><b>MODALIDADE DE LICITAÇÃO</b></th>
                        <th><b>OBJETO</b></th>
                        <th><b>FUNDAMENTO LEGAL</b></th>
                        <th><b>VIGÊNCIA</b></th>
                        <th><b>DATA DE PUBLICAÇÃO</b></th>
                        <th><b>VALOR</b></th>
                        <th><b>SITUAÇÃO</b></th>
                    </thead>
                    <tbody>
                    <?php
                        foreach($contratos as $cont){
                            foreach( $fornecedores as $fornecedor ){
                                if( $fornecedor['Fornecedor']['co_fornecedor'] == $cont['ContratosRelatorio']['co_fornecedor'] ){
                                    $nome_fornecedor = $fornecedor['Fornecedor']['no_razao_social'];
                                    $cnpj_fornecedor = $fornecedor['Fornecedor']['nu_cnpj'];
                                }
                            }
                            foreach( $situacoes as $situacao ){
                                if( $situacao['Situacao']['co_situacao'] == $cont['ContratosRelatorio']['co_situacao'] ){
                                    $nome_situacao = $situacao['Situacao']['ds_situacao'];
                                }
                            }
                            foreach( $contratacoes as $contratacao ){
                                if( $contratacao['Contratacao']['co_contratacao'] == $cont['ContratosRelatorio']['co_contratacao'] ){
                                    $modalidade_contratacao = $contratacao['Contratacao']['ds_contratacao'];
                                }
                            }

                            $data1 = strtotime(dtDb($cont['ContratosRelatorio']['dt_ini_vigencia']));
                            $data2 = strtotime(dtDb($cont['ContratosRelatorio']['dt_fim_vigencia']));
                            $vigencia = (int)round(($data2 - $data1)/86400);
                    ?>
                            <tr>
                                <td> <?php echo FunctionsComponent::mascara($cont['ContratosRelatorio']['nu_contrato'],'contrato');                         ?></td>
                                <td> <?php echo FunctionsComponent::mascara($cont['ContratosRelatorio']['nu_processo'],'processo');                         ?></td>
                                <td> <?php if( isset( $nome_fornecedor ) ){ echo $nome_fornecedor; }else{ echo '---'; }                                     ?></td>
                                <td> <?php if( isset( $cnpj_fornecedor ) ){ echo FunctionsComponent::mascara($cnpj_fornecedor,'cnpj'); }else{ echo '---'; } ?></td>
                                <td> <?php if( isset( $modalidade_contratacao ) ){ echo $modalidade_contratacao; }else{ echo '---'; }                       ?></td>
                                <td> <?php echo $cont['ContratosRelatorio']['ds_objeto'];                                                                   ?></td>
                                <td> <?php echo $cont['ContratosRelatorio']['ds_fundamento_legal'];                                                         ?></td>
                                <td> <?php echo $vigencia . " dias corridos";                                                                               ?></td>
                                <td> <?php echo $cont['ContratosRelatorio']['dt_publicacao'];                                                               ?></td>
                                <td> <?php echo vlReal($cont['ContratosRelatorio']['vl_global']);                                                           ?></td>
                                <td> <?php if( isset( $nome_situacao )){ echo $nome_situacao; }else{ echo '---'; }                                          ?></td>
                            </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span9">

            </div>
            <div class="span3">
                <?php echo $html->image('logo_sistema.png'); ?>
            </div>
        </div>
    </div>

<script type="text/javascript">

    $("#btnPdf").click(function () {
        $('<form action="<?php echo $this->base; ?>/contratos_relatorio/printPdf" method="POST">' +
            '<textarea name="html">' + $('#contratosReport').html() + '</textarea>' +
//        '<input type="hidden" name="html" value="' + $('#contratosReport').html() + '">' +
        '</form>').submit();
    });

//        function generatePdf()
//        {
//            $.form('<?php //echo $this->base; ?>///contratos_relatorio/printPdf', { html: $('#contratosReport').html() }, 'POST').submit();
//        window.location.href = "<?php //echo $this->base; ?>///contratos_relatorio/printPdf/" + $('#contratosReport').html();
//        $.ajax({
//            url: '<?php //echo $this->base; ?>///contratos_relatorio/printPdf',
//            data: { html: $('#contratosReport').html() },
//            type: 'POST',
//            success: function(result) {
//            }
//        });
//        }
</script>

</body>
</html>