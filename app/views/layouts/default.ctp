<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        ReducallWork
    </title>
    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('bootstrap');

    echo $this->Html->css('bootstrap-responsive.min');

    echo $this->Html->css('font-awesome.min.css');
    ?>
    <!--[if IE 7]>
    <?php echo $this->Html->css( 'font-awesome-ie7.min.css' ); ?>
    <![endif]-->
    <!--[if lte IE 8]>
    <?php echo $this->Html->css( 'ace-ie.min.css' ); ?>
    <![endif]-->

    <?php
    echo $this->Html->css('ace-fonts.css');
    echo $this->Html->css('ace.min.css');
    echo $this->Html->css('ace-responsive.min.css');
    echo $this->Html->css('ace-skins.min.css');

    echo $this->Html->css('sis');

    echo $this->Html->css('jquery-ui-1.10.3.full.min');
    //echo $this->Html->css( 'jquery.ui.datepicker' ); // acrescentei

    echo $this->Html->css('chosen');

    echo $this->Html->css('jquery.treegrid'); // acrescentei

    echo $this->Html->css('jquery-silk-icons'); // acrescentei

    echo $this->Html->css('bootstrap-datetimepicker.min');
    echo $this->Html->css('dataTables.bootstrap.min.css');

    echo $this->Html->script('ace-extra.min');

    echo $this->Html->script('jquery-2.0.3.min');
    echo $this->Html->script('jquery.easy-pie-chart.min');

    echo $this->Html->script('jquery.maskMoney'); // acrescentei

    echo $this->Html->script('bootstrap');
    echo $this->Html->script('bootstrap-datetimepicker.min');

    echo $this->Html->script('ace-elements.min');

    echo $this->Html->script('ace.min.js');

    echo $this->Html->script('jquery.maskedinput.min');

    echo $this->Html->script('jquery-ui-1.10.3.full.min');

    echo $this->Html->script('chosen.jquery.min');


    echo $this->Html->script('app'); // acrescentei
    echo $this->Html->script('aba'); //acrescentei
    echo $this->Html->script('jsapi');

    echo $this->Html->script('jquery.treegrid');
    echo $this->Html->script('jquery.treegrid.bootstrap2');
    echo $this->Html->script('jquery.cookie');

    echo $this->Html->script('datatables.min.js');
    echo $this->Html->script('jquery.dataTables.min.js');
    echo $this->Html->script('dataTables.bootstrap.min.js');

    echo $this->Html->script('jquery.limit-1.2'); // acrescentado para limitar a quantidade de caracteres no textarea
    echo $this->Html->script('moment/moment.js');
    $usuario = $this->Session->read('usuario');

    if (isset($usuario['palavra_chave']) && $this->params['url']['url'] == 'contratos' || $this->params['named']['page'] != null) {
        $palavra_chave = $usuario['palavra_chave'];
    }

    ?>
    <?php if (Configure::read('App.config.component.barraGoverno.enabled')) {
        echo '<meta property="creator.productor" content="http://estruturaorganizacional.dados.gov.br/id/unidade-organizacional/'
            . Configure::read('App.config.component.barraGoverno.params.unidadeOrganizacional')
            . '">';
    } ?>
</head>

<?php if (Configure::read('App.config.component.barraGoverno.enabled')): ?>
<body>
<div class="navbar " id="navbar">
    <div id="barra-brasil" style="background:#7F7F7F; height: 20px; padding:0 0 0 10px;display:block;">
        <ul id="menu-barra-temp" style="list-style:none;">
            <li style="display:inline; float:left;padding-right:10px; margin-right:10px; border-right:1px solid #EDEDED">
                <a href="http://brasil.gov.br" style="font-family:sans,sans-serif; text-decoration:none; color:white;">Portal
                    do Governo Brasileiro</a></li>
            <li><a style="font-family:sans,sans-serif; text-decoration:none; color:white;"
                   href="http://epwg.governoeletronico.gov.br/barra/atualize.html">Atualize sua Barra de Governo</a>
            </li>
        </ul>
    </div>
    <?php else: ?>

    <body class="navbar-fixed">
    <div class="navbar navbar-fixed-top" id="navbar">

        <?php endif; ?>

        <script type="text/javascript">
            try {
                ace.settings.check('navbar', 'fixed');
            } catch (e) {
            }
        </script>

        <div class="navbar-inner">
            <div class="container-fluid">
                <div style="float: left;">
                    <a title="ReducallWork"
                       href="<?php echo Router::url('/', true); ?>"><img
                                src="<?php echo $this->base; ?>/img/logo_sistema_branco.png" width="190"
                                height="40"/></a>
                </div>

                <div class="navbar navbar-inverse">
                    <div class="navbar-inner" style="float: left;">
                        <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                        <button type="button" class="btn btn-small btn-navbar" data-toggle="collapse"
                                data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                        <div class="nav-collapse collapse">
                            <ul class="nav">
                                <?php
                                echo $menu->menu($menus, array('class' => 'submenu'));
                                ?>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!-- /.navbar-inner -->
                    <div style="float: right;">
                        <ul class="nav ace-nav pull-right">
                            <li class="green" id="list_emails"></li>
                            <li class="light-blue">
                                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
									<span class="user-info">
									<small><?php echo $this->Print->saudacao(); ?>,</small>
                                        <?php echo $usuario['Usuario']['ds_nome']; ?>
									</span>
                                    <i class="icon-caret-down"></i>
                                </a>
                                <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                                    <li>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'edit', $usuario['Usuario']['co_usuario'])); ?>">
                                            <i class="icon-user"></i>Perfil</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'sair')); ?>">
                                            <i class="icon-off"></i>Sair</a>
                                    </li>
                                </ul>
                            </li>
                        </ul><!-- /.ace-nav -->
                    </div>
                </div><!-- /.navbar -->
            </div><!-- /.container-fluid -->
        </div><!-- /.navbar-inner -->
    </div>

    <div id="view_email2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="tituloAbaEmail"></h3>
        </div>
        <div class="modal-body maior" id="mensagemAbaEmail">
        </div>
    </div>

    <div id="view_pesquisa" class="modal hide fade maior tela" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Pesquisa Avançada</h3>
        </div>
        <div class="modal-body tela">
            <form method="post"
                  action="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'pesquisa_avancada')); ?>"
                  accept-charset="utf-8" name="pesqAvan" id="pesqAvan">
                <input type="hidden" id="base_url_sistema" value="<?php echo $this->base; ?>">
                <?php echo $this->Form->hidden('Filtro.ic_tipo_contrato', array('value' => $this->Print->getIcTipoContrato($usuario))); ?>
                <div class="widget-main">
                    <div class="row-fluid">
                        <div id="camposPesquisa"></div>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal-footer">
            <a class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
            <button onclick="return false;" class="btn btn-primary btn-small" id="btnPesqAvan">
                Pesquisar
                <i class="icon-search icon-on-right bigger-110"></i>
            </button>

        </div>
    </div>

    <div class="main-container container-fluid">
        <div class="main-content">
            <?php if (Configure::read('App.config.component.barraGoverno.enabled')): ?>
            <div class="breadcrumbs" id="breadcrumbs">
                <?php else: ?>
                <div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
                    <?php endif; ?>

                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {
                        }
                    </script>

                    <div class="nav-search" id="nav-search">
                        <form class="form-search" method="post"
                              action="<?php echo $this->Html->url(array('controller' => 'contratos', 'action' => 'index')); ?>"
                              accept-charset="utf-8" id="searchContrato">
                            <?php echo $this->Form->hidden('Contrato.ic_tipo_contrato', array('value' => $this->Print->getIcTipoContrato($usuario))); ?>
                            <span class="input-icon input-icon-right">
                                        <i style="cursor:pointer" class="icon-search nav-search-icon"
                                           id="searchIconContrato"></i>
                                        <input class="input-xlarge nav-search-input alert-tooltip"
                                               title="Esta consulta poderá ser feita através dos seguintes campos: 1) Número do <?php __('Processo'); ?>, 2) Número do <?php __('Contrato'); ?>, 3) Objeto, 4) Observação, 5) Situação, 6) Tipo de <?php __('Contrato'); ?>, 7) <?php __('Fornecedor'); ?>."
                                               name="data[Contrato][palavra_chave]" placeholder="Digite aqui para Pesquisar" type="text"
                                               value="<?php echo $palavra_chave; ?>" id="palavra-chave"/>
                                    </span>
                            <i style="cursor:pointer" id="searchAvan"
                               class="icon-caret-down bigger-125 blue alert-tooltip"
                               title="Mostrar opções de pesquisa avançada"></i>
                        </form>
                    </div><!-- #nav-search -->
                    <div style="float: right;">

                    </div>

                    <div style="float: right;" id="sessao"></div>

                </div>

                <div class="page-content">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php
                            if ($this->Session->check("Message.flash")) {
                                ?>
                                <div class="alert alert-block">
                                    <?php echo $this->Session->flash(); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <?php echo $content_for_layout; ?>
                            <?php echo $this->element('bootjs'); ?>
                        </div><!-- /.span -->
                    </div><!-- /.row-fluid -->
                </div><!-- /.page-content -->

            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <script type="text/javascript">
            $(document).ready(function () {
                // Chama a função ao carregar a tela
                startCountdown();
                atualizarMenuEmail();
            });

            function atualizarMenuEmail() {
                $.ajax({
                    url: '<?php echo $this->Html->url(array('controller' => 'emails', 'action' => 'list_menu'))?>',
                    type: 'GET',
                    success: function (resp) {
                        $("#list_emails").html(resp);
                    },
                    error: function (error) {
                        console.error('Error: ', error);
                    }
                });
            }

            function abrirMensagem(idMensagem, titulo) {
                $('#view_email2').modal();
                $('#mensagemAbaEmail').html("");
                $('#tituloAbaEmail').html(titulo);

                var url_abrir = "<?php echo $this->base; ?>/emails/abrir/" + idMensagem;
                $.ajax({
                    type: "GET",
                    url: url_abrir,
                    success: function (result) {
                        $('#mensagemAbaEmail').append(result);
                        atualizarMenuEmail();
                    }
                });
            }

    var inicioSessao = new Date();
    //var inicioSessao    = data.getHours() + ':' + data.getMinutes();
    var tempo = new Number();
    // Tempo em segundos
    <?php if ($usuario['Usuario']['licenca_concorrente'] == 1): ?>
    tempo = <?php echo(Configure::read('App.config.resource.licenca.timeout.concorrente') * 60) ?>
    <?php else: ?>
        tempo = <?php echo(Configure::read('App.config.resource.licenca.timeout.nominal') * 60) ?>;
    <?php endif; ?>

            tempo2 = tempo;

            function startCountdown() {
                var agora = new Date();
                var diferenca = (agora - inicioSessao) / 1000;

                if (Math.floor(diferenca) === tempo2) {
                    fimDaSessao();
                }

                // Se o tempo não for zerado
                if ((tempo - 1) >= 0) {

                    // Pega a parte inteira dos minutos
                    var min = parseInt(tempo / 60);

                    // horas, pega a parte inteira dos minutos
                    var hor = parseInt(min / 60);

                    //atualiza a variável minutos obtendo o tempo restante dos minutos
                    min = min % 60;

                    // Calcula os segundos restantes
                    var seg = tempo % 60;

                    // Formata o número menor que dez, ex: 08, 07, ...
                    if (min < 10) {
                        min = "0" + min;
                        min = min.substr(0, 2);
                    }

                    if (seg <= 9) {
                        seg = "0" + seg;
                    }

                    if (hor <= 9) {
                        hor = "0" + hor;
                    }

                    // Cria a variável para formatar no estilo hora/cronômetro
                    horaImprimivel = hor + ':' + min + ':' + seg;
                    //JQuery pra setar o valor
                    $("#sessao").html('Sua sessão expira em: <b>' + horaImprimivel + '</b>&nbsp;&nbsp;&nbsp;');

                    // Define que a função será executada novamente em 1000ms = 1 segundo
                    setTimeout('startCountdown()', 1000);

                    // diminui o tempo
                    tempo--;

                    // Quando o contador chegar a zero faz esta ação
                } else {
                    fimDaSessao();
                }

            }

            function fimDaSessao() {
                window.open('<?php echo $this->base; ?>/usuarios/sair', '_self');
            }


        </script>
        <footer>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
            <div class="sistema">&copy; <?php echo date('Y') . ' ' . __('ReducallWork', true); ?> -
                Versão: <?php echo Configure::read('App.config.version'); ?></div>
            <div class="sistema">Página processada em: $$$BENCHMARK$$$ segundos</div>
        </footer>
        <?php
        echo $this->Session->flash('email');
        echo $this->element('sql_dump');
        ?>
        <?php if (Configure::read('App.config.component.barraGoverno.enabled')): ?>
            <script defer="defer" src="//barra.brasil.gov.br/barra.js" type="text/javascript"></script>
        <?php endif; ?>


    </body>
</html>
