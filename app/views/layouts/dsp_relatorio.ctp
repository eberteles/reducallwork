<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)https://sistemas.novacap.df.gov.br/sisprot/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.: Protocolo | Novacap :.</title>
    <link rel="shortcut icon" href="/img/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
        echo $this->Html->meta( 'icon' );

        echo $this->Html->css( 'bootstrap' );
        echo $this->Html->css( 'style' );
        echo $this->Html->css( 'fonts' );
        echo $this->Html->css( 'msg' );
        echo $this->Html->css( 'table' );
        echo $this->Html->css( 'estiloGeral' );
        echo $this->Html->css( 'nav-h' );
        echo $this->Html->css( 'alertas' );
        echo $this->Html->css( 'bootstrap-responsive.min' );
        echo $this->Html->css( 'font-awesome.min.css' );
        echo $this->Html->css( 'ace-fonts.css' );
        echo $this->Html->css( 'ace.min.css' );
        echo $this->Html->css( 'ace-responsive.min.css' );
        echo $this->Html->css( 'ace-skins.min.css' );
        echo $this->Html->css( 'chosen' );
        echo $this->Html->css( 'jquery.treegrid' );
        echo $this->Html->css( 'jquery-silk-icons' );
        echo $this->Html->css( 'bootstrap-datetimepicker.min' );

        echo $this->Html->script( 'jquery-1.4.2.min' );
        echo $this->Html->script( 'bootstrap-datetimepicker.min' );

    ?>

</head>

<body style="z-index:0;">

<div class="header" style="margin-top: 0px;">
    <div class="headerLogoNovacap" style="position: relative; bottom: -20px;">
        <span class="siglaSis"><?php echo $html->image('logo_sistema_branco.png'); ?></span><br>
        <span class="descricaoSis">DIÁRIAS E PASSAGENS</span><br>
    </div>
    <div class="headerLogoNovacap2">
    </div>
</div>

<div class="wrapper" style="margin-top: -17px;">
    <div class="barraMenu">
        <p style="position: relative;bottom: -10px;">Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    </div>

    <br />

    <?php echo $this->Form->create('PreImprimir', array('url' => "/dsp_relatorio/imprimirRelatorioGeralHtml/")); ?>
    
    <fieldset style="border-style: radius; border-radius: 5px; border: 1px solid #F0F0F0; width: 100%; height: 30px;">
        <p style="margin-top: 0.5%;">Selecione o tipo de filtro para gerar o Relatório de Despesas.</p>
    </fieldset>

    <br />

    <div class="tabs-container">
        <input type="radio" name="tabs" class="tabs" id="tab1" checked>
        <label for="tab1">Por Ano</label>
        <div>
            <br />

            <div class="row-fluid">
                <div class="span3">
                    <label for="dt_inicio" style="position: relative; bottom: -18px;" ></label>
                    <?php
                        echo $this->Form->input('ano', array(
                                'class' => 'input-xlarge', 'onchange' => 'carregaMeses()', 'empty' => 'Selecione...', 'id' => 'ano', 'type'=>'select', 'options' => $anos, 'label' => 'Selecione o ano das Despesas', 'style' => 'width: 80%;')
                        );
                    ?>
                    <div class="meses">
                        <ul>

                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <input type="radio" name="tabs" class="tabs" id="tab3">
        <label for="tab3">Por Funcionário</label>
        <div>
            <br />

            <?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

            <div class="row-fluid">
                <div class="span3">
                    <?php
                        echo $this->Form->input('nome_funcionario', array(
                            'class' => 'input-xlarge', 'empty' => 'SELECIONE...', 'id' => 'funcionario', 'type'=>'select', 'options' => $funcionarios, 'label' => 'Selecione o nome do funcionário')
                        );
                    ?>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="btn-group">
                        <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Gerar </button>
                        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
                    </div>
                </div>
            </div>
        </div>

        <input type="radio" name="tabs" class="tabs" id="tab2">
        <label for="tab2">Por Período</label>
        <div>
            <br />

            <?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

            <div class="row-fluid">
                <div class="span3">
                    <label for="dt_inicio" style="position: relative; bottom: -18px;" >Selecione data Início das Despesas</label>
                    <?php
                    echo $this->Form->input('dt_inicio', array(
                            'before' => '<div class="input-append date datetimepicker">',
                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                            'class' => 'input-small','label' => 'Início (Período)', 'type'=>'text', 'style' => 'width: 80%;')
                    );
                    ?>
                </div>

                <div class="span3">
                    <label style="position: relative; bottom: -18px;" >Selecione data Fim das Despesas</label>
                    <?php
                    echo $this->Form->input('dt_final', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                        'class' => 'input-small','label' => 'Fim (Período)', 'type'=>'text', 'style' => 'width: 80%;'));
                    ?>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="btn-group">
                        <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Gerar </button>
                        <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="footer"><br>
    <center>Copyright © Novacap 2015 - Todos direitos reservados</center>
</div>

<script type="text/javascript">
    $( document).ready(function(){
        $('.meses').hide();
    });

    function carregaMeses(){
        var ano = $("#ano").val();
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'dsp_relatorio', 'action' => 'listar') )?>/" + ano, null, function(data){
            var options = "<ul>";
            $.each(data, function(index, val) {
                console.log(data.index);
                options += "<li><a target='_blank' href='<?php echo $this->Html->url(array ('controller' => 'dsp_relatorio', 'action' => 'imprimirRelatorioGeralHtml') )?>/"+ index +"'>" + val + "</a></li>";
            });
            options += "</ul>";
            $(".meses").html(options);
        });
        $(".meses").show();
    }
</script>

</body>
</html>