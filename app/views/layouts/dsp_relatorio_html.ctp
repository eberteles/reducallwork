<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)https://sistemas.novacap.df.gov.br/sisprot/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.: Protocolo | Novacap :.</title>
    <link rel="shortcut icon" href="/img/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
    echo $this->Html->meta( 'icon' );

    echo $this->Html->css( 'bootstrap' );
    echo $this->Html->css( 'style' );
    echo $this->Html->css( 'fonts' );
    echo $this->Html->css( 'msg' );
    echo $this->Html->css( 'table' );
    echo $this->Html->css( 'estiloGeral' );
    echo $this->Html->css( 'nav-h' );
    echo $this->Html->css( 'alertas' );
    echo $this->Html->css( 'bootstrap-responsive.min' );
    echo $this->Html->css( 'font-awesome.min.css' );
    echo $this->Html->css( 'ace-fonts.css' );
    echo $this->Html->css( 'ace.min.css' );
    echo $this->Html->css( 'ace-responsive.min.css' );
    echo $this->Html->css( 'ace-skins.min.css' );
    echo $this->Html->css( 'chosen' );
    echo $this->Html->css( 'jquery.treegrid' );
    echo $this->Html->css( 'jquery-silk-icons' );
    echo $this->Html->css( 'bootstrap-datetimepicker.min' );

    echo $this->Html->script( 'jquery-1.4.2.min' );
    echo $this->Html->script( 'bootstrap-datetimepicker.min' );

    ?>

</head>

<body style="z-index:0; width: 90%; margin: 0 auto; margin-top: 2%;">

    <?php
        //debug($this->data);die;
        $str = implode("",$this->params['url']);
        $url = explode("/",$str);
        $month = end($url);
        $mes = null;
        if( $this->data["PreImprimir"]["dt_inicio"] != null && $this->data["PreImprimir"]["dt_final"] != null ) {
            $dt_inicio = implode("-", array_reverse(explode("-", str_replace("/", "-", $this->data["PreImprimir"]["dt_inicio"]))));
            $dt_final = implode("-", array_reverse(explode("-", str_replace("/", "-", $this->data["PreImprimir"]["dt_final"]))));
            $mes = 'geral';
        }
    ?>

    <div class="row-fluid">
        <div class="span12">
            <fieldset style="border: 2px solid #F0F0F0;height: 50px;border-style: radius; border-radius: 5px;" >
                <?php
                    if( $month != null ){
                ?>
                        <a href="<?php echo $this->Html->url(array ('controller' => 'dsp_relatorio', 'action' => 'imprimirRelatorioGeral' . '/' . $month) )?>"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>
                <?php }elseif( $mes != null ){ ?>
                        <a href="<?php echo $this->Html->url(array ('controller' => 'dsp_relatorio', 'action' => 'imprimirRelatorioGeral' . '/' . $mes . '/' . $dt_inicio . '/' . $dt_final) )?>"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>
                <?php }else{ ?>
                        <a href="<?php echo $this->Html->url(array ('controller' => 'dsp_relatorio', 'action' => 'imprimirRelatorioGeralPorFuncionario' . '/' . $this->data["PreImprimir"]["nome_funcionario"]) )?>"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>
                <?php } ?>
            </fieldset>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span2">
            <?php echo $html->image('logo_novacap.png'); ?>
        </div>

        <div class="span8" style='font-size: 30px; font-family: Arial; text-align: center;'>
            TABELA DE DESPESAS COM DIÁRIAS E PASSAGENS
            <br />
            <p style='font-size: 12px;'>(Lei de Acesso à Informação - LAI, Lei Federal nº 12.527, de 18 de novembro de 2011)</p>
        </div>

        <div class="span2">
            <?php echo $html->image('logo_gdf.png'); ?>
        </div>
    </div>

    <div class="row-fluid" >
        <div class="span12">
            <table style="max-width: 100%; font-size: 10px;" class="table table-bordered table-striped">
                <thead>
                    <th><b>UNIDADE DE LOTAÇÃO</b></th>
                    <th><b>NOME / MATRÍCULA</b></th>
                    <th><b>CARGO / FUNÇÃO</b></th>
                    <th><b>ORIGEM</b></th>
                    <th><b>DESTINO</b></th>
                    <th><b>INÍCIO PERÍODO</b></th>
                    <th><b>FIM PERÍODO</b></th>
                    <th><b>MOTIVO</b></th>
                    <th><b>MEIO DE TRANSPORTE</b></th>
                    <th><b>CAT. DA PASSAGEM</b></th>
                    <th><b>VALOR DA PASSAGEM</b></th>
                    <th><b>Nº DE DIÁRIAS</b></th>
                    <th><b>VALOR TOTAL DAS DIÁRIAS</b></th>
                    <th><b>VALOR TOTAL DA VIAGEM</b></th>
                    <th><b>PROCESSO</b></th>
                </thead>
                <tbody>
                <?php
                    foreach($despesas as $despesa){
                        for( $i = 0; $i < count($unidade); $i++ ){
                            if( $unidade[$i]['DspUnidadeLotacao']['co_unidade'] == $despesa['DspRelatorio']['cod_unidade_lotacao'] ){
                                $uni = $unidade[$i]['DspUnidadeLotacao']['nome_unidade'];
                            }
                        }
                        for( $i = 0; $i < count($funcionarios); $i++ ){
                            if( $funcionarios[$i]['GrupoAuxiliar']['co_usuario'] == $despesa['DspRelatorio']['co_funcionario'] ){
                                $nome_funcionario = $funcionarios[$i]['GrupoAuxiliar']['ds_nome'];
                                $matricula = $funcionarios[$i]['GrupoAuxiliar']['nu_cpf'];
                            }
                        }
                        for( $i = 0; $i < count($cargo); $i++ ){
                            if( $cargo[$i]['DspCargos']['co_cargo'] == $despesa['DspRelatorio']['cod_cargo'] ){
                                $crg = $cargo[$i]['DspCargos']['nome_cargo'];
                            }
                        }
                        for( $i = 0; $i < count($funcao); $i++ ){
                            if( $funcao[$i]['DspFuncao']['co_funcao'] == $despesa['DspRelatorio']['cod_funcao'] ){
                                $fnc = $funcao[$i]['DspFuncao']['nome_funcao'];
                            }
                        }
                        for( $i = 0; $i < count($meio_transporte); $i++ ){
                            if( $meio_transporte[$i]['DspMeioTransporte']['co_meio_transporte'] == $despesa['DspRelatorio']['cod_meio_transporte'] ){
                                $mtp = $meio_transporte[$i]['DspMeioTransporte']['nome_meio_transporte'];
                            }
                        }
                        for( $i = 0; $i < count($categoria); $i++ ){
                            if( $categoria[$i]['DspCategoriaPassagem']['co_categoria_passagem'] == $despesa['DspRelatorio']['cod_categoria_passagem'] ){
                                $ctg = $categoria[$i]['DspCategoriaPassagem']['nome_categoria_passagem'];
                            }
                        }
                        $valor_passagem = number_format($despesa['DspRelatorio']['valor_passagem'], 2, ',', '.');
                        $valor_total_diarias = number_format($despesa['DspRelatorio']['valor_total_diarias'], 2, ',', '.');
                        $valor_total_viagem = number_format($despesa['DspRelatorio']['valor_total_viagem'], 2, ',', '.');
                        $numero_diarias = number_format($despesa['DspRelatorio']['numero_diarias'], 1, ',', '');

                        $origem  = $despesa['DspRelatorio']['uf_origem'] ?: $despesa['DspRelatorio']['pais_origem'];
                        $destino = $despesa['DspRelatorio']['uf_destino'] ?: $despesa['DspRelatorio']['pais_destino'];

                        if( !isset( $fnc ) ){
                            $fnc = '---';
                        }
                        if( !isset( $crg ) ){
                            $crg = '---';
                        }
                        if( !isset( $mtp ) ){
                            $mtp = '---';
                        }
                        if( !isset( $ctg ) ){
                            $ctg = '---';
                        }
                        if( !isset( $uni ) ){
                            $uni = '---';
                        }
                ?>
                        <tr>
                            <td style="width: 3%;"><?php echo $uni; ?></td>
                            <td><?php echo $nome_funcionario . ' / ' . $matricula; ?></td>
                            <td><?php echo $crg . ' / ' . $fnc; ?></td>
                            <td><?php echo $despesa['DspRelatorio']['origem'] . ' / ' . $origem; ?></td>
                            <td><?php echo $despesa['DspRelatorio']['destino'] . ' / ' . $destino; ?></td>
                            <td><?php echo $despesa['DspRelatorio']['dt_inicio']; ?></td>
                            <td><?php echo $despesa['DspRelatorio']['dt_final']; ?></td>
                            <td style="font-size: 8px; width: 5%;"><?php echo $despesa['DspRelatorio']['motivo_viagem']; ?></td>
                            <td><?php echo $mtp; ?></td>
                            <td><?php echo $ctg; ?></td>
                            <td style="width: 7%;"><?php echo 'R$ ' . $valor_passagem; ?></td>
                            <td><?php echo $numero_diarias; ?></td>
                            <td style="width: 7%;"><?php echo 'R$ ' . $valor_total_diarias; ?></td>
                            <td style="width: 7%;"><?php echo 'R$ ' . $valor_total_viagem; ?></td>
                            <td><?php echo $despesa['DspRelatorio']['numero_processo']; ?></td>
                        </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">

        </div>
        <div class="span3">
            <?php echo $html->image('logo_sistema.png'); ?>
        </div>
    </div>

</body>
</html>