<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <style type="text/css">
    .container {
      margin-right: auto;
      margin-left: auto;
    }

    .container:before,
    .container:after {
      display: table;
      line-height: 0;
      content: "";
    }

    .container:after {
      clear: both;
    }

    .container-fluid {
      padding-right: 20px;
      padding-left: 20px;
    }

    .container-fluid:before,
    .container-fluid:after {
      display: table;
      line-height: 0;
      content: "";
    }

    .container-fluid:after {
      clear: both;
    }
    
    .row-fluid {
      width: 100%;
      *zoom: 1;
    }

    .row-fluid:before,
    .row-fluid:after {
      display: table;
      line-height: 0;
      content: "";
    }

    .row-fluid:after {
      clear: both;
    }

    .row-fluid [class*="span"] {
      display: block;
      float: left;
      width: 100%;
      min-height: 30px;
      margin-left: 2.127659574468085%;
      *margin-left: 2.074468085106383%;
      -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
              box-sizing: border-box;
    }

    .row-fluid [class*="span"]:first-child {
      margin-left: 0;
    }

    .row-fluid .controls-row [class*="span"] + [class*="span"] {
      margin-left: 2.127659574468085%;
    }

    .row-fluid .span12 {
      width: 100%;
      *width: 99.94680851063829%;
    }

    .row-fluid .span11 {
      width: 91.48936170212765%;
      *width: 91.43617021276594%;
    }

    .row-fluid .span10 {
      width: 82.97872340425532%;
      *width: 82.92553191489361%;
    }

    .row-fluid .span9 {
      width: 74.46808510638297%;
      *width: 74.41489361702126%;
    }

    .row-fluid .span8 {
      width: 65.95744680851064%;
      *width: 65.90425531914893%;
    }

    .row-fluid .span7 {
      width: 57.44680851063829%;
      *width: 57.39361702127659%;
    }

    .row-fluid .span6 {
      width: 48.93617021276595%;
      *width: 48.88297872340425%;
    }

    .row-fluid .span5 {
      width: 40.42553191489362%;
      *width: 40.37234042553192%;
    }

    .row-fluid .span4 {
      width: 31.914893617021278%;
      *width: 31.861702127659576%;
    }

    .row-fluid .span3 {
      width: 23.404255319148934%;
      *width: 23.351063829787233%;
    }

    .row-fluid .span2 {
      width: 14.893617021276595%;
      *width: 14.840425531914894%;
    }

    .row-fluid .span1 {
      width: 6.382978723404255%;
      *width: 6.329787234042553%;
    }

    .row-fluid .offset12 {
      margin-left: 104.25531914893617%;
      *margin-left: 104.14893617021275%;
    }

    .row-fluid .offset12:first-child {
      margin-left: 102.12765957446808%;
      *margin-left: 102.02127659574467%;
    }

    .row-fluid .offset11 {
      margin-left: 95.74468085106382%;
      *margin-left: 95.6382978723404%;
    }

    .row-fluid .offset11:first-child {
      margin-left: 93.61702127659574%;
      *margin-left: 93.51063829787232%;
    }

    .row-fluid .offset10 {
      margin-left: 87.23404255319149%;
      *margin-left: 87.12765957446807%;
    }

    .row-fluid .offset10:first-child {
      margin-left: 85.1063829787234%;
      *margin-left: 84.99999999999999%;
    }

    .row-fluid .offset9 {
      margin-left: 78.72340425531914%;
      *margin-left: 78.61702127659572%;
    }

    .row-fluid .offset9:first-child {
      margin-left: 76.59574468085106%;
      *margin-left: 76.48936170212764%;
    }

    .row-fluid .offset8 {
      margin-left: 70.2127659574468%;
      *margin-left: 70.10638297872339%;
    }

    .row-fluid .offset8:first-child {
      margin-left: 68.08510638297872%;
      *margin-left: 67.9787234042553%;
    }

    .row-fluid .offset7 {
      margin-left: 61.70212765957446%;
      *margin-left: 61.59574468085106%;
    }

    .row-fluid .offset7:first-child {
      margin-left: 59.574468085106375%;
      *margin-left: 59.46808510638297%;
    }

    .row-fluid .offset6 {
      margin-left: 53.191489361702125%;
      *margin-left: 53.085106382978715%;
    }

    .row-fluid .offset6:first-child {
      margin-left: 51.063829787234035%;
      *margin-left: 50.95744680851063%;
    }

    .row-fluid .offset5 {
      margin-left: 44.68085106382979%;
      *margin-left: 44.57446808510638%;
    }

    .row-fluid .offset5:first-child {
      margin-left: 42.5531914893617%;
      *margin-left: 42.4468085106383%;
    }

    .row-fluid .offset4 {
      margin-left: 36.170212765957444%;
      *margin-left: 36.06382978723405%;
    }

    .row-fluid .offset4:first-child {
      margin-left: 34.04255319148936%;
      *margin-left: 33.93617021276596%;
    }

    .row-fluid .offset3 {
      margin-left: 27.659574468085104%;
      *margin-left: 27.5531914893617%;
    }

    .row-fluid .offset3:first-child {
      margin-left: 25.53191489361702%;
      *margin-left: 25.425531914893618%;
    }

    .row-fluid .offset2 {
      margin-left: 19.148936170212764%;
      *margin-left: 19.04255319148936%;
    }

    .row-fluid .offset2:first-child {
      margin-left: 17.02127659574468%;
      *margin-left: 16.914893617021278%;
    }

    .row-fluid .offset1 {
      margin-left: 10.638297872340425%;
      *margin-left: 10.53191489361702%;
    }

    .row-fluid .offset1:first-child {
      margin-left: 8.51063829787234%;
      *margin-left: 8.404255319148938%;
    }
    
    .acesso-rapido{
      background: #FAFAFA;
      -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    padding: 10px;
    border:1px solid #d2d2d2;
    }
    .acesso-rapido h2 {
      font-weight: normal;
    }
    .acesso-rapido .span4 p {
      margin-left: 10px;
      margin-right: 10px;
    }
    </style>

</head>
    <body class="topo">
		<div class="container">
			<div class="row-fluid">
				 <div class="span3">
				      <h1 class="logo-empresa"><a class="brand" data-placement="right" title="ReducallWork" href="<?php echo $this->base; ?>"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAAEAYABgAAD//gAfTEVBRCBUZWNobm9sb2dpZXMgSW5jLiBWMS4wMQD/2wCEAAUFBQgFCAwHBwwMCQkJDA0MDAwMDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0BBQgICgcKDAcHDA0MCgwNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDf/EAaIAAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKCwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+foRAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/AABEIADoBLAMBEQACEQEDEQH/2gAMAwEAAhEDEQA/APsugAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA57xN4ltvCtp9tu1ldC2wCJNxLEEqCchVBxgFiBnjqQK1hB1Hyxt8zOc1TV3f5HlXir4sXWnzJFpsEflSxxTxzyksJY5Bu+VF27eQ0b5YsrqwxkCuunQTV5N3TastLNf1c5Z1mnaK00d31R0Gm/F3Sbmzaa6LWt1GpzAQzb2AJAjdRghiMDdtIz8wHWs3h5J2jqu/b1NFXi1d6PsUfDfxgtdTlS11GFrSWVgiuh8yLcxAUNwGXk4zgr6kVU8O4q8Xe3TZkwrp6SVvyPZK4TsCgAoAKACgAoAKACgD5C+J2oXkXiS6hgmlQZhCqsjAZMEXQA4GT6d+a+zwUYvDwcoxfxatJ/afkfP4htVZJN9Or7Iy7zTfFnhyP7dcfbbaNCMyCV8KScDcUc4BOB83BJA74raM8NXfs4+zk305V+F0vwM3GtT958yXe/8AwT6A+FnjKfxVYyR3pDXdmVVnAA8xHB2OQMANlWVsDBwD1Jr5zHYeOHmnT0hJNpdmt16bNHrYaq6sWpfFHr3XQ8F8a3d/L4jurS1lm3PdGONFkYAszAKo+YAZJAHQCvocNGCw8JyUbKF22k9r3ex5VVydWUYt/FZK7LH/AAhHjL/njc/+BCf/AB2p+s4T+aH/AIB/9qP2NftL/wAC/wCCeo6tb3+jeATFemSG8iK78vlxuvcj51Y9UI4DdDj2ryabhUx3NCzg9tNNIdmu/kd0lKGGtK6kvPX4u54ho2n+IfECu+m/abhYiA5WUjaWzjq464PT0r3qk6FGyq8kb7XiunojzIRqVL8nM7b6/wDBOx8H+P8AVfCuojTdZaV7feI5Y5yxkgJON6l/mAXO4rnay52jJDDixGFpYin7SikpWunHaVujtp6PdPc6aVedKXJUva9mnuvM7j443k1pDYm3keLc0+djFc4EOM4Izjt6c+tcGWRTdTmSekd0n1fc6cY2lCza1ezt2O/+Gsrz+HbOSVmd2WXLMSxP7+XqTk9OB6DivOxqSxFRJWV1tp9lHXh9aUb9n+bPnrw1qV2/iyKFppWj+2sNpkYjAduMZxj2r6WtCKwsmoxv7Na2V9l1sePTk/bJXdud9X5nqHxa8d3Og+XpOmsYp5k8yWVfvJGSVVUP8LMVYlh8ygDby2R5GAwsat61VXinZR6N9W/JaadTvxVZ07U4aNq7fZeR4vN4a8SWdmNedJ0iIEnnCX96FbG2QgP5qg5B3EDA5OBzXuKth5T+rpxctuXl0v2WlrnmunVjH2rvbe99fXe57t8J/HE/iWGSw1A77q1UMsneSInb83qyNgFv4gy5ywYn5/H4aNBqpSVoS0t2fl5P8D1MNWdROE/iXXuv8zu/GV8dN0S9uFO1lt5ApBwQzjYhBHQhmGPevPw0PaVqcOjkr+i1f4I6qsuWnKXZM+K4Nev4ZFkFxMSrBgDI+ODn1r7p04NNcsdbr4V1+R82pyWt3p5s+87adbmJJk+7IiuPowBH6Gvz1rlbi+jt9x9UndJrqfJnjvUrqHxZPFHNKkYmgAVXYAAxRZwAcDqa+wwsYvDRbjFvllq0r7y8jwK8mqzSb3j1fkfXNfGn0B5v8WJ5Lbw9PJCzRuJIcMpKkZkUHkYNepgEnXimk1aW6v0ZxYptUnbTVfmcx8ELua6srtp5HlImQAuxbA2HpknFdeZxUZ0+VJe69kl18jHBtuMrtvVb+h7fXgHpnyT8T7q80jxHN5M0qJJ5UyKHYAZRQcAHpvVq+ywKjUw8U4xbXNF6K+/p2Z8/iG4VXZu2j3f9dD034v608Wh2r2ztG11MjgoxU7PKdsZBHGWX9K8rL6adafMk+WLWqvrdL/M7sVO1ONna7W3pcyPgfq012b22uJHlKiGRN7Fsf6xWxknHVK2zOCj7OcUl8Sdkl2a2+Zng5N80W29nr8zgI9UnvPENxfz3MkVhbXwkkAeQ5Rrg+XFGi5JZ1UgDAVVBJIAGe6ajRw8Ycqc5U7LRb8urbfa/qcqbnVcrtRUrvV7X0SXmeqeM/iLpTG60G8tZ7hFJikZGjUbhg5QkkhkboccMOlfOU6MlapFpdep6dSrHWm030PKIYj401Gz0jT4zaWkKeVErESPHHkyzzSPgb3dtzY4UEqoA5J7P4UZTlq9308kkcv8AEkoR0S0X5ts9qvdJ8IeBoEjvoonkcceYnnzyY6ttwSBnuAqDoPSuFSq1XeLaXlokdjjTpK0kr/ezN1Hwj4T1+xTUbKVNPWZhHFMjbE80nhHjcgBsgggbG44PSrjUqwfLJc1tWutu91/wSXCnJc0Xy30v5+hnXPjvXfBDx6frMEd6gIMd0rFTNCMZKnG1pAOu7ackbsg7zSpQq+9BteXZk+0nS92av590eqXWr2+saHPf2L74pLSdkYZDAiN8g91dWGCOqsK5oRcasYyX2o/mjplJSg5R/lf5Hx7pK63r07W2nPcTyqpcqsrZCggE8sBjLAfjX3FR0aK5qihFXtdxW/3HzsFUm7Q5m7X3f+Ztw694l8CXiC6e4iPDGG4ZnikTJB+UkqQeRuQhgejAiud0sPi4vlUWtuaKSaf4fc9GaKdWhLW68nqmv67H0r4g1Ual4WuNRtiyCexMq4OGUsmcZGOVPBx3FfL0afs8TGlLXlqJPs7M9qpLmoucesb/AIHlHwRvp7q9uxPK8gWBSN7swHzjnkkfjXsZnFRhDlSXvPZJdPI4MG25Su29Fu/M5fxR4hvfG/iIWmlyusJkFtAEZgpVSd0p2kcE7pC3URgA/drqoUoYShz1Ur25pXSbv0jr8l6mFScq9Xlg3a/KrP73+vofVem2CaXaxWcZZlgRUDMSWbA5ZierMck+5r5Gc3Uk5vdu+miXkj3ox5EororHyT8Sv+Rsn/37f/0TFX2OC/3WPpP82eBiP4z9Y/kj6V8fAHw/f5/593/TFfL4T+PT/wASPZr/AMKf+FnjnwG/19//ANc4P/QpK9rNPhp+svyR52C3n6L9TjNd/wCR2b/sJRf+jkrupf7mv+vUvyZzT/jv/GvzR9iV8UfRHnPxY/5Fq6+sH/o+OvTwH+8Q/wC3v/SWceJ/hS+X5o4j4D/8e99/10g/lLXfmnxU/SX5o5cFtP1X5M4H4yAL4hcjjMMRP124/kAK9DLv4C/xS/Q5cV/Ffojtfjh/x6ab/wBtv/QYK4cs+Kr/ANu/mzpxm0Pn+SPSfhh/yLVl/uy/+j5a8zHf7xU9V/6Sjtw38KPo/wA2fOPhf/kcIf8Ar+f/ANDavpq/+6y/69r8keNT/jL/ABv9TU+M3/Iw/wDbCH/2assu/gL/ABy/QvF/xP8At1fqfSeq2MmpaHNZwAGWeyaJATgbnhKrk9hkjntXy9OSp1ozltGab9Ez2pJypuK3cbL7jy/4Y+AtV8K6lLdX6xrE9u0YKyBjuMkTDgdsK3P09a9bG4qliKahSbupX1VtLNfqcOGozpScppWtbR36o3/jJe/ZfD7RZwbmaKP6gEyn/wBF1zZdHmrp/wAsZP8AT9TXFu1O3dpfr+h8w3Wkm30q21H/AJ+J7iM/SJYCv6u/5V9VGd6sqX8sYP7+b/JHiuNoRn3cl91v+CfY3gK9+36BYzZyRbrGT7xZiP6pXxWKjyV6kf7zf36/qfQ0HzU4PyS+7T9D5m8f/wDI33H/AF3t/wD0VDX1OE/3WP8Ahn+cjxa/8Z+sf0Psavij6I8y+L3/ACLc/wD10g/9GrXq5f8A7xH0l+TOLFfwn6r8zlfgT/x43n/XZP8A0A115p8dP/C/zMMF8MvVfke718+eofMvx1svKv7S8A/1sDRk+8T7v5SivqsrleE4dpJ/ev8AgHi4xWlGXdNfc/8AgmD4/wBWN9ouhxZzttGLe5XZCD+cTV0YWnyVcQ/76S+d5fqjGtLmp0l/d/Ky/Qb8IdU/szVJyxwrWU5/GLbL+io1GPh7SlG3Scf/ACa6/wAgwsuWb/wv8NTnNEkjit1nujiKXU7fzWwT8kCF34HJ/wBdnAyTUYz4owj0hK3zaS/IVHbmfWS/DV/mdJr3hi71S5uNU0ySLVIJ5ZJj9lbdLGHdmw8BxKNoIGVVh9K8mE1FKE04tJLXbTz2OuUHJuUbSTd9N18tyz8J5kg8QxrJ8pkimjXPHzYDY+uFPH4Uq6vTdu6HR0mvmdm3hD/hNPE+ovqEjrb2TRIFUgOwZAURSc7UA3MSBkluMHJrD2nsqceVau//AATbk9pUlzbK3/APWNJ8I6bo1k+mwxb7aVmaRJj5oYsADndkYwAMAfrXHKpKTUm7NbW0OqNOMVypaeepwOqeHI7WQeHLpi+l6jv/ALPkclpLK7RSwhVjkmJly0YJ4AeMnBFdMZ3XtV8UfiX80e/qYOFv3b+F/D/dfb0MrwjrcdlpN74XvdsGoWy3UUadDOXVyAmQNz7jgDO5lKEA5rWUb1IVl8LcW32s1+FiIStCVJ6SSaXnucX8EeNdmH/TpL/6Nhr28z/gr/GvyZw4P+I/8L/NG/8AHofvNPP+zc/zgrmyvap/27/7ca43eHz/AEOvtv8Akn5/7B8n/s1csv8Af/8AuKv0N1/u3/bjPnjw94kbQLS+jhyJ72FYEYfwqWzI2ex2AquOQW3DpX0dWiq0qbl8MJOTXd20X37nkwqezU0t5JJeWuv4HsHwS8L7RJr1wvJzDb59P+Wsg/8ARan/AK6CvFzKvth4v+9L9F+v3HoYOnvVfov1f6fefQtfNnrnxt8U5DF4ounXqpgI+ogir7bA64aC/wAX/pTPncTpVl8vyRX174ma14htWsbl0SGQjeIkCFwDkKx5OMgHAIzjnI4qqWDo0JKpBPmW13e3/BJniKlRcsmrdbKx7r8HvDX9jaWb92VpdR2P8vIWJNwRSf72WYt6ZCnlTXgZhW9pU9ktFTuvVvd+myX3nqYWnyQ53vLX0XQ8E8bxyy+J7uO33GZrvbGFOG3lgF2nsd2Me9fQ4ZpYeDlsoa32tre/yPKq39rJLfm09TqtB8PeL4dStZLpL7yEuYWk3TMV2CRS+4eZyu3OR3GRXJVrYV05qDp83LK1o63tpbQ3hTrKcXJStdX16X9T2f4sf8i1dfWD/wBHx14eA/3iH/b3/pLPSxP8KXy/NHEfAf8A4977/rpB/KWu/NPip+kvzRy4LafqvyZwXxl/5GFv+uEX/oJr0Mu/gL/FL9Dlxf8AFfojtPjh/wAeem/9tv8A0GCuHLPiq/8Abv5s6cZtD5/kj0n4Yf8AItWX+7L/AOj5a8zHf7xU9V/6Sjtw38KPo/zZ84+F/wDkcIf+v5//AENq+mr/AO6y/wCva/JHjU/4y/xv9TU+M3/Iw/8AbCH/ANmrLLv4C/xy/QvF/wAV/wCFfqfVtj/x7xf9c0/9BFfIS+J+r/M96Oy9EWqgo+e/jxe7UsbMdzLKw+gRE/m9fSZXH+JP/DH82/yR5GNfwR9X+hzviDRzB4E02YD5knaRv92czEH8hH+ldNKpfG1Y9OW3/gPL/wAExnG2Hg/O/wB9/wDgHpXwWvftGgmHvbXEiY9mCyD9XavLzKPLW5v5op/dp+h24R3p27Nr9f1PFfH/APyN9x/13t//AEVDXuYT/dY/4Z/nI82v/GfrH9D7Gr4o+iPMvi9/yLc//XSD/wBGrXq5f/vEfSX5M4sV/CfqvzOV+BP/AB43n/XZP/QDXXmnx0/8L/MwwXwy9V+R7vXz56h4n8crLzdKt7oDJguNp9lkRs/+PRqK93LJWqSh3j+TX+bPMxivCMuz/NHzbcXsmoR21qefsyGJPffNJL/OQj8K+nUVBzl/M+Z/KKX6HjtuSjHsrL5tv9TVvkk8JatdW0ecw/abf6pJHJDn/vl81jG2IpQk+vLL5pp/mjR/upyiul196t+p6h8EtMj1Fr/7UgmtwkSmNwGjLOX+bacjcFTG7GQDjNeTmcuVU0tJXk79bK2l+12duDjfmvqrJW6Gv8RPBVt4agTXdDVrWSCZDIqM21QT8jKOSoDhQwB2kN0ryaNR1H7OprdaHXVpqmueGlmcB4nlfRdeGoWg8mV/IvvKPWGWZFkkhkA+6dxbK9djiuiHvQ5XqtY37paXOefuz5lo9Hbs3rY9qi1uJXi8X2IaSyuolg1KNRukgMZ+SbaOSYCzJKByYiJFyFGeLl0dCXxJ3i+jv0+fTzOzmtatHZq0l2t1+X5Ho0ur2cNp/aLzRi0CB/O3AptPRgwyCDkYx34rl5W3yJa9jp5klzX07njPjTx3Za1NZafpDG4lW+t5TKqkKu1wAqEgFmbdgkDAUHk5rupUnBSlPRcrVjjqVFLljDV3Tucr8TtNlPiUCxTfPNDFMFXA+ePdlyTgABYwzMSAApJIrWg/3dntdr7/APhzKsvf93eyf3f8McPB4mn8K61dXukNE/mtIobAkTbKyysFIIB2uNu4HBwcEg5r6P2KxFGEK6aaSbWzulbU832jpVJSpta381Z6kd5q2p/EPU7eC7kQyyssMQICRx7m7Aep5J5ZuBzhRVxp08FTlKCfKlzPq3b+vRCcp4iaUmrvRdEj6h8R6bHo/hS5sIclLaxaME9TtTBY+7HJPua+UozdXFQqS3lUT+9nt1IqFGUFso2+5HxWqluFBOBnj0HU/hX2584fTfwQ8QfabObR5T89qfNiB/55OfnA9kk+Y+8tfL5nS5ZRrraWj9Vt96/I9rBzunTfTVej/wAn+Z7rXz56h8efEv8A5Gyf/ft//RMVfa4L/do+k/zZ89iP4z/7d/JHpfxv0aP+z7a/hRVMEzRttUD5ZVyCcdg0YA9C3vXlZZUfPOm29Y3XrF/5M7cZD3YyXR2+/wD4Y3fgxqn23Q/szHLWczpj/Yf94p+m5nA/3awzGHLW51tOKfzWj/JGmEleny/ytr5PVHi2u/8AI7N/2Eov/RyV7lL/AHNf9epfkzzZ/wAd/wCNfmj7Er4o+iPOfix/yLV19YP/AEfHXp4D/eIf9vf+ks48T/Cl8vzRxHwH/wCPa+/66Qf+gy135p8VP0l+aOXBbT9V+TOA+MTrJ4idUIYrFEpA5w23OD74IOPevRy9Ww6v/NJnLiv4rt2R2/xxUraaaDxgzA+x2w1wZZ8VX/t382dOM2h8/wAkejfC51bw1Z7SDtWUHHYieXg+hrzMdpiKnqv/AElHZhv4Ufn+bPnHwowk8Xwsh3A3rEEcgjexyPbHNfT19MLJP/n2vyR41L+Mrfzv9TX+Mwx4g/7d4f8A2asct/gL/HL9DTF/xP8At1fqfQ+v3PleG7ieJ9hGnuyOrYIJh+QqwIOSSNpBznGK+ZpRviIxav8AvFdf9va3R7E3ak2n9nf5HivwY1S9v9XmS5nmnRbRyBJI7qD5sIBwzEZwSAeuCfWvdzGEIUouMYp862SXR9jzcJKTm0237vVt9UY3xqvftGuiAHi2t40x6MxaQ/mHX8q3y2PLR5v5pN/dZfozPFu9S3ZL8dTC1D4feI7Cya6uoiLWFA7fvom2qO+wSE8Z6AZFdEMXQnNQg/fbsvda19bGMqFWMeaS91K+6/K56L8B73m+sz3EUqj6F1b+aV5maR0pz/xR/Jr9TswT+KPo/wBDg/H/APyN9x/13t//AEVDXoYT/dY/4Z/nI5a/8Z+sf0Psavij6I8y+L3/ACLc/wD10g/9GrXq5f8A7xH0l+TOLFfwn6r8zlfgT/x43n/XZP8A0A115p8dP/C/zMMF8MvVfke718+eocD8T7L7d4cu1HWJUlHt5cis3/jm6vRwMuTEQ824/emvzOTEq9KXlr9zPk3wnZ/b9YsrfqHuYQf93zFLf+O5r6+vLkpVJdoS/Kx4NNc04x/vL8zqvi3Z/ZPEc7DgTrFKPxjVT/48rVyYCXNh4+Tkvxv+pviVy1X52f4f8A9e+B9n5GjzXB6z3JA/3Y0QD/x5mrxszlerGH8sV+Lb/wAj0MGrQcu8vyRoeLfE9vNqkehFlBiTz9kvywz3QG60tpX6LHvxI5Pyuyxxk4LV59ODUfad9NN0urXn0+83nNOXJ2112b6J+XU+bNT+1fa5jqAcXZkYzCQYbzCctkfyxxtxt4xXpxtZcu1tPQ893u+bfqbfhXxbeeE5zLbYkhlwJoHPySAd++1wOjgHjhgy8VE6aqKz3Wz7Fwm6butuq7ntehah4T12OeGNzZrfIVmsZZDFEGJDGSJM+UJN3IeIjnnaDXDJVYWe9tpJXfo+tvU7IunK6Wl903ZfLp9xJpnhjwx4IkOqzXQlePJiMsiMUyD/AKuOMAvJjIBwx54xmhzqVfcUbd7J/mwUKdL3m722v/kjyPU/Gf8AaOvPqxUrbyI1tswC4tnRomIH/PTa7SAf3vk6V2whyRUVumpfNO/3aWOSU+aTl0enyat95sfCPTvsfiS6srgJK0FvMhOAVJWWEbgDnGRzjquSp5BFejj58+HhUjdc0k+z1TMsLHlqyi+ia+5o534g2jeHPFEs0ACfvY7qLHAy21z06AShhx6V1YSXt8NGMuzg/wAvyaMa69nVbXdSX9ep9J+LrpL3wzd3MXKTWTSL/uvHuH6Gvl8PFwxEIvdVEn8nY9mq70pNbOLf4HhHwa0q21W6vorlQ4a0aL6LKQj49CV4B6jnHU19BmM5U4U3B29+/wA46o8vCRUpSUv5bffuct4fvZfAfiRRcHaLadoJ+wMRJRmx3GMSJ6kKa66sVjMO+X7UVKPqtUvzRhBuhV16Oz9P61PtMEMMjkHkEV8NsfSHy94+8J6vqPiSa8tbSeWBnhKyKhKkLFEDg9OCCD7g19ZhK9KGHjCc4qSUtG9dW7Hh16U5VXKMW1pr8ke6+OtHbXNEurOJS8pj3xqOpeMiRVX3YrtH1xXz+FqKjWhNuyvZ+j0f5nqVoc9OUVvbT1Wp5X8H9H1bQL24hvrWeCC4iB3uhC+ZG3yjJ9Vd/wAq9fMKlKrCMqc4ylFtWT1s1/mkcOFjOnJqcWk117r/AIc4rxl4Q1y6126vLO0uGRpy8ckat2OVZWHuMgg5Fd+Hr0Y0IQnOKajZpv10Zy1aVR1JSjF2vdNFL+y/HH/UU/7+XH/xVXz4PvS+5f5C5cR/f+9/5nrN7perX3gX7FcR3E2osRujk3PMcXm4Z3EscRgEZOdoFePGdKGN54uKprZrSPwW/P8AE73Gbw/K03Ps9/i/yPGbDwz4t0sMtlBf2wfBYRGWMNjOM7CM4ycZ6ZNe3KthalueVOVtr2dvvR5sadaPwqa9NDuPBPwr1C6vV1LX1MUUbiTy3YNLM4ORv5O1c8vuO5vu7fm3DgxOOpxg6WHd21a6VoxXltr2tojqo4aTkp1dEnez3b8z2Tx34RXxhpxtQwjuIm8yBznAcAgq2Odjg4OM4O1sHbg+JhcR9Vqc9rxatJeXl5r/AIB6Nal7aPLs1qn5nzIfD3i3QhJp0MN9HFKSHWDzWik7ZzFlDkcHnkcH0r6r2uFq2qOVNtbOVlJffqeLyVqd4JSSfa9n9x6p8L/htc6RcDWNWXypUDCCEkFlLAqZJMZAO0kKvXJy20qAfJxuMjUj7Ci7p/FLpp0Xz3f3HdhsO4P2lTRrZfqze+KHw/l8Uol/p+De26bChIXzo8lgoY4AdCWK5IBDEEggVzYHFrDt06vwSd7/AMr/AMn1NcTQdW04fEtLd1/meENoviyeJdHaHUGgQjELLN5Ix06/uwoPIOdo6ivoPaYZP2ylT5n9rTm/zv8AieXyVmvZ2nbtrb/I+g/hn4EfwhbvPeFTe3IAYKciJF5CBhwWJ5cj5chQM7dzfOY3FLEyUafwR2vpd97fketh6PsU3L4n+C7f5nlHiLwlrGueJ5blrSf7NLdonmFDt8lGWPfnpt2Luz6V7FGvSpYZR548yg3a+vM03b1uzgqUpzqt8r5XJa9LKy/I+lNcsv7R0+5swMme3ljA92RlH6kV8tSlyTjPtJP7me1Nc0ZR7pr8D5++FPh7V9B1kyXdrPBBNBJGzuhCg5V1yT7oAPc19Jj6tKrStCcXJSTST16p/meRhYTpzvKLSaa1+8x/iB4R1q/8QXV5ZWk8kTvGUkRGIO2GMZUj0YEfUVrhK9GFCEJzimk7pvzf6GdelUlUlKMW1pZr0Rlf2Z45/wCor/38uP8A4qtufB/9OvuX+RHLiP8Ap597/wAz1vW9K1TUfBMdnJHPNqB8vej7nmJWckls5Y4XB55xivIpzpQxjmnFU/es1pH4enzO+cZyw6i03PS667ni1j4Z8XaYpSzhv7dXOWERljBI4BIQjJ+te3Kthams5U5W2vZ2+9HmqnWjpFTXpoel/Diy8Twaurax9u+y+XID57ytHu2/LkOxGc9O+a8zGSwzotUfZ8918KSduuyO3Dqqpr2nNy2e7dj3TWLP+0bG4tMZ8+CWPH++jKP518/TlyTjPtJP7mepJc0XHumvwPmj4ceCdVstet7q+tZoIIfMcs6FVz5bhBk/7RFfU4zE0pUZxpzi27Kyetrpv8jxcPRmqkXOLSV9X6HS/GDwnf6xfW93p1vJcfuDG5jUttKOzLnHTIc4+ntXLl9enThKFWSj711fTda/kbYqnKUoyhFvSzt5P/gnpvw70iXRNCtrW4QxTASPIrDDBnkdgCPUKVFeVjKiq15yi7x0Sa2skkd2Hi4U4xas9W16sb418D23i635xFeRg+VNj8dkgH3oyfxU/MvOQcKVV0n3j1X6rzKqU1UXZrZ/5ngksBv5hofiJJ4dStcLBcwxG4mkiHSGRFIM6FeYJ8/L0c7c16HwrnpWcXum7JPun080cO75Kl1JbNK79H38mdVP4PvtKtd9hpFvcQEZlF7IJryRR04jZI4PXZAzOCOSeRWSqRk7Sm0+nLpFf5/M15HFe7BNdb6v/gfI851aysbvT01fTomtlFw1tc27SmVY3KCSJ42ZQ4SQBxhyxDLjPr0xbUuSTvpdO1vJnO0nHnira2avf0IfB2jRa5q1vZTKWicuXCnaSqRs+Nw5XJABI6AmipJwi5Ldf5ipx5pKLPSdM8MSapPMt1pI0650tDLCbct5M7KC0MEiuXE5LDd50bhiBtcDIFYc6jy+/eMmk77pdX5adGdCg5XvCzirq2z7Lz9RPhP4b1bStamutSt5olkt5QZJFKhpGliY5J/ibDN+Br0cfWpVKShSnF2ktE9kk0YYWnOM3KcWrp6vvdGn8YvCV7rM9readA9w4jeKURqWICsGjzjsd7/lWWXV4Uozp1ZKKumr/c/yReKpSm4ygm9Gnb8P1Oi02z1GfwVJp9xBKl5HbSwLEykOwGRFtHU/IVUe4Nc85U44xVIyTg5xldbK+/43NYqTw7g0+ZRat18vwOY+DvhzUtEvLp9Qt5bZZIVVTIpUEhwcDPfHNdWY1qdWEFTmpNSd7PbQxwlOcJSc4taLf1KfxZ8D3t/qMepaXA9x9oj2zCNdxWSPChmxz8yFQP8AcPrV4DEwhTdKrJR5XeN+z6fJ/mTiqMpSU4Ju61t3X/A/I9Z8BTXraPBFqcUkFzbDyWEikFlQDY4zyQUKqSerK1ePilBVZOjJOMveVul919/4WO+g5ezSmmmtNfLZ/cdlXCdIUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFADPKTf5m1d4G3dgbsdcZ64zzjpTv06C8x9IZ4Rc+Cr+w1G/gt7FdQ0vUPLlCNcC3CSK/mAhuXBRi4+UDKMBnqK9BVIuMW5cs46bX8jhdNxlJKN4vzt/VjrfBfhJdKne7uNPtbCVBthaGea4fDA79zSHaOwG0ZPPOKxqVLrlUm11ukvQ1pw5XdxSfSzbPSq5TpCgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//9k="/></a></h1>
				 </div>
			</div>
		</div>
		<div class="container">
			<div class="acesso-rapido">
				<?php echo $content_for_layout; ?>
			</div>
		</div>        
    </body>
</html>