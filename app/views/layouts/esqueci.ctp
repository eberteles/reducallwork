<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php __('ReducallWork'); ?> </title>
    <?php
    echo $this->Html->meta('icon');
    echo $this->Html->script('jquery-2.0.3.min');
    echo $this->Html->css('bootstrap');
    echo $this->Html->css('bootstrap-responsive');
    echo $this->Html->css('sis');
    echo $this->Html->script('bootstrap');
    echo $this->Html->script('app');

    $usuario = $this->Session->read('usuario');
    ?>
</head>
<body class="topo">
<div class="container">
    <div class="row-fluid">
        <div class="span3">
            <h1 class="logo-empresa">
                <a class="brand" data-placement="right" style="cursor: default;"
                   title="<?php echo Configure::read('App.config.resource.layout.logo_superior.esquerda.title'); ?>"
                   href="#">
                    <img src="<?php echo $this->base . Configure::read('App.config.resource.layout.logo_superior.esquerda.path'); ?>"/>
                </a>
            </h1>
        </div>
        <div class="span6"></div>
        <div class="span3">
            <div class="pull-right">
                <h1 class="logo-n2o">
                    <a class="brand" data-placement="left"
                       href="<?php echo Configure::read('App.config.resource.layout.logo_superior.direita.link'); ?>"
                       title="<?php echo Configure::read('App.config.resource.layout.logo_superior.direita.title'); ?>">
                        <img src="<?php echo $this->base . Configure::read('App.config.resource.layout.logo_superior.direita.path'); ?>"/>
                    </a>
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="acesso-rapido">
        <?php
        if ($this->Session->check("Message.flash")) {
            ?>
            <div class="alert alert-block">
                <?php echo $this->Session->flash(); ?>
            </div>
            <?php
        }
        ?>
        <?php echo $content_for_layout; ?>
        <?php echo $this->element('bootjs'); ?>
    </div>
</div>
<div class="container">
    <!-- FOOTER -->
    <div class="featurette-divider"></div>
    <footer>
        <!-- <p class="pull-right"><a href="#" class="btn btn-small btn-primary irtopo"><i class="icon-circle-arrow-up icon-white"></i> Topo</a></p> -->
        <div class="sistema">Sistema melhor visualizado em 1024x768</div>
        <div class="sistema">&copy; <?php echo date('Y') . ' ' . __('ReducallWork', true); ?> -
            Versão: <?php echo Configure::read('App.config.version'); ?></div>
    </footer>
</div>
<?php
echo $this->element('sql_dump');
?>
</body>
</html>
