<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo __('GESCON') . ' - ' . $title_for_layout; ?></title>
        
	<?php
                echo $this->Html->css( 'bootstrap.min' );
                echo $this->Html->css( 'bootstrap-responsive.min' );
                echo $this->Html->css( '../assets/css/font-awesome.min' );
                echo $this->Html->css( '../assets/css/ace-fonts' );
                echo $this->Html->css( '../assets/css/ace.min' );
                echo $this->Html->css( '../assets/css/ace-responsive.min' );
                
                echo $this->Html->script( '../assets/js/ace-extra.min' );
                
		echo $scripts_for_layout;
	?>
        
</head>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='../assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="../assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="../assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="../assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.slimscroll.min.js"></script>
		<script src="../assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.min.js"></script>
		<script src="../assets/js/flot/jquery.flot.min.js"></script>
		<script src="../assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="../assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
<body>
    

		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , '')}catch(e){}
				</script>
                                
					<div class="nav-search" id="nav-search">
                                            <form class="form-search">
                                            <span class="input-icon">
                                                <input type="text" placeholder="Palavra Chave ..." class="input-small nav-search-input" id="palavra-chave" />
						<i class="icon-search nav-search-icon"></i>
                                            </span>
                                            </form>
					</div><!-- #nav-search -->
                                
				<ul class="nav nav-list">
					<li class="active" id="li_view_dashbord">
						<a href="#dashbord" id="link_view_dashbord">
							<i class="icon-dashboard"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
					</li>
					<li>
                                                <a href="#" class="dropdown-toggle">
							<i class="icon-list-alt"></i>
							<span class="menu-text"> Cadastro </span>
                                                        <b class="arrow icon-angle-down"></b>
						</a>
						<ul class="submenu">
							<li>
								<a href="form-elements.html">
									<i class="icon-double-angle-right"></i>
									Nova Proposta
								</a>
							</li>

							<li>
								<a href="form-wizard.html">
									<i class="icon-double-angle-right"></i>
									Novo <?php __('Contrato'); ?>
								</a>
							</li>

							<li id="li_list_contratos">
								<a href="#list_contratos" id="link_list_contratos">
									<i class="icon-double-angle-right"></i>
									Meus <?php __('Contratos'); ?> / Propostas
								</a>
							</li>

						</ul>
					</li>
                                    
				</ul><!-- /.nav-list -->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
				</div>

				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

			<div class="main-content">

				<div class="page-content">

				<?php echo $this->Session->flash(); ?>
				
                                    
                                    
                                    <div class="row-fluid" id="id_diviframe" style="display: none;">
                                        <div class="span12">

                                            <iframe id="iframe_generico" src="" frameborder="0" style="width:100%; height: 750px; overflow: hidden">
                                              <p>Favor utilizar outro Navegador de Internet.</p>
                                            </iframe>

                                        </div>
                                    </div>
                                    
                                    <div class="row-fluid" id="view_dashbord">
                                        <div class="span12">
                                            <?php echo $content_for_layout; ?>
                                        </div>
                                    </div>
                                    
				</div>

			</div>
		</div>
</body>
</html>

  <script>
  
    function fechar_tudo() {
        $('#view_dashbord').hide();
        $('#id_diviframe').hide();
        
        $('#li_view_dashbord').removeClass('active');
        $('#li_list_contratos').removeClass('active');
    }
  
    $('#palavra-chave').keydown(function(e){
        var keyCode     = e.which;
        var conteudo    = $(this).val();
        if(keyCode == 13) {
            if(conteudo != '' && conteudo.length >= 2) {
                fechar_tudo();
                $('#id_diviframe').show();
                document.getElementById('iframe_generico').src = "http://localhost/gesconti/contratos/index?palavra_chave=" + conteudo;
            }
            return false;
        }
    });
    
    $('#link_view_dashbord').click(function(){
        fechar_tudo();
        $('#li_view_dashbord').addClass('active');
        $('#view_dashbord').show();
    })
    
    $('#link_list_contratos').click(function(){
        fechar_tudo();
        $('#li_list_contratos').addClass('active');
        $('#id_diviframe').show();
        document.getElementById('iframe_generico').src = "http://localhost/gesconti/contratos/index?list_contratos=1";
    })
    
  </script>