<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo Configure::read('App.config.resource.layout.caption'); ?></title>
    <?php
    echo $this->Html->meta('icon');
    echo $this->Html->css('bootstrap');
    echo $this->Html->css('bootstrap-responsive.min');
    echo $this->Html->css('font-awesome.min.css');
    ?>
    <!--[if IE 7]>
    <?php echo $this->Html->css( 'font-awesome-ie7.min.css' ); ?>
    <![endif]-->
    <!--[if lte IE 8]>
    <?php echo $this->Html->css( 'ace-ie.min.css' ); ?>
    <![endif]-->

    <?php
    echo $this->Html->css('ace-fonts.css');
    echo $this->Html->css('ace.min.css');
    echo $this->Html->css('ace-responsive.min.css');
    echo $this->Html->css('ace-skins.min.css');
    echo $this->Html->css('sis');
    echo $this->Html->css('jquery-ui-1.10.3.full.min');
    echo $this->Html->css('chosen');
    echo $this->Html->css('jquery.treegrid'); // acrescentei
    echo $this->Html->css('jquery-silk-icons'); // acrescentei
    echo $this->Html->css('bootstrap-datetimepicker.min');
    echo $this->Html->css('dataTables.bootstrap.min.css');

    echo $this->Html->script('ace-extra.min');
    echo $this->Html->script('jquery-1.10.2.min');
    echo $this->Html->script('jquery.easy-pie-chart.min');
    echo $this->Html->script('jquery.maskMoney'); // acrescentei
    echo $this->Html->script('bootstrap');
    echo $this->Html->script('bootstrap-datetimepicker.min');
    echo $this->Html->script('ace-elements.min');
    echo $this->Html->script('ace.min.js');
    echo $this->Html->script('jquery.maskedinput.min');
    echo $this->Html->script('jquery-ui-1.10.3.full.min');
    echo $this->Html->script('chosen.jquery.min');
    echo $this->Html->script('datatables.min.js');
    echo $this->Html->script('jquery.dataTables.min.js');
    echo $this->Html->script('moment/moment.js');
    echo $this->Html->script('app'); // acrescentei
    echo $this->Html->script('aba'); //acrescentei
    echo $this->Html->script('jsapi');

    echo $this->Html->script('jquery.treegrid');
    echo $this->Html->script('jquery.treegrid.bootstrap2');
    echo $this->Html->script('jquery.cookie');

    echo $this->Html->script('datatables.min.js');
    echo $this->Html->script('jquery.dataTables.min.js');
    echo $this->Html->script('dataTables.bootstrap.min.js');

    echo $this->Html->script('jquery.limit-1.2'); // acrescentado para limitar a quantidade de caracteres no textarea
    echo $this->Html->script('moment/moment.js');
    $usuario = $this->Session->read('usuario');
    ?>
</head>

<body class="navbar-fixed" style>
<div class="navbar navbar-fixed-top" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="navbar-inner">
        <div class="container-fluid">
            <div style="float: left;">
                <a title="<?php echo Configure::read('App.config.resource.layout.caption'); ?>"
                   href="<?php echo Router::url('/', true); ?>">
                    <img alt="Logo"
                         src="<?php echo $this->base . Configure::read('App.config.resource.layout.img_logo_topo'); ?>"/>
                </a>
            </div>

            <div class="navbar navbar-inverse">

            </div><!-- /.navbar -->
        </div><!-- /.container-fluid -->
    </div><!-- /.navbar-inner -->
</div>

<div class="main-container container-fluid">
    <div class="main-content">

        <div class="page-content">
            <div class="row-fluid">
                <div class="span12">
                    <?php
                    if ($this->Session->check("Message.flash")) {
                        ?>
                        <div class="alert alert-block">
                            <?php echo $this->Session->flash(); ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php echo $content_for_layout; ?>
                    <?php echo $this->element('bootjs'); ?>
                </div><!-- /.span -->
            </div><!-- /.row-fluid -->
        </div><!-- /.page-content -->

    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<footer>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">

        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
    <div class="sistema">&copy; <?php echo date('Y') . ' ' . __('GESCON', true); ?> -
        Versão: <?php echo Configure::read('App.config.version'); ?></div>
</footer>
<?php
echo $this->Session->flash('email');
echo $this->element('sql_dump');
?>
</body>
</html>
