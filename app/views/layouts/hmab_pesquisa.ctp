<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HMAB | Pesquisa</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
    echo $this->Html->meta( 'icon' );

    echo $this->Html->css( 'bootstrap' );
    echo $this->Html->css( 'bootstrap-responsive.min' );
    echo $this->Html->css( 'font-awesome.min.css' );
    ?>
    <!--[if IE 7]>
    <?php echo $this->Html->css( 'font-awesome-ie7.min.css' ); ?>
    <![endif]-->
    <!--[if lte IE 8]>
    <?php echo $this->Html->css( 'ace-ie.min.css' ); ?>
    <![endif]-->
    <?php
    echo $this->Html->css( 'ace-fonts.css' );
    echo $this->Html->css( 'ace.min.css' );
    echo $this->Html->css( 'ace-responsive.min.css' );
    echo $this->Html->css( 'ace-skins.min.css' );
    echo $this->Html->css( 'sis' );
    echo $this->Html->css( 'jquery-ui-1.10.3.full.min' );
    echo $this->Html->css( 'chosen' );
    echo $this->Html->css( 'jquery.treegrid' ); // acrescentei
    echo $this->Html->css( 'jquery-silk-icons' ); // acrescentei
    echo $this->Html->css( 'bootstrap-datetimepicker.min' );

    echo $this->Html->script( 'ace-extra.min' );
    echo $this->Html->script( 'jquery-1.10.2.min' );
    echo $this->Html->script( 'jquery.easy-pie-chart.min' );
    echo $this->Html->script( 'jquery.maskMoney' ); // acrescentei
    echo $this->Html->script( 'bootstrap' );
    echo $this->Html->script( 'bootstrap-datetimepicker.min' );
    echo $this->Html->script( 'ace-elements.min' );
    echo $this->Html->script( 'ace.min.js' );
    echo $this->Html->script( 'jquery.maskedinput.min' );
    echo $this->Html->script( 'jquery-ui-1.10.3.full.min' );
    echo $this->Html->script( 'chosen.jquery.min' );
    echo $this->Html->script( 'app' ); // acrescentei
    echo $this->Html->script( 'aba' ); //acrescentei
    echo $this->Html->script( 'jsapi' );
    echo $this->Html->script( 'jquery.treegrid' );
    echo $this->Html->script( 'jquery.treegrid.bootstrap2' );
    echo $this->Html->script( 'jquery.cookie' );
    echo $this->Html->script( 'jquery.limit-1.2' ); // acrescentado para limitar a quantidade de caracteres no textarea

    ?>

</head>

<body style="z-index:0; width: 90%; margin: 0 auto; margin-top: 2%;">

<div class="row-fluid">
    <div class="span2">
        <?php echo $html->image('logo_hmab.jpg', array('style' => 'width: 48px;')); ?>
    </div>

    <div class="span8" style='font-size: 30px; font-family: Arial; text-align: center;'>
        Pesquisa de local para seu Exame ou Consulta
    </div>

    <div class="span2">
        <?php echo $html->image('logo_sistema.png'); ?>
    </div>
</div>

<hr style="width: 100%;" />

<?php echo $this->Form->create('Busca', array('url' => '/hmab_pesquisa/index'));?>

<div class="row-fluid">
    <h1>Encontre o local para realizar seu exame ou consulta.</h1>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha um dos campos para fazer a pesquisa</p>
    </div>

    <div class="row-fluid">
        <div class="span4">
            <div class="controls">
                <?php echo $this->Form->input('exame', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $exames, 'label' => 'Qual o nome do Exame ou Consulta que deseja pesquisar?')); ?>
            </div>
        </div>
        <div class="span4">
            <div class="controls">
                <?php echo $this->Form->input('bairro', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $bairros, 'label' => 'Em qual Cidade você gostaria de realizar seu Exame ou Consulta?')); ?>
            </div>
        </div>
        <div class="span4">
            <div class="controls">
                <?php echo $this->Form->input('fornecedor', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $fornecedores, 'label' => 'Existe algum Hospital ou Clínica de sua preferência?')); ?>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
    </div>
</div>

<div class="page-header"><h1>Resultados da Pesquisa</h1></div>
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbPesquisa">
    <tr>
        <th>EXAME/CONSULTA</th>
        <th>HOSPITAL OU CLÍNICA</th>
        <th>CIDADE</th>
        <th>ENDEREÇO</th>
        <th>TELEFONES</th>
        <th>OBSERVAÇÕES</th>
    </tr>
    <?php
    $i = 0;
    foreach ($locais as $local){
        $class = null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
        ?>
        <tr <?php echo $class;?>>
            <td><?php echo $local['ExameConsulta']['codigo'] . ' -' . $local['ExameConsulta']['nome']; ?></td>
            <td><?php echo $local['LocalAtendimento']['Fornecedor']['no_razao_social']; ?></td>
            <td><?php echo isset($local['LocalAtendimento']['Bairro']['bairro']) ? $local['LocalAtendimento']['Bairro']['bairro'] : "---"; ?></td>
            <td><?php echo $local['LocalAtendimento']['endereco']; ?></td>
            <td><?php echo $local['LocalAtendimento']['telefone1'] . ' / ' . $local['LocalAtendimento']['telefone2']; ?></td>
            <td><?php echo $local['LocalAtendimento']['observacoes']; ?></td>
        </tr>
    <?php } ?>
</table>

<p><?php
    echo $this->Paginator->counter(array(
        'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
    ));
    ?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</body>
</html>