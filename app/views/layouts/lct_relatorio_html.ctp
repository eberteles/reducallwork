<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)https://sistemas.novacap.df.gov.br/sisprot/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.: Protocolo | Novacap :.</title>
    <link rel="shortcut icon" href="/img/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <http-equiv="pragma" content="NO-CACHE">

    <?php
    echo $this->Html->meta( 'icon' );

    echo $this->Html->css( 'bootstrap' );
    echo $this->Html->css( 'style' );
    echo $this->Html->css( 'fonts' );
    echo $this->Html->css( 'msg' );
    echo $this->Html->css( 'table' );
    echo $this->Html->css( 'estiloGeral' );
    echo $this->Html->css( 'nav-h' );
    echo $this->Html->css( 'alertas' );
    echo $this->Html->css( 'bootstrap-responsive.min' );
    echo $this->Html->css( 'font-awesome.min.css' );
    echo $this->Html->css( 'ace-fonts.css' );
    echo $this->Html->css( 'ace.min.css' );
    echo $this->Html->css( 'ace-responsive.min.css' );
    echo $this->Html->css( 'ace-skins.min.css' );
    echo $this->Html->css( 'chosen' );
    echo $this->Html->css( 'jquery.treegrid' );
    echo $this->Html->css( 'jquery-silk-icons' );
    echo $this->Html->css( 'bootstrap-datetimepicker.min' );

    echo $this->Html->script( 'jquery-1.4.2.min' );
    echo $this->Html->script( 'bootstrap-datetimepicker.min' );

    ?>

</head>

<body style="z-index:0; width: 90%; margin: 0 auto; margin-top: 2%;">

    <div class="row-fluid">
        <div class="span12">
            <fieldset style="border: 2px solid #F0F0F0;height: 50px;border-style: radius; border-radius: 5px;" >
                <?php if( $modalidade != null ) { ?>
                    <a href="<?php echo $this->Html->url(array ('controller' => 'lct_relatorio', 'action' => 'imprimirRelatorioGeral' . '/' . $modalidade) )?>"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>
                <?php }else{ ?>
                    <a href="<?php echo $this->Html->url(array ('controller' => 'lct_relatorio', 'action' => 'imprimirRelatorioGeral') )?>"><button type="button" class="btn btn-primary" style="margin-left: 1%;margin-top: 3px;" >Exportar para PDF</button></a>
                <?php } ?>
            </fieldset>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span2">
            <?php echo $html->image('logo_novacap.png'); ?>
        </div>

        <div class="span8" style='font-size: 30px; font-family: Arial; text-align: center;'>
            TABELA DE LICITAÇÕES
            <br />
            <p style='font-size: 12px;'>(Lei de Acesso à Informação - LAI, Lei Federal nº 12.527, de 18 de novembro de 2011)</p>
        </div>

        <div class="span2">
            <?php echo $html->image('logo_gdf.png'); ?>
        </div>
    </div>

    <div class="row-fluid" >
        <div class="span12">
            <table style="max-width: 100%; font-size: 10px;" class="table table-bordered table-striped">
                <thead>
                    <th><b>ITEM</b></th>
                    <th><b>OBJETO</b></th>
                    <th><b>MODALIDADE PROCESSO</b></th>
                    <th><b>DATA</b></th>
                    <th><b>VALOR ESTIMADO</b></th>
                    <th><b>VALOR CONTRATADO</b></th>
                    <th><b>DIFERENÇA</b></th>
                    <th><b>PRAZO DE EXECUÇÃO</b></th>
                    <th><b>FASE ATUAL</b></th>
                </thead>
                <tbody>
                <?php
                    foreach($licitacoes as $lic){
                        $fase_atual = '';
                        foreach ( $fases as $fase ) {
                            if( $fase['LctFases']['co_licitacao'] == $lic['LctRelatorio']['id'] ) {
                                $fase_atual = $fase['LctFases']['descricao'];
                            }
                        }
                ?>
                        <tr>
                            <td style="width: 3%;"><?php echo $lic['LctRelatorio']['id']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['objeto']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['nu_licitacao'] . ' / ' . $lic['LctRelatorio']['modalidade_processo']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['dt'] . "às " . $lic['LctRelatorio']['hora']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['valor_estimado']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['valor_contratado']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['diferenca']; ?></td>
                            <td><?php echo $lic['LctRelatorio']['prazo_execucao']; ?></td>
                            <td><?php echo $fase_atual; ?></td>
                        </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span9">

        </div>
        <div class="span3">
            <?php echo $html->image('logo_sistema.png'); ?>
        </div>
    </div>

</body>
</html>