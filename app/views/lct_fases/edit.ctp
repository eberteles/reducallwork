<?php echo $this->Form->create('LctFases', array('url' => "/lct_fases/edit/" . null . "/" . false)); ?>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Fases'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span4">
                        <?php
                            echo $this->Form->input('data', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    'class' => 'input-small','label' => 'Data', 'type'=>'text', 'id' => 'DataFase')
                            );
                            echo $this->Form->input('descricao', array('class' => 'input-xlarge', 'type' => 'textarea', 'id' => 'DescricaoFase', 'label' => 'Fase Atual'));
                            echo $this->Form->input('co_licitacao', array('class' => 'input-xlarge', 'type' => 'hidden'));
                            echo $this->Form->input('id', array('class' => 'input-xlarge', 'type' => 'hidden'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
        </div>
    </div>
</div>
