<?php echo $this->Form->create('LctLicitacoes', array('url' => "/lct_licitacoes/edit/", 'id' => 'FormLicitacao')); ?>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Cadastro de Licitação'); ?></h1></div>
    <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Editar <?php __('Licitação'); ?>. Campos com * são obrigatórios.</p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'lct_licitacoes', 'action' => 'index')); ?>" class="btn btn-small btn-primary" title="Listar <?php __('Licitações'); ?>">Listagem</a>
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
        </div>
    </div>

    <div class="row-fluid">
        <p>Campos com * são obrigatórios</p>
        <div class="widget-header widget-header-small"><h4><?php __('Dados da Licitação'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span4">
                        <?php
                        echo $this->Form->input('sub_judice', array('type' => 'checkbox', 'id' => 'SubJudice', 'label' => 'Esta licitação está <b>sub judice</b>'));
                        echo "<br/>";
                        echo $this->Form->input('modalidade_licitacao', array('class' => 'input-xlarge', 'id' => 'ModalidadeLicitacao', 'label'=>__('Modalidade da Licitação', true), 'type' => 'select', 'empty' => 'SELECIONE...', 'options' => array( 'CONCORRÊNCIA' => 'CONCORRÊNCIA', 'T. DE PREÇOS' => 'T. DE PREÇOS', 'CONVITE' => 'CONVITE', 'PRE-QUALIFICAÇÃO' => 'PRE-QUALIFICAÇÃO', 'P. ELETRÔNICO' => 'P. ELETRÔNICO', 'P. PRESENCIAL' => 'P. PRESENCIAL' )));
                        echo $this->Form->input('objeto', array('class' => 'input-xlarge', 'label'=>'Objeto', 'id' => 'Objeto', 'type' => 'textarea', 'style' => 'margin: 0px 0px 10px; width: 400px; height: 120px; max-width: 400px; max-height: 120px;', 'maxlength' => 255));
                        ?>
                    </div>
                    <div class="span3">
                        <?php
                        echo $this->Form->input('valor_estimado', array('class' => 'input-xlarge', 'onblur' => 'calculaDiferenca()', 'id' => 'ValorEstimado', 'label'=>__('Valor Estimado R$', true), 'type' => 'text', 'value' => formatMoney($this->data['LctLicitacoes']['valor_estimado'])));
                        echo $this->Form->input('valor_contratado', array('class' => 'input-xlarge', 'onblur' => 'calculaDiferenca()', 'id' => 'ValorContratado', 'label'=>'Valor Contratado R$', 'type' => 'text', 'readonly' => true));
                        echo $this->Form->input('diferenca', array('class' => 'input-xlarge',  'id' => 'Diferenca', 'label'=>'Diferença %', 'type' => 'text', 'disabled' => 'disabled'));
                        ?>
                    </div>
                    <div class="span5">
                        <?php
                            echo $this->Form->input('modalidade_processo', array('class' => 'input-xlarge', 'id' => 'ModalidadeProcesso', 'label'=>'Processo', 'type' => 'text', 'mask' => '999.999.999/9999', 'readonly' => true));
                            echo $this->Form->input('nu_licitacao', array('class' => 'input-xlarge', 'id' => 'NuLicitacao', 'label' => 'Nº da Licitação', 'type' => 'text', 'readonly' => true));
                        ?>
                        <div class="row-fluid">
                            <div class="span4">
                                <?php
                                    echo $this->Form->input('dt', array(
                                            'before' => '<div class="input-append date datetimepicker">',
                                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                            'class' => 'input-small','label' => 'Data', 'type'=>'text', 'id' => 'DataLicitacao')
                                    );
                                ?>
                            </div>
                            <div class="span6">
                                <?php
                                    echo $this->Form->input('hora', array(
                                        'type'  => 'text',
                                        'mask'  => '99:99',
                                        'label' => 'Hora',
                                        'class' => 'input-mini',
                                        'id'    => 'HoraLicitacao'
                                    ));
                                ?>
                            </div>
                        </div>
                        <?php
                            echo $this->Form->input('prazo_execucao', array('class' => 'input-xlarge', 'id' => 'PrazoExecucao', 'label'=>__('Prazo de Execução (em dias)', true), 'type' => 'text', 'mask' => '9?999'));
                        ?>
                    </div>
                </div>
                <div class="">
                    <div class="btn-group">
                        <button rel="tooltip" onclick="atualizarLicitacao()" type="button" class="btn btn-small btn-primary bt-pesquisar" id="Salvar" title="Salvar"><i class="icon-save"></i> Salvar</button>
                        <button rel="tooltip" type="reset" title="Resetar dados preenchidos" class="btn btn-small" id="Limpar"><i class="icon-undo"></i> Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="lotes">
        <div class="span12">
            <div class="widget-header widget-header-small"><h5><?php __('Lotes'); ?></h5></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="listaLotes">
                                <table class='table table-bordered table-striped' id='TableLotes' >
                                    <thead>
                                        <th width="20%">Objeto            </th>
                                        <th width="15%">Valor Estimado    </th>
                                        <th width="15%">Valor Contratado  </th>
                                        <th>            Diferença         </th>
                                        <th>            Empresa Vencedora </th>
                                        <th width="10%">Ações             </th>
                                    </thead>
                                    <tbody id='TbodyLotes'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <a href="#view_novo_lote" data-toggle="modal" id="AbrirNovoLote" class="btn btn-small btn-success alert-tooltip" title="Adicionar Lote"><i class="icon-plus"></i> Adicionar Lotes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="fases">
        <div class="span12">
            <div class="widget-header widget-header-small"><h5><?php __('Fases'); ?></h5></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="listaFases">
                                <table class='table table-bordered table-striped' id='TableLotes' >
                                    <thead>
                                        <th width="20%">Data               </th>
                                        <th>            Fase Atual         </th>
                                        <th width="10%">Ações              </th>
                                    </thead>
                                    <tbody id='TbodyFases'>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <a href="#view_nova_fase" data-toggle="modal" id="AbrirNovaFase" class="btn btn-small btn-success alert-tooltip" title="Adicionar Fase"><i class="icon-plus"></i> Adicionar Fases</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix" >
        <div class="pull-left btn-group">
            <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
        </div>
    </div>

    <div id="view_novo_lote" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Novo <?php __('Lote'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_lote">
        </div>
    </div>

    <div id="view_edit_lote" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Alterar <?php __('Lote'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="edit_lote">
        </div>
    </div>

    <div id="view_nova_fase" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;height: 600px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova <?php __('Fase'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="add_fase">
        </div>
    </div>

    <div id="view_edit_fase" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;height: 600px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Alterar <?php __('Fase'); ?></h3>
        </div>
        <div class="modal-body-iframe" id="edit_fase">
        </div>
    </div>
</div>

<script>
    $("#NuLicitacao").mask('aa 999/9999');
    loadTableLotes(<?php echo $this->params['pass'][0]; ?> , 'false');
    loadTableFases(<?php echo $this->params['pass'][0]; ?>);
    loadValorContratado(<?php echo $this->params['pass'][0]; ?> , 'false');

    function loadValorContratado( licitacao , atualizaLicitacao ){
        $.ajax({
            type:"POST",
            url: "<?php echo $this->Html->url(array('controller' => 'lct_lotes', 'action' => 'loadValorContratado')); ?>",
            data:{
                'lic': licitacao
            },
            success: function( data ){
                var valor = data.replace("\"","").replace("\"","");
                $("#ValorContratado").val( valor );
                calculaDiferenca();
                if( atualizaLicitacao == 'true' )
                {
                    atualizarLicitacao( 'false' );
                }
            }
        });
    }

    function atualizarLicitacao( msg )
    {
        var modalidadeLicitacao = $("#ModalidadeLicitacao").val();
        var objeto = $("#Objeto").val();
        var valorEstimado = $("#ValorEstimado").val();
        var valorContratado = $("#ValorContratado").val();

        var diferenca = $("#Diferenca").val();
        if(diferenca == '')
        {
            diferenca = 0.0;
        }

        var modalidadeProcesso = $("#ModalidadeProcesso").val();
        modalidadeProcesso = modalidadeProcesso.replace(/[^\d]+/g,'');

        var nuLicitacao = $("#NuLicitacao").val();

        var dataLicitacao = $("#DataLicitacao").val();
        dataLicitacao = dataLicitacao.substr(6,4) + '-' + dataLicitacao.substr(3,2) + '-' + dataLicitacao.substr(0,2);

        var horaLicitacao = $("#HoraLicitacao").val();

        var prazoExecucao = $("#PrazoExecucao").val();
        var faseAtual = $("#FaseAtual").val();

        var subJudice = 0;
        if( document.getElementById("SubJudice").checked ){
            subJudice = 1;
        }

        $.ajax({
            type:"POST",
            url: "<?php echo $this->Html->url(array('controller' => 'lct_licitacoes', 'action' => 'editLicitacao')); ?>",
            data:{
                'id': <?php echo $this->params['pass'][0]; ?>,
                'modalidade_licitacao': modalidadeLicitacao,
                'objeto': objeto,
                'valor_estimado': valorEstimado,
                'valor_contratado': valorContratado,
                'diferenca': diferenca,
                'modalidade_processo': modalidadeProcesso,
                'nu_licitacao': nuLicitacao,
                'dt': dataLicitacao,
                'hora': horaLicitacao,
                'prazo_execucao': prazoExecucao,
                'fase_atual': faseAtual,
                'sub_judice': subJudice
            },
            success: function(){
                if( msg != 'false' )
                {
                    alert("Licitação Atualizada com Sucesso!");
                }
            }
        });
    }

    function calculaDiferenca()
    {
        var valor_estimado = $("#ValorEstimado").val();
        var valor_contratado = $("#ValorContratado").val();

        valor_estimado = valor_estimado.replace(/\./g,'').replace(',','.');
        valor_contratado = valor_contratado.replace(/\./g,'').replace(',','.');

        if( valor_contratado != null && valor_contratado != "0.00" && valor_contratado != '' && valor_estimado != null && valor_estimado != '0.00' && valor_estimado != '' )
        {
            var diferenca = ((parseFloat(valor_estimado) - parseFloat(valor_contratado))/parseFloat(valor_estimado))*100;
            diferenca = diferenca.toFixed(2);
            $("#Diferenca").val(diferenca);
        }else{
            $("#Diferenca").val(0.00);
        }
    }

    function loadLotesEFases()
    {
        var modalidadeLicitacao = $("#ModalidadeLicitacao").val();
        var objeto = $("#Objeto").val();
        var valorEstimado = $("#ValorEstimado").val();
        var valorContratado = $("#ValorContratado").val();

        var diferenca = $("#Diferenca").val();
        if(diferenca == '')
        {
            diferenca = 0.0;
        }

        var modalidadeProcesso = $("#ModalidadeProcesso").val();
        modalidadeProcesso = modalidadeProcesso.replace(/[^\d]+/g,'');

        var nuLicitacao = $("#NuLicitacao").val();

        var dataLicitacao = $("#DataLicitacao").val();
        dataLicitacao = dataLicitacao.substr(6,4) + '-' + dataLicitacao.substr(3,2) + '-' + dataLicitacao.substr(0,2);

        var horaLicitacao = $("#HoraLicitacao").val();

        var prazoExecucao = $("#PrazoExecucao").val();
        var faseAtual = $("#FaseAtual").val();

        var subJudice = 0;
        if( document.getElementById("SubJudice").checked ){
            subJudice = 1;
        }

        $.ajax({
            type:"POST",
            url: "<?php echo $this->Html->url(array('controller' => 'lct_licitacoes', 'action' => 'addLicitacao')); ?>",
            data:{
                'modalidade_licitacao': modalidadeLicitacao,
                'objeto': objeto,
                'valor_estimado': valorEstimado,
                'valor_contratado': valorContratado,
                'diferenca': diferenca,
                'modalidade_processo': modalidadeProcesso,
                'nu_licitacao': nuLicitacao,
                'dt': dataLicitacao,
                'hora': horaLicitacao,
                'prazo_execucao': prazoExecucao,
                'fase_atual': faseAtual,
                'sub_judice': subJudice
            },
            success: function(){
                var form = document.getElementById("FormLicitacao");
                var elements = form.elements;
                for (var i = 0, len = elements.length; i < len; ++i) {
                    elements[i].readOnly = true;
                }
                document.getElementById("ModalidadeLicitacao").disabled = true;
                document.getElementById("SubJudice").disabled = true;
                $("#Limpar").hide();
                $("#Salvar").hide();
            }
        });
    }

    function loadTableLotes( licitacao , msg ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'lct_lotes', 'action' => 'listar') )?>" + '/' + licitacao, null, function(data){
            var i;
            $("#TbodyLotes tr").remove();
            var row = '';

            for( i = 0; i < data.lotes.length; i++ ){
                if( data.lotes[i]['LctLotes']['valor_estimado'] != null ){
                    var est = data.lotes[i]['LctLotes']['valor_estimado'];
                }else{
                    var est = '0.00';
                }

                if( data.lotes[i]['LctLotes']['valor_contratado'] != null ){
                    var cnt = data.lotes[i]['LctLotes']['valor_contratado'];
                }else{
                    var cnt = '0.00';
                }

                var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;
                if(est.match(decimal)){
                    var estimado = est.replace(/[^\d]+/g,'');
                    estimado = formatReal(estimado);
                }else{
                    var estimado = formatReal(data.lotes[i]['LctLotes']['valor_estimado'] + '00');
                }

                if(cnt.match(decimal)){
                    var contratado = cnt.replace(/[^\d]+/g,'');
                    contratado = formatReal(contratado);
                }else{
                    var contratado = formatReal(data.lotes[i]['LctLotes']['valor_contratado'] + '00');
                }

                row += '<tr> <td> ' + data.lotes[i]['LctLotes']['objeto'] + ' </td> <td id="valor" width="20%"> R$ '+ estimado +'</td><td id="valor" width="20%"> R$ '+ contratado +'</td><td width="10%">'+data.lotes[i]['LctLotes']['diferenca']+'% </td><td>'+data.lotes[i]['LctLotes']['empresa_vencedora']+'</td>' +
                '<td class="actions" width="10%">' +
                '<div class="btn-group acoes">' +
                '<a href="#view_edit_lote" onclick="abrirEditLote('+ data.lotes[i]['LctLotes']['id'] +')" data-toggle="modal" class="btn btn-small btn-default AbrirEditLote"><i class="icon-edit"></i></a>' +
                '<a onclick="excluirLote('+data.lotes[i]['LctLotes']['id']+','+ licitacao +')" class="btn btn-small btn-danger"><i class="icon-remove"></i></a>' +
                '</div>' +
                '</td></tr>';
            }
            $("#TbodyLotes").append(row);
            $("#valor").maskMoney({thousands:'.', decimal:','});
            if( msg != 'false' )
            {
                msg = 'true';
            }
            console.log(msg);
            loadValorContratado( licitacao , msg );
        });
    }

    function loadTableFases( licitacao ){
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'lct_fases', 'action' => 'listar') )?>" + '/' + licitacao, null, function(data){
            var i;
            $("#TbodyFases tr").remove();
            var row = '';
            for( i = 0; i < data.fases.length; i++ ){
                row += '<tr><td width="20%">' + data.fases[i]['LctFases']['data'] + '</td><td>' + data.fases[i]['LctFases']['descricao'] + '</td>' +
                '<td class="actions" width="10%">' +
                '<div class="btn-group acoes">' +
                '<a href="#view_edit_fase" onclick="abrirEditFase('+ data.fases[i]['LctFases']['id'] +')" data-toggle="modal" class="btn btn-small btn-default AbrirEditFase"><i class="icon-edit"></i></a>' +
                '<a onclick="excluirFase('+data.fases[i]['LctFases']['id']+','+ licitacao +')" class="btn btn-small btn-danger"><i class="icon-remove"></i></a>' +
                '</div>' +
                '</td></tr>';
            }
            $("#TbodyFases").append(row);
        });
    }

    function excluirLote( id , licitacao )
    {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->Html->url(array('controller' => 'lct_lotes', 'action' => 'removeLote')); ?>",
            data: {
                'id': id
            },
            success: function(){
                loadTableLotes( licitacao );
            }
        });
    }

    function excluirFase( id , licitacao )
    {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->Html->url(array('controller' => 'lct_fases', 'action' => 'removeFase')); ?>",
            data: {
                'id': id
            },
            success: function(){
                loadTableFases( licitacao );
            }
        });
    }

    $("#AbrirNovoLote").bind('click', function(e) {
        var url_fn = "<?php echo $this->base; ?>/lct_lotes/iframe/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                'lic': <?php echo $this->params['pass'][0]; ?>
            },
            success:function(result){
                $('#add_lote').html("");
                $('#add_lote').append(result);
            }
        })
    });

    $("#AbrirNovaFase").bind('click', function(e) {
        var url_fn = "<?php echo $this->base; ?>/lct_fases/iframe/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                'lic': <?php echo $this->params['pass'][0]; ?>
            },
            success:function(result){
                $('#add_fase').html("");
                $('#add_fase').append(result);
            }
        })
    });

    function abrirEditFase( id )
    {
        console.log(id);
        var url_fn = "<?php echo $this->base; ?>/lct_fases/iframe_edit/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                'id': id
            },
            success:function(result){
                $('#edit_fase').html("");
                $('#edit_fase').append(result);
            }
        })
    }

    function abrirEditLote( id )
    {
        var url_fn = "<?php echo $this->base; ?>/lct_lotes/iframe_edit/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                'id': id
            },
            success:function(result){
                $('#edit_lote').html("");
                $('#edit_lote').append(result);
            }
        })
    }

    $(".AbrirEditFase").click(function() {
        console.log("AbrirEditFase");
        var licitacao = "<?php echo $this->params['pass'][0]; ?>";
        var url_fn = "<?php echo $this->base; ?>/lct_fases/iframeEdit/";
        $.ajax({
            type:"POST",
            url:url_fn,
            data:{
                'modal': true
            },
            success:function(result){
                loadTableFases( licitacao );
            }
        })
    });

    $('#Voltar2').click(function(event){
        event.preventDefault();
        parent.closeIFrame();
    });

    $("#ValorEstimado").maskMoney({thousands:'.', decimal:','});
    $("#ValorContratado").maskMoney({thousands:'.', decimal:','});

    $('input:text').bind("keydown", function(e) {
        var n = $("input:text").length;
        if (e.which == 13)
        { //Enter key
            e.preventDefault();
            var nextIndex = $('input:text').index(this) + 1;
            if(nextIndex < n)
                $('input:text')[nextIndex].focus();
            else
            {
                $('input:text')[nextIndex-1].blur();
            }
        }
    });

</script>
