<?php $usuario = $this->Session->read ('usuario'); ?>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Licitação'); ?></h1></div>
            <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'lct_man_i') ) { ?>
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><span class="required" title="Required"></span> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'lct_licitacoes', 'action' => 'preImprimir')); ?>" data-toggle="modal" class="btn btn-small btn-info" title="Imprimir Relatório"><i class="icon-print icon-white"></i>Relatório Geral</a>
            <a href="<?php echo $this->Html->url(array('controller' => 'lct_licitacoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Licitação"><i class="icon-plus icon-white"></i> Nova <?php __('Licitação'); ?></a>
          </div>
        </div>
      <?php } ?>

        <?php echo $this->Form->create('LctLicitacoes', array('url' => array('controller' => 'lct_licitacoes', 'action' => 'index')));?>

        <div class="row-fluid">
            <div class="span3">
                <div class="controls">
                    <?php
                        echo $this->Form->input('id', array('class' => 'input-xlarge','type' => 'text','label' => __('Item', true),'maxLength'=>'50'));
                    ?>
                </div>
            </div>
            <div class="span2">
                <div class="controls">
                    <?php
                    echo $this->Form->input('dt_inicio', array(
                            'before' => '<div class="input-append date datetimepicker">',
                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                            'class' => 'input-small','label' => 'Data de Início', 'type'=>'text')
                    );
                    ?>
                </div>
            </div>
            <div class="span2">
                <div class="controls">
                    <?php
                    echo $this->Form->input('dt_final', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                        'class' => 'input-small','label' => 'Data Final', 'type'=>'text'));
                    ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
                <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th>Item</th>
		<th>Modalidade de Licitação</th>
		<th>Objeto</th>
		<th>Processo</th>
		<th>Data</th>
		<th>Valor Estimado</th>
		<th>Valor Contratado</th>
		<th>Diferença</th>
		<th>Prazo de Execução</th>
		<th>Fase Atual</th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($licitacoes as $licitacao):

        $fase_atual = '';
        foreach ( $fases as $fase ) {
            if( $fase['LctFases']['co_licitacao'] == $licitacao['LctLicitacoes']['id'] ) {
                $fase_atual = $fase['LctFases']['descricao'];
            }
        }

        $class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}

        $date = ($licitacao['LctLicitacoes']['dt'] != "") ? ($licitacao['LctLicitacoes']['dt'] . " às " . $licitacao['LctLicitacoes']['hora']) : '---';
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $licitacao['LctLicitacoes']['id']; ?></td>
		<td><?php echo $licitacao['LctLicitacoes']['modalidade_licitacao']; ?></td>
		<td><?php echo $licitacao['LctLicitacoes']['objeto']; ?></td>
		<td id="ModalidadeProcesso"><?php echo substr($licitacao['LctLicitacoes']['modalidade_processo'],0,3) . '.' . substr($licitacao['LctLicitacoes']['modalidade_processo'],3,3) . '.' . substr($licitacao['LctLicitacoes']['modalidade_processo'],6,3) . '/' . substr($licitacao['LctLicitacoes']['modalidade_processo'],9,4); ?></td>
		<td><?php echo $date; ?></td>
		<td>R$ <?php echo number_format($licitacao['LctLicitacoes']['valor_estimado'],2,',','.'); ?></td>
		<td>R$ <?php echo number_format($licitacao['LctLicitacoes']['valor_contratado'],2,',','.'); ?></td>
		<td><?php echo $licitacao['LctLicitacoes']['diferenca']; ?>%</td>
		<td><?php echo $licitacao['LctLicitacoes']['prazo_execucao']; ?></td>
		<td><?php echo $fase_atual; ?></td>
		<td class="actions">
			<div class="btn-group acoes">
				<?php
                    echo $this->element(
                        'actions4', array(
                            'id' => $licitacao['LctLicitacoes']['id'],
                            'class' => 'btn',
                            'local_acao' => 'lct_licitacoes/index'
                        )
                    )
				?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>


