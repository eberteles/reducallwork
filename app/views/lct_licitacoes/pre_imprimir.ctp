<?php echo $this->Form->create('PreImprimir', array('url' => "/lct_licitacoes/imprimirRelatorioGeral/")); ?>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Relatório de Licitações'); ?></h1></div>
    <p>Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011</p>
    <br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Relatório de Licitações'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span3">
                        <div class="controls">
                            <?php
                                echo $this->Form->input('modalidade_licitacao', array('class' => 'input-xlarge', 'id' => 'ModalidadeLicitacao', 'label'=>__('Modalidade da Licitação', true), 'type' => 'select', 'empty' => 'TODAS', 'options' => array( 'CONCORRÊNCIA' => 'CONCORRÊNCIA', 'T. DE PREÇOS' => 'T. DE PREÇOS', 'CONVITE' => 'CONVITE', 'PRE-QUALIFICAÇÃO' => 'PRE-QUALIFICAÇÃO', 'P. ELETRÔNICO' => 'P. ELETRÔNICO', 'P. PRESENCIAL' => 'P. PRESENCIAL' )));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" formtarget="_blank" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Gerar </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

