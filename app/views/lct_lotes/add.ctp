<?php echo $this->Form->create('LctLotes', array('url' => "/lct_lotes/add/" . $modal)); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Lote'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span4">
                        <?php
                            echo $this->Form->input('objeto', array('class' => 'input-xlarge', 'label'=>'Objeto', 'id' => 'Objeto', 'type' => 'textarea', 'style' => 'margin: 0px 0px 10px; width: 400px; height: 120px; max-width: 300px; max-height: 120px;', 'maxlength' => 255));
                        ?>
                    </div>
                    <div class="span4">
                        <?php
                            echo $this->Form->input('valor_estimado', array('class' => 'input-xlarge', 'onblur' => 'calculaDiferenca()', 'id' => 'ValorEstimado', 'label' => 'Valor Estimado'));
                            echo $this->Form->input('valor_contratado', array('class' => 'input-xlarge', 'onblur' => 'calculaDiferenca()', 'id' => 'ValorContratado', 'label' => 'Valor Contratado'));
                        ?>
                    </div>
                    <div class="span4">
                        <?php
                            echo $this->Form->input('diferenca', array('class' => 'input-xlarge', 'id' => 'Diferenca', 'label' => 'Diferença', 'readonly' => true));
                            echo $this->Form->input('empresa_vencedora', array('class' => 'input-xlarge', 'label' => 'Empresa Vencedora'));
                            echo $this->Form->input('co_licitacao', array('class' => 'input-xlarge', 'type' => 'hidden', 'value' => $licitacao));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
        </div>
    </div>
</div>

<script>
    $("#ValorEstimado").maskMoney({thousands:'.', decimal:','});
    $("#ValorContratado").maskMoney({thousands:'.', decimal:','});

    function calculaDiferenca()
    {
        var valor_estimado = $("#ValorEstimado").val();
        var valor_contratado = $("#ValorContratado").val();

        valor_estimado = valor_estimado.replace(/\./g,'').replace(',','.');
        valor_contratado = valor_contratado.replace(/\./g,'').replace(',','.');
        console.log(valor_estimado);
        console.log(valor_contratado);

        if( valor_contratado != null && valor_contratado != "0.00" && valor_contratado != '' && valor_estimado != null && valor_estimado != '0.00' && valor_estimado != '' )
        {
            console.log("Entrou aqui");
            var diferenca = ((parseFloat(valor_estimado) - parseFloat(valor_contratado))/parseFloat(valor_estimado))*100;
            diferenca = diferenca.toFixed(2);
            $("#Diferenca").val(diferenca);
            $("#Diferenca").value = diferenca;
        }else{
            $("#Diferenca").val(0.00);
            $("#Diferenca").value = 0.00;
        }
    }
</script>
