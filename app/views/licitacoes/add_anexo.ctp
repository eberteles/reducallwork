<?php 
    $usuario = $this->Session->read ('usuario');
    echo $this->Html->script( 'inicia-datetimepicker' ); 
?>
<div class="aditivos form">
<?php echo $this->Form->create('Anexo', array('type' => 'file', 'url' => "/licitacoes/add_anexo/$coLicitacao"));?>
    
	<div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Anexo - <b>Campos com * são obrigatórios.</p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'licitacoes', 'action' => 'anexos', $coLicitacao)); ?>" class="btn btn-small btn-primary" title="Listar Anexos">Listagem</a>
            </div>
        </div>
    
       <div class="row-fluid">
                        
            <div class="span12 ">
                <div class="widget-header widget-header-small"><h4>Novo Anexo</h4></div>
             
            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span4">
                  <?php 
                        echo $this->Form->input('ds_anexo', array('label' => 'Descrição', 'size' => '100'));
                        echo $this->Form->input('conteudo', array('type' => 'file', 'label' => 'Anexo') );
                    ?>
	           </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" id="salvarAnexo" title="Salvar Anexo"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm"> Limpar</button>
          <button rel="tooltip" title="Voltar" class="btn btn-small voltarcss"> Voltar</button>
       </div>
    </div>

</div>

    <div id="view_nova_pasta" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova Pasta</h3>
        </div>
        <div class="modal-body-iframe" id="add_pasta">
        </div>
    </div>

    <div id="aguarde" class="modal fade" data-backdrop="static" style="display: none;background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
            <br> <br> <br> <br> <br>&nbsp;<br>
            <center>Aguarde...</center>
            <br>
    </div>
<script type="text/javascript">

    $('#LimparForm').click(function () {

        $('#AnexoCoAnexoPasta').val('');
        $('#AnexoDtAnexo').val('');
        $('#AnexoDsAnexo').val('');
    });


</script>
<?php echo $this->Html->scriptStart() ?>

    $('#salvarAnexo').click(function(){
        $('#aguarde').modal();
    });
    
    $('#AnexoConteudo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            thumbnail:false, //| true | large,
            //whitelist:'gif|png|jpg|jpeg',
            //blacklist:'exe|php'
    });
    

    $('a.remove').on('click', function() {
        $('#AnexoConteudo').ace_file_input('reset_input');
    });
        
<?php echo $this->Html->scriptEnd() ?>