<?php
$usuario = $this->Session->read ('usuario');
?>

<div class="anexos index">

    <?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/add') ) { ?>
        <div class="actions">
            <div class="acoes-formulario-top clearfix" >
                <div class="pull-left span6">
                    <?php
                    if($modulo == "" || $modulo == "padrao") {
                        echo $this->Form->input('tp_documento', array('type' => 'select', 'div'=>false, 'label'=>false, 'empty' => 'Filtrar por Tipo de Documento...', 'options' => $tpAnexo, 'value' => $tpDocumento));
                        echo '&nbsp;';
                        echo $this->Form->input('ft_documento', array('label'=>false, 'placeholder'=>'Filtrar pelo nome do Documento...', 'value' => 'note', 'id'=>'pesquisaAnexo', 'div'=>false));
                        echo '&nbsp;';
                        echo '<button id="submitPesquisa" class="btn btn-small btn-primary" style="margin-bottom: 10px"><i class="icon-search"></i> Pesquisar</button>';
                    }
                    ?>
                </div>
                <div class="pull-left btn-group span12">
                    <a href="<?php echo $this->Html->url(array('controller' => 'licitacoes', 'action' => 'add_anexo', $coLicitacao)); ?>" class="btn btn-small btn-primary"><i class="icon-cloud-upload"></i> Adicionar Documento</a>
                    <!--<a id="gerenciar_pastas" href="#view_dominio" data-toggle="modal" class="btn btn-small btn-primary"><i class="icon-folder-open-alt"></i> Gerenciar Pastas</a>-->
                </div>
            </div>

        </div>

        <div id="view_dominio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="tituloAbaDominio"></h4>
            </div>
            <div class="modal-body" id="list_dominio">
            </div>
        </div>

        <?php
    }

    $permissaoEditar    = $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/edit');
    $permissaoExcluir   = $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'anexos/delete');

    $i = 0;
    echo $this->Html->scriptStart();
    $nomePastaPai   = 'tree_anexo';
    echo 'var ' . $nomePastaPai . ' = { ';

    // Varrendo Pastas
    // foreach ($anexos_pastas as $pasta):
    //     $i++;
    //     echo $this->element( 'imprimir_pasta_anexo', array( 'pasta'=>$pasta, 'nomePastaPai'=>$nomePastaPai, 'permissaoEditar'=>$permissaoEditar, 'permissaoExcluir'=>$permissaoExcluir ) );
    // endforeach;
    // Varrendo Arquivos sem Pasta
    foreach ($anexos as $anexo):
        $i++;
        echo $this->element( 'imprimir_anexo_licitacoes', array( 'anexo'=>$anexo, 'nomePastaPai'=>$nomePastaPai, 'permissaoEditar'=>true, 'permissaoExcluir'=>true ) );
    endforeach;
    echo ' }; ';
    echo $this->Html->scriptEnd();

    // if( Configure::read('App.config.resource.certificadoDigital') ) {
    //     echo $this->element( 'modal_assinatura_digital' );
    // }
    ?>
    <div class="row-fluid">
        <div class="widget-box span12">
            <div class="widget-header widget-header-small">
                <h5>Documentos</h5>
            </div>

            <div class="widget-body">
                <div class="widget-main padding-8">
                    <?php
                    if(count($anexos) < 1) {
                        echo 'Não existem registros a serem exibidos.';
                    } else {
                        ?>
                        <div id="treeDocumentos" class="tree" style="height: 360px;"></div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script( 'fuelux/fuelux.tree.min' );
?>

<script type="text/javascript">
    var DataSourceTree = function(options) {
        this._data 	= options.data;
        this._delay = options.delay;
    };

    DataSourceTree.prototype.data = function(options, callback) {
        var self = this;
        var $data = null;

        if(!("name" in options) && !("type" in options)){
            $data = this._data;//the root tree
            callback({ data: $data });
            return;
        }
        else if("type" in options && options.type == "folder") {
            if("additionalParameters" in options && "children" in options.additionalParameters)
                $data = options.additionalParameters.children;
            else $data = {}//no data
        }

        if($data != null)//this setTimeout is only for mimicking some random delay
            setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);

        //we have used static data here
        //but you can retrieve your data dynamically from a server using ajax call
        //checkout examples/treeview.html and examples/treeview.js for more info
    };

    var treeDataSource = new DataSourceTree({data: tree_anexo});

    $('#treeDocumentos').on('loaded', function (evt, data) {
        $('.alert-tooltip').tooltip();
        $('[data-rel=popover]').popover({container:'body'});
    });

    $('#treeDocumentos').ace_tree({
        dataSource: treeDataSource ,
        loadingHTML:'<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
        'open-icon' : 'icon-folder-open',
        'close-icon' : 'icon-folder-close',
        'selectable' : false,
        'selected-icon' : null,
        'unselected-icon' : null
    });

    function abrirFrame(titulo, url_frame) {
        $('#tituloAbaDominio').html("");
        $('#tituloAbaDominio').append(titulo);
        $.ajax({
            type:"POST",
            url:url_frame,
            data:{
            },
            success:function(result){
                $('#list_dominio').html("");
                $('#list_dominio').append(result);
            }
        });
    }

    // $('#gerenciar_pastas, #gerenciar_pastas2').click(function(){
    //     abrirFrame("Gerenciar Pastas", "<?php //echo $this->base; ?>/anexos_pastas/iframe/<?php //echo $coLicitacao; ?>/<?php //echo $modulo; ?>");
    // });

    $('.alert-tooltip').tooltip();

    // $('#submitPesquisa').click(function(){
    //     var texto_pesquisa = $('#pesquisaAnexo').val();
    //     var id_selecionado = 0;
    //     if($("#tp_documento option:selected ").val() > 0){
    //         id_selecionado = $("#tp_documento option:selected ").val();
    //     }
    //     var url_filtro  = "<?php //echo $this->Html->url(array('controller' => 'anexos', 'action' => 'index', $coContrato, $modulo, $idModulo, )); ?>" + "/" + id_selecionado + '/0/' + texto_pesquisa;
    //     console.log(url_filtro);
    //     $(location).attr('href',url_filtro);
    // });

    // <?php
    // if ($indexarArquivo > 0 && $this->Modulo->pesquisaTextual) {
    // ?>
    // var url_id = "<?php //echo $this->base; ?>/anexos_indexacoes/index/<?php //echo $indexarArquivo; ?>";
    // $.ajax({
    //     type:"GET",
    //     url:url_id
    // });
    // <?php
    // }
    // ?>

    function abrirAnexo(coAnexo) {
        if( typeof parent.abrirAnexo == 'function' ) {
            parent.abrirAnexo(coAnexo);
        } else {
            abrirDocumento(coAnexo);
        }
    }

    function abrirDocumento(coAnexo) {
        var url_abrir = "<?php echo $this->base; ?>/anexos/lerArquivo/" + coAnexo + "/1";
        $('#view_dominio').modal();
        $('#list_dominio').html("");
        $('#tituloAbaDominio').html('Abrir Documento');
        $.ajax({
            type:"POST",
            url:"<?php echo $this->base; ?>/anexos/iframe_abrir/350",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data:{
                "data[url]": url_abrir
            },
            success:function(result){
                $('#list_dominio').html("Caso o arquivo não abra nesta Janela, verifique a área de Downloads do seu Navegador.");
                $('#list_dominio').append(result);
            }
        })
    }

    function excluirAnexo(coAnexo) {
        if( confirm('Tem certeza de que deseja excluir este registro ?') ) {
            var url_excluir = "<?php echo $this->base; ?>/licitacoes/delete_anexo/" + coAnexo + "/<?php echo $coLicitacao; ?>";
            $(location).attr('href',url_excluir);
        }
    }


</script>