<?
$usuario = $this->Session->read ('usuario');
echo $this->Form->create('Licitacao', array('url' => "/licitacoes/edit/$id". $modal));
echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Html->script( 'date' );

?>

<div class="row-fluid">

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha os campos abaixo para adicionar uma Licitação.</p>

        <div class="pull-right btn-group">
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span12 ">
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h4>
                        <?php __('Licitação'); ?>
                    </h4>
                </div>


                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span4">
                                <dl class="dl-horizontal">
                                    <?
                                    echo $this->Form->hidden('co_licitacao');

                                    //                             Numero licitacão
                                    echo $this->Form->input('nu_licitacao', array('class' => 'input-xlarge', 'label'=>'Nº Licitação', 'id'=>'nu_licitacao'));

                                    //                             Numero processo
                                    echo $this->Form->input('nu_processo', array('class' => 'input-xlarge', 'label'=>'Nº Processo', 'onkeyup'=>'somenteNumeros(this)', 'maxlength'=>'12' ));

                                    //                             Data publicação
                                    echo $this->Form->input('dt_publicacao', array(
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                        'class' => 'input-small','label' => 'Data de Abertura', 'type'=>'text'));

                                    //                            Data abertura
                                    echo $this->Form->input('dt_abertura', array(
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                        'class' => 'input-small','label' => 'Data de Abertura', 'type'=>'text'));

                                    //                            Prazo de execução
                                    echo $this->Form->input('ds_prazo_execucao', array('class' => 'input-xlarge', 'label'=>'Prazo de Execução', 'maxlength'=>'20' ));
                                    ?>
                                </dl>
                            </div>


                            <div class="span4">
                                <dl class="dl-horizontal">
                                    <?
                                    //                                Modalidade Licitação
                                    echo $this->Form->input('co_contratacao',      array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...',
                                        'label'=>__('Modalidade da Licitação', true), 'options' => $contratacao));

                                    //                                Tipo Licitação
                                    echo $this->Form->input('co_tipo_licitacao',      array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...',
                                        'label'=>__('Tipo de Licitação', true), 'options' => $tiposLicitacoes));

                                    //                                Valor estimado R$
                                    echo $this->Form->input('vl_estimado', array('class' => 'input-xlarge','label'=>'Valor estimado R$', 'maxlength' => 20, "id" => "vl_estimado", "onkeyup" => "diferenca()" ));

                                    //                                Valor contratado R$
                                    echo $this->Form->input('vl_contratado', array('class' => 'input-xlarge','label'=>'Valor contratado R$', 'maxlength' => 20, "id" => "vl_contratado" , "onkeyup" => "diferenca()"  ));

                                    //                                Diferença %/R$
                                    echo $this->Form->input('nu_diferenca', array('class' => 'input-xlarge','label'=>'Diferença %/R$', 'maxlength' => 20, 'id' => 'nu_diferenca', "onkeyup" => "diferenca()"));
                                    ?>
                                </dl>
                            </div>

                            <div class="span4">
                                <dl class="dl-horizontal">

                                    <?

                                    echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=> $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'options' => $fornecedor,
                                        'after' => $this->Print->getBtnEditCombo('Novo ' .  $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()), 'AbrirNovoFornecedor', '#view_novo_fornecedor', $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/add')) ) );


                                    //                                Situação
                                    echo $this->Form->input('co_situacao',      array('class' => 'input-xlarge','type' => 'select', 'empty' => 'Selecione...',
                                        'label'=>__('Situação', true), 'options' => $situacao));

                                    //                                Objeto
                                    echo $this->Form->input('ds_objeto', array('class' => 'input-xlarge','label'=>'Objeto','type' => 'texarea', 'cols'=>'42', 'rows'=>'6',
                                        'onKeyup'=>'$(this).limit("1500","#charsLeft3")',
                                        'after' => '<br><span id="charsLeft3">1500</span> caracteres restantes.'));
                                    ?>

                                </dl>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <div class="form-actions">
                <div class="btn-group">
                    <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar processo"> Salvar</button>
                    <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                    <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                </div>
            </div>
        </div>

        <div id="view_novo_fornecedor" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Novo <?php echo $this->Print->getLabelFornecedor($this->Modulo->isContratoExterno()); ?></h3>
            </div>
            <div class="modal-body-iframe" id="add_fornecedor">

            </div>
        </div>
    </div>


    <script type="text/javascript">
        jQuery("#nu_licitacao").mask("9999/9999");
        $("#vl_estimado").maskMoney({thousands:'.', decimal:'.'});
        $("#vl_contratado").maskMoney({thousands:'.', decimal:'.'});
        $("#nu_diferenca").maskMoney({thousands:'.', decimal:'.'});

        function somenteNumeros(num) {
            var er = /[^0-9.]/;
            er.lastIndex = 0;
            var campo = num;
            if (er.test(campo.value)) {
                campo.value = "";
            }
        }
        $('.upload-arquivo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            thumbnail:false

        });
        $('a.remove').on('click', function(e) {
            $(e.target).closest('div').find('input').ace_file_input('reset_input');
        });

        function diferenca() {
            var valEstimado = $("#vl_estimado").val().replace(/[\.]/g, "");
            var splitEstimado = valEstimado.split(",");
            var estimado = splitEstimado[0]+"."+splitEstimado[1];

            var valContratado = $("#vl_contratado").val().replace(/[\.]/g, "");
            var splitContratado = valContratado.split(",");
            var contratado = splitContratado[0]+"."+splitContratado[1];

            if(valContratado != null && valContratado != "" && valEstimado != null && valEstimado != "" ){
                var newContratado = parseFloat(contratado);
                var newEstimado = parseFloat(estimado);
                var total = newContratado - newEstimado;
                $("#nu_diferenca").maskMoney('mask', total);
            }
        }
        $("#AbrirNovoFornecedor").bind('click', function(e) {
            var url_fn = "<?php echo $this->base; ?>/fornecedores/iframe/";
            $.ajax({
                type:"POST",
                url:url_fn,
                data:{
                },
                success:function(result){
                    $('#add_fornecedor').html("");
                    $('#add_fornecedor').append(result);
                }
            })
        });

        function atzComboFornecedor(co_fornecedor) {
            $.getJSON("<?php echo $this->Html->url(array ('controller' => 'fornecedores', 'action' => 'listar') )?>", null, function(data){
                var options = '<option value="">Selecione..</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });

                $("select[id^=LicitacaoCoFornecedor]:enabled").html(options);
                $("[id^=LicitacaoCoFornecedor]:enabled option[value=" + co_fornecedor + "]").attr("selected", true);
                $("[id^=LicitacaoCoFornecedor]:enabled").trigger("chosen:updated");
            });
        }
    </script>