<?php
echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Html->script( 'date' );

$usuario = $this->Session->read('usuario');
if (! $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/index')):
    echo "Acesso negado.";
else:
?>
<div class="row-fluid">
	<div class="page-header position-relative">
		<h1>Licitações</h1>
    </div>
    <p>
        Lei de Acesso à informação - LAI,Lei Federal n° 12.527, de 18 de novembro de 2011
    </p>
    <?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/add')): ?>
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">
                Preencha um dos campos para fazer a pesquisa
            </p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'licitacoes', 'action' => 'add')); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary"
                   title="Novo Usuário"><i class="icon-plus icon-white"></i>Cadastrar Nova Licitação</a>
                
                <a href="<?php echo $this->Html->url(array('controller' => 'licitacoes', 'action' => 'relatorios')); ?>"
                data-toggle="modal" class="btn btn-small btn-primary"
                title="Relatórios"><i class="icon-search icon-white"></i>Relatórios</a>
            </div>
        </div>
    <?php endif; ?>

    <div class="row-fluid">
        <?php echo $this->Form->create('Licitacao', array(
        'type' => 'GET',
    )); ?>
        <!-- número de licitação -->
		<div class="span2">
    		<?php echo $this->Form->input('nu_licitacao', array(
                'class' => 'input-normal',
                'label' => 'Nº de Licitação',
            )); ?>
		</div>

        <!-- data de publicação -->
		<div class="span1" style="smargin-left:60px">
        	<?php
                echo $this->Form->input('dt_publicacao', array(
                    'class' => 'input-small',
                    'type' => 'text',
                    'label'=>__('Data Publicação', true),
                    'before' => '<div class="input-append date datetimepicker">',
                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                    'data-format' => 'dd/MM/yyyy',
                    'mask'=>'99/99/9999',
                    'require' => false
                    )
                );
            ?>
		</div>

        <!-- modalidade de licitação -->
		<div class="span2" style="margin-left:60px">
        	<?php echo $this->Form->input('co_contratacao', array(
                'type' => 'select',
                'class' => 'input-normal chosen-select',
                'empty' => 'Selecione...',
                'label'=> 'Modalidade de Licitação',
                'options' => $contratacoes,
            )); ?>
		</div>

        <!-- situação -->
        <div class="span2">
        	<?php echo $this->Form->input('co_situacao', array(
                'type' => 'select',
                'class' => 'input-normal chosen-select',
                'empty' => 'Selecione...',
                'label'=>'Situação',
                'options' => $situacoes
            )); ?>
		</div>
	</div>

	<div class="form-actions">
		<div class="btn-group">
			<button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
			<button rel="tooltip" type="button" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small">Limpar</button>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>

	<!--<?php //if (count($licitacoes) > 0) : ?>-->
    <table id="tableLicitacoes" cellpadding="0" cellspacing="0" style="background-color: white" class="table table-hover table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo 'Nº da Licitação';?></th>
				<th><?php echo 'Objeto';?></th>
				<th><?php echo 'Modalidade da Licitação';?></th>
			    <th><?php echo 'Data de Publicação';?></th>
				<th><?php echo 'Valor estimado R$';?></th>
                <th><?php echo 'Valor contratado R$';?></th>
                <th><?php echo 'Diferença %/R$';?></th>
                <th><?php echo 'Prazo de Execução';?></th>
                <th><?php echo 'Empresa vencedora';?></th>
                <th><?php echo 'Situção';?></th>
                <th><?php echo 'Data de Abertura';?></th>
				<th class="actions"><?php __('Ações');?></th>
			</tr>
		<thead>
    	<tbody>
            <?php if (count($licitacoes) > 0) {
                foreach ($licitacoes as $licitacao) : ?>
                <tr>
                    <td><?php echo $this->Print->licitacao($licitacao['Licitacao']['nu_licitacao']) ?></td>
                    <td><?php echo $this->Print->printHelp($licitacao['Licitacao']['ds_objeto'], $licitacao['Licitacao']['ds_objeto'], 50) ?></td>
                    <td><?php echo $licitacao['Contratacao']['ds_contratacao'] ?></td>
                    <td><?php echo $licitacao['Licitacao']['dt_publicacao'] ?></td>
                    <td><?php echo $this->Formatacao->moeda($licitacao['Licitacao']['vl_estimado']) ?></td>
                    <td><?php echo $this->Formatacao->moeda($licitacao['Licitacao']['vl_contratado']) ?></td>
                    <td><?php echo $licitacao['Licitacao']['nu_diferenca'] ?></td>
                    <td><?php echo $licitacao['Licitacao']['ds_prazo_execucao'] ?></td>
                    <td><?php echo $licitacao['Fornecedor']['no_razao_social'] ?></td>
                    <td><?php echo $licitacao['Situacao']['ds_situacao'] ?></td>
                    <td><?php echo $licitacao['Licitacao']['dt_abertura'] ?></td>
                    <td class="actions"><?php $id = $licitacao['Licitacao']['co_licitacao']; ?>
                        <div class="btn-group acoes">	
                            <!--<a class="btn btn-primary" href="#view_pastas"><i class="icon-upload"></i></a>-->
                            <a class="modalAnexoLicitacao btn btn-primary" href="#view_anexos" data-toggle="modal" titulo-aba="Suporte Documental" data-target="#view_anexos" data-id="<?php echo $licitacao['Licitacao']['co_licitacao']; ?>"><i class="yellow icon-folder-open"></i></a>
                            <?php
                                echo $this->element('actions2', array('id' => $licitacao['Licitacao']['co_licitacao'], 'class' => 'btn', 'local_acao' => 'licitacoes/index'));
                            ?>
                                
                        </div>
                    </td>
                </tr>
                
            <?php endforeach;
                } else {
                     echo '<tr><td colspan=12>Não existem registros a serem exibidos.</td></tr>';
                } ?>
    	</tbody>
    </table>
    <!--<?php //endif ?>-->
	<!-- <p><?php
        // echo $this->Paginator->counter(array(
        // 'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
    // ))
    ?></p>

	<div class="pagination">
		<ul>
        <?php //echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php //echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php //echo //$this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
	</div> -->
</div>
<?php endif ?>

<!-- modal de documentos -->
<div id="view_anexos" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="">Documentos</h3>
    </div>
    <div class="modal-body" id="contentDoc">
    </div>
</div>


<script>
$().ready(function () {
    $('#tableLicitacoes').on('click', 'a.modalAnexoLicitacao', function() {    
        var coLicitacao = $(this).data('id'),
            url = "<?php echo $this->base; ?>/licitacoes/iframe/" + coLicitacao ;
            // console.log(url);debugger; 
        abrirFrame(
            "Suporte Documental: " + $(this).attr('titulo-aba'), 
            url
        );
    });
});

function abrirFrame(titulo, url_frame) {
    var tit = titulo || null;
    // $('#tituloAbaDominio').html("");
    // $('#tituloAbaDominio').append(titulo);
    $.ajax({
        type:"POST",
        url:url_frame,
        data:{},
        success:function(result){
            console.log(result);
            $('#contentDoc').html("");
            $('#contentDoc').append(result);
        }
    });
}
</script>