<?
if($tipoRelatorio  == "C"){
    $tipo = "Contratos";
}
if($tipoRelatorio  == "L"){
    $tipo = "Licitações";
}
if($tipoRelatorio  == "E"){
    $tipo = "Empresa Suspensas";
}

?>

<div class="actions">
    <div class="acoes-formulario-top clearfix" >
        <div class="pull-left btn-group span12">
        <button id="btnImprimir" url="<?php echo $this->base . '/' . $url ?>" class="btn btn-normal btn-default"><i class="icon icon-print"></i> EXPORTAR PARA PDF</button>
        </div>
    </div>
</div>
<div class="center">
    <h1>Tabela de <?php echo $tipo; ?></h1>
    <p>(Lei de Acesso à Informação - LAI, Lei Federal nº 12.527, de 18 de novembro de 2011)</p>
</div>

<?php echo $this->Form->create(null, array(
    'type' => 'GET',
    'action' => 'relatorios'
)) ?>
<div class="row-fluid">
    <div class="span2">
        <?php echo $this->Form->input('tipo_relatorio', array(
            'class' => 'input-normal',
            'type' => 'select',
            'label'=>'Tipo de Relatório',
            'options' => array(
                'C' => 'Contratos',
                'L' => 'Licitações',
                'E' => 'Empresas Suspensas'
            ),
             'id'=> 'tipoRelatorio',
            'empty' => 'Selecione...'
            )
        ); ?>
    </div>

    <div class="span2">
        <?php echo $this->Form->input('palavra_chave', array(
            'class' => 'input-normal',
            'type' => 'text',
            'label'=>'Palavra Chave',
            )
        ); ?>
    </div>

    <div class="span2 empresaVencedora">
        <?php echo $this->Form->input('co_contratacao', array(
            'id' => 'coContratacao',
            'class' => 'input-normal',
            'type' => 'select',
            'label'=>'Modalidade',
            'empty' => 'Selecione...',
            'options' => $contratacoes
            )
        ); ?>
    </div>

    <div class="span2 empresaVencedora">
        <?php echo $this->Form->input('co_situacao', array(
            'id' => 'coSituacao',
            'class' => 'input-normal',
            'type' => 'select',
            'label'=>'Situacão',
            'empty' => 'Selecione...',
            'options' => $situacoes
            )
        ); ?>
    </div>
    </div>
    <!--<div class="form-actions">-->
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
            <button rel="tooltip" type="button" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small  limparRelatorio">Limpar</button>
            <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'licitacoes', 'action' => 'index')); ?>" title="Voltar" class="btn btn-small" id=""> Voltar</a>
        </div>
    <!--</div>-->

    <?php echo $this->Form->end() ?>

<?php
if($tipoRelatorio  == "C"){
    echo $this->element('licitacoes/contratos', array('contratos' => $relatorio));
}
if($tipoRelatorio  == "L"){
    echo $this->element('licitacoes/licitacao', array('licitacoes' => $relatorio));
}
if($tipoRelatorio  == "E"){
    echo $this->element('licitacoes/empresas_vencedoras', array('empresas' => $relatorio));
}
?>

<!-- modal impressão -->
<div id="imprimirModal" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <p class="pull-right">
        <a id="imprimirPdf" href="#imprimirPdf" class="btn btn-small btn-primary" ><i class="icon-print"></i> Imprimir</a>
        <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    </p>
    <h3 id="tituloAbaImpressoa">Impressão de Pesquisa</h3>
  </div>
    <div class="modal-body maior" id="impressao" styles="background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
    </div>
</div>

<!-- modal aviso -->
<div style="top:12%" id="modalAviso" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Alerta</h3>
    </div>
    
    <div class="modal-body">
        <p style="font-size:14px">Não existem relatórios para impressão</p>
    </div>
    
    <div class="modal-footer">
        <a href="#" class="btn btn-primary btn-small" data-dismiss="modal" > Ok </a>
    </div>
</div>

<script>
$().ready(function () {
    var empresa = "<?php echo $tipoRelatorio?>";

    $('.limparRelatorio').click(function () {
        $('#tipoRelatorio').val('C').trigger('chosen:updated')
    });

    $('#btnImprimir').on('click', function (e) {
        // $('#modalAviso').modal();
        e.preventDefault();
        imprimir($(this).attr('url'));
    });

    $('#imprimirPdf').click(function(){
        var frm = document.getElementById("iframe_imp_contrato").contentWindow;
        frm.focus();
        frm.print();
        return false;
    });

    if(empresa === "E"){
        $(".empresaVencedora").hide();
    }

    $('#tipoRelatorio').change(function() {
        if ($(this).val() == "E") {
            $('#coContratacao').val('').trigger('chosen:updated');
            $('#coSituacao').val('').trigger('chosen:updated');
            $(".empresaVencedora").hide();
        }else{
            $(".empresaVencedora").show();
        }
    });
});

function imprimir(url_imp) {
    if ($('.table-relatorio').DataTable().data().length < 1) {
        $('#modalAviso').modal();
        return;
    }

    $('#imprimirModal').modal();

    $.ajax({
        type: 'POST',
        url:"<?php echo $this->base; ?>/relatorios/iframe_imprimir/",
        // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data:{
            "data[url]": $(location).attr("href") + '&imprimir=pdf',
        },
        success:function(result){
            $('#impressao').html(result);
        }
    })
}

$('.table-relatorio').DataTable({
    ordering: false,
    sorting: false,
    pageLength: 10,
    // info: true,
    bLengthChange: false,
    searching: false,
    // pagingType: 'simple',
    language: {
        // search: '<strong>Pesquisar</strong>:',
        zeroRecords: "Nehum resultado encontrato",
        infoFiltered: "",
        info: 'Mostrando _START_ - _END_ de _TOTAL_ registros',
        paginate: {
            previous: '‹‹ Anterior',
            next:     'Proximo ››'
        },
        // "info": "Showing page _PAGE_ of _PAGES_",
        infoEmpty: "Nenhum registro encontrado",
    }
});
// $('.table-relatorio_filter').remove();
// $('.table-relatorio_wrapper').find('div.row-fluid .span6:nth-child(1)').remove();

</script>
