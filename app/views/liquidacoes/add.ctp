<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<div class="aditivos form">
    <?php
    echo $this->Form->create('Liquidacao', array('url' => array('controller' => 'liquidacoes', 'action' => 'add', $coContrato)));
    ?>
    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar uma Liquidação - <b>Campos com *
                são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'liquidacoes', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar Liquidações">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 ">
            <div class="widget-header widget-header-small"><h4>Nova Liquidação</h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                                echo $this->Form->input('nu_liquidacao', array('class' => 'input-xlarge', 'type' => 'text', 'maxlength' => 23, 'label' => 'Número da Liquidação', 'id' => 'nuLiquidacao'));

                                //echo $this->Form->input('co_empenho', array('label' => 'Empenho', 'type' => 'select', 'empty' => 'Selecione...', 'class' => 'input-xxlarge chosen-select', 'options' => $empenho));
                                echo $this->Form->input('radioIsNota', array(
                                    'type' => 'checkbox',
                                    'label' => 'Tenho a nota fiscal cadastrada no sistema'
                                ));
                                echo $this->Form->input('co_nota', array('label' => 'Nota Fiscal', 'type' => 'select', 'empty' => 'Selecione...', 'class' => 'input-xxlarge chosen-select', 'options' => $notasFiscais));
                                echo $this->Form->input('vl_liquidacao', array('class' => 'input-xlarge', 'label' => 'Valor da liquidação (R$)'));
                                //echo $this->Form->input('co_nota', array('label' => 'Nota Fiscal', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $notasFiscais));

                                echo $this->Form->input('dt_liquidacao', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de Liquidação', 'type' => 'text'));

                                echo $this->Form->input('dt_envio_sefin', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de envio ao Setor Financeiro', 'type' => 'text'));

                                echo $this->Form->input('dt_recebimento_sefin', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de recebimento do Setor Financeiro', 'type' => 'text'));
                                ?>
                                <div id="erro_data_recebimento" class="error-message hide">A Data de Envio não pode ser
                                    maior que a Data de recebimento! Insira uma data válida e tente novamente!
                                </div>
                                <?php
                                echo $this->Form->input('ds_liquidacao', array('class' => 'input-xlarge', 'label' => 'Descrição', 'type' => 'textarea', 'rows' => '4',
                                    'onKeyup' => '$(this).limit("500","#charsLeft")',
                                    'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Aditivo">
                Salvar
            </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"
                    id="Limpar"> Limpar
            </button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(this).limit("500", "#charsLeft");
            $("#erro_data_recebimento").hide();

            $('#LiquidacaoRadioIsNota').on('change', function (par1, par2, par3) {
                if ($(this).prop('checked')) {
                    $('.input.text').find('#LiquidacaoVlLiquidacao').parent().hide();
                    $('#LiquidacaoVlLiquidacao').val('');
                    $('.input.select').find('#LiquidacaoCoNota').parent().show();
                } else {
                    $('.input.text').find('#LiquidacaoVlLiquidacao').parent().show();
                    $('.input.select').find('#LiquidacaoCoNota').parent().hide();
                    $('select#LiquidacaoCoNota option').removeAttr("selected");
                }
            });

            $('#LiquidacaoRadioIsNota').prop('checked', true).change();
            $("#LiquidacaoVlLiquidacao").maskMoney({thousands: '.', decimal: ','});

            // Validação data de recimento do financeiro.
            $("form").on('submit', function () {
                if (!verificaDataRecebimento()) {
                    return false;
                }
            });

            $("#nuLiquidacao").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 90)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });

        function verificaDataRecebimento() {
            var dataEnvio = $("#LiquidacaoDtEnvioSefin").val(),
                dataReceb = $("#LiquidacaoDtRecebimentoSefin").val();
            if ($.datepicker.parseDate('dd/mm/yy', dataEnvio) > $.datepicker.parseDate('dd/mm/yy', dataReceb)) {
                $("#erro_data_recebimento").show();
                return false;
            } else {
                $("#erro_data_recebimento").hide();
            }
            return true;
        }
    </script>

</div>
