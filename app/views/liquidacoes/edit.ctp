<?php
echo $this->Html->script('inicia-datetimepicker');
?>
<div class="liquidacao form">
    <?php echo $this->Form->create('Liquidacao', array('url' => array('controller' => 'liquidacoes', 'action' => 'edit', $id, $coContrato), 'id' => "cleanForm")); ?>
    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar a Liquidação - <b>Campos com * são
                obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'liquidacoes', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar Penalidades">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 ">
            <div class="widget-header widget-header-small"><h4>Alterar Liquidação</h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php

                                $blockUnputs = false;
                                if ($liquidacao['ic_importacao_siafi'] === 'S') {
                                    $blockUnputs = true;
                                    $msgTooltipInfoSiafi = 'A edição do campo é bloqueada quando esse é preenchido automaticamento com informações do SIAFI';
                                }


                                echo $this->Form->hidden('co_liquidacao');
                                echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                                if ($blockUnputs) {
                                    echo $this->Form->input('nu_liquidacaox', array(
                                            'class' => 'input-xlarge',
                                            'type' => 'text',
                                            'maxlength' => 23,
                                            'label' => 'Número da Liquidação',

                                            'value' => $liquidacao['nu_liquidacao'],
                                            'disabled' => 'disabled',
                                            'before' => '<div class="input-append">',
                                            'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                        )
                                    );
                                    echo $this->Form->hidden('nu_liquidacao');
                                } else {
                                    echo $this->Form->input('nu_liquidacao', array(
                                            'class' => 'input-xlarge',
                                            'type' => 'text',
                                            'maxlength' => 23,
                                            'label' => 'Número da Liquidação'
                                        )
                                    );

                                }
                                //echo $this->Form->input('co_empenho', array('label' => 'Empenho', 'type' => 'select', 'empty' => 'Selecione...', 'class' => 'input-xxlarge chosen-select', 'options' => $empenhos));


                                if ($blockUnputs) {
                                    $hasNotaFiscal = (!empty($liquidacao['co_nota']));
                                    $hasNotaFiscalSiafi = ($this->data['NotaFiscal']['ic_importacao_siafi'] === 'S') ? true : false;

                                    $arrConfigInputNota = array(
                                        'label' => 'Nota Fiscal',
                                        'type' => 'select',
                                        'empty' => 'Selecione...',
                                        'class' => 'input-xlarge',
                                        'options' => $notasFiscais,
                                    );

                                    if ($hasNotaFiscal && $hasNotaFiscalSiafi) {
                                        echo $this->Form->input('radioIsNotax', array(
                                            'type' => 'checkbox',
                                            'label' => 'Tenho a nota fiscal cadastrada no sistema',
                                            'checked' => $hasNotaFiscal,
                                            'disabled' => 'disabled'
                                        ));
                                        echo $this->Form->hidden('radioIsNota', array('value' => $hasNotaFiscal));

                                        echo $this->Form->input('co_notax',
                                            array_merge(
                                                $arrConfigInputNota,
                                                array(
                                                    'value' => $liquidacao['co_nota'],
                                                    'disabled' => 'disabled',
                                                    'before' => '<div class="input-append">',
                                                    'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                                )
                                            )
                                        );
                                        echo $this->Form->hidden('co_nota');

                                    } else {

                                        echo $this->Form->input('radioIsNota', array(
                                            'type' => 'checkbox',
                                            'label' => 'Tenho a nota fiscal cadastrada no sistema',
                                            'checked' => $hasNotaFiscal,
                                        ));

                                        echo $this->Form->input('co_nota', $arrConfigInputNota);
                                    }

                                    if (!$hasNotaFiscalSiafi) {
                                        echo $this->Form->input('vl_liquidacaox', array(
                                                'class' => 'input-xlarge',
                                                'label' => 'Valor da liquidação (R$)',
                                                'value' => $this->Print->real($this->data['Liquidacao']['vl_liquidacao']),
                                                'before' => '<div class="input-append">',
                                                'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                                'disabled' => 'disabled',
                                            )
                                        );
                                    }
                                    echo $this->Form->hidden('vl_liquidacao');

                                    echo $this->Form->input('dt_liquidacaox', array(
                                        'before' => '<div class="input-append">',
                                        'after' => '<span class="add-on alert-tooltip" title="' . $msgTooltipInfoSiafi . '"><i class="icon-info-sign"></i></span></div>',
                                        'disabled' => 'disabled',
                                        'value' => $this->data['Liquidacao']['dt_liquidacao'],
                                        'data-format' => 'dd/MM/yyyy',
                                        'mask' => '99/99/9999',
                                        'class' => 'input-small',
                                        'label' => 'Data de Liquidação', 'type' => 'text'));

                                    echo $this->Form->hidden('dt_liquidacao');

                                } else {

                                    echo $this->Form->input('radioIsNota', array(
                                        'type' => 'checkbox',
                                        'label' => 'Tenho a nota fiscal cadastrada no sistema',
                                        'checked' => (!empty($liquidacao['co_nota']))
                                    ));

                                    echo $this->Form->input('co_nota', array(
                                        'label' => 'Nota Fiscal',
                                        'type' => 'select',
                                        'empty' => 'Selecione...',
                                        'class' => 'input-xlarge',
                                        'options' => $notasFiscais));

                                    echo $this->Form->input('vl_liquidacao', array(
                                            'class' => 'input-xlarge',
                                            'label' => 'Valor da liquidação (R$)',
                                            'value' => $this->Print->real($this->data['Liquidacao']['vl_liquidacao'])
                                        )
                                    );

                                    echo $this->Form->input('dt_liquidacao', array(
                                        'before' => '<div class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data de Liquidação', 'type' => 'text'));
                                }

                                echo $this->Form->input('dt_envio_sefin', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de envio ao Setor Financeiro', 'type' => 'text'));

                                echo $this->Form->input('dt_recebimento_sefin', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data de recebimento do Setor Financeiro', 'type' => 'text'));
                                echo $this->Form->input('ds_liquidacao', array('class' => 'input-xlarge', 'label' => 'Descrição', 'type' => 'textarea', 'rows' => '4',
                                    'onKeyup' => '$(this).limit("500","#charsLeft")',
                                    'after' => '<br><span id="charsLeft"></span> caracteres restantes.',
                                    'value' => $liquidacao['ds_liquidacao']));
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Salvar Liquidação"> Salvar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm">
                Limpar
            </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"
                    onclick="voltar()"> Voltar
            </button>
        </div>
    </div>
    <script>
        function voltar() {
            self.location = '<?php echo $this->Html->url(array('controller' => 'liquidacoes', 'action' => 'index', $coContrato))?>'
        }
        $('#LiquidacaoRadioIsNota').on('change', function (par1, par2, par3) {
            if ($(this).prop('checked')) {
                $('.input.text').find('#LiquidacaoVlLiquidacao,#LiquidacaoVlLiquidacaox').parent().hide();
                var select = $('.input.select').find('#LiquidacaoCoNota');
                select.parent().show();
                if (!select.hasClass('chosen-select')) {
                    select.addClass('chosen-select');
                    select.chosen();
                }
            } else {
                $('.input.text').find('#LiquidacaoVlLiquidacao, #LiquidacaoVlLiquidacaox').parent().show();
                $('.input.select').find('#LiquidacaoCoNota').parent().hide();
                $('select#LiquidacaoCoNota option').removeAttr("selected");
            }
        });

        $('#LiquidacaoRadioIsNota').change();
        $("#LiquidacaoVlLiquidacao").maskMoney({thousands: '.', decimal: ','});

        $('#LimparForm').click(function () {
            $('#LiquidacaoNuLiquidacao').val('');
            $('#LiquidacaoVlLiquidacao').val('');
            $('#LiquidacaoDtLiquidacao').val('');
            $('#LiquidacaoDtEnvioSefin').val('');
            $('#LiquidacaoDtRecebimentoSefin').val('');
            $('#LiquidacaoDsLiquidacao').val('');
        });
    </script>
</div>
