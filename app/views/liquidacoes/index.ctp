        
<?php
$usuario = $this->Session->read ('usuario');
$hasPenalidade = $this->Session->read ('hasPenalidade');
?>
<div class="liquidacoes index">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped" id="tbLiquidacoes">
        <tr>
            <th><?php __('Número da NL'); ?></th>
            <th><?php __('Data Liquidação'); ?></th>
            <th><?php __('Data envio ao Setor Financeiro'); ?></th>
            <th><?php __('Data recebimento do Setor Financeiro'); ?></th>
            <th><?php __('Valor'); ?></th>
            <th><?php __('Número da Nota Fiscal'); ?></th>
            <th><?php __('Descrição'); ?></th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($liquidacoes as $liquidacao):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td>
                    <?php if (isset($liquidacao['Liquidacao']['nu_liquidacao'])) {
                        echo $liquidacao['Liquidacao']['nu_liquidacao'];
                    } else {
                        echo '- - -';
                    } ?>
                </td>
                <td><?php echo $liquidacao['Liquidacao']['dt_liquidacao']; ?> &nbsp;</td>
                <td>
                    <?php
                    if (isset($liquidacao['Liquidacao']['dt_envio_sefin'])) {
                        echo $liquidacao['Liquidacao']['dt_envio_sefin'];
                    }
                    ?>
                </td>
                <td><?php
                    if (isset($liquidacao['Liquidacao']['dt_recebimento_sefin'])) {
                        echo $liquidacao['Liquidacao']['dt_recebimento_sefin'];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $vl = (empty($liquidacao['Liquidacao']['co_nota'])) ? $liquidacao['Liquidacao']['vl_liquidacao'] : $liquidacao['NotaFiscal']['vl_nota'];
                    echo $this->Formatacao->moeda($vl);
                    ?>
                </td>
                <td><?php
                    if (isset($liquidacao['NotaFiscal']['nu_nota'])) {
                        echo $liquidacao['NotaFiscal']['nu_nota'];
                    } else {
                        echo 'Liquidação sem Nota Fiscal anexada!';
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (isset($liquidacao['Liquidacao']['ds_liquidacao'])) {
                        $textoCompleto = $liquidacao['Liquidacao']['ds_liquidacao'];
                        echo substr($liquidacao['Liquidacao']['ds_liquidacao'], 0, 60);
                        if (strlen($liquidacao['Liquidacao']['ds_liquidacao']) > 59)
                            echo "... <a href='' class='alert-tooltip' title='{$textoCompleto}'>Leia mais</a>";
                    } else {
                        echo '- - -';
                    }
                    ?>
                </td>
                <td class="actions"><?php $id = $liquidacao['Liquidacao']['co_liquidacao']; ?>
                    <div class="btn-group acoes">
                        <?php echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'liquidacoes/', $hasPenalidade)) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
</div>
<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'liquidacoes/add', $hasPenalidade) ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'liquidacoes', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>
    </div>
<?php } ?>

<?php echo $this->Html->scriptStart() ?>
    $('.alert-tooltip').tooltip();
<?php echo $this->Html->scriptEnd() ?>
