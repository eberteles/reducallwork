<?php echo $this->Form->create('LocalAtendimento', array('url' => array('controller' => 'locais_atendimento', 'action' => 'edit'))); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Dados do Local de Atendimento</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="row-fluid">
                    <div class="span6">
                        <?php
                        echo $this->Form->input('co_locais_atendimento');
                        echo $this->Form->hidden('co_fornecedor', array('value' => $coFornecedor));
                        echo $this->Form->input('uf', array('class' => 'input-xlarge','label' => 'UF','type' => 'select', 'options' => $ufs, 'empty' => 'Selecione'));
                        echo $this->Form->input('endereco', array('class' => 'input-xlarge','label' => 'Endereço', 'maxlength' => '50'));
                        echo $this->Form->input('observacoes', array('class' => 'input-xlarge','label' => 'Observações'));
                        ?>
                    </div>
                    <div class="span6">
                        <?php
                        echo $this->Form->input('municipio', array('class' => 'input-xlarge','label' => 'Cidade', 'maxlength' => '60'));
                        echo $this->Form->input('co_bairro', array('class' => 'input-xlarge chosen-select', 'label' => 'Bairro', 'type' => 'select', 'options' => $bairros, 'empty' => 'Selecione'));
                        echo $this->Form->input('telefone1', array('class' => 'input-xlarge','label' => 'Telefone 1', 'maxlength' => '11', 'mask' => '(99) 9999-9999?9'));
                        echo $this->Form->input('telefone2', array('class' => 'input-xlarge','label' => 'Telefone 2', 'maxlength' => '11', 'mask' => '(99) 9999-9999?9'));
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>

        </div>
    </div>
</div>

