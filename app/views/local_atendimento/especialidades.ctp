<?php echo $this->Form->create('Especialidade', array('url' => "/locais_atendimento/especialidades/" . $id . "/" . $coFornecedor)); ?>

<?php //echo '<pre>';var_dump($this->data);die; ?>

<div class="page-header position-relative"><h1>Visualizar/Adicionar Especialidades</h1></div>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <?php

            $especs = array();

            if(isset($this->data)){
                $counter = 0;
                foreach ($this->data as $le) {
                    echo $this->Form->hidden('id', array('value' => $le['LocaisEspecialidades']['id'], 'name' => 'data[' . $counter . '][Especialidade][id]'));

                    $especs[] = $le['Especialidade']['co_especialidade'];

                    $counter++;
                }
            } else {
                echo $this->Form->hidden('id');
            }

            echo $this->Form->hidden('co_locais_atendimento', array('value' => $id));
            echo $this->Form->input('especialidades', array('label' => 'Selecione a Especialidade para Adicionar', 'multiple'=>true, 'data-placeholder'=>'Selecione a Especialidade', 'class' => 'input-xxlarge chosen-select', 'type' => 'select', 'options' => $especialidades, 'id' => 'Especialidades', 'selected' => $especs));
        ?>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

        </div>
    </div>
</div>

