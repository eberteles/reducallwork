<div id="alertasExames"></div>

<?php echo $this->Form->create('ExameConsulta', array(
    'id' => 'examesForm',
    'class' => ''
    // 'url' => "/locais_atendimento/exames/" . $id . "/" . $coFornecedor));
));?>

<ul id="idsExames" style="display:none">
    <?php foreach ($idsExames as $value) :?>
    <li><?php echo $value ?></li>
    <?php endforeach; ?>
</ul>

<div class="page-header position-relative"><h1>Visualizar/Adicionar Exames/Consultas</h1></div>
<input type="hidden" id="urlsubmit" value="/locais_atendimento/exames/<?php echo $id . "/" . $coFornecedor ?>" />

<div id="table-exames" style="overflow:visible" class="row-fluid">
    <!-- <b>Campos com * são obrigatórios.</b><br> -->
    <!-- <div class="row-fluid"> -->
    <?php
    $especs = array();

    if (isset($this->data)) {
        $counter = 0;
        foreach ($this->data as $le) {
            echo $this->Form->hidden('id', array('value' => $le['LocaisExames']['id'], 'name' => 'data[' . $counter . '][ExameConsulta][id]'));

            $especs[] = $le['ExameConsulta']['id'];
            $counter++;
        }
    } else {
        echo $this->Form->hidden('id');
    }

    echo $this->Form->hidden('co_locais_atendimento', array('value' => $id));
    echo '<div style="float:left;overflow-y:visible';
    echo $this->Form->input('exames', array(
        'id' => 'exameup',
        'style' => '',
        'label' => 'Selecione o Exame para Adicionar',
        'multiple'=>false, 'data-placeholder'=>'Selecione o Exame',
        'class' => 'input-xxlarge chosen-select', 'type' => 'select',
        'options' => $exames, 'selected' => false, 'empty' => 'Selecione um exame'));
    ?>
    <input id="btnAddExame" class="btn btn-small btn-primary" style="margin: 1.8em 0 0 0.5em" type="submit" value="Adicionar" />
    </div>
    <br style="clear:both"/>

    <table cellpadding="0" cellspacing="0" border="0" style="width:100% !important" id="products-table" class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
               <th>Exames/Consultas</th>
               <th>Valor</th>
               <th>Ações</th>
           </tr>
       </thead>
       <!--<tfoot>
           <tr>
               <td colspan="5" style="text-align: left;">
                   <button onclick="AddTableRow()" type="button">Adicionar Produto</button>
               </td>
           </tr>
       </tfoot>-->
       <tbody>
           <?php foreach ($this->data as $exame) : ?>
            <tr>
                <td><?php echo $exame['ExameConsulta']['nome_combo'] ?></td>

                <td>
                    R$:&nbsp;<input onkeyup="mudarIconeSalvar(<?php echo $exame['LocaisExames']['id'] ?>)" maxlength="12" style="height: 17px" id="exameValor<?php echo $exame['LocaisExames']['id'] ?>" class="mascaraDihneiro" type="text"
                        value="<?php echo empty($exame['LocaisExames']['valor']) ? '0,00' :  number_format($exame['LocaisExames']['valor'], 2 , ',' , '.') ?>" />
                </td>

                <td>
                    <div class="btn-group">
                        <a id="<?php echo 'iconeMudar'.$exame['LocaisExames']['id'] ?>" onclick="updateExameValor(<?php echo $exame['LocaisExames']['id']?>, this)" class="btn       btn-success" href=""><i class="icon-ok"></i>
                        </a>
                        <a onclick="deleteExameVinculado(this, <?php echo $exame['LocaisExames']['id'] . ','    .$exame['ExameConsulta']['id']?>)" class="btn btn-danger" href=""><i class="icon-remove"></i>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
        <?php
        /*echo $this->Paginator->counter(
            array('format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));*/
        ?>
        <!--<div class="pagination">
            <ul>
            <?php /*echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?>
            <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled'));*/ ?>
        </ul>
        </div>-->

    <!--<div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

        </div>
    </div> -->
</div>
