<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
	    <div class="acoes-formulario-top clearfix" >
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'locais_atendimento', 'action' => 'add/' . $coFornecedor)); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Local de Atendimento"><i class="icon-plus icon-white"></i> Novo Local de Atendimento</a>
          </div>
        </div>

<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>UF</th>
		<th>CIDADE</th>
		<th>ENDEREÇO</th>
		<th>TELEFONES</th>
		<th>OBSERVAÇÕES</th>
		<th class="actions">AÇÕES</th>
	</tr>
	<?php
	$i = 0;
	foreach ($locais as $local){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $local['LocalAtendimento']['uf']; ?></td>
        <td><?php echo $local['LocalAtendimento']['municipio']; ?></td>
        <td><?php echo $local['LocalAtendimento']['endereco']; ?></td>
        <td><?php echo $local['LocalAtendimento']['telefone1'] . ' - ' . $local['LocalAtendimento']['telefone2']; ?></td>
        <td><?php echo $local['LocalAtendimento']['observacoes']; ?></td>

		<td class="actions">
			<div class="btn-group acoes">
				<?php
                    echo $this->element( 'actions12', array( 'id' => $local['LocalAtendimento']['co_locais_atendimento'] . '/' . $coFornecedor, 'class' => 'btn', 'local_acao' => 'adm_ctb_' ) );
                ?>
			</div>
		</td>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
