<style>
    .titleEmpenho {
        background-color: #D3D3D3;
        padding: 5px;
        font-weight: bold;
        font-size: 1.4em;
    }
</style>
<div class="index">

    <?php
    $formatacaoHelper = new FormatacaoHelper();
    $formatacaoHelper->Number = new NumberHelper;

    $arrTipoOperacao = array(
        'U' => 'Alteração',
        'I' => 'Inclusão'
    );
    $arrTipoEmpenho = array(
        'O' => 'Original',
        '' => ''
    );

    ?>

    <?php switch ($tipoEntidade):
        case 4:?>
            <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
                <thead>
                <tr>
                    <th><?php __('Número') ?></th>
                    <th><?php __('Tipo') ?></th>
                    <th><?php __('Valor') ?></th>
                    <th><?php __('Operação') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arrDados as $nrEmpenho => $arrRegistros): ?>
                    <?php foreach ($arrRegistros as $dados):
                        $tupla = json_decode($dados['ds_log']);
                        ?>
                        <tr>
                            <td><?php echo $dados['nu_empenho'] ?></td>
                            <td><?php echo $arrTipoEmpenho[$tupla->Empenho->tp_empenho]; ?></td>
                            <td><?php echo $formatacaoHelper->moeda($tupla->Empenho->vl_empenho); ?></td>
                            <td><?php echo $arrTipoOperacao[$dados['tp_operacao']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
    <?php break; ?>
    <?php case 3: ?>
            <?php foreach($arrDados as $nuEmpenho=>$arrRegistros): ?>
                <div class="titleEmpenho" style="">Empenho : <?php echo $nuEmpenho; ?></div>
                <br/>
                <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><?php __('Número') ?></th>
                        <th><?php __('Valor') ?></th>
                        <th><?php __('Operação') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($arrRegistros as $dados):
                        $tupla = json_decode($dados['ds_log']);
                        ?>
                        <tr>
                            <td><?php echo $tupla->NotaFiscal->nu_nota; ?></td>
                            <td><?php echo $formatacaoHelper->moeda($tupla->NotaFiscal->vl_nota); ?></td>
                            <td><?php echo $arrTipoOperacao[$dados['tp_operacao']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <br/>
                <br/>
            <?php endforeach; ?>
    <?php break; ?>

    <?php case 2: ?>
            <?php foreach($arrDados as $nuEmpenho=>$arrRegistros): ?>
                <div class="titleEmpenho">Empenho : <?php echo $nuEmpenho; ?></div>
                <br/>
                <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><?php __('Número') ?></th>
                        <th><?php __('N° Nota') ?></th>
                        <th><?php __('Valor') ?></th>
                        <th><?php __('Operação') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    App::import('Model', 'NotaFiscal');
                    $mdlNotaFiscal = new NotaFiscal();
                    foreach ($arrRegistros as $dados):
                        $tupla = json_decode($dados['ds_log']);
                        $nuNota = '-';
                        if ($tupla->Liquidacao->co_nota) {
                            $dadosNota = $mdlNotaFiscal->find(
                                'first',
                                array('conditions' => array('NotaFiscal.co_nota' => $tupla->Liquidacao->co_nota))
                            );
                            $nuNota = 'NF ' . $dadosNota['NotaFiscal']['nu_nota'];
                        }
                        ?>
                        <tr>
                            <td><?php echo $tupla->Liquidacao->co_documento_siafi; ?></td>
                            <td><?php echo $nuNota; ?></td>
                            <td><?php echo $formatacaoHelper->moeda($tupla->Liquidacao->vl_liquidacao); ?></td>
                            <td><?php echo $arrTipoOperacao[$dados['tp_operacao']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <br/>
                <br/>
            <?php endforeach; ?>
    <?php break; ?>

    <?php default; ?>
            <?php foreach($arrDados as $nuEmpenho=>$arrRegistros): ?>
                <div class="titleEmpenho">Empenho : <?php echo $nuEmpenho; ?></div>
                <br/>
                <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><?php __('N° Ordem Bancária') ?></th>
                        <th><?php __('Valor') ?></th>
                        <th><?php __('Operação') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($arrRegistros as $dados):
                        $tupla = json_decode($dados['ds_log']);
                        ?>
                        <tr>
                            <td><?php echo $tupla->Pagamento->co_documento_siafi; ?></td>
<!--                            <td>--><?php //echo $formatacaoHelper->moeda($tupla->Pagamento->vl_pagamento, array('negative'=>'- ')); ?><!--</td>-->
                            <td><?php echo $formatacaoHelper->moeda($tupla->Pagamento->vl_pagamento); ?></td>
                            <td><?php echo $arrTipoOperacao[$dados['tp_operacao']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <br/>
                <br/>
            <?php endforeach; ?>
    <?php endswitch;?>
</div>