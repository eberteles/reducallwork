<?php

echo $this->Html->script('inicia-datetimepicker'); ?>

<div class="row-fluid">
    <div class="page-header position-relative">
        <h1><?php __('Log de Processamento Siafi'); ?></h1>
    </div>
    <?php echo $this->Form->create('LogSiafi'); ?>

    <div class="row-fluid">
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input('dt_ini_log', array(
                    'before' => '<div class="input-append date datetimepicker">',
                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                    'class' => 'input-small', 'label' => 'Data Inicial', 'type' => 'text'));
                ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input('dt_fim_log', array(
                    'before' => '<div class="input-append date datetimepickerMaxHoje">',
                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                    'class' => 'input-small', 'label' => 'Data Final', 'type' => 'text'));
                ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('tp_entidade', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Todos', 'label' => 'Tipo', 'options' => $tiposEntidade)); ?>
            </div>
        </div>
        <div class="span1">
            <div class="controls">
                <?php
                echo $this->Form->input('nu_uasg', array(
                    'mask' => '999999',
                    'class' => 'input-mini', 'label' => 'UASG', 'type' => 'text'));
                ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input(
                    'nu_empenho',
                    array(
                        'mask' => FunctionsComponent::pegarFormato('empenho'),
                        'class' => 'input-medium',
                        'label' => 'Empenho',
                        'type' => 'text'
                    )
                );

                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"><i class="icon-search icon-white"></i> Pesquisar
            </button>
            <button rel="tooltip" type="button" class="btn btn-small" id="limpar">Limpar</button>
            <button rel="tooltip" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Data', 'dt_operacao'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade de Empenhos', 'empenho'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade de Notas', 'nota'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade de Liquidações', 'liquidacao'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade de Pagamentos', 'pagamento'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($arrRegistroLog)): ?>
            <?php
            $i = 0;
            foreach ($arrRegistroLog as $linha):
                $linha = current($linha);
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class; ?>>
                    <td><?php echo DateTime::createFromFormat('Y-m-d', $linha['dt_operacao'])->format('d/m/Y'); ?></td>
                    <td>
                        <?php if ($linha['empenho'] > 0): ?>
                            <a style="cursor: pointer;"
                               class="detalhar"
                               href="javascript:void(0);"
                               data-title="Empenhos"
                               data-tipo-entidade="4"
                               data-dt-operacao="<?php echo $linha['dt_operacao'] ?>"
                               title="Detalhar Empenho">
                                <?php echo $linha['empenho']; ?>
                            </a>
                        <?php else:
                            echo $linha['empenho'];
                            ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($linha['nota'] > 0): ?>
                            <a style="cursor: pointer;"
                               class="detalhar"
                               href="javascript:void(0);"
                               data-title="Notas Fiscais"
                               data-tipo-entidade="3"
                               data-dt-operacao="<?php echo $linha['dt_operacao'] ?>"
                               title="Detalhar Notas Fiscais">
                                <?php echo $linha['nota']; ?>
                            </a>
                        <?php else:
                            echo $linha['nota'];
                            ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($linha['liquidacao'] > 0): ?>
                            <a style="cursor: pointer;"
                               class="detalhar"
                               href="javascript:void(0);"
                               data-title="Liquidações"
                               data-tipo-entidade="2"
                               data-dt-operacao="<?php echo $linha['dt_operacao'] ?>"
                               title="Detalhar Liquidações">
                                <?php echo $linha['liquidacao']; ?>
                            </a>
                        <?php else:
                            echo $linha['liquidacao'];
                            ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($linha['pagamento'] > 0): ?>
                            <a style="cursor: pointer;"
                               class="detalhar"
                               href="javascript:void(0);"
                               data-title="Pagamentos"
                               data-tipo-entidade="1"
                               data-dt-operacao="<?php echo $linha['dt_operacao'] ?>"
                               title="Detalhar Pagamentos">
                                <?php echo $linha['pagamento']; ?>
                            </a>
                        <?php else:
                            echo $linha['pagamento'];
                            ?>

                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="5">Nenhum registro encontrado.</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
    </fieldset>
</div>

<div id="modal-detalheLog" class="modal modal-medium hide fade" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="modal-title-detalheLog"></h3>
    </div>
    <div id="modal-detalhe-body" class="modal-body"></div>

    <div class="modal-footer">
        <a class="btn btn-small btn-primary" data-dismiss="modal"><i class="icon-remove"></i> Fechar</a>
    </div>
</div>

<script type="application/javascript">

    var LogSiafi = {
        init: function () {

            $('a.detalhar').on('click', LogSiafi.handleClickLinkDetalhar);

            $('#limpar').on('click', function(){
                $('form#LogSiafiIndexForm :input:not(:button)').val('');
            });

        },

        handleClickLinkDetalhar: function () {

            var tpEntidade = parseInt($(this).data('tipo-entidade'))
                , dtOperacao = $(this).data('dt-operacao')
                , title = $(this).data('title');

            $('#modal-title-detalheLog').text('Detalhamento de ' + title);

            var url_an = "<?php echo $this->base; ?>/log_siafis/detalhar/" +tpEntidade+'/'+dtOperacao;
            $.ajax({
                type:"POST",
                url:url_an,
                success:function(result){
                    $('#modal-detalhe-body').html(result);
                    $('#modal-detalheLog').modal();
                }
            });

        }
    };


    $(LogSiafi.init);


</script>