<?php echo $this->Html->script('inicia-datetimepicker'); ?>

<div class="row-fluid">
    <div class="page-header position-relative">
        <h1><?php __('Log de Acesso'); ?></h1>
    </div>
    <?php echo $this->Form->create('Log'); ?>

    <div class="row-fluid">
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input('dt_ini_log', array(
                    'before' => '<div class="input-append date datetimepicker">',
                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                    'class' => 'input-small', 'label' => 'Data Inicial', 'type' => 'text'));
                ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input('dt_fim_log', array(
                    'before' => '<div class="input-append date datetimepicker">',
                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                    'class' => 'input-small', 'label' => 'Data Final', 'type' => 'text'));
                ?>
            </div>
        </div>
        <div class="span2">
            <div class="controls">
                <?php
                echo $this->Form->input('nu_cpf', array(
                    'class' => 'input-medium',
                    'id' => 'nuCpf',
                    'label' => 'CPF',
                    'mask' => '999.999.999-99'
                ));
                ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('ds_email', array(
                    'class' => 'input-xlarge',
                    'label' => 'E-mail'
                ));
                ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->hidden('co_usuario'); ?>
                <?php echo $this->Form->input('ds_nome', array(
                    'class' => 'input-xlarge autocomplete',
                    'label' => 'Usuário'
                )); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small"
                    id="btnClearForm">
                Limpar
            </button>
            <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Data', 'dt_log'); ?></th>
            <th><?php echo $this->Paginator->sort('Nome', 'ds_nome'); ?></th>
            <th><?php echo $this->Paginator->sort('Email', 'ds_email'); ?></th>
            <th><?php echo $this->Paginator->sort('Usuário', 'co_usuario'); ?></th>
            <th><?php echo $this->Paginator->sort('IP', 'nu_ip'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($glogs)): ?>
            <?php
            $i = 0;
            foreach ($glogs as $linha):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class; ?>>

                    <td><?php echo $linha['Log']['dt_log']; ?>&nbsp;</td>
                    <td>
                        <?php
                        echo $linha['Usuario']['ds_nome'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $linha['Usuario']['ds_email'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $linha['Usuario']['no_usuario'];
                        ?>
                    </td>
                    <td><?php echo $linha['Log']['nu_ip']; ?>&nbsp;</td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="3">Nenhum registro encontrado.</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
    </fieldset>
</div>

<script type="text/javascript">

    var Log = {
        init: function () {
            Log.handleBtnLimpar()
                .handleAutocompleteUsuario();
        },
        handleBtnLimpar: function () {
            $('#btnClearForm').on('click', function () {
                $('form#LogIndexForm :input:not(button)').val('');
            });
            return Log;
        },
        handleAutocompleteUsuario: function () {
            $('.autocomplete').autocomplete({
                delay: 500,
                minLength: 5,
                change: function (event, ui) {
                    if (!ui.item) {
                        $('#LogCoUsuario').val('');
                        $('#LogDsNome').val('');
                    }
                },
                response: function (event, ui) {
                    console.log('response: ', ui);
                    if (ui.content.length == 1 && ui.content[0].value == '') {
                        $(this).val('');
                    }
                },
                source: function (request, response) {
                    $.ajax({
                        url: "<?php echo $this->Html->url(array('controller' => 'logs', 'action' => 'getUsersAutocomplete')) ?>/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            if (!data || data.length == 0) {
                                data = [{
                                    no_usuario: 'Nenhum registro encontrado',
                                    co_usuario: ''
                                }];
                            }
                            response($.map(data, function (item) {
                                return {
                                    label: item.no_usuario,
                                    value: item.co_usuario
                                }
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    var value = ui.item.value;
                    var label = ui.item.label;
                    if (value !== '') {
                        ui.item.label = value;
                        ui.item.value = label;
                        $('#LogCoUsuario').val(value);
                    }
                }
            });
            return Log;
        }
    };

    $(Log.init);
</script>