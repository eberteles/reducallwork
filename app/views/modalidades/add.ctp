<?php echo $this->Form->create('Modalidade', array('url' => array('controller' => 'modalidades', 'action' => 'add', $isModal))); ?>

<div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Tipo de Contrato'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
              <?php
                  echo $this->Form->input('co_modalidade');
                  if($this->Modulo->isCamposModalidade('nu_modalidade') == true) {
                    echo $this->Form->input('nu_modalidade', array('class' => 'input-xlarge','label' => 'Número','maxLength' => '10'));
                  }
                  echo $this->Form->input('ds_modalidade', array('class' => 'input-xlarge','label' => 'Descrição','maxLength' => '255'));
                  if($this->Modulo->isCamposModalidade('tp_modalidade') == true) {
                      echo $this->Form->input('tp_modalidade', array('type' => 'select','class' => 'input-xlarge', 'empty' => 'Selecione...', 'label'=>'Tipo de Modalidade', 'options' => $tiposModalidade));
                  }
                  if($this->Modulo->isCamposModalidade('tp_prova') == true) {
                      echo $this->Form->input('tp_prova', array('type' => 'select', 'class' => 'input-xlarge', 'empty' => 'Selecione...', 'label'=>'Tipo de Prova', 'options' => $tiposProva));
                  }
              ?>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <?php if(!$isModal): ?>
                    <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                <?php endif; ?>
            </div>
        </div>
    </div>

