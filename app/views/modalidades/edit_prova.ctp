<div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <?php echo $this->Form->create('Prova', array('url' => array('controller' => 'modalidades', 'action' => 'edit_prova', $coProva)));?>
            <div class="widget-header widget-header-small"><h4><?php __('Prova da Modalidade'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                      <div class="span6">
                        <?php
                            echo $this->Form->hidden('co_prova');
                            echo $this->Form->hidden('co_modalidade');
                            echo $this->Form->input('ds_prova', array('class' => 'input-xlarge', 'label' => 'Nome da Prova'));
                        ?>
                      </div>
                      <!-- <div class="span6">
                          Prova dividida por Sexo?<br>
                        <?php
                            //echo $this->Form->checkbox('is_sexo');
                        ?>
                      </div> -->
                  </div>
                    <div class="form-actions">
                        <div class="btn-group">
                            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>

                        </div>
                    </div>
              </div>
            </div>
            
            <?php echo $form->end(); ?>
        </div>
  
        <div class="row-fluid">
            <div class="span4">
                <div class="widget-header widget-header-small"><h4><?php __('Categorias / Classe'); ?></h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">
                          <?php echo $this->Form->input('ds_categoria', array('class' => 'input-large', 'label' => 'Nome da Nova Categoria', 
                              'after' => $this->Print->getBtnEditCombo('Adicionar Nova Categoria', 'AdcionarNovaCategoria', '#', true, 'blue icon-plus bigger-150')) ); ?>
                          <div id="listCategorias" style="height: auto;">
                              
                          </div>
                      </div>
                  </div>
                </div>
            </div>

            <div class="span4">
                <div class="widget-header widget-header-small"><h4><?php __('Sub-Categorias'); ?></h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">
                          <?php echo $this->Form->input('ds_subcategoria', array('class' => 'input-large', 'label' => 'Nome da Nova Sub-Categoria', 
                              'after' => $this->Print->getBtnEditCombo('Adicionar Nova Sub-Categoria', 'AdcionarNovaSubCategoria', '#', true, 'blue icon-plus bigger-150')) ); ?>
                          <div id="listSubCategorias" style="height: auto;">
                              
                          </div>
                      </div>
                  </div>
                </div>
            </div>

            <div class="span4">
                <div class="widget-header widget-header-small"><h4><?php __('Classificação Funcional'); ?></h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">
                          <?php echo $this->Form->input('ds_classificacao', array('class' => 'input-large', 'label' => 'Nome da Nova Classificação', 
                              'after' => $this->Print->getBtnEditCombo('Adicionar Nova Classificação', 'AdcionarNovaClassificacao', '#', true, 'blue icon-plus bigger-150')) ); ?>
                          <div id="listClassificacoes" style="height: auto;">
                              
                          </div>
                      </div>
                  </div>
                </div>
            </div>

        </div>

</div>
<?php echo $this->Html->scriptStart() ?>

    atualizaListCategorias();
    atualizaListSubCategorias();
    atualizaListClassificacao();

    function atualizaListCategorias(){
            <?php echo $this->JqueryEngine->request( $this->Html->url(array('controller' => 'provas_categorias', 'action' => 'index', $coProva)), array( 'update' => '#listCategorias' ) ); ?>
    }
    
    function atualizaListSubCategorias(){
            <?php echo $this->JqueryEngine->request( array('controller' => 'provas_subcategorias', 'action' => 'index', $coProva), array( 'update' => '#listSubCategorias' ) ); ?>
    }
    
    function atualizaListClassificacao(){
            <?php echo $this->JqueryEngine->request( array('controller' => 'provas_classificacoes', 'action' => 'index', $coProva), array( 'update' => '#listClassificacoes' ) ); ?>
    }
    
    function addCategoria() {
    
        var url_add = "<?php echo $this->Html->url(array('controller' => 'provas_categorias', 'action' => 'add')); ?>";
        $.ajax({
            type:"POST",
            url:url_add,
            data:{
                "data[ProvaCategoria][co_prova]":<?php echo $coProva; ?>,
                "data[ProvaCategoria][ds_prova_categoria]":$('#ds_categoria').val()
            },
            success:function(result){
                $('#listCategorias').html("");
                $('#listCategorias').append(result);
            }
        })
    
    }
    
    function addSubCategoria() {
    
        var url_add = "<?php echo $this->Html->url(array('controller' => 'provas_subcategorias', 'action' => 'add')); ?>";
        $.ajax({
            type:"POST",
            url:url_add,
            data:{
                "data[ProvaSubCategoria][co_prova]":<?php echo $coProva; ?>,
                "data[ProvaSubCategoria][ds_prova_subcategoria]":$('#ds_subcategoria').val()
            },
            success:function(result){
                $('#listSubCategorias').html("");
                $('#listSubCategorias').append(result);
            }
        })
    
    }
    
    function addClassificacao() {
    
        var url_add = "<?php echo $this->Html->url(array('controller' => 'provas_classificacoes', 'action' => 'add')); ?>";
        $.ajax({
            type:"POST",
            url:url_add,
            data:{
                "data[ProvaClassificacao][co_prova]":<?php echo $coProva; ?>,
                "data[ProvaClassificacao][ds_prova_classificacao]":$('#ds_classificacao').val()
            },
            success:function(result){
                $('#listClassificacoes').html("");
                $('#listClassificacoes').append(result);
            }
        })
    
    }
    
    $(document).ready(function() {
    
        $('#AdcionarNovaCategoria').click(function(){
            addCategoria();
        })
        
        $('#ds_categoria').keydown(function(e){
            var keyCode = e.which;
            if(keyCode == 13) {
                addCategoria();
            }
        })
        
        $('#AdcionarNovaSubCategoria').click(function(){
            addSubCategoria();
        })
        
        $('#ds_subcategoria').keydown(function(e){
            var keyCode = e.which;
            if(keyCode == 13) {
                addSubCategoria();
            }
        })
        
        $('#AdcionarNovaClassificacao').click(function(){
            addClassificacao();
        })
        
        $('#ds_classificacao').keydown(function(e){
            var keyCode = e.which;
            if(keyCode == 13) {
                addClassificacao();
            }
        })
    
    })

<?php echo $this->Html->scriptEnd() ?>