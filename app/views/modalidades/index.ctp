<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Tipo de Contrato'); ?></h1></div>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'modalidades/add') ) { ?>        
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha um dos campos para fazer a pesquisa</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'modalidades', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Setor"><i class="icon-plus icon-white"></i> Novo <?php __('Tipo de Contrato'); ?></a> 
          </div>
        </div>
      <?php } ?>
         
        <?php echo $this->Form->create('Modalidade', array('url' => array('controller' => 'modalidades', 'action' => 'index')));?>
            
<div class="row-fluid">
    	<div class="span3">
            <div class="controls">
                <div class="input text">
                    <label for="dsModalidade">Descrição</label>
                    <input name="data[Modalidade][ds_modalidade]" type="text" class="input-xlarge" id="dsModalidade" maxlength="100">
                </div>
            </div>
    	</div>
    <?php
        if($this->Modulo->isCamposModalidade('tp_modalidade') == true) {
    ?>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('tp_modalidade', array('type' => 'select','class' => 'input-xlarge', 'empty' => 'Selecione...', 'label'=>'Tipo de Modalidade', 'options' => $tiposModalidade)); ?>
            </div>
        </div>
    <?php
        }
        if($this->Modulo->isCamposModalidade('tp_prova') == true) {
    ?>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('tp_prova', array('type' => 'select', 'class' => 'input-xlarge', 'empty' => 'Selecione...', 'label'=>'Tipo de Prova', 'options' => $tiposProva)); ?>
            </div>
        </div>
    <?php
        }
    ?>
</div>
<div class="form-actions">
            <div class="btn-group">
                <?php if (count($modalidades)) : ?>
                <button type="button" id="btnImprimir" class="btn btn-primary btn-small" url="<?php echo $this->base . '/' . $url ?>"><i class="icon-print"></i>Imprimir</button>
              <?php endif ?>
              <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button> 
              <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="limparForm"> Limpar</button>

            </div>
</div>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<!-- th>< ?php echo $this->Paginator->sort('Código', 'co_modalidade');?></th -->
    <?php
        if($this->Modulo->isCamposModalidade('nu_modalidade') == true) {
    ?>
		<th><?php echo $this->Paginator->sort('Número', 'nu_modalidade');?></th>
    <?php
        }
    ?>
		<th><?php echo $this->Paginator->sort('Descrição', 'ds_modalidade');?></th>
    <?php
        if($this->Modulo->isCamposModalidade('tp_modalidade') == true) {
    ?>
		<th><?php echo $this->Paginator->sort('Tipo de Modalidade', 'tp_modalidade');?></th>
    <?php
        }
        if($this->Modulo->isCamposModalidade('tp_prova') == true) {
    ?>
		<th><?php echo $this->Paginator->sort('Tipo de Prova', 'tp_prova');?></th>
    <?php
        }
    ?>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($modalidades as $modalidade):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<!--td>< ?php echo $modalidade['Modalidade']['co_modalidade']; ?>&nbsp;</td-->
            <?php
                if($this->Modulo->isCamposModalidade('nu_modalidade') == true) {
            ?>
		<td><?php echo $modalidade['Modalidade']['nu_modalidade']; ?>&nbsp;</td>
            <?php
                }
            ?>
		<td><?php echo $modalidade['Modalidade']['ds_modalidade']; ?>&nbsp;</td>
            <?php
                if($this->Modulo->isCamposModalidade('tp_modalidade') == true) {
            ?>
                <td><?php if($modalidade['Modalidade']['tp_modalidade'] > 0) { echo $tiposModalidade[ $modalidade['Modalidade']['tp_modalidade'] ]; } ?>&nbsp;</td>
            <?php
                }
                if($this->Modulo->isCamposModalidade('tp_prova') == true) {
            ?>
                <td><?php if($modalidade['Modalidade']['tp_prova'] > 0) { echo $tiposProva[ $modalidade['Modalidade']['tp_prova'] ]; } ?>&nbsp;</td>
            <?php
                }
            ?>
		<td class="actions">
			<div class="btn-group acoes">
                            <?php
                                if($this->Modulo->isCamposModalidade('tp_prova') == true) {
                            ?>
                                <a id="<?php echo $modalidade['Modalidade']['co_modalidade']; ?>" title-modalidade="<?php echo $modalidade['Modalidade']['ds_modalidade']; ?>" class="v_provas btn" href="#view_provas"
                                        data-toggle="modal"><i
                                        class="silk-icon-award-star-add alert-tooltip"
                                        title="Provas da Modalidade" style="display: inline-block;"></i></a>
                            <?php
                                }
                            ?>
				<?php echo $this->element( 'actions2', array( 'id' => $modalidade['Modalidade']['co_modalidade'], 'class' => 'btn', 'local_acao' => 'modalidades/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<div id="view_provas" class="modal hide fade maior" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-header page-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
                <h1 id="titleModalProva">Provas da Modalidade</h1>
	</div>
	<div class="modal-body-iframe" id="prova_modalidade"></div>
</div>

<!-- modal impressão -->
<div id="imprimirModal" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <p class="pull-right">
        <a id="imprimirPdf" href="#imprimirPdf" class="btn btn-small btn-primary" ><i class="icon-print"></i> Imprimir</a>
        <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    </p>
    <h3 id="tituloAbaImpressoa">Impressão de Pesquisa</h3>
  </div>
    <div class="modal-body maior" id="impressao" styles="background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;">
    </div>
</div>

<script type="text/javascript">

    $('#limparForm').click(function () {
        $('#dsModalidade').val('');
    });

    $(document).ready(function () {
        $('#dsModalidade').val('');
    });
</script>


<?php echo $this->Html->scriptStart() ?>
    $('#tbModalidade tr td a.v_provas').click(function(){
        var title   = 'Provas - ' + $(this).attr('title-modalidade');
        var url_pv  = "<?php echo $this->Html->url(array('controller' => 'modalidades', 'action' => 'iframe_vincular')); ?>/" + $(this).attr('id');
        $('#titleModalProva').html("");
        $('#titleModalProva').append( title );
        $.ajax({
            type:"POST",
            url:url_pv,
            data:{
                },
                success:function(result){
                    $('#prova_modalidade').html("");
                    $('#prova_modalidade').append(result);
                }
            })
    });

    $('#imprimirPdf').click(function(){
        var frm = document.getElementById("iframe_imp_contrato").contentWindow;
        frm.focus();
        frm.print();
        return false;

    });

    $('#btnImprimir').on('click', function (e) {
        e.preventDefault();
        imprimir($(this).attr('url'));
    });

     function imprimir(url_imp) {
        $('#imprimirModal').modal();
        var queryString = $('#qs').html();
        
        $.ajax({
            type: 'POST',
            url:"<?php echo $this->Html->url(array('controller' => 'relatorios', 'action' => 'iframe_imprimir')); ?>",
            // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data:{
                "data[url]": url_imp  + '?imprimir=pdf',
                },
                success:function(result){
                    $('#impressao').html(result);
                }
            });
    }

    $('#limpar').on('click', function () {
        $('#dsModalidade').val('');
    });
<?php echo $this->Html->scriptEnd() ?>