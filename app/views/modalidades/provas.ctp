<div class="row-fluid">
    <?php echo $this->Form->create('Prova', array('url' => array ('controller' => 'modalidades', 'action' => 'provas', $co_modalidade)));?>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">
        <div class="span8">
         <?php 
            echo $this->Form->hidden('co_modalidade', array('value'=>$co_modalidade));
            echo $this->Form->input('ds_prova', array('class' => 'input-xxlarge', 'label' => 'Nome da Prova'));
         ?>
        </div>
        <div class="span3"><br>
            <button type="submit" class="btn btn-small btn-primary bt-pesquisar alert-tooltip" title="Adcionar Prova à Modalidade">Adcionar Prova</button>
        </div>
        </p>
    </div>
    <table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tbSetor">
	<tr>
            <th><?php __('Prova');?></th>
            <!-- <th><?php //__('Divisão por Sexo');?></th> -->
            <th><?php __('Categorias / Classe');?></th>
            <th><?php __('Sub-Categorias');?></th>
            <th><?php __('Classificação Funcional');?></th>
            <th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php //debug($provas);die;
	foreach ($provas as $prova):
            $id = $prova['Prova']['co_prova'];
        ?>
        <tr>
            <td><?php echo $prova['Prova']['ds_prova']; ?>&nbsp;</td>
            <!-- <td><?php //echo $this->Print->tpIndicador( $prova['Prova']['is_sexo'] ); ?>&nbsp;</td> -->
            <td>
                <?php
                $categorias = array();
                foreach ($prova['ProvaCategoria'] as $categoria):
                    $categorias[ $categoria['co_prova_categoria'] ] = $categoria['ds_prova_categoria'];
                endforeach;
                if( count($categorias) > 0) {
                    echo $this->Form->input('co_categoria',   array('label'=>false, 'class' => 'input-large','type' => 'select', 'options' => $categorias));
                } else {
                    echo '--';
                }
                ?>
            </td>
            <td>
                <?php
                $subcategorias = array();
                foreach ($prova['ProvaSubCategoria'] as $subcategoria):
                    $subcategorias[ $subcategoria['co_prova_subcategoria'] ] = $subcategoria['ds_prova_subcategoria'];
                endforeach;
                if( count($subcategorias) > 0) {
                    echo $this->Form->input('co_subcategoria',   array('label'=>false, 'class' => 'input-large','type' => 'select', 'options' => $subcategorias));
                } else {
                    echo '--';
                }
                ?>
            </td>
            <td>
                <?php
                $classificacoes = array();
                foreach ($prova['ProvaClassificacao'] as $classificacao):
                    $classificacoes[ $classificacao['co_prova_classificacao'] ] = $classificacao['ds_prova_classificacao'];
                endforeach;
                if( count($classificacoes) > 0) {
                    echo $this->Form->input('co_classificacao',   array('label'=>false, 'class' => 'input-large','type' => 'select', 'options' => $classificacoes));
                } else {
                    echo '--';
                }
                ?>
            </td>
            <td class="actions">
                <div class="btn-group acoes">
                    <?php echo $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit_prova', $id), array('escape' => false, 'title'=>'Editar Prova','class' => 'btn alert-tooltip')); ?>
                    <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $id), array('escape' => false, 'title'=>'Excluir Prova','class' => 'btn  btn-danger alert-tooltip'), sprintf(__('Tem certeza de que deseja desvincular este registro?', true), $id)); ?>
                </div>
            </td>
        </tr>
        <?php
        endforeach; 
        ?>
    </table>
</div>