<script type="text/javascript" src="/js/inicia-datetimepicker.js"></script>

<div class="relatorios index">
    <style>
        input[type=checkbox] {
            float: none;
        }
        input[type=radio] {
            float: none;
        }
        .templateTextarea {
            height: 100px;
            width: 500px;
        }
    </style>
    <div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Modelos'); ?></h1></div>

            <fieldset class="ativo">

                <table style="background-color:white" class="table table-hover table-bordered table-striped">
                    <tr>
                        <td>Check list:</td>
                        <td><a id="linkCheckList" href="#">Gerar documento</a></td>
                    </tr>
                    <tr>
                        <td>Instruções de preenchimento:</td>
                        <td><?php echo $this->Html->link('Gerar documento', $this->Html->url(array('controller' => 'modelos', 'action' => 'gerarInstrucoesPreenchimento'))) ?></td>
                    </tr> 
                    <tr>
                        <td>01. Oficialização de demanda:</td>
                        <?php //echo $this->Html->link('Gerar documento', $this->Html->url(array('controller' => 'modelos', 'action' => 'gerarOficializacaoDemanda'))') ?>
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>02. Análise de viabilidade:</td>
                        <!-- gerarAnaliseViabilidade -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>03. Plano de sustentação:</td>
                        <!-- gerarPlanoSustentacao -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>04. Estratégia de contratação:</td>
                        <!-- gerarEstrategiaContratacao-->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>05. Análise de risco:</td>
                        <!-- gerarAnaliseRisco-->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>06. Termo de referência ou projeto básico:</td>
                        <!-- gerarTermoReferencia -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>07. Plano de inserção:</td>
                        <!-- gerarPlanoInsercao -->
                        <td><?php echo $this->Html->link('Gerar documento', '#/') ?></td>
                    </tr>
                    <tr>
                        <td>08. Termo de ciência:</td>
                        <!-- gerarTermoCiencia-->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>09. Termo de compromisso:</td>
                        <!-- gerarTermoCompromisso -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>10. Ordem de serviço ou fornecimento de bens:</td>
                        <!-- gerarOrdemServico-->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>11. Termo de recebimento provisório:</td>
                        <!-- gerarTermoRecebimentoProvisorio -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>12. Termo de recebimento definitivo:</td>
                        <!-- gerarTermoRecebimentoDefinitivo-->
                        <td><?php echo $this->Html->link('Gerar documento', '/modelos/') ?></td>
                    </tr>
                    <tr>
                        <td>13. Termo de encerramento do <?php __('Contrato'); ?>:</td>
                        <!-- gerarTermoEncerramentoContrato -->
                        <td><?php echo $this->Html->link('Gerar documento', '#') ?></td>
                    </tr>
                    <tr>
                        <td>14. Memorando:</td>
                        <td><?php echo $this->Html->link('Gerar documento', '#', array('class' => 'btnMemorando')) ?></td>
                    </tr>                    
                </table>
            </fieldset>
    </div>
</div>

<div id="viewGerarDocumento" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <p class="pull-right">
            <a id="btnGerarDocumento" href="#" class="btn btn-small btn-primary" ><i class="icon-print"></i> Gerar Documento</a>
            <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
        </p>
        <h3 id="tituloDocumento"></h3>
    </div>
    <div class="modal-body maior" id="impressao">
        <div class="row-fluid">
            <div class="box-tabs-white">
                <label class="control-label">
                    <h5>Documentos presentes:</h5>
                    <span style="color:green">* Marque os documentos que já estão prontos/entregues, coloque comentários pertinentes em cada um e clique em << Gerar Documento >></span>
                </label>
                <ul class="nav nav-tabs" id="myTab">
                    <?php
                        echo $aba->render(array(
                            array('tabList01', '01 - 04', true, ''),
                            array('tabList02', '05 - 08', true, '')
                        ), null, 0);
                    ?>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <iframe id="iframeDownload" style="display:none"></iframe>
                    <form id="frmCheckList" target="_blank" action="<?php echo $this->Html->url(array('controller' => 'modelos', 'action' => 'gerarCheckList')) ?>" method="post">
                        <div id="tabList01" class="ativo container-fluid">
                            <div class="row-fluid">
                                <div class="span5">
                                    <?php 
                                        echo $this->element('modelos', array(
                                            'label'=>'01. Planejamento estratégico da Instituição (PEI):', 
                                            'id'=>'01')
                                        ); 
                                        echo $this->element('modelos', array(
                                            'label'=>'2. Plano Diretor de Tecnologia da Informação (PDTI)', 
                                            'id'=>'02')
                                        );
                                    ?>
                                </div>
                                <div class="span5">
                                    <?php
                                        echo $this->element('modelos', array(
                                            'label'=>'3. ESPECIFICAÇÃO DA DEMANDA:', 
                                            'id'=>'03')
                                        );
                                        echo $this->element('modelos', array(
                                            'label'=>'04. PLANEJAMENTO DA DEMANDA:', 
                                            'id'=>'04')
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div id="tabList02" class="inativo container-fluid" style="display:none">
                            <div class="row-fluid">
                                <div class="span5">
                                    <?php 
                                        echo $this->element('modelos', array(
                                            'label'=>'05. PLANO DE SUSTENTAÇÃO:', 
                                            'id'=>'05')
                                        ); 
                                        echo $this->element('modelos', array(
                                            'label'=>'06. ESTRATÉGIA DE CONTRATAÇÃO:', 
                                            'id'=>'06')
                                        );
                                    ?>
                                </div>
                                <div class="span5">
                                    <?php
                                        echo $this->element('modelos', array(
                                            'label'=>'07. Análise de Riscos:', 
                                            'id'=>'07')
                                        );
                                        echo $this->element('modelos', array(
                                            'label'=>'08. Termo de Referência:', 
                                            'id'=>'08')
                                        );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="view_form_memorando" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <p class="pull-right">
        <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
    </p>
    <h3 id="tituloAbaModelo">Memorando</h3>
  </div>
    <div id="conteudoModelo" style="padding:10px;">

    	<div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Dados do documento:</h4></div>
            <div class="widget-body">
              <div class="widget-main">
				<div class="input text required"><label>Número:</label><input type="text" class="input-xlarge" style="width:100px;"/></div>
				<div class="input text"><label>Sigla:</label><input type="text" class="input-xlarge" style="width:100px;"/></div>
				<div class="input text required">
				    <div class="input-append date datetimepicker">
				        <label>Data de Publicação</label>
				        <input type="text" data-format="dd/MM/yyyy" mask="99/99/9999" class="input-small" /><span class="add-on"><i data-date-icon="icon-calendar"></i></span>
				    </div>
				</div>
				<div class="input text required"><label>Nome do Signatário:</label><input type="text" class="input-xlarge" style="width:300px;"/></div>
				<div class="input text required"><label>Cargo do Signatário:</label><input type="text" class="input-xlarge" style="width:300px;"/></div>
			  </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Gerar Documento" onclick='$("#dl")[0].src="<?php echo $this->Html->url(array('controller' => 'modelos', 'action' => 'gerarMemorando')) ?>"'> Gerar Documento</button>
            </div>
        </div>
        
    </div>
  </div>
</div>

<iframe id="dl" style="display:none"></iframe>

<script>
    $(document).ready(function(){
        $("#myTab > li > a").bind('click',function(e){
            e.preventDefault();
            var selecionado = $(this);
            $("#myTab > li").each(function(){
                $(this).removeClass('active');
            });
            $("#myTabContent > form > div").each(function(){
                $(this).hide(function(){
                    selecionado.parent().addClass('active');
                    $(selecionado.attr('href')).show();
                });
            });		
        });
        $('#linkCheckList').on('click', function () {
            $('#tituloDocumento').html('Modelo de Documento - Check List');
            $('input:checkbox').removeAttr('checked');
            $('input:checkbox').each(function(key, value){ 
                $('#' + this.id.replace(/chk/g, 'div')).hide();
            });
            $('#viewGerarDocumento').modal(true)
            $.get('<?php echo $this->Html->url(array('controller' => 'modelos', 'action' => 'limparArquivosTemporarios')) ?>');
            // target="_blank" href="/modelos/gerarCheckList"
        })
        $('input:checkbox').each(function(key, value){
            $(this).on('click', function(){
                var id = this.id.replace(/chk/g, 'div');
                if (this.checked)
                    $('#'+id).show();
                else
                    $('#'+id).hide();
            })
        })
        $('#btnGerarDocumento').on('click', function() {
            $('#frmCheckList').submit();
        })

    	$('.btnMemorando').click(function(){
    		$('#view_form_memorando').modal();
    	})
    });
</script>
