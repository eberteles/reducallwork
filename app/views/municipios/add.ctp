<div class="municipios form"><?php echo $this->Form->create('Municipio');?>
<fieldset><legend><?php __('Municipio'); ?></legend> <?php
echo $this->Form->input('co_municipio');
echo $this->Form->input('sg_uf');
echo $this->Form->input('ds_municipio');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Listar Municipios', true), array('action' => 'index'));?></li>
</ul>
</div>
