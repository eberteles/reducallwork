<div class="municipios index">
<!-- h2>< ?php __('Municipios');?></h2 -->

<fieldset>
	<legend><?php __('Relatórios'); ?></legend>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('co_municipio');?></th>
		<th><?php echo $this->Paginator->sort('sg_uf');?></th>
		<th><?php echo $this->Paginator->sort('ds_municipio');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($municipios as $municipio):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $municipio['Municipio']['co_municipio']; ?>&nbsp;</td>
		<td><?php echo $municipio['Municipio']['sg_uf']; ?>&nbsp;</td>
		<td><?php echo $municipio['Municipio']['ds_municipio']; ?>&nbsp;</td>
		<td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $municipio['Municipio']['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $municipio['Municipio']['id'])); ?>
		<?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $municipio['Municipio']['id']), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $municipio['Municipio']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</fieldset>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Municipio', true), array('action' => 'add')); ?></li>
</ul>
</div>
