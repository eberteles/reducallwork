<?php echo $this->Form->create('NotaCategoria', array('url' => array('controller' => 'notas_categorias', 'action' => 'add', $modal))); ?>


    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php __('Categoria de Nota'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                            <?php
                            echo $this->Form->input('co_nota_categoria');
                            echo $this->Form->input('ds_nota_categoria', array('class' => 'input-xlarge', 'label' => 'Descrição'));
                            ?>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

