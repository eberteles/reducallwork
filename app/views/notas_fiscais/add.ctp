<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="aditivos form">
<?php
    echo $this->Form->create('NotaFiscal', array('type' => 'file', 'url' => "/notas_fiscais/add/$coContrato"));
?>

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar uma Nota Fiscal - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'notas_fiscais', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Notas">Listagem</a>
          </div>
        </div>

       <div class="row-fluid">

            <div class="span6 ">
                <div class="widget-header widget-header-small"><h4>Nova Nota Fiscal</h4></div>

            <div class="widget-body">
              <div class="widget-main">
                <?php if( $this->Modulo->isEmpenho() ) { ?>
                <div class="row-fluid">
                    <div class="span12 required">
                        <?php
                        echo $this->Form->input('co_empenhos', array(
                                'label' => __('Empenhos', true),
                            'multiple'=>true,
                            'data-placeholder'=> __('Selecione ' . __('osEmpenhos', true) . ' que esta Nota atende...', true), 'class' => 'input-xxlarge chosen-select',
                            'type' => 'select',
                            'options' => $empenhos,
                            'id' => 'Empenhos',
                            'onchange' => 'verificaValorDosEmpenhos()')
                        ); ?>
                    </div>
                </div>
                <?php } ?>
                <div class="row-fluid">
                  <div class="span6">
              	     <dl class="dl-horizontal">
                    <?php
                        echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                        echo $this->Form->input('nu_nota', array('class' => 'input-small', 'label'=>'Nº Nota'));

                        if($this->Modulo->isCamposNota('dt_recebimento')) {
                        echo $this->Form->input('dt_recebimento', array(
                                'before' => '<div id="divDtRecebimento" class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data de Recebimento', 'type'=>'text'));
                        }

                        echo $this->Form->input('dt_envio', array(
                                'before' => '<div id="divDtEnvio" class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => __('Envio ao Financeiro', true), 'type'=>'text'));

                        echo $this->Form->input('ds_nota', array('class' => 'input-large','type' => 'texarea','label' => 'Observações',
                            'onKeyup'=>'$(this).limit("500","#charsLeft")',
                            'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));

                    ?>
	             </dl>
                  </div>
                  <div class="span6">
              	     <dl class="dl-horizontal">
                    <?php
                    if($this->Modulo->isCamposNota('nu_serie')) :
                        echo $this->Form->input('nu_serie', array('class' => 'input-small', 'label'=>'Série'));
                    endif;

                    if($this->Modulo->isCamposNota('co_nota_categoria')) {
                        echo $this->Form->input('co_nota_categoria', array('label' => __('Categoria da Nota', true), 'class' => 'chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $categorias,
                            'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Adicionar Categoria" id="AbrirNovaCategoria" href="#view_nova_categoria" data-toggle="modal"><i class="blue icon-edit bigger-150"></i></a>' ));
                        echo '<br>';
                    }

                    echo $this->Form->input('vl_nota', array('class' => 'input-large ValorNota','label'=>'Valor Nota (R$)', 'onblur' => 'verificaValorDosEmpenhos()', 'onfocus' => 'limpaCss()', 'id' => 'ValorNota', 'maxlength' => 18 ));
                    ?>
                        <span id="NotaFiscalError" style="font-weight: bold; color: red;"> O Valor da Nota Fiscal não pode superar o Valor Total <?php __("dosEmpenhos") ?> </span>
                    <?php

                    if($this->Modulo->isCamposNota('vl_glosa')) :
                        echo $this->Form->input('vl_glosa', array('class' => 'input-large','label'=>'Valor da Glosa (R$)', 'maxlength' => 18 ));
                    endif;

                    if($this->Modulo->isCamposNota('dt_nota')) {
                        echo $this->Form->input('dt_nota', array(
                            'before' => '<div id="divDtNota" class="input-append date datetimepickerDtNota">',
                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                            'class' => 'input-small', 'label' => 'Data da Nota', 'type' => 'text',
                            'id' => 'dtNota',
                            'onchange' => 'checkDate()'
                        ));
                    }
                    ?>
                     <span style="color: red;font-weight: bold;" id="dtNotaErro">A data da nota deve ser maior ou igual às datas <?php __('dosEmpenhos'); ?></span>
                    <?php

                    if($this->Modulo->notaFiscalCompetencia){
                        echo $this->Form->input('ds_competencia', array('class' => 'input-large', 'label'=>'Competência do Serviço'));

                        echo $this->Form->input('dt_competencia', array(
                            'before'      => '<div id="divDtNota" class="input-append date datetimepickerDtNota">',
                            'after'       => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                            'class'       => 'input-small', 'label' => 'Data de Competência', 'type' => 'text',
                            'id'          => 'dtCompetencia',
                        ));
                    }

                    ?>
                    <?php
                    if($this->Modulo->isCamposNota('dt_vencimento')) :
                        echo $this->Form->input('dt_vencimento', array(
                            'before' => '<div id="divDtVencimento" class="input-append date datetimepicker">',
                            'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                            'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                            'class' => 'input-small', 'label' => 'Data de Vencimento', 'type' => 'text'
                        ));
                    endif;
                    
                    echo $this->Form->input('anexo', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Nota Digitalizada'));

                        //echo $this->Form->input('anexo', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo da Nota') );
                    ?>
	             </dl>
                  </div>
                </div>
              </div>
	      </div>

            </div>
           
           <?php if($this->Modulo->notaFiscalAtesto){ ?>

            <div class="span6 ">
                <div class="widget-header widget-header-small"><h4>Atesto da Nota</h4></div>

            <div class="widget-body">
              <div class="widget-main">
                <div class="row-fluid">
                  <div class="span6">
                    <dl class="dl-horizontal">
                    <?php
                        echo $this->Form->input('ic_atesto', array('label' => 'Atestado', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $situacoesAtesto));

                        echo $this->Form->input('dt_atesto', array(
                                'before' => '<div id="divDtAtesto" class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data do Atesto', 'type'=>'text'));

                        echo $this->Form->input('ds_atesto', array('class' => 'input-large','type' => 'texarea','label' => 'Motivo/Observação',
                            'onKeyup'=>'$(this).limit("500","#charsLeft1")',
                            'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'));
                    ?>
                    </dl>
	         </div>
                </div>
              </div>
	      </div>

            </div>
           <?php } ?>

        </div>

    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Aditivo" id="BtnSave" > Salvar</button>
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar" id="Limpar"> Limpar</button>
          <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

    <div id="view_nova_categoria" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova Categoria</h3>
        </div>
        <div class="modal-body-iframe" id="add_categoria">
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#NotaFiscalError").hide();
        $("#dtNotaErro").hide();
        $("#ValorNota").maskMoney({thousands:'.', decimal:','});
        $("#NotaFiscalVlGlosa").maskMoney({thousands:'.', decimal:','});

        $('.upload-arquivo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });


        $("#AbrirNovaCategoria").bind('click', function(e) {
            var url_md = "<?php echo $this->base; ?>/notas_categorias/iframe/";
            $.ajax({
                type:"POST",
                url:url_md,
                data:{
                },
                success:function(result){
                    $('#add_categoria').html("");
                    $('#add_categoria').append(result);
                }
            })
        });
    });

    function checkDate(){
        var date     = $("#dtNota").val();
        var empenhos = $("#Empenhos").val();
        if(empenhos != null && date) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "<?php echo $this->Html->url(array('controller' => 'notas_fiscais', 'action' => 'checkEmpenhoDate')); ?>",
                data: {
                    'empenhos': empenhos,
                    'dtNota': date
                },
                success: function (data){
                    if(data.msg == 'false'){
                        $("#BtnSave").prop("disabled",true);
                        $("#dtNotaErro").show();
                    }else{
                        $("#BtnSave").prop("disabled",false);
                        $("#dtNotaErro").hide();
                    }
                }
            });
        }
    }

    function limpaCss(){
        $(".ValorNota").css("color","");
        $(".ValorNota").css("border-color","");
        $(".ValorNota").css("font-weight","");
        $("#NotaFiscalError").hide();
        document.getElementById('BtnSave').disabled = false;
    }

    function verificaValorDosEmpenhos(){
      <?php if( $this->Modulo->isEmpenho() ) { ?>
        var empenhos = $("#Empenhos").val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "<?php echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'getValorEmpenho')); ?>",
            data: {
                'empenhos': empenhos
            },
            success: function (valor) {
                var valorEmpenhos = valor.vlTotalEmpenhos;
                var valorMin = valor.minValue;

                //valorEmpenhos = valorEmpenhos.replace("\"", '').replace("\"", '');
                valorEmpenhos = parseFloat(valorEmpenhos);
                valorMin = parseFloat(valorMin);

                var valorNota = $("#ValorNota").val().replace(/[^\d,]+/g,'');
                valorNota = valorNota.replace(',','.');
                valorNota = parseFloat(valorNota);
                if( valorEmpenhos < valorNota ){
                    $(".ValorNota").css({'color' : 'red', 'border-color' : 'red', 'font-weight' : 'bold'});
                    $("#NotaFiscalError").show();
                    document.getElementById('BtnSave').disabled = true;
                }else{
//                    if(valorMin > valorNota) {
//                        $(".ValorNota").css({'color' : 'red', 'border-color' : 'red', 'font-weight' : 'bold'});
//                        $("#NotaFiscalError").show();
//                        document.getElementById('BtnSave').disabled = true;
//                    } else {
                        $(".ValorNota").css("color","");
                        $(".ValorNota").css("border-color","");
                        $(".ValorNota").css("font-weight","");
                        $("#NotaFiscalError").hide();
                        document.getElementById('BtnSave').disabled = false;
//                    }
                }
            }
        });

        checkDate();

//        for( counter = 0 ; counter < empenhos.length ; counter++ ) {
//            $.ajax({
//                type: "POST",
//                url: "<?php //echo $this->Html->url(array('controller' => 'empenhos', 'action' => 'getValorEmpenho')); ?>//",
//                data: {
//                    'id': empenhos[counter]
//                },
//                success: function (valor) {
//                    valor = valor.replace("\"", '').replace("\"", '');
//                    valor = parseFloat(valor);
//                    valorTotalEmpenhos += valor;
//
//                    console.log("Contador " + counter);
//                    console.log("Num "  + num);
//                    if( counter == num ){
//                        console.log(valorTotalEmpenhos);
//                    }
//
//                }
//            });
//        }

      <?php } ?>
    }

    function atzComboCategoria(co_nota_categoria) {
        $.getJSON("<?php echo $this->Html->url(array ('controller' => 'notas_categorias', 'action' => 'listar') )?>", null, function(data){
            var options = '<option value="">Selecione..</option>';
            $.each(data, function(index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#NotaFiscalCoNotaCategoria").html(options);
            $("#NotaFiscalCoNotaCategoria option[value=" + co_nota_categoria + "]").attr("selected", true);
            $("#NotaFiscalCoNotaCategoria").trigger("chosen:updated");
        });
    }

</script>
