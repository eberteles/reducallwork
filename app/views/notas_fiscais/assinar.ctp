<?php
    if(Configure::read('App.config.resource.certificadoDigital')) {
        $signatureStarter = new \Lacuna\FullXmlSignatureStarter($this->Util->getRestPkiClient());
        $signatureStarter->setXmlToSignPath($filename);
        $signatureStarter->setSignaturePolicy(\Lacuna\StandardSignaturePolicies::XML_XADES_BES);
        $signatureStarter->setSecurityContext(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
        $token = $signatureStarter->startWithWebPki();
        $this->Util->setNoCacheHeaders();

        echo $this->Html->script('lacuna-web-pki-2.5.0');
        echo $this->Html->script('jquery.blockUI');
    }
?>

<script>

    var pki = new LacunaWebPKI();

    // -------------------------------------------------------------------------------------------------
    // Function called once the page is loaded
    // -------------------------------------------------------------------------------------------------
    function init() {

        // Wireup of button clicks
        $('#salvarConfirmarAssinatura').click(sign);
        $('#refreshButton').click(refresh);

        // Block the UI while we get things ready
        $.blockUI();

        // Call the init() method on the LacunaWebPKI object, passing a callback for when
        // the component is ready to be used and another to be called when an error occurs
        // on any of the subsequent operations. For more information, see:
        // https://webpki.lacunasoftware.com/#/Documentation#coding-the-first-lines
        // http://webpki.lacunasoftware.com/Help/classes/LacunaWebPKI.html#method_init
        pki.init({
            ready: loadCertificates, // as soon as the component is ready we'll load the certificates
            defaultError: onWebPkiError
        });
    }

    // -------------------------------------------------------------------------------------------------
    // Function called when the user clicks the "Refresh" button
    // -------------------------------------------------------------------------------------------------
    function refresh() {
        // Block the UI while we load the certificates
        $.blockUI();
        // Invoke the loading of the certificates
        loadCertificates();
    }

    // -------------------------------------------------------------------------------------------------
    // Function that loads the certificates, either on startup or when the user
    // clicks the "Refresh" button. At this point, the UI is already blocked.
    // -------------------------------------------------------------------------------------------------
    function loadCertificates() {

        // Call the listCertificates() method to list the user's certificates
        pki.listCertificates({

            // specify that expired certificates should be ignored
            filter: pki.filters.isWithinValidity,

            // in order to list only certificates within validity period and having a CPF (ICP-Brasil), use this instead:
            //filter: pki.filters.all(pki.filters.hasPkiBrazilCpf, pki.filters.isWithinValidity),

            // id of the select to be populated with the certificates
            selectId: 'certificateSelect',

            // function that will be called to get the text that should be displayed for each option
            selectOptionFormatter: function (cert) {
                return cert.subjectName + ' (issued by ' + cert.issuerName + ')';
            }

        }).success(function () {

            // once the certificates have been listed, unblock the UI
            $.unblockUI();
        });

    }

    // -------------------------------------------------------------------------------------------------
    // Function called when the user clicks the "Sign" button
    // -------------------------------------------------------------------------------------------------
    function sign() {

        // Block the UI while we perform the signature
        $.blockUI();

        // Get the thumbprint of the selected certificate
        var selectedCertThumbprint = $('#certificateSelect').val();

        // Call signWithRestPki() on the Web PKI component passing the token received from REST PKI and the certificate
        // selected by the user.
        pki.signWithRestPki({
            token: '<?php echo $token; ?>',
            thumbprint: selectedCertThumbprint
        }).success(function() {
            // Once the operation is completed, we submit the form
            $('#form_assinatura').submit();
        });
    }

    // -------------------------------------------------------------------------------------------------
    // Function called if an error occurs on the Web PKI component
    // -------------------------------------------------------------------------------------------------
    function onWebPkiError(message, error, origin) {
        // Unblock the UI
        $.unblockUI();
        // Log the error to the browser console (for debugging purposes)
        if (console) {
            console.log('An error has occurred on the signature browser component: ' + message, error);
        }
        // Show the message to the user. You might want to substitute the alert below with a more user-friendly UI
        // component to show the error.
        alert(message);
    }

    // Schedule the init function to be called once the page is loaded
    $(document).ready(init);

</script>


<?php echo $this->Form->create('Assinatura', array('action'=>'assinar','id'=>'form_assinatura')); ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">Assinar Documento</h3>
    </div>
    <div class="modal-body-iframe" id="assinar_doc">
        <div class="span10">
            Documento a ser assinado: <br>
            <pre id="TextoASerAssinado">Nº da Nota:  <?php echo $nota['nu_nota']; ?> <br />Recebimento: <?php echo $nota['dt_recebimento']; ?>  <br />Valor:  <?php echo $this->Formatacao->moeda( $nota['vl_nota'] ); ?>.</pre>
        </div>
        <div class="span10">
            <?php
                echo $this->Form->hidden('redirect_controller', array('value' => 'notas_fiscais'));
                echo $this->Form->hidden('redirect_action', array('value' => 'index'));
                echo $this->Form->hidden('redirect_complemento', array('value' => $nota['co_contrato']));
                echo $this->Form->hidden('vincula_modal', array('value' => 'NotaFiscal'));
                echo $this->Form->hidden('vincula_id', array('id' => 'VinculaId', 'value' => $nota['co_nota']));
                echo $this->Form->hidden('token', array('value' => $token));
            ?>

            <div class="form-group">
                <label for="certificateSelect">Choose a certificate</label>
                <select id="certificateSelect" class="form-control"></select>
            </div>

<!--              --><?php //echo $this->element( 'applet_assinar_documento' ); ?>
        </div>
    </div>
    <div class="actions">
      <div class="acoes-formulario-top clearfix" >
          <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
        <div class="pull-right btn-group">
            <button rel="tooltip" type="button" class="btn btn-small btn-primary" id="salvarConfirmarAssinatura" title="Confirmar Assinatura"> Confirmar Assinatura</button>
        </div>
      </div>

    </div>

<!--    <script type="text/javascript">-->
<!---->
<!--    $('#form_assinatura').submit(function(){-->
<!--        if( $('#StatusCertificacao').val() == "" ) {-->
<!--            alert('Clique em Assinar e aguarde a conclusão do processo de Assinatura Digital.');-->
<!--            return false;-->
<!--        }-->
<!--        if( $('#StatusCertificacao').val() != "000" ) {-->
<!--            alert('Ocorreu um problema na verificação do seu certificado: ' + $('#StatusMsgCertificacao').val());-->
<!--            return false;-->
<!--        }-->
<!---->
<!--        var obj = jQuery.parseJSON( $("#JsonIcp").val() );-->
<!--        $('#UsuarioToken').val( obj.NOME );-->
<!--    });-->
<!---->
<!--    </script>-->

<?php echo $this->Form->end(); ?>