<?php if ($readOnly === true) { ?>

    <script>
        $(document).ready(function () {

            $('input').attr('disabled', 'disabled');
            $('textarea').attr('disabled', 'disabled');
            $('select').attr('disabled', 'disabled');
            $('.requiredLegend').hide();
        });
    </script>

<?php } ?>

<?php
    echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Html->script('inicia-datetimepicker');
?>
<div class="aditivos form">
    <?php echo $this->Form->create('NotaFiscal', array('type' => 'file', 'url' => "/notas_fiscais/edit/$id/$coContrato")); ?>
    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar a Nota Fiscal - <b>Campos com * são
                obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'notas_fiscais', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar Notas Fiscais">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span6 ">
            <div class="widget-header widget-header-small"><h4>Alterar Nota Fiscal</h4></div>

            <div class="widget-body">
                <div class="widget-main">
                    <?php if ($this->Modulo->isEmpenho()) { ?>
                        <div class="row-fluid">
                            <div class="span12 required">
                                <?php echo $this->Form->input('co_empenhos', array('label' => __('Empenhos', true), 'multiple' => true, 'data-placeholder' => 'Selecione ' . __('osEmpenhos', true) . ' que esta Nota atende...', 'class' => 'input-xxlarge chosen-select', 'type' => 'select', 'options' => $empenhos, 'selected' => $empenhosNota)); ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row-fluid">
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->hidden('co_nota');
                                echo $this->Form->hidden('ic_importacao_siafi');
                                echo $this->Form->hidden('co_contrato', array('value' => $this->data['NotaFiscal']['co_contrato']));

                                $blockUnputs = false;
                                if ($this->data['NotaFiscal']['ic_importacao_siafi'] === 'S') {
                                    $blockUnputs = true;
                                    $containerTooltipInfoSiafi = '<span class="add-on alert-tooltip" title="A edição do campo é bloqueada quando esse é preenchido automaticamento com informações do SIAFI"><i class="icon-info-sign"></i></span></div>';
                                }

                                $arrConfigInputNuNota = array(
                                    'class' => 'input-small',
                                    'label' => 'Nº Nota'
                                );
                                $arrConfigInputDtRecebimento = array(
                                    'before' => '<div id="divDtRecebimento" class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy',
                                    'mask' => '99/99/9999',
                                    'class' => 'input-small',
                                    'label' => 'Data de Recebimento',
                                    'type' => 'text'
                                );
                                $arrConfigInputDtEnvio = array(
                                    'before' => '<div id="divDtEnvio" class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy',
                                    'mask' => '99/99/9999',
                                    'class' => 'input-small',
                                    'label' => __('Envio ao Financeiro', true),
                                    'type' => 'text'
                                );

                                if ($blockUnputs) {
                                    echo $this->Form->input('nu_notax', $arrConfigInputNuNota + array(
                                            'value' => $nota['nu_nota'],
                                            'disabled' => 'disabled',
                                            'before' => '<div class="input-append">',
                                            'after' => $containerTooltipInfoSiafi,
                                        )
                                    );
                                    echo $this->Form->hidden('nu_nota');

                                    echo $this->Form->input('dt_recebimentox', array_merge($arrConfigInputDtRecebimento, array(
                                                'value' => $nota['dt_recebimento'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('dt_recebimento');

                                    echo $this->Form->input('dt_enviox', array_merge($arrConfigInputDtEnvio, array(
                                                'value' => $nota['dt_envio'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('dt_envio');

                                } else {
                                    echo $this->Form->input('nu_nota', $arrConfigInputNuNota);
                                    if($this->Modulo->isCamposNota('dt_recebimento')) {
                                        echo $this->Form->input('dt_recebimento', $arrConfigInputDtRecebimento);
                                    }
                                    echo $this->Form->input('dt_envio', $arrConfigInputDtEnvio);
                                }

                                echo $this->Form->input('ds_nota', array('class' => 'input-large', 'type' => 'texarea', 'label' => 'Observações',
                                    'onKeyup' => '$(this).limit("500","#charsLeft")',
                                    'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                                ?>
                            </dl>
                        </div>
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <?php
                                if ($this->Modulo->isCamposNota('nu_serie')) {
                                    $arrConfigInputNuSerie = array('class' => 'input-small', 'label' => 'Série');
                                    if ($blockUnputs) {
                                        echo $this->Form->input('nu_seriex', $arrConfigInputNuSerie + array(
                                                'value' => $nota['nu_serie'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        );
                                        echo $this->Form->hidden('nu_serie');
                                    } else {
                                        echo $this->Form->input('nu_serie', $arrConfigInputNuSerie);
                                    }
                                }

                                if ($this->Modulo->isCamposNota('co_nota_categoria')) {
                                    echo $this->Form->input('co_nota_categoria', array('label' => __('Categoria da Nota', true), 'class' => 'chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $categorias,
                                        'after' => '&nbsp;&nbsp;&nbsp;<a class="alert-tooltip" title="Adicionar Categoria" id="AbrirNovaCategoria" href="#view_nova_categoria" data-toggle="modal"><i class="blue icon-edit bigger-150"></i></a>'));
                                    echo '<br>';
                                }

                                $arrConfigInputVlNota = array(
                                    'class' => 'input-large',
                                    'label' => 'Valor Nota (R$)',
                                    'value' => $this->Print->real($nota['vl_nota']),
                                    'maxlength' => 18
                                );

                                if ($blockUnputs) {
                                    echo $this->Form->input('vl_notax', $arrConfigInputVlNota + array(
                                            'value' => $nota['vl_nota'],
                                            'disabled' => 'disabled',
                                            'before' => '<div class="input-append">',
                                            'after' => $containerTooltipInfoSiafi,
                                        )
                                    );
                                    echo $this->Form->hidden('vl_nota');
                                } else {
                                    echo $this->Form->input('vl_nota', $arrConfigInputVlNota);
                                }


                                if ($this->Modulo->isCamposNota('vl_glosa')) {
                                    $arrConfigInputVlGlosa = array(
                                        'class' => 'input-large',
                                        'label' => 'Valor Glosa (R$)',
                                        'value' => $this->Print->real($nota['vl_glosa']),
                                        'maxlength' => 18
                                    );
                                    if ($blockUnputs) {
                                        echo $this->Form->input('vl_glosax', $arrConfigInputVlGlosa + array(
                                                'value' => $nota['vl_glosa'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        );
                                        echo $this->Form->hidden('vl_glosa');
                                    } else {
                                        echo $this->Form->input('vl_glosa', $arrConfigInputVlGlosa);
                                    }
                                }

                                if ($this->Modulo->isCamposNota('dt_nota')) {
                                    $arrConfigInputDtNota = array(
                                        'before' => '<div id="divDtNota" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data da Nota', 'type' => 'text'
                                    );

                                    if ($blockUnputs) {
                                        echo $this->Form->input('dt_notax', array_merge($arrConfigInputDtNota, array(
                                                    'value' => $nota['dt_nota'],
                                                    'disabled' => 'disabled',
                                                    'before' => '<div class="input-append">',
                                                    'after' => $containerTooltipInfoSiafi,
                                                )
                                            )
                                        );
                                        echo $this->Form->hidden('dt_nota');
                                    } else {
                                        echo $this->Form->input('dt_nota', $arrConfigInputDtNota);
                                    }
                                }

                                if ($this->Modulo->isCamposNota('dt_vencimento')) :
                                    echo $this->Form->input('dt_vencimento', array(
                                        'before' => '<div id="divDtVencimento" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data de Vencimento', 'type' => 'text'
                                    ));
                                endif;

                                if ($this->Modulo->notaFiscalCompetencia) {
                                    echo $this->Form->input('ds_competencia', array('class' => 'input-large', 'label' => 'Competência do Serviço'));

                                    echo $this->Form->input('dt_competencia', array(
                                        'before' => '<div id="divDtNota" class="input-append date datetimepickerDtNota">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                        'class' => 'input-small', 'label' => 'Data de Competência', 'type' => 'text',
                                        'id' => 'dtCompetencia',
					'value'	  => '',
                                    ));
                                }

                                //echo $this->Form->input('anexo', array('class' => 'upload-arquivo', 'type' => 'file', 'label' => 'Anexo da Nota'));
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if($this->Modulo->notaFiscalAtesto){ ?>
        
        <div class="span6 ">
            <div class="widget-header widget-header-small"><h4>Atesto da Nota</h4></div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <?php
                                $arrConfigInputIcAtesto = array(
                                    'label' => 'Atestado',
                                    'type' => 'select',
                                    'empty' => 'Selecione...',
                                    'options' => $situacoesAtesto
                                );
                                $arrConfigInputDtAtesto = array(
                                    'before' => '<div id="divDtAtesto" class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy',
                                    'mask' => '99/99/9999',
                                    'class' => 'input-small',
                                    'label' => 'Data do Atesto',
                                    'type' => 'text'
                                );
                                $arrConfigInputDsAtesto = array(
                                    'class' => 'input-large',
                                    'type' => 'texarea',
                                    'label' => 'Motivo/Observação',
                                    'onKeyup' => '$(this).limit("500","#charsLeft1")',
                                    'after' => '<br><span id="charsLeft1"></span> caracteres restantes.'
                                );
                                if ($blockUnputs) {
                                    echo $this->Form->input('ic_atestox', array_merge($arrConfigInputIcAtesto, array(
                                                'value' => $nota['ic_atesto'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('ic_atesto');

                                    echo $this->Form->input('dt_atestox', array_merge($arrConfigInputDtAtesto, array(
                                                'value' => $nota['dt_atesto'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('dt_atesto');

                                    echo $this->Form->input('ds_atestos', array_merge($arrConfigInputDsAtesto, array(
                                                'value' => $nota['ds_atesto'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('ds_atesto');
                                } else {
                                    echo $this->Form->input('ic_atesto', $arrConfigInputIcAtesto);
                                    echo $this->Form->input('dt_atesto', $arrConfigInputDtAtesto);
                                    echo $this->Form->input('ds_atesto', $arrConfigInputDsAtesto);
                                }
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php } ?>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <?php if ($readOnly === false) { ?>
                <button id="btnSalvarGarantia" rel="tooltip" type="submit"
                        class="btn btn-small btn-primary bt-pesquisar" title="Salvar <?php __('Garantia'); ?>"> Salvar
                </button>
                <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                    Limpar
                </button>
                <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
            <?php } ?>
        </div>
    </div>

    <div id="view_nova_categoria" class="modal hide fade maior" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Nova Categoria</h3>
        </div>
        <div class="modal-body-iframe" id="add_categoria">
        </div>
    </div>

</div>
<script type="text/javascript">

    $(function () {

        $("#NotaFiscalVlNota").maskMoney({thousands: '.', decimal: ','});
        $("#NotaFiscalVlGlosa").maskMoney({thousands: '.', decimal: ','});

    });

    $('.upload-arquivo').ace_file_input({
        no_file: 'Nenhum arquivo selecionado ...',
        btn_choose: 'Selecionar',
        btn_change: 'Alterar',
        droppable: false,
        onchange: null,
        thumbnail: false //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        //blacklist:'exe|php'
        //onchange:''
        //
    });


    $("#AbrirNovaCategoria").bind('click', function (e) {
        var url_md = "<?php echo $this->base; ?>/notas_categorias/iframe/";
        $.ajax({
            type: "POST",
            url: url_md,
            data: {},
            success: function (result) {
                $('#add_categoria').html("");
                $('#add_categoria').append(result);
            }
        })
    });

    function atzComboCategoria(co_nota_categoria) {
        $.getJSON("<?php echo $this->Html->url(array('controller' => 'notas_categorias', 'action' => 'listar'))?>", null, function (data) {
            var options = '<option value="">Selecione..</option>';
            $.each(data, function (index, val) {
                options += '<option value="' + index + '">' + val + '</option>';
            });
            $("select#NotaFiscalCoNotaCategoria").html(options);
            $("#NotaFiscalCoNotaCategoria option[value=" + co_nota_categoria + "]").attr("selected", true);
            $("#NotaFiscalCoNotaCategoria").trigger("chosen:updated");
        });
    }

</script>
