<?php
$usuario = $this->Session->read('usuario');
$hasPenalidade = $this->Session->read('hasPenalidade');
?>

<div class="penalidades index">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped" id="tbNota">
        <tr>
            <?php
            $tabela = $this->Modulo->getNotasFiscais();

            foreach ($tabela as $tabelas):
                //if ($this->Modulo->isEmpenho()) {
                    echo '<th>' . $tabelas . '</th>';
                //}
            endforeach;
            ?>

            <th><?php echo __("Ações"); ?></th>
        </tr>
        <?php
        foreach ($notas as $nota) {
            ?>
            <tr>
                <?php
                if ($this->Modulo->isEmpenho()) {
                    $empenhosUtilizados = '';
                    foreach ($nota['Empenho'] as $empenho):
                        $empenhosUtilizados .= $this->Print->printHelp($empenho['nu_empenho'], $empenho['ds_empenho']);
                    endforeach;
                    ?>
                    <td><a href="/notas_fiscais/edit/<?php echo $nota['NotaFiscal']['co_nota'];
                        echo '/' . $coContrato; ?>/readonly"><?php echo $nota['NotaFiscal']["nu_nota"]; ?>&nbsp;</a>
                    </td>
                    <td><a href="/notas_fiscais/edit/<?php echo $nota['NotaFiscal']['co_nota'];
                        echo '/' . $coContrato; ?>/readonly"><?php echo $empenhosUtilizados; ?>&nbsp;</a></td>
                <?php } ?>

                <?php
                if (array_key_exists('nu_nota', $tabela)) {
                    echo '<td>';
                    echo $nota['NotaFiscal']['nu_nota'];
                    if (isset($nota['NotaFiscal']['nu_serie']) && $nota['NotaFiscal']['nu_serie'] != '') {
                        echo "-" . $nota['NotaFiscal']['nu_serie'];
                    }
                    echo '</td>';
                }
                if (array_key_exists('dt_recebimento', $tabela)) {
                ?>
                    <td><?php echo $nota['NotaFiscal']['dt_recebimento']; ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('dt_envio', $tabela)) {
                ?>
                <td><?php echo $nota['NotaFiscal']['dt_envio']; ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('co_categoria', $tabela)) {
                ?>
                    <td><?php echo $nota['NotaCategoria']['ds_nota_categoria']; ?>&nbsp;</td>
                    <?php
                }
                if (array_key_exists('vl_nota', $tabela)) {
                ?>
                <td><?php echo $this->Formatacao->moeda($nota['NotaFiscal']['vl_nota']); ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('nu_glosa', $tabela)) { ?>
                    <td>
                        <?php echo $this->Formatacao->moeda($nota['NotaFiscal']['vl_glosa']); ?>&nbsp;
                    </td>
                <?php
                }
                if (array_key_exists('ds_nota', $tabela)) { ?>
                <td><?php echo $nota['NotaFiscal']['ds_nota']; ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('nu_glosa', $tabela)) { ?>
                <td><?php echo $this->Print->tpIndicador($nota['NotaFiscal']['ic_atesto']); ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('dt_atesto', $tabela)) { ?>
                <td><?php echo $nota['NotaFiscal']['dt_atesto']; ?>&nbsp;</td>
                <?php
                }
                if (array_key_exists('ds_atesto', $tabela)) { ?>
                <td><?php echo $nota['NotaFiscal']['ds_atesto']; ?>&nbsp;</td>
                <?php } ?>

                <?php if ($coEmpenho <= 0) { ?>
                    <td class="actions"><?php $id = $nota['NotaFiscal']['co_nota']; ?>
                        <div class="btn-group acoes">
                            <?php
                            if (Configure::read('App.config.resource.certificadoDigital')) {
                                if ($nota['NotaFiscal']['assinatura_id'] > 0) { ?>
                                <span class="btn" title="" data-original-title="Documento Assinado" data-rel="popover"
                                      data-trigger="hover" data-placement="bottom"
                                      data-content="<b>Data</b>: <?php echo $nota['Assinatura']['created']; ?><BR>
                                        <b>Usuário</b>: <?php echo $nota['Assinatura']['Usuario']['no_usuario']; ?><BR>
                                        <b>Assinado por</b>: <?php echo $nota['Assinatura']['usuario_token']; ?><BR>"
                                      data-html="true">
                                        <i class="icon-certificate green" style="display: inline-block;"></i>
                                    </span><?php
                                } else {
                                    $dataAssinatura = 'Nº da Nota: ' . $nota['NotaFiscal']['nu_nota'] . '<BR>' .
                                        'Recebimento: ' . $nota['NotaFiscal']['dt_recebimento'] . '<BR>' .
                                        'Valor: ' . $this->Formatacao->moeda($nota['NotaFiscal']['vl_nota']);
                                    echo '<a id="' . $id . '" class="v_assinar btn" href="#view_assinar" data-toggle="modal" data-assinatura="' . $dataAssinatura . '"><i class="icon-certificate alert-tooltip" title="Assinar Documento" style="display: inline-block;"></i></a>';
                                }
                            }
                            ?>
                            <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal">
                                <i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Nota"
                                   style="display: inline-block;">
                                </i>
                            </a>
                            <?php
                            echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'notas_fiscais/'));
                            ?>
                        </div>
                    </td>
                <?php } ?>
            </tr>
            <?php
        }
        if (empty($notas)) {
            echo "<td colspan='12'>Não há notas cadastradas.</td>";
        }
        ?>
    </table>
    <?php if ($coEmpenho <= 0) { ?>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            </ul>
        </div>

        <?php
        if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'notas_fiscais/add')) { ?>
            <?php
            //remover gambiarra
            if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'notas_fiscais/add')) { ?>
                <div class="actions">
                    <div class="acoes-formulario-top clearfix">
                        <p class="requiredLegend pull-left">Selecione uma Ação</p>
                        <div class="pull-right btn-group">
                            <a href="<?php echo $this->Html->url(array('controller' => 'notas_fiscais', 'action' => 'add', $coContrato)); ?>"
                               class="btn btn-small btn-primary">Adicionar</a>
                        </div>
                    </div>

                </div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental - Anexar Nota</h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo"></div>
</div>

<?php if (Configure::read('App.config.resource.certificadoDigital')): ?>
    <div id="view_assinar" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('[data-rel=popover]').popover({container: 'body'});

        $('.alert-tooltip').tooltip();

        $('#tbNota tr td a.v_anexo').click(function () {
            var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/nota/" + $(this).attr('id');
            $.ajax({
                type: "POST",
                url: url_an,
                data: {},
                success: function (result) {
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
        });

        $('#tbNota tr td a.v_assinar').click(function () {
            var url_an = "<?php echo $this->base; ?>/notasFiscais/assinar/" + $(this).attr('id'),
                img_lo = $("<img>").attr('src', '<?php echo $this->base; ?>/img/loading.gif');

            $("#view_assinar").html(img_lo);

            $.ajax({
                type: "GET",
                url: url_an,
                success: function (content) {
                    $("#view_assinar").html("");
                    $('#view_assinar').append(content);
                }
            });
        });
    });
</script>