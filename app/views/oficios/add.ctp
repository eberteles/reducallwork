<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="oficios form">
<?php echo $this->Form->create('Oficio', array('url' => "/oficios/add/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Ofício - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'oficios', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Ofícios">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Novo Ofício</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('co_oficio');
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                              
                              echo $this->Form->input('nu_oficio', array('label' => __('Número do ofício', true)) );
                              echo $this->Form->input('dt_recebimento', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Data de recebimento', 'type'=>'text'));
                              
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('pz_resposta', array('label' => __('Prazo (Dias)', true), 'value'=>5) );
                              echo $this->Form->input('dt_resposta', array(
                                          'before' => '<div class="input-append date datetimepicker">', 
                                          'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                          'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                          'class' => 'input-small','label' => 'Data resposta', 'type'=>'text'));
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Ofício"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>

<?php echo $this->Html->scriptStart() ?>

$(document).ready(function() {

    $("#OficioPzResposta").maskMoney({precision:0, allowZero:false, thousands:''});

});

<?php echo $this->Html->scriptEnd() ?>