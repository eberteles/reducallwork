<?php echo $this->Form->create('Operadora'); ?>

    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
        <div class="row-fluid">

            <div class="span12 ">
            <div class="widget-header widget-header-small"><h4><?php __('Operadora'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                            <?php
                            echo $this->Form->input('id');
                            echo $this->Form->input('nome', array('type'=>'textarea','class' => 'input-xlarge', 'label' => 'Nome (Número máximo de caracteres: 100)', 'maxLength' => '100'));
                            ?>
              </div>
            </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

