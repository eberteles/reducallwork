<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Operadoras'); ?></h1></div>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'operadoras/add') ) { ?>        
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'operadoras', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Usuário"><i class="icon-plus icon-white"></i> Nova Operadora</a> 
          </div>
        </div>
      <?php } ?>
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th><?php echo $this->Paginator->sort('Código', 'id');?></th>
		<th><?php echo $this->Paginator->sort('Nome', 'nome');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($operadoras as $operadora):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $operadora['Operadora']['id']; ?>&nbsp;</td>
		<td><?php echo ($operadora['Operadora']['nome']); ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group acoes">	
				<?php echo $this->element( 'actions', array( 'id' => $operadora['Operadora']['id'], 'class' => 'btn', 'local_acao' => 'operadoras/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>



