<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="ordens index">
<!-- h2>< ?php __('Empenhos');?></h2-->

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbEmpenho">
	<tr>
		<th><?php echo $this->Paginator->sort('OB', 'gr_ug_gestao_an_numero_obuq');?></th>
		<th><?php echo $this->Paginator->sort('Data Transação', 'it_da_transacao');?></th>
		<th><?php echo $this->Paginator->sort('Data Emissão', 'it_da_emissao');?></th>
                <th><?php echo $this->Paginator->sort('Valor', 'it_va_evento_sistema');?></th>
		<th><?php echo $this->Paginator->sort('Observação Título', 'it_tx_observacao');?></th>
		<th><?php echo $this->Paginator->sort('Documento Referência', 'gr_an_nu_documento_referencia');?></th>
		<!-- th class="actions">< ?php __('Ações');?></th-->
	</tr>
	<?php
	$i = 0;
	foreach ($ordens as $ordem):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $ordem['OrdemBancaria']['gr_ug_gestao_an_numero_obuq']; ?>&nbsp;</td>
                <td><?php echo FunctionsComponent::data($ordem['OrdemBancaria']['it_da_transacao']); ?>&nbsp;</td>
                <td><?php echo FunctionsComponent::data($ordem['OrdemBancaria']['it_da_emissao']); ?>&nbsp;</td>
                <td><?php echo $this->Formatacao->moeda( $ordem['OrdemBancaria']['it_va_evento_sistema'] ); ?>&nbsp;</td>
                <td><?php echo ($ordem['OrdemBancaria']['it_tx_observacao']); ?>&nbsp;</td>
		<td><?php echo $ordem['OrdemBancaria']['gr_an_nu_documento_referencia']; ?>&nbsp;</td>
		<!-- td class="actions">
			<div class="btn-group acoes">	
                            <a id="< ?php echo $ordem['Empenho']['co_empenho']; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Empenho" style="display: inline-block;"></i></a>
                            < ?php echo $this->element( 'actions', array( 'id' => $ordem['Empenho']['co_empenho'].'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'ctr_emp_' ) ) ?>
			</div>
		</td-->
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <!-- a href="< ?php echo $this->Html->url(array('controller' => 'ordens', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a--> 
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Empenho</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

    $('#tbEmpenho tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/empenho/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });
    
<?php echo $this->Html->scriptEnd() ?>