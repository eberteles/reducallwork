<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="empenhos form">
<?php echo $this->Form->create('OrdemFornecimentoServico', array('type' => 'file', 'url' => "/ordem_fornecimento_servico/edit/$id/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix">
		<p class="requiredLegend pull-left">
			Preencha os campos abaixo para alterar o Empenho - <b>Campos com *
				são obrigatórios.</b>
		</p>
		<div class="pull-right btn-group">
			<a
				href="<?php echo $this->Html->url(array('controller' => 'ordem_fornecimento_servico', 'action' => 'index', $coContrato)); ?>"
				class="btn btn-small btn-primary" title="Listar Empenhos">Listagem</a>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widget-header widget-header-small">
			<h4>Alterar <?php __('Ordem de Fornecimento/Serviço');?></h4>
		</div>
		<div class="widget-body">
			<div class="widget-main">
				<div class="row-fluid">
					<div class="span4">
						<dl class="dl-horizontal">
                      <?php
                    echo $this->Form->hidden('co_ordem_fornecimento_servico');
                    
                    echo $this->Form->hidden('co_contrato', array(
                        'value' => $coContrato
                    ));
                    
                    echo $this->Form->input('nu_ordem', array(
                        'label' => 'Nº Ordem',
                        'mask' => FunctionsComponent::pegarFormato('ordem')
                    ));
                    
                    echo $this->Form->input('dt_emissao', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy',
                        'mask' => '99/99/9999',
                        'class' => 'input-small',
                        'label' => 'Data de Emissão',
                        'type' => 'text'
                    ));
                    ?>
                       </dl>
					</div>
					<div class="span4">
						<dl class="dl-horizontal">                  
                      <?php
                    
                    echo $this->Form->input('dt_inicial', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy',
                        'mask' => '99/99/9999',
                        'class' => 'input-small',
                        'label' => 'Prazo Inicial',
                        'type' => 'text'
                    ));
                    
                    echo $this->Form->input('dt_final', array(
                        'before' => '<div class="input-append date datetimepicker">',
                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                        'data-format' => 'dd/MM/yyyy',
                        'mask' => '99/99/9999',
                        'class' => 'input-small',
                        'label' => 'Prazo Final',
                        'type' => 'text'
                    ));
                    
                    ?>
                       </dl>
					</div>
					<div class="span4">
						<dl class="dl-horizontal">                  
                      <?php
                    echo $this->Form->input('dc_descricao', array(
                        'class' => 'input-xlarge',
                        'label' => 'Descrição dos bens e serviços',
                        'type' => 'texarea',
                        'cols' => '42',
                        'rows' => '4',
                        'onKeyup' => '$(this).limit("500","#charsLeft2")',
                        'after' => '<br><span id="charsLeft2"></span> caracteres restantes.'
                    ));
                    
                    ?>
                       </dl>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<div class="btn-group">
			<button rel="tooltip" type="submit"
				class="btn btn-small btn-primary bt-pesquisar"
				title="Salvar Garantia">Salvar</button>
			<button rel="tooltip" type="reset" title="Limpar dados preenchidos"
				class="btn btn-small" id="Limpar">Limpar</button>
			<button rel="tooltip" type="reset" title="Limpar dados preenchidos"
				class="btn btn-small" id="Voltar">Voltar</button>
		</div>
	</div>
</div>

<?php echo $this->Html->scriptStart()?>
    
    
    $('#OrdemFornecimentoServicoDocAnexo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
    });
        
<?php echo $this->Html->scriptEnd()?>
