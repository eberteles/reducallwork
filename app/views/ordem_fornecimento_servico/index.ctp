<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="ordens index">
<h2><?php __('Ordem de Fornecimento/Serviço');?></h2>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbOrdemFornecimento">
	<tr>
		<th><?php echo $this->Paginator->sort('Nº Ordem', 'nu_ordem');?></th>
		<th><?php echo $this->Paginator->sort('Data de Emissão', 'dt_emissao');?></th>
        <th><?php echo $this->Paginator->sort('Data Inicial', 'dt_inicial');?></th>
        <th><?php echo $this->Paginator->sort('Data Final', 'dt_final');?></th>
		<th><?php echo $this->Paginator->sort('Descrição dos Bens/Serviço', 'dc_descricao');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ordens_fornecimento as $ordem):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	
	$caminho_arquivo = $this->Html->url(array('controller' => 'ordem_fornecimento_servico', 'action' => 'lerArquivo', $ordem['OrdemFornecimentoServico']['co_ordem_fornecimento_servico']));
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $this->Print->numeroDeOrdem($ordem['OrdemFornecimentoServico']['nu_ordem']); ?>&nbsp;</td>
        <td><?php echo $ordem['OrdemFornecimentoServico']['dt_emissao']; ?>&nbsp;</td>
        <td><?php echo $ordem['OrdemFornecimentoServico']['dt_inicial']; ?>&nbsp;</td>
        <td><?php echo $ordem['OrdemFornecimentoServico']['dt_final']; ?>&nbsp;</td>
        <td><?php echo $ordem['OrdemFornecimentoServico']['dc_descricao']; ?>&nbsp;</td>
	    <td class="actions"><?php $id = $ordem['OrdemFornecimentoServico']['co_ordem_fornecimento_servico']; ?> 
		    <div class="btn-group acoes">
		        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Ordem de Fornecimento e Serviço" style="display: inline-block;"></i></a>
				<?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'ordem_fornecimento_servico/index' ) )?>
		</div></td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'empenhos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'ordem_fornecimento_servico', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a> 
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Ordem de Fornecimento e Serviço</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

    $('#tbOrdemFornecimento tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/ordem_de_fornecimento/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });
    
<?php echo $this->Html->scriptEnd() ?>