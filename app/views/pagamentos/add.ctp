<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<?php $usuario = $this->Session->read('usuario'); ?>

<script>

$(document).ready(function(){

$(this).limit("250","#charsLeft")

});

</script>

<div class="pagamentos form">
    <?php echo $this->Form->create('Pagamento', array('url' => "/pagamentos/add/$coContrato/$coAtividade")); ?>
    <?php
    echo $this->Form->hidden('nu_nota_fiscal');
    echo $this->Form->hidden('co_pagamento');
    echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
    echo $this->Form->hidden('co_atividade', array('value' => $coAtividade));
    echo $this->Form->hidden('co_usuario', array('value' => $usuario['Usuario']['co_usuario']));
    ?>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um <?php echo __('Pagamento'); ?> - <b>Campos com * são obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'pagamentos', 'action' => 'index', $coContrato, $coAtividade)); ?>" class="btn btn-small btn-primary" title="Listar Pagamentos">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span10">
            <div class="widget-header widget-header-small"><h4>Novo <?php echo __('Pagamento'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                      <div class="span12">
                          <?php echo $this->Form->input('co_notas', array('label' => __('Notas Fiscais', true) . '<b><font color="red"> *</font></b>', 'multiple'=>false, 'data-placeholder'=>'Selecione a Nota que este ' . __('Pagamento', true) . ' atende...', 'class' => 'input-xxlarge chosen-select', 'type' => 'select', 'options' => $notas)); ?>
                      </div>
                  </div>
                  <div class="row-fluid">
                    <div class="span2">
                        <?php 
                        echo $this->Form->input('repetir_pagamento', array('class' => 'input-small', 'type' => 'select', 'empty' => 'Não', 'label' => 'Repetir este pagamento?', 'options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11)));
                        echo $this->Form->input('nu_mes_pagamento', array('class' => 'input-small', 'label' => 'Mês Competência', 'mask' => '99'));
                        ?>
                    </div>

                        <div class="span2">
                            <div class="input text">
                                <label><?php echo __('Pagamento'); ?> já realizado?</label>
                                <input name="pgtRealizado" type="radio" id="jaRealizadoSim" onchange="verificaPagamento()" onclick=" document.getElementById('PagamentoDtPagamento').disabled = false;" /> Sim &nbsp;&nbsp;&nbsp;
                                <input name="pgtRealizado" type="radio" id="jaRealizadoNao" onchange="verificaPagamento()" onclick=" document.getElementById('PagamentoDtPagamento').disabled = true;"  checked="checked" /> Não
                            </div>
                            <span id="MsgPgtEfetuado" style="color: red;font-weight: bold;">Por favor, preencha a data do <?php echo __('Pagamento'); ?></span>
                            <br>
                            <?php
                            echo $this->Form->input('nu_ano_pagamento', array('class' => 'input-small', 'label' => 'Ano', 'mask' => '9999'));
                            ?>
                        </div>

                        <div class="span3">
                            <?php
                            echo $this->Form->input('ds_observacao', array('label'=>'Observações','type' => 'texarea', 'cols'=>'45', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("250","#charsLeft")',
                                'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                            echo '<br />';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <div class="row-fluid">

        <div class="span6 ">
            <div class="widget-header widget-header-small"><h4><?php __('Valores'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                            <?php
                            if($isAtividade != 0) {
                                echo $this->Form->input('vl_orcamento', array('class' => 'input-medium', 'label' => 'Saldo/Orçamento Disponível', 'maxlength' => 18));
                            }
                            echo $this->Form->input('vl_pagamento', array('class' => 'input-medium', 'label' => 'Valor Bruto (R$)', 'maxlength' => 18));
                            echo $this->Form->input('vl_liquido', array('class' => 'input-medium', 'label' => 'Valor Líquido (R$)', 'maxlength' => 18));
                            ?>
                        </div>
                        <div class="span6">
                            <?php
                            echo $this->Form->input('vl_imposto', array('class' => 'input-medium', 'label' => __('Valor Imposto (R$)', true), 'maxlength' => 18));
                            echo $this->Form->input('vl_pago', array('class' => 'input-medium', 'label' => $this->Print->getLabelVlPago($ic_tipo_contrato), 'maxlength' => 18 ));
                            ?>
                            <div id="erro_vl_glosa" class="error-message">Valor acima do permitido!</div>
                            <div id="erro_max_vl_glosa" class="error-message"><?php echo __('Pagamento'); ?> não permitido!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span4 ">
            <div class="widget-header widget-header-small"><h4><?php __('Datas'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                            <?php
                            echo $this->Form->input('dt_vencimento', array(
                                'before' => '<div class="input-append date datetimepicker">',
                                'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data Vencimento', 'type'=>'text'));
                            ?>
                        </div>

                        <div class="span6">
                            <?php
                            echo $this->Form->input('dt_pagamento', array(
                                'before' => '<div class="input-append btn_datetimepicker date datetimepicker">',
                                'after' => '<span class="add-on btn_calendar"><i data-date-icon="icon-calendar"></i></span></div>',
                                'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                'class' => 'input-small','label' => 'Data ' . __('Pagamento', true), 'type'=>'text', 'disabled' => 'true'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button id="salvarPagamento" rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar <?php echo __('Pagamento'); ?>"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<div id="financeiro" title="Consulta ao Financeiro">
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#MsgPgtEfetuado").hide();
        $(".btn_calendar").hide();
        $('#erro_vl_glosa').hide();
        $('#erro_max_vl_glosa').hide();

        $('#PagamentoCoNotas').change(function () {
            verificaNotaFiscal();
        })

        $("form").submit(function(){
            if (!verificaNotaFiscal()) {
                return false;
            }

            var val = $('#PagamentoVlPagamento').val();
            var vlCampo = val;

            for (var i = 0; i < (val.split('.').length -1) ; i++) {
                vlCampo = vlCampo.replace('.', '');
            }

            $('#PagamentoVlPagamento').val(vlCampo);
        });

        $("#PagamentoDtPagamento").on('blur', function(){
            if( $(this).val() ) {
                $("#MsgPgtEfetuado").hide();
                $("#PagamentoDtPagamento").css('borderColor', '');
            } else {
                $("#MsgPgtEfetuado").show();
                $("#PagamentoDtPagamento").css('borderColor', 'red');
            }
        });
    });

    function verificaNotaFiscal(){
        var totalNotas = 0;
        var newValue = null;
        var vlGlosa = 0;

        $('#PagamentoCoNotas_chosen').children().each(function() {
            if($(this).attr('class') == 'chosen-single'){
                var values = $(this).text().split("R$");
                if( $(this).text() != 'Selecione...' ) {
                    values.forEach(function(val, index){
                        if(val.indexOf(";") < 0){
                            var nu_nota = values[index - 1].split(';');

                            $('#PagamentoNuNotaFiscal').val(nu_nota[0].trim());

                            var value = val;
                            newValue = val;

                            for(var i = 0; i < (value.split('.').length -1) ; i++ ){
                                newValue = newValue.replace('.', '');
                            }

                            <?php foreach ($glosas as $glosa) : ?>

                            if(nu_nota[0].trim() == '<?php echo $glosa['nu_nota']; ?>'){
                                vlGlosa = '<?php echo $glosa['vl_glosa']; ?>';
                            }

                            <?php endforeach; ?>

                            totalNotas = totalNotas + newValue.trim().replace(',','.');
//                            totalNotas = totalNotas + newValue.trim();
                        }
                    });
                }
            }
        });

        $('#PagamentoVlPagamento').val(totalNotas);
        $('#PagamentoVlPagamento').focus();
        $('#PagamentoVlPagamento').attr('readonly', true);

        var enquantoVal = $('#PagamentoVlPago').val();
        var valorCampo = enquantoVal;

        for (var i = 0; i < (enquantoVal.split('.').length -1) ; i++) {
            valorCampo = valorCampo.replace('.', '');
        }

        valorCampo = parseFloat(valorCampo.replace(',', '.'));

        if(totalNotas == 0 || isNaN(totalNotas)) {
            alert('Selecione pelo menos uma nota antes de inserir o valor bruto.');
            $('#PagamentoVlPago').val('');
            return false;
        } else if(valorCampo == 0) {
            alert('O campo de valor pago não pode ser zero ou vazio.');
            return false;
        } else if (valorCampo > totalNotas) {
            alert('O campo de valor pago não pode ser maior que a soma do valor das notas vinculadas ao <?php echo __('Pagamento'); ?>.');
            return false;
        } else if(document.getElementById("jaRealizadoSim").checked) {
            if($("#PagamentoDtPagamento").val() == ''){
                alert('Campo Data pagamento em branco.');
                return false;
            }
        }

        //Restringe o valor maximo para o valor pago
        if(verificarImpostos(vlGlosa)){
            return true;
        }

    }

    function verificarImpostos(vlGlosa) {
        var impostos = parseFloat( (vlGlosa != '') ? vlGlosa : 0 ) + parseFloat( ($('#PagamentoVlImposto').val().replace(',', '.') != '' ) ? $('#PagamentoVlImposto').val().replace(',', '.') : 0 );
        var maxValPago = $('#PagamentoVlPagamento').val() - impostos;

        if(maxValPago < 0 || impostos > $('#PagamentoVlPagamento').val()) {
            $('#erro_max_vl_glosa').show();
            return false;
        } else if($('#PagamentoVlPago').val().replace(',', '.') > maxValPago) {
            $('#erro_max_vl_glosa').hide();
            $('#erro_vl_glosa').show();
            return false;
        } else {
            $('#erro_vl_glosa').hide();
            return true;
        }
    }

    function verificaPagamento() {
        var dtPagamento = $("#PagamentoDtPagamento").val();
        if(document.getElementById("jaRealizadoSim").checked) {
            $(".btn_calendar").show();
        } else if(document.getElementById("jaRealizadoNao").checked) {
            $(".btn_calendar").hide();
        }
        if(document.getElementById("jaRealizadoSim").checked == true && dtPagamento == "") {
            $("#MsgPgtEfetuado").show();
            $("#PagamentoDtPagamento").css('borderColor', 'red');
        } else {
            $("#MsgPgtEfetuado").hide();
            $("#PagamentoDtPagamento").css('borderColor', '');
            $("#PagamentoDtPagamento").val('');
        }
    }



    $(function(){
        $("#PagamentoVlNota").maskMoney({thousands:'.', decimal:','});
        $("#PagamentoVlImposto").maskMoney({thousands:'.', decimal:','});
        $("#PagamentoVlLiquido").maskMoney({formatOnBlur: true, thousands:'.', decimal:','});
        $("#PagamentoVlPagamento").maskMoney({thousands:'.', decimal:','});
        $("#PagamentoVlPago").maskMoney({thousands:'.', decimal:','});
        $("#PagamentoVlOrcamento").maskMoney({thousands:'.', decimal:','});
    });

    <?php if ($temFornecedor) { ?>

    $("#showFinanceiro").click( function(){
        $(function(){
            <?php echo $this->JqueryEngine->request("/financeiros/iframe/$coContrato", array('update' => '#financeiro')); ?>

            $("#financeiro").dialog({
                height: 420,
                width: 700,
                modal: true
            });

        });
    });

    <?php } else { ?>

    $("#showFinanceiro").click( function(){
        alert('É necessário cadastrar um Fornecedor para utilizar este recurso.');
    });

    <?php } ?>

</script>

