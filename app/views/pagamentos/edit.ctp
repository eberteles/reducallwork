<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<?php $usuario = $this->Session->read('usuario'); ?>
    <div class="pagamentos form">
        <?php
        echo $this->Form->create('Pagamento', array('url' => "/pagamentos/edit/$id/$coContrato/$coAtividade"));

        echo $this->Form->hidden('co_pagamento');
        echo $this->Form->hidden('ic_importacao_siafi');
        echo $this->Form->hidden('co_contrato');
        echo $this->Form->hidden('co_usuario', array('value' => $usuario['Usuario']['co_usuario']));


        $blockUnputs = false;
        if ($this->data['Pagamento']['ic_importacao_siafi'] === 'S') {
            $blockUnputs = true;
            $containerTooltipInfoSiafi = '<span class="add-on alert-tooltip" title="A edição do campo é bloqueada quando esse é preenchido automaticamento com informações do SIAFI"><i class="icon-info-sign"></i></span></div>';
        }
        ?>

        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar o <?php echo __('Pagamento'); ?>
                - <b>Campos com * são obrigatórios.</b></p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'pagamentos', 'action' => 'index', $coContrato, $coAtividade)); ?>"
                   class="btn btn-small btn-primary" title="Listar <?php echo __('Pagamentos'); ?>">Listagem</a>
            </div>
        </div>

        <div class="row-fluid">

            <div class="span10">
                <div class="widget-header widget-header-small"><h4>Alterar <?php echo __('Pagamento'); ?></h4></div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span12">
                                <?php echo $this->Form->input('co_notas', array('label' => __('Notas Fiscais', true) . '<b><font color="red"> *</font></b>', 'multiple' => true, 'data-placeholder' => 'Selecione as Notas que este ' . __('Pagamento', true) . ' atende...', 'class' => 'input-xxlarge chosen-select', 'type' => 'select', 'options' => $notas, 'selected' => $notasPagamento)); ?>
                            </div>
                        </div>
                        <div class="row-fluid">

                            <div class="span2">
                                <?php if ($blockUnputs): ?>
                                    <input id="jaRealizadoSim" name="pgtRealizado" type="radio"
                                           disabled="disabled"
                                        <?php if (!empty($pagamento['dt_pagamento'])) {
                                            echo 'checked="checked"';
                                        } ?>/> Sim &nbsp;&nbsp;&nbsp;
                                    <input name="pgtRealizado" type="radio"
                                           disabled="disabled"
                                        <?php if (empty($pagamento['dt_pagamento'])) {
                                            echo '"checked="checked"';
                                        } ?> /> Não
                                <?php else: ?>
                                    <?php echo __('Pagamento'); ?> já realizado? <br>
                                    <input id="jaRealizadoSim" name="pgtRealizado" type="radio"
                                           onchange="verificaPagamento()"
                                           onclick=" document.getElementById('PagamentoDtPagamento').disabled = false ;" <?php if (!empty($pagamento['dt_pagamento'])) {
                                        echo '"checked="checked"';
                                    } ?>/> Sim &nbsp;&nbsp;&nbsp;
                                    <input name="pgtRealizado" type="radio" onchange="verificaPagamento()"
                                           onclick=" document.getElementById('PagamentoDtPagamento').disabled = true ;" <?php if (empty($pagamento['dt_pagamento'])) {
                                        echo 'checked="checked"';
                                    } ?> /> Não

                                    <br>
                                    <span id="MsgPgtEfetuado" style="color: red;font-weight: bold;">Por favor, preencha a data do pagamento</span>
                                <?php endif; ?>
                            </div>

                            <div class="span2">
                                <?php
                                $arrConfigInputMesPagamento = array('class' => 'input-small', 'label' => 'Mês Competência', 'mask' => '99');
                                $arrConfigInputAnoPagamento = array('class' => 'input-small', 'label' => 'Ano', 'mask' => '9999');
                                if ($blockUnputs) {
                                    echo $this->Form->input('nu_mes_pagamentox', array_merge($arrConfigInputMesPagamento, array(
                                                'value' => $pagamento['nu_mes_pagamento'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('nu_mes_pagamento');

                                    echo $this->Form->input('nu_ano_pagamentox', array_merge($arrConfigInputAnoPagamento, array(
                                                'value' => $pagamento['nu_ano_pagamento'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('nu_ano_pagamento');
                                } else {
                                    echo $this->Form->input('nu_mes_pagamento', $arrConfigInputMesPagamento);
                                    echo $this->Form->input('nu_ano_pagamento', $arrConfigInputAnoPagamento);
                                }
                                ?>
                            </div>

                            <!--                    <div class="span4">
                                                    < ?php
                                                    if ($this->Modulo->isEmpenho()) {
                                                        echo $this->Form->input('co_empenho', array('label' => 'Empenho', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $empenhos));
                                                    }
                                                    if ($this->Modulo->isExecucao()) {
                                                        echo '<div class="input select"><label for="PagamentoCoAtividade">Atividade</label>';
                                                        echo '<select name="data[Pagamento][co_atividade]" id="PagamentoCoAtividade">';
                                                        echo '<option value="">Selecione...</option>';
                                                        foreach ($atividades as $atividade):
                                                            $id_selecionado = "";
                                                            if ($pagamento['co_atividade'] == $atividade['Atividade']['co_atividade']) {
                                                                $id_selecionado = 'selected="selected"';
                                                            }
                                                            echo '<option value="' . $atividade['Atividade']['co_atividade'] . '" ' . $id_selecionado. '>' . $atividade['Atividade']['ds_atividade'] . '</option>';
                                                            foreach ($atividade['children'] as $tarefa):
                                                                $id_selecionado = "";
                                                                if ($pagamento['co_atividade'] == $tarefa['Atividade']['co_atividade']) {
                                                                    $id_selecionado = 'selected="selected"';
                                                                }
                                                                echo '<option value="' . $tarefa['Atividade']['co_atividade'] . '" ' . $id_selecionado. '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $tarefa['Atividade']['ds_atividade'] . '</option>';
                                                            endforeach;
                                                        endforeach;
                                                        echo '</select></div>';
                                                    }
                                                    ? >
                                                </div>-->

                            <div class="span3">
                                <?php
                                echo $this->Form->input('ds_observacao', array('label' => 'Observações', 'type' => 'texarea', 'cols' => '45', 'rows' => '4',
                                    'onKeyup' => '$(this).limit("250","#charsLeft")',
                                    'after' => '<br><span id="charsLeft"></span> caracteres restantes.'));
                                echo '<br />';
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <br>

        <div class="row-fluid">

            <div class="span6">
                <div class="widget-header widget-header-small"><h4><?php __('Valores'); ?></h4></div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <!--                    <div class="span4">
                                                  < ?php
                                                      echo $this->Form->input('nu_nota_fiscal', array('class' => 'input-medium', 'label' => 'Nota Fiscal'));
                                                      echo $this->Form->input('nu_serie_nf', array('class' => 'input-medium', 'label' => 'N° Série '));
                                                  ? >
                                                </div>-->
                            <div class="span6">
                                <?php
                                $arrConfigInputVlPagamento = array('class' => 'input-medium', 'label' => 'Valor Bruto (R$)', 'value' => $this->Print->real($pagamento['vl_pagamento']), 'maxlength' => 18);
                                if ($blockUnputs) {
                                    echo $this->Form->input('vl_pagamentox', array_merge($arrConfigInputVlPagamento, array(
                                                'value' => $pagamento['vl_pagamento'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->input('vl_pagamento');
                                } else {
                                    echo $this->Form->input('vl_pagamento', $arrConfigInputVlPagamento);
                                }
                                echo $this->Form->input('vl_imposto', array('class' => 'input-medium', 'label' => __('Valor Imposto (R$)', true), 'value' => $this->Print->real($pagamento['vl_imposto']), 'maxlength' => 18));
                                ?>
                            </div>
                            <div class="span6">
                                <?php
                                echo $this->Form->input('vl_liquido', array('class' => 'input-medium', 'label' => 'Valor Líquido (R$)', 'value' => $this->Print->real($pagamento['vl_liquido']), 'maxlength' => 18));
                                echo $this->Form->input('vl_pago', array('class' => 'input-medium', 'label' => $this->Print->getLabelVlPago($ic_tipo_contrato), 'value' => $this->Print->real($pagamento['vl_pago']), 'maxlength' => 18));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span4 ">
                <div class="widget-header widget-header-small"><h4><?php __('Datas'); ?></h4></div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span6">
                                <?php
                                echo $this->Form->input('dt_vencimento', array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data Vencimento', 'type' => 'text'));
                                //                          echo $this->Form->input('dt_administrativo', array(
                                //                                      'before' => '<div class="input-append date datetimepicker">',
                                //                                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                //                                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                //                                      'class' => 'input-small','label' => __('Data do envio ADM', true), 'type'=>'text'));
                                ?>
                            </div>
                            <div class="span6">
                                <?php
                                $arrConfigInputDtPagamento = array(
                                    'before' => '<div class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy',
                                    'mask' => '99/99/9999',
                                    'class' => 'input-small',
                                    'label' => 'Data ' . __('Pagamento', true),
                                    'type' => 'text',
                                    'disabled' => 'true'
                                );

                                if ($blockUnputs) {
                                    echo $this->Form->input('dt_pagamentox', array_merge($arrConfigInputDtPagamento, array(
                                                'value' => $pagamento['dt_pagamento'],
                                                'disabled' => 'disabled',
                                                'before' => '<div class="input-append">',
                                                'after' => $containerTooltipInfoSiafi,
                                            )
                                        )
                                    );
                                    echo $this->Form->hidden('dt_pagamento');
                                } else {
                                    echo $this->Form->input('dt_pagamento', $arrConfigInputDtPagamento);
                                    //                          echo $this->Form->input('dt_financeiro', array(
                                    //                                      'before' => '<div class="input-append date datetimepicker">',
                                    //                                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    //                                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                    //                                      'class' => 'input-small','label' => 'Data do envio Financeiro', 'type'=>'text'));
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                        title="Salvar <?php echo __('Pagamento'); ?>"> Salvar
                </button>
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                    Limpar
                </button>
                <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#MsgPgtEfetuado").hide();
        });

        function verificaPagamento() {
            var dtPagamento = $("#PagamentoDtPagamento").val();
            console.log(dtPagamento);
            if (document.getElementById("jaRealizadoSim").checked == true && dtPagamento == "") {
                $("#MsgPgtEfetuado").show();
                document.getElementById("PagamentoDtPagamento").style.borderColor = "red";
            } else {
                $("#MsgPgtEfetuado").hide();
                document.getElementById("PagamentoDtPagamento").style.borderColor = "";
            }
        }
    </script>

<?php echo $this->Html->scriptStart() ?>

    $(function(){
    $("#PagamentoVlNota").maskMoney({thousands:'.', decimal:','});
    $("#PagamentoVlImposto").maskMoney({thousands:'.', decimal:','});
    $("#PagamentoVlLiquido").maskMoney({thousands:'.', decimal:','});
    $("#PagamentoVlPagamento").maskMoney({thousands:'.', decimal:','});
    $("#PagamentoVlPago").maskMoney({thousands:'.', decimal:','});
    })

<?php echo $this->Html->scriptEnd() ?>
