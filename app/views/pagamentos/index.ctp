<?php echo $this->Html->scriptStart() ?>

$( function() {
parent.atualizaTotalizaValores();
parent.atualizaTotalizaPercentuais();
});

<?php echo $this->Html->scriptEnd() ?>

<?php
$usuario = $this->Session->read('usuario');
$hasPenalidade = $this->Session->read('hasPenalidade');
if (isset($pendencias)) {
    ?>
    <div class="alert alert-error">
        <?php echo $pendencias; ?>
    </div>
<?php } ?>

<div class="pagamentos index">
    <?php
    if ($this->Modulo->isExecucao() && $coAtividade <= 0) {
        echo '<div class="input select"><label for="CoAtividade">Atividades da Execução</label>';
        echo '<select name="co_atividade" id="co_atividade">';
        echo '<option value="">Selecione...</option>';
        foreach ($listAtividades as $atividade):
            $id_selecionado = "";
            if ($filtroAtividade == $atividade['Atividade']['co_atividade']) {
                $id_selecionado = 'selected="selected"';
            }
            echo '<option value="' . $atividade['Atividade']['co_atividade'] . '" ' . $id_selecionado . '>' . $atividade['Atividade']['ds_atividade'] . '</option>';
            foreach ($atividade['children'] as $tarefa):
                $id_selecionado = "";
                if ($filtroAtividade == $tarefa['Atividade']['co_atividade']) {
                    $id_selecionado = 'selected="selected"';
                }
                echo '<option value="' . $tarefa['Atividade']['co_atividade'] . '" ' . $id_selecionado . '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $tarefa['Atividade']['ds_atividade'] . '</option>';
            endforeach;
        endforeach;
        echo '</select></div>';
    }
    ?>
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped" id="tbPagamento">
        <tr>
            <th><?php __('Notas'); ?></th>
            <?php
            if ($this->Modulo->isEmpenho()) {
                ?>
                <th><?php __('Empenhos'); ?></th>
                <?php
            }
            ?>
            <th><?php __('Mês'); ?>/<?php __('Ano'); ?></th>
            <?php if ($this->Modulo->isExecucao()) {
                echo "<th>" . __('Atividade', true) . "</th>";
            } ?>
            <th><?php __('Vencimento'); ?></th>
            <th><?php __('Envio ADM'); ?></th>
            <th><?php __('Envio Financeiro'); ?></th>
            <th><?php echo $this->Print->getLabelDtPagamento($ic_tipo_contrato); ?></th>
            <th><?php echo $this->Print->getLabelVlPagamento($ic_tipo_contrato); ?></th>
            <th><?php __('Valor Imposto (R$)'); ?></th>
            <th><?php __('Líquido'); ?></th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;

        foreach ($pagamentos as $k => $pagamento):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>

            <tr>
                <td><?php echo $pagamentos[$k]['Pagamento']['nu_nota_fiscal']; ?></td>
                <?php
                if ($this->Modulo->isEmpenho()): ?>
                    <td><?php echo isset($pagamentos[$k]['Pagamento']['empenhos_utilizados']) ? $pagamentos[$k]['Pagamento']['empenhos_utilizados'] : '-' ?></td>
                <?php endif; ?>
                <td><?php echo $pagamento['Pagamento']['nu_mes_pagamento']; ?>
                    /<?php echo $pagamento['Pagamento']['nu_ano_pagamento']; ?>
                </td>
                <?php if ($this->Modulo->isExecucao()) {
                    echo "<td>" . $pagamento['Atividade']['ds_atividade'] . "</td>";
                } ?>
                <td><?php echo $pagamento['Pagamento']['dt_vencimento']; ?></td>
                <td><?php echo $pagamento['Pagamento']['dt_administrativo']; ?></td>
                <td><?php echo $pagamento['Pagamento']['dt_financeiro']; ?></td>
                <td><?php echo $pagamento['Pagamento']['dt_pagamento']; ?></td>
                <td><?php echo $this->Formatacao->moeda($pagamento['Pagamento']['vl_pagamento']); ?></td>
                <td><?php echo $this->Formatacao->moeda($pagamento['Pagamento']['vl_imposto']); ?></td>
                <td><?php echo $this->Formatacao->moeda($pagamento['Pagamento']['vl_liquido']); ?></td>
                <td class="actions"><?php $id = $pagamento['Pagamento']['co_pagamento']; ?>
                    <div class="btn-group acoes">
                        <a id="<?php echo $id ?>"
                           class="v_anexo btn" href="#view_anexo" data-toggle="modal">
                            <i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Pagamento"
                               style="display: inline-block;"></i>
                        </a>

                <?php if(!$hasPenalidade) : ?>
                    <a href="/pagamentos/delete/<?php echo $id?>/<?php echo $coContrato?>/<?php echo $coAtividade?>"
                       class="btn btn-danger alert-tooltip" title="Excluir"
                       onclick="return confirm('Tem certeza de que deseja excluir # ' + <?php echo $id?> + '/' + <?php echo $coContrato?> + '/' + <?php echo $coAtividade?> + '?');">
                        <i class="icon-trash icon-white"></i>
                    </a>
                <?php endif; ?>

                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>

</div>

<?php
if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pagamentos/add')) { ?>
    <div class="actions">
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">Selecione uma Ação
                <!--span class="required" title="Required">*</span--></p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'pagamentos', 'action' => 'add', $coContrato, $coAtividade)); ?>"
                   class="btn btn-small btn-primary">Adicionar</a>
            </div>

        </div>

    </div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental -
            Anexar <?php $this->Print->getLabelPagamento($ic_tipo_contrato); ?></h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo">
    </div>
</div>

<?php echo $this->Html->scriptStart() ?>

$('.alert-tooltip').tooltip();

$('#tbPagamento tr td a.v_anexo').click(function(){
var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/pagamento/" + $(this).attr('id');
$.ajax({
type:"POST",
url:url_an,
data:{},
success:function(result){
$('#fis_anexo').html("");
$('#fis_anexo').append(result);
}
})
});

$("#co_atividade").change(function() {
var id_selecionado  = $(this).val();
var url_filtro  = "<?php echo $this->Html->url(array('controller' => 'pagamentos', 'action' => 'index', $coContrato, $coAtividade)); ?>" + "/" + id_selecionado;
$(location).attr('href',url_filtro);
});

<?php echo $this->Html->scriptEnd() ?>
