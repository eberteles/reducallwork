<div class="auxiliares index">
	<!--h2>< ?php __('Cadastro');?></h2-->
    
    <fieldset><legend><?php __('Cadastro'); ?></legend>
	
	<div class="actions">
		<ul>
			<!--li>< ?php echo $this->Html->link(__('Perfis de Acesso', true), array('controller' => 'privilegios', 'action' => 'index')); ?></li-->
                        <li><?php echo $this->Html->link(__('Setores', true), array('controller' => 'setores', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Tipo de Contrato', true), array('controller' => 'modalidades', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Modalidade de Contratação', true), array('controller' => 'contratacoes', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Andamento Processual', true), array('controller' => 'situacoes', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Descrição do Serviço', true), array('controller' => 'servicos', 'action' => 'index')); ?></li>
                        <li><?php echo $this->Html->link(__('Fiscalização', true), array('controller' => 'fiscais', 'action' => 'index')); ?></li>
		</ul>
	</div>
	</fieldset>			
</div>
