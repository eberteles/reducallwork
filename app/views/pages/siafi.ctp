<div class="relatorios index">
    <!-- h2>< ?php __('Relatórios');?></h2 -->

    <div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Importar/Exportar arquivo SIAFI'); ?></h1></div>
        <div>
            <?php
                echo $this->Form->create('siafi', array('enctype' => 'multipart/form-data', 'action' => 'upload'));
                echo $this->Form->file('arquivo', array('type'=>'file'));
                echo '<br/><button type="submit" class="btn">Enviar</button>';
                echo $this->Form->end(); 
            ?>
        </div>
    </div>
</div>
