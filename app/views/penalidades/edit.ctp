<?php
echo $this->Html->script('inicia-datetimepicker');
?>
<div class="aditivos form">
    <?php echo $this->Form->create('Penalidade', array('url' => array('controller' => 'penalidades', 'action' => 'edit', $id, $coContrato))); ?>
    <div class="acoes-formulario-top clearfix">
        <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar a Penalidade - <b>Campos com * são
                obrigatórios.</b></p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'penalidades', 'action' => 'index', $coContrato)); ?>"
               class="btn btn-small btn-primary" title="Listar Penalidades">Listagem</a>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span12 ">
            <div class="widget-header widget-header-small"><h4>Alterar Penalidade</h4></div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->hidden('co_penalidade');
                                echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                                if ($this->Modulo->notaFiscalInPenalidade) {
                                    echo $this->Form->input('co_nota', array('label' => 'Nota Fiscal', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $notasFiscais));
                                }

                                echo $this->Form->input('dt_penalidade', array(
                                    'before' => '<div id="divDtPenalidade" class="input-append date datetimepicker">',
                                    'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                    'data-format' => 'dd/MM/yyyy', 'mask' => '99/99/9999',
                                    'class' => 'input-small', 'label' => 'Data da Penalidade', 'type' => 'text'));

                                echo $this->Form->input('ds_observacao', array('label' => 'Observações'));

                                echo $this->Form->input('dt_ini_vigencia', array(
                                        'id' => 'dtIniVigencia',
                                        'type' => 'text',
                                        'before' => '<div id="" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'label' => 'Início da penalidade',
                                        'class' => 'input-small',
                                        'data-format' => 'dd/MM/yyyy',
                                        'mask' => '99/99/9999'
                                    )
                                );

                                echo $this->Form->input('dt_fim_vigencia', array(
                                        'id' => 'dtFimVigencia',
                                        'type' => 'text',
                                        'before' => '<div id="" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'label' => 'Fim da penalidade',
                                        'class' => 'input-small',
                                        'data-format' => 'dd/MM/yyyy',
                                        'mask' => '99/99/9999'
                                    )
                                );

                                echo $this->Form->input('dt_publicacao', array(
                                        'id' => 'dtPublicacao',
                                        'type' => 'text',
                                        'before' => '<div id="" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'label' => 'Data de publicação',
                                        'class' => 'input-small',
                                        'data-format' => 'dd/MM/yyyy',
                                        'mask' => '99/99/9999'
                                    )
                                );

                                echo $this->Form->input('dt_registro_sicaf', array(
                                        'id' => 'dtRegistroSicaf',
                                        'type' => 'text',
                                        'before' => '<div id="" class="input-append date datetimepicker">',
                                        'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                        'label' => 'Data de registro da penalidade SICAF',
                                        'class' => 'input-small',
                                        'data-format' => 'dd/MM/yyyy',
                                        'mask' => '99/99/9999'
                                    )
                                );

                                ?>
                            </dl>
                        </div>
                        <div class="span4">
                            <dl class="dl-horizontal">
                                <?php
                                echo $this->Form->input('co_tipo_penalidade', array(
                                        'label' => 'Situação da Penalidade',
                                        'type' => 'select',
                                        'empty' => 'Selecione...',
                                        'options' => $situacoes
                                    )
                                );
                                echo $this->Form->input('mt_penalidade', array(
                                        'label' => 'Motivo da Penalidade'
                                    )
                                );
                                ?>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Salvar Penalidade"> Salvar
            </button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm">
                Limpar
            </button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

</div>
<script type="text/javascript">
    $('#LimparForm').click(function () {

        $('#PenalidadeCoNota').val('');
        $('#PenalidadeDtPenalidade').val('');
        $('#PenalidadeDsObservacao').val('');
        $('#dtIniVigencia').val('');
        $('#dtFimVigencia').val('');
        $('#PenalidadeIcSituacao').val('');
        $('#PenalidadeMtPenalidade').val('');
    });
</script>