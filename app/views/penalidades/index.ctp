<?php $usuario = $this->Session->read('usuario'); ?>
<div class="penalidades index">
    <table cellpadding="0"
           cellspacing="0"
           style="background-color:white"
           class="table table-hover table-bordered table-striped"
           id="tbPenalidade">
        <tr>
            <?
            $tabela = $this->Modulo->getTabelaPenalidades();
            foreach ($tabela as $tabelas):
                if ($this->Modulo->isEmpenho()) {
                    echo '<th>' . $tabelas . '</th>';
                }
            endforeach;
            ?>
        </tr>
        <?php
        $i = 0;
        foreach ($penalidades as $penalidade):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $penalidade['Penalidade']['dt_penalidade']; ?>&nbsp;</td>
                <?
                if (array_key_exists('nu_nota', $tabela)) {
                    echo "<td>" . $this->Print->notaFiscal($penalidade['NotaFiscal']) . "&nbsp;</td>";
                }
                ?>

                <td><?php echo $penalidade['Penalidade']['ds_observacao']; ?></td>
                <td><?php echo $penalidade['TipoPenalidade']['no_tipo_penalidade']; ?></td>
                <td><?php echo $penalidade['Penalidade']['mt_penalidade'] ? $penalidade['Penalidade']['mt_penalidade'] : '--' ?></td>
                <td><?php echo $penalidade['Penalidade']['dt_ini_vigencia'] ? $penalidade['Penalidade']['dt_ini_vigencia'] : '--' ?></td>
                <td><?php echo $penalidade['Penalidade']['dt_fim_vigencia'] ? $penalidade['Penalidade']['dt_fim_vigencia'] : '--' ?></td>
                <td class="actions"><?php $id = $penalidade['Penalidade']['co_penalidade']; ?>
                    <div class="btn-group acoes">
                        <!-- funcionalidade com erro -->
                        <!--                        <a id="--><?php //echo $id;
                        ?><!--" class="v_historico btn" href="#view_historico" data-toggle="modal"><i class="icon-film alert-tooltip" title="Histórico da Penalidade" style="display: inline-block;"></i></a>-->

                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i
                                    class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Penalidade"
                                    style="display: inline-block;"></i></a>
                        <?php echo $this->element('actions', array('id' => $id . '/' . $coContrato, 'class' => 'btn', 'local_acao' => 'penalidades/')) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
<?php
if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'penalidades/add')) { ?>
    <div class="actions">
        <div class="acoes-formulario-top clearfix">
            <p class="requiredLegend pull-left">Selecione uma Ação
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'penalidades', 'action' => 'add', $coContrato)); ?>"
                   class="btn btn-small btn-primary">Adicionar</a>
            </div>
        </div>
    </div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Suporte Documental - Anexar Penalidade</h3>
    </div>
    <div class="modal-body-iframe" id="fis_anexo">
    </div>
</div>

<div id="view_historico" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Penalidades - Histórico da Penalidade</h3>
    </div>
    <div class="modal-body-iframe" id="pen_hist">
    </div>
</div>

<script type="text/javascript">

    $('.alert-tooltip').tooltip();

    $('#tbPenalidade tr td a.v_anexo').click(function () {
        var url_an = "<?php echo $this->Html->url(array('controller' => 'anexos', 'action' => 'iframe', $coContrato)); ?>/penalidade/" + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_an,
            data: {},
            success: function (result) {
                $('#fis_anexo').html("");
                $('#fis_anexo').append(result);
            }
        })
    });

    $('#tbPenalidade tr td a.v_historico').click(function () {
        var url_ht = "../list_historico/" + $(this).attr('id');
        $.ajax({
            type: "POST",
            url: url_ht,
            data: {},
            success: function (result) {
                $('#pen_hist').html("");
                $('#pen_hist').append(result);
            }
        })
    });

</script>