<div class="penalidades index">
    <table cellpadding="0" cellspacing="0" style="background-color:white"
           class="table table-hover table-bordered table-striped">
        <tr>
            <th><?php __('Data'); ?></th>
            <th><?php __('Data da Penalidade'); ?></th>
            <th><?php __('Nota Fiscal'); ?></th>
            <th><?php __('Observações'); ?></th>
            <th><?php __('Situação da Penalidade'); ?></th>
            <th><?php __('Motivo da Penalidade'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($penalidades as $penalidade):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $penalidade['LogPenalidade']['dt_log']; ?></td>
                <td><?php echo $penalidade['LogPenalidade']['dt_penalidade']; ?></td>
                <td><?php echo $this->Print->notaFiscal($penalidade['NotaFiscal']); ?></td>
                <td><?php echo $penalidade['LogPenalidade']['ds_observacao']; ?></td>
                <td><?php echo $situacoes[$penalidade['LogPenalidade']['co_tipo_penalidade']]; ?></td>
                <td><?php echo $penalidade['LogPenalidade']['mt_penalidade']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>