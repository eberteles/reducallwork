<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="pendencias form">
<?php echo $this->Form->create('Pendencia', array('url' => "/pendencias/edit/$id/$coContrato/$modulo"));?>

	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar a Pendência - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'pendencias', 'action' => 'index', $coContrato, $modulo)); ?>" class="btn btn-small btn-primary" title="Listar Pendências">Listagem</a>
          </div>
        </div>

       <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Alterar Pendência</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                    <div class="span4">
                      <?php
                          echo $this->Form->hidden('co_pendencia');
                          echo $this->Form->hidden('co_situacao');
                          if($modulo == "ata" || $modulo == "pendencia_ata") {
                              echo $this->Form->hidden('co_ata', array('value' => $coContrato));
                          } else {
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                          }

                          $limitCampo = 255;

                          echo $this->Form->input('ds_pendencia', array('label' => 'Descrição','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("' . $limitCampo . '","#charsLeft")',
                                'after' => '<br><span id="charsLeft">'.$limitCampo.'</span> caracteres restantes.'));
                          echo '<br />';
                          echo $this->Form->input('dt_inicio', array(
                                      'before' => '<div class="input-append date datetimepicker">',
                                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                      'class' => 'input-small','label' => 'Início', 'type'=>'text'));
                          echo $this->Form->input('dt_fim', array(
                                      'before' => '<div class="input-append date datetimepicker">',
                                      'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                                      'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                      'class' => 'input-small','label' => 'Fim', 'type'=>'text'));
                      ?>
                     </div>
                    <div class="span4">
                      <?php
                          echo $this->Form->input('st_pendencia', array('label' => 'Situação', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $situacoes, 'onchange' => 'showDtFim()'));
                          echo $this->Form->input('ds_observacao', array('label'=>'Observação','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("500","#charsLeft1")',
                                'after' => '<br><span id="charsLeft1">500</span> caracteres restantes.'));
                          echo '<br />';

                          echo $this->Form->input('ds_impacto_pagamento', array(
                                  'label' => 'Impacto no ' . __('Pagamento', true),
                                  'type' => 'select',
                                  'options' => array(true => "Sim", false => "Não"))
                          );
                      ?>
                    </div>
                  </div>
	      </div>

            </div>

        </div>

    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Pendência"> Salvar</button>
          <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparForm"> Limpar</button>
          <button rel="tooltip" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>
<script type="text/javascript">
    $(function () {
        showDtFim();
    })

    $('#LimparForm').click(function () {

        $('#PendenciaDsPendencia').val('');
        $('#PendenciaDtInicio').val('');
        $('#PendenciaDtFim').val('');
        $('#PendenciaStPendencia').val('');
        $('#PendenciaDsObservacao').val('');
        $('#PendenciaDsImpactoPagamento').val('');


    });

    function showDtFim() {
        if($('#PendenciaStPendencia').val() == 'C'){
            $("#PendenciaDtFim").closest('.input').show();
            $('#PendenciaDtFim').prop('required', true);
            $('#PendenciaDtFim').closest('.input').addClass('required');
        }else{
            $("#PendenciaDtFim").closest('.input').hide();
            $('#PendenciaDtFim').prop('required', false);
            $('#PendenciaDtFim').closest('.input').removeClass('required');
        }
    }
</script>
