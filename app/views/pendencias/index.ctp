<?php $usuario = $this->Session->read ('usuario'); ?>
<div style="overflows-x:auto" class="row-fluid pendencias index">
<!-- h2>< ?php __('Pendências');?></h2 -->
<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered table-striped" id="tbPendencia">
	<tr>
		<th><?php __('Descrição');?></th>
		<th><?php __('Data Início');?></th>
		<th><?php __('Data Fim');?></th>
		<th><?php __('Situação');?></th>
		<th><?php __('Observação');?></th>
        <th>Impacto no <?php echo __('Pagamento'); ?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($pendencias as $pendencia):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr style= <?php echo $class;?>>
		<td style="text-align:justify;word-wrap: break-word;min-width: 400px;max-width: 400px;white-space:normal;">
			<?php echo $pendencia['Pendencia']['ds_pendencia'] ?></td>
		<td><?php echo $pendencia['Pendencia']['dt_inicio']; ?></td>
		<td><?php echo $pendencia['Pendencia']['dt_fim']; ?></td>
		<td><?php echo $this->Print->type( $pendencia['Pendencia']['st_pendencia'], $situacoes); ?></td>
        <td styles="text-align:justify;word-wrap: break-word;min-width: 400px;max-width: 400px;white-space:normal;">
            <?php echo $pendencia['Pendencia']['ds_observacao'];?>
        </td>

        <td><?php echo $pendencia['Pendencia']['ds_impacto_pagamento'] ? 'Sim' : 'Não' ?></td>

		<td class="actions"><?php $id = $pendencia['Pendencia']['co_pendencia']; ?>
            <div class="btn-group acoes">
                <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Pendência" style="display: inline-block;"></i></a>
                <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato.'/'.$modulo , 'class' => 'btn', 'local_acao' => 'pendencias/' ) ) ?>
            </div>
        </td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pendencias/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'pendencias', 'action' => 'add', $coContrato, $modulo)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Pendência</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php
    $modulo_anexo   = "pendencia";
    if($modulo == "ata") {
        $modulo_anexo   = $modulo_anexo . "_ata";
    }
    echo $this->Html->scriptStart();
?>

    $('.alert-tooltip').tooltip();

    $('#tbPendencia tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato . "/" . $modulo_anexo; ?>/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
