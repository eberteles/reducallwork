<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Perfis'); ?></h1></div>
        <div class="acoes-formulario-top clearfix" >
		<p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
		<div class="pull-right btn-group">
			<a href="<?php echo $this->Html->url(array('controller' => 'perfis', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Novo Perfil'); ?>"><i class="icon-plus icon-white"></i> <?php __('Novo Perfil'); ?></a>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort(__('Sequência', true), 'co_perfil');?></th>
                <th><?php echo $this->Paginator->sort('Perfil', 'no_perfil');?></th>
                <th class="actions"><?php __('Ações');?></th>
            </tr>
            <?php
    		$usuario = $this->Session->read ('usuario');
            $i = 0;
            foreach ($perfis as $perfil):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class;?>>
                    <td><?php echo $perfil['Perfil']['co_perfil']; ?>&nbsp;</td>
                    <td><?php echo $perfil['Perfil']['no_perfil']; ?>&nbsp;</td>
                    <td class="actions">
                        <div class="btn-group">                            
                            <?php 
                                if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'recursos/acessototal') ):
		                            // Buttton Administrador
		                            echo $this->Html->link('<i class="icon-star"></i>', array(
		                            		'controller' => 'recursos',
		                            		'action' => 'acessototal',
		                            		$perfil['Perfil']['co_perfil']
		                            ), array(
		                            		'escape' => false,
		                            		'class' => 'btn alert-tooltip',
		                            		'title' => 'Definir como acesso total'
		                            ));
                                endif;
                                
                                if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'permissoes/perfil') ):
		                            // Buttton Permissões
		                            echo $this->Html->link('<i class="icon-user"></i>', array(
		                            		'controller' => 'permissoes',
		                            		'action' => 'perfil',
		                            		$perfil['Perfil']['co_perfil']
		                            ), array(
		                            		'escape' => false,
		                            		'class' => 'btn alert-tooltip',
		                            		'title' => 'Permissões'
		                            ));
                                endif;
                                
                                if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'perfis/edit') ):
		                            // Buttton Editar
		                            echo $this->Html->link('<i class="icon-pencil"></i>', array(
		                            		'action' => 'edit',
		                            		$perfil['Perfil']['co_perfil']
		                            ), array(
		                            		'escape' => false,
		                            		'class' => 'btn alert-tooltip',
		                            		'title' => 'Editar'
		                            ));
                                endif;
                                
                                if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'perfis/bloquearOrDesbloquear') ):
		                            // Buttton Bloquear
	                            	if ($perfil['Perfil']['ic_ativo'] == 1) {
											echo $this->Html->link('<i class="icon-remove"></i>', array(
	                            			'action' => 'bloquearOrDesbloquear',
											$perfil['Perfil']['co_perfil']
	                            	), array(
	                            			'escape' => false,
	                            			'class' => 'btn btn-danger alert-tooltip',
	                            			'title' => 'Bloquear'
	                            	));							
	                            	} else {
		                            	// Desbloquear
		                            	if ($perfil['Perfil']['ic_ativo'] == 0) {
		                            
		                            		echo $this->Html->link('<i class="icon-ok"></i>', array(
		                            				'action' => 'bloquearOrDesbloquear',
		                            				$perfil['Perfil']['co_perfil']
		                            		), array(
		                            				'escape' => false,
		                            				'class' => 'btn btn-success',
		                            				'title' => 'Desbloquear'
		                            		));
		                            	}
	                            	}  
                            	endif;
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
                <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
            </ul>
        </div>
</div>

<script type="text/javascript">
    
</script>