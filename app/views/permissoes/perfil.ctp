<div style="padding-top: 50px;">
    <?php echo $this->Form->create('Recurso', array('url' => array('controller' => 'permissoes', 'action' => 'perfil', $perfil['Perfil']['co_perfil']))); ?>
    <div class="row-fluid">
        <div class="span9">
            <div class="input text">
                <label for="RecursoNoRecurso">Recurso</label>
                <input name="data[Recurso][no_recurso]" type="text" class="input-xlarge" maxlength="20"
                       id="RecursoNoRecurso"/>
            </div>
        </div>
        <div class="span3">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                    title="Pesquisar processo" style="top: 25px;"><i class="icon-search icon-white"></i> Pesquisar
            </button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<div id="recursos">
    <div class="row-fluid">
        <?php echo $this->Form->create('Perfil', array('url' => array('controller' => 'permissoes', 'action' => 'add'))); ?>
        <div class="perfilLabel">
            <h3>Perfil: <?php echo $perfil['Perfil']['no_perfil']; ?></h3>
            <input type="hidden" name="co_perfil" id="coPerfil" value="<?php echo $perfil['Perfil']['co_perfil']; ?>"/>
        </div>
        <table cellpadding="0" cellspacing="0" style="background-color:white"
               class="table table-hover table-bordered table-striped">
            <tr>
                <th width="50"><?php echo $this->Paginator->sort(__('Código', true), 'co_recurso'); ?></th>
                <th><?php echo $this->Paginator->sort('Recurso', 'no_recurso'); ?></th>
                <th width="20"><?php echo $this->Paginator->sort('Acesso', 'ic_publico'); ?></th>
            </tr>
            <?php
            $i = 0;
            foreach ($recursos as $recurso):
                $class = 'class="expandir"';
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow expandir"';
                }
                ?>
                <tr <?php echo $class; ?> id="rsc<?php echo $recurso['Recurso']['co_recurso']; ?>"
                                          style="cursor:pointer;">
                    <?php
                    $checked = "";
                    if (count($recurso['Permissao'])):
                        if (isset($recurso['Permissao'][0])
                            && $recurso['Permissao'][0]['co_recurso'] == $recurso['Recurso']['co_recurso']
                        ):
                            $checked = "checked='checked'";
                        endif;
                    endif;
                    ?>
                    <td><input type="checkbox" <?php echo $checked; ?> class='inputRecurso' name="recurso[]"
                               value="<?php echo $recurso['Recurso']['co_recurso']; ?>"/>&nbsp;</td>
                    <td>
                        <?php if ($recurso['Recurso']['ic_childs']): ?>
                            <i class="icon-chevron-right icon-black"></i>
                        <?php endif; ?>
                        <?php echo $recurso['Recurso']['no_recurso']; ?>&nbsp;
                    </td>
                    <td><?php echo ($recurso['Recurso']['ic_publico']) ? 'Público' : 'Restrito'; ?>&nbsp;</td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php echo $this->Form->end(); ?>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?>
        </p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            </ul>
        </div>
    </div>
</div>

<?php echo $this->Html->script('jquery.blockUI'); ?>
<script type="text/javascript">
    $(document).ready(function () {

        var expandirFunction = function () {
            var trTag = $(this).parents('tr');
            coRecurso = trTag.find("input[type='checkbox']").val(),
                url = "<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'childs')); ?>/" + coRecurso + "/" + $('#coPerfil').val();

            $.blockUI();

            $.ajax(url, {
                type: 'GET',
                success: function (response) {

                    var data = $.parseJSON(response),
                        html = "<tr style'cursor:pointer;'>";
                    html += "<td colspan='4'>";
                    html += "<table class='table table-hover table-bordered table-striped rsc" + coRecurso + "'>";

                    $.each(data, function (index, item) {
                        var checked = "";

                        if (item.Permissao.length > 0
                            && item.Permissao[0].co_recurso == item.Recurso.co_recurso) {
                            checked = "checked='checked'";
                        }

                        html += "<tr class='expandir' style='cursor:pointer;'>";

                        if (item.Recurso.ic_publico == 1) {
                            html += "<td width='50'><input type='checkbox' checked='checked' disabled='disabled' />&nbsp;</td>";
                        } else {
                            html += "<td width='50'><input type='checkbox' " + checked + " class='inputRecurso' name='recurso[" + item.Recurso.parent_id + "]' value='" + item.Recurso.co_recurso + "' />&nbsp;</td>";
                        }

                        if (item.Recurso.ic_childs == 1) {
                            html += "<td><i class='icon-chevron-right icon-black'></i>&nbsp;" + item.Recurso.no_recurso + "</td>";
                        } else {
                            html += "<td>" + item.Recurso.no_recurso + "</td>";
                        }

                        if (item.Recurso.ic_publico == 1) {
                            html += "<td width='50'>Público&nbsp;</td>";
                        } else {
                            html += "<td width='50'>Restrito&nbsp;</td>";
                        }

                        html += "</tr>";
                    });

                    html += "</td>";
                    html += "</tr>";
                    html += "</table>";

                    $(trTag).after(html);
                    trTag.removeClass('expandir altrow').addClass('recolher');
                    trTag.find('i').removeClass('icon-chevron-right').addClass('icon-chevron-down');

                    $.unblockUI();

                },
                error: function (response) {
                    $.unblockUI();
                }
            });
        };

        var recolherFunction = function () {
            var trTag = $(this).parents('tr');
            coRecurso = trTag.find("input[type='checkbox']").val();

            trTag.next('tr').remove();
            trTag.removeClass('recolher').addClass('expandir altrow');
            trTag.find('i').removeClass('icon-chevron-down').addClass('icon-chevron-right');
        };

        $(document).delegate('tr.recolher td:not(:first-child,:last-child,.bloqueio)', 'click', recolherFunction);
        $(document).delegate('tr.expandir td:not(:first-child,:last-child,.bloqueio)', 'click', expandirFunction);

        $(document).delegate("input.inputRecurso", 'click', function () {

            var coRecurso = $(this).val(),
                coPerfil = $("#coPerfil").val(),
                trParent = $(this).parents('tr'),
                hasTable = trParent.next().find('table').length;

            if (hasTable > 0) {
                if ($(this).is(':checked')) {
                    trParent.next().find('input[type="checkbox"]:not(:checked,:disabled)').prop('checked', true);
                } else {
                    trParent.next().find('input[type="checkbox"]:checked').prop('checked', false);
                }
            }

            if ($(this).is(':checked')) {
                permissoesFunction($(this), coRecurso, coPerfil, '<?php echo $this->Html->url(array('controller' => 'permissoes', 'action' => 'add')); ?>');
            } else {
                permissoesFunction($(this), coRecurso, coPerfil, '<?php echo $this->Html->url(array('controller' => 'permissoes', 'action' => 'delete')); ?>');
            }

        });

        var permissoesFunction = function (input, coRecurso, coPerfil, url) {
            var dataPost = {
                'co_recurso': coRecurso,
                'co_perfil': coPerfil
            }

            $.ajax(url, {
                type: 'POST',
                data: dataPost,
                success: function (response) {

                },
                error: function (data) {
                    if (url.indexOf('add') > -1) {
                        input.prop('checked', false);
                    } else {
                        input.prop('checked', true);
                    }
                }
            });
        };

    });
</script>
