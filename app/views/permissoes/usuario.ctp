<div id="alertContainer" class="alert hide"></div>
<div class="row-fluid">
	<div class="span12">		
		<div class="usuarioLabel">
			<h3>Usuário: <?php echo $usuario['Usuario']['ds_nome']; ?></h3>
	    	<input type="hidden" name="co_usuario" id="coUsuario" value="<?php echo $usuario['Usuario']['co_usuario']; ?>" />
		</div>
	</div>
</div>
<div class="row-fluid">
	<?php if( count($recursos) ): ?>
		<?php $newColumn = true; $totalRows = count($recursos); ?>
		<?php foreach ($recursos as $key => $recurso): ?>		
			<?php if($newColumn): ?>
			<div class="span6">
				<div class="widget-header widget-header-small"><h4><?php __('Permissões'); ?></h4></div>
				<div class="widget-body">
					<div class="widget-main">
						<table>
							<tr>
								<th width="300">Módulo</th>
								<th title="Consultar">C</th>
								<th title="Incluir">I</th>
								<th title="Alterar">A</th>
								<th title="Excluir">E</th>
							</tr>
			<?php endif; ?>					
				<tr>
					<?php $coModulo = $recurso['Recurso']['co_recurso'];?>				
					<td><?php echo $recurso['Recurso']['no_recurso']; ?></td>
					
					<?php if($recurso[0]['EC']): ?>
						<?php $checkedDisabled = ($recurso['C']['C'] > 0) ? true : false; ?>
						<td><?php echo $this->Form->checkbox("CONSULTA", array( 'class' => 'inputRecurso', 'value' => $coModulo, 'checked' => $checkedDisabled, 'disabled' => $checkedDisabled ) ); ?></td>
					<?php else: ?>
						<td><input type="checkbox" disabled="disabled" /></td>
					<?php endif; ?>
					
					<?php if($recurso[0]['EI']): ?>
						<?php $checkedDisabled = ($recurso['I']['I'] > 0) ? true : false; ?>
						<td><?php echo $this->Form->checkbox("INSERE", array( 'class' => 'inputRecurso', 'value' => $coModulo, 'checked' => $checkedDisabled, 'disabled' => $checkedDisabled ) ); ?></td>
					<?php else: ?>
						<td><input type="checkbox" disabled="disabled" /></td>
					<?php endif; ?>
					
					<?php if($recurso[0]['EA']): ?>
						<?php $checkedDisabled = ($recurso['A']['A'] > 0) ? true : false; ?>
						<td><?php echo $this->Form->checkbox("ALTERA", array( 'class' => 'inputRecurso', 'value' => $coModulo,'checked'  => $checkedDisabled, 'disabled' => $checkedDisabled ) ); ?></td>
					<?php else: ?>
						<td><input type="checkbox" disabled="disabled" /></td>
					<?php endif; ?>
					
					<?php if($recurso[0]['EE']): ?>
						<?php $checkedDisabled = ($recurso['E']['E'] > 0) ? true : false; ?>
						<td><?php echo $this->Form->checkbox("EXCLUI", array( 'class' => 'inputRecurso', 'value' => $coModulo, 'checked' => $checkedDisabled, 'disabled' => $checkedDisabled) ); ?></td>
					<?php else: ?>
						<td><input type="checkbox" disabled="disabled" /></td>
					<?php endif; ?>
				</tr>
				<?php if((($totalRows/2)-1) == $key || end($recursos) == $recurso): ?>					
							</table>
						</div>
					</div>
				</div>
				<?php $newColumn = true; ?>
			<?php else: ?>		
				<?php $newColumn = ((($totalRows/2)-1) == $key) ? true : false; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="span12">
			<p>Nenhum registro encontrado.</p>
		</div>		
	<?php endif; ?>
</div>
	
    <div class="form-actions">
    	<br />
    	<button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>	
<script type="text/javascript">
	var msgSucesso = '<?php echo __('Acesso concedido com sucesso!'); ?>',
		msgErro = '<?php echo __('Não foi possivel realizar esta operação, tente novamente tarde.'); ?>';

	$(document).ready(function(){
		
		$( document ).delegate("input.inputRecurso", 'click', function(){
			var coUsuario = $("#coUsuario").val();
			
			if( $(this).is(':checked') ){
				permissoesFunction($(this), coUsuario, 'add');
			} else {
				permissoesFunction($(this), coUsuario, 'delete');
			}
			
		});

		var permissoesFunction = function( input, coUsuario, operation ){
						
			var dataPost = {
				'sg_tipo'	 : input.attr('name'),
				'co_recurso' : input.val(),
				'co_usuario' : coUsuario
			}				
			
			var url = '/permissoes/' + operation + '/usuario';
			
			$.ajax(url, {
				type: 'POST',
				data: dataPost,
				success: function(response){
					var data = $.parseJSON(response);
					if( data.status == 200 ){
						$("#alertContainer").addClass('alert-success')
											.html('<p>' + msgSucesso + '</p>')
											.fadeIn(1000)
											.delay(3000)
											.fadeOut(2000);
					} else {
						$("#alertContainer").addClass('alert-error')
											.html('<p>' + msgErro + '</p>')
											.fadeIn(1000)
											.delay(3000)
											.fadeOut(2000);
					}
						
				},
				error: function(response){
					var data = $.parseJSON(response);

					if( data.status == 500 ){
						$("#alertContainer").addClass('alert-error')
											.html('<p>' + msgErro + '</p>')
											.fadeIn(1000)
											.delay(3000)
											.fadeOut(2000);
					}
					
					if( operation == 'add' ){
						input.prop('checked', false);
					} else {
						input.prop('checked', true);
					}
				}
			});			
		};
		
	});
</script>