<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<div class="empenhos form">
<?php echo $this->Form->create('PreEmpenho', array('url' => "/pre_empenhos/edit/$id/$coContrato"));?>
    
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para alterar o Pré-empenho - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'pre_empenhos', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Pré-empenhos">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
           <div class="widget-header widget-header-small"><h4>Alterar Pré-empenho</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                    <div class="span4">
                       <dl class="dl-horizontal">
                    <?php
                            echo $this->Form->hidden('co_contrato', array('value' => $coContrato));

                            echo $this->Form->input('nu_pre_empenho', array('label'=>'Número do Pré-empenho', 'mask'=>FunctionsComponent::pegarFormato( 'empenho' )));
//                            echo $this->Form->input('tp_empenho', array('label'=>'Tipo', 'type' => 'select', 'empty' => 'Selecione...', 'options' => $tipos));
                            echo $this->Form->input('ds_pre_empenho', array('class' => 'input-xlarge','label'=>'Descrição', 'type'=>'textarea', 'rows'=>'4'));
                    ?>
                        </dl>
                      </div>
                     <div class="span4">
                        <dl class="dl-horizontal">                  
                       <?php
//                               echo $this->Form->input('co_plano_interno', array('label' => __('Plano Interno', true)));
                               echo $this->Form->input('vl_pre_empenho', array('label' => 'Valor (R$)', 'value' => $this->Print->real( $empenho['vl_pre_empenho'] ) ));
                               echo $this->Form->input('ds_fonte_recurso', array('label' => 'Fonte de Recurso'));
                       ?>
                        </dl>
                      </div>
                     <div class="span4">
                        <dl class="dl-horizontal">                  
                       <?php
                               echo $this->Form->input('dt_pre_empenho', array(
                                       'before' => '<div class="input-append date datetimepicker">', 
                                       'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>', 
                                       'data-format' => 'dd/MM/yyyy', 'mask'=>'99/99/9999',
                                       'class' => 'input-small','label' => 'Data', 'type'=>'text'));

                                echo $this->Form->input('nu_natureza_despesa', array('label' => 'Natureza de Despesa', 'maxLength' => '5', 'value' => $empenho['nu_natureza_despesa']));

//                               if($this->Modulo->siafi == true) :
//                                    echo $this->Form->input('nu_ptres', array('label' => 'PTRES'));
//                               endif;
                       ?>
                        </dl>
                      </div>
                  </div>
	      </div>
                  
            </div>
              
        </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Garantia"> Salvar</button> 
          <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>

</div>    
