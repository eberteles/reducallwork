<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="empenhos index">
<!-- h2>< ?php __('Empenhos');?></h2-->

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbEmpenho">
	<tr>
		<th>Número do Pré-empenho</th>
		<th>Data do Pré-empenho</th>
		<th>Descrição</th>
		<th>Valor</th>
		<th class="actions">Ações</th>

<!--        --><?php //if($this->Modulo->siafi == true) : ?>
<!--		    <th>--><?php //echo $this->Paginator->sort('PTRES', 'it_co_programa_trabalho_resumido');?><!--</th>-->
<!--        --><?php //endif; ?>

	</tr>

	<?php
	$i = 0;
	foreach ($empenhos as $empenho):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $empenho['PreEmpenho']['nu_pre_empenho']; ?>&nbsp;</td>
		<td><?php echo $empenho['PreEmpenho']['dt_pre_empenho']; ?>&nbsp;</td>
		<td><?php echo $empenho['PreEmpenho']['ds_pre_empenho']; ?>&nbsp;</td>
		<td><?php echo $this->Formatacao->moeda( $empenho['PreEmpenho']['vl_pre_empenho'] ); ?>&nbsp;</td>

<!--        --><?php //if($this->Modulo->siafi == true) : ?>
<!--		    <td>--><?php //echo $empenho['PreEmpenho']['it_co_programa_trabalho_resumido']; ?><!--&nbsp;</td>-->
<!--        --><?php //endif; ?>
		<td class="actions">
			<div class="btn-group acoes">
                <?php echo $this->element( 'actions2', array( 'id' => $empenho['PreEmpenho']['co_pre_empenho'].'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'pre_empenhos/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

</div>

<?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'pre_empenhos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'pre_empenhos', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Empenho</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>
        
    $('.alert-tooltip').tooltip();

    $('#tbEmpenho tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/empenho/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });
    
<?php echo $this->Html->scriptEnd() ?>