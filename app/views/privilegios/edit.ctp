<div class="privilegios form"><?php echo $this->Form->create('Privilegio');?>
<fieldset><legend><?php __('Perfis de Acesso'); ?></legend>
<?php
	echo $this->Form->input('co_privilegio');
	echo $this->Form->input('sg_privilegio', array('label' => 'Nome'));
	echo $this->Form->input('ds_privilegio', array('label' => 'Descrição'));
?>
</fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('Privilegio.co_privilegio')), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $this->Form->value('Privilegio.co_privilegio'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Perfis', true), array('action' => 'index'));?></li>
</ul>
</div>
