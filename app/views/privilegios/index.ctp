<div class="privilegios index">
<!-- h2>< ?php __('Privilégios');?></h2-->

<fieldset><legend><?php __('Perfis de Acesso'); ?></legend>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('Código', 'co_privilegio');?></th>
		<th><?php echo $this->Paginator->sort('Nome', 'sg_privilegio');?></th>
		<th><?php echo $this->Paginator->sort('Descrição', 'ds_privilegio');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($privilegios as $privilegio):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $privilegio['Privilegio']['co_privilegio']; ?>&nbsp;</td>
		<td><?php echo $privilegio['Privilegio']['sg_privilegio']; ?>&nbsp;</td>
		<td><?php echo $privilegio['Privilegio']['ds_privilegio']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->element( 'actions', array( 'id' => $privilegio['Privilegio']['co_privilegio'] ) ) ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</fieldset>
</div>

<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo', true), array('action' => 'add')); ?></li>
</ul>
</div>
