<div class="privilegiosAcessos form"><?php echo $this->Form->create('PrivilegioAcesso');?>
<fieldset><legend><?php __('Privilegios Acesso'); ?></legend> <?php
echo $this->Form->input('co_privilegio_acesso');
echo $this->Form->input('co_acesso');
echo $this->Form->input('co_privilegio');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('PrivilegioAcesso.id')), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $this->Form->value('PrivilegioAcesso.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Privilegios Acessos', true), array('action' => 'index'));?></li>
</ul>
</div>
