<div class="produtos form">
	<?php echo $this->Form->create('Produto', array('url' => "/produtos/add/$coContrato"));?>
    
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar um Produto - <b>Campos com * são obrigatórios.</b></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'produtos', 'action' => 'index', $coContrato)); ?>" class="btn btn-small btn-primary" title="Listar Produtos">Listagem</a>
          </div>
        </div>
    
       <div class="row-fluid">
                        
          <div class="span12">
              <div class="widget-header widget-header-small"><h4>Novo Produto</h4></div>
                <div class="widget-body">
                  <div class="widget-main">
                      <div class="row-fluid">                          
                        <div class="span4">
                          <?php
                              echo $this->Form->hidden('co_produto');
                              echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                              
                              echo $this->Form->input('co_produto_tipo', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Tipo de Produto', true), 'options' => $tipos, 'id' => 'selecao'));
                              echo $this->Form->input('ds_produto', array('label' => __('Produto', true)) );
                              echo $this->Form->input('nu_serie', array('label' => __('Nº de Série', true)) );
                              echo $this->Form->input('produto_recebido', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>'Produto já foi recebido pelo Órgão?', 'options' => array(0 => 'Não Recebido', 1 => 'Recebido')));
                              
                          ?>
                         </div>
                        <div class="span4">
                          <?php
                              echo $this->Form->input('ds_garantia', array('label'=>'Garantia','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("100","#charsLeft")', 
                                'after' => '<br><span id="charsLeft">100</span> caracteres restantes.'));
                              echo '<br />';
                              echo $this->Form->input('ds_fabricante', array('label'=>'Fabricante','type' => 'texarea', 'cols'=>'40', 'rows'=>'4',
                                'onKeyup'=>'$(this).limit("100","#charsLeft2")', 
                                'after' => '<br><span id="charsLeft2">100</span> caracteres restantes.'));
                              echo '<br />';
                          ?>
                         </div>
                      </div>
                  </div>
            </div>
              
          </div>
       </div>
    
    <div class="form-actions">
       <div class="btn-group">
          <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar Produto"> Salvar</button> 
          <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
          <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
       </div>
    </div>
	<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<script>
	$(document).ready(function(){
		$('.input,.select').addClass('required');
	});
</script>