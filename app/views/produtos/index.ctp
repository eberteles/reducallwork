<?php $usuario = $this->Session->read ('usuario'); ?>
<div class="produtos index">
<!-- h2>< ?php __('Produtos');?></h2 -->
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbProduto">
	<tr>
		<th><?php __('Tipo de Produto');?></th>
		<th><?php __('Produto');?></th>
		<th><?php __('Nº de Série');?></th>
		<th><?php __('Garantia');?></th>
		<th><?php __('Fabricante');?></th>
		<th>Produto recebido?</th>

		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($produtos as $produto):
	?>
	<tr>
		<td><?php echo $produto['ProdutoTipo']['ds_tipo']; ?>&nbsp;</td>
		<td><?php echo $produto['Produto']['ds_produto']; ?>&nbsp;</td>
		<td><?php echo $produto['Produto']['nu_serie']; ?>&nbsp;</td>
		<td><?php echo $produto['Produto']['ds_garantia']; ?>&nbsp;</td>
		<td><?php echo $produto['Produto']['ds_fabricante']; ?>&nbsp;</td>
		<td><?php if ($produto['Produto']['produto_recebido'] == 1) { echo 'Recebido'; } else { echo 'Não recebido'; } ?>&nbsp;</td>
		<td class="actions"><?php $id = $produto['Produto']['co_produto']; ?>

                    <div class="btn-group acoes">
                        <a id="<?php echo $id; ?>" class="v_anexo btn" href="#view_anexo" data-toggle="modal"><i class="silk-icon-folder-table-btn alert-tooltip" title="Anexar Produto" style="display: inline-block;"></i></a>
                        <?php echo $this->element( 'actions', array( 'id' => $id.'/'.$coContrato , 'class' => 'btn', 'local_acao' => 'produtos/' ) ) ?>
                    </div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<?php
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'produtos/add') ) { ?>
<div class="actions">
<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left">Selecione uma Ação<!--span class="required" title="Required">*</span--></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'produtos', 'action' => 'add', $coContrato)); ?>" class="btn btn-small btn-primary">Adicionar</a>
          </div>
        </div>

</div>
<?php } ?>

<div id="view_anexo" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Suporte Documental - Anexar Produtos</h3>
  </div>
  <div class="modal-body-iframe" id="fis_anexo">
  </div>
</div>

<?php echo $this->Html->scriptStart() ?>

    $('.alert-tooltip').tooltip();

    $('#tbProduto tr td a.v_anexo').click(function(){
        var url_an = "<?php echo $this->base; ?>/anexos/iframe/<?php echo $coContrato; ?>/produto/" + $(this).attr('id');
        $.ajax({
            type:"POST",
            url:url_an,
            data:{
                },
                success:function(result){
                    $('#fis_anexo').html("");
                    $('#fis_anexo').append(result);
                }
            })
    });

<?php echo $this->Html->scriptEnd() ?>
