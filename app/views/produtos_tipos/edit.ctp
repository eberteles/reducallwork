<?php echo $this->Form->create('ProdutoTipo', array('onsubmit' => "return confirm('Ao editar esse tipo de produto, você alterará todos os contratos e outros registros que possuem esse tipo de produto atrelado. Deseja prosseguir com essa alteração? (Sua alteração ficará registrada em log de alterações de tipos de produtos)');", 'url' => "/produtosTipos/edit/id/" . $id));?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>
    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Tipos de Produto / Produtos'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_produto_tipo');
                echo $this->Form->input('ds_tipo', array('class' => 'input-xlarge','label' => 'Descrição'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

