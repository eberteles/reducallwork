<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Tipos de Produto / Produtos'); ?></h1></div>
    <?php
    // if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'adm_ctb_i') ): ?>
        <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'produtosTipos', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Novo Tipo de Produto'); ?>"><i class="icon-plus icon-white"></i> <?php __('Novo Tipo de Produto'); ?></a>
            </div>
        </div>
    <?php // endif; // debug($situacoes); exit(); ?>
    <?php  //debug($produtosTipos); exit(); ?>
</div>

<div class="box-tabs-white" id="tabs">
    <ul class="nav nav-tabs">
        <li><?php echo $this->Html->link(__('Tipos de Produto', true), array('action' => 'listaProdutosTipos')) ?></li>
    </ul>
</div>

<script>
    $(function() {
        $( "#tabs" ).tabs({
                beforeLoad: function( event, ui ) {
                        ui.jqXHR.fail(function() {
                                ui.panel.html("Não foi possível carregar o conteúdo, porfavor tente novamente.");
                        });
                },
                load: function( event, ui){
                        $('table th a').on('click', function() {
                                $("a", ui.tab).attr('href', $(this).attr('href'));                                                      
                                $("#tabs").tabs("load", $("#tabs").tabs("option", "active"));
                                return false;
                        });             
                        $('.pagination a').on('click', function(){
                                var newUrl = $(this).attr('href');
                                $("a", ui.tab).attr('href', newUrl);                                                      
                                $("#tabs").tabs("load", $("#tabs").tabs("option", "active"));
                                return false;
                        });
                }
        });
        setTimeout(function() { 
            $('.alert').fadeOut('slow'); 
        }, 4000);  
    });
</script>