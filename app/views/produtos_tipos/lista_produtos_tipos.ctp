<div id="situacoes">
    <div class="row-fluid">
        <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort(__('Sequência', true), 'co_produto_tipo');?></th>
                <th><?php echo $this->Paginator->sort('Descrição', 'ds_situacao');?></th>
                <th class="actions"><?php __('Ações');?></th>
            </tr>
            <?php
            $i = 0;
            foreach ($produtosTipos as $produtoTipo):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class;?>>
                    <td><?php echo $produtoTipo['ProdutoTipo']['co_produto_tipo']; ?>&nbsp;</td>
                    <td><?php echo $produtoTipo['ProdutoTipo']['ds_tipo']; ?>&nbsp;</td>
                    <td class="actions">
                        <div class="btn-group">
                            <?php echo $this->element( 'actions2', array( 'id' => $produtoTipo['ProdutoTipo']['co_produto_tipo'], 'class' => 'btn', 'local_acao' => 'produtosTipos/index' ) ) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
                <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
            </ul>
        </div>
    </div>
</div>