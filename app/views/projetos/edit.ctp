<?php echo $this->Form->create('Projeto', array('url' => "/projetos/edit", 'onsubmit' => "return confirm('Ao editar esse 
Projeto, você alterará todos os contratos e outros registros que possuem esse Projeto atrelado. Deseja prosseguir com essa 
alteração? (Sua alteração ficará registrada em log de alterações de Projetos)');")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Dados do Projeto</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_projeto');
                echo $this->Form->input('no_projeto', array('class' => 'input-xlarge','label' => 'Nome do Projeto', 'type' => 'text', 'maxlength' => '30'));
                echo $this->Form->input('ds_projeto', array('class' => 'input-xlarge','type' => 'textarea','label' => 'Descrição do Projeto','maxlength' => '300'));
                ?>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="LimparFormulario"> Limpar</button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#LimparFormulario').click(function () {

            $('#ProjetoNoProjeto').val('');
            $('#ProjetoDsProjeto').val('');

        });


    });
</script>