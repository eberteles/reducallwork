<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1>Lista de Projetos</h1></div>

    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha um dos campos para fazer a pesquisa</p>
        <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'projetos', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Projeto"><i class="icon-plus icon-white"></i> Novo Projeto</a>
        </div>
    </div>

    <?php echo $this->Form->create('Projeto', array('url' => '/projetos/index'));?>

    <div class="row-fluid">
        <div class="span3">
            <div class="input text ">
                <label for="ProjetoNoProjeto">Nome do Projeto</label>
                <input name="data[Projeto][no_projeto]" type="text" class="input-xlarge" maxlength="30" id="ProjetoNoProjeto">
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
            <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
        </div>
    </div>

    <table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
        <tr>
            <th>NOME DO PROJETO</th>
            <th>DESCRIÇÃO</th>
            <th class="actions">AÇÕES</th>
        </tr>
        <?php
        $i = 0;
        foreach ($projetos as $projeto):
            $class = null;
            if ($i++ % 2 == 0):
                $class = ' class="altrow"';
            endif;
            ?>
            <tr <?php echo $class;?>>
                <td><?php echo $projeto['Projeto']['no_projeto']; ?></td>
                <td><?php echo $projeto['Projeto']['ds_projeto']; ?></td>

                <td class="actions">
                    <div class="btn-group acoes">
                        <?php echo $this->element( 'actions2', array( 'id' => $projeto['Projeto']['co_projeto'], 'class' => 'btn', 'local_acao' => 'projetos/index' ) ); ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
            <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
        </ul>
    </div>
</div>