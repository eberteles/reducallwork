
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tabelaClassificacoes">
	<tr>
		<th><?php __('Nome');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	foreach ($classificacoes as $classificacao):
            $id  = $classificacao['ProvaClassificacao']['co_prova_classificacao'];
        ?>
        <tr id="<?php echo $id; ?>">
            <td class="edit_descricao alert-tooltip" title="Clique duas vezes para editar a descrição."><?php echo $classificacao['ProvaClassificacao']['ds_prova_classificacao'] ?></td>
            <td class="actions">
                <?php echo $this->Html->link('<i class="icon-trash icon-white"></i>', array('action' => 'delete', $coProva, $id), array('title'=>'Excluir Classificação','escape' => false,'class' => 'btn  btn-danger alert-tooltip'), sprintf(__('Tem certeza de que deseja excluir este registro?', true), $id)); ?>
            </td>
        </tr>
        <?php
        endforeach; 
        ?>
</table>

<div id="dialog-mensagem-cla" class="hide">
        <div class="alert alert-info bigger-110">
              <?php echo $mensagem; ?>
        </div>
</div>

<?php echo $this->Html->scriptStart() ?>

$('.alert-tooltip').tooltip();

$(document).ready(function() {

<?php
    if($mensagem != '') {
?>
    $("#dialog-mensagem-cla").dialog({
          position: 'top',
          resizable: true,
          modal: true,
          title: 'Classificação de Prova',
          title_html: true,
    });
<?php
    }
?>
    
    $('#tabelaClassificacoes tr td.edit_descricao').dblclick(function(){
        if($('td > input').length > 0){
            return;
        }
        var conteudoOriginal    = $(this).text();
        var novo_elemento       = $('<input/>', {type:'text', value: conteudoOriginal});
        $(this).html(novo_elemento.bind('blur keydown', function(e){
            var keyCode         = e.which;
            var conteudoNovo    = $(this).val();
            if( (keyCode == 13 || e.type == "blur" ) &&  conteudoNovo != '' && conteudoNovo != conteudoOriginal) {
                var objeto  = $(this);
                var url_desc = "<?php echo $this->base; ?>/provas_classificacoes/edit/" + $(this).parents('tr').attr('id');
                $.ajax({
                    type:"POST",
                    url:url_desc,
                    data:{
                        "data[ds_prova_classificacao]":conteudoNovo
                    },
                    success:function(result){
                        objeto.parent().html(conteudoNovo.toUpperCase());
                        //alert(result);
                        //$('body').append(result);
                    }
                })
            }
            if(keyCode == 27 || (conteudoNovo == conteudoOriginal && e.type == "blur") ){
                $(this).parent().html(conteudoOriginal);
            }
        }));
        $(this).children().select();
    })
    
})
    
<?php echo $this->Html->scriptEnd() ?>
