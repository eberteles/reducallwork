<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
        <?php echo $this->Form->create('Providencia', array('url' => array('controller' => 'providencias', 'action' => 'index', $coReclamacao))); ?>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>DATA</th>
		<th>USUÁRIO/SESSÃO</th>
		<th>PROVIDÊNCIA</th>
	</tr>
	<?php
	$i = 0;
	foreach ($providencias as $providencia){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $providencia['Providencia']['dt_providencia']; ?></td>
        <td><?php echo $providencia['Usuario']['ds_nome']; ?></td>
        <td><?php echo $providencia['Providencia']['ds_providencia']; ?></td>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>

        <?php
            echo $this->Form->input('co_providencia');
            echo $this->Form->hidden('co_reclamacao', array('value' => $coReclamacao));
            echo $this->Form->hidden('co_usuario', array('value' => $usuario['Usuario']['co_usuario']));
            echo $this->Form->input('ds_providencia', array('class' => 'input-xlarge','placeholder' => 'Digite a solução ou providência sobre o problema e clique em salvar','maxlength' => '500', 'type' => 'textarea', 'label' => 'Providência', 'style' => 'width: 70%;', 'rows' => '1'));
        ?>
        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>

</div>