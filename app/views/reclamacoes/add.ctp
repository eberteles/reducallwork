<?php echo $this->Html->script( 'inicia-datetimepicker' ); ?>
<?php echo $this->Form->create('Reclamacao', array('url' => array('controller' => 'reclamacoes', 'action' => 'add'))); ?>

    <div class="row-fluid">
  <b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4>Reclamação</h4></div>
            <div class="widget-body">
              <div class="widget-main">
                  <div class="row-fluid">
                      <div class="span3">
                          <?php
                          echo $this->Form->input('co_reclamacao');
                          echo $this->Form->input('co_fornecedor', array('class' => 'input-xlarge chosen-select','label' => 'Fornecedor Reclamado','id' => 'fornecedorReclamado','type' => 'select', 'options' => $fornecedores, 'empty' => 'Selecione'));
                          echo $this->Form->input('co_contrato', array('class' => 'input-xlarge','label' => 'Contrato Referente','type' => 'select', 'empty' => 'Selecione','id' => 'contratoReferente', 'readonly' => true));
                          echo $this->Form->input('tp_reclamacao', array('class' => 'input-xlarge','type' =>'select','options' => $tipos,'empty' => 'Selecione','label' => 'Tipo de Reclamação','id' => 'tpReclamacao'));
                          echo $this->Form->input('dt_problema', array(
                              'before' => '<div class="input-append date datetimepickerMaxHoje">',
                              'after' => '<span class="add-on"><i data-date-icon="icon-calendar"></i></span></div>',
                              'data-format' => 'dd/MM/yyyy',
                              'mask' => '99/99/9999',
                              'class' => 'input-small',
                              'label' => 'Data do Problema',
                              'type' => 'text'
                          ));
                          ?>
                      </div>
                      <div class="span3">
                          <?php
                          echo $this->Form->input('assunto_reclamacao', array('class' => 'input-xlarge','label' => 'Assunto da Reclamação','id' => 'assuntoReclamacao','maxlength' => '50'));
                          echo $this->Form->input('ds_reclamacao', array('class' => 'input-xlarge','type' => 'textarea', 'rows'=>'4','label' => 'Descrição da Reclamação','id' => 'dsReclamacao','maxlength' => '250'));
                          ?>
                      </div>
                  </div>
                </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button> 
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>

            </div>
        </div>
    </div>

<script type="text/javascript">
    $("#nuReclamacao").mask("9999/9999");

    $("#fornecedorReclamado").change(function() {
        var fornecedor = $("#fornecedorReclamado").val();
        console.log(fornecedor);
        $.ajax({
            method: "POST",
            dataType: "json",
            url: '<?php $this->base; ?>/reclamacoes/listContratos',
            data: {
                co_fornecedor: fornecedor
            }
        }).success(function(data) {
            var i;
            var selectContrato = document.getElementById("contratoReferente");
            if (selectContrato.length > 0) {
                selectContrato.remove(selectContrato.length-1);
            }
            for(i = 0; i < data.length; i++) {
                $("#contratoReferente").removeAttr("readonly");
                var nuContrato = data[i].Contrato.nu_contrato.substr(0,4) + '/' + data[i].Contrato.nu_contrato.substr(5,8);
                $("#contratoReferente").append('<option value="' + data[i].Contrato.co_contrato + '">' + nuContrato + '</option>');
            }
//            $.each(data, function(data.Contrato.co_contrato, data.Contrato.nu_contrato) {
//                console.log(data.Contrato.co_contrato);
//                $("#contratoReferente").append('<option value=""></option>');
//            })
        });
    });
</script>

