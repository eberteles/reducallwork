<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">

    <?php if(isset($coFornecedor)) { ?>
        <p>Para cadastrar uma Reclamação, selecione Estação de Trabalho => Reclamações</p>
    <?php } ?>

    <?php if(!isset($coFornecedor)) { ?>
        <div class="page-header position-relative"><h1>Reclamações</h1></div>

	    <div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> Preencha um dos campos para fazer a pesquisa</p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'reclamacoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova Reclamação"><i class="icon-plus icon-white"></i> Nova Reclamação</a>
          </div>
        </div>
         
        <?php echo $this->Form->create('Reclamacao', array('url' => array('controller' => 'reclamacoes', 'action' => 'index'))); ?>
            
<div class="row-fluid">
    	<div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('num_reclamacao', array('class' => 'input-xlarge','label' => 'Número da Reclamação')); ?>
            </div>
    	</div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('stt_reclamacao', array('class' => 'input-xlarge','label' => 'Status da Reclamação', 'type' => 'select', 'empty' => 'Selecione', 'options' => $tipos)); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('cod_fornecedor', array('class' => 'input-xlarge chosen-select','label' => 'Fornecedor Reclamado','id' => 'fornecedorReclamado','type' => 'select', 'options' => $fornecedores, 'empty' => 'Selecione')); ?>
            </div>
        </div>
        <div class="span3">
            <div class="controls">
                <?php echo $this->Form->input('cod_contrato', array('class' => 'input-xlarge chosen-select','label' => 'Contrato Referente','id' => 'contratoReferente','type' => 'select', 'options' => $contratos, 'empty' => 'Selecione')); ?>
            </div>
        </div>
</div>
<div class="form-actions">
    <div class="btn-group">
      <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar"><i class="icon-search icon-white"></i> Pesquisar</button>
      <button rel="tooltip" type="reset" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
    </div>
</div>

    <?php } ?>
            
<table cellpadding="0" cellspacing="0"style="background-color:white" class="table table-hover table-bordered table-striped" id="tbModalidade">
	<tr>
		<th>NÚMERO DA RECLAMAÇÃO</th>
		<th>FORNECEDOR RECLAMADO</th>
		<th>CONTRATO REFERENTE</th>
		<th>DATA DO PROBLEMA</th>
		<th>ASSUNTO DA RECLAMAÇÃO</th>
		<th>STATUS DA RECLAMAÇÃO</th>
		<th class="actions">AÇÕES</th>
	</tr>
	<?php
	$i = 0;
	foreach ($reclamacoes as $reclamacao){
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
        <td><?php echo $reclamacao['Reclamacao']['nu_prefix'] . '/' . $reclamacao['Reclamacao']['nu_year']; ?></td>
        <td><?php echo $reclamacao['Fornecedor']['no_razao_social']; ?></td>
        <td><a target="_blank" href="<?php echo $this->base; ?>/contratos/detalha/<?php echo $reclamacao['Contrato']['co_contrato']; ?>"><?php echo substr($reclamacao['Contrato']['nu_contrato'],0,5) . "/" . substr($reclamacao['Contrato']['nu_contrato'],5); ?></a></td>
        <td><?php if($reclamacao['Reclamacao']['dt_problema'] == "30/11/-0001"){ echo ''; } else { echo $reclamacao['Reclamacao']['dt_problema']; } ?></td>
        <td><?php echo $reclamacao['Reclamacao']['assunto_reclamacao']; ?></td>
        <td><?php echo $reclamacao['Reclamacao']['st_reclamacao']; ?></td>
		<td class="actions">
			<div class="btn-group acoes">
				<?php
                    if(!isset($coFornecedor)) {
                        echo $this->element('actions10', array('id' => $reclamacao['Reclamacao']['co_reclamacao'], 'class' => 'btn', 'local_acao' => 'adm_ctb_'));
                    } else { ?>
                        <a href="#view_providencias" class="btn alert-tooltip AbrirProvidencias" co_reclamacao="<?php echo $reclamacao['Reclamacao']['co_reclamacao']; ?>" title="Visualizar Providências Cadastradas" id="AbrirProvidencias" data-toggle="modal"><i class="icon-eye-open"></i></a>
                    <?php } ?>
			</div>
		</td>
	</tr>
    <?php } ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>

<div id="view_providencias" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Providências</h3>
    </div>
    <div class="modal-body-iframe" id="add_providencia">
    </div>
</div>

<script type="text/javascript">
    $(".AbrirProvidencias").bind('click', function(e) {
        var id = $(this).attr('co_reclamacao');
        console.log(id);
        var url_md = "<?php echo $this->Html->url(array('controller' => 'providencias', 'action' => 'iframe')); ?>/" + id;
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
            },
            success:function(result){
                console.log(result);
                $('#add_providencia').html("");
                $('#add_providencia').append(result);
            }
        })
    });
</script>