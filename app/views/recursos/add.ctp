<?php echo $this->Form->create('Recurso', array('url' => "/recursos/add")); ?>

<div class="row-fluid">
    <b>Campos com * são obrigatórios.</b><br>

    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php  __('Recursos'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->input('co_recurso');
                echo $this->Form->input('parent_id', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Agrupador', true), 'options' => $imprimir->getArrayNested($recursos, array('table' => 'Recurso', 'primary' => 'co_recurso', 'label' => 'no_recurso', 'nivel' => 0)), 'escape' => false));
                echo $this->Form->input('no_recurso', array('class' => 'input-xlarge', 'label' => 'Recurso'));
                echo $this->Form->input('ds_rota', array('class' => 'input-xlarge', 'label' => 'Rota'));
                echo $this->Form->input('sg_tipo', array('type' => 'select', 'label' => 'Operação:', 'options' => $arSgTipo));
                echo $this->Form->input('ic_publico', array('type' => 'select', 'label' => 'Público:', 'options' => array(
                	'Não',
                	'Sim',
                )));
                echo $this->Form->input('ic_menu', array('type' => 'select', 'label' => 'Acesso no menu:', 'options' => array(
                	'Não',
                	'Sim',
                )));
                echo $this->Form->input('ic_childs', array('type' => 'select', 'label' => 'Tem filhos:', 'options' => array(
                	'Não',
                	'Sim',
                )));
                echo $this->Form->input('ds_icon', array('class' => 'input-xlarge', 'label' => 'Ícone'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

<?php echo $this->Form->end(); ?>

