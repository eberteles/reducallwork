<div id="recursos">
    <div class="row-fluid">
        <?php echo $this->Form->create('Recurso', array('url' => array('controller' => 'recursos', 'action' => 'index'))); ?>
        <div class="row-fluid">
            <div class="span3">
                <div class="input text">
                    <label for="RecursoNoRecurso">Recurso</label>
                    <input name="data[Recurso][no_recurso]" type="text" class="input-xlarge" maxlength="20"
                           id="RecursoNoRecurso"/>
                </div>
            </div>
            <div class="span3">
                <div class="input text">
                    <label for="RecursoDsRota">Rota</label>
                    <input name="data[Recurso][ds_rota]" type="text" class="input-xlarge" maxlength="20"
                           id="RecursoDsRota"/>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                        title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
                </button>
                <button rel="tooltip" type="button" id="Limpar" title="Limpar dados preenchidos"
                        class="btn btn-small btn-reset" id="Limpar"> Limpar
                </button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>

        <div class="acoes-formulario-top clearfix">
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'add')); ?>"
                   class="btn btn-small btn-primary" title="<?php __('Adicionar Recursos'); ?>"><i
                            class="icon-plus icon-white"></i> <?php __('Adicionar Recursos'); ?></a>
                <a href="javascript:void(0);" id="btnAtualizarRecursos" data-toggle="modal"
                   class="btn btn-small btn-primary" title="<?php __('Atualizar Recursos'); ?>"><i
                            class="icon-refresh icon-white"></i> <?php __('Atualizar Recursos'); ?></a>
            </div>
        </div>

        <table cellpadding="0" cellspacing="0" style="background-color:white"
               class="table table-hover table-bordered table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort(__('Sequência', true), 'co_recurso'); ?></th>
                <th><?php echo $this->Paginator->sort('Recurso', 'no_recurso'); ?></th>
                <th><?php echo $this->Paginator->sort('Rota', 'ds_rota'); ?></th>
                <th><?php echo $this->Paginator->sort('Menu', 'ic_menu'); ?></th>
                <th><?php echo $this->Paginator->sort('Público', 'ic_publico'); ?></th>
                <th><?php echo $this->Paginator->sort('Tipo', 'sg_tipo'); ?></th>
                <th width="60" class="actions"><?php __('Ordem'); ?></th>
                <th width="30" class="actions"><?php __('Ações'); ?></th>
            </tr>
            <?php
            if (count($recursos) > 0):
                $i = 0;
                foreach ($recursos as $key => $recurso):
                    $class = "";
                    if ($recurso['Recurso']['ic_childs']):
                        $class = 'class="expandir"';
                    endif;
                    if ($i++ % 2 == 0) {
                        if ($recurso['Recurso']['ic_childs']):
                            $class = ' class="altrow expandir"';
                        else:
                            $class = ' class="altrow"';
                        endif;
                    }
                    ?>
                    <tr <?php echo $class; ?> style="cursor:pointer;">
                        <td><?php echo $recurso['Recurso']['co_recurso']; ?>&nbsp;<input type="hidden"
                                                                                         class='inputRecurso'
                                                                                         name="recurso[]"
                                                                                         value="<?php echo $recurso['Recurso']['co_recurso']; ?>"/>
                        </td>
                        <td>
                            <?php if ($recurso['Recurso']['ic_childs']): ?>
                                <i class="treeicon icon-chevron-right icon-black"></i>&nbsp;
                            <?php endif; ?>
                            <?php echo $recurso['Recurso']['no_recurso']; ?>&nbsp;
                        </td>
                        <td><?php echo $recurso['Recurso']['ds_rota']; ?>&nbsp;</td>
                        <td><?php echo ($recurso['Recurso']['ic_menu']) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                        <td><?php echo ($recurso['Recurso']['ic_publico']) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                        <td><?php echo $arSgTipo[$recurso['Recurso']['sg_tipo']]; ?>&nbsp;</td>
                        <td class="actions bloqueio">
                            <div class="btn-group">
                                <?php if ($key != 0 || $this->Paginator->hasPrev()): ?>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'ordem', $recurso['Recurso']['co_recurso'], 'up', $page)); ?>"
                                       class="btn alert-tooltip" title="Up"><i class="icon-chevron-up"></i></a>
                                <?php endif; ?>

                                <?php if (($recursos[$key] != end($recursos)) || $this->Paginator->hasNext()): ?>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'ordem', $recurso['Recurso']['co_recurso'], 'down', $page)); ?>"
                                       class="btn alert-tooltip" title="Down"><i class="icon-chevron-down"></i></a>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td class="actions">
                            <div class="btn-group">
                                <a href="<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'edit', $recurso['Recurso']['co_recurso'])); ?>"
                                   class="btn alert-tooltip" title="Editar"><i class="icon-pencil"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan='8'>Nenhum registro encontrado.</td>
                </tr>
            <?php endif; ?>
        </table>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>
        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            </ul>
        </div>
    </div>
</div>

<div id="modalAgrupar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Agrupar Recursos</h3>
    </div>
    <div class="modal-body">
        <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btnModalClose">Fechar</a>
        <a href="#" class="btn btn-primary">Salvar</a>
    </div>
</div>

<?php echo $this->Html->script('jquery.blockUI'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnAtualizarRecursos").click(function () {
            var url = "<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'atualizar')); ?>";
            $.blockUI();
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                success: function (result) {
                    $.unblockUI();
                    location.reload();
                },
                error: function () {
                    $.unblockUI();
                }
            });
        });

        var expandirFunction = function () {
            var trTag = $(this).parents('tr');
            coRecurso = trTag.find("input[type='hidden']").val(),
                url = '<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'childs')); ?>/' + coRecurso;

            $.blockUI();

            $.ajax(url, {
                type: 'GET',
                success: function (response) {

                    var data = $.parseJSON(response),
                        html = "<tr>";
                    html += "<td colspan='8'>";
                    html += "<table class='table table-hover table-bordered table-striped rsc" + coRecurso + "'>";
                    html += "<tr><th>Sequência</th><th>Recurso</th><th>Menu</th><th>Público</th><th width='60' class='actions'><?php __('Ordem');?></th><th width='30'>Ação</th></tr>"

                    if (data.length <= 0) {
                        html += "<tr><td colspan='6'>Nenhum registro encontrado.</td></tr>";
                    } else {
                        $.each(data, function (index, item) {
                            var checked = "";

                            if (item.Permissao.length > 0
                                && item.Permissao[0].co_recurso == item.Recurso.co_recurso) {
                                checked = "checked='checked'";
                            }
                            if (item.Recurso.ic_childs == 1) {
                                html += "<tr class='expandir' style='cursor:pointer;'>";
                            } else {
                                html += "<tr>";
                            }

                            html += "<td width='25'>" + item.Recurso.co_recurso + "&nbsp;<input type='hidden' class='inputRecurso' name='recurso[" + item.Recurso.parent_id + "]' value='" + item.Recurso.co_recurso + "' /></td>";

                            if (item.Recurso.ic_childs == 1) {
                                html += "<td><i class='treeicon icon-chevron-right icon-black'></i>&nbsp;" + item.Recurso.no_recurso + "</td>";
                            } else {
                                html += "<td>" + item.Recurso.no_recurso + "</td>";
                            }

                            if (item.Recurso.ic_menu == 1) {
                                html += "<td width='50'>Sim&nbsp;</td>";
                            } else {
                                html += "<td width='50'>Não&nbsp;</td>";
                            }

                            if (item.Recurso.ic_publico == 1) {
                                html += "<td width='50'>Sim&nbsp;</td>";
                            } else {
                                html += "<td width='50'>Não&nbsp;</td>";
                            }

                            html += '<td class="ordens bloqueio">';
                            html += '<div class="btn-group">';
                            if (index > 0) {
                                html += '<a href="javascript:void(0);" class="btn ordenar alert-tooltip" title="Up"><i class="icon-chevron-up"></i></a>';
                            }
                            if (data[index] != data[data.length - 1]) {
                                html += '<a href="javascript:void(0);" class="btn ordenar alert-tooltip" title="Down"><i class="icon-chevron-down"></i></a>';
                            }
                            html += '</div></td>';

                            html += '<td width="20"><div class="btn-group"><a href="<?php echo $this->Html->url(array ('controller' => 'recursos', 'action' => 'edit')); ?>/' + item.Recurso.co_recurso + '" class="btn alert-tooltip" title="Editar"><i class="icon-pencil"></i></a></div></td>';

                            html += "</tr>";
                        });
                    }

                    html += "</td>";
                    html += "</tr>";
                    html += "</table>";

                    $(trTag).after(html);
                    trTag.removeClass('expandir altrow').addClass('recolher');
                    trTag.find('i.treeicon').removeClass('icon-chevron-right').addClass('icon-chevron-down');

                    $.unblockUI();

                },
                error: function (response) {
                    $.unblockUI();
                }
            });
        };

        var recolherFunction = function () {
            var trTag = $(this).parents('tr');

            trTag.next('tr').remove();
            trTag.removeClass('recolher').addClass('expandir altrow');
            trTag.find('i.treeicon').removeClass('icon-chevron-down').addClass('icon-chevron-right');
        };

        $(document).delegate('tr.recolher td:not(:first-child,:last-child,.bloqueio)', 'click', recolherFunction);
        $(document).delegate('tr.expandir td:not(:first-child,:last-child,.bloqueio)', 'click', expandirFunction);

        var ordenar = function () {
            var coRecurso = $($(this).parents('tr')[0]).find('.inputRecurso').val(),
                url = '',
                link = $(this);

            if ($(this).find('i').hasClass('icon-chevron-up')) {
                url = '<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'ordem')); ?>/' + coRecurso + '/up/null/json';
            } else {
                url = '<?php echo $this->Html->url(array('controller' => 'recursos', 'action' => 'ordem')); ?>/' + coRecurso + '/down/null/json';
            }

            $.ajax(url, {
                type: 'GET',
                success: function (response) {
                    // Subir
                    if (link.find('i').hasClass('icon-chevron-up')) {
                        var anterior = $(link.parents('tr')[0]).prev(),
                            atual = $(link.parents('tr')[0]),
                            ordensAnterior = anterior.find('.ordens').html(),
                            ordensAtual = atual.find('.ordens').html();

                        if (anterior) {
                            anterior.find('.ordens').html(ordensAtual);
                            atual.find('.ordens').html(ordensAnterior);
                            anterior.insertAfter(atual);
                        }
                    } else {
                        var proximo = $(link.parents('tr')[0]).next(),
                            atual = $(link.parents('tr')[0]),
                            ordensProximo = proximo.find('.ordens').html(),
                            ordensAtual = atual.find('.ordens').html();

                        if (proximo) {
                            proximo.find('.ordens').html(ordensAtual);
                            atual.find('.ordens').html(ordensProximo);
                            proximo.insertBefore(atual);
                        }
                    }
                }
            });
        };

        $(document).delegate('a.ordenar', 'click', ordenar);

    });
</script>
