<?php
$usuario = $this->Session->read ('usuario');
echo $this->Html->script( 'inicia-datetimepicker' );
$temPermissaoParaRelatorioEmExcel = Configure::read('App.config.resource.available.relatorioExcel');
?>

<script>
    $(function(){
        $('periodos').click(function(event) {
            num = $('.periodos:checkbox:checked').size();
            if (num > 10) {
                alert('Número de colunas selecionadas excedido, por favor selecionar até 10 colunas.');
                event.preventDefault();
            }
        });

        $('#opt_processo').click(function() {
            $('#nu_processo').focus();
        });

        $('#nu_processo').click(function() {
            $('#opt_processo').attr("checked", true);
        });

        $('#opt_razao_social').click(function() {
            $('#no_razao_social').focus();
        });

        $('#no_razao_social').click(function() {
            $('#opt_razao_social').attr("checked", true);
        });
        $('#gerar_relatorio').click(function(event) {
            $('input:radio[name=especie]').each(function() {
                if ($(this).is(':checked')) {

                    nome = $(this).val();
                    campo = $('input[name=' + nome + ']');
                    if (campo.length && campo.val() == '') {
                        alert('Favor entrar com as informações no campo selecionado em especie.');
                        event.preventDefault();
                        return false;
                    }

                }
            });
        });
    });
</script>

<div class="relatorios index">

    <style>
        input[type=checkbox] {
            float: none;
        }
        input[type=radio] {
            float: none;
        }
    </style>

    <div class="row-fluid">
        <div class="page-header position-relative"><h1><?php __('Relatórios'); ?></h1></div>

        <div class="box-tabs-white">
            <ul class="nav nav-tabs" id="myTab">
                <?php
                $abas = array();
                $abas[] = array('fsOperacional', 'Relatórios Básicos', true);
                echo $aba->render($abas);
                ?>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div>
                    <!-- RELATÓRIOS OPERACIONAIS -->

                    <fieldset id="fsOperacional" class="ativo">
                        <table style="background-color:white" class="table table-hover table-bordered table-striped">

                            <?php if($this->Modulo->isPam()) { ?>
                                <!-- Relatório de Lista de PAMs por tempo em fase (Item 43) -->
                                <tr>
                                    <td>1. <?php __('Lista'); ?> de <?php __('PAM/s'); ?>:</td>
                                    <td>
                                        <?php echo $this->Html->link('Pdf', '#impListaGeral', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array('controller' => 'relatorios', 'action' => 'listarProspeccao')))); ?>
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'relatorios', 'action' => 'listarPamPorTempoFase')); ?>?tipo=excel" target="blank" class="btn btn-small">Excel</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <!-- Fim do Item 43 -->
                            <?php } ?>

                            <!-- Relatório Lista Geral de Contratos/Processos (Item 44, 45 e 59) -->
                            <tr>
                                <td>2. <?php __('Lista'); ?> Geral de <?php __('Contratos'); ?>/<?php __('Processos'); ?>:</td>
                                <td>
                                <br/>
                                <div class="row">
                                  <div class="span6" style="text-align:right">Situação do Contrato:</div>
                                  <div class="span6" style="text-align:left"><?php echo $this->Form->input('co_situacao', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => false, 'empty' => array(0 => 'Selecione...'), 'options' => $situacaos )); ?></div>
                                </div>
                                <div class="row">
                                  <div class="span6" style="text-align:right">Unidade Administrativa:</div>
                                  <div class="span6" style="text-align:left"><?php echo $this->Form->input('co_setores', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => false, 'empty' => 'Selecione...', 'options' => $setores )); ?></div>
                                </div>
                                <div class="row">
                                  <div class="span6" style="text-align:right">Ano:</div>
                                  <div class="span6" style="text-align:left"><?php echo $this->Form->input('co_ano_cge', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'label' => false, 'empty' => 'Selecione...', 'options' => $anos)); ?></div>
                                </div>
                                <div class="row">
                                  <div class="span6" style="text-align:right">Período:</div>
                                  <div class="span6" style="text-align:left">
                                        <?php
                                            $options=array('30'=>' 30 ','60'=>' 60 ','90'=>' 90 ','120'=>' 120 ','null'=>' Todos');
                                            $attributes=array('label' => false, 'legend'=>false, "div"=>true, "class" => true, 'value' => 'null');
                                            echo $this->Form->radio('intervalo',$options,$attributes);
                                        ?>
                                  </div>
                                </div>
                                    <?php
                                        echo $this->Html->link('Pdf', '#impListaGeral', array('name' => 'pdf', 'class' => 'btn btn-small btImpUrlSimples relatorioGeral', 'url_rel' => $this->Html->url(array('controller' => 'relatorios', 'action' => 'listarContratosGeral'))));
                                    ?>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'relatorios', 'action' => 'listarContratosGeral')); ?>?tipo=excel" target="blank" name="excel" class="btn btn-small relatorioGeral">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim dos Itens 44, 45 e 59 -->

                            <!-- Relatório Lista de Contratos com Pendências (Item 51) -->
                            <?php if($this->Modulo->getRelatorioContratosComPendencias()) { ?>
                                <tr>
                                    <td>3. <?php __('Lista'); ?> de <?php __('Contratos'); ?> com Pendências:</td>
                                    <td>
                                        <?php echo $this->Html->link('Pdf', '#impPend', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array('controller' => 'relatorios', 'action' => 'contratosComPendencia')))); ?>
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'relatorios', 'action' => 'contratosComPendencia')); ?>?tipo=excel" target="blank" class="btn btn-small">Excel</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <!-- Fim do Item 51 -->

                            <!-- Relatório Lista de Fiscais por Contrato (Item 46) -->
                            <?php if($this->Modulo->getRelatorioFiscaisPorContrato()) { ?>
                                <tr>
                                    <td>4. <?php __('Lista'); ?> de <?php __('Fiscais'); ?> por <?php __('Contrato'); ?>:</td>
                                    <td>
                                        <?php echo $this->Html->link('Pdf', '#impFiscaisC', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array('controller' => 'relatorios', 'action' => 'fiscaisPorContrato')))); ?>
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <a href="<?php echo $this->Html->url(array('controller' => 'relatorios', 'action' => 'fiscaisPorContrato')); ?>?tipo=excel" target="blank" class="btn btn-small">Excel</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <!-- Fim do Item 46 -->

                            <!-- Relatório de Contratos por Tipo (Item 47) -->
                            <?php if($this->Modulo->getRelatorioContratosPorTipo()) { ?>
                                <tr>
                                    <td>5. <?php __('Lista'); ?> de <?php __('Contratos'); ?> por Tipo:</td>
                                    <td>
                                        <?php echo $this->Form->input(
                                                'co_modalidade',
                                                array(
                                                    'class' => 'input-xlarge chosen-select',
                                                    'type' => 'select',
                                                    'empty' => 'Selecione...',
                                                    'label' => 'Tipo:',
                                                    'options' => $modalidades,
                                                    'after' => '<br/><input id="relTpVigentes" type="checkbox">Exibir somente Vigentes<br/>'
                                                )
                                            );
                                        ?>
                                        <input type="button" id="impRelPorTipo" class="btn btn-small" value="Pdf">
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <input type="button" class="btn btn-small" value="Excel" onclick="imprimirRelatorioPorTipo('excel')();">
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <!-- Fim do Item 47 -->

                            <!-- Relatório Lista de Contratos por tipo de Modalidade de Contratação (Item 49) -->
                            <?php if($this->Modulo->getRelatorioContratosPorModalidade() && $this->Modulo->isCamposContrato('ds_contratacao')) : ?>
                            <tr>
                                <td>6. <?php __('Lista'); ?> de <?php __('Contratos'); ?> por Tipo de Modalidade de Contratação:</td>
                                <td>
                                    <?php echo $this->Form->input(
                                            'co_contratacao',
                                            array(
                                                'type' => 'select',
                                                'empty' => 'Selecione...',
                                                'label' => 'Tipo:',
                                                'options' => $contratacoes,
                                                'after' => '<br/><input id="relTpVigentesModalidade" type="checkbox">Exibir somente Vigentes<br/>'
                                            )
                                        );
                                    ?>
                                    <input type="button" id="impRelPorModalidade" class="btn btn-small" value="Pdf">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <input type="button" value="Excel" class="btn btn-small" onclick="imprimeRelatorioPorModalidade('excel')();">
                                    <?php endif; ?>
                            </tr>
                            <?php endif ?>
                            <!-- Fim do Item 49 -->

                            <!-- Relatório Lista de Contratos por Categoria (Item 52) -->
                            <?php if($this->Modulo->isCamposContrato('co_categoria') == true) : ?>
                            <tr>
                                <td>7. <?php __('Lista'); ?> de <?php __('Contratos'); ?> por Categoria:</td>
                                <td id="lista-contratos-por-categoria">
                                    <?php
                                        echo $this->Form->input(
                                                'co_categoria',
                                                array(
                                                    'class' => 'input-xlarge chosen-select',
                                                    'type' => 'select',
                                                    'empty' => 'Selecione...',
                                                    'label' => 'Categoria:',
                                                    'options' => $categorias,
                                                )
                                        );
                                    ?>
                                    <br/>
                                    <input
                                        type="button"
                                        value="Pdf"
                                        class="btn btn-small"
                                        onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorCategoria')); ?>/' + document.getElementById('co_categoria').value);"
                                    />
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a
                                        href="#lista-contratos-por-categoria"
                                        class="btn btn-small"
                                        onclick="window.open('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorCategoria')); ?>/' + document.getElementById('co_categoria').value + '?tipo=excel', '_blank');"
                                        >Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <!-- Fim do Item 52 -->

                            <!-- Relatório Lista de Contratos por Fiscais (Item 53) -->
                            <?php if($this->Modulo->getRelatorioContratosPorFiscais()) : ?>
                            <tr>
                                <td>8. <?php __('Lista'); ?> de <?php __('Contratos'); ?> por <?php __('Fiscais'); ?>:</td>
                                <td>
                                    <?php echo $this->Html->link('Pdf', '#impCtrFisc', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorFiscais')))); ?>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorFiscais')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <!-- Fim do Item 53 -->

                            <!-- Relatório de Lista de Contratos em Garantia (Item 50) -->
                            <?php if ($this->Modulo->isGarantia()) : ?>
                            <tr>
                                <td>9. <?php __('Lista'); ?> de <?php __('Contratos'); ?> sem <?php __('Garantias'); ?>:</td>
                                <td>
                                    <?php echo $this->Html->link('Pdf', '#impCtrSemGar', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosSemGarantias')))); ?>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosSemGarantias')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <!-- Fim do Item 50 -->

                            <tr>
                                <td>10. <?php __('Lista'); ?> de <?php __('Fornecedores'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onClick="javascript:imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'fornecedores')); ?>')">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'fornecedores')); ?>?tipo=excel" class="btn btn-small" target="_blank">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <!-- [GES-599] Listar contratos com aditivos pendentes -->
                            <tr>
                                <td>10. <?php __('Lista'); ?> de <?php __('Contratos'); ?> com Aditivos Pendentes:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onClick="javascript:imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'listaContratosAditivosPendentes')); ?>')">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'listaContratosAditivosPendentes')); ?>?tipo=excel" class="btn btn-small" target="_blank">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <?php if($this->Modulo->isEmpenho()) : ?>
                            <!-- Relatório de Empenhos por Contrato/Processo (Item 48) -->
                            <tr>
                                <td>11. Relatório de <?php __('Empenhos'); ?> dos <?php __('Contratos'); ?>/<?php __('Processos'); ?> ativos</td>
                                <td>
                                    <?php echo $this->Html->Link('Pdf', '#impEmpCtrAtv', array('class' => 'btn btn-small btImpUrlSimples', 'url_rel' => $this->Html->url(array ('controller' => 'relatorios', 'action' => 'empenhosContratosAtivos')))); ?>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'empenhosContratosAtivos')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 48 -->
                            <?php endif; ?>

                            <!-- Relatório de Responsáveis por Atividades (Item 60, 62) -->
                            <tr>
                                <td>12. Relatório de <?php __('Responsáveis'); ?> e Percentuais por <?php __('Atividade'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'responsaveisAtividades')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'responsaveisAtividades')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 60, 62 -->

                            <!-- Relatório de Quantidade de Contratos Realizados Mensalmente por Ano (Item 61) -->
                            <tr id="relatorio-quantidade-contratos-realizados por-mes">
                                <td>13. Quantidade de Contratos Realizados Mensalmente por Ano: </td>
                                <td>
                                    <?php echo $this->Form->input('co_ano', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Ano:', 'options' => $anos)); ?>
                                    <br/>
                                    <input type="button" id="impRelQtdMensalContratos" class="btn btn-small" value="Pdf"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="#relatorio-quantidade-contratos-realizados por-mes" class="btn btn-small" onclick="imprimeRelatorioDeQuantidadeMensalDeContratos('excel')();">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 61 -->

                            <!-- Relatório de Datas das Atividades (Item 63) -->
                            <tr>
                                <td>14. Relatório de  Datas das <?php __('Atividades'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'datasAtividades')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'datasAtividades')); ?>?tipo=excel" class="btn btn-small" target="_blank">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 63 -->

                            <!-- Relatório de Atividades Atrasadas (Item 66) -->
                            <tr>
                                <td>15. Relatório de <?php __('Atividades'); ?>  <?php __('Atrasadas'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'atividadesAtrasadas')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'atividadesAtrasadas')); ?>?tipo=excel" class="btn btn-small" target="_blank">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 66 -->

                            <tr>
                                <td>16. Lista de <?php __('Contratos'); ?> com <?php echo __('Pagamento'); ?> em atraso:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onClick="javascript:imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPagamentosAtraso')); ?>')">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPagamentosAtraso')); ?>?tipo=excel" class="btn btn-small" target="_blank">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <!-- Relatório de Atividades com Pendências (Item 67) -->
                            <tr>
                                <td>17. Relação de <?php __('Atividades'); ?> com Pendências: </td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'listarAtividadesPendentes')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'listarAtividadesPendentes')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 67 -->

                            <!-- Relatório de Liquidações por Pagamento (Item 68) -->
                            <tr>
                                <td>18. Relatório de Liquidações por <?php echo __('Pagamento'); ?>: </td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'liquidacoesPorPagamento')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'liquidacoesPorPagamento')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 68 -->

                            <!-- Relatório Financeiro de Atividades (Item 64) -->
                            <tr>
                                <td>19. Relatório Financeiro de <?php __('Atividade'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'financeiroAtividades')); ?>');" />
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'financeiroAtividades')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 64 -->

                            <!-- Relatório de Processos por Fase de Tramitação (Item 56) -->
                            <tr>
                                <td>20. Relatório de Processos por Fase de Tramitação:</td>
                                <td>
                                    <?php echo $this->Form->input('dsFase', array('type' => 'select', 'empty' => 'Selecione...', 'label' => 'Fases: ', 'options' => $fases)) ?>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'processosPorFaseTramitacao')); ?>/' + document.getElementById('dsFase').value);" />
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <input type="button" value="Excel" class="btn btn-small" onclick="window.open('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'processosPorFaseTramitacao')); ?>/' + document.getElementById('dsFase').value + '?tipo=excel', '_blank');" />
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 56 -->

                            <!-- Relatório de Itens de Produtos Fornecidos por Processo/Contrato (Item 69) -->
                            <tr>
                                <td>21. Relatório de Itens de Produtos fornecidos por Processo / Contrato:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'produtosFornecidos')); ?>');"/>
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'produtosFornecidos')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 69 -->

                            <!-- Relatório de Itens de Produtos que Compõem o Processo/Contrato (Item 70) -->
                            <tr>
                                <td>22. Relatório de Itens de Produtos que compõem o Processo / Contrato:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'produtosCompoem')); ?>');">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'produtosCompoem')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 70 -->

                            <!-- Relatório de Atestos das Atividades/Tarefas dos Contratos (Item 65) -->
                            <tr>
                                <td>23. Relatório de Atestos das Atividades/Tarefas dos Contratos:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'atestosAtividades')); ?>');">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'atestosAtividades')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 65 -->

                            <!-- Relatório de Percentual de Contratos por Tipo (Item 58) -->
                            <tr>
                                <td width="300">24. Percentual de <?php __('Contratos'); ?> por <?php __('Tipo de Contrato'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPorTipo')); ?>');">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPorTipo')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 58 -->

                            <!-- Relatório de Contratos Anualmente Celebrados (Item 57) -->
                            <tr>
                                <td width="300">25. Percentual e Quantidade de <?php __('Contratos'); ?> Anualmente Celebrados por <?php __('Situação'); ?>:</td>
                                <td>
                                    <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPorStatus')); ?>');">
                                    <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                    <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPorStatus')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <!-- Fim do Item 57 -->

                            <?php if($this->Modulo->getRelEstContratosEncPagAno()) : ?>
                                <!-- Relatório de Contratos Encaminhados para Pagamentos por Ano (Item 55) -->
                                <tr>
                                    <td width="300">26. Percentual de <?php __('Contratos'); ?> encaminhados para <?php echo __('Pagamentos'); ?> por Ano:</td>
                                    <td>
                                        <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'percentualContratosPagamentoPorAno')); ?>');">
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'percentualContratosPagamentoPorAno')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <!-- Fim do Item 55 -->
                            <?php endif; ?>

                            <?php if ($this->Modulo->getRelEstQtdEPercPgtoAtraso()) : ?>
                                <!-- Relatório de Contratos com Pagamentos em Atraso (Item 54) -->
                                <tr>
                                    <td>27. Quantidade e % de <?php __('Contratos'); ?> com <?php echo __('Pagamentos'); ?> em atraso:</td>
                                    <td>
                                        <input type="button" value="Pdf" class="btn btn-small" onclick="imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPagamentoAtraso')); ?>');">
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <a href="<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratoPagamentoAtraso')); ?>?tipo=excel" target="_blank" class="btn btn-small">Excel</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <!-- Fim do Item 54 -->
                            <?php endif; ?>

                            <?php if ($this->Modulo->RelItensAta) : ?>
                                <!-- Relatório Itens de Ata -->
                                <tr>
                                    <td>28. Itens de Ata:</td>
                                    <td>
                                        <input type="number" class="x-large" id="uasg_id" value="" placeholder="Uasg Gerenciadora" />
                                        <input type="number" class="x-large" id="licitacao_numero" value="" placeholder="Nº da Licitação" />
                                        <input type="number" class="x-large" id="processo_id" value="" placeholder="Nº do Processo" />
                                        <input type="text" pattern="[a-zA-Z0-9]" class="x-large" id="item_nome" value="" placeholder="Item Licitado" />
                                        <br />
                                        <input type="button" value="Pdf" class="btn btn-small" id="btnPdfRelatorioItensDeAta" />
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <input type="button" value="Excel" class="btn btn-small" id="btnExcelRelatorioItensDeAta" />
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <!-- Fim do Item -->
                            <?php endif; ?>

                            <?php if ($this->Modulo->RelConsumoItensAta) : ?>
                                <!-- Relatório Consumo de Itens de Ata -->
                                <tr>
                                    <td>29. Consumo de Itens de Ata:</td>
                                    <td>
                                        <input type="number" class="x-large" id="consumo_uasg_id" value="" placeholder="Uasg Gerenciadora" />
                                        <input type="number" class="x-large" id="consumo_licitacao_numero" value="" placeholder="Nº da Licitação" />
                                        <input type="number" class="x-large" id="consumo_processo_id" value="" placeholder="Nº do Processo" />
                                        <input type="text" pattern="[a-zA-Z0-9]" class="x-large" id="consumo_item_nome" value="" placeholder="Item Licitado" />
                                        <br />
                                        Tipo de Uasg:
                                        <select class="x-large" id="tipo_uasg">
                                            <option value="">Todas</option>
                                            <option value="Gerenciadora">Gerenciadora</option>
                                            <option value="Participante">Participante</option>
                                            <option value="Carona">Carona</option>
                                        </select>
                                        <br />

                                        <input type="button" value="Pdf" class="btn btn-small" id="btnPdfRelatorioConsumoItensDeAta" />
                                        <?php if ($temPermissaoParaRelatorioEmExcel) : ?>
                                        <input type="button" value="Excel" class="btn btn-small" id="btnExcelRelatorioConsumoItensDeAta" />
                                        <?php endif; ?>
                                        </td>
                                </tr>
                                <!-- Fim do Item -->
                            <?php endif; ?>

                        </table>
                    </fieldset>

                    <!-- FIM DOS RELATÓRIOS OPERACIONAIS -->

                </div>
            </div>
        </div>
    </div>
</div>

<div id="view_imp_relatorio" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <p class="pull-right">
            <a id="imprimir_pdf" href="#imprimir_pdf" class="btn btn-small btn-primary" ><i class="icon-print"></i> Imprimir</a>
            <a class="btn btn-small" data-dismiss="modal" ><i class="icon-remove"></i> Fechar</a>
        </p>
        <h3 id="tituloAbaImpressoa">Impressão de Relatório</h3>
    </div>
    <div class="modal-body maior" id="impressao" style="background: #fff url(<?php echo $this->base; ?>/img/ajaxLoader.gif) no-repeat center;"></div>
</div>

<script>

    $(document).ready(function(e){

        $("#cpvl1").maskMoney({thousands:'.', decimal:','});
        $("#cpvl2").maskMoney({thousands:'.', decimal:','});
        $("#nuContEmp").mask('<?php echo FunctionsComponent::pegarFormato('contrato'); ?>');

        // imprimir relatório
        function imprimir(url_relatorio) {
            $('#view_imp_relatorio').modal();
            $('#impressao').html("");
            $.ajax({
                type:"POST",
                url:"<?php echo $this->base; ?>/relatorios/iframe_imprimir/",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                data:{
                    "data[url]": "<?php echo $this->base; ?>" + url_relatorio
                },
                success:function(result){
                    $('#impressao').append(result);
                },
                error: function (err) {
                    console.log('erro: ', err);
                }
            });
        }
        window.imprimir = imprimir;

        $('.btImpUrlSimples').click(function(){
            imprimir($(this).attr('url_rel'));
        });

        $('#imprimir_pdf').click(function(){
            var frm = document.getElementById("iframe_imp_contrato").contentWindow;
            frm.focus();
            frm.print();
            return false;

        });

        function imprimirRelatorioPorTipo (tipoRelatorio) {
            return function(){
                var coModalidade = 0;
                var tpExibicao = 0;
                if($('#co_modalidade').val() > 0) {
                    coModalidade = $('#co_modalidade').val();
                }
                if($('#relTpVigentes').is(":checked")) {
                    tpExibicao = 1;
                }
                var url = '<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorTipo')); ?>/' + coModalidade + '/' + tpExibicao;
                if (tipoRelatorio === 'excel') {
                    window.open(url + '?tipo=excel', '_blank');
                } else {
                    imprimir(url);
                }
            };
        };
        window.imprimirRelatorioPorTipo = imprimirRelatorioPorTipo;

        $('#impRelPorTipo').click(imprimirRelatorioPorTipo(''));

        function imprimeRelatorioDeQuantidadeMensalDeContratos(tipoRelatorio) {
            return function() {
                var ano = 0;
                if($('#co_ano').val() > 0) {
                    ano = $('#co_ano').val();
                }
                var url = '<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'quantidadeContratosPorMes')); ?>/' + ano;
                if (tipoRelatorio === 'excel') {
                    window.open(url + '?tipo=excel', '_blank');
                } else {
                    imprimir(url);
                }
            };
        };
        $('#impRelQtdMensalContratos').click(imprimeRelatorioDeQuantidadeMensalDeContratos(''));
        window.imprimeRelatorioDeQuantidadeMensalDeContratos = imprimeRelatorioDeQuantidadeMensalDeContratos;

        $('#impRelPorSituacao').click(function(){
            var coSituacao = 0;
            var tpExibicao = 0;
            if($('#co_situacao').val() > 0) {
                coSituacao = $('#co_situacao').val();
            }
            if( $('#relTpVigentesSituacao').is(":checked") ) {
                tpExibicao  = 1;
            }
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorStatus')); ?>/' + coSituacao + '/' + tpExibicao);
        });

        $('#impRelPorFase').click(function(){
            var coFase = 0;
            if($('#co_fase').val() > 0) {
                coFase = $('#co_fase').val();
            }
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosCiPorFase')); ?>/' + coFase );
        });

        function imprimeRelatorioPorModalidade (tipoRelatorio) {
            return function(){
                var coModalidade = 0;
                var tpExibicao = 0;
                if($('#co_contratacao').val() > 0) {
                    coModalidade = $('#co_contratacao').val();
                }
                if($('#relTpVigentesModalidade').is(":checked")) {
                    tpExibicao  = 1;
                }
                var url = '<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorTipoContratacao')); ?>/' + coModalidade + '/' + tpExibicao;
                if (tipoRelatorio === 'excel') {
                    window.open(url + '?tipo=excel', '_blank');
                } else {
                    imprimir(url);
                }
            };
        };
        // por tipo de modalidade de contratação
        $('#impRelPorModalidade').click(imprimeRelatorioPorModalidade(''));
        window.imprimeRelatorioPorModalidade = imprimeRelatorioPorModalidade;

        $('#impRelPorUnidadeSolicitante').click(function(){
            var coSetorProcesso = 0;
            var tpExibicao = 0;
            if($('#co_setores').val() > 0) {
                coSetorProcesso = $('#co_setores').val();
            }
            if($('#relUnidadeSolicitante').is(":checked")) {
                tpExibicao  = 1;
            }
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosPorUnidadeSolicitante')); ?>/' + coSetorProcesso + '/' + tpExibicao);
        });

        $('#impRelPorUnidadeSolicitanteProcessos').click(function(){
            var coSetorProcesso = 0;
            var tpExibicao = 0;
            if($('#co_setorCP').val() > 0) {
                coSetorProcesso = $('#co_setorCP').val();
            }

            var coMes = $("#relUSM").val();

            if($('#relUnidadeSolicitanteProcesso').is(":checked")) {
                tpExibicao  = 1;
            }
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosEProcessosPorUnidadeSolicitante')); ?>/' + coSetorProcesso + '/' + tpExibicao + '/' + coMes);
        });

        $('#impRelAExpirar').click(function(){
            var tpExibicao = 0;
            if($('#relTpAExpirar').is(":checked")) {
                tpExibicao  = 1;
            }
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'contratosAExpirar')); ?>/' + document.getElementById('sel1').value + '/' + document.getElementById('sel2').value + '/' + tpExibicao);
        });

        $('#impRelHistoricos').click(function(){
            imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'historioPorPeriodo')); ?>/' + document.getElementById('selHis1').value + '/' + document.getElementById('selHis2').value);
        });

        $('#impRelHistoricosPorTipoOcorrencia').click(function(){
            imprimir('/relatorios/impRelHistoricosPorTipoOcorrencia/' + document.getElementById('tipoOcorrencia').value);
        });

        setMascaraCampo("#ContratoNuProcesso", "<?php echo FunctionsComponent::pegarFormato('processo'); ?>");

        $("#co_funcionario").change( function() {
            $.getJSON("<?php echo $this->Html->url(array ('controller' => 'indenizacoes', 'action' => 'listar_ano') )?>" + "/" + $(this).val(), null, function(data){
                var options = '<option value="">Selecione..</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#co_ano_ind").html(options);
                $("#co_ano_ind").trigger("chosen:updated");
            });
        });

        $("#co_ano_ind").change( function() {
            $.getJSON("<?php echo $this->Html->url(array ('controller' => 'indenizacoes', 'action' => 'listar_mes') )?>" + "/" + $("#co_funcionario").val() + "/" + $(this).val(), null, function(data){
                var options = '<option value="">Selecione..</option>';
                $.each(data, function(index, val) {
                    options += '<option value="' + index + '">' + val + '</option>';
                });
                $("select#co_mes_ind").html(options);
                $("#co_mes_ind").trigger("chosen:updated");
            });
        });

        const generateRelatorioUrl =
            (basePath, filter) =>
            basePath + Object.keys(filter)
                             .filter(field => filter[field] !== "")
                             .reduce(
                                (prev, next) => prev + (prev === "" ? "?" : "&") + next + "=" + filter[next],
                                "");

        const imprimirRelatorioAtas = (controller, action) => (tipo, filtroRetriever) => () => {
            const baseUrl = location.protocol + "//" + location.host + controller + action + tipo;

            imprimir(generateRelatorioUrl(baseUrl, filtroRetriever()));
        };

        const getFiltroItensAta = () => {
            const uasg_id          = $("#uasg_id").val();
            const licitacao_numero = $("#licitacao_numero").val();
            const processo_id      = $("#processo_id").val();
            const item_nome        = $("#item_nome").val();

            return {uasg_id, licitacao_numero, processo_id, item_nome}
        };

        const getFiltroConsumoItensAta = () => {
            const uasg_id          = $("#consumo_uasg_id").val();
            const licitacao_numero = $("#consumo_licitacao_numero").val();
            const processo_id      = $("#consumo_processo_id").val();
            const item_nome        = $("#consumo_item_nome").val();
            const tipo_uasg        = $("#tipo_uasg").val();

            return {uasg_id, licitacao_numero, processo_id, item_nome, tipo_uasg}
        };

        $("#btnPdfRelatorioItensDeAta").click(
            imprimirRelatorioAtas(
                location.pathname,
                '/relatorioItensAta/'
            )('PDF', getFiltroItensAta)
        );

        $("#btnExcelRelatorioItensDeAta").click(
            imprimirRelatorioAtas(
                location.pathname,
                '/relatorioItensAta/'
            )('EXCEL', getFiltroItensAta)
        );

        $("#btnPdfRelatorioConsumoItensDeAta").click(
            imprimirRelatorioAtas(
                location.pathname,
                '/relatorioConsumoItensAta/'
            )('PDF', getFiltroConsumoItensAta)
        );

        $("#btnExcelRelatorioConsumoItensDeAta").click(
            imprimirRelatorioAtas(
                location.pathname,
                '/relatorioConsumoItensAta/'
            )('EXCEL', getFiltroConsumoItensAta)
        );
    });

    $('.relatorioGeral').click(function(){

        var situacao = $('#co_situacao').val();
        var setor = $('#co_setores').val();
        var periodo = $('input[type="radio"]:checked').val();
        var ano = $('#co_ano_cge').val();
        var excel = '';

        if(setor == ''){
            setor = 'null';
        }

        if(this.name == 'excel'){
            excel = '?tipo=excel'
        }

        imprimir('<?php echo $this->Html->url(array ('controller' => 'relatorios', 'action' => 'listarContratosGeral')); ?>/' + periodo + '/' + situacao + '/' + setor + '/' + ano + '/' + excel);

    });


</script>
