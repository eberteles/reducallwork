<?php echo $this->Form->create('ProcessoSei', array('url' => "/sei/add_processo/{$coContrato}")); ?>

<div class="row-fluid">
    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Inclusão de Processo SEI'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->hidden('co_contrato', array('value' => $coContrato));
                echo $this->Form->input('nu_processo_sei', array(
                        'class' => 'input-xlarge',
                        'label' => 'Nr Processo',
                        'mask' => FunctionsComponent::pegarFormato('processo')
                    )
                );
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary" title="Salvar">Salvar</button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                Limpar
            </button>
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

