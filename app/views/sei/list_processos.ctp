<style>
    #btn-visualiza-tela-cheia, .indent ~ .glyphicon-unchecked { display: none; }
    object {
        width: 100%;
        min-height: 300px;
        position: absolute;
        display: block;
    }
</style>
<form id="form-delete" action=""></form>
<?php echo $this->Html->css('../bower_components/bootstrap/dist/css/bootstrap.min'); ?>
<?php echo $this->Html->css('../bower_components/bootstrap-treeview/dist/bootstrap-treeview.min'); ?>
<div>
    <div class="row">
        <?php
        echo $this->Form->create('ProcessoSei', array(
            'onsubmit' => '$("#btn-vincular-processo-sei").attr("disabled", true); return true;',
            'url' => "/sei/add_processo/{$coContrato}"
        ));
        ?>
        <div class="col-md-4">
            <div class="form-group">
                <input id="id-contrato" type="hidden" name="data[ProcessoSei][co_contrato]" value="<?php echo $coContrato; ?>"/>
                <input
                    type="input"
                    id="input-search"
                    name="data[ProcessoSei][nu_processo_sei]"
                    class="form-control"
                    placeholder="Digite para pesquisar/adicionar..."
                    value=""
                    mask="<?php echo FunctionsComponent::pegarFormato('processo'); ?>"
                    style="margin-left: 25px;"
                    />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <button id="btn-vincular-processo-sei" type="submit" class="btn btn-primary">Vincular</button>
                <a id="btn-remover-processo" href="javascript:void(0);" class="btn btn-danger" disabled="disabled" onclick="excluirProcesso();">
                    <span class="glyphicon glyphicon-trash"></span>Excluir
                </a>
            </div>
        </div>
        <?php echo $this->Form->Close(); ?>
        <div class="col-md-5">
            <button id="btn-visualiza-tela-cheia" class="btn btn-info pull-right" onclick="visualizaDocumento(); return false;">Visualizar em tela cheia</button>
        </div>
    </div>
    <div class="row">
        <div id="tree" class="col-md-6"></div>
        <div class="col-md-6">
            <object id="preview-documento" data="" type="application/pdf"></object>
        </div>
    </div>
</div>
    <div id="modal" style="display: none;" title="Visualização Externa SEI">
    <table id="tabela-historico" class="table">
        <thead>
            <tr>
                <th>Data</th>
                <th>Unidade</th>
                <th>Descrição</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div id="modal-visualiza-documento" style="display: none;" title="Visualiza Documento">
    <object id="visualiza-documento" data="" type="application/pdf"></object>
</div>
<?php
    echo $this->Html->script('../bower_components/bootstrap-treeview/dist/bootstrap-treeview.min');
    echo $this->Html->script('sei/ListaProcessos');
?>
<script>
    $(document).ready(function() {
        carregaArvore(<?php echo $this->Sei->formataProcessosDoSeiParaJsonDaTreeView($processosDoSei); ?>);
    });
</script>