<?php echo $this->Form->create('Servico'); ?>

    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
        <div class="row-fluid">

            <div class="span12 ">
            <div class="widget-header widget-header-small"><h4><?php __('Descrição do Serviço'); ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                            <?php
                            echo $this->Form->input('co_servico');
                            echo $this->Form->input('ds_servico', array('type'=>'textarea','class' => 'input-xlarge', 'label' => 'Descrição (Número máximo de caracteres: 255)', 'maxLength' => '255'));
                            ?>
              </div>
            </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>

