<?php $usuario = $this->Session->read ('usuario'); ?>

	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Descrição de Serviço'); ?></h1></div>
      <?php 
          if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'servicos/add') ) { ?>        
		<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"></p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'servicos', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Novo Usuário"><i class="icon-plus icon-white"></i> Nova Descrição de Serviço</a> 
          </div>
        </div>
      <?php } ?>
<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
	<tr>
		<th><?php echo $this->Paginator->sort('Código', 'co_servico');?></th>
		<th><?php echo $this->Paginator->sort('Descrição', 'ds_servico');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($servicos as $servico):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $servico['Servico']['co_servico']; ?>&nbsp;</td>
		<td><?php echo ($servico['Servico']['ds_servico']); ?>&nbsp;</td>
		<td class="actions">
			<div class="btn-group acoes">	
				<?php echo $this->element( 'actions', array( 'id' => $servico['Servico']['co_servico'], 'class' => 'btn', 'local_acao' => 'servicos/index' ) ) ?>
			</div>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>



