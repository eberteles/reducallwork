<?php echo $this->Form->create('Setor', array('url' => array('controller' => 'setores', 'action' => 'add', $modal))); ?>
<?php echo $this->Html->script('jquery.blockUI'); ?>
<script>

    function checkNome(event)
    {

            $.blockUI();
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'verificaSetor')); ?>",
                type: "post",
                data: {nome: $(event).val()},
                success: function (response) {
                    if (response !== "false") {
                        $("input").prop('disabled', true);
                        $(event).prop('disabled',false);

                        $('#btnPesqAvan').prop('disabled',false);
                        response = $.parseJSON(response);
                        $('#cpfResponsavel').val(response['Setor']['nu_cpf']);
                        $('#SetorNoResponsavel').val(response['Setor']['no_responsavel']);
                        $('#SetorDsEmail').val(response['Setor']['ds_email']);
                        $('#Telefone').val(response['Setor']['nu_telefone']);
                        $('.generated-text').remove();
                        console.log(response['Setor']['ic_ativo']);
                        if(response['Setor']['ic_ativo'] === '0')
                        {
                            $('.required').append('<div class="error-message generated-text">Unidade administrativa já cadastrada, porém, bloqueada. Esta unidade administrativa será reativada </div>');
                            $('.btn-primary').prop('disabled', false);
                        }
                        else
                        {
                            $('.required').append('<div class="error-message generated-text">Unidade administrativa já cadastrada. </div>');
                            $('.btn-primary').prop('disabled', true);
                            $('<div id="confirmBox"></div>').appendTo('body')
                            .html('<div><h6>Unidade administrativa já cadastrada, deseja editar "' + response.Setor.ds_setor + '"?</h6></div>')
                            .dialog({
                                modal: true,
                                title: 'Confirme Sim ou Não',
                                zIndex: 10000,
                                autoOpen: true,
                                width: 'auto',
                                resizable: false,
                                buttons: {
                                    "Sim": function () {
                                        $(this).dialog("close");
                                        window.location.href = "/setores/edit/" + response.Setor.co_setor;
                                    },
                                    "Não": function () {
                                        $(this).dialog("close");
                                        $("#confirmBox").remove();
                                    }
                                },
                                close: function (event, ui) {
                                    $(this).remove();
                                }
                            });
                        }

                    }
                    else {
                        $('.generated-text').remove();
                        $('input').prop('disabled', false);
                        $('.btn-primary').prop('disabled', false);
                    }
                    $.unblockUI();
                },
                error: function(response){
                    $.unblockUI();
                }
            });


    }
</script>

    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
        <div class="row-fluid">
                <div class="widget-header widget-header-small"><h4><?php __('Unidade Administrativa'); ?></h4></div>
                    <div class="widget-body">
                      <div class="widget-main">
                        <?php
                            echo $this->Form->hidden('co_setor');
                            echo $this->Form->input('parent_id', array('class' => 'input-xlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Vinculado a', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false));
                            echo $this->Form->input('ds_setor', array('class' => 'input-xlarge', 'label' => 'Descrição','onblur'=>'checkNome(this);'));

                            echo '<div class="controls-row">';
                            echo '<div class="span12">';
                            if (!isset($iframeFromSetor)) {
                                echo $this->Form->input('co_usuario', array(
                                    'id' => 'coUsuarioSetor',
                                    'class' => 'input-xlarge chosen-select',
                                    'type' => 'select',
                                    'empty' => 'Selecione...',
                                    'label' => 'Responsável',
                                    'options' => $usuarios,
                                    'after' => $this->Print->getBtnEditCombo('Novo Grupo Auxiliar', null, '#modalTipoUsuario', true)
                                ));
                                echo '<div class="alert messageValidationUsuario" style="display:none;width:590px"><p class="text-warning"><strong>O usuário selecionado já esta vinculado em uma unidade administrativa. Ao continuar, o usuário será vinculado à uma nova unidade administrativa!</strong></p></div>';
                            }

                        echo '</div></div>';

                        echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail','maxLength' => '255'));


                        echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'id' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
                        ?>
                    </div>
                </div>

                        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button id="salvarSetor" rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <a rel="tooltip" href="<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'index')); ?>" title="Voltar" class="btn btn-small"> Voltar</a>

            </div>
        </div>
    </div>

<!-- modal selecionar novo tipo de cadastro de usuario -->
<div id="modalTipoUsuario" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Selecione o tipo de Usuário</h3>
    </div>
    <div class="modal-body-iframe">
        <div style="text-align:center">
            <button id="btnNovoAuxiliar" class="btn btn-primary btn-small"><i class="icon-plus"></i>Novo Grupo Auxiliar</button>
            <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'add')); ?>" class="btn btn-primary btn-small"><i class="icon-plus"></i>Novo Usuário</a>
        </div>
    </div>
    <div class="modal-footer">
        <!--<button id="btnEditar" coAtividade="" class="btn btn-small btn-primary"><i class="icon-edit"></i>Editar</button>-->
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<!-- modal novo grupo auxiliar -->
<div id="modalNovoAuxiliar" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Novo Grupo Auxiliar</h3>
    </div>
    <div class="modal-body-iframe" id="add_auxiliar">
    </div>
    <div class="modal-footer">
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<script type="text/javascript">
function verificaCPF() {
    // 822 656 296 18
    var cpf = $("#cpfResponsavel").val();
    cpf = cpf.replace('.','').replace('.','').replace('-','');
    $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function(data) {
        document.getElementById('isValidMessage').style.color = data.color;
        document.getElementById('isValidMessage').style.fontWeight = data.bold;
        $("#isValidMessage").html(data.message).show();

        if (data.validation == 'true') {
            $('#cpfResponsavel').addClass('valid');
        } else {
            $('#cpfResponsavel').removeClass('valid');
        }
    }).done(function(){
        // console.log("Second success");
    });
}

$(document).ready(function () {
    $('#salvarSetor').on('click', function(e) {
        if ($('#isValidMessage').is(':visible') && !$("#cpfResponsavel").hasClass('valid')) {
            e.preventDefault();
            return;
        } else {

        }
    });

    // validação de usuário já cadastrado em um setor
    $('#coUsuarioSetor').on('change', function (e) {
        var coUsuario = $(this).val();
        $.ajax({
            type: 'GET',
            url: '<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'checkusuario')); ?>/' + coUsuario,
            dataType: 'json',
            success: function (res) {
                if (res.valid) {
                    $('.messageValidationUsuario').hide();
                } else {
                    $('.messageValidationUsuario').show();
                }
            },
            error: function(err) {
                $('.messageValidationUsuario').hide();
            }
        })
    });

    // modal grupo auxiliar
    $("#btnNovoAuxiliar").on('click', function(e) {
        e.preventDefault();
        $('#modalTipoUsuario').modal('hide');
        $('#modalNovoAuxiliar').modal('show');


        var url_md = "<?php echo $this->base; ?>/grupo_auxiliares/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
            },
            success:function(result){
                $('#add_auxiliar').html(result);
            }
        })
    });
});
</script>



