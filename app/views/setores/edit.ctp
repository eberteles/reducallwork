<?php echo $this->Form->create('Setor', array('url' => array('controller' => 'setores', 'action' => 'edit'), 'onsubmit' => "return confirm('Ao editar essa Unidade Administrativa/Setor, você alterará todos os contratos e outros registros que possuem essa Unidade Administrativa/Setor atrelado. Deseja prosseguir com essa alteração? (Sua alteração ficará registrada em log de alterações de Unidades Administrativas/Setores)');")); ?>

    <div class="row-fluid">

<b>Campos com * são obrigatórios.</b><br>
		<div class="row-fluid">
			<div class="widget-header widget-header-small"><h4><?php __('Unidade Administrativa'); ?></h4></div>
				<div class="widget-body">
					<div class="widget-main">
						<?php
							echo $this->Form->hidden('co_setor', array('id' => 'coSetor'));
                            echo $this->Form->input('parent_id', array('id' => 'parentId', 'class' => 'input-xxlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Vinculado a', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false));
                            echo $this->Form->input('ds_setor', array('class' => 'input-xlarge', 'label' => 'Nome','onblur'=>'checkNome(this);')); ?>
						<label for="coUsuarioSetor">Responsável</label>
						<select id="coUsuarioSetor" name="data[Setor][co_usuario]" class="input-xlarge chosen-select" />
						<option >Selecione...</option>
						<?php foreach ($usuarios as $coUsuario => $nome):
							$selected = $this->data['Usuario']['co_usuario'] == $coUsuario ? true : false;
							if ($selected):
								echo "<option selected=\"selected\" value=\"{$coUsuario}\">{$nome}</option>";
							else:
								echo "<option value=\"{$coUsuario}\">{$nome}</option>";
							endif;
						?>
						<?php endforeach; ?>
						</select>
					<?php
						echo '<div class="alert messageValidationUsuario" style="display:none;width:590px"><p class="text-warning"><strong>O usuário selecionado já esta vinculado em uma unidade administrativa. Ao continuar, o usuário será vinculado à uma nova unidade administrativa!</strong></p></div>';
						echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail', 'id' => 'Email', 'maxLength' => '255'));
						echo $this->Form->input('nu_telefone', array('class' => 'input-xlarge', 'label' => 'Telefone', 'id' => 'Telefone', 'mask' => '(99) 9999-9999?9'));
					?>
				</div>
			</div>
		</div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
				<button rel="tooltip" type="button" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small btn-reset" id="Limpar"> Limpar</button>
            	<a rel="tooltip"  href="<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'index')); ?>" title="Voltar" class="btn btn-small btn-voltar"> Voltar</a>
            </div>
        </div>
    </div>


<!-- modal selecionar novo tipo de cadastro de usuario -->
<div id="modalTipoUsuario" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Selecione o tipo de Usuário</h3>
    </div>
    <div class="modal-body-iframe">
        <div style="text-align:center">
            <button id="btnNovoAuxiliar" class="btn btn-primary btn-small"><i class="icon-plus"></i>Novo Grupo Auxiliar</button>
            <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'add')); ?>" class="btn btn-primary btn-small"><i class="icon-plus"></i>Novo Usuário</a>
        </div>
    </div>
    <div class="modal-footer">
        <!--<button id="btnEditar" coAtividade="" class="btn btn-small btn-primary"><i class="icon-edit"></i>Editar</button>-->
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>

<!-- modal novo grupo auxiliar -->
<div id="modalNovoAuxiliar" class="modal hide fade maior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Novo Grupo Auxiliar</h3>
    </div>
    <div class="modal-body-iframe" id="add_auxiliar">
    </div>
    <div class="modal-footer">
        <button class="btn btn-small" data-dismiss="modal"><i class="icon-remove"></i> Fechar</button>
    </div>
</div>
<?php echo $this->Html->script('jquery.blockUI'); ?>
<script type="text/javascript">
function checkNome(event) {
	if(current_project !== $(event).val()) {
            $("input").prop('disabled', true);
            $(event).prop('disabled',false);
            $('.btn-primary').prop('disabled', true);
            $('#btnPesqAvan').prop('disabled',false);

            $.blockUI();

            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'verificaSetor')); ?>",
                type: "post",
                data: {nome: $(event).val()},
                success: function (response) {
    				if (response !== "false") {
    					response = $.parseJSON(response);
    					$('#Email').val(response.ds_email);
    					$('#Telefone').val(response.nu_telefone);
    					$('.generated-text').remove();

    					if(response.ic_ativo === '0') {
    						$('.required').append('<div class="error-message generated-text">Unidade administrativa já cadastrada, porém, bloqueada. Esta unidade administrativa será reativada </div>');
    					} else {
    						$('.required').append('<div class="error-message generated-text">Unidade administrativa já cadastrada. </div>');
    					}
    				} else {
    					$('.generated-text').remove();
    					$('input').prop('disabled', false);
    					$('.btn-primary').prop('disabled', false);
    					$('#Email').val(current_email);
    					$('#Telefone').val(current_telefone);
    				}
                    $.unblockUI();
                },
                error: function(response){
                    $.unblockUI();
                }
	        });
	} else {
		$('.generated-text').remove();
		$('input').prop('disabled', false);
		$('.btn-primary').prop('disabled', false);
		$('#Email').val(current_email);
		$('#Telefone').val(current_telefone);
		$('.btn-primary').prop('disabled', false);
	}
}
</script>
<script>
var current_project = "";

$(document).ready(function () {
    current_project = $('#SetorDsSetor').val();
    current_email = $('#Email').val();
    current_telefone = $('#Telefone').val();
    // desalibitar o option de coSetor igual
    $('#parentId option[value=' + $('#coSetor').val()+ ']').addClass('disabled').prop('disabled', true).trigger("chosen:updated");

    // validação de usuário já cadastrado em um setor
    $('#coUsuarioSetor').on('change', function (e) {
        var coUsuario = $(this).val();
        $.ajax({
            type: 'GET',
            url: '<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'checkusuario')); ?>/' + coUsuario + '/' + $('#coSetor').val(),
            dataType: 'json',
            success: function (res) {
                if (res.valid) {
                    $('.messageValidationUsuario').hide();
                } else {
                    $('.messageValidationUsuario').show();
                }
            },
            error: function(err) {
                $('.messageValidationUsuario').hide();
                console.error(err);
            }
        })
    });

    // modal grupo auxiliar
    $("#btnNovoAuxiliar").on('click', function(e) {
        e.preventDefault();
        $('#modalTipoUsuario').modal('hide');
        $('#modalNovoAuxiliar').modal('show');


        var url_md = "<?php echo $this->base; ?>/grupo_auxiliares/iframe/";
        $.ajax({
            type:"POST",
            url:url_md,
            data:{
            },
            success:function(result){
                $('#add_auxiliar').html(result);
            }
        })
    });
});
</script>