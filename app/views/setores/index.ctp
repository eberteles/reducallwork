
	<div class="row-fluid">
            <div class="page-header position-relative"><h1><?php __('Unidades Administrativas'); ?></h1></div>
	<div class="acoes-formulario-top clearfix" >
            <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
          <div class="pull-right btn-group">
            <a href="<?php echo $this->Html->url(array('controller' => 'setores', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Nova Unidade'); ?>"><i class="icon-plus icon-white"></i> <?php __('Nova Unidade'); ?></a>
          </div>
        </div>
	<?php echo $this->Form->create('Setor', array('url' => array('controller' => 'setores', 'action' => 'index')));?>
	<div class="row-fluid">
    	<div class="span3">
            <div class="input text ">
                <label for="SetorDsSetor">Unidade Administrativa</label>
                <input name="data[Setor][ds_setor]" type="text" class="input-xlarge" maxlength="20" id="SetorDsSetor">
            </div>
    	</div>
	</div>
 	<div class="form-actions">
    	<div class="btn-group">
        	<button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar</button>
            <button rel="tooltip" type="button" id="Limpar" title="Limpar dados preenchidos" class="btn btn-small btn-reset" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="button" id="Voltar" title="Voltar" class="btn btn-small btn-voltar"> Voltar</button>
        </div>
    </div>	
	<?php echo $this->Form->end();?></div>
	<table cellpadding="0" cellspacing="0" style="background-color:white" class="table tree table-hover table-bordered table-striped" id="tbSetor">
		<tr>
			<th><?php __('Descrição');?></th>
			<th><?php __('E-mail');?></th>
			<th><?php __('Telefone');?></th>
				<th>Responsável</th>
			<th class="actions"><?php __('Ações');?></th>
		</tr>
		<?php		
			foreach ($setors as $setor):
				$imprimir->listaTabelaSetor($setor, '');
			endforeach; 
		?>
	</table>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('.tree').treegrid({
            'initialState': 'collapsed',
            'saveState': true,
            treeColumn: 0
        });
    });

</script>
