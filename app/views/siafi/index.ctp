<div class="relatorios index">
    <!-- h2>< ?php __('Relatórios');?></h2 -->

    <div class="row-fluid">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#importar-arquivos" data-toggle="tab">Importar arquivos</a></li>
            <li><a href="#arquivos" data-toggle="tab">Arquivos</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="importar-arquivos">
                <div class="page-header position-relative"><h1><?php __('Importar/Exportar arquivo SIAFI'); ?></h1></div>
                <div class="span4">
                    <?php
                        echo $this->Form->create('siafi', array('enctype' => 'multipart/form-data', 'action' => 'upload'));
                        echo $this->Form->file('arquivo', array('type'=>'file'));
                        echo $this->Form->input('layout', array('options' => $arquivos));
                        echo '<br/><button type="submit" class="btn btn-small btn-primary">Enviar</button>';
                        echo $this->Form->end(); 
                    ?>
                </div>
            </div>
            <div class="tab-pane" id="arquivos">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pre-empenho" data-toggle="tab">Pré-Empenho</a></li>
                    <li><a href="#empenho" data-toggle="tab">Empenho</a></li>
                    <li><a href="#liquidacao" data-toggle="tab">Liquidação</a></li>
                    <li><a href="#pagametno" data-toggle="tab"><?php echo __('Pagamento'); ?></a></li>
                    <li><a href="#ordemBancaria" data-toggle="tab">Ordem Bancária</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="pre-empenho">
                        <p>Aguardando o processamento do arquivo.</p>
                    </div>
                    <div class="tab-pane" id="liquidacao">
                        <p>Aguardando o processamento do arquivo.</p>
                    </div>
                    <div class="tab-pane" id="pagametno">
                        <p>Aguardando o processamento do arquivo.</p>
                    </div>
                    <div class="tab-pane" id="empenho">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?php __('Contrato'); ?></th>
                                    <th>Número</th>
                                    <th>Descrição</th>
                                    <th>Tipo</th>
                                    <th>Valor</th>
                                    <th>Data</th>
                                    <th>Fonte</th>
                                    <th>UF</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($empenhos as $empenho) {
                                echo "<tr>"."\n";
                                echo "  <td>".$empenho['Empenho']['co_contrato']."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['nu_empenho']."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['ds_empenho']."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['co_plano_interno']."</td>"."\n";
                                echo "  <td>".number_format ($empenho['Empenho']['vl_empenho'], 2, ',', '.')."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['dt_empenho']."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['ds_fonte_recurso']."</td>"."\n";
                                echo "  <td>".$empenho['Empenho']['uf_beneficiada']."</td>"."\n";
                                echo "</tr>"."\n";
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="tab-pane" id="ordemBancaria">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>it_da_transacao</th>
                                    <th>gr_ug_gestao_an_numero_obuq</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($ordembancarias as $ob) { ?>
                                    <tr>
                                        <td><?php echo $ob['OrdemBancaria']['co_ordem_bancaria']; ?></td>
                                        <td><?php echo $ob['OrdemBancaria']['it_da_transacao']; ?></td>
                                        <td><?php echo $ob['OrdemBancaria']['gr_ug_gestao_an_numero_obuq']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->scriptStart() ?>
        
    $('#siafiArquivo').ace_file_input({
            no_file:'Nenhum arquivo selecionado ...',
            btn_choose:'Selecionar',
            btn_change:'Alterar',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
    });
        
<?php echo $this->Html->scriptEnd() ?>