<script>

	$(document).ready(function(){
		$('#siafiArquivo').ace_file_input({
		    no_file:'Selecione um arquivo para enviar...',
		    btn_choose:'Selecionar',
		    btn_change:'Alterar',
		    droppable:false,
		    onchange:null,
		    thumbnail:false
		});

        $('.deletefile').click(function(){
            var file = this;
            var filenames = file.getAttribute('ref') + 'xoxo' + file.getAttribute('txt');
            deleteFile(filenames);
        })
	});

	function detalhaMovimentoSiafi(el) {
		var id = el.value;
		var topico = id.substring(11);
		el.selectedIndex = 0;
		$('#movimentoTopicoDiv').html('Carregando informações...');
		$('#movimentoDetalhesDiv').html('');
		
		$.ajax({
			url: '/siafi2/detalha/'+id,
		}).done(function(data) {
			$('#movimentoTopicoDiv').html('Movimento: '+id.substring(11));
			$('#movimentoDetalhesDiv').html(data);			
		});
		
	}

	function importAll(){
		$.ajax({
			url: '/siafi2/importAll',
		}).done(function(data) {
			if(data){
				location.reload()
			}
			//alert(data);
			//console.log(data);
		});
	}

	function importNE() {
		$.ajax({
			url: '/siafi2/importaMovimento/ne'
		}).done(function(data) {
			alert(data);
            // console.log(data);
            location.reload();
		});
	}

    function deleteFile(filename) {
        $.ajax({
            url: '/siafi2/deleteFile/' + filename
        }).done(function(data) {
//            alert(data);
            location.reload();
        });
    }

</script>

<style>
	.icon-search:HOVER {cursor:pointer;}
	TABLE TBODY TR TH { text-align: left; }
	.importarBtn:HOVER { cursor:pointer }
</style>

<div class="page-header position-relative"><h1>SIAFI</h1></div>

<div class="row-fluid">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#arquivos" data-toggle="tab"><i class="pink icon-folder-open bigger-110"></i>&nbsp; Arquivos</a></li>
		<li class=""><a href="#pesquisa" data-toggle="tab"><i class="icon-search nav-search-icon"></i>&nbsp; Pesquisa</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="arquivos">
		
			<div class="acoes-formulario-top clearfix">
				<p class="requiredLegend pull-left">Envio de arquivos SIAFI:</p>
			</div>			
		
			<div style="padding-left:50px;">
				<span style="color:blue">* É necessário enviar o arquivo de Layout (REF) juntamente com o arquivo de movimentos.</span></br>
				<span style="color:blue">* Envie primeiro o arquivo TXT e em seguida o REF, quando os dois estiverem no servidor aparecerão na lista para importação.</span></br>
				<span style="color:blue">* Caso os arquivos não apareçam na lista, verifique o formato e tente novamente.</span></br>
				<span style="color:blue">* Para remover um arquivo da lista de importação, clique no botão MENOS(-), essa opção só é valida para arquivos que ainda não foram importados!.</span>
				<br><br>
				<form enctype="multipart/form-data" id="siafiUploadForm" method="post" action="/siafi2/upload" accept-charset="utf-8"">
					<input type="hidden" name="_method" value="POST"/>
					<div style="width:400px;"><input type="file" name="data[siafi][arquivo]" type="file" id="siafiArquivo" /></div>
					<button type="submit" class="btn btn-small btn-primary">Enviar</button>
				</form>
			</div>

			<br>
			
			<div class="acoes-formulario-top clearfix">
				<p class="requiredLegend pull-left">Arquivos no sistema:</p>
				<div class="pull-right btn-group">
					<a id="siafi_import_all_btn" href="#myModal" role="button" class="btn btn-small btn-primary importarBtn" data-toggle="modal" onclick="importAll()">
						importar todos
					</a>
				</div>
			</div>			
			
			<table class="table table-hover table-bordered table-striped" style="margin:0px;" cellpadding="0" cellspacing="0">
			<tbody>
			    <tr>
				<th style="width:40px;text-align:left;">Id</th>
				<th style="width:300px;text-align:left;">Conteúdo</th>
				<th style="width:300px;text-align:left;">Layout</th>
				<th style="text-align:left;">Data Importação</th>
				<th style="width:120px;text-align:left;">&nbsp;</th> 
				</tr>
			
				<?php foreach ($arquivos as $k=>$v) { ?>
				<tr>
					<td><?=$v['id']?></td>
					<td><?=$k?></td>
					<td><?=$v['layout']?></td>
					<td><?=$v['data']?></td>
					<td>
						<?php if ($v['data'] == '') { ?>
							<?php if (strtolower(substr($k, -3)) != 'ref') { ?>
								<a class="btn btn-small btn-success deletefile" title="O arquivo ainda não foi importado, clique para deletar!" txt="<?php echo str_replace('.TXT', '', $k)?>" ref="<?php echo str_replace('.REF', '', $v['layout'])?>"><i class="icon-minus icon-white"></i></a>
								&nbsp;
							<?php } ?>
						<?php } else { ?>
							    <a class="btn btn-small btn-success" title="O arquivo já foi importado!"><i class="icon-ok icon-white"></i></a>
						<?php } ?>
					</td>
			    </tr>
			    <?php } ?>
			</tbody>
			</table>
		</div>
		<div class="tab-pane" id="pesquisa">
			<!--  
			Tipos de movimento disponíveis:
			<div style='height:10px;'></div>
			<select>
				<option value="">Selecione...</option>
				<?php foreach ($tabelas as $k=>$v) { ?>
					<option value='<?=$k?>'><?=$v?></option>
				<?php } ?>
			</select>
			 -->
				
			<div class="acoes-formulario-top clearfix">
				<p class="requiredLegend pull-left">Tipos de movimento:</p>
			</div>			
				
			<div class="row-fluid">
				<div class="span12 ">
					<div class="span6" style="width:500px;">
						<dl class="dl-horizontal">
							<?php foreach ($tudo as $tipo => $items) { ?>
								<dt style='width:300px;padding-left:30px;'>
									<i class="pink icon-folder-open bigger-110"></i> &nbsp; <?=$tipo?>:
									<?php if ($tipo == 'Empenho - NE') { ?>
                                        <a onclick="importNE()" style="cursor: pointer;"  href="#myModal2" role="button" data-toggle="modal" >(importar todos)</a>
									<?php } ?>
								</dt>
								<dd>
									<select style="width:130px;" onchange="detalhaMovimentoSiafi(this);">
										<option>Selecione:</option>
										<?php foreach ($items as $item) { ?>
											<option value="<?=$item?>"> &nbsp; <?=substr($item, 11)?></option>
										<?php } ?>
									</select>
								</dd>
							<?php } ?>
						</dl>
					</div>
					<div class="span7" style="margin-top:10px;">
						<div class="widget-header widget-header-small"><h4><font id='movimentoTopicoDiv'>Metadados do movimento</font></h4></div>
						<div class="widget-body">
							<div class="widget-main" id="movimentoDetalhesDiv">
								<br>
								&larr; Selecione um movimento
								<br><br><br>
								<br><br><br>
							</div>
						</div>
					</div>
				</div>
			</div>		
							
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 200px" data-backdrop="static" data-keyboard="false">
	<div class="modal-body">
		Importando os dados do <b>SIAFI</b>, por favor aguarde, isso pode demorar alguns minutos...
	</div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 200px" data-backdrop="static" data-keyboard="false">
    <div class="modal-body">
        Importando dados do <b>SIAFI</b> para o Gescon. Isso pode demorar alguns minutos..
    </div>
</div>
