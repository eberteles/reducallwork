<script>
    $(document).ready(function () {
        $("#ano").change(function () {
            alert($('#ano').val());
        });
    });

    var contratos = [<?=$contratos?>];
    var empenhos = [<?=$empenhos?>];
    var countEmpenhos = [<?=$countEmpenhos?>];
    //    var aditivos = <?//=$aditivos?>//; TODO: por algum motivo, quando sem contratos, essa variável 'buga' o index, aparentemente não está sendo usada, verificar o impacto do sistema ficar sem ela ou arruma-la.. se for o caso
    var cObj = null;

    function replaceAll(find, replace, str) {
        while (str.indexOf(find) > -1)
            str = str.replace(find, replace);
        return str;
    }

    function createHtml(xmp, obj) {
        html = $('#' + xmp).html();
        tmpTag = null;
        for (x in obj) {
            tmpTag = '%' + x + '%';
            if (html.indexOf(tmpTag) >= 0)
                html = replaceAll(tmpTag, obj[x], html);
        }
        return html;
    }

    function redirectPost(location, args) {
        var form = '';
        $.each(args, function (key, value) {
            form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form id="formUasg" action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
    }

    function importContrato(id) {
        c = null;
        for (a in cObj) {
            if (cObj[a]['numero_processo'] == id.replace('-', '\/')) {
                c = cObj[a];
                break;
            }
        }

       $("#output").html("<img src='<?php echo $this->base; ?>/img/loading.gif' style='height'> Importando. Por favor, aguarde...");
        $.post("<?php echo $this->Html->url(array('controller' => 'siasg', 'action' => 'importar' )); ?>", {
            json: '[' + JSON.stringify(cObj[a]) + ']'
        }).done(function (data) {
            $("#output").html(data);
            location.reload();
        });
    }

    function importarTudo() {
        $("#output").html("<img src='<?php echo $this->base; ?>/img/loading.gif' style='height'> Importando. Por favor, aguarde...");
        $.post('<?php echo $this->Html->url(array('controller' => 'siasg', 'action' => 'importar')); ?>', {
            json: JSON.stringify(cObj)
        }).done(function (data) {
            $("#output").html(data);
            location.reload();
        });
    }

    function atualizarSiasg(ano, uasg) {

        data = new Date();
        retroativo = data.getFullYear() - ano;
        $.get('<?php echo $this->Html->url(array('controller' => 'siasg', 'action' => 'atualizarRobo')); ?>/' + uasg + '/' + retroativo, {}).done(function (data) {

            if (data.substring(0, 3) != '<b>') {
                alert("A UASG " + uasg + " não está cadastrada no ComprasNet.");
                location.reload();
            }

            $('.modal').hide();
            $('.modal-backdrop').hide();
            $('#atualizaRegistros').hide();

            div = document.getElementById('output');
            div.insertAdjacentHTML('afterbegin', 'Registros Atualizados com Sucesso!<br/>');
        });
    }

    function fixDate(d) {
        return d.substring(8, 10) + '/' + d.substring(5, 7) + '/' + d.substring(0, 4);
    }

    function pesquisaUasg() {
        var ano = $('#ano').val();
        var uasg_selecionada = $('#uasg_selecionada').val();

        var st = new Date().getTime();
        $('#uasg')[0].disabled = 'disabled';
        $('#searchIcon').hide();
        $("#output").html("<img src='<?php echo $this->base; ?>/img/loading.gif' style='height'> Carregando. Por favor, aguarde...");
        $('#resDiv').hide();
        var cC = 0;
        var cE = 0;
        var cA = 0;
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller' => 'siasg', 'action' => 'pesquisa')); ?>/' + uasg_selecionada + '_' + ano
        }).done(function (data) {
            if (data.indexOf("null") > -1) {
                $("#output").html(
                    "<p>" +
                    "  Ano para visualização: " +
                    "<select id='uasg_selecionada' style='margin-top:10px'>" +
                    "<option value=''>Selecione...</option>" +
                    "<option value='2017'>2017</option>" +
                    "<option value='2016'>2016</option>" +
                    "<option value='2015'>2015</option>" +
                    "<option value='2014'>2014</option>" +
                    "<option value='2013'>2013</option>" +
                    "<option value='2012'>2012</option>" +
                    "<option value='2011'>2011</option>" +
                    "<option value='2010'>2010</option>" +
                    "<option value='2009'>2009</option>" +
                    "<option value='2008'>2008</option>" +
                    "<option value='2007'>2007</option>" +
                    "<option value='2006'>2006</option>" +
                    "<option value='2005'>2005</option>" +
                    "<option value='2004'>2004</option>" +
                    "<option value='2003'>2003</option>" +
                    "<option value='2002'>2002</option>" +
                    "<option value='2001'>2001</option>" +
                    "<option value='2000'>2000</option>" +
                    "<option value='1999'>1999</option>" +
                    "<option value='1998'>1998</option>" +
                    "<option value='1997'>1997</option>" +
                    "<option value='1996'>1996</option>" +
                    "<option value='1995'>1995</option>" +
                    "</select>    " +
                    " UASG: " +
                    "<select id='ano' style='margin-top:10px'>" +
                    "<option value=''>Selecione...</option>" +
                    <?php foreach($uasgs as $uasg) { ?>
                    "<option value='<?php echo $uasg['Uasg']['uasg']; ?>'><?php echo $uasg['Uasg']['uasg']?></option>" +
                    <?php } ?>
                    "</select>    " +
                    "<a role='button' class='btn btn-small btn-primary importarBtnAno' onclick='pesquisaUasg()'>" +
                    "Pesquisar" +
                    "</a>    ");
                $('#uasg')[0].disabled = '';
                $('#searchIcon').show();
                if (ano != undefined) {
                    var atualizaRegistros = "<div id='atualizaRegistros'>O ano <b>" + uasg_selecionada + "</b> e a uasg <b>" + ano + "</b> selecionados não possuem registros..." +
                        "<a href='#myModal2' data-toggle='modal' role='button' class='btn btn-small btn-primary importarBtn' onclick='atualizarSiasg(" + uasg_selecionada + "," + ano + ")'>" +
                        "Atualizar registros para ANO:" + uasg_selecionada + " / UASG:" + ano +
                        "</a></div>";
                    div = document.getElementById('output');
                    div.insertAdjacentHTML('afterbegin', atualizaRegistros);
                }
                return false;
            }

            cObj = JSON && JSON.parse(data) || $.parseJSON(data);
            cObj = cObj[0];

            var html = '';
            for (a in cObj) {
                cC++;
                _isContratoNovo = (contratos.indexOf(cObj[a]['modalidade_numero']) < 0) ? true : false;
                _hasUpdate = false;

                window.rep = cObj[a];

                id = cObj[a]['numero_processo'].replace(/\//g, '-');

                cObj[a]['data_assinatura'] = fixDate(cObj[a]['data_assinatura']);
                cObj[a]['vigencia_de'] = fixDate(cObj[a]['vigencia_de']);
                cObj[a]['vigencia_ate'] = fixDate(cObj[a]['vigencia_ate']);
                cObj[a]['id'] = id;
                cObj[a]['htmlEmpenhos'] = '';
                cObj[a]['htmlAditivos'] = '';
                cObj[a]['btnEdit'] = _isContratoNovo ? createHtml('xmpBtnEdit', {onclick:"importContrato('"+id+"')",label:'Importar'}) : '';

                if (cObj[a]['aditivos']) {
                    cA++;
                    tmpHtml = '';
                    for (b in cObj[a]['aditivos'])
                        tmpHtml += createHtml('xmpAditivoItem', cObj[a]['aditivos'][b]);
                    cObj[a]['htmlAditivos'] = createHtml('xmpAditivoHeader', {aditivos_html: tmpHtml, id: id});
                }

                cObj[a]['flags'] = '';
                cObj[a]['flags'] += _isContratoNovo ?
                    '<i class="orange icon-exclamation-sign bigger-110" style="font-size:12px;"> &nbsp;não cadastrado</i> &nbsp; ' : '';

                cObj[a]['flags'] += _hasUpdate ?
                    '<i class="green icon-exclamation-sign bigger-110" style="font-size:12px;"> &nbsp;contém atualizações</i> &nbsp; ' : '';

                html += createHtml('xmpContrato', cObj[a]);
            }

            $('#resDiv').html(html);
            $('#resDiv').show();
            $(".accordion-toggle").click();
            $("#output").html(
                "Foram encontrados <b>" + cC + "</b> contratos  " +
                "em <b>" + (new Date().getTime() - st) / 1000 + "</b> segundos. " +
                "<a href='#myModal' role='button' class='btn btn-small btn-primary importarBtn' data-toggle='modal' onclick='importarTudo()'>" +
                "Importar Todos" +
                "</a>" +
                "<p>" +
                "  Ano para visualização: " +
                "<select id='uasg_selecionada' style='margin-top:10px'>" +
                "<option value=''>Selecione...</option>" +
                "<option value='2017'>2017</option>" +
                "<option value='2016'>2016</option>" +
                "<option value='2015'>2015</option>" +
                "<option value='2014'>2014</option>" +
                "<option value='2013'>2013</option>" +
                "<option value='2012'>2012</option>" +
                "<option value='2011'>2011</option>" +
                "<option value='2010'>2010</option>" +
                "<option value='2009'>2009</option>" +
                "<option value='2008'>2008</option>" +
                "<option value='2007'>2007</option>" +
                "<option value='2006'>2006</option>" +
                "<option value='2005'>2005</option>" +
                "<option value='2004'>2004</option>" +
                "<option value='2003'>2003</option>" +
                "<option value='2002'>2002</option>" +
                "<option value='2001'>2001</option>" +
                "<option value='2000'>2000</option>" +
                "<option value='1999'>1999</option>" +
                "<option value='1998'>1998</option>" +
                "<option value='1997'>1997</option>" +
                "<option value='1996'>1996</option>" +
                "<option value='1995'>1995</option>" +
                "</select>    " +
                " UASG: " +
                "<select id='ano' style='margin-top:10px'>" +
                "<option value=''>Selecione...</option>" +
                <?php foreach($uasgs as $uasg) { ?>
                "<option value='<?php echo $uasg['Uasg']['uasg']; ?>'><?php echo $uasg['Uasg']['uasg']?></option>" +
                <?php } ?>
                "</select>    " +
                "<a role='button' class='btn btn-small btn-primary importarBtnAno' onclick='pesquisaUasg()'>" +
                "Pesquisar" +
                "</a>    "
            );
            $('#uasg')[0].disabled = '';
            $('#searchIcon').show();
        });
    }
</script>

<style>
    .icon-search:HOVER {
        cursor: pointer;
    }

    .siasg TBODY TR TH {
        text-align: left;
    }
</style>

<div id="container"></div>

<div class="page-header position-relative row-fluid">
    <div class="span10">
        <h1>
            ComprasNet
        </h1>
    </div>
    <div class="span2">
        <a href='<?php echo $this->Html->url(array('controller' => 'uasg', 'action' => 'index')); ?>' role='button' class='btn btn-small btn-primary gerenciarUasg'>
            gerenciar UASGs
        </a>
    </div>
</div>

<div class="row-fluid">
    <div class="widget-box">
        <div class="widget-header widget-header-small"><h4>Consulta ao sistema</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php

                if (Configure::read('App.config.resource.available.siasg')) {
                    ?>
                <input type="hidden" id="uasg" value="<?= Configure::read('App.config.resource.uasg'); ?>"/>
                    <span id="output" style="color:#1b6094;padding-left:3px;"><img
                                src='<?php echo $this->base; ?>/img/loading.gif' style='height'> Carregando. Por favor, aguarde...</span>
                    <script>
                        $(document).ready(function () {
                            pesquisaUasg();
                        });
                    </script>
                <?php
                } else {
                ?>
                    Código UASG não cadastrado.<br>
                    Para efetuar pesquisas na base de dados do SIASG será necessário solicitar acesso a esta funcionalidade ao administrador.
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<xmp id="xmpBtnEdit" style="display:none">
    <a onclick="%onclick%" data-toggle="modal" class="btn btn-small btn-primary"><i class="icon-pencil icon-white"></i>
        %label%</a>
</xmp>

<xmp id="xmpBtnEmpenho" style="display:none">
    <a onclick="editEmpenho('%id%')" class="btn btn-small btn-success" title="Importar"><i class="icon-plus icon-white"
                                                                                           style="display:none"></i></a>
</xmp>

<xmp id="xmpContrato" style="display:none">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a id="title_%id%" href="#collapseIdent_%modalidade%%id%" data-parent="#accordion1" data-toggle="collapse"
               class="accordion-toggle"><i class="pink icon-folder-open bigger-110"></i> &nbsp; Contrato:
                %modalidade_numero% &nbsp; - &nbsp; %contratado% &nbsp; &nbsp; %flags%</a>
        </div>

        <div class="accordion-body in collapse" id="collapseIdent_%modalidade%%id%">
            <div class="accordion-inner">

                <div class="acoes-formulario-top clearfix">
                    <p class="requiredLegend pull-left">Detalhes do contrato:</p>
                    <div class="pull-right btn-group">
                        %btnEdit%
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <dt>Processo:</dt>
                                <dd>%numero_processo%</dd>
                                <dt>Fornecedor:</dt>
                                <dd>%contratado%</dd>
                                <dt>CPF / CNPJ:</dt>
                                <dd>%contratado_cpf_cnpj%</dd>
                                <dt>Modalidade de Contratação:</dt>
                                <dd>%licitacao_modalidade%</dd>
                                <dt>Modalidade SIASG:</dt>
                                <dd>%modalidade%</dd>
                                <dt>Nº Licitação:</dt>
                                <dd>%licitacao_numero%</dd>
                                <dt>Data de Publicação:</dt>
                                <dd>%data_publicacao%</dd>
                                <dt>Data da assinatura:</dt>
                                <dd>%data_assinatura%</dd>
                                <dt>Vigência:</dt>
                                <dd>%vigencia_de% até %vigencia_ate%</dd>
                            </dl>
                        </div>
                        <div class="span6">
                            <dl class="dl-horizontal">
                                <dt>Objeto:</dt>
                                <dd>%objeto%</dd>
                                <dt>Fundamento Legal:</dt>
                                <dd>%fundamento_legal%</dd>
                                <dt>Valor:</dt>
                                <dd>R$ %valor_total%</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    %htmlAditivos%
                </div>
            </div>
        </div>
    </div>
</xmp>

<xmp id="xmpContrato_2" style="display:none">
    <table class="table table-hover table-bordered table-striped" style="margin:0px" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td colspan="11" style="font-size:14px;"><b>Contrato: %modalidade_numero%</b> - %contratado% - <b>CPF /
                    CNPJ:</b> %contratado_cpf_cnpj%
            </td>
            <td class="actions" style="width:80px;">
                <center>
                    <a onclick="detalhaContrato('%id%')" class="v_impressao btn btn-small btn-info" id="362" href="#"
                       data-toggle="modal"><i class="icon-print"></i></a><a onclick=""
                                                                            class="btn btn-small btn-success"><i
                                class="icon-pencil"></i></a>
                </center>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table table-hover table-bordered table-striped siasg" style="margin:0px;border-top:0px;"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <th>Processo</th>
            <th>Licitação</th>
            <th>Publicação</th>
            <th>Objeto</th>
            <th>Fundamento Legal</th>
            <th style="width:160px;">Vigência</th>
            <th style="width:80px;">Valor</th>
            <th style="width:80px;">Assinatura</th>
        </tr>
        <tr>
            <td>%numero_processo%</td>
            <td>%licitacao_numero%</td>
            <td>%data_publicacao%</td>
            <td>%objeto%</td>
            <td>%fundamento_legal%</td>
            <td>%vigencia_de% até %vigencia_ate%</td>
            <td>R$ %valor_total%</td>
            <td>%data_assinatura%</td>
        </tr>
        </tbody>
    </table>
</xmp>

<xmp id="xmpEmpenhoHeader" style="display:none">
    <table id="empenhosTableItems_%id%" class="table table-hover table-bordered table-striped" style="margin:0px;"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <th style="width:120px;text-align:left;">Empenho</th>
            <th style="width:80px;text-align:left;">Número Ug</th>
            <th style="text-align:left;">Ug</th>
            <th style="width:80px;text-align:left;">Gestao</th>
            <th style="width:80px;text-align:left;">Programa</th>
            <th style="width:40px;text-align:left;"></th>
        </tr>
        %empenhos_html%
        </tbody>
    </table>
</xmp>

<xmp id="xmpEmpenhoItem" style="display:none">
    <tr>
        <td>%empenho%</td>
        <td>%ug_numero%</td>
        <td>%ug_nome%</td>
        <td>0000%gestao%</td>
        <td>%programa%</td>
        <td>%btnEdit%</td>
    </tr>
</xmp>

<xmp id="xmpAditivoHeader" style="display:none;">
    <table id="aditivosTableItems_%id%" class="table table-hover table-bordered table-striped" style="margin:0px;"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <th style="width:80px;text-align:left;">Termo Aditivo</th>
            <th style="width:80px;text-align:left;">Data de Publicação</th>
            <th style="width:160px;text-align:left;">Fundamento Legal</th>
            <th style="text-align:left;">Objeto</th>
            <th style="width:160px;text-align:left;">Vigência</th>
            <th style="width:100px;text-align:left;">Valor</th>
            <th style="width:80px;text-align:left;">Assinatura</th>
            <th style="width:1px;text-align:left;"></th>
        </tr>
        %aditivos_html%
        </tbody>
    </table>
</xmp>

<xmp id="xmpAditivoItem" style="display:none">
    <tr>
        <td>%termo_aditivo%</td>
        <td>%data_publicacao%</td>
        <td>%fundamento_legal%</td>
        <td>%objeto%</td>
        <td>%vigencia_de% até %vigencia_ate%</td>
        <td>R$ %valor_total%</td>
        <td>%data_assinatura%</td>
        <td><a class="btn btn-small btn-success" title="Editar"><i class="icon-pencil"></i></a></td>
    </tr>
</xmp>

<div id="resDiv"></div>

<script>
    $(document).ready(function () {
        $.fn.enterKey = function (fnc) {
            return this.each(function () {
                $(this).keypress(function (ev) {
                    var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };
        $("#uasg").enterKey(function () {
            pesquisaUasg();
        });
        $("#searchIcon").bind('click', function (e) {
            pesquisaUasg();
        });
    });
</script>

<!-- Modal -->
<div id="myModal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="margin-top: 200px" data-backdrop="static" data-keyboard="false">
    <div class="modal-body">
        Importando os dados do <b>ComprasNet</b>, por favor aguarde, isso pode demorar alguns minutos...
    </div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="margin-top: 200px" data-backdrop="static" data-keyboard="false">
    <div class="modal-body">
        Atualizando registros à importar do <b>ComprasNet</b>, por favor aguarde...
    </div>
</div>
