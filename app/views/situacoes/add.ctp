<?php echo $this->Form->create('Situacao', array('url' => array('controller' => 'situacoes', 'action' => 'add', $modal))); ?>

    <div class="row-fluid">
<b>Campos com * são obrigatórios.</b><br>

        <div class="row-fluid">
            <div class="widget-header widget-header-small"><h4><?php
            if($this->Modulo->isCamposContrato('co_situacao_processo')) : __('Situação do Processo / Contrato'); else : __('Situação do Contrato'); endif; ?></h4></div>
            <div class="widget-body">
              <div class="widget-main">
                           <?php
echo $this->Form->input('co_situacao');
if($this->Modulo->isCamposContrato('co_situacao_processo')) :
    echo $this->Form->input('tp_situacao', array('class' => 'input-xlarge', 'label' => '', 'options' => array('P' => 'Processo', 'C' => __('Contrato', true) )));
else :
    echo $this->Form->input('tp_situacao', array('class' => 'input-xlarge', 'label' => '', 'options' => array('C' => __('Contrato', true) )));
endif;
   if($this->Modulo->getSeqSituacoes()) {
       echo $this->Form->input('nu_sequencia',
       array('class' => 'input-xlarge',
       'label' => 'Sequência',
       'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57'));
   }
   echo $this->Form->input('ds_situacao', array('class' => 'input-xlarge', 'label' => 'Descrição'));

?>
              </div>
            </div>

        </div>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
                <button rel="tooltip" type="reset" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>


            </div>
        </div>
    </div>
