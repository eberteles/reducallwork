<?php $usuario = $this->Session->read ('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Situações do Processo / Contrato'); ?></h1></div>
    <?php 
    if( $this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'situacoes/add') ): ?>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left"></p>
        <div class="pull-right btn-group">
           <a href="<?php echo $this->Html->url(array('controller' => 'situacoes', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="<?php __('Nova Situação'); ?>"><i class="icon-plus icon-white"></i> <?php __('Nova Situação'); ?></a> 
        </div>
    </div>
    <?php endif; ?>
</div>



<div class="box-tabs-white" id="tabs">
    <ul class="nav nav-tabs">
        <?php if($this->Modulo->isCamposContrato('co_situacao_processo')) : ?>
        <li><?php echo $this->Html->link(__('Situações do Processo', true), array('action' => 'listaSituacoes', 'P')) ?></li>
        <?php endif; ?>
        <li><?php echo $this->Html->link(__('Situações do Contrato', true), array('action' => 'listaSituacoes', 'C')) ?></li>
    </ul>
</div>

<script>
$(document).ready(function(){
    $( "#tabs" ).tabs({
        beforeLoad: function( event, ui ) {
			ui.jqXHR.fail(function() {
				ui.panel.html("Não foi possível carregar o conteúdo, porfavor tente novamente.");
			});
		},
		load: function( event, ui){
			$('table th a').on('click', function() {
				$("a", ui.tab).attr('href', $(this).attr('href'));							
				$("#tabs").tabs("load", $("#tabs").tabs("option", "active"));
		        	return false;
		    	});		
		    	$('.pagination a').on('click', function(){
				var newUrl = $(this).attr('href');
				$("a", ui.tab).attr('href', newUrl);                                                      
                                $("#tabs").tabs("load", $("#tabs").tabs("option", "active"));
				return false;
			});
		}
	});
    setTimeout(function() {
        $('.alert').fadeOut('slow');
    }, 3000);   
});
</script>