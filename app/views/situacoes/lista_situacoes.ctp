<div id="situacoes">
    <div class="row-fluid">
        <table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped">
            <tr>
                <?php if($this->Modulo->getSeqSituacoes()){ ?>
                    <th><?php echo $this->Paginator->sort(__('Sequência', true), 'nu_sequencia');?></th>
                <?php } ?>
                <th><?php echo $this->Paginator->sort('Descrição', 'ds_situacao');?></th>
                <th class="actions"><?php __('Ações');?></th>
            </tr>
            <?php
            $i = 0;
            foreach ($situacoes as $situacao):
            $class = null;
            if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class;?>>
                <?php if($this->Modulo->getSeqSituacoes()){ ?>
                    <td><?php echo $situacao['Situacao']['nu_sequencia']; ?>&nbsp;</td>
                <?php } ?>
                <td><?php echo $situacao['Situacao']['ds_situacao']; ?>&nbsp;</td>
                <td class="actions">
                    <div class="btn-group">
                        <?php echo $this->element( 'actions2', array( 'id' => $situacao['Situacao']['co_situacao'], 'class' => 'btn', 'local_acao' => 'situacoes/index' ) ) ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
                <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
            </ul>
        </div>
    </div>
</div>