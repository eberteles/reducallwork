<?php echo $this->Form->create('Subcategoria', array('url' => array('controller' => 'subcategorias', 'action' => 'add', $modal))); ?>
<div class="row-fluid">

    <b>Campos com * são obrigatórios.</b><br>
    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4><?php __('Sub Categoria'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                echo $this->Form->hidden('co_subcategoria');
                echo $this->Form->input('co_categoria', array('class' => 'input-xxlarge chosen-select','type' => 'select', 'empty' => 'Selecione...', 'label'=>__('Categoria', true), 'options' => $categorias));                    
                echo $this->Form->input('no_subcategoria', array('id' => 'noSubcategoria', 'class' => 'input-xlarge', 'label' => 'Descrição'));
                echo '<div id="message-noSubcategoria" class="" style="display:none;width:285px">
                        <p class="error-message"><strong> A categoria já existe e será reativada</strong></p>
                    </div>';
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button> 
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
            <button rel="tooltip" type="reset" titlse="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
</div>

<script>
    $().ready(function () {
        $('#noSubcategoria').on('blur', function (e) {
            // console.log('aqui');

            if ($(this).val()) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '<?php echo $this->Html->url(array('controller' => 'subcategorias', 'action' => 'checar_nome')); ?>/' + $(this).val() + '/1',
                    success: function (res) {
                        if (res.status) {
                            $('#message-noSubcategoria').show();
                        } else {
                            $('#message-noSubcategoria').hide();
                        }
                    },
                    error: function (err) {
                        console.error(err);
                    } 
                });
            }
        });
    });
</script>


