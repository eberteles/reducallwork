<?php $usuario = $this->Session->read('usuario'); ?>

<div class="row-fluid">
    <div class="page-header position-relative"><h1><?php __('Sub Categorias'); ?></h1></div>
    <?php if ($this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'fornecedores/index')) { ?>
        <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--></p>
        <div class="acoes-formulario-top clearfix">
            <div class="pull-right btn-group">
                <a href="<?php echo $this->Html->url(array('controller' => 'subcategorias', 'action' => 'add')); ?>"
                   data-toggle="modal" class="btn btn-small btn-primary"
                   title="<?php echo __('Nova Sub Categoria'); ?>"><i
                            class="icon-plus icon-white"></i> <?php __('Nova Sub Categoria'); ?></a>
            </div>
        </div>
    <?php } ?>
    <table cellpadding="0" cellspacing="0" style="background-clip: white"
           class="table table-hover table-bordered table-striped">
        <tr>
            <th><?php echo __('Código'); ?></th>
            <th><?php echo __('Categoria'); ?></th>
            <th><?php echo __('Descrição'); ?></th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($subcategorias as $subcategoria) :
            $class = null;
            if ($i++ % 2 == 0) :
                $class = 'altrow';
            endif;
            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $subcategoria['Subcategoria']['co_subcategoria']; ?></td>
                <td><?php echo $categorias[$subcategoria['Subcategoria']['co_categoria']]; ?></td>
                <td><?php echo $subcategoria['Subcategoria']['no_subcategoria']; ?></td>
                <td class="actions">
                    <div class="btn-group acoes">
                        <?php echo $this->element('actions', array(
                                'id' => $subcategoria['Subcategoria']['co_subcategoria'],
                                'class' => 'btn',
                                'local_acao' => 'subcategorias/'
                            )
                        );  ?>
                    </div>
                </td>
            </tr>
            <?php
        endforeach;
        ?>
    </table>
    <p><?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
        ));
        ?></p>
    <div class="pagination">
        <ul>
            <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
        </ul>
    </div>
</div>
