<form id="TesteIndexForm" method="post" action="<?php echo $this->Html->url(array('controller' => 'teste', 'action' => 'index')); ?>" accept-charset="utf-8">

<?php
    echo "SMTP: " . $this->Modulo->getEmail('host') . ":" . $this->Modulo->getEmail('port');
    echo $this->Form->input('subject', array('label' => 'Assunto'));
    echo $this->Form->input('to', array('label' => 'Para'));
    echo $this->Form->input('mensagem', array('label' => 'Mensagem'));
?>

        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Enviar"> Enviar</button> 
            </div>
        </div>
    
</form>