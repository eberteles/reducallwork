<?
echo $this->Form->create('TipoLicitacao', array('url' => "/tipos_licitacoes/edit/$id"));

echo $this->Html->script( 'inicia-datetimepicker' );
echo $this->Html->script( 'date' );

?>

<div class="row-fluid">
<form>
    <div class="acoes-formulario-top clearfix" >
        <p class="requiredLegend pull-left">Preencha os campos abaixo para adicionar Tipo Licitação.</p>

        <div class="pull-right btn-group">
            <button rel="tooltip" type="reset" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>

    <div class="row-fluid">

        <div class="span12 ">
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h4>
                        <?php __('Tipo Licitação'); ?>
                    </h4>
                </div>


                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="span3">
                                <dl class="dl-horizontal">
                                    <?
                                    echo $this->Form->hidden('co_tipo_licitacao');
                                    //                             Tipo licitacão
                                    echo $this->Form->input('no_tipo_licitacao', array('class' => 'input-xlarge', 'label'=>'Tipo Licitação', 'id'=>'nu_licitacao'));

                                    ?>
                                </dl>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="form-actions">
                    <div class="btn-group">
                        <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar processo"> Salvar</button>
                        <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar"> Limpar</button>
                        <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
                    </div>
                </div>

            </div>

        </div>

