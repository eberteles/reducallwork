<?php echo $this->Form->create('Uasg', array('url' => array('controller' => 'uasg', 'action' => 'add'))); ?>

<div class="row-fluid">
    <div class="row-fluid">
        <div class="widget-header widget-header-small"><h4>Cadastro de UASG</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <?php
                    echo $this->Form->input('uasg', array('class' => 'input-xlarge', 'label' => 'Código da UASG'));
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar</button>
        </div>
    </div>
</div>

