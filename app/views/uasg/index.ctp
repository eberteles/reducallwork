<div class="page-header position-relative">
    <h1>UASG</h1>
</div>

<div class="acoes-formulario-top clearfix" >
    <p class="requiredLegend pull-left"><!--span class="required" title="Required">*</span--> </p>
    <div class="pull-right btn-group">
        <a href="<?php echo $this->Html->url(array('controller' => 'uasg', 'action' => 'add')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Nova UASG"><i class="icon-plus icon-white">
            </i>Nova UASG
        </a>
    </div>
</div>

<table cellpadding="0" cellspacing="0" style="background-color:white" class="table table-hover table-bordered table-striped" id="tbSetor">
    <tr>
        <th>UASG</th>
        <th class="actions"><?php __('Ações');?></th>
    </tr>
    <?php foreach($uasgs as $uasg){ ?>
        <tr>
            <th><?php echo $uasg['Uasg']['uasg']?></th>
        <th>
            <a href="/uasg/delete/<?php echo $uasg['Uasg']['co_uasg']?>" class="btn btn-danger alert-tooltip" title="Excluir"><i class="icon-remove"></i></a>
        </th>
        </tr>
    <?php } ?>
</table>
