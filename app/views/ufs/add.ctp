<div class="ufs form"><?php echo $this->Form->create('Uf');?>
<fieldset><legend><?php __('Uf'); ?></legend> <?php
echo $this->Form->input('sg_uf');
echo $this->Form->input('no_uf');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Listar Ufs', true), array('action' => 'index'));?></li>
</ul>
</div>
