<div class="ufs index">
    <div class="page-header position-relative"><h1><?php __('Ufs');?></h1></div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('sg_uf');?></th>
		<th><?php echo $this->Paginator->sort('no_uf');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($ufs as $uf):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $uf['Uf']['sg_uf']; ?>&nbsp;</td>
		<td><?php echo $uf['Uf']['no_uf']; ?>&nbsp;</td>
		<td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $uf['Uf']['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $uf['Uf']['id'])); ?>
		<?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $uf['Uf']['id']), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $uf['Uf']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Uf', true), array('action' => 'add')); ?></li>
</ul>
</div>
