<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<?php
$usuario = $this->Session->read('usuario');
if (!$this->Print->checkPermissao($usuario['UsuarioPerfil']['co_perfil'], 'usuarios/index')):
    echo "Você não tem permissão para cadastrar usuários";
else:
    echo $this->Form->create('Usuario'); ?>

    <div class="row-fluid">
        <div class="span6 ">
            <div class="widget-header widget-header-small"><h4><?php __('Dados Básicos'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="row-fluid">
                        <div class="span6">
                            <?php
                            echo $this->Form->hidden('co_usuario');
                            echo $this->Form->hidden('ic_acesso', array('value' => '1'));
                            echo $this->Form->input('nu_cpf', array('class' => 'input-xlarge', 'id' => 'nuCpf', 'label' => 'CPF', 'mask' => '999.999.999-99', 'onblur' => 'verificaCPF()'));
                            ?>
                            <span id="isValidMessage"></span>
                            <?php
                            echo $this->Form->input('ds_nome', array('class' => 'input-xlarge autocomplete', 'label' => 'Nome', 'maxLength' => '100', 'required' => 'required'));

                            // unidade administrativa
                            if (!isset($iframeFromSetor)) {
                                echo $this->Form->input('co_setor', array('id' => 'coSetorUsuario', 'class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores),
                                    'escape' => false));
                            }


                            echo $this->Form->input('co_projeto', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Projeto', 'options' => $projetos));
                            echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'label' => 'E-mail', 'required' => 'required'));
                            echo $this->Form->hidden('dt_liberacao', array('class' => 'input-xlarge', 'value' => date('d/m/Y H:i:s')));
                            echo $this->Form->label('dt_liberacao', 'Data de liberação', array('class' => 'input-xlarge', 'class' => 'required'));
                            echo date('d/m/Y H:i:s');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6 ">
            <div class="widget-header widget-header-small"><h4><?php __('Acesso'); ?></h4></div>
            <div class="widget-body">
                <div class="widget-main">
                    <?php
                    echo $this->Form->input('no_usuario', array('class' => 'input-xlarge', 'label' => 'Usuário', 'required' => 'required'));
                    ?>

                    <?php if (!Configure::read('App.config.component.ldap.enabled')): ?>
                        <?php
                        echo $this->Form->input('ds_senha', array('class' => 'input-xlarge', 'label' => 'Senha', 'type' => 'password', 'class' => 'required', 'maxLength' => '15', 'required' => 'required'));
                        echo $this->Form->input('ds_confirma_senha', array('class' => 'input-xlarge', 'label' => 'Confirmar senha', 'type' => 'password', 'class' => 'required', 'maxLength' => '15', 'required' => 'required'));
                        ?>
                    <?php endif; ?>

                    <?php
                    echo $this->Form->input('UsuarioPerfil.co_perfil', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Perfil', 'options' => $perfis, 'required' => 'required'));
                    echo $this->Form->input('co_gestor', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Gestor', 'options' => $gestores, 'disabled' => true));
                    ?>

                    <fieldset>
                        <legend>Tipo de Licença</legend>

                        <input type="hidden" name="data[Usuario][tipo_licenca]" id="UsuarioTipoDeLicença_" value="">

                        <label class="radio">
                            <input type="radio" name="data[Usuario][tipo_licenca]" id="UsuarioTipoDeLicençaNominal"
                                   value="nominal" <?php if (!$this->Modulo->check($total)) { ?> disabled="disabled" <?php } else { ?> checked="checked" <?php } ?> />
                            Licença nominal
                            <?php if (!$this->Modulo->check($total)): ?> (As licenças nominais acabaram.)<?php endif; ?>
                        </label>

                        <?php if (Configure::read('App.config.resource.licenca.concorrente')): ?>
                            <label class="radio">
                                <input type="radio" name="data[Usuario][tipo_licenca]"
                                       id="UsuarioTipoDeLicençaConcorrente" onchange="checkPrivileges()"
                                       value="concorrente" <?php if (!$this->Modulo->check($total)) { ?> checked="checked" <?php } ?>>
                                Licença concorrente
                            </label>
                        <?php endif; ?>
                        <?php if (Configure::read('App.config.resource.licenca.bi')): ?>
                            <?php $disabled = true; ?>
                            <?php if ($this->Modulo->checkBI($totalBI)): ?>
                                <?php $disabled = false; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <div class="btn-group">
            <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar" title="Salvar"> Salvar
            </button>
            <button rel="tooltip" type="button" title="Limpar dados preenchidos" class="btn btn-small" id="Limpar">
                Limpar
            </button>
            <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>

<script type="text/javascript">

    var Usuario = {
        init: function () {
            <?php if (Configure::read('App.config.component.ldap.enabled')): ?>

            var clearFields = function () {
                var fields = $('#UsuarioDsNome').data('fields');
                if (fields) {
                    fields.split(',').forEach(function (i) {
                        $('input[name="data[Usuario][' + i + ']"]').removeProp('readonly').val('');
                    });
                }
            };

            $('.autocomplete').autocomplete({
                delay: 500,
//                minLength: 5,
                change: function (event, ui) {
                    if (!ui.item) {
                        //limpa campos desabilitados no momento da selecao do item
                        clearFields();
                    }
                },
                response: function (event, ui) {
                    console.log('response: ', ui);
                    if (ui.content.length == 1 && ui.content[0].value == '') {
                        $(this).val('');
                    }
                },
                source: function (request, response) {
                    $.ajax({
                        url: "<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'getUsersLdap')) ?>/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            if (!data || data.length == 0) {
                                data = [{
                                    label: 'Nenhum registro encontrado',
                                    value: ''
                                }];
                                console.log(data);
                                response($.map(data, function (item) {
                                    return {
                                        label: item.label,
                                        value: item.value,
                                    }
                                }));
                            } else {
                                if (data.error == true) {
                                    alert(data.msg);
                                } else {
                                    response($.map(data, function (item) {
                                        return {
                                            label: item.ds_nome,
                                            value: item.ds_nome,
                                            user: item
                                        }
                                    }));
                                }
                            }
                        }
                    });
                },
                select: function (event, ui) {
                    var fields = [];
                    for (key in ui.item.user) {
                        if (ui.item.user[key] != null) {
                            var input = $('input[name="data[Usuario][' + key + ']"]');
                            input.val(ui.item.user[key]);
                            if (key != 'ds_nome') {
                                fields.push(key);
                                input.prop('readonly', 'readonly')
                            }
                        }
                    }
                    $('#UsuarioDsNome').data('fields', fields.join(','));
                }
            });

            //incrementa o bind do botão limpar para remover o atributos dos inputs
            //desabilitados no momento da seleção de um item no autocomplete
            $('#Limpar').bind('click', clearFields);

            <?php endif; ?>
        }
    };

    $(document).ready(function () {
        Usuario.init();

        $("#isValidMessage").hide();
        var coFiscal = <?php echo((defined('FISCAL')) ? constant("FISCAL") : false); ?>;

        $('#UsuarioPerfilCoPerfil').on('change', function () {
            var coFiscal = <?php echo((defined('FISCAL')) ? constant("FISCAL") : false); ?>;
            if ($('#UsuarioPerfilCoPerfil').val() == coFiscal) {
                $('#UsuarioCoGestor').attr('disabled', false);
            } else {
                $('#UsuarioCoGestor').attr('disabled', true);
                $('#UsuarioCoGestor').val("");
            }
        });

    });

    function verificaCPF() {
        var cpf = $("#nuCpf").val();

        cpf = cpf.replace('.', '').replace('.', '').replace('-', '');
        $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function (data) {
            document.getElementById('isValidMessage').style.color = data.color;
            document.getElementById('isValidMessage').style.fontWeight = data.bold;
            $("#isValidMessage").html(data.message).show();
        }).done(function () {
        });
    }
</script>

