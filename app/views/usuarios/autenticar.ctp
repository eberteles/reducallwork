<?php
if (Configure::read('App.config.resource.certificadoDigital')):
    $auth = $this->Util->getRestPkiClient()->getAuthentication();
    $token = $auth->startWithWebPki(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
    $this->Util->setNoCacheHeaders();
endif;
?>
<div class="modal-body">
    <form class="form-horizontal" method="post" id="form_login" action="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'autenticar')); ?>">
      <div class="control-group">
          <label for="input01" class="control-label">
              <span class="required">*</span> Usuário</label>
          <div class="controls">
              <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span>
                <input type="text" name="data[Usuario][no_usuario]" class="input-large" id="no_usuario">
              </div>
          </div>
      </div>
      <div class="control-group">
          <label for="input01" class="control-label">
              <span class="required">*</span> Senha
          </label>
          <div class="controls">
              <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span>
                <input type="password" name="data[Usuario][ds_senha]" maxlength="15" class="input-large" id="ds_senha">
              </div>
          </div>
      </div>
        <div class="controls required" id="id_validacao">
            <div class="error-message" id="id_error_login" style="display: none;">Campo Usuário e/ou Senha inválido.</div>
        </div>
</div>
<div class="modal-footer">

<button type="button" class="btn btn-small btn-primary" id="signInButton"><i class="icon-arrow-right icon-white"></i> Entrar</button>
    <?php if (Configure::read('App.config.resource.esqueciMinhaSenha') && !Configure::read('App.config.component.ldap.enabled')) { ?>
    <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'esqueci')); ?>" data-toggle="modal" class="btn btn-small btn-primary" title="Esqueci a minha senha"><i class="icon-plus icon-white"></i> Esqueci a minha Senha</a>
    <?php } ?>
    <a class="btn btn-small voltarcss" data-dismiss="modal" href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'autenticar')); ?>"><i class="icon-arrow-left icon-envelope"></i> Voltar</a>
  </form>
</div>
