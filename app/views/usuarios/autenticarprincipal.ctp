  <aside>
    <div style="text-align:center;margin-top:50%">
    <h1 class="logo-empresa">
      <a class="brand" data-placement="right" style="cursor: default;"
      title="<?php echo Configure::read('App.config.resource.layout.logo_superior.esquerda.title'); ?>"
      href="#">
      <img style="width:60%; min-width:180px;" src="<?php echo $this->base . Configure::read('App.config.resource.layout.logo_superior.esquerda.path'); ?>"/>
    </a>
  </h1>
  <form class="form-horizontal" id="form_login" method="post"
  action="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'autenticar')); ?>">
  <fieldset>
  <legend>Login de acesso</legend>
  <div class="controls required" id="id_validacao">
      <div class="error-message" id="id_error_login" style="display: none;">Campo Usuário e/ou
          Senha
          inválido.
      </div>
  </div>
  <div class="input-prepend">
      <span class="add-on"><i class="icon-user"></i></span>
      <input type="text" name="data[Usuario][no_usuario]" class="input-large"
             id="no_usuario" placeholder="Usuário">
  </div>
  <br /><br />
  <div class="input-prepend">
      <span class="add-on"><i class="icon-lock"></i></span>
      <input type="password" name="data[Usuario][ds_senha]" maxlength="15" class="input-large"
             id="ds_senha" placeholder="Senha">
  </div><br/>
  <br/>

  <button type="submit" class="btn btn-small btn-primary" id="signIn"><i
              class="icon-arrow-right icon-white"></i> Entrar
  </button>
  <?php if (Configure::read('App.config.resource.esqueciMinhaSenha') && !Configure::read('App.config.component.ldap.enabled')) { ?>
      <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'esqueci')); ?>"
         data-toggle="modal" class="btn btn-small btn-primary" title="Esqueci a minha senha"><i
                  class="icon-refresh icon-white"></i> Esqueci a minha Senha</a>
  <?php } ?>
</fieldset>
  </form>
  </div>
  </aside>

  <style>
  body {
    width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        font-family: 'Roboto slab', serif;
        position: absolute;
        background: #369;
        background-image: url(/fundoLogin.jpg);
        background-size: cover;
  }

  aside {
    width:350px;
    background:#fff;
    height:100%;
    Position:fixed;
    top:0;
    right:0;
    bottom:0;
  }

  </style>
