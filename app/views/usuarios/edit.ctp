<?php echo $this->Html->script('inicia-datetimepicker'); ?>
<?php $usuario = $this->Session->read('usuario'); ?>
<?php echo $this->Form->create('Usuario'); ?>

<div class="row-fluid">
    <div class="span6 ">
        <div class="widget-header widget-header-small"><h4><?php __('Dados Básicos'); ?>:</h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <dl class="dl-horizontal">
                    <?php
                    echo $this->Form->hidden('co_usuario');
                    echo $this->Form->hidden('ic_acesso', array('value' => '1'));

                    $options = array(
                        'class' => 'input-xlarge',
                        'label' => 'CPF',
                        'mask' => '999.999.999-99',
                        'id' => 'CPF'
                    );

                    if (!empty($pre_edicao['Usuario']['nu_cpf'])):
                        $options['readonly'] = 'readonly';
                    endif;

                    echo $this->Form->input('nu_cpf', $options);

                    $options = array(
                        'id' => 'nomeUsuario',
                        'class' => 'input-xlarge',
                        'label' => 'Nome',
                        'maxLength' => '100'
                    );

                    $disabled = true;

                    if ($usuario['UsuarioPerfil']['co_perfil'] == $coAdministrador
                        || !Configure::read('App.config.component.ldap.enabled')
                        && ($usuario['Usuario']['co_usuario'] == $this->data['Usuario']['co_usuario'])):
                        $disabled = false;
                    endif;

                    if (!empty($pre_edicao['Usuario']['ds_nome'])):
                        $options['readonly'] = 'readonly';
                    endif;

                    echo $this->Form->input('ds_nome', $options);

                    echo $this->Form->input('co_setor', array('id' => 'coSetorUsuario', 'class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'disabled' => $disabled, 'escape' => false));
                    echo $this->Form->input('co_projeto', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Projeto', 'disabled' => $disabled, 'options' => $projetos));
                    echo $this->Form->input('ds_email', array('class' => 'input-xlarge', 'disabled' => $disabled, 'label' => 'E-mail'));
                    echo $this->Form->hidden('dt_liberacao', array('class' => 'input-xlarge', 'value' => $dt_liberacao));
                    ?>
                </dl>
            </div>
        </div>
    </div>
    <div class="span6 ">
        <div class="widget-header widget-header-small"><h4><?php __('Acesso'); ?></h4></div>
        <div class="widget-body">
            <div class="widget-main">
                <dl class="dl-horizontal">
                    <?php
                    $readonly = false;
                    if ($disabled):
                        $readonly = true;
                    endif;

                    if (Configure::read('App.config.component.ldap.enabled') && !$readonly):
                        $readonly = true;
                    endif;

                    echo $this->Form->input('no_usuario', array('class' => 'input-xlarge', 'label' => 'Usuário', 'readonly' => $readonly));
                    ?>
                    <?php if (!Configure::read('App.config.component.ldap.enabled')): ?>
                        <div class="input password">
                            <label for="UsuarioDsSenha">Senha</label>
                            <input name="data[Usuario][ds_senha]" maxlength="15" id="UsuarioDsSenha" type="password">
                        </div>
                        <div class="input password">
                            <label for="UsuarioDsConfirmaSenha">Confirmar senha</label>
                            <input name="data[Usuario][ds_confirma_senha]" maxlength="15" id="UsuarioDsConfirmaSenha"
                                   type="password">
                        </div>
                    <?php endif; ?>

                    <?php
                    $readonly = false;
                    if ($usuario['UsuarioPerfil']['co_perfil'] != $coAdministrador):
                        $readonly = true;
                    endif;
                    echo $this->Form->input('UsuarioPerfil.co_perfil', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Perfil', 'disabled' => $readonly, 'options' => $perfis));
                    echo $this->Form->input('co_gestor', array('class' => 'input-xlarge', 'type' => 'select', 'empty' => 'Selecione...', 'label' => 'Gestor', 'disabled' => $readonly, 'options' => $gestores));
                    ?>

                    <fieldset>
                        <legend>Tipo de Licença</legend>
                        <input type="hidden" name="data[Usuario][tipo_licenca]" id="UsuarioTipoDeLicença_" value="">
                        <label class="radio">
                            <?php
                            $disabledText = "";
                            if ($usuario['UsuarioPerfil']['co_perfil'] != $coAdministrador):
                                $disabledText = "disabled='disabled'";
                            endif;
                            $readonlyN = 'disabled';
                            $checkedN = '';
                            if (!$this->Modulo->check($total)):
                                echo '(As licenças nominais acabaram.)';
                                $readonlyN = 'disabled';
                            else:
                                $readonlyN = '';
                            endif;
                            
                            if ($pre_edicao['Usuario']['licenca_nominal'] == '1'):
                                $checkedN = 'checked';
                            endif;
                            ?>
                            <input type="radio" name="data[Usuario][tipo_licenca]" <?php echo $disabledText; ?>
                                   id="UsuarioTipoDeLicençaNominal"
                                   value="nominal" <?php echo $readonlyN; ?> <?php echo $checkedN; ?> /> Licença nominal
                        </label>

                        <?php if (Configure::read('App.config.resource.licenca.concorrente')): ?>
                            <label class="radio">
                                <input type="radio" name="data[Usuario][tipo_licenca]" <?php echo $disabledText; ?>
                                       id="UsuarioTipoDeLicençaConcorrente"
                                       value="concorrente" <?php if ($pre_edicao['Usuario']['licenca_concorrente'] == '1') {
                                    echo 'checked';
                                } ?>> Licença concorrente
                            </label>
                        <?php endif; ?>

                        <?php if (Configure::read('App.config.resource.licenca.bi')): ?>
                            <?php $disabledBi = true; ?>
                            <?php if ($usuario['UsuarioPerfil']['co_perfil'] == $coAdministrador && $this->Modulo->checkBI($totalBI)): ?>
                                <?php $disabledBi = false; ?>
                            <?php endif; ?>

                            <?php if ($disabled): ?>
                                <?php $disabledBi = $disabled; ?>
                            <?php endif; ?>
                            <label class="checkbox">
                                <?php echo $this->Form->checkbox('licenca_bi', array('disabled' => $disabledBi)); ?>
                                Acesso ao B.I
                            </label>
                            <?php echo $this->Modulo->messageBI($totalBI); ?>
                        <?php endif; ?>

                    </fieldset>
                </dl>
            </div>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="btn-group">
        <button rel="tooltip" type="submit"
                class="btn btn-small btn-primary bt-pesquisar" <?php echo ($disabled) ? "disabled" : ""; ?>
                title="Salvar"> Salvar
        </button>
        <button rel="tooltip" type="button"
                title="Limpar dados preenchidos" <?php echo ($disabled) ? "disabled" : ""; ?> class="btn btn-small"
                id="Limpar"> Limpar
        </button>
        <button rel="tooltip" type="button" title="Voltar" class="btn btn-small" id="Voltar"> Voltar</button>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $(document).ready(function () {

        $("#isValidMessage").hide();

        var changeCoPerfil = function () {
            var coFiscal = '<?php echo ($disabled) ? "disabled" : $coFiscal; ?>';
            var coSetor = '<?php echo $coSetor; ?>';

            switch ($('#UsuarioPerfilCoPerfil').val()) {

                case coFiscal:
                    $('#UsuarioCoGestor').attr('disabled', false);
                    break;

                case coSetor:
                    $($("#coSetorUsuario").parents('div')[0]).addClass('required');
                    // $("#coSetorUsuario").attr('required', true);
                    break;

                default:
                    $('#UsuarioCoGestor').attr('disabled', true);
                    $('#UsuarioCoGestor').val("");

            }
        };
        changeCoPerfil();
        $('#UsuarioPerfilCoPerfil').on('change', changeCoPerfil);
    });

    function verificaCPF() {
        var cpf = $("#nuCpf").val();

        cpf = cpf.replace('.', '').replace('.', '').replace('-', '');
        $.getJSON("<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'verifyCPF')) ?>//" + "/" + cpf, function (data) {
            document.getElementById('isValidMessage').style.color = data.color;
            document.getElementById('isValidMessage').style.fontWeight = data.bold;
            $("#isValidMessage").html(data.message).show();
        }).done(function () {
        });
    }
</script>
