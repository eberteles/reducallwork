<form class="form-horizontal" method="post" action="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'esqueci')); ?>">
    <div class="modal-body">
        <div class="control-group">
            <label for="input01" class="control-label">
                <span class="required">*</span> E-mail</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" name="data[Usuario][ds_email]" class="input-large" >
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-small btn-primary"><i class="icon-arrow-right icon-white"></i> Enviar</button>
        <a class="btn btn-small voltarcss" data-dismiss="modal" href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'autenticar')); ?>"><i class="icon-arrow-left icon-envelope"></i> Voltar</a>
    </div>
</form>
