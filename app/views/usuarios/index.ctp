<?php
$usuarioSessao = $this->Session->read('usuario');

if (!$this->Print->checkPermissao($usuarioSessao['UsuarioPerfil']['co_perfil'], 'usuarios/index')):
    echo "Acesso negado.";
else:
    ?>

    <div class="row-fluid">

        <div class="page-header position-relative">
            <h1>
                <?php echo $this->Modulo->message($total); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php

                if (Configure::read('App.config.resource.licenca.bi')) {
                    echo 'Licenças de B.I.: ' . $totalBI . ' / ' . Configure::read('App.config.resource.licenca.bi');
                };

                if (Configure::read('App.config.resource.licenca.concorrente')) {
                    echo $this->Modulo->messageConcorrentes($totalConcorrente, $concorrentesOnline);
                };
                ?>
            </h1>
        </div>
        <?php if ($this->Print->checkPermissao($usuarioSessao['UsuarioPerfil']['co_perfil'], 'usuarios/add')): ?>
            <div class="acoes-formulario-top clearfix">
                <p class="requiredLegend pull-left">
                    Preencha um dos campos para fazer a pesquisa
                </p>
                <div class="pull-right btn-group">
                    <a href="<?php echo $this->Html->url(array('controller' => 'usuarios', 'action' => 'add')); ?>"
                       data-toggle="modal" class="btn btn-small btn-primary"
                       title="Novo Usuário"><i class="icon-plus icon-white"></i> Novo Usuário</a>
                </div>
            </div>
        <?php endif; ?>
        <?php echo $this->Form->create('Usuario'); ?>


        <div class="row-fluid">
            <div class="span3">
                <div class="controls">
                    <?php echo $this->Form->input('no_usuario', array('class' => 'input-xlarge', 'label' => 'Usuário')); ?>
                </div>
            </div>
            <div class="span2">
                <div class="controls">
                    <?php
                    echo $this->Form->input('nu_cpf', array(
                        'class' => 'input-medium',
                        'id' => 'nuCpf',
                        'label' => 'CPF',
                        'mask' => '999.999.999-99'
                    ));
                    ?>
                </div>
            </div>
            <div class="span3">
                <div class="controls">
                    <?php
                    echo $this->Form->input('co_setor', array('class' => 'input-xlarge chosen-select', 'type' => 'select', 'empty' => 'Selecione...', 'label' => __('Unidade Administrativa', true), 'options' => $imprimir->getArraySetores($setores), 'escape' => false));
                    ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="controls">
                    <?php echo $this->Form->input('UsuarioPerfil.co_perfil', array('type' => 'select', 'class' => 'input-xlarge', 'empty' => 'Selecione...', 'label' => 'Perfil', 'options' => $perfis)); ?>
                </div>
            </div>
            <div class="span3">
                <div class="controls">
                    <?php

                    echo $this->Form->input('ic_ativo', array(
                        'type' => 'select',
                        'class' => 'input-xlarge',
                        'empty' => 'Selecione...',
                        'label' => 'Situação',
                        'options' => array(
                            0 => 'Inativo',
                            1 => 'Ativo'
                        )
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="btn-group">
                <button rel="tooltip" type="submit" class="btn btn-small btn-primary bt-pesquisar"
                        title="Pesquisar processo"><i class="icon-search icon-white"></i> Pesquisar
                </button>
                <button rel="tooltip" type="button" id="Limpar" title="Limpar dados preechidos" class="btn btn-small">
                    Limpar
                </button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>

        <table cellpadding="0" cellspacing="0" style="background-color: white"
               class="table table-hover table-bordered table-striped">
            <thead>
            <tr>
                <th><?php echo 'CPF'; ?></th>
                <th><?php echo 'Usuário'; ?></th>
                <th><?php echo 'Setor'; ?></th>
                <th><?php echo 'Perfil'; ?></th>
                <th><?php echo 'E-mail'; ?></th>
                <th class="actions"><?php __('Ações'); ?></th>
            </tr>


            <thead>


            <tbody>
            <?php
            $i = 0;
            foreach ($usuarios as $usuario) :
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr <?php echo $class; ?>>
                    <td><?php echo $this->Print->cpf($usuario['Usuario']['nu_cpf']); ?>&nbsp;</td>
                    <td><?php echo $usuario['Usuario']['no_usuario']; ?>&nbsp;</td>
                    <td><?php echo(isset($usuario['Setor']['ds_setor']) ? $usuario['Setor']['ds_setor'] : '-'); ?>
                        &nbsp;</td>
                    <td><?php echo (isset($usuario["UsuarioPerfil"]['Perfil']['no_perfil'])) ? $usuario["UsuarioPerfil"]['Perfil']['no_perfil'] : " - "; ?>
                        &nbsp;</td>
                    <td><?php echo $usuario['Usuario']['ds_email']; ?>&nbsp;</td>
                    <td class="actions">
                        <div class="btn-group acoes">
                            <?php
                            if ($this->Print->checkPermissao($usuarioSessao['UsuarioPerfil']['co_perfil'], 'usuarios/edit')):
                                // Buttton Editar
                                echo $this->Html->link('<i class="icon-pencil"></i>', array(
                                    'action' => 'edit',
                                    $usuario['Usuario']['co_usuario']
                                ), array(
                                    'escape' => false,
                                    'class' => 'btn alert-tooltip',
                                    'title' => 'Editar'
                                ));
                            endif;

                            if ($this->Print->checkPermissao($usuarioSessao['UsuarioPerfil']['co_perfil'], 'usuarios/bloquearOrDesbloquear')):
                                // Buttton Bloquear
                                if ($usuario['Usuario']['ic_ativo'] == 1):
                                    echo $this->Html->link('<i class="icon-remove"></i>', array(
                                        'action' => 'bloquearOrDesbloquear',
                                        $usuario['Usuario']['co_usuario']
                                    ), array(
                                        'escape' => false,
                                        'class' => 'btn btn-danger alert-tooltip',
                                        'title' => 'Bloquear'
                                    ));
                                else:
                                    // Desbloquear
                                    if ($usuario['Usuario']['ic_ativo'] == 0):
                                        echo $this->Html->link('<i class="icon-ok"></i>', array(
                                            'action' => 'bloquearOrDesbloquear',
                                            $usuario['Usuario']['co_usuario']
                                        ), array(
                                            'escape' => false,
                                            'class' => 'btn btn-success',
                                            'title' => 'Desbloquear'
                                        ));
                                    endif;
                                endif;
                            endif;

                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p><?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
            ));
            ?></p>

        <div class="pagination">
            <ul>
                <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
                <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '')); ?><?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag' => 'li', 'separator' => ''), null, array('class' => 'disabled')); ?>
            </ul>
        </div>
    </div>

<?php endif; ?>
