<div class="usuariosAcoes form"><?php echo $this->Form->create('UsuariosAcao');?>
<fieldset><legend><?php __('Usuarios Acao'); ?></legend> <?php
echo $this->Form->input('co_usuario_acao');
echo $this->Form->input('co_acao');
echo $this->Form->input('co_usuario');
echo $this->Form->input('nu_ip');
echo $this->Form->input('dt_inclusao');
?></fieldset>
<?php echo $this->Form->end(__('Confirmar', true));?></div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>

	<li><?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $this->Form->value('UsuariosAcao.id')), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $this->Form->value('UsuariosAcao.id'))); ?></li>
	<li><?php echo $this->Html->link(__('Listar Usuarios Acoes', true), array('action' => 'index'));?></li>
</ul>
</div>
