<div class="usuariosAcoes index">
    <div class="page-header position-relative"><h1><?php __('Usuarios Acoes');?></h1></div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('co_usuario_acao');?></th>
		<th><?php echo $this->Paginator->sort('co_acao');?></th>
		<th><?php echo $this->Paginator->sort('co_usuario');?></th>
		<th><?php echo $this->Paginator->sort('nu_ip');?></th>
		<th><?php echo $this->Paginator->sort('dt_inclusao');?></th>
		<th class="actions"><?php __('Ações');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($usuariosAcoes as $usuariosAcao):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	?>
	<tr <?php echo $class;?>>
		<td><?php echo $usuariosAcao['UsuariosAcao']['co_usuario_acao']; ?>&nbsp;</td>
		<td><?php echo $usuariosAcao['UsuariosAcao']['co_acao']; ?>&nbsp;</td>
		<td><?php echo $usuariosAcao['UsuariosAcao']['co_usuario']; ?>&nbsp;</td>
		<td><?php echo $usuariosAcao['UsuariosAcao']['nu_ip']; ?>&nbsp;</td>
		<td><?php echo $usuariosAcao['UsuariosAcao']['dt_inclusao']; ?>&nbsp;</td>
		<td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $usuariosAcao['UsuariosAcao']['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $usuariosAcao['UsuariosAcao']['id'])); ?>
		<?php echo $this->Html->link(__('Excluir', true), array('action' => 'delete', $usuariosAcao['UsuariosAcao']['id']), null, sprintf(__('Tem certeza de que deseja excluir este registro?', true), $usuariosAcao['UsuariosAcao']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<p><?php
echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% de %count% registro(s)', true)
));
?></p>

<div class="pagination">
    <ul>
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array('tag'=>'li', 'separator'=>''), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(array('tag'=>'li', 'separator'=>''));?> <?php echo $this->Paginator->next(__('Próxima', true) . ' >>', array('tag'=>'li', 'separator'=>''), null, array('class' => 'disabled'));?>
    </ul>
</div>
</div>
<div class="actions">
<h3><?php __('Ações'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('Novo Usuarios Acao', true), array('action' => 'add')); ?></li>
</ul>
</div>
