/** * Fun��o para criar um objeto XMLHTTPRequest */
function CriaRequest(){
	try{ 
		request = new XMLHttpRequest(); 
	}
	catch (IEAtual){
		try{
			request = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(IEAntigo){
			try{
				request = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(falha){
				request = false;
			}
		}
	}
	if (!request)
		alert("Seu navegador n�oo suporta Ajax!");
	else return request;
}

//BUSCAR SERVIDORES
function getDocTipo(){
	var doc = document.getElementById("tipodoc").value;
	var result = document.getElementById("RetornoNomeDoc");
	var xmlreq = CriaRequest();
	
	// Exibi a imagem de progresso
	result.innerHTML = '<img src="../img/progressbar14.gif"/>';
	// Iniciar uma requisi��o
	xmlreq.open("GET", "../js/buscadoc.php?doc="+doc, true);
	
	xmlreq.onreadystatechange = function(){
		if (xmlreq.readyState == 4){ //alert(xmlreq.status);
			if (xmlreq.status == 200){ // 200 - o servidor retornou a p�gina com sucesso.
				result.innerHTML = xmlreq.responseText;				
			}
			else{
				result.innerHTML = "Erro: "+xmlreq.statusText;
			}
		}
	};
	xmlreq.send(null);
}
