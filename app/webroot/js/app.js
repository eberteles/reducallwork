$(document).ready(function () {

    $('#signInButton').click(signIn);

    function signIn() {

        var noUsuario = document.getElementById("no_usuario");
        var dsSenha = document.getElementById("ds_senha");

        if (noUsuario.value == "" || dsSenha.value == "") {
            document.getElementById("id_error_login").style.display = 'block';
            return false;
        } else {
            document.getElementById("id_error_login").style.display = 'none';
            $('#form_login').submit();
        }

    }

    $(document).keypress(function (e) {
        var elementExists = document.getElementById("form_login");
        if (e.which == 13 && elementExists != null) {
            signIn();
        }
    });

    $('.chosen-select').chosen();
    $('[data-rel=popover]').popover({container: 'body'});
    $('.alert-tooltip').tooltip();

    $('#Voltar').click(function (event) {
        event.preventDefault();
        history.back(1);
    });

    $('#Limpar').click(function () {
        $.each($('form').find('input:visible,textarea:visible,select:visible'), function (index, element) {
            var elementType = $(element).prop('nodeName');
            if (!$(element).is(":disabled")
                && !$(element).is("[readonly]")) {
                $(element).val("");
                if (elementType == 'SELECT') {
                    $('.chosen-select').val('').trigger("chosen:updated");
                }
            }
        });
    });

    $('.voltarcss').click(function (event) {
        event.preventDefault();
        history.back(1);
    });

    $("#searchIconContrato").bind('click', function (e) {
        $("#searchContrato").submit();
    });

    $("#searchAvan").bind('click', function (e) {
        $('#tp_filtro_avan').val('');
        $('#camposPesquisa').html("");
        var url_add_campo = $('#base_url_sistema').val() + "/contratos/add_campo/";
        $.ajax({
            type: "POST",
            url: url_add_campo,
            success: function (result) {
                $('#camposPesquisa').append(result);
            }
        });
        $('#view_pesquisa').modal();
    });

    $("#manualSistema").bind('click', function (e) {
        window.open($('#base_url_sistema').val() + "/manuais/downloadManualGescon");
    });

    // menu so sistema de pregão
    // $("li a[href='/pregao']").parent().parent().append('<li><a target="_blank" href="http://localhost:8081/pagina/portal/autologin.jsf"><i class="icon icon-cloud"></i>&nbsp;&nbsp;&nbsp;&nbsp;Pregão Eletrônico</a>')
    if ($("li a[href='/pregao']").length > 0) {
        $("li a[href='/pregao']").attr({
            href: 'http://localhost:8081/pagina/portal/autologin.jsf',
            target: '_blank'
        });
    }

    $("#btnPesqAvan").bind('click', function (e) {
        $("#pesqAvan").submit();
    });

    $('#Email').blur(function () {
        // captura email
        var email = $("#Email").val();
        // expressão regular
        var emailValido = /^.+@.+\..{2,}$/;

        if (!emailValido.test(email)) {
            alert('Email inválido!');
        }
    });

    $('#CPF').blur(function () {
        CPF = $(this).val();
        if (!CPF) {
            return false;
        }
        erro = new String;
        cpfv = CPF;

        if (cpfv.length == 14 || cpfv.length == 11) {
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('-', '');

            var nonNumbers = /\D/;

            if (nonNumbers.test(cpfv)) {
                erro = "A verificacao de CPF suporta apenas números!";
            } else {
                if (cpfv == "00000000000" ||
                    cpfv == "11111111111" ||
                    cpfv == "22222222222" ||
                    cpfv == "33333333333" ||
                    cpfv == "44444444444" ||
                    cpfv == "55555555555" ||
                    cpfv == "66666666666" ||
                    cpfv == "77777777777" ||
                    cpfv == "88888888888" ||
                    cpfv == "99999999999") {
                    erro = "Número de CPF inválido!"
                }
                var a = [];
                var b = new Number;
                var c = 11;

                for (i = 0; i < 11; i++) {
                    a[i] = cpfv.charAt(i);
                    if (i < 9) b += (a[i] * --c);
                }
                if ((x = b % 11) < 2) {
                    a[9] = 0
                } else {
                    a[9] = 11 - x
                }
                b = 0;
                c = 11;
                for (y = 0; y < 10; y++) b += (a[y] * c--);

                if ((x = b % 11) < 2) {
                    a[10] = 0;
                } else {
                    a[10] = 11 - x;
                }
                if ((cpfv.charAt(9) != a[9]) || (cpfv.charAt(10) != a[10])) {
                    erro = "Número de CPF inválido.";
                }
            }
        } else {
            if (cpfv.length == 0) {
                return false;
            } else {
                erro = "Número de CPF inválido.";
            }
        }
        if (erro.length > 0) {
            $(this).val('');
            alert(erro);
            setTimeout(function () {
                $(this).focus();
            }, 100);
            return false;
        }
        return $(this);
    });

    $('#CNPJ').blur(function () {
        CNPJ = $(this).val();
        if (!CNPJ) {
            return false;
        }
        erro = new String;
        if (CNPJ == "00.000.000/0000-00") {
            erro += "CNPJ inválido\n\n";
        }
        CNPJ = CNPJ.replace(".", "");
        CNPJ = CNPJ.replace(".", "");
        CNPJ = CNPJ.replace("-", "");
        CNPJ = CNPJ.replace("/", "");

        var a = [];
        var b = new Number;
        var c = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
        for (i = 0; i < 12; i++) {
            a[i] = CNPJ.charAt(i);
            b += a[i] * c[i + 1];
        }
        if ((x = b % 11) < 2) {
            a[12] = 0
        } else {
            a[12] = 11 - x
        }
        b = 0;
        for (y = 0; y < 13; y++) {
            b += (a[y] * c[y]);
        }
        if ((x = b % 11) < 2) {
            a[13] = 0;
        } else {
            a[13] = 11 - x;
        }
        if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])) {
            erro += "Dígito verificador com problema!";
        }
        if (erro.length > 0) {
            $(this).val('');
            alert(erro);
            setTimeout(function () {
                $(this).focus()
            }, 50);
        }
        return $(this);
    });

    // mascara de dinheiro
    $(".mascaraDihneiro").maskMoney({allowZero: true, thousands: '.', decimal: ','});

    // abrir manual em nova aba
    $("a[href=\"/manuais/downloadManualGescon\"").attr('target', '_blank');

    window.arrIds = [];
    var i;
    $("#idsExames li").each(function (index) {
        window.arrIds.push($(this).html());
    });

    $('#exameup').children().each(function (index) {
        for (i = 0; i < arrIds.length; i++) {
            if ($(this).attr('value').toString() == window.arrIds[i]) {
                $('#exameup option[value=' + $(this).attr('value').toString() + ']').hide().trigger('chosen:updated');
            }
        }
    });
    // adicionar novo exame
    $("#examesForm").submit(function (event) {
        event.preventDefault();
        $("#btnAddExame").toggleClass('disabled');
        var values = $(this).serialize();
        $.ajax({
            url: $("#urlsubmit").val(),
            type: 'POST',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            $("#btnAddExame").toggleClass('disabled');
            $("#exameup option:selected").hide().trigger('chosen:updated');
            $('#exameup').children().each(function () {
                $(this).removeAttr('selected').trigger('chosen:updated');
            });
            $("#alertasExames").append('<div id="ExameSuccMsg" class="alert alert-success">\
	            <button type="button" class="close" data-dismiss="alert">&times;</button>\
	            O registro foi salvo com sucesso\
	        </div>');
            addNewRow(data.exame.nome, data.exame.id, data.exame.idexame);

        }).error(function (error) {
            console.log(error);
        });
    });

    $("#dtNuFimVigencia").on('input', function () {
        if ($('#dtIniVigencia').val() != '' || $('#dtIniVigencia').val() != ' ') {
            if ($('#dtNuFimVigencia').val() != "" || $('#dtNuFimVigencia').val() != " ") {
                var value = moment($("#dtIniVigencia").val(), 'DD/MM/YYYY');

                $("#dtFimVigencia").val(
                    value
                        .add($('#dtNuFimVigencia').val(), 'days')
                        .format('DD/MM/YYYY')
                );
            }
        }
    });

    // paginação da tabela de atividades fiscalizações
    if ($('#table-atividades-contratos').length > 0) {
        $('#table-atividades-contratos').DataTable({
            ordering: false,
            sorting: false,
            pageLength: 10,
            bLengthChange: false,
            language: {
                search: '<strong>Pesquisar</strong>:',
                paginate: {
                    previous: '‹‹ Anterior',
                    next: 'Proximo ››'
                },
                aria: {
                    paginate: {
                        previous: 'Anterior',
                        next: 'Proximo'
                    }
                }
            }
        });
    }

    $('#table-atividades-contratos_wrapper').find('div.row-fluid .span6:nth-child(1)').remove();
    // deletar atividade
    $(".modal-delAtividade").on('click', function () {
        event.preventDefault();
        var self = $(this);
        var idAtividade = $(this).data('id');

        $("#ok-delete-atividade").on('click', function () {
            $.ajax({
                url: '/atividades/delete_atividadecontrato/' + idAtividade,
                method: 'POST',
                dataType: 'json'
            }).done(function () {
                $("#alertasAtividades").append('<div id="ExameSuccMsg" class="alert alert-warning">\
				    <button type="button" class="close" data-dismiss="alert">&times;</button>\
				    O registro foi excluído com sucesso\ </div>');
                removeTableRow2(self);
            });
            $('#confirm-delete-atividade').modal('hide');
        });
    });

    // selectContratos
    $("#selectContratos").on('input', function () {
        var text = $('#selectContratos option:selected').text().toLowerCase();
        if (text == 'todos') {
            $('#selectAtividadesContratos').attr('disabled', "disabled").trigger('chosen:updated');
        } else if (text == 'específicos') {
            $('#selectAtividadesContratos').removeAttr('disabled').trigger('chosen:updated');
        }
    });

    $("#nu_agencia").keypress(verificaNumero);
    $("#nu_conta").keypress(verificaNumero);

});

function formatReal(int) {
    var tmp = int + '';
    var neg = false;
    if (tmp.indexOf("-") == 0) {
        neg = true;
        tmp = tmp.replace("-", "");
    }

    if (tmp.length == 1) tmp = "0" + tmp

    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6)
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    if (tmp.length > 9)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2,$3");

    if (tmp.length > 12)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, ".$1.$2.$3,$4");

    if (tmp.indexOf(".") == 0) tmp = tmp.replace(".", "");
    if (tmp.indexOf(",") == 0) tmp = tmp.replace(",", "0,");

    return (neg ? '-' + tmp : tmp);
}

function aleatorio(inferior, superior) {
    numPossibilidades = superior - inferior;
    aleat = Math.random() * numPossibilidades;
    aleat = Math.floor(aleat);
    return parseInt(inferior) + aleat;
}

function dar_cor_aleatoria() {
    hexadecimal = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
    cor_aleatoria = "#";
    for (i = 0; i < 6; i++) {
        posarray = aleatorio(0, hexadecimal.length);
        cor_aleatoria += hexadecimal[posarray];
    }
    return cor_aleatoria;
}

function setMascaraCampo(campo, mascara) {

    $(campo).unmask();
    if (mascara != "") {
        $(campo).mask(mascara);
    }

}

function duasCasas(numero) {
    var retorno = numero.toString();
    if (retorno.length < 2) {
        retorno = "0" + retorno;
    }
    return retorno;
}

function calculaDtFimContrato(dataInicial) {
    var partes = dataInicial.split('/');
    dtm = new Date(partes[2], partes[1] - 1, partes[0]);
    dtm.addMonths(parseInt($("#ContratoNuQtdParcelas").val()) - 1);
    $("#ContratoDtFimVigencia").val(duasCasas(dtm.getDate()) + '/' + duasCasas((parseInt(dtm.getMonth()) + 1)) + '/' + dtm.getFullYear());
}

function calculaDtRecebimentoEntrega(dataOficial, nuPrazo) {
    if (dataOficial != '' && nuPrazo != '') {
        var partes = dataOficial.split('/');
        dtm = new Date(partes[2], partes[1] - 1, partes[0]);
        dtm.addDays(nuPrazo);
        $("#EntregaDtRecebimento").val(duasCasas(dtm.getDate()) + '/' + duasCasas((parseInt(dtm.getMonth()) + 1)) + '/' + dtm.getFullYear());
    }
}

function verificaNumero(e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
}

function addNewRow(exame, id, id_exame) {
    $("#products-table").DataTable()
        .row.add([
        exame, 'R$: <input onkeyup="mudarIconeSalvar(' + id + ')" maxlengh="12" style="height: 17px" id="exameValor' + id + '" class="input-xmedium mascaraDihneiro" type="text" value="0.00">',
        '<div class="btn-group"><a id="#iconeMudar ' + id + '" onclick="updateExameValor(' + id + ', $(this))" class="btn btn-warning" href=""><i class="icon-save"></i></a><a onclick="deleteExameVinculado(this, ' + id + ', ' + id_exame + ')" class="btn btn-danger" href=""><i class="icon-remove"></i></a>'
    ]).draw(true);
    $(".mascaraDihneiro").maskMoney({allowZero: true, thousands: '', decimal: ','});
    window.parent.$("body").animate({scrollTop: 300}, 'normal');
    $(".alert").fadeTo(3900, 0, function () {
        $(this).remove();
    });
}

function updateExameValor(id, self) {
    event.preventDefault();
    var valor = $("#exameValor" + id).maskMoney({thousands: '', decimal: ','}).trigger('mask.maskMoney').val(),
        self = $(self);

    $("#exameValor" + id).maskMoney({thousands: '.', decimal: ','}).trigger('mask.maskMoney')

    $.ajax({
        url: '/locais_atendimento/updateExameValor/' + id,
        type: 'POST',
        data: {'data': valor},
        dataType: 'json'
    }).done(function (data) {
        $("#alertasExames").append(
            '<div id="ExameErrorMsg" class="alert alert-success">\
                <button type="button" class="close" data-dismiss="alert">&times;</button>\
                O registro foi salvo com successo\
            </div>');
        self.removeClass('btn-warning').addClass('btn-success');
        self.find('i').removeClass('icon-save').addClass('icon-ok');
        window.parent.$("body").animate({scrollTop: 300}, 'normal');
        $(".alert").fadeTo(3900, 0, function () {
            $(this).remove();
        });
    });
}

function deleteExameVinculado(self, id, id_exame) {
    $('#exameup option[value=' + id_exame.toString() + ']').show().trigger('chosen:updated');
    event.preventDefault();

    $.ajax({
        url: '/locais_atendimento/deleteExameVinculado/' + id,
        type: 'POST',
        dataType: 'json'
    }).done(function (data) {
        $("#alertasExames").append(
            '<div id="ExameSuccMsg" class="alert alert-success">\
                <button type="button" class="close" data-dismiss="alert">&times;</button>\
                O registro foi excluído com sucesso\
            </div>');
        removeTableRow(self);
    });
}

function removeTableRow(handler) {
    var tr = $(handler).closest('tr');
    // $("#products-table").DataTable().row(handler).remove().draw(false);

    tr.fadeOut(400, function () {
        $("#products-table").DataTable().row(tr).remove().draw(false);
        window.parent.$("body").animate({scrollTop: 300}, 'normal');
        $(".alert").fadeTo(3900, 0, function () {
            $(this).remove();
        });
    });
};

function removeTableRow2(handler) {
    var tr = $(handler).closest('tr');
    // $("#table-atividades-contratos").DataTable().row(handler).remove().draw(false);

    tr.fadeOut(400, function () {
        $("#table-atividades-contratos").DataTable().row(tr).remove().draw(false);
        window.parent.$("body").animate({scrollTop: 300}, 'normal');
        $(".alert").fadeTo(3900, 0, function () {
            $(this).remove();
        });
    });
};

function mudarIconeSalvar(id) {
    // console.log($("#iconeMudar" + id));
    $("#iconeMudar" + id).removeClass('btn-success').addClass('btn-warning');
    $("#iconeMudar" + id).find('i').removeClass('icon-ok').addClass('icon-save');
};
function somenteNumeros(num) {
    var er = /[^0-9.]/;
    er.lastIndex = 0;
    var campo = num;
    if (er.test(campo.value)) {
        campo.value = "";
    }
}
