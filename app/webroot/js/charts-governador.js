/**
 * Gráficos para Governador
 */

// Primeiro Gráfico do dashboard
function contratosAtivosPorSetor(url, dados) {

	var TotalDeContratos = null;
	TotalDeContratos = dados[0]['y'] + dados[1]['y'];
	$("#totalDeContratos").html(
			"<hr><center><h5>Total de contratos: " + TotalDeContratos
					+ "</h5></center><hr>");
	// Grafico de pizza
	$('#container').highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							options3d : {
								enabled : true,
								alpha : 45,
								beta : 0
							},
						},
						title : {
							text : null
						},
						exporting : {
							enabled : false
						},
						tooltip : {
							pointFormat : '<b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : false
								},
								showInLegend : true,
								depth : 35
							},
							series : {
								cursor : 'pointer',
								point : {
									events : {
										click : function() {
											var _loadingText = '<div style="width:100%;height:100%;text-align:center;font-size:20px;background-color:#CCC;"><span style="position:fixed; top:40%; margin-left:30px;"><br><br><img src="img/loading.gif"></span></div>';
											$('#content-innerCt').html(_loadingText);
											$("#tabela").html("");
											$("#tituloAbaImpressoa").html("");
											$('#view_imp_relatorio').modal();
											var condicao = this.name;
											$.ajax({
														type : "POST",
														url : url
																+ "/dashboard/contratosAtivos",
														data : {
															"data[condicao]" : this.name
														},
														success : function(response) {
															$("#content-innerCt").html('');
															var arr = JSON.parse(response);
															var out = "<table id='table-ativos' width='100%' style='border:1px solid #ccc;' cellspacing='0'><thead><tr><th style='border:1px solid #ccc; width:500px;'>Secretaria</th><th style='border:1px solid #ccc;'>Quantidade de Contratos "
																	+ condicao
																	+ "  desta Secretaria</th><th style='border:1px solid #ccc;'>Total de Contratos desta Secretaria</th><th>% "
																	+ condicao
																	+ " na Secretaria</th><th style='border:1px solid #ccc;'>% "
																	+ condicao
																	+ " em relação ao Estado</th></tr></thead><tbody>";
															for (var i = 0; i < arr.length; i++) {
																out += "<tr><td style='border:1px solid #ccc;'>"
																		+ arr[i].Contrato.instituicao
																		+ "</td><td style='border:1px solid #ccc;'><center>"
																		+ arr[i].Contrato.quantidade_de_contratos_ativos
																		+ "</center></td><td style='border:1px solid #ccc;'><center>"
																		+ arr[i].Contrato.total_de_contratos
																		+ "</center></td><td style='border:1px solid #ccc;'><center>"
																		+ arr[i].Contrato.percentual_de_contratos_ativos
																		+ "%</center></td><td style='border:1px solid #ccc;'><center>"
																		+ arr[i].Contrato.percentual_de_contratos_ativos_em_relacao_ao_total_de_contratos
																		+ "%</center></td></tr>";
															}
															out += "</tbody></table>";
															$("#tabela").html(out);
															$("#table-ativos").DataTable();
															$("#tituloAbaImpressoa").html("<h3>Contratos " + condicao + "</h3>");
														}
													})
										}
									}
								}
							},
						},
						series : [ {
							type : 'pie',
							name : null,
							data : dados
						} ]
					});
}

// Segundo Gráfico do dashboard
function contratos_A_pagar(url, valor, percentual) {

	ValorTotalAPagar = null
	for (var int = 0; int < valor.length; int++) {
		window.ValorTotalAPagar += valor[int];
	}

	$("#ValorTotalAPagar").html(
			"<hr><center><h5>Valor total: R$ " + formatReal(ValorTotalAPagar)
					+ "</h5></center><hr>");
	// Grafico de barras
	var chart = new Highcharts.Chart(
			{
				chart : {
					renderTo : 'bars',
					type : 'column',
					size: '100%',
					height : 305,
					options3d : {
						enabled : true,
						alpha : 0,
						beta : 10,
						viewDistance : 25
					},
				},
				exporting : {
					enabled : false
				},
				title : {
					text : null
				},
				xAxis : {
					categories : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril',
							'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
							'Outubro', 'Novembro', 'Dezembro' ],
					title : {
						text : null
					},
					gridLineWidth : 0
				},
				yAxis : {
					min : 0,
					title : {
						text : 'Valores em R$ 1.000,00 (mil)',
						align : 'high'
					},
					labels : {
						overflow : 'justify',
						formatter : function() {
							return this.value / 1000
						}
					}
				},
				plotOptions : {
					bar : {
						dataLabels : {
							enabled : true
						}
					},
					series : {
						cursor : 'pointer',
						point : {
							events : {
								click : function() {
									var _loadingText = '<div style="width:100%;height:100%;text-align:center;font-size:20px;background-color:#CCC;"><span style="position:fixed;top:40%; margin-left: 30px;"<br><br><img src="img/loading.gif"></span></div>';
									$('#content-innerCt').html(_loadingText);
									$("#tabela").html("");
									$("#tituloAbaImpressoa").html("");
									var mes = this.x + 1;
									var valorMensal = this.y;
									$('#view_imp_relatorio').modal();
									$.ajax({
												type : "POST",
												url : url
														+ '/dashboard/contratos_A_PagarPorMes',
												data : {
													"data[Mes]" : mes,
													"data[ValorMensal]" : valorMensal
												},
												success : function(response) {
													$("#content-innerCt").html('');
													var arr = JSON.parse(response);
													var out = "<table id='table-a-pagar' width='100%' style='border:1px solid #ccc;' cellspacing='0'><thead><tr><th style='border:1px solid #ccc; width:500px;'>Secretaria</th><th style='border:1px solid #ccc;'>% em relação ao valor total à ser pago no mês</th><th style='border:1px solid #ccc;'>Valor à ser pago no mês</th></tr></thead><tbody>";
													for (var i = 0; i < arr.length; i++) {
														out += "<tr> <td style='border:1px solid #ccc;'>"
																+ arr[i].Contrato.instituicao
																+ "</td> <td style='border:1px solid #ccc;'><center>"
																+ arr[i].Contrato.percentual_em_relacao_ao_total_do_mes
																+ "%</center></td><td style='border:1px solid #ccc;'><center>"
																+ "R$ "
																+ arr[i].Contrato.a_pagar
																+ "</td></tr>";
													}
													out += "</tbody></table>";
													$("#tabela").html(out);
													$("#table-a-pagar").DataTable();
													$("#tituloAbaImpressoa").html("");
													$("#tituloAbaImpressoa").html("<h3>Valores à pagar em " + retornaMes(mes) + "</h3>");
												}
											}) // END AJAX

								} // END CLICK
							}
						}
					}
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'top',
					x : -40,
					y : 100,
					floating : true,
					borderWidth : 1,
					backgroundColor : ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
					shadow : true
				},
				credits : {
					enabled : false
				},
				series : [ {
					name : 'Valor',
					data : valor,
					color : '#FF6600',
					tooltip : {
						valuePrefix : 'R$ ',
						valueDecimals : 2,
					},
					dataLabels : {
						enabled : true,
						rotation : 30,
						color : '#FFFFFF',
						align : 'right',
						format : '{point.y:.1f}', // one decimal
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : 'R$ {point.y:.2f}'
					}
				}, {
					name : 'Percentual',
					data : percentual,
					tooltip : {
						valueSuffix : '%',
						valueDecimals : 0,
					},
					dataLabels : {
						enabled : true,
						rotation : 30,
						color : '#FFFFFF',
						align : 'right',
						format : '{point.y:.0f}', // one decimal
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : '{point.y:.0f}%'
					}
				} ]
			});

}

function contratosPagos(url, valor, percentual) {

	ValorTotalPago = null
	for (var int = 0; int < valor.length; int++) {
		window.ValorTotalPago += valor[int];
	}

	$("#ValorTotalPago").html(
			"<hr><center><h5>Valor total: R$ " + formatReal(ValorTotalPago)
					+ "</h5></center><hr>");

	// Grafico de barras
	var chart = new Highcharts.Chart(
			{
				chart : {
					renderTo : 'bars2',
					type : 'column',
			        spacingBottom: 15,
			        spacingTop: 10,
			        spacingLeft: 10,
			        spacingRight: 10,
			        size: '100%',
					height : 305,
					options3d : {
						enabled : true,
						alpha : 0,
						beta : 10,
						viewDistance : 25
					},
				},
				exporting : {
					enabled : false
				},
				title : {
					text : null,
				},
				xAxis : {
					categories : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril',
							'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
							'Outubro', 'Novembro', 'Dezembro' ],
					title : {
						text : null
					},
					gridLineWidth : 0
				},
				yAxis : {
					min : 0,
					title : {
						text : 'Valores em R$ 1.000,00 (mil)',
						align : 'high'
					},
					labels : {
						overflow : 'justify',
						formatter : function() {
							return this.value / 1000
						}
					}
				},
				plotOptions : {
					bar : {
						dataLabels : {
							enabled : true
						}
					},
					series : {
						cursor : 'pointer',
						point : {
							events : {
								click : function() {
									var _loadingText = '<div style="width:100%;height:100%;text-align:center;font-size:20px;background-color:#CCC;"><span style="position:fixed;top:40%; margin-left: 30px;"><br><br><img src="img/loading.gif"></span></div>';
									$('#content-innerCt').html(_loadingText);
									$("#tabela").html("");
									$("#tituloAbaImpressoa").html("");
									$('#view_imp_relatorio').modal();
									var mes = this.x + 1;
									var valorMensal = this.y;
									$.ajax({
												type : "POST",
												url : url
														+ '/dashboard/contratosPagosPorMes',
												data : {
													"data[Mes]" : mes,
													"data[ValorMensal]" : valorMensal
												},
												success : function(response) {
													$("#content-innerCt").html('');
													var arr = JSON
															.parse(response);
													var out = "<table id='table-pago' width='100%' style='border:1px solid #ccc;' cellspacing='0'><thead><tr><th style='border:1px solid #ccc; width:500px'>Secretaria</th><th style='border:1px solid #ccc;'>% em relação ao valor total pago no mês</th><th style='border:1px solid #ccc;'>Valor pago no mês</th></tr></thead><tbody>";
													for (var i = 0; i < arr.length; i++) {
														out += "<tr><td style='border:1px solid #ccc;'>"
																+ arr[i].Contrato.instituicao
																+ "</td><td style='border:1px solid #ccc;'><center>"
																+ arr[i].Contrato.percentual_em_relacao_ao_total_do_mes
																+ "%</center></td><td style='border:1px solid #ccc;'><center>"
																+ "R$ "
																+ arr[i].Contrato.pago
																+ "</center></td></tr>";
													}
													out += "</tbody></table>";
													$("#tabela").html(out);
													$("#table-pago").DataTable();
													$("#tituloAbaImpressoa").html("<h3>Valores pagos em " + retornaMes(mes) + "</h3>");
												}
											}) // END AJAX

								} // END CLICK
							}
						}
					}
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'top',
					x : -40,
					y : 100,
					floating : true,
					borderWidth : 1,
					backgroundColor : ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
					shadow : true
				},
				credits : {
					enabled : false
				},
				series : [ {
					name : 'Valor',
					data : valor,
					tooltip : {
						valuePrefix : 'R$ ',
						valueDecimals : 2,
					},
					color : '#00FF66',
					dataLabels : {
						enabled : true,
						rotation : 30,
						color : '#FFFFFF',
						align : 'right',
						format : '{point.y:.1f}', // one decimal
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : 'R$ {point.y:.2f}'
					}
				}, {
					name : 'Percentual',
					data : percentual,
					tooltip : {
						valueSuffix : '%',
						valueDecimals : 0,
					},
					dataLabels : {
						enabled : true,
						rotation : 30,
						color : '#FFFFFF',
						align : 'right',
						format : '{point.y:.0f}', // one decimal
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : '{point.y:.0f}%'
					}
				} ]
			});
}

function trimestre(pagos, dados) {

	$("#vta").html(
			"<hr><center><h5>Valor total à pagar: R$ "
					+ formatReal(ValorTotalAPagar)
					+ "<br>Valor total pago: R$ " + formatReal(ValorTotalPago)
					+ "</h5></center><hr>");

	var chart = new Highcharts.Chart(
			{
				chart : {
					renderTo : 'bars3',
					type : 'column',
					size: '100%',
					height : 300,
					options3d : {
						enabled : true,
						alpha : 0,
						beta : 20,
						viewDistance : 25
					},
				},
				title : {
					text : null
				},
				exporting : {
					enabled : false
				},
				xAxis : {
					categories : [ '1º Trimestre', '2º Trimestre',
							'3º Trimestre', '4º Trimestre' ],
					title : {
						text : null
					},
					gridLineWidth : 0
				},
				yAxis : {
					min : 0,
					title : {
						text : 'Valores em R$ 1.000,00 (mil)',
						align : 'high'
					},
					labels : {
						overflow : 'justify',
						formatter : function() {
							return this.value / 1000
						}
					}
				},
				tooltip : {
					valuePrefix : 'R$ '
				},
				plotOptions : {
					bar : {
						dataLabels : {
							enabled : true
						}
					}
				},
				legend : {
					layout : 'vertical',
					align : 'right',
					verticalAlign : 'top',
					x : -280,
					y : 10,
					floating : true,
					borderWidth : 1,
					backgroundColor : ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
					shadow : true
				},
				credits : {
					enabled : false
				},
				series : [ {
					name : 'Pago',
					data : pagos,
					color : '#00FF66',
					dataLabels : {
						enabled : true,
						rotation : 0,
						color : '#FFFFFF',
						align : 'right',
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : 'R$ {point.y:.2f}'
					}
				}, {
					name : 'À Pagar',
					data : dados,
					color : '#FF6600',
					dataLabels : {
						enabled : true,
						rotation : 0,
						color : '#FFFFFF',
						align : 'right',
						y : 0, // 10 pixels down from the top
						style : {
							fontSize : '10px',
							fontFamily : 'Verdana, sans-serif'
						},
						format : 'R$ {point.y:.2f}'
					}
				} ]
			});

}

function vencimentosContratos1(total) {

	var gaugeOptions = {

			chart : {
				type : 'solidgauge',
				height : 140,
				width : 300
			},
			title : null,
			pane : {
				center : [ '60%', '30%' ],
				size : '70%',
				startAngle : -90,
				endAngle : 90,
				background : {
					backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
							|| '#EEE',
					innerRadius : '60%',
					outerRadius : '100%',
					shape : 'arc'
				}
			},
			// the value axis
			yAxis : {
				stops : [ [ 0.1, '#55BF3B' ], // green
				[ 0.5, '#DDDF0D' ], // yellow
				[ 0.9, '#DF5353' ] // red
				],
				lineWidth : 0,
				minorTickInterval : null,
				tickPixelInterval : 400,
				title : {
					y : -70
				},
				labels : {
					y : 16
				}
			},
			exporting : {
				enabled : false
			},
			plotOptions : {
				solidgauge : {
					dataLabels : {
						y : 5,
						borderWidth : 0,
						useHTML : true
					}
				}
			}
		};
		// The speed gauge
		$('#gauge')
				.highcharts(
						Highcharts
								.merge(
										gaugeOptions,
										{
											yAxis : {
												min : 0,
												max : 50,
												title : {
													text : null
												}
											},
											credits : {
												enabled : false
											},
											series : [ {
												name : null,
												enabled : true,
												enableMouseTracking: false,
												data : [ total ],
												dataLabels : {
													format : '<div style="text-align:center"><span style="font-size:25px;color:'
															+ ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black')
															+ '">{y}</span><br/>'
															+ '<span style="font-size:12px;color:silver">30 Dias</span></div>'
												},
												tooltip : {
													enabled : true,
													valueSuffix : null
												}
											} ]
										}));


}

function vencimentosContratos2(total) {

	var gaugeOptions = {

		chart : {
			type : 'solidgauge',
			height : 140,
			width : 300
		},
		title : null,
		pane : {
			center : [ '60%', '30%' ],
			size : '70%',
			startAngle : -90,
			endAngle : 90,
			background : {
				backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
						|| '#EEE',
				innerRadius : '60%',
				outerRadius : '100%',
				shape : 'arc'
			}
		},
		// the value axis
		yAxis : {
			stops : [ [ 0.1, '#55BF3B' ], // green
			[ 0.5, '#DDDF0D' ], // yellow
			[ 0.9, '#DF5353' ] // red
			],
			lineWidth : 0,
			minorTickInterval : null,
			tickPixelInterval : 400,
			title : {
				y : -70
			},
			labels : {
				y : 16
			}
		},
		exporting : {
			enabled : false
		},
		plotOptions : {
			solidgauge : {
				dataLabels : {
					y : 5,
					borderWidth : 0,
					useHTML : true
				}
			}
		}
	};
	// The speed gauge
	$('#gauge2')
			.highcharts(
					Highcharts
							.merge(
									gaugeOptions,
									{
										yAxis : {
											min : 0,
											max : 50,
											title : {
												text : null
											}
										},
										credits : {
											enabled : false
										},
										series : [ {
											name : null,
											enabled : true,
											enableMouseTracking: false,
											data : [ total ],
											dataLabels : {
												format : '<div style="text-align:center"><span style="font-size:25px;color:'
														+ ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black')
														+ '">{y}</span><br/>'
														+ '<span style="font-size:12px;color:silver">60 Dias</span></div>'
											},
											tooltip : {
												enabled : true,
												valueSuffix : null
											}
										} ]
									}));
}

function vencimentosContratos3(total) {

	var gaugeOptions = {

		chart : {
			type : 'solidgauge',
			height : 140,
			width : 300
		},
		title : null,
		pane : {
			center : [ '60%', '30%' ],
			size : '70%',
			startAngle : -90,
			endAngle : 90,
			background : {
				backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
						|| '#EEE',
				innerRadius : '60%',
				outerRadius : '100%',
				shape : 'arc'
			}
		},
		// the value axis
		yAxis : {
			stops : [ [ 0.1, '#55BF3B' ], // green
			[ 0.5, '#DDDF0D' ], // yellow
			[ 0.9, '#DF5353' ] // red
			],
			lineWidth : 0,
			minorTickInterval : null,
			tickPixelInterval : 400,
			title : {
				y : -70
			},
			labels : {
				y : 16
			}
		},
		exporting : {
			enabled : false
		},
		plotOptions : {
			solidgauge : {
				dataLabels : {
					y : 5,
					borderWidth : 0,
					useHTML : true
				}
			}
		}
	};
	// The speed gauge
	$('#gauge3')
			.highcharts(
					Highcharts
							.merge(
									gaugeOptions,
									{
										yAxis : {
											min : 0,
											max : 50,
											title : {
												text : null
											}
										},
										credits : {
											enabled : false
										},
										series : [ {
											name : null,
											enabled : true,
											enableMouseTracking: false,
											data : [ total ],
											dataLabels : {
												format : '<div style="text-align:center"><span style="font-size:25px;color:'
														+ ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black')
														+ '">{y}</span><br/>'
														+ '<span style="font-size:12px;color:silver">90 Dias</span></div>'
											},
											tooltip : {
												enabled : true,
												valueSuffix : null
											}
										} ]
									}));

}

function vencimentosContratos4(total) {

	var gaugeOptions = {

		chart : {
			type : 'solidgauge',
			height : 140,
			width : 300
		},
		title : null,
		pane : {
			center : [ '60%', '30%' ],
			size : '70%',
			startAngle : -90,
			endAngle : 90,
			background : {
				backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
						|| '#EEE',
				innerRadius : '60%',
				outerRadius : '100%',
				shape : 'arc'
			}
		},
		// the value axis
		yAxis : {
			stops : [ [ 0.1, '#55BF3B' ], // green
			[ 0.5, '#DDDF0D' ], // yellow
			[ 0.9, '#DF5353' ] // red
			],
			lineWidth : 0,
			minorTickInterval : null,
			tickPixelInterval : 400,
			title : {
				y : -70
			},
			labels : {
				y : 16
			}
		},
		exporting : {
			enabled : false
		},
		plotOptions : {
			solidgauge : {
				dataLabels : {
					y : 5,
					borderWidth : 0,
					useHTML : true
				}
			}
		}
	};
	// The speed gauge
	$('#gauge4')
			.highcharts(
					Highcharts
							.merge(
									gaugeOptions,
									{
										yAxis : {
											min : 0,
											max : 50,
											title : {
												text : null
											}
										},
										credits : {
											enabled : false
										},
										series : [ {
											name : null,
											enabled : true,
											enableMouseTracking: false,
											data : [ total ],
											dataLabels : {
												format : '<div style="text-align:center"><span style="font-size:25px;color:'
														+ ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black')
														+ '">{y}</span><br/>'
														+ '<span style="font-size:12px;color:silver">120 Dias</span></div>'
											},
											tooltip : {
												enabled : true,
												valueSuffix : null
											}
										} ]
									}));

}

function contratosComPagamentoAtrasado(url, dados) {

/*	var atrasos = null;
	atrasos = dados[0]['y'] + dados[1]['y'];
	$("#totalDeAtrasosENaoAtraso").html(
			"<hr><center><h5>Total de contratos: " + atrasos
					+ "</h5></center><hr>");*/

	// Grafico de pizza
	$('#pagamento-atraso')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							options3d : {
								enabled : true,
								alpha : 45,
								beta : 0
							},

						},
						title : {
							text : null
						},
						tooltip : {
							pointFormat : '<b>{point.percentage:.1f}%</b>'
						},
						exporting : {
							enabled : false
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : false
								},
								showInLegend : true,
								depth : 35,
							},
							series : {
								cursor : 'pointer',
								point : {
									events : {
										click : function() {
											var _loadingText = '<div style="width:100%;height:100%;text-align:center;font-size:20px;background-color:#CCC;"><span style="position:fixed;top:40%; margin-left: 30px;"<br><br><img src="img/loading.gif"></span></div>';
											$('#content-innerCt').html(_loadingText);
											$("#tabela").html("");
											$("#tituloAbaImpressoa").html("");
											$('#view_imp_relatorio').modal();
											var condicao = this.name;
											var p = null;
											var t = null;

											if (this.name == "Em Dia") {
												p = "Contratos com Pagamento em Dia";
												t = "Valor pago";
											} else if (this.name == "Com Atraso") {
												p = "Contratos com Pagamento em Atraso";
												t = "Valor à ser pago";
											}

											$.ajax({
														type : "POST",
														url : url
																+ "/dashboard/contratosComPagamentosEmAtrasoPorSetor",
														data : {
															"data[condicao]" : this.name
														},
														success : function(
																response) {
															$(
																	"#content-innerCt")
																	.html('');
															var arr = JSON
																	.parse(response);
															var out = "<table id='table-atrasos' width='100%' style='border:1px solid #ccc;' cellspacing='0'><thead><tr><th style='width:550px; border:1px solid #ccc;'>Secretaria</th><th style='border:1px solid #ccc;'>Quantidade de Contratos "
																	+ condicao
																	+ " </th><th style='border:1px solid #ccc;'>Total de Contratos desta Secretaria</th><th style='border:1px solid #ccc;'>% de Contratos "
																	+ condicao
																	+ " em Relação ao Estado</th><th style='width:100px;'>"
																	+ t
																	+ " </th></tr></thead><tbody>";
															for (var i = 0; i < arr.length; i++) {
																out += "<tr><td style='border:1px solid #ccc;'><h7>"
																		+ arr[i].Contrato.instituicao
																		+ "</h7></td'><td style='border:1px solid #ccc;'><center><h7>"
																		+ arr[i].Contrato.quantidade_de_contratos
																		+ "</h7></center></td><td style='border:1px solid #ccc;'><center><h7>"
																		+ arr[i].Contrato.total_de_contratos
																		+ "</h7></center></td><td style='border:1px solid #ccc;'><center><h7>"
																		+ arr[i].Contrato.percentual
																		+ "%</h7></center></td><td style='border:1px solid #ccc;'><center><h7>R$ "
																		+ arr[i].Contrato.valor
																		+ "</h7></center></td></tr>";
															}
															out += "</tbody></table>";
															$("#tabela").html(out);
															$("#table-atrasos").DataTable();
															$("#tituloAbaImpressoa").html("<h3>" + p + "</h3>");
															
														}
													})
													
										}
									}
								}
							},
						},
						series : [ {
							type : 'pie',
							name : null,
							data : dados,
							dataLabels : {
								enabled : false,
								format : '<b>{point.name}</b> ({point.y:,.0f})',
								color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
										|| 'black',
								softConnector : true
							},
							neckWidth : '30%',
							neckHeight : '25%'
						} ]
					});
}

function contratosComPendencias(p1) {

	/*
	 * var dadosPendents; window.dadosPendents = p1;
	 * 
	 * var pendencias = null; pendencias = p1[0]['y'] + p1[1]['y'];
	 * $("#totalPendencia").html( "<hr><center><h5>Total de contratos: " +
	 * pendencias + "</h5></center><hr>");
	 */

	$('#contratos-pendentes').highcharts({
		chart : {
			type : 'pie',
			options3d : {
				enabled : true,
				alpha : 45,
				beta : 0
			}
		},
		title : {
			text : null
		},
		exporting : {
			enabled : false
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				depth : 35,
				dataLabels : {
					enabled : true,
					format : '{point.name}'
				}
			}
		},
		tooltip : {
			pointFormat : '<b>{point.percentage:.1f}%</b>'
		},
		series : [ {
			name : null,
			data : p1
		} ]
	});
}

function retornaMes(numero_do_mes) {

	switch (numero_do_mes) {
	case 1:
		numero_do_mes = "Janeiro";
		break;

	case 2:
		numero_do_mes = "Fevereiro";
		break;

	case 3:
		numero_do_mes = "Março";
		break;

	case 4:
		numero_do_mes = "Abril";
		break;

	case 5:
		numero_do_mes = "Maio";
		break;

	case 6:
		numero_do_mes = "Junho";
		break;

	case 7:
		numero_do_mes = "Julho";
		break;

	case 8:
		numero_do_mes = "Agosto";
		break;

	case 9:
		numero_do_mes = "Setembro";
		break;

	case 10:
		numero_do_mes = "Outubro";
		break;

	case 11:
		numero_do_mes = "Novembro";
		break;

	case 12:
		numero_do_mes = "Dezembro";
		break;

	}

	return numero_do_mes;

}

function formatReal(int) {
	var value = null;
	value = int.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	return value;

}

