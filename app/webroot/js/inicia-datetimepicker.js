  $(function() {
    $('.datetimepicker').datetimepicker({
      language: 'pt-BR',
      pickTime: false
    });

      $('.datetimepickerMaxHoje').datetimepicker({
          language: 'pt-BR',
          pickTime: false
      });
    
    $('.datetimepickerDtInicial').datetimepicker({
      language: 'pt-BR',
      pickTime: false
    }).on('changeDate', function(ev){
        calculaDtFimContrato( $( "#ContratoDtIniVigencia" ).val() );
    });
    
    $('.datetimepickerDtEntregaOficial').datetimepicker({
      language: 'pt-BR',
      pickTime: false
    }).on('changeDate', function(ev){
        calculaDtRecebimentoEntrega( $("#EntregaDtEntregaOficial").val(), $("#EntregaPzRecebimento").val() );
    });
    
    $('.datetimepickerDtNota').datetimepicker({
      language: 'pt-BR',
      pickTime: false
    }).on('changeDate', function(ev){
        checkDate();
    });
    
  });
