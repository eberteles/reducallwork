/* ===================================================
 * BRUNO SOARES ARAUJO
 * 27/06/2013
 * ========================================================== */

 //COMPONENTE TOOLTIP
 $(function() {
    
    /* COMPONENTES DO SISTEMA*/
	/* CADASTRE-SE LOGIN*/
    $('.bt-cadastrese').on('click', function() {
        $('#modal-login').modal('hide');
        $('#modal-cadastrese').modal('show');
    });
    $('.bt-ok').on('click', function() {
        $('#modal-login').modal('show');
        $('#modal-sucesso').modal('hide');
    });
    $('.bt-concluir').on('click', function() {
        $('#modal-cadastrese').modal('hide');
        $('#modal-sucesso').modal('show');
    });
    /* DATEPIKER */
    $('.datetimepicker').datetimepicker({
      pickTime: false
    });
    //data-format="dd/MM/yyyy hh:mm:ss"
    $('.datetimepicker-horas').datetimepicker({
      pickTime: true
    });

    //Mensagens
    $('.bt-salvar').on('click', function() {
        $('.mensagem-sucesso').show();
		$('.mensagem-sucesso-conclusao').hide();
        $('html,body').animate({scrollTop: $('.topo').offset().top}, 1000);
    });
	$('.bt-concluir').on('click', function() {
        $('.mensagem-sucesso-conclusao').show();
		$('.mensagem-sucesso').hide();
        $('html,body').animate({scrollTop: $('.topo').offset().top}, 1000);
    });
    // Mais informações
    $('.bt-fechar').on('click', function() {
        $('.accordion-toggle').hide();
    });
	


    /*TOOLTIP*/
    $('.alert-tooltip').tooltip();

    /* FIM COMPONENTES DO SISTEMA*/

    /* BOTAO VOLTAR PARA O TOPO*/

    $('.irtopo').on('click', function() {
        $('html,body').animate({scrollTop: $('.topo').offset().top}, 1000);
    });

    /*NAVEGAÇÃO DE TELAS*/

    $('.bt-pesquisar').on('click', function() {
        $('.tabela-resultado').show();
        $('html,body').animate({scrollTop: $('.tabela-resultado').offset().top}, 1000);
    });


    
    
});
