function carregaArvore(dados) {
    var treeView = $('#tree').treeview({
        levels: 1,
        showTags: true,
        highlightSelected: false,
        showCheckbox: true,
        onNodeChecked: function (event, node) {
            treeView.treeview('uncheckAll', {silent: true});
            treeView.treeview('checkNode', [node.nodeId, {silent: true}]);
            $('#btn-remover-processo').attr('disabled', false);
        },
        onNodeUnchecked: function (event, node) {
            $('#btn-remover-processo').attr('disabled', !treeView.treeview('getChecked').length);
        },
        data: dados
    });

    var search = function (e) {
        var pattern = $('#input-search').val();
        var options = {
            ignoreCase: true,
            revealResults: true
        };
        treeView.treeview('search', [pattern, options]);
    };

    $('#input-search').on('keyup', search);
};

function detalharAndamento(self) {
    var historico = $(self).data('historico');
    var tableRef = document.getElementById('tabela-historico').getElementsByTagName('tbody')[0];
    historico.forEach(function (andamento) {
        var novaLinha = tableRef.insertRow(tableRef.rows.length);
        novaLinha.insertCell(0).appendChild(document.createTextNode(andamento.descricao));
        novaLinha.insertCell(0).appendChild(document.createTextNode(andamento.unidade));
        novaLinha.insertCell(0).appendChild(document.createTextNode(andamento.data));
    });
    $("#modal").dialog({
        minWidth: $(window).width() - 100,
        minHeight: $(window).height() - 100
    });
}
;

function excluirProcesso() {
    var elementosSelecionados = treeView.treeview('getChecked');
    var mensagemDeConfirmacao = 'Tem certeza de que deseja excluir este registro? Esta operação ';
    mensagemDeConfirmacao += 'será registrada nos logs e este item será removido permanentemente';
    if (confirm(mensagemDeConfirmacao)) {
        $('#form-delete').attr('action', '/sei/delete_processo/' + elementosSelecionados[0].id + '/' + $('#id-contrato').val());
        $('#form-delete').submit();
    }
    ;
}
;

function carregaDocumento(url) {
    $('#btn-visualiza-tela-cheia').show();
    $('#preview-documento').attr('data', url);
    $('#visualiza-documento').attr('data', url);
}

function visualizaDocumento() {
    $('#modal-visualiza-documento').dialog({
        minWidth: $(window).width() - 100,
        minHeight: $(window).height() - 100
    });
}