$(document).ready(function () {
    $('.alert-tooltip').tooltip();

    var usuarioResponsavel = [];
    // tabela atividades
    var table = $('#tabelaExecucao').DataTable({
        ordering: false,
        sorting: false,
        pageLength: 10,
        info: false,
        paging: true,
        bLengthChange: true,
        pagingType: 'simple_numbers',
        language: {
            lengthMenu: "Exibir _MENU_ registros",
            zeroRecords: "Nenhum resultado encontrado",
            infoEmpty: "",
            infoFiltered: "",
            search: '<strong>Pesquisar </strong>:',
            paginate: {
                previous: '‹‹ Anterior',
                next: 'Proximo ››',
            }
        },
        columns: [
            // atividades pais
            {
                "data": "Atividade.nome", render: function (data, type, row, meta) {
                // adicionar o icone de atividades filhas
                if (row.children.length) {
                    data = '<i id="' + row.Atividade.co_atividade + '" style="cursor:pointer" class="parent icon-chevron-right"></i> ' + data;
                }
                return data;
            }
            },
            {"data": "Atividade.periodicidade"},
            {
                "data": "Usuario.ds_nome", render: function (data, type, row) {
                // adicionar o icone de status da atividade
                iconeResponsavel = getIconeAtividade(row.Atividade.tp_andamento, row.Atividade.co_atividade);
                return iconeResponsavel + ' ' + data;
            }
            },
            {"data": "Atividade.dt_ini_planejado"},
            {"data": "Atividade.dt_fim_planejado"},
            {"data": "Atividade.dt_ini_execucao"},
            {"data": "Atividade.dt_fim_execucao"},
            {"data": "Atividade.termino"},
            {"data": "Atividade.pc_executado"},
            {
                "data": "acoes", className: "parentAction", render: function (data, type, row) {
                // icones da ações
                return printIconesAtividade(row.Atividade);
            }
            }
        ],
        ajax: {
            url: baseUrlAtividadesController + '/index/' + $('#idContrato').val()
        },
        fnDrawCallback: function () {
            // adidicionar o filtro de atividades
            $('#tabelaExecucao_filter').css('text-align', 'right');
            $('#tabelaExecucao_length').removeClass('span6').addClass('span6').css('margin-left', '4px');
            $('#tabelaExecucao_length').after($('#filtro'));
        },
        createdRow: function (row, data, dataIndex) {
            // acrescentar a cor, nas atividades pais
            $(row).addClass(data.Atividade.alerta);

            // adicionar editar pct para uma atividade pai que não pode ter filhas, e que já está iniciada
            // if (data.Atividade.tp_andamento != 'N' && data.children.length < 1) {
            //     $(row)
            //     .find('td').eq(8)
            //     //.addClass('modalEditarPercentual')
            //     .attr('data-id', data.Atividade.co_atividade);
            // }
        }
    });

    table.on('page.dt', function () {
        $(this).find('tbody tr')
            .removeClass('loaded')
            .find('i.parent')
            .removeClass('icon-chevron-down')
            .addClass('icon-chevron-right');
    });

    table.on('length.dt', function (e, settings, len) {
        $(this).find('tbody tr')
            .removeClass('loaded')
            .find('i.parent')
            .removeClass('icon-chevron-down')
            .addClass('icon-chevron-right');
    });

    table.on('search.dt', function () {
        $(this).find('tbody tr')
            .removeClass('loaded')
            .find('i.parent')
            .removeClass('icon-chevron-down')
            .addClass('icon-chevron-right');
    });

    // on loaded
    table.on('xhr', function () {
        // var parents = table.ajax.json().data,
        // icones = '',
        // iconeResponsavel = '';

        $('.gifLoadAtv-small').hide();

        // for (var i = 0, len = parents.length; i < len; i++) {
        //     icones = printIconesAtividade(parents[i].Atividade.pendencia, parents[i]),

        //     iconeResponsavel = getIconeAtividade(parents[i].Atividade.tp_andamento, parents[i].Atividade.co_atividade);

        //     parents[i].Usuario.ds_nome =  iconeResponsavel + ' ' + parents[i].Usuario.ds_nome;

        //     // coluna de ações
        //     parents[i].acoes = icones;

        //     setTimeout(function (parents, i) {
        //         $('#tabelaExecucao tbody td.parentAction a[data-id=' + parents[i].Atividade.co_atividade + ']')
        //         .eq(0)
        //         .closest('tr').addClass(parents[i].Atividade.alerta);
        //     }, 4000, parents, i );

        //     // se a atividade pai tem fillhas
        //     if (parents[i].children.length) {
        //         parents[i].Atividade.nome = '<i id="' + parents[i].Atividade.co_atividade + '" style="cursor:pointer" class="parent icon-chevron-right"></i> ' + parents[i].Atividade.nome;
        //     }
        // }

    });

    // mostrar/esconder atividades filhas
    $('#tabelaExecucao tbody').on('click', 'i.parent', function () {
        var self = $(this),
            tr = self.closest('tr'),
            row = table.row(tr),
            data = row.data(),
            parentId = self.attr('id');

        if (!tr.hasClass('loaded')) {
            self.removeClass('icon-chevron-right')
                .addClass('icon-chevron-down');
            tr.addClass('loaded')
                .addClass('shown');

            for (var i = 0, len = data.children.length; i < len; i++) {
                // adicionar as atividades filhas
                addChildRow(table, data.children[i], parentId, tr);
            }
        } else {
            if (tr.hasClass('shown')) {
                self.addClass('icon-chevron-right')
                    .removeClass('icon-chevron-down');
                tr.removeClass('shown');
                $('#tabelaExecucao tbody tr#parent-id-' + parentId).hide();
            } else {
                self.removeClass('icon-chevron-right')
                    .addClass('icon-chevron-down');
                tr.addClass('shown');
                $('#tabelaExecucao tbody tr#parent-id-' + parentId).show();
            }
        }
    });

    $('#tabelaExecucao tbody tr.children td.tdResponsavel').css('text-align', 'right');

    // deletar pendencia
    $('#tabelaExecucao tbody').on('click', 'a.del_pendencia', function (e) {
        e.preventDefault();
        if (!confirm('Tem certeza de que deseja remover a pendência desta atividade?')) {
            return false;
        } else { // Remover Pendência da Atividade
            var url_del = baseUrlAtividadesController + "/removePendencia/" + $(this).data('id') + '/' + $('#idContrato').val();
            $('#modalLoader').modal('show');
            $.ajax({
                type: "POST",
                url: url_del,
                success: function (result) {
                    table.ajax.reload(function () {
                        $('#modalLoader').modal('hide');
                    }, false);
                }
            })
        }
    })

    // aplicar pendencia
    $('#tabelaExecucao tbody').on('click', 'a.apl_pendencia', function (e) {
        e.preventDefault();
        if (!confirm('Tem certeza de que deseja aplicar uma pendência a esta atividade?')) {
            return false;
        } else { // Aplicar Pendência à Atividade
            var url_del = baseUrlAtividadesController + "/aplicarPendencia/" + $(this).data('id') + '/' + $('#idContrato').val();
            $('#modalLoader').modal('show');
            $.ajax({
                type: "POST",
                url: url_del,
                success: function (result) {
                    table.ajax.reload(function () {
                        $('#modalLoader').modal('hide');
                    }, false);
                }
            })
        }
    });

    // iniciar atividade
    $('#modalIniAtividade').on('hidden.bs.modal', function () {
        $('#ok-ini-atividade').unbind('click').off('click');
    });
    $('#tabelaExecucao tbody').on('click', 'a.ini_atividade', function (e) {
        e.preventDefault();
        var me = $(this),
            tr = me.closest('tr'),
            coAtividade = me.attr('data-id'),
            parentId = tr.attr('id') ? tr.attr('id').substring(10) : 0,
            coContrato = $('#idContrato').val(),
            fimVigenciaContrato = $('#dt_fim_vigencia_atividade').val();

        $('#modalIniAtividade').modal('show');


        $('#ok-ini-atividade').on('click', function (e) {
            e.preventDefault();
            var url_ini = baseUrlAtividadesController + "/iniciar_atividade_botao";

            $('#modalLoader').modal('show');
            $.ajax({
                type: "POST",
                url: url_ini,
                dataType: 'json',
                data: {
                    "data[coAtividade]": coAtividade,
                    "data[parentId]": parentId,
                    "data[coContrato]": coContrato,
                    "data[fimVigenciaContrato]": fimVigenciaContrato
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        $('#modalLoader').modal('hide');
                        // if (tr.hasClass('children')) {
                        //     $('#tabelaExecucao tbody tr td i#' + parentId).click();
                        //     tr.css('border', '2px solid #000');
                        // }
                    }, false);

                }, error: function (error) {
                    console.error(error);
                }
            });

            $('#modalIniAtividade').modal('hide');
        });
    });

    // encerrar atividade
    $('#modalEncAtividade').on('hidden.bs.modal', function () {
        $('#ok-encerrar-atividade').unbind('click').off('click');
    });
    $('#tabelaExecucao tbody').on('click', 'a.enc_atividade', function (e) {
        e.preventDefault();
        var me = $(this),
            tr = me.closest('tr'),
            coAtividade = me.attr('data-id'),
            parentId = tr.attr('id') ? tr.attr('id').substring(10) : 0,
            coContrato = $('#idContrato').val(),
            fimVigenciaContrato = $('#dt_fim_vigencia_atividade').val();

        $('#modalEncAtividade').modal('show');

        $('#ok-encerrar-atividade').on('click', function (e) {
            e.preventDefault();
            var url_ini = baseUrlAtividadesController + "/encerrar_atividade/" + coContrato;

            $('#modalLoader').modal('show');
            $.ajax({
                type: "POST",
                url: url_ini,
                dataType: 'json',
                data: {
                    "data[coAtividade]": coAtividade,
                    "data[parentId]": parentId,
                    "data[coContrato]": coContrato,
                    "data[fimVigenciaContrato]": fimVigenciaContrato
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        $('#modalLoader').modal('hide');
                    }, false);

                }, error: function (error) {
                    console.error(error);
                }
            });

            $('#modalEncAtividade').modal('hide');
        });
    });


    // deletar atividade
    $('#modalDelAtividade').on('hidden.bs.modal', function () {
        $('#ok-del-atividade').unbind('click').off('click');
    });
    $('#tabelaExecucao tbody').on('click', 'a.del_atividade', function (e) {
        $('.parentMsg').hide();
        $('#modalDelAtividade .modal-footer').hide();
        e.preventDefault();
        var me = $(this),
            tr = me.closest('tr'),
            coAtividade = me.attr('data-id'),
            parentId = tr.attr('id') ? tr.attr('id').substring(10) : 0,
            coContrato = $('#idContrato').val();

        $.ajax({
            type: "POST",
            url: baseUrlAtividadesController + "/findParent",
            data: {"data[coAtividade]": coAtividade},
            success: function (result) {
                $('#modalDelAtividade .modal-footer').show();
                result = JSON.parse(result);
                if (result.data != '') {
                    $('.parentMsg').show();
                }
            }, error: function (err) {
                console.log('error :', err);
            }
        });

        $('#modalDelAtividade').modal('show');

        $('#ok-del-atividade').on('click', function (e) {
            e.preventDefault();
            var url_ini = baseUrlAtividadesController + "/delete_atividade";

            $('#modalLoader').modal('show');

            $.ajax({
                type: "POST",
                url: url_ini,
                // dataType: 'json',
                data: {
                    "data[coAtividade]": coAtividade,
                    "data[parentId]": parentId,
                    "data[coContrato]": coContrato
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        $('#modalLoader').modal('hide');
                    }, false);

                }, error: function (error) {
                    console.error(error);
                }
            });

            $('#modalDelAtividade').modal('hide');
        });
    });


    // modal editar percentual
    $('#editar-porcentagem').on('hidden.bs.modal', function () {
        $('#ok-editar-porcentagem').unbind('click').off('click');
    });
    $("#tabelaExecucao tbody ").on('click', 'a.modalEditarPercentual', function (e) {
        $('#editar-porcentagem').modal();

        $('#editar-porcentagem').on('hidden.bs.modal', function () {
            $('#message_pendencia_required').hide();
        });

        var self = $(this),
            coAtividade = self.data('id'),
            target = $(e.target),
            porcentagem = $.trim(self.attr('data-pct'));
        tr = $(this).closest('tr'),
            parentId = tr.attr('id') ? tr.attr('id').substring(10) : 0;
        // porcentagem = porcentagem.substring(0, porcentagem.length - 1);

        // if ($('#ini' + coAtividade + ' a > i').hasClass('ui-icon-control-play-blue')) {
        //     $('#modalAlertaIniAtividade').modal('show');
        // }

        if (porcentagem == 100) {
            $('#atvPendencia').prop('disabled', true);
        } else {
            $('#atvPendencia').prop('disabled', false);
        }

        $("#atvPorcentagem").on('change', function (e) {
            if ($(this).val() == 100) {
                $('#atvPendencia').prop('disabled', true);
            } else {
                $('#atvPendencia').prop('disabled', false);
            }
        });

        $('#atvPorcentagem option[value="' + porcentagem + '"]').prop('selected', true);
        $('#atvPendencia').val($(e.target).attr('pendencia'));

        $("#ok-editar-porcentagem").on('click', function () {
            if ($('#atvPorcentagem').val() < 100 && $('#atvPendencia').val() == '' || $('#atvPendencia').val() == ' ') {
                $('#message_pendencia_required').show();
                return;
            } else {
                $('#modalLoader').modal('show');
                $.ajax({
                    url: baseUrlAtividadesController + '/editar_pct/' + $('#idContrato').val(),
                    method: 'POST',
                    // dataType: 'json'
                    data: {
                        "data[co_atividade]": coAtividade,
                        "data[pc_executado]": $('#atvPorcentagem').val(),
                        "data[pendencia]": $('#atvPorcentagem').val() == 100 ? null : $('#atvPendencia').val()
                    }
                }).success(function (data) {
                    table.ajax.reload(function () {
                        // $('#add-atividade').modal('hide');
                        setTimeout(function () {
                            $('#modalLoader').modal('hide')
                        }, 500);
                    }, false);
                });

            }

            $('#editar-porcentagem').modal('hide');
        });
    });

    $('#selAtvs').on('change', function () {
        var trNodes = $('#tabelaExecucao');
        $('.gifLoadAtv-small').show();
        if ($(this).val() == 'N') {
            // $('#tabelaExecucao').DataTable().column(6).search('^(?!100%)\(\).*$', true, true).draw();
            loadAtividades('N');
        } else if ($(this).val() == 'F') {
            // $('#tabelaExecucao').DataTable().column(6).search('100%', false, false).draw();
            loadAtividades('F');
        } else if ($(this).val() == 'T') {
            // $('#tabelaExecucao').DataTable().column(6).search('').draw();
            loadAtividades('T');
        } else if ($(this).val() == 'C') {
            // $('#tabelaExecucao').DataTable().column(6).search('').draw();
            loadAtividades('C');
        }
        // trNodes.treegrid('collapseAll');
    });

    // modal email
    $('#tabelaExecucao tbody').on('click', 'a.view_email', function (e) {
        e.preventDefault();
        var url_pg = baseUrlAtividadesController + "/iframe/" + $('#idContrato').val() + "/" + $(this).data('id');
        $.ajax({
            type: "POST",
            url: url_pg,
            success: function (result) {
                $('#add_email').html("");
                $('#add_email').append(result);
            }
        })
    });

    // modal financeiro
    $('#tabelaExecucao tbody').on('click', 'a.view_financeiro', function () {
        var url_pg = baseUrlPagamentosController + "/iframe/" + $('#idContrato').val() + "/" + $(this).data('id');
        $.ajax({
            type: "POST",
            url: url_pg,
            data: {},
            success: function (result) {
                $('#atv_financeiro').html("");
                $('#atv_financeiro').append(result);
            }
        })
    });

    // modal anexo
    $('#tabelaExecucao tbody').on('click', 'a.view_anexo', function () {
        var url_pg = $('#base_url_atv').val() + "/anexos/iframe/" + $('#idContrato').val() + "/atividade/" + $(this).data('tr');
        $.ajax({
            type: "POST",
            url: url_pg,
            data: {},
            success: function (result) {
                $('#atv_anexo').html("");
                $('#atv_anexo').append(result);
            }
        })
    });


    // modal atesto
    $('.v_atesto').click(function (event) {
        event.preventDefault();
        var self = $(this),
            coAtividade = self.data('id'),
            atestoId = '#atesto_' + coAtividade,
            atvAtestoId = '#atividade_atesto_' + coAtividade;

        $.ajax({
            url: baseUrlAtividadesController + '/atestarAtividade/' + coAtividade,
            method: 'POST',
            data: {}
        }).success(function (data) {
            $(atestoId).remove();
            $(atvAtestoId).append("<i class='icon-certificate alert-tooltip' title='Atividade Atestada' style='display: inline-block;'></i>");
        });
    });

    /***** Validações de campos ******/
    // descrição da atividade
    $('.ds_atividade').on('input blur', function () {
        var tamanho = $('.ds_atividade').val().length;
        if (tamanho < 3 || tamanho > 100) {
            $('.message_atividade').show();
        } else {
            $('.message_atividade').hide();
            $('.message_atividade2').hide();
        }

        if ($('.ds_atividade').val() == '' || $('.ds_atividade').val() == ' ') {
            $('.message_atividade2').show();
        } else {
            $('.message_atividade2').hide();
        }
    });

    $('#dt_ini_planejado + span').on('click', function () {
        $('#dt_ini_planejado').focus();
    });

    // data de inicio da atividade
    $('#dt_ini_planejado').on('blur', function () {
        var data_ini = $('#dt_ini_planejado').val();
        if (data_ini == "" || data_ini == " " || data_ini == '__/__/____') {
            $('#message_dt_inicio_atividade').show();
            $('#add_concluir').prop('disabled', true);
        } else {
            $('#add_concluir').prop('disabled', false);
            $('#message_dt_inicio_atividade').hide();

            var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
            var dataInicioAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');
            var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');

            if (dataInicioAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                $('#message_dt_inicio_atividade2').show();
                $('#add_concluir').prop('disabled', true);

                if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                    $('#add_concluir').prop('disabled', true);
                    $('#message_dt_fim_atividade').show();
                }
                return;
            } else {
                $('#message_dt_inicio_atividade2').hide();
                $('#add_concluir').prop('disabled', false);
            }

            if (dataFimAtividade.diff(dataInicioAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
                $('#message_dt_fim_atividade2').show();
                $('#add_concluir').prop('disabled', true);
            } else {
                $('#add_concluir').prop('disabled', false);
                $('#message_dt_fim_atividade2').hide();
            }
        }
    });

    $('#dt_fim_planejado + span').on('click', function () {
        $('#dt_fim_planejado').focus();
    });

    // data do fim da atividade
    $("#dt_fim_planejado").on('blur', function () {
        var data_fim = $('#dt_fim_planejado').val();

        // if (data_fim == "" || data_fim == " " || data_fim == '__/__/____')  {
        //       $('#message_dt_fim_atividade0').show();
        //       $('#add_concluir').prop('disabled', true);
        // } else {
        //       $('#add_concluir').prop('disabled', false);
        //       $('#message_dt_fim_atividade0').hide();
        // }

        if (data_fim != "" || data_fim != " " || data_fim != '__/__/____') {
            var dataVigenciaContrato = moment($('#dt_fim_vigencia_atividade').val(), 'DD/MM/YYYY');
            var dataFimAtividade = moment($('#dt_fim_planejado').val(), 'DD/MM/YYYY');
            var dataIniAtividade = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');


            if (dataFimAtividade.diff(dataVigenciaContrato, 'days') > 0) {
                $('#message_dt_fim_atividade').show();
                $('#add_concluir').prop('disabled', true);
                return;
            } else {
                $('#message_dt_fim_atividade').hide();
                $('#add_concluir').prop('disabled', false);
            }

            if (dataFimAtividade.diff(dataIniAtividade, 'days') < 0 || $('#dt_ini_planejado').val() == '') {
                $('#message_dt_fim_atividade2').show();
                $('#add_concluir').prop('disabled', true);
            } else {
                $('#message_dt_fim_atividade2').hide();
                $('#add_concluir').prop('disabled', false);
            }
        }
    });

    // vincular atividade
    $('#parent_id').on('input', function () {
        if ($(this).val()) {
            $('#message_dia_execucao_atividade').hide();
            $('#message_periodicidade_atividade').hide();
            $("#dia_execucao").prop('disabled', true);
            $("#periodicidade").prop('disabled', true);
        } else {
            var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
            $("#periodicidade").prop('disabled', false);
            if (arr.indexOf($('#periodicidade').val()) > -1) {
                $("#dia_execucao").prop('disabled', true);

            } else {
                $("#dia_execucao").prop('disabled', false);
            }
        }

    });

    // periodicidade
    $("#periodicidade").on('input', function () {
        var arr = ['Diariamente', 'Quando houver necessidade', 'Na solicitação'];
        if (arr.indexOf($('#periodicidade').val()) != -1) {
            $('#message_dia_execucao_atividade').hide();
            $('#dia_execucao').prop('disabled', true);
            $('#dia_execucao option:first').prop('selected', true);
        } else {
            $('#dia_execucao').prop('disabled', false);
        }

        if ($(this).val() == '') {
            $('#message_periodicidade_atividade').show();
        } else {
            $('#message_periodicidade_atividade').hide();
        }
    });

    // dia execução
    $('#dia_execucao').on('input', function () {
        if ($(this).val() == '') {
            $('#message_dia_execucao_atividade').show();
        } else {
            $('#message_dia_execucao_atividade').hide();
        }
    });

    // nome do responsável
    $('.co_responsavel').on('input', function () {
        if ($('.co_responsavel').val() == '' || $('.co_responsavel').val() == '') {
            $('.message_co_responsavel_atividade').show();
        } else {
            $('.message_co_responsavel_atividade').hide();
        }
    });

    // função responsável
    $('.funcao_responsavel').on('input', function () {
        if ($('.funcao_responsavel').val() == '' || $('.funcao_responsavel').val() == '') {
            $('.add_concluir').attr('disabled', 'disabled');
            $('.message_funcaoResponsavel').show();
        } else {
            $('.message_funcaoResponsavel').hide();
        }
    });

    $('.co_responsavel').change(function () {
        var selectFuncaoResponsaveis = $('#funcao_responsavel');

        $.each(usuarioResponsavel, function (coUsuario, responsavel) {
            if (responsavel.co_usuario == $('.co_responsavel').val()) {
                selectFuncaoResponsaveis.append(
                    '<option selected value="' + responsavel.responsavel.co_fiscais_tipos + '">' +
                    responsavel.responsavel.no_fiscais_tipos +
                    '</option>');
            }
        });
    })
    // nome atividade
    $('.nomeAtividade').on('input blur', function () {
        if ($('.nomeAtividade').val() == '' || $('.nomeAtividade').val() == '') {
            $('.message_nome_atividade').show();
        } else {
            $('.message_nome_atividade').hide();
        }
    });

    // on hide modal add atividade, modal editar atividade
    $('#add-atividade, #editar-atividade').on('hidden.bs.modal', function (e) {
        $(this).find("input,textarea,select").val('');
        $('#add_concluir').prop('disabled', false);
        $('.messageErrorAtv').hide();
    });

    // editar atividade
    $('#btnEditar').on('click', function () {
        $('#detalhar-atividade').modal('hide');
        $('#editar-atividade').modal('show');
        $.ajax({
            url: baseUrlAtividadesController + '/editar_atividade/' + $('#btnEditar').attr('coAtividade') + '/' + $('#idContrato').val(),
            method: 'GET',
            dataType: 'json'
        })
            .done(function (resp) {
                var optionResponsaveis = '',
                    optionFuncoes = '',
                    optionPeridiocidade = '',
                    optionDiaExecucao = '',
                    funcoes = [
                        'Gestor',
                        'Aux. Fiscal Técnico',
                        'Fiscal Requisitante',
                        'Fiscal Técnico',
                        'Fiscal Administrativo',
                        'Aux. Fiscal Administrativo'
                    ],
                    periodicidades = [
                        'Diariamente',
                        'Semanal',
                        'Quinzenal',
                        'Mensal',
                        'Bimestral',
                        'Trimestral',
                        'Semestral',
                        'Anual',
                        'Quando houver necessidade',
                        'Na solicitação'
                    ],
                    dia_execucao = [
                        'Segunda',
                        'Terça',
                        'Quarta',
                        'Quinta',
                        'Sexta',
                        'Quando Possível'
                    ];

                $(".editAtvResp option").remove();
                $(".editAtvVinc option").remove();
                $(".editAtvFunc option").remove();

                // select com os responsaveis
                $.each(resp.responsaveis, function (coUsuario, responsavel) {
                    if (coUsuario == resp.atividade.Atividade.co_responsavel) {
                        optionResponsaveis = '<option selected value="' + coUsuario + '">' + responsavel + '</option>';
                    } else {
                        optionResponsaveis = '<option value="' + coUsuario + '">' + responsavel + '</option>';
                    }

                    $(".editAtvResp").append(optionResponsaveis);
                });

                // select com as atividades pais
                var countVinc = false;
                var optionVinc = '';
                if ($('#tabelaExecucao tbody tr td i#' + $('#btnEditar').attr('coAtividade')).length == 0) {
                    $(".editAtvVinc").attr('disabled', false);
                    $(".editAtvVinc").toggleClass('disabled');

                    $(".editAtvVinc").append('<option value="0">Selecione...</option>');
                    $.each(resp.atividadesPai, function (parentId, atividadePai) {
                        if (resp.atividade.Atividade.parent_id == parentId) {
                            optionVinc = '<option  selected value="' + parentId + '">' + atividadePai + '</option>';
                            countVinc = true;
                        } else {
                            optionVinc = '<option  value="' + parentId + '">' + atividadePai + '</option>';
                        }

                        $(".editAtvVinc").append(optionVinc);
                    });

                    if (!countVinc) {
                        $(".editAtvVinc option:first").attr('selected', true)
                    }
                } else {
                    $(".editAtvVinc").attr('disabled', true);
                    $(".editAtvVinc").toggleClass('disabled');
                }

                // select funcao_responsavel
                $.each(funcoes, function (i, value) {
                    if (value == resp.atividade.Atividade.funcao_responsavel) {
                        optionFuncoes = '<option  selected value="' + value + '">' + value + '</option>';
                    } else {
                        optionFuncoes = '<option  value="' + value + '">' + value + '</option>';
                    }

                    $(".editAtvFunc").append(optionFuncoes);
                });

                $(".editPeriodicidade").html('<option>Selecione</option>');
                // select peridiocidade
                $.each(periodicidades, function (i, value) {
                    if (value == resp.atividade.Atividade.periodicidade) {
                        optionPeridiocidade = '<option  selected value="' + value + '">' + value + '</option>';
                    } else {
                        optionPeridiocidade = '<option  value="' + value + '">' + value + '</option>';
                    }

                    $(".editPeriodicidade").append(optionPeridiocidade);
                });
                //selectPeridiocidade

                $(".editDiaExecucao").html('<option>Selecione</option>');
                // select diaExecucao
                $.each(dia_execucao, function (i, value) {
                    if (value == resp.atividade.Atividade.dia_execucao) {
                        optionDiaExecucao = '<option  selected value="' + value + '">' + value + '</option>';
                    } else {
                        optionDiaExecucao = '<option  value="' + value + '">' + value + '</option>';
                    }

                    $(".editDiaExecucao").append(optionDiaExecucao);
                });
                //selectDiaExecucao

                $('#gifAtv').addClass('hideGif').hide();

                $('#showEditar').show();

                $('#editNome').val(resp.atividade.Atividade.nome);

                $('#editDesc').val(resp.atividade.Atividade.ds_atividade);

                $('#edit_dt_ini').val(resp.atividade.Atividade.dt_ini_planejado);

                $('#edit_dt_fim').val(resp.atividade.Atividade.dt_fim_planejado);
            });

    });

    // detalhar atividade
    $(document).on('click', '.modal-detalharAtividade', function () {
        var self = $(this);
        var coAtividade = $(this).data('id');
        var coContrato = $('#idContrato').val();

        $('#editar-concluir').attr('coAtividade', coAtividade);
        $('#btnEditar').attr('coAtividade', coAtividade);

        $('#gifAtv').hasClass('hideGif') ? $('#gifAtv').hide() : $('#gifAtv').show();

        $.ajax({
            url: baseUrlAtividadesController + '/detalhar_atividade/' + coAtividade + '/' + coContrato,
            method: 'GET',
            dataType: 'json'
        })
            .done(function (resp) {
                $('#gifAtv').addClass('hideGif').hide();
                $('#showDetalhes').show();
                $('.detalheAtvResponsavel').html(resp[0].Usuario.ds_nome || '-');
                $('.detalheAtvFuncao').html(resp[0].Atividade.funcao_responsavel || '-');
                $('.detalheAtvNomeAtividade').html(resp[0].Atividade.nome);
                $('.detalheAtvDescricao').html(resp[0].Atividade.ds_atividade);
                $('#detalheAtvPeriodicidade').html(resp[0].Atividade.periodicidade || '-');
                $('#detalheAtvIniAtividade').html(resp[0].Atividade.dt_ini_planejado || '-');
                $('#detalheAtvFimAtividade').html(resp[0].Atividade.dt_fim_planejado || '-');
                $('#detalheAtvDiaExecucao').html(resp[0].Atividade.dia_execucao || '-');
            });
    });

    // Modal add atividade
    $(document).on('click', '#bt-add-atividade', function () {
        var selectResponsaveis = $('#co_responsavel'),
            optionResponsaveis = $('#co_responsavel option'),
            selectVincularAtividade = $('#parent_id'),
            optionVincularAtividade = $('#parent_id option');
        $.ajax({
            url: baseUrlAtividadesController + '/add_atividade/' + $('#idContrato').val(),
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#co_responsavel option').remove();
                $('#parent_id option').remove();
                selectResponsaveis.append('<option value>Selecione</option>');
                selectVincularAtividade.append('<option value>Selecione</option>');

                $.each(data.usuarios, function (coUsuario, responsavel) {
                    if (data.fiscais) {
                        usuarioResponsavel.push({'co_usuario': coUsuario, 'responsavel': responsavel});
                        selectResponsaveis.append('<option value="' + coUsuario + '">' + responsavel.ds_nome + '</option>');
                    } else {
                        selectResponsaveis.append('<option value="' + coUsuario + '">' + responsavel + '</option>');
                    }
                });

                $.each(data.atividadesPai, function (parentId, atividadePai) {
                    selectVincularAtividade.append('<option value="' + parentId + '">' + atividadePai + '</option>');
                });
            },
            error: function (error) {
                $('#co_responsavel option').remove();
                selectResponsaveis.append(optionResponsaveis);
                $('#parent_id option').remove();
                selectVincularAtividade.append(optionVincularAtividade);
            }
        });
    });

    // add atividade
    $('#add_concluir').click(function (e) {
        var validacao = false;

        // responsável
        if ($('.co_responsavel').val() == '' || $('.co_responsavel').val() == '') {
            $('.message_co_responsavel_atividade').show();
            validacao = true;
        }

        // função
        if ($('.funcao_responsavel').val() == '' || $('.funcao_responsavel').val() == ' ') {
            $('.message_funcaoResponsavel').show();
            validacao = true;
        }

        // nome atividade
        if ($('.nomeAtividade').val() == '') {
            $('.message_nome_atividade').show();
            validacao = true;
        }

        // descrição
        if (!$.trim($(".ds_atividade").val())) {
            $('.message_atividade2').show();
            validacao = true;
        }

        // inicio atividade
        if ($('#dt_ini_planejado').val() == '') {
            $('#message_dt_inicio_atividade').show();
            validacao = true;
        }

        if ($('#message_dt_fim_atividade').is(":visible") || $('#message_dt_inicio_atividade').is(":visible") || $('#message_dt_inicio_atividade2').is(":visible") || $('#message_dt_fim_atividade2').is(':visible')) {
            validacao = true;
        }

        // periodicidade
        if ($('#periodicidade').val() == '' && $('#periodicidade').prop('disabled') == false && !$('#parent_id').val()) {
            $('#message_periodicidade_atividade').show();
            validacao = true;
        }

        // dia execucao
        if ($('#dia_execucao').val() == '' && ($('#dia_execucao').prop('disabled') == false) && !$('#parent_id').val()) {
            $('#message_dia_execucao_atividade').show();
            // $('#message_periodicidade_atividade').show();
            validacao = true;
        }

        var iniPlanejado = moment($('#dt_ini_planejado').val(), 'DD/MM/YYYY');
        // var hoje = new Date();
        var dataFimPlanejado = $('#dt_fim_planejado').val().length ? $('#dt_fim_planejado').val() : $('#dt_fim_vigencia_atividade').val();
        var fimPlanejado = moment(dataFimPlanejado, 'DD/MM/YYYY');
        var periodicidde;
        if ($('#periodicidade').val() !== undefined) {
            switch ($('#periodicidade').val().toLowerCase()) {
                case 'diariamente':
                    periodicidade = 1;
                    break;
                case 'semanal':
                    periodicidade = 7;
                    break;
                case 'quinzenal':
                    periodicidade = 15;
                    break;
                case 'mensal':
                    periodicidade = 30;
                    break;
                case 'bimestral':
                    periodicidade = 60;
                    break;
                case 'trimestral':
                    periodicidade = 90;
                    break;
                case 'semestral':
                    periodicidade = 180;
                    break;
                case 'anual':
                    periodicidade = 365;
                    break;
            }
        }

        // hoje = moment(hoje.getDate() + '/' + (hoje.getMonth()+1) + '/' + hoje.getFullYear(), 'DD/MM/YYYY');
        // console.log('t: ', fimPlanejado.diff(iniPlanejado, 'days') / periodicidade);
        // return false;

        if ($('#dt_ini_planejado').val() && dataFimPlanejado && $('#periodicidade').val()) {
            if ((fimPlanejado.diff(iniPlanejado, 'days') / periodicidade) < 1 && $('#periodicidade').prop('disabled') == false && $('#dia_execucao').prop('disabled') == false) {
                validacao = true;
                $('#modalAlertPeriodicidade').modal('show');
            }

            // vinculo atividade
            if ($('#vincularAtv').val()) {
                // validacao = false;
                $('#message_dia_execucao_atividade').hide();
                $('#message_periodicidade_atividade').hide();
            }
        }

        // if ($('#dt_ini_planejado').val()) {
        //     if (hoje.diff(iniPlanejado, 'days') > 0) {
        //         validacao = true;
        //          $('#modalAlertDataInicio').modal('show');
        //     }
        // }

        // if($('#dt_fim_planejado').val() == '') {
        // $('#message_dt_fim_atividade0').show();
        //      validacao   = true;
        // }

        if (validacao) {
            return false;
        } else {
            if (dataFimPlanejado == '') {
                alert('Processo sem data fim!');
                return false;
            }

            // console.log($('#parent_id option:selected').val() > 0) {
            // $('#tabelaExecucao tbody tr i#' + $('#parent_id option:selected').closest('tr')).hide();
            // }
            $(this).prop('disabled', true);
            $('#modalLoader').modal();
            var url_add = baseUrlAtividadesController + "/add_atividade/" + $('#idContrato').val();
            // var dataFimPlanejado = $('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '' ?
            //     $('#dt_fim_vigencia_atividade').val() : $('#dt_fim_planejado').val();

            // console.log($('#dt_fim_planejado').val() == ' ' || $('#dt_fim_planejado').val() == '');
            // console.log($('#dt_fim_vigencia_atividade').val());
            // return;
            $.ajax({
                type: "POST",
                url: url_add,
                dataType: 'json',
                data: {
                    "data[Atividade][co_contrato]": $('#idContrato').val(),
                    "data[Atividade][parent_id]": $('#parent_id option:selected').val(),
                    "data[Atividade][ds_atividade]": $('#ds_atividade').val(),
                    "data[Atividade][co_responsavel]": $('#co_responsavel option:selected').val(),
                    "data[Atividade][funcao_responsavel]": $('#funcao_responsavel option:selected').val(),
                    "data[Atividade][dt_ini_planejado]": $('#dt_ini_planejado').val(),
                    "data[Atividade][dt_fim_planejado]": dataFimPlanejado,
                    "data[Atividade][tp_andamento]": "N",
                    "data[Atividade][nome]": $("#nomeAtividade").val(),
                    "data[UsuarioPerfil][co_perfil]": $("#co_perfil").val(),
                    "data[Atividade][periodicidade]": $('#parent_id option:selected').val() ? null : $("#periodicidade").val(),
                    "data[Atividade][dia_execucao]": $('#parent_id option:selected').val() ? null : $("#dia_execucao").val()
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        $(this).prop('disabled', false);
                        $('#add-atividade').modal('hide');
                        setTimeout(function () {
                            $('#modalLoader').modal('hide')
                        }, 500);
                    }, false);
                    $('#formAddAtividade').find("input,textarea,select").val('');
                    // $('#collapseExec').css('height', 'auto');
                }
            });
        }

    });


    // atualizar atividade
    $('#editar-concluir').on('click', function () {
        var validacao = false;
        $('#co_responsavel option').remove();

        // responsável
        if ($('#editAtvResp').val() == '' || $('#editAtvResp').val() == '') {
            $('.message_co_responsavel_atividade').show();
            validacao = true;
        }

        // função
        if ($('.editAtvFunc').val() == '' || $('.editAtvFunc').val() == ' ') {
            $('.message_funcaoResponsavel').show();
            validacao = true;
        }

        // nome atividade
        if ($('#editNome').val() == '') {
            $('.message_nome_atividade').show();
            validacao = true;
        }

        // descrição
        if (!$.trim($("#editDesc").val())) {
            $('.message_atividade2').show();
            validacao = true;
        }

        if (validacao) {
            return false;
        } else {
            var p = $('.editAtvVinc option:selected').length;
            $('#modalLoader').modal('show');

            $.ajax({
                type: "POST",
                url: baseUrlAtividadesController + '/editar_atividade/' + $('#btnEditar').attr('coAtividade') + '/' + $('#idContrato').val(),
                dataType: 'json',
                data: {
                    "data[Atividade][co_atividade]": $('#btnEditar').attr('coAtividade'),
                    "data[Atividade][parent_id]": $('.editAtvVinc option:selected').val(),
                    "data[Atividade][ds_atividade]": $('#editDesc').val(),
                    "data[Atividade][co_responsavel]": $('.editAtvResp option:selected').val(),
                    "data[Atividade][funcao_responsavel]": $('.editAtvFunc option:selected').val(),
                    "data[Atividade][nome]": $("#editNome").val(),
                    "data[Atividade][periodicidade]": $('.editPeriodicidade option:selected').val(),
                    "data[Atividade][dt_ini_planejado]": $('#edit_dt_ini').val(),
                    "data[Atividade][dt_fim_planejado]": $('#edit_dt_fim').val(),
                },
                success: function (result) {
                    $('.editAtvFunc option').removeAttr('selected');
                    $('#editar-atividade').modal('hide');
                    $('#formEditAtividade').find("input,textarea,select").val('');
                    table.ajax.reload(function () {
                        $(this).prop('disabled', false);
                        $('#editar-atividade').modal('hide');
                        setTimeout(function () {
                            $('#modalLoader').modal('hide')
                        }, 600);
                    }, false);
                }
            });
        }
    });
});

/******  funções *****/
// selecionar filtro
function selectFiltro(tpAndamento) {
    switch (tpAndamento) {
        case 'F':
            $('#conc').prop('selected', true);
            break;
        case 'N':
            $('#fazer').prop('selected', true);
            break;
        case 'C':
            $('#enc').prop('selected', true);
            break;
        case 'T':
            $('#todas').prop('selected', true);
        default:
            break;
    }
}

// carregar atividades
function loadAtividades(tpAndamento) {
    $('#tabelaExecucao').DataTable().ajax.url(baseUrlAtividadesController + '/index/' + $('#idContrato').val() + '/' + tpAndamento);
    $('#tabelaExecucao').DataTable().ajax.reload(function () {
        selectFiltro(tpAndamento);
    });

}

// titulo da atividade
function tituloAtividade(tpAndamento) {
    if (tpAndamento == "N") {
        return "Não Iniciada - Clique para iniciar a Atividade!";
    }
    if (tpAndamento == "E") {
        return "Em Andamento";
    }
    if (tpAndamento == "P") {
        return "Pendente";
    }
    if (tpAndamento == "F") {
        return "Finalizada";
    }
}

// icone da atividade
function getIconeAtividade(tpAndamento, coAtividade) {
    var icone = '<i class="alert-tooltip ',
        titulo = 'title="' + tituloAtividade(tpAndamento) + '" style="display: inline-block;"></i>';

    if (tpAndamento == "N") {
        icone += 'ui-icon-control-play-blue"' + titulo;
        icone = '<a data-id="' + coAtividade + '" class="ini_atividade" href="#">' + icone + '</a>';
    }
    if (tpAndamento == "E") {
        icone += 'ui-icon-hourglass"' + titulo;
    }
    if (tpAndamento == "P") {
        icone += 'silk-icon-flag-red"' + titulo;
    }
    if (tpAndamento == "F") {
        icone += 'silk-icon-accept"' + titulo;
    }
    return icone;
}

function printIconesAtividade(atividade) {
    var icones = [];
    // icone de pendente
    if (atividade.pendencia) {
        icones.push('<a id="atvPendenciaTooltip' + atividade.co_atividade + '" class="alert-tooltip" title="' + atividade.pendencia + '" style="text-decoration:none;color:orange;font-size: 16px;cursor:pointer"><i class="icon-exclamation-sign" style="cursor:normal;display: inline-block;"></i></a>');
    }

    // icone detalhe
    icones.push('<a href="#" style="text-decoration:none;color:#010;font-size: 20px;" data-id="' + atividade.co_atividade + '" class="modal-detalharAtividade" data-target="#detalhar-atividade" data-toggle="modal">\
        <i class="icon-eye-open alert-tooltip" title="Detalhar" style="display: inline-block;"></i></a>');

    // icone de pendência
    if (atividade.ic_pendente > 0) {
        icones.push('<a data-id="' + atividade.co_atividade + '" href="#" class="del_pendencia" ><i class="icon-exclamation-sign red bigger-150 alert-tooltip" title="Atividade Pendente (Clique para tirar a Pendência)" style="display: inline-block;"></i></a>');
    } else {
        icones.push('<a data-id="' + atividade.co_atividade + '" href="#apl_pendencia" class="apl_pendencia" ><i class="icon-ok-circle bigger-150 alert-tooltip" title="Atividade Normal (Clique para aplicar a Pendência)" style="display: inline-block;"></i></a>');
    }

    // icone financeiro
    icones.push('<a class="view_financeiro" href="#view_financeiro" data-toggle="modal" data-id="' + atividade.co_atividade + '">\
        <i class="silk-icon-calculator alert-tooltip" title="Orçamentário/Financeiro" style="display: inline-block;"></i></a>');

    // icone anexo
    icones.push('<a class="view_anexo" href="#view_anexo" data-toggle="modal" data-id="' + atividade.co_atividade + '">\
        <i class="silk-icon-folder-table alert-tooltip" title="Suporte Documental" style="display: inline-block;"></i></a>');

    // icone email
    icones.push('<a class="view_email" href="#view_email" data-toggle="modal" data-id="' + atividade.co_atividade + '">\
        <i class="icon-envelope icon-large alert-tooltip" title="Email" style="display: inline-block;color: #000;position: relative;top: -4px;"></i></a>');

    // icone andamento %
    // so adicionar o ícone de pct se a atividade já estiver iniciada
    if (atividade.tp_andamento != 'N') {
        icones.push('<a class="view_percent modalEditarPercentual" href="#view_percent" data-toggle="modal" data-id="' + atividade.co_atividade + '" data-pct="' + atividade.pc_executado + '">\
        <i class="icon-time icon-large alert-tooltip " title="Porcentagem do andamento." style="display: inline-block;color: #000;position: relative;top: -4px;"></i></a>');
    }

    // icone de encerrar
    if (atividade.ic_encerrada == 0) {
        icones.push('<a href="#" class="enc_atividade" style="text-decoration:none;color:cadetblue;font-size: 17px;" data-id="' + atividade.co_atividade + '" class="modal-encerrarAtividade" data-target="#encerrar-atividade" data-toggle="modal">\
        <i class="icon-exclamation-sign alert-tooltip" title="Encerrar" style="display: inline-block;"></i></a>');
    } else {
        icones.push('<a href="#" class="" style="text-decoration:none;color:darkred;font-size: 17px;" data-id="' + atividade.co_atividade + '" class="modal-encerrarAtividade" data-target="#encerrar-atividade" data-toggle="modal" >\
        <i class="icon-exclamation-sign alert-tooltip" title="Atividade encerrada" style="display: inline-block;"></i></a>');
    }

    // icone excluir
    icones.push('<a class="del_atividade" href="#" data-id="' + atividade.co_atividade + '">\
        <i class="silk-icon-cross alert-tooltip" title="Excluir" style="display: inline-block;"></i></a>');

    return icones.join(' ');
}

// adicionar atividade filha
function addChildRow(table, atividadeData, parentId, trParent) {
    var atividade = jQuery.extend(true, {}, atividadeData),
        pendencia = atividade.Atividade.pendencia || '',
        periodicidade = atividade.Atividade.periodicidade || '-';
    icones = '';
    iconeResponsavel = getIconeAtividade(atividade.Atividade.tp_andamento, atividade.Atividade.co_atividade);
    // adicionar a classe para editar a pct, se atividade já estiver iniciada
    // classModalEditarPercentual = atividade.Atividade.tp_andamento != 'N' ? 'modalEditarPercentual' : ' ';

    // icone de status da atividade
    atividade.Usuario.ds_nome = iconeResponsavel + ' ' + atividade.Usuario.ds_nome;

    // icones de ações
    icones = printIconesAtividade(atividade.Atividade);

    trParent.after(
        '<tr id="parent-id-' + parentId + '" class="children ' + atividade.Atividade.alerta + '">' +
        '<td class="tdResponsavel">' + atividade.Atividade.nome + '</td>\
            <td>' + atividade.Atividade.periodicidade + '</td>\
            <td>' + atividade.Usuario.ds_nome + '</td>\
            <td>' + atividade.Atividade.dt_ini_planejado + '</td>\
            <td>' + atividade.Atividade.dt_fim_planejado + '</td>\
            <td>' + atividade.Atividade.dt_ini_execucao + '</td>\
            <td>' + atividade.Atividade.dt_fim_execucao + '</td>\
            <td>' + atividade.Atividade.termino + '</td>\
            <td\
                title="' + atividade.Atividade.pc_executado + '"\
                data-id="' + atividade.Atividade.co_atividade + '"\
                pendencia="' + pendencia + '">' + atividade.Atividade.pc_executado + '</td>\
            <td>' + icones + '<td>\
        </tr>'
    );
}


// $('#tabelaExecucao tbody tr td.edit_descricao').dblclick(function(){
//     if($('td > input').length > 0){
//         return;
//     }
//     var conteudoOriginal    = $(this).text();
//     var novo_elemento       = $('<input/>', {type:'text', value: conteudoOriginal});
//     $(this).html(novo_elemento.bind('blur keydown', function(e){
//         var keyCode         = e.which;
//         var conteudoNovo    = $(this).val();
//         if(keyCode == 13 && conteudoNovo != '' && conteudoNovo != conteudoOriginal) {
//             var objeto  = $(this);
//             var url_desc = $('#base_url_atv').val() + "/atividades/index/" + $('#co_contrato').val();
//             $.ajax({
//                 type:"POST",
//                 url:url_desc,
//                 data:{
//                     "data[edit_atividade]":"1",
//                     "data[co_atividade]":$(this).parents('tr').attr('id'),
//                     "data[ds_atividade]":conteudoNovo
//                 },
//                 success:function(result){
//                     objeto.parent().html(conteudoNovo);
//                     //alert(result);
//                     //$('body').append(result);
//                 }
//             })
//         }
//         if(keyCode == 27 || e.type == "blur"){
//             $(this).parent().html(conteudoOriginal);
//         }
//     }));
//     $(this).children().select();
// })

// $('#tabelaExecucao tbody tr td.edit_responsavel').dblclick(function(){
//     if($('td > select').length > 0){
//         return;
//     }
//     var conteudoOriginal    = $(this).text();
//     var novo_elemento       = $("#co_responsavel").clone();
//     var id_original         = $(this).attr('title');

//     novo_elemento.val( id_original );

//     $(this).html(novo_elemento.bind('blur change', function(e){
//         var id_selecionado  = $(this).val();
//         var ds_selecionado  = $(this).find('option').filter(':selected').text();
//         if(id_selecionado != id_original) {
//             var objeto  = $(this);
//             var url_resp = $('#base_url_atv').val() + "/atividades/index/" + $('#co_contrato').val();
//             $.ajax({
//                 type:"POST",
//                 url:url_resp,
//                 data:{
//                     "data[edit_responsavel]":"1",
//                     "data[co_atividade]":$(this).parents('tr').attr('id'),
//                     "data[co_contrato]":$("#co_contrato").val(),
//                     "data[co_responsavel]":id_selecionado
//                 },
//                 success:function(){
//                     objeto.parent().attr('title', id_selecionado);
//                     objeto.parent().html(ds_selecionado);
//                 }
//             })
//         } else {
//             $(this).parent().html(conteudoOriginal);
//         }

//     }));
//     //$(this).children.focus();
//     novo_elemento.focus();
// })

//     /* editar porcentagem */
//     $('#tabelaExecucao tbody tr td.edit_percentual').dblclick(function(){
//         if($('td > select').length > 0){
//             return;
//         }

//         var conteudoOriginal    = $(this).text();
//         var novo_elemento       = $("#id_percentual").clone();
//         var id_original         = $(this).attr('title');

//         novo_elemento.val( id_original );

//         $(this).html(novo_elemento.bind('blur change', function(e){
//             var id_selecionado  = $(this).val();
// //            var ds_selecionado  = $(this).find('option').filter(':selected').text();
//             if(id_selecionado != id_original) {
// //                var objeto  = $(this);
//                 if( id_selecionado == 100 ) {
//                     if( !confirm('Tem certeza de que deseja finalizar a Atividade?') ) { return; }
//                 }

//                 var url_pct = $('#base_url_atv').val() + "/atividades/index/" + $('#co_contrato').val();
//                 $.ajax({
//                     type:"POST",
//                     url:url_pct,
//                     data:{
//                         "data[edit_percentual]":"1",
//                         "data[co_atividade]":$(this).parents('tr').attr('id'),
//                         "data[pc_executado]":id_selecionado
//                     },
//                     success:function(result){
// //                        objeto.parent().attr('title', id_selecionado);
// //                        objeto.parent().html(ds_selecionado);
//                         $('#id_pg_execucao').html("");
//                         $('#id_pg_execucao').append(result);
//                     }
//                 })
//             } else {
//                 $(this).parent().html(conteudoOriginal);
//             }

//         }));
//         //$(this).children.focus();
//         novo_elemento.focus();
//     });

// deletar atividade
// $('#tabelaExecucao tbody tr td a.del_atividade').click(function(){
//     if(!confirm('Tem certeza de que deseja excluir?')) {
//         return false;
//     } else { // Detelar Atividade
//         var url_del = $('#base_url_atv').val() + "/atividades/index/" + $('#co_contrato').val();
//         $.ajax({
//             type:"POST",
//             url:url_del,
//             data:{
//                 "data[del_atividade]":"1",
//                 "data[co_atividade]":$(this).parents('tr').attr('id')
//             },
//             success:function(result){
//                 $('#id_pg_execucao').html("");
//                 $('#id_pg_execucao').append(result);
//             }
//         })
//     }
// });
