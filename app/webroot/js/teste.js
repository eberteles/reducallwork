/* ===================================================
 * BRUNO SOARES ARAUJO
 * 16/05/2013
 * ========================================================== */
 $(function() {
 	

   
      
   $('.btnAdicionar').live('click', function(){
      $('#tabelaRotas tfoot').show();
      });
      $('.btnOK').live('click', function(){
      var input = $('.inputRota').val();
      var tr = "<tr>";
      tr += "<td style='text-align:center;'><input type='radio' id='rotaPrincipal' name='grupoRota'/></td>";
      tr += "<td class='campoRota'>" + input + "</td>";
      tr += "<td class='colAcoes'>";
      tr += "<button type='button' class='btn btn-mini btnOKEditar hide' href='#' title='Ok'><i class='icon-ok'></i></button>";
      tr += "<div class='btn-group'><button type='button' class='btn btn-mini btnEditar' href='#' title='Editar'><i class='icon-pencil'></i></button>";
      tr += "<button type='button' class='btn btn-mini btnExcluir' href='#' title='Excluir'><i class='icon-trash'></i></button></td>";
      tr += "</div></tr>";
      $('tr.mensagem').hide();
      $('#tabelaRotas tbody').append(tr);
       
      $('#tabelaRotas tfoot').hide();
      $('#tabelaRotas .inputRota').val('');
      });
 
      //Evento EXCLUIR Unidade Favorecida
      $('.btnExcluir').live('click', function(){
      var index = $('.btnExcluir').index(this)+1;
      $('#tabelaRotas tbody tr').eq(index).remove();
      if( $('#tabelaRotas tbody tr').length == 1 ){
      $('tr.mensagem').show();
      } else{
      $('tr.mensagem').hide();
      }
      });
      // EVENTO BTN EDITAR
      $('.btnEditar').live('click', function(){
      var index = $('.btnEditar').index(this);
      var inputRota = "<input type='text' class='span10 inputRota' />";
      var valorCampo = $('.campoRota').eq(index).html();
      $('.campoRota').eq(index).html($(inputRota).val(valorCampo));
      $('.btnEditar').eq(index).hide();
      $('.btnExcluir').eq(index).hide();
      $('.btnOKEditar').eq(index).show();
      });
 
      // EVENTO BTN OK Alteracao
      $('.btnOKEditar').live('click', function(){
      var index = $('.btnOKEditar').index(this);
      var input = $('.inputRota').val();
       
      $('.btnOKEditar').eq(index).hide();
      $('.btnEditar').eq(index).show();
      $('.btnExcluir').eq(index).show();
       
      $('.campoRota').eq(index).html(input);
       
      });


	
    
});