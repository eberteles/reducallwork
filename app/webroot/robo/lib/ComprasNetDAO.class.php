<?

require_once 'ComprasNetPDO.class.php';

/**
 * CompasNetDAO
 *
 * @author leo
 */
class ComprasNetDAO {

    private $pdo;

    public function __construct() {
        $this->pdo = new ComprasNetPDO();
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getContrato($uasg, $modalidade_numero=null, $modalidade=null) {
        if ($modalidade_numero !== null) $res = $this->pdo->query("SELECT * FROM contrato WHERE uasg=? AND modalidade_numero=? AND modalidade=?",$uasg,$modalidade_numero, $modalidade);
        else $res = $this->pdo->query("SELECT * FROM contrato WHERE uasg=?",$uasg);
        $ret = array();
        while ($o = $res->fetch()) { $ret[] = $o; }
        return $ret;
    }

    public function getEmpenho($uasg, $modalidade_numero=null, $empenho=null) {
        if ($modalidade_numero!==null && $empenho!==null) $res = $this->pdo->query("SELECT * FROM empenho WHERE contrato_uasg=? AND contrato_modalidade_numero=? AND empenho=?",$uasg,$modalidade_numero,$empenho);
        else if ($modalidade_numero !== null) $res = $this->pdo->query("SELECT * FROM empenho WHERE contrato_uasg=? AND contrato_modalidade_numero=?",$uasg,$modalidade_numero);
        else $res = $this->pdo->query("SELECT * FROM empenho WHERE contrato_uasg=?",$uasg);
        $ret = array();
        while ($o = $res->fetch()) { $ret[] = $o; }
        return $ret;
    }

    public function getAditivo($uasg, $modalidade_numero=null, $termo_aditivo=null, $modalidade_contrato=null) {
        if ($modalidade_numero!==null && $termo_aditivo!==null && $modalidade_contrato!==null){
            $res = $this->pdo->query("SELECT
                                        *
                                      FROM
                                        aditivo
                                      WHERE
                                        contrato_uasg=?
                                      AND contrato_modalidade_numero=?
                                      AND termo_aditivo=?
                                      AND modalidade_contrato=?
                                      ",$uasg,$modalidade_numero,$termo_aditivo, $modalidade_contrato);
        }
        $ret =
            array();
        while ($o = $res->fetch()) { $ret[] = $o; }
        return $ret;
    }

    public function getUasg($uasg) {
        $c = $this->pdo->query("SELECT * FROM contrato WHERE uasg=? ORDER BY YEAR(vigencia_de) desc",$uasg);
        while ($o = $c->fetch(PDO::FETCH_ASSOC)) { $contratos[] = $o; }
        if (!isset($contratos)) return;

        $res = array();
        foreach ($contratos as $contrato){
            if ($contrato['orgao_numero'] != 666) {
                $a = $this->pdo->query("SELECT * FROM aditivo WHERE contrato_uasg=? AND contrato_modalidade_numero=? AND modalidade_contrato=?",$contrato['uasg'],$contrato['modalidade_numero'],$contrato['modalidade']);
                while ($o = $a->fetch(PDO::FETCH_ASSOC)) { if (!isset($contrato['aditivos'])) $contrato['aditivos'] = array(); $contrato['aditivos'][] = $o; }
                $res[] = $contrato;
            }
        }
        return $res;
    }

    public function getUasgAno($uasg, $ano) {
        $c = $this->pdo->query("SELECT * FROM contrato WHERE uasg=? AND modalidade_numero like '%$ano%' ORDER BY YEAR(vigencia_de) desc",$uasg);
        while ($o = $c->fetch(PDO::FETCH_ASSOC)) { $contratos[] = $o; }
        if (!isset($contratos)) return;

        $res = array();
        foreach ($contratos as $contrato){
            if ($contrato['orgao_numero'] != 666) {
                $e = $this->pdo->query("SELECT
                                          *
                                        FROM
                                          empenho
                                        WHERE
                                          contrato_uasg=?
                                        AND
                                        contrato_modalidade_numero=?",
                    $contrato['uasg'],
                    $contrato['modalidade_numero']
                );
                while ($o = $e->fetch(PDO::FETCH_ASSOC)) {
                    if (!isset($contrato['empenhos'])){
                        $contrato['empenhos'] = array();
                    }
                    $contrato['empenhos'][] = $o;
                }

                $a = $this->pdo->query("SELECT
                                          contrato_uasg,
                                          contrato_modalidade_numero,
                                          termo_aditivo,
                                          data_publicacao,
                                          fundamento_legal,
                                          objeto,
                                          if(vigencia_de is not null, vigencia_de, '---') as vigencia_de,
                                          if(vigencia_ate is not null, vigencia_ate, '---') as vigencia_ate,
                                          valor_total,
                                          data_assinatura,
                                          modalidade_contrato
                                        FROM aditivo
                                        WHERE contrato_uasg=?
                                        AND contrato_modalidade_numero=?
                                        AND modalidade_contrato=?",
                    $contrato['uasg'],$contrato['modalidade_numero'],$contrato['modalidade']);
                while ($o = $a->fetch(PDO::FETCH_ASSOC)) {
                    if (!isset($contrato['aditivos'])) {
                        $contrato['aditivos'] = array();
                    }
                    $contrato['aditivos'][] = $o;
                }
                $res[] = $contrato;
            }
        }
        return $res;
    }


    public function inserts($dados) {
        foreach ($dados as $uasg => $tmp) {
        // insere contratos
        foreach ($dados[$uasg]['contratos'] as $modalidade_numero => $data) {

                if($this->insertContrato($uasg, $modalidade_numero, $data)){
                    if (isset($data['empenhos'])){
                        foreach ($data['empenhos'] as $empenho){
							try {
								$this->insertEmpenho($uasg, $modalidade_numero, $empenho, $data['modalidade']);
							} catch ( Exception $e ){
								echo 'Caught exception: ',  $e->getMessage(), "\n";
							}
                        }
                    }
                    if( isset($data['aditivos']) ){
                    	foreach ($data['aditivos'] as $aditivo){
                    		try {
                    			$this->insertAditivo($uasg, $modalidade_numero, $aditivo, $data['modalidade']);
                    		} catch ( Exception $e ){
                    			echo 'Caught exception: ',  $e->getMessage(), "\n";
                    		}
                    	}
                    }
                }
            }
        }
    }

    public function cleanSTR($str)
    {
        $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);
        $str = preg_replace('/[^0-9,.]/', '', $str);
        $str = preg_replace('/[^0-9,.]/', '', $str);
        $str = str_replace('-', '', $str);
        return str_replace('.', '', $str);
    }

    private function insertContrato($uasg, $modalidade_numero, $data) {
        if (empty($data['orgao_numero'])) {
            return false;
        }

        $find = $this->getContrato($uasg, $modalidade_numero, $data['modalidade']);
        if (count($find) > 0){
            return $this->updateContrato($uasg, $modalidade_numero, $data);
        }

        if($data['orgao_nome'] != null) {
            $sql = "SET character_set_client = 'UTF8';
                    SET character_set_results = 'UTF8';
                    ";

            $sql .= "INSERT INTO contrato (uasg, uasg_nome, orgao_numero, orgao_nome, modalidade, modalidade_numero, numero_processo, licitacao_modalidade, licitacao_numero, data_publicacao, contratado_cpf_cnpj, contratado, contratante, objeto, fundamento_legal, vigencia_de, vigencia_ate, valor_total, data_assinatura) ".
                "VALUES (:uasg, :uasg_nome, :orgao_numero, :orgao_nome, :modalidade, :modalidade_numero, :numero_processo, :licitacao_modalidade, :licitacao_numero, :data_publicacao, :contratado_cpf_cnpj, :contratado, :contratante, :objeto, :fundamento_legal, :vigencia_de, :vigencia_ate, :valor_total, :data_assinatura)";

            $stmt = $this->pdo->prepare($sql);

            $data_publicacao = self::fD($data['data_publicacao']);
            $vigencia_de = self::fD($data['vigencia_de']);
            $vigencia_ate = self::fD($data['vigencia_ate']);
            $data_assinatura = self::fD($data['data_assinatura']);

            $modalidade = $data['modalidade'] == '' ? 'sem_modalidade' : $data['modalidade'] ;

            $stmt->bindParam(':uasg', $uasg);
            $stmt->bindParam(':uasg_nome', $data['uasg_nome']);
            $stmt->bindParam(':orgao_numero', $data['orgao_numero']);
            $stmt->bindParam(':orgao_nome', $data['orgao_nome']);
            $stmt->bindParam(':modalidade', $modalidade);
            $stmt->bindParam(':modalidade_numero', $modalidade_numero);
            $stmt->bindParam(':numero_processo', $data['numero_processo']);
            $stmt->bindParam(':licitacao_modalidade', $data['licitacao_modalidade']);
            $stmt->bindParam(':licitacao_numero', $data['licitacao_numero']);
            $stmt->bindParam(':data_publicacao', $data_publicacao);
            $stmt->bindParam(':contratado_cpf_cnpj', $data['contratado_cpf_cnpj']);
            $stmt->bindParam(':contratado', $data['contratado']);
            $stmt->bindParam(':contratante', $data['contratante']);
            $stmt->bindParam(':objeto', $data['objeto']);
            $stmt->bindParam(':fundamento_legal', $data['fundamento_legal']);
            $stmt->bindParam(':vigencia_de', $vigencia_de);
            $stmt->bindParam(':vigencia_ate', $vigencia_ate);
            if(SISG == "1"){
                $stmt->bindParam(':valor_total', str_replace(',', '.', str_replace('.', '', $data['valor_global'])));
                $stmt->bindParam(':numero_processo', $this->cleanSTR($data['numero_processo']));
            } else {
                $stmt->bindParam(':valor_total', $data['valor_total']);
                $stmt->bindParam(':numero_processo', $data['numero_processo']);
            }
            $stmt->bindParam(':data_assinatura', $data_assinatura);

            $stmt->execute();
        } else {
            return false;
        }

        return true;
    }

    private function updateContrato($uasg, $modalidade_numero, $data) {
        $sql = "SET character_set_client = 'UTF8';
                SET character_set_results = 'UTF8';
                ";
        $sql .= "UPDATE contrato SET uasg_nome=:uasg_nome, orgao_numero=:orgao_numero, orgao_nome=:orgao_nome, modalidade=:modalidade, numero_processo=:numero_processo, licitacao_modalidade=:licitacao_modalidade, licitacao_numero=:licitacao_numero, data_publicacao=:data_publicacao, contratado_cpf_cnpj=:contratado_cpf_cnpj, contratado=:contratado, contratante=:contratante, objeto=:objeto, fundamento_legal=:fundamento_legal, vigencia_de=:vigencia_de, vigencia_ate=:vigencia_ate, valor_total=:valor_total, data_assinatura=:data_assinatura ".
            "WHERE uasg=:uasg AND modalidade=:modalidade AND modalidade_numero=:modalidade_numero";

        $stmt = $this->pdo->prepare($sql);

        $vigencia_de = self::fD($data['vigencia_de']);
        $vigencia_ate = self::fD($data['vigencia_ate']);
        $data_assinatura = self::fD($data['data_assinatura']);

        $stmt->bindParam(':uasg', $uasg);
        $stmt->bindParam(':uasg_nome', utf8_decode($data['uasg_nome']));
        $stmt->bindParam(':orgao_numero', $data['orgao_numero']);
        $stmt->bindParam(':orgao_nome', $data['orgao_nome']);
        $stmt->bindParam(':modalidade', $data['modalidade']);
        $stmt->bindParam(':modalidade_numero', $modalidade_numero);
        $stmt->bindParam(':numero_processo', $data['numero_processo']);
        $stmt->bindParam(':licitacao_modalidade', $data['licitacao_modalidade']);
        $stmt->bindParam(':licitacao_numero', $data['licitacao_numero']);
        $stmt->bindParam(':data_publicacao', $data['data_publicacao']);
        $stmt->bindParam(':contratado_cpf_cnpj', $data['contratado_cpf_cnpj']);
        $stmt->bindParam(':contratado', $data['contratado']);
        $stmt->bindParam(':contratante', $data['contratante']);
        $stmt->bindParam(':objeto', $data['objeto']);
        $stmt->bindParam(':fundamento_legal', $data['fundamento_legal']);
        $stmt->bindParam(':vigencia_de', $vigencia_de);
        $stmt->bindParam(':vigencia_ate', $vigencia_ate);
        if(SISG == "1"){
            $stmt->bindParam(':valor_total', str_replace(',', '.', str_replace('.', '', $data['valor_global'])));
        } else {
            $stmt->bindParam(':valor_total', $data['valor_total']);
        }
        $stmt->bindParam(':data_assinatura', $data_assinatura);

        return $stmt->execute();
    }

    private function insertEmpenho($uasg, $modalidade_numero, $data, $modalidade) {
        $find = $this->getEmpenho($uasg, $modalidade_numero, $data['empenho']);
        if (isset($data['empenho']) && !empty($find))
            return $this->updateEmpenho($uasg, $modalidade_numero, $data);

        $sql = "SET character_set_client = 'UTF8';
                SET character_set_results = 'UTF8';
                ";
        $sql .= "INSERT INTO empenho (contrato_uasg, contrato_modalidade_numero, ug_numero, ug_nome, gestao, programa, empenho, modalidade) ".
            "VALUES (:contrato_uasg, :contrato_modalidade_numero, :ug_numero, :ug_nome, :gestao, :programa, :empenho, :modalidade)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':contrato_uasg', $uasg);
        $stmt->bindParam(':contrato_modalidade_numero', $modalidade_numero);
        $stmt->bindParam(':ug_numero', $data['ug_numero']);
        $stmt->bindParam(':ug_nome', $data['ug_nome']);
        $stmt->bindParam(':gestao', $data['gestao']);
        $stmt->bindParam(':programa', $data['programa']);
        $stmt->bindParam(':empenho', $data['empenho']);
        $stmt->bindParam(':modalidade', $modalidade);

        return $stmt->execute();
    }

    private function updateEmpenho($uasg, $modalidade_numero, $data) {

        $sql = "SET character_set_client = 'UTF8';
                SET character_set_results = 'UTF8';
                ";
        $sql .= "UPDATE empenho SET ug_numero=:ug_numero, ug_nome=:ug_nome, gestao=:gestao, programa=:programa ".
            "WHERE contrato_uasg=:uasg AND contrato_modalidade_numero=:modalidade_numero AND empenho=:empenho";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':uasg', $uasg);
        $stmt->bindParam(':modalidade_numero', $modalidade_numero);
        $stmt->bindParam(':ug_numero', $data['ug_numero']);
        $stmt->bindParam(':ug_nome', $data['ug_nome']);
        $stmt->bindParam(':gestao', $data['gestao']);
        $stmt->bindParam(':programa', $data['programa']);
        $stmt->bindParam(':empenho', $data['empenho']);

        $stmt->execute();
    }

    private function insertAditivo($uasg, $modalidade_numero, $aditivo, $modalidade) {
        if($aditivo['data_publicacao'] == null || $aditivo['data_publicacao'] == ''){
            return false;
        }
        $find = $this->getAditivo($uasg, $modalidade_numero, $aditivo['termo_aditivo'], $modalidade);
        if (isset($aditivo['termo_aditivo']) && !empty($find)){
            return $this->updateAditivo($uasg, $aditivo);
        }

        $sql = "SET character_set_client = 'UTF8';
                SET character_set_results = 'UTF8';
                ";
        $sql .= "INSERT INTO aditivo (contrato_uasg, contrato_modalidade_numero, termo_aditivo, data_publicacao, fundamento_legal, objeto, vigencia_de, vigencia_ate, valor_total, data_assinatura, modalidade_contrato) ".
            "VALUES (:contrato_uasg, :contrato_modalidade_numero, :termo_aditivo, :data_publicacao, :fundamento_legal, :objeto, :vigencia_de, :vigencia_ate, :valor_total, :data_assinatura, :modalidade_contrato)";

        $stmt = $this->pdo->prepare($sql);

        $data_publicacao = self::fD($aditivo['data_publicacao']);
        $vigencia_de = self::fD($aditivo['vigencia_de']);
        $vigencia_ate = self::fD($aditivo['vigencia_ate']);
        $data_assinatura = self::fD($aditivo['data_assinatura']);

        $stmt->bindParam(':contrato_uasg', $uasg);
        $stmt->bindParam(':contrato_modalidade_numero', $aditivo['modalidade_numero']);
        $stmt->bindParam(':termo_aditivo', $aditivo['termo_aditivo']);
        $stmt->bindParam(':data_publicacao', $data_publicacao);
        $stmt->bindParam(':fundamento_legal', $aditivo['fundamento_legal']);
        $stmt->bindParam(':objeto', $aditivo['objeto']);
        $stmt->bindParam(':vigencia_de', $vigencia_de);
        $stmt->bindParam(':vigencia_ate', $vigencia_ate);
        $stmt->bindParam(':valor_total', $aditivo['valor_total']);
        $stmt->bindParam(':data_assinatura', $data_assinatura);
        $stmt->bindParam(':modalidade_contrato', $aditivo['modalidade']);
        try{
            return $stmt->execute();
        }catch(Exception $e){
            var_dump($e->getMessage());die;
            return false;
        }
    }

    private function updateAditivo($uasg, $aditivo) {
        $sql = "SET character_set_client = 'UTF8';
                SET character_set_results = 'UTF8';
                ";
        $sql .= "UPDATE aditivo SET data_publicacao=:data_publicacao, fundamento_legal=:fundamento_legal, objeto=:objeto, vigencia_de=:vigencia_de, vigencia_ate=:vigencia_ate, valor_total=:valor_total, data_assinatura=:data_assinatura, modalidade_contrato=:modalidade_contrato ".
            "WHERE contrato_uasg=:uasg AND contrato_modalidade_numero=:modalidade_numero AND termo_aditivo=:termo_aditivo AND :modalidade_contrato";

        $stmt = $this->pdo->prepare($sql);

        $data_publicacao = self::fD($aditivo['data_publicacao']);
        $vigencia_de = self::fD($aditivo['vigencia_de']);
        $vigencia_ate = self::fD($aditivo['vigencia_ate']);
        $data_assinatura = self::fD($aditivo['data_assinatura']);

        $stmt->bindParam(':uasg', $uasg);
        $stmt->bindParam(':modalidade_numero', $modalidade_numero);
        $stmt->bindParam(':termo_aditivo', $aditivo['termo_aditivo']);
        $stmt->bindParam(':data_publicacao', $aditivo['data_publicacao']);
        $stmt->bindParam(':fundamento_legal', $aditivo['fundamento_legal']);
        $stmt->bindParam(':objeto', $aditivo['objeto']);
        $stmt->bindParam(':vigencia_de', $vigencia_de);
        $stmt->bindParam(':vigencia_ate', $vigencia_ate);
        $stmt->bindParam(':valor_total', $aditivo['valor_total']);
        $stmt->bindParam(':data_assinatura', $data_assinatura);
        $stmt->bindParam(':modalidade_contrato', $aditivo['modalidade']);

        return $stmt->execute();
    }

    public function truncateAll() {
        $sql = "DELETE FROM empenho; DELETE FROM aditivo; DELETE FROM contrato;";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }

    public static function fD(&$d) {
        if(strlen($d) >= 9){
            return substr($d,6,4).'-'.substr($d,3,2).'-'.substr($d,0,2);
        }else{
            return null;
        }
    }
}

class tmpContrato {
    public $uasg, $uasg_nome, $orgao_numero, $orgao_nome, $modalidade, $modalidade_numero, $numero_processo, $licitacao_modalidade, $licitacao_numero, $data_publicacao, $contratado_cpf_cnpj, $contratado, $contratante, $objeto, $fundamento_legal, $vigencia_de, $vigencia_ate, $valor_total, $data_assinatura;
    public function __construct($uasg, $modalidade_numero) { $this->uasg=$uasg; $this->$modalidade_numero=$modalidade_numero; $this->orgao_numero='666'; }
}



?>
