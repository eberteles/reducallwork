<?php
	/**
	 * ComprasNet - Rastreador de Contratos e Aditivos
	 *
	 * Essa lasca de código faz elegantemente a captura das informações publicadas
	 * no site comprasnet.gov.br, responsável pela divulgação das operações do
	 * aplicativo SIASG.
	 *
	 * A manutenção da sessão, cookies e sequência de requisições são fundamentais
	 * e indispensáveis para o funcionamento correto deste mecanismo.
	 *
	 * Na utilização do conteúdo coletado, tal qual a execução deste algorítimo,
	 * qualquer prática abusiva ou maliciosa deste é integralmente responsabilidade
	 * do interessado.
	 *
	 * Formas de uso:
	 *   - Informar o código UASG da instituição no parâmetro da requisição:
	 *
	 * 		http://localhost/cn.php?uasg=160082 (Retorna um JSON com os resultados)
	 *
	 * 		http://localhost/cn.php?uasg=160082&fromdb=1 (Retorna um JSON com os resultados da base de dados)
	 *
	 * 		http://localhost/cn.php?uasg=160082&debug=1 (Imprime o resultado amigavelmente para avaliação)
	 *
     * 		http://localhost/cn.php?uasg=160082&retroativo=1 (Retroativo a 1 ano ao período da pesquisa, pode-se colocar quantos anos pra tras se quer, 0 é o ano atual)
	 *
	 * 		http://localhost/cn.php?uasg=160082&updatedb=1 (Atualiza o banco de dados com o resultado)
	 *
	 * 		Os parametros sugeridos acima podem ser usados em paralelo.
	 *
	 * @author Stéfan de Lima Amaral
	 * @copyright stefan.com
	 * @version 1.0
	 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
	 * @since 2014-09-18
	 */
    session_start();

		define ('CODIGO_UASG', $_GET['uasg']);
		define ('RETROATIVO', isset($_GET['retroativo'])?$_GET['retroativo']:0);
    define ('SISG', isset($_GET['sisg'])?$_GET['sisg']:0);

    /**
     * Verifica se cliente é SISG
     */
    if(SISG == '1'){
        define ('URL_COMPRASNET', 'http://comprasnet.gov.br/livre/Contratoent/conrecontent');
    } else {
        define ('URL_COMPRASNET', 'http://comprasnet.gov.br/livre/Contrato/conrecon');
    }

	$ch = null;

	/**
	 * CompasNetStep
	 *
	 * @author leo
	 */
	class ComprasNetStep {
		private $url, $params, $html, $dig, $digFn;

		public function __construct() { $this->dig = array(); }
		public function setUrl($url) { $this->url = $url; }
		public function getUrl() { return $this->url; }
		public function addParam($param, $value) { $this->params[$param] = $value; }
		public function getHtml() { return $this->html; }
		public function setDigFn($fn) {
			$m = new ReflectionMethod('ComprasNetDig', $fn);
			$this->digFn = implode('', array_slice(file($m->getFileName()), $m->getStartLine(), $m->getEndLine()-1-$m->getStartLine()));
		}
		public function getDig($param=null, $index=null) {
			if ($param === null) return $this->dig;
			else if ($index === null) return $this->dig[$param];
			else if (!is_numeric($index)) return $this->dig[$param][$index];
			else if ($this->dig[$param] === null){ echo "UASG Nao Cadastrada no ComprasNet"; die(); }
			else { $a = array_values($this->dig[$param]); return $a[$index]; }
		}
		public function getDigK($param, $index) {
			$a = array_values($this->dig[$param]);
			$i = 0;
			foreach ($this->dig[$param] as $k => $v) {
				if ($i == $index)
					return $k;
				$i++;
			}
		}

		public function paramsStr() {
			$str = '';
			if ($this->params) {
				foreach($this->params as $k=>$v) { $str .= $k.'='.$v.'&'; }
				rtrim($str, '&');
			}
			return $str;
		}

		public function crawlHtml() {
            set_time_limit(120);

			curl_setopt($GLOBALS["ch"], CURLOPT_URL, $this->url);
			curl_setopt($GLOBALS["ch"], CURLOPT_POST, count($this->params));
			curl_setopt($GLOBALS["ch"], CURLOPT_POSTFIELDS, $this->paramsStr());

			$this->html = curl_exec($GLOBALS["ch"]);

			eval($this->digFn);
		}

		public function cropDig($name, $ns) {
			$a = $this->html;
			foreach ($ns as $n) $a = c($a, $n[0], $n[1]);
			$this->dig[$name] = str_replace('', "", trim($a));
		}
	}

	/**
	 * CompasNetCrawler
	 *
	 * @author leo
	 */
	class ComprasNetCrawler {
		public $steps, $results;

		public function __construct() {
			$this->results = array();
			$this->results[CODIGO_UASG] = array();
			$this->results[CODIGO_UASG]['contratos'] = array();
			$this->results[CODIGO_UASG]['credenciamentos'] = array();

			$cookieName = realpath('.').'/cookies/_cookie.txt';
			$cookieFile = fopen($cookieName, "w");
			if (!file_exists($cookieName) || !is_writable($cookieName)){
				echo 'Sem permissão de escrita no cookie.';
				die();
			}
			fwrite($cookieFile, '');
			fclose($cookieFile);

			$GLOBALS["ch"] = curl_init();
			curl_setopt($GLOBALS["ch"], CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($GLOBALS["ch"], CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($GLOBALS["ch"], CURLOPT_COOKIEJAR, $cookieName);
			curl_setopt($GLOBALS["ch"], CURLOPT_COOKIEFILE, $cookieName);
			curl_setopt($GLOBALS["ch"], CURLOPT_RETURNTRANSFER, true);
		}

		public function __destruct() { curl_close($GLOBALS["ch"]); }

		public function run() {
            $this->populateStepsContrato();
            // $this->populateStepsCredenciamento();
        }

		private function populateStepsContrato() {
            $mes_ate = '12';
            if( RETROATIVO == 0 ) {
                $mes_ate = date('m');
            }
			$step1 = new ComprasNetStep();
			$step1->setUrl(URL_COMPRASNET.'0.asp');
			$this->addStep($step1);

			$step2 = new ComprasNetStep();
			$step2->setUrl(URL_COMPRASNET.'1.asp');
			$step2->addParam('sg_uf', '');
			$step2->addParam('co_no_orgao', '');
			$step2->addParam('co_uasg', CODIGO_UASG);
			$step2->addParam('no_uasg', '');
			$step2->setDigFn('conrecon1');
			$this->addStep($step2);

			$step3 = new ComprasNetStep();
			$step3->setUrl(URL_COMPRASNET.'3.asp');
			$step3->addParam('co_no_uasg', $step2->getDig('uasg', 0));
			$step3->addParam('da_mesini', '01');
			$step3->addParam('da_anoini', date('Y')-RETROATIVO);
			$step3->addParam('da_mesfim', $mes_ate);
			$step3->addParam('da_anofim', date('Y')-RETROATIVO);
			$step3->addParam('mod_cont_term', $step2->getDig('modalidade', '50 - Contrato'));
			$step3->setDigFn('conrecon3');
			$this->addStep($step3);

            $tmp = $step3->getDig('contrato');

            if(count($tmp) > 0){
                foreach ($tmp as $k => $v) {
                    $step = new ComprasNetStep();
                    $step->setUrl(URL_COMPRASNET.'5.asp');
                    $step->addParam('nu_cont_term', $v);
                    $step->addParam('ok', 'OK');
                    $step->setDigFn('conrecon5');
                    $this->addStep($step);
                    $this->results[CODIGO_UASG]['contratos'][str_replace(' ', '', $k)] = $step->getDig();

                    $stepA = new ComprasNetStep();
                    // TODO: implementar modalidade automática, quando for necessário
                    $stepA->setUrl(str_replace(' ', '+', URL_COMPRASNET.'7.asp?modalidade=50'));
                    $stepA->setDigFn('empenhos');

                    $this->addStep($stepA);
                    $this->results[CODIGO_UASG]['contratos'][str_replace(' ', '', $k)]['empenhos'] = $stepA->getDig('empenhos');
                    // Importando Aditivos dos contratos.
		            $nuCont = str_replace(array(' ', '/'), '', $k);
					// Retorna lista de aditivos do contrato.
		            $stepB = new ComprasNetStep();
		            $stepB->setUrl(str_replace(' ', '+', URL_COMPRASNET.'8.asp?mod_cont_term9=55 - Termo Aditivo&nu_cont=' . $nuCont . '&mod_cont=50'));
		            // conrecon8.asp?mod_cont_term9=55 - Termo Aditivo&nu_cont=000042016&mod_cont=50'
		            $stepB->setDigFn('conrecon3a');

		            $this->addStep($stepB);
		            $tmpA = $stepB->getDig('aditivo');
		            if( count($tmpA) > 0 ){
		                foreach ($tmpA as $ka => $va) {
		                	$step4 = new ComprasNetStep();
		                	$step4->setUrl(URL_COMPRASNET.'13.asp');
		                	$step4->addParam('nu_cont_term9', $va);
		                	$step4->addParam('ok', 'OK');
		                	$step4->setDigFn('conrecon11');
		                	$this->addStep($step4);
		                	$this->results[CODIGO_UASG]['contratos'][str_replace(' ', '', $k)]['aditivos'][$ka] = $step4->getDig();
		                }
		                $tmpA = null;
		            }
                }
            }
		}

		public function results() {
			return utf8ize($this->results);
		}

		public function getJson() {
			return utf8_converter_json($this->results);
		}

		protected function addStep($step) {
			$step->crawlHtml();
			$this->steps[] = $step;
		}

		public function debug() {
			echo "<pre style='border:1px solid blue;padding:10px;font-size:10px;'>";
			print_r($this->steps);
			echo "</pre>";
		}
	}

	/**
	 * CompasNetDig
	 *
	 * @author leo
	 */
	class ComprasNetDig {

		private static function conrecon1() {
			$a = c($this->html, '<select name="co_no_uasg"', '</select>');
			$b = cN($a, '<option ', '/option>');
			foreach ($b as $c)
				$this->dig['uasg'][c($c, '>', '<')] = c($c, 'value="', '"');

			$a = c($this->html, '<select name="mod_cont_term"', '</select>');
			$b = cN($a, '<option ', '/option>');
			foreach ($b as $c)
				$this->dig['modalidade'][c($c, '>', '<')] = c($c, 'value="', '"');
		}

		private static function conrecon3() {
			$a = c($this->html, '<select name="nu_cont_term"', '</select>');
			$b = cN($a, '<option ', '/option>');
			foreach ($b as $c)
				$this->dig['contrato'][c($c, '>', '<')] = c($c, 'value="', '"');
		}

        private static function conrecon4() {
            $a = c($this->html, '<select name="nu_cont_term"', '</select>');
            $b = cN($a, '<option ', '/option>');
            foreach ($b as $c)
                $this->dig['credenciamento'][c($c, '>', '<')] = c($c, 'value="', '"');
        }

		private static function conrecon3a() {
			$a = c($this->html, '<select name="nu_cont_term9"', '</select>');
			$b = cN($a, '<option ', '/option>');
			foreach ($b as $c)
				$this->dig['aditivo'][c($c, '>', '<'). '_' . rand()] = c($c, 'value="', '"');
		}

		private static function conrecon5() {
			$this->cropDig('orgao_numero', array(array('Órgão: </td>','/span>'),array('<span class=tex3>',' -')));
			$this->cropDig('orgao_nome', array(array('Órgão: </td>','/span>'),array('- ','<')));
			$this->cropDig('uasg_nome', array(array('Uasg: </td>','/span>'),array('- ','<')));
			$this->cropDig('periodo_de', array(array('odo: </td>','/span>'),array('<span class=tex3>',' a')));
			$this->dig['periodo_de'] = str_replace(' ', '', $this->dig['periodo_de']);
			$this->cropDig('periodo_ate', array(array('odo: </td>','/span>'),array('a ','<')));
			$this->dig['periodo_ate'] = str_replace(' ', '', $this->dig['periodo_ate']);
			$this->cropDig('modalidade', array(array('Modalidade:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('modalidade_numero', array(array('Modalidade:</td>','</tr>'),array(' : </span>','<')));
			$this->dig['modalidade_numero'] = str_replace(' ', '', $this->dig['modalidade_numero']);
			$this->cropDig('numero_processo', array(array(' do Processo:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('licitacao_modalidade', array(array('Modalidade de','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('licitacao_numero', array(array('Modalidade de','</tr>'),array(': </span>','<')));
			$this->dig['licitacao_numero'] = str_replace(' ', '', $this->dig['licitacao_numero']);
			$this->cropDig('data_publicacao', array(array('Data da','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('contratado_cpf_cnpj', array(array('CNPJ/CPF Contratado:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('contratado', array(array('>Contratado:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('contratante', array(array('Contratante: </td>','/span>'),array('<span class=tex3>','<')));

			$this->cropDig('objeto', array(array('Objeto: </td>','/span>'),array('<span class=tex3>','<')));
			$this->dig['objeto'] = tA($this->dig['objeto']);
			$this->cropDig('fundamento_legal', array(array('Fundamento Legal: </td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('vigencia_de', array(array('ncia: </td>','/span>'),array('<span class=tex3>',' a')));
			$this->cropDig('vigencia_ate', array(array('ncia: </td>','/span>'),array('a ','<')));
			$this->cropDig('valor_total', array(array('Valor total: </td>','/span>'),array('<span class=tex3>','<')));

			$this->dig['valor_total'] = str_replace('.', '', $this->dig['valor_total']);
			$this->dig['valor_total'] = str_replace(',', '.', $this->dig['valor_total']);
			$this->cropDig('data_assinatura', array(array('Data de assinatura: </td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('url_aditivos', array(array('value="Termo aditivo" class="texField2" onClick="', 'return false;'),array('location.href=\'','\';')));
			$this->cropDig('url_empenhos', array(array('value="Empenhos" class="texField2" onClick="', 'return false;'),array('location.href=\'','\';')));
		}

		private static function conrecon11() {
			$this->cropDig('orgao_numero', array(array('Órgão: </td>','/span>'),array('<span class=tex3>',' -')));
			$this->cropDig('orgao_nome', array(array('Órgão: </td>','/span>'),array('- ','<')));
			$this->cropDig('uasg_nome', array(array('Uasg: </td>','/span>'),array('- ','<')));
			$this->cropDig('periodo_de', array(array('odo: </td>','/span>'),array('<span class=tex3>',' a')));
			$this->dig['periodo_de'] = str_replace(' ', '', $this->dig['periodo_de']);
			$this->cropDig('periodo_ate', array(array('odo: </td>','/span>'),array('a ','<')));
			$this->dig['periodo_ate'] = str_replace(' ', '', $this->dig['periodo_ate']);
			$this->cropDig('modalidade', array(array('Modalidade:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('modalidade_numero', array(array('Modalidade:</td>','</tr>'),array(' : </span>','<')));
			$this->dig['modalidade_numero'] = str_replace(' ', '', $this->dig['modalidade_numero']);

			$this->cropDig('termo_aditivo', array(array('<td class="tex3" colspan="2">','</td>')));
			$this->dig['termo_aditivo'] = str_replace(' ', '', $this->dig['termo_aditivo']);

			$this->cropDig('numero_processo', array(array(' do Processo:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('data_publicacao', array(array('Data da','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('contratado_cpf_cnpj', array(array('CNPJ/CPF Contratado:</td>','/span>'),array('<span class=tex3>','<')));
			$this->cropDig('contratado', array(array('>Contratado:</td>','/span>'),array('<span class=tex3>','<')));
			$this->dig['contratado'] = tA($this->dig['contratado']);
			$this->cropDig('contratante', array(array('Contratante: </td>','/span>'),array('<span class=tex3>','<')));
			$a = c($this->html, 'Objeto: </td>', '/span>');
			$this->dig['objeto'] = str_replace('"', '', str_replace('                            ', '', trim(c($a, '<span class=tex3>', '<'))));
			$this->dig['objeto'] = tA($this->dig['objeto']);
			$this->cropDig('fundamento_legal', array(array('Fundamento Legal: </td>','/span>'),array('<span class=tex3>','<')));
			$this->dig['fundamento_legal'] = tA($this->dig['fundamento_legal']);
			$this->cropDig('vigencia_de', array(array('ncia: </td>','/span>'),array('<span class=tex3>',' a')));
			$this->cropDig('vigencia_ate', array(array('ncia: </td>','/span>'),array('a ','<')));
			$this->cropDig('valor_total', array(array('Valor total: </td>','/span>'),array('<span class=tex3>','<')));
			$this->dig['valor_total'] = str_replace('.', '', $this->dig['valor_total']);
			$this->dig['valor_total'] = str_replace(',', '.', $this->dig['valor_total']);
			$this->cropDig('data_assinatura', array(array('Data de assinatura: </td>','/span>'),array('<span class=tex3>','<')));
		}

		private static function empenhos() {
			$us = cN($this->html, '<td class="tex3b">UG:</td>', '</td>');
			$uc=array();
			$ul=array();
			$i=0;
			foreach ($us as $x) {
				$uc[$i] = c($x, '<span class=tex3>', ' -');
				$ul[$i] = c($x, '- ', '</span>');
				$i++;
			}

			$gs = cN($this->html, 'o:</td>', '</td>');
			$g=array();$i=0;
			foreach ($gs as $x) {$g[$i] = c($x, '<span class=tex3>', '</span>');$i++;}

			$ps = cN($this->html, 'Programa : </td>', '</td>');
			$p=array();$i=0;
			foreach ($ps as $x) {$p[$i] = c($x, '<span class=tex3>', '</span>');$i++;}

			$es = cN($this->html, 'Empenho: ', '/td>');
			$e=array();$i=0;
			foreach ($es as $x) {$e[$i] = c($x, '</span>', '<');$i++;}

			$this->dig['empenhos'] = array();
			for ($i=0; $i<sizeof($uc); $i++) {
				$this->dig['empenhos'][$i] = array();
				$this->dig['empenhos'][$i]['ug_numero'] = $uc[$i];
				$this->dig['empenhos'][$i]['ug_nome'] = $ul[$i];
				$this->dig['empenhos'][$i]['gestao'] = $g[$i];
				$this->dig['empenhos'][$i]['programa'] = $p[$i];
				$this->dig['empenhos'][$i]['empenho'] = $e[$i];
			}
		}

	}

	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v){
                $d[$k] = utf8ize($v);
            }
		} else if (is_string ($d)) {
			return mb_convert_encoding($d, 'UTF-8');
        }
		return $d;
	}

	function c($txt, $nI, $nO) {
		$pI = strpos($txt, $nI) + strlen($nI);
		$pO = strpos($txt, $nO, $pI);
		return substr($txt, $pI, $pO - $pI);
	}

	function cN($txt, $nI, $nO) {
		$texts = array();
		$pI = strpos($txt, $nI);
		while ($pI !== false) {
			$pI += strlen($nI);
			$pO = strpos($txt, $nO, $pI);
			$texts[] = substr($txt, $pI, $pO - $pI);
			$pI = strpos($txt, $nI, $pO + strlen($nO));
		}
		return $texts;
	}

	function tA($str, $what=null, $with=' ') {
		if ($what === null) $what = "\\x00-\\x20";
		return trim(preg_replace("/[".$what."]+/", $with, $str), $what);
	}

	function utf8_converter_json($array)
	{
        if( !is_array($array) ){
            return false;
        }
		array_walk_recursive($array, function(&$item, $key){
			if(is_string($item)){
                $item = mb_convert_encoding($item, 'UTF-8');
			}
		});

        $jsonData =json_encode($array);

        $jsonDataEscaped = preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {
            return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'UTF-8');
        }, $jsonData);

        return $jsonDataEscaped;
	}

	$arrRetorno = null;

	try {
		if (isset($_GET['uasg']) && $_GET['uasg']!='') {
			if (isset($_GET['fromdb'])) {
	            require_once 'lib/ComprasNetDAO.class.php';
	            $dao = new ComprasNetDAO();

	            if(isset($_GET['ano'])){
	                if (isset($_GET['debug'])) {
	                    echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>';
	                    echo "<pre style='border:1px solid blue;padding:10px;font-size:10px;'>";
	                    print_r($dao->getUasgAno($_GET['uasg'], $_GET['ano']));
	                    echo "</pre></body></html>";
	                    die();
	                } else {
                        header('Content-Type: application/json');
	                    echo utf8_converter_json($dao->getUasgAno($_GET['uasg'], $_GET['ano']));
                        exit;
	                }
	            } else {
	                if (isset($_GET['debug'])) {
	                    echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>';
	                    echo "<pre style='border:1px solid blue;padding:10px;font-size:10px;'>";
	                    print_r($dao->getUasg($_GET['uasg']));
	                    echo "</pre></body></html>";
	                    die();
	                } else {
                        header('Content-Type: application/json');
	                    echo utf8_converter_json($dao->getUasg($_GET['uasg']));
                        exit;
	                }
	            }
			} else {
				$a = new ComprasNetCrawler();
				$a->run();

				if (isset($_GET['updatedb'])) {
					require_once 'lib/ComprasNetDAO.class.php';
					$dao = new ComprasNetDAO();
					$dao->inserts($a->results());
				}

				if (isset($_GET['debug'])) {
					echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>';
					echo "<pre style='border:1px solid blue;padding:10px;font-size:10px;'>";
					print_r($a->results());
					echo "</pre></body></html>";
				} else {
                    header('Content-Type: application/json');
					echo $a->getJson();
                    exit;
				}
			}
		}
	} catch (Exception $e) {
		$arrRetorno = array(
			'status' => false,
			'data' => $e->getTraceAsString()
		);
	}
?>
