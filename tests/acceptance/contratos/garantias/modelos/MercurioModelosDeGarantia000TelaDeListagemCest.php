<?php

class MercurioModelosDeGarantia000TelaDeListagemCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('administrador', 'suporte09');
        $I->amOnPage('/garantiasModalidade');
    }

    public function VerificaCamposQueDevemAparacerNaTelaListagem(AcceptanceTester $I)
    {
        #labels das colunas da grid
        $grid = '/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr[1]';
        $I->see('Código', "$grid/th[1]");
        $I->see('Descrição', "$grid/th[2]");
        $I->see('Ações', "$grid/th[3]");
    }

}
