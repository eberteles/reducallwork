<?php

class MercurioModelosDeGarantia001TelaDeListagemBotaoIncluirCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('administrador', 'suporte09');
        $I->amOnPage('/garantiasModalidade');
        $I->click('Nova Modalidade De Garantia');
    }

    public function VerificaCamposQueDevemAparacerNaTelaIncluir(AcceptanceTester $I)
    {
        $I->see('Modalidade de Garantia');
        $I->see('Descrição');
        $I->seeElement('//*[@id="GarantiaModalidadeDsModalidadeGarantia"]');

        $I->see('Listagem');
        $I->see('Salvar');
        $I->see('Limpar');
        $I->see('Voltar');
    }

    public function LimparCampos(AcceptanceTester $I)
    {
        $campoDescricao = '//*[@id="GarantiaModalidadeDsModalidadeGarantia"]';
        $I->fillField($campoDescricao, 'xalala');
        $I->assertNotEmpty($I->grabValueFrom($campoDescricao));
        $I->click('Limpar');
        $I->assertEmpty($I->grabValueFrom($campoDescricao));
    }

    public function Voltar(AcceptanceTester $I)
    {
        $I->click('Voltar');
        $I->cantSeeCurrentUrlEquals('garantiasModalidade');
    }

    public function Listagem(AcceptanceTester $I)
    {
        $I->click('Listagem');
        $I->cantSeeCurrentUrlEquals('garantiasModalidade');
    }

}
