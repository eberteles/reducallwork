<?php

class MercurioModelosDeGarantia002TelaDeListagemBotaoEditarCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('administrador', 'suporte09');
        $I->amOnPage('/garantiasModalidade');
        # botão de editar da primeira linha
        $I->click('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr[2]/td[3]/div/a[1]');
    }

    public function VerificaCamposQueDevemAparacerNaTelaIncluir(AcceptanceTester $I)
    {
        $I->see('Modalidade de Garantia');
        $I->see('Descrição');
        $I->seeElement('//*[@id="GarantiaModalidadeDsModalidadeGarantia"]');

        $I->see('Listagem');
        $I->see('Salvar');
        $I->see('Limpar');
        $I->see('Voltar');
    }

    public function LimparCampos(AcceptanceTester $I)
    {
        $campoDescricao = '//*[@id="GarantiaModalidadeDsModalidadeGarantia"]';
        $I->assertNotEmpty($I->grabValueFrom($campoDescricao));
        $I->click('Limpar');
        # limpar no editar volta o valor original da edição
        $I->assertNotEmpty($I->grabValueFrom($campoDescricao));
    }

    public function Voltar(AcceptanceTester $I)
    {
        $I->click('Voltar');
        $I->cantSeeCurrentUrlEquals('garantiasModalidade');
    }

    public function Listagem(AcceptanceTester $I)
    {
        $I->click('Listagem');
        $I->cantSeeCurrentUrlEquals('garantiasModalidade');
    }

}
