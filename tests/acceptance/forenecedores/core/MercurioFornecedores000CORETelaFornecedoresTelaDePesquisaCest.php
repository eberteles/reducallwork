<?php

class MercurioFornecedores000CORETelaFornecedoresTelaDePesquisaCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function VerificaCamposQueDevemAparacerNaTelaPesquisa(AcceptanceTester $I)
    {
        #labels das do form de pesquisa
        $form = '//*[@id="FornecedorIndexForm"]/div[2]/';
        $I->see('Tipo de Fornecedor', "$form/div[1]/div/div/label");
        $I->see('CNPJ / CPF', "$form/div[2]/div/div/label");
        $I->see('Razão Social / Nome', "$form/div[3]/div/div/label");
        $I->see('Área', "$form/div[4]/div/div/label");

        #labels das colunas da grid
        $grid = '/html/body/div[4]/div/div[2]/div/div/div/table/thead/tr/';
        $I->see('CNPJ / CPF', "$grid/th[1]");
        $I->see('Razão Social / Nome', "$grid/th[2]");
        $I->see('Área', "$grid/th[3]");
        $I->see('E-mail', "$grid/th[4]");
        $I->see('Telefone', "$grid/th[5]");
        $I->see('Ações', "$grid/th[6]");
    }

}
