<?php

class MercurioFornecedores001COREValidacaoDoCampoTipoDeFornecedorTelaDePesquisaCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function VerificarValoresDoCampoTipoDeFornecedorNaTelaPesquisa(AcceptanceTester $I)
    {
        $I->seeElement('select', ['id' => 'FornecedorTpFornecedor']);

        # Garante que as opções do combobox do tipo de fornecedor são Selecione.., Pessoa Física e PessoaJurídica
        $I->assertEquals('Selecione..', $I->grabTextFrom('form select[id=FornecedorTpFornecedor] option:nth-child(1)'));
        $I->assertEquals('Pessoa Jurídica', $I->grabTextFrom('form select[id=FornecedorTpFornecedor] option:nth-child(2)'));
        $I->assertEquals('Pessoa Física', $I->grabTextFrom('form select[id=FornecedorTpFornecedor] option:nth-child(3)'));

        # Confirma que o valor default é Pessoa Jurídica
        $I->seeInField('#FornecedorTpFornecedor', 'Pessoa Jurídica');
    }

}