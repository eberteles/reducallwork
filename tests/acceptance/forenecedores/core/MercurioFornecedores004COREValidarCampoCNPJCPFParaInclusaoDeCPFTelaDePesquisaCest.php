<?php

class MercurioFornecedores004COREValidarCampoCNPJCPFParaInclusaoDeCPFTelaDePesquisaCest
{

    /**
     *
     * @var string Xpath do combobox do tipo de fornecedor
     */
    private $campoMascaraCpfCnpj = 'data[Fornecedor][nu_cnpj]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
        # seleciona a opção Pessoa Física, tem que aplicar máscara de CPF
        $I->selectOption('#FornecedorTpFornecedor', 'F');
    }

    public function InserirNoCampoCaracteresAlfabeticosEspaçoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $valorCpf = 'abc &[]{}  *¨%';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirNumerosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '123456789012';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirMenosQue11DigitosERetirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#FornecedorNuCnpj', WebDriverKeys::TAB);
        $I->assertEquals('', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirUmCpfInvalidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '222.222.222-22';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#FornecedorNuCnpj', WebDriverKeys::TAB);
        $I->see('CNPJ / CPF inválido');
    }

    public function InserirMaisQue11DigitosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345678901234576';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

}
