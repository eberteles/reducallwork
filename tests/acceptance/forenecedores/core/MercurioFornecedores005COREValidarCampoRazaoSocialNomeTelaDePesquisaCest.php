<?php

class MercurioFornecedores005COREValidarCampoRazaoSocialNomeTelaDePesquisaCest
{

    /**
     *
     * @var string Xpath do input do nome da razao social
     */
    private $campoNomeRazaoSocial = 'data[Fornecedor][no_razao_social]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function InserirNoCampoCaracteresAlfabeticosEspaçoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $nome = 'abc &[]{}  *¨%';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function Inserir100DigitosCaracteresNoCampo(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Meu Deus Que Nome Grande';
        $nome .= 'Com exatos 100 caracteres não é mesmo?';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function InserirMaisQue100DigitosCaracteresNoCampo(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Meu Deus Que Nome Grande';
        $nome .= 'Com mais que 100 caracteres não é mesmo AAAAAAAAAAAHHHHHHHHH';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals(substr($nome, 0, 102), $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

}
