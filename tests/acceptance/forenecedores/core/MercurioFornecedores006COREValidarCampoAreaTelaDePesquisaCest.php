<?php

class MercurioFornecedores006COREValidarCampoAreaTelaDePesquisaCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function DeveApresentarOpcoesParaSelecaoEUmCampoParaPesquisa(AcceptanceTester $I)
    {
        # Seleciona um valor no combobox do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[2]');
    }

}
