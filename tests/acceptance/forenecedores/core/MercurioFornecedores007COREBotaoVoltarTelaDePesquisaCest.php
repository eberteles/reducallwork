<?php

class MercurioFornecedores007COREBotaoVoltarTelaDePesquisaCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function ClicarNoCampoVoltar(AcceptanceTester $I)
    {
        $I->click('Voltar');
        $I->see('Dashboard');
    }

}
