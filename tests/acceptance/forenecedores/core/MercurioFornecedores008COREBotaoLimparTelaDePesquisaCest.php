<?php

class MercurioFornecedores008COREBotaoLimparTelaDePesquisaCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function ClicarNoCampoLimpar(AcceptanceTester $I)
    {
        $I->selectOption('#FornecedorTpFornecedor', 'J');
        $I->fillField('data[Fornecedor][nu_cnpj]', '12.345.678/9012-34');
        $I->fillField('data[Fornecedor][no_razao_social]', 'Nome da empresa');
        # Seleciona um valor no combobox do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[2]');

        $I->click('Limpar');
        $I->seeInField('#FornecedorTpFornecedor', '');
        $I->seeInField('data[Fornecedor][nu_cnpj]', '');
        $I->seeInField('data[Fornecedor][no_razao_social]', '');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

}
