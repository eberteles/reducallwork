<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores009COREPesquisarFornecedorPorMeioDoCampoTipoDeFornecedorComRegistrosCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function PesquisarPorPessoaJuridica(AcceptanceTester $I)
    {
        # seleciona a opção Pessoa Jurídica
        $I->selectOption('#FornecedorTpFornecedor', 'J');
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        $I->fillField('data[Fornecedor][no_razao_social]', '');

        # Seleciona o valor 'Selecione...' no combobox do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');

        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
        });
    }

    public function PesquisarPorPessoaFisica(AcceptanceTester $I)
    {
        # seleciona a opção Pessoa Física
        $I->selectOption('#FornecedorTpFornecedor', 'F');
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        $I->fillField('data[Fornecedor][no_razao_social]', '');

        # Seleciona o valor 'Selecione...' no combobox do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');

        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
        });
    }

}
