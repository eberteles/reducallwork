<?php

class MercurioFornecedores010COREPesquisarFornecedorPorMeioDoCampoTipoDeFornecedorSemOsRegistrosCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
    }

    public function PesquisarPorPessoaJuridica(AcceptanceTester $I)
    {
        # seleciona a opção Pessoa Jurídica
        $I->selectOption('#FornecedorTpFornecedor', 'J');
        # pesquisa intencionalmente por um nome que não existe
        $I->fillField('data[Fornecedor][nu_cnpj]', 'isudfhids');

        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        $I->see('Nenhum registro encontrado');
    }

    public function PesquisarPorPessoaFisica(AcceptanceTester $I)
    {
        # seleciona a opção Pessoa Fpisica
        $I->selectOption('#FornecedorTpFornecedor', 'F');
        # pesquisa intencionalmente por um nome que não existe
        $I->fillField('data[Fornecedor][nu_cnpj]', 'isudfhids');

        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        $I->see('Nenhum registro encontrado');
    }

}
