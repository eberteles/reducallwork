<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores012COREPesquisarFornecedorPorMeioDoCampoCNPJCPFComRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma que têm linhas|registros na tabela
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
        });
    }

    public function PesquisarPorCnpj(AcceptanceTester $I)
    {
        # pesquisar por um cnpj conhecido
        $I->fillField('data[Fornecedor][nu_cnpj]', '04.238.297/0001-89');
    }

    public function PesquisarPorCpf(AcceptanceTester $I)
    {
        # pesquisar por um cpf conhecido
        $I->fillField('data[Fornecedor][nu_cnpj]', '685.010.542-67');
    }

}
