<?php

class MercurioFornecedores013COREPesquisarFornecedorPorMeioDoCampoCNPJCPFSemOsRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');

        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma a mensagem de que não encontrou registros
        $I->see('Nenhum registro encontrado');
    }

    public function PesquisarPorCnpj(AcceptanceTester $I)
    {
        # pesquisar por um cnpj válido porém nao cadastrado
        $I->fillField('data[Fornecedor][nu_cnpj]', '78.664.228/0001-73');
    }

    public function PesquisarPorCpf(AcceptanceTester $I)
    {
        # pesquisar por um cpf válido porém não cadastrado
        $I->fillField('data[Fornecedor][nu_cnpj]', '766.423.654-10');
    }

}
