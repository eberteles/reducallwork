<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores016COREPesquisarFornecedorPorMeioDoCampoRazaoSocialNomeParteDoNomeDistincaoDeMaiusculosOuAcentosComOsRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');

        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma que têm exatamente quatro linhas|registros na tabela
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertCount(4, $rows);
        });
    }

    public function PesquisarPorParteDaRazaoSocialEmMaiusculoSemAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'A UNIAO');
    }

    public function PesquisarPorParteDaRazaoSocialEmMinusculoSemAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'a uniao');
    }

    public function PesquisarPorParteDaRazaoSocialEmMaiusculoComAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'A UNIÃO');
    }

    public function PesquisarPorParteDaRazaoSocialEmMinusculoComAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'a união');
    }

    public function PesquisarPorParteDoNomeEmMaiusculoSemAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'BELTRANO');
    }

    public function PesquisarPorParteDoNomeEmMinusculoSemAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'beltrano');
    }

    public function PesquisarPorParteDoNomeEmMaiusculoComAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'BELTRÂNO');
    }

    public function PesquisarPorParteDoNomeEmMinusculoComAcentuacao(AcceptanceTester $I)
    {
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', 'beltrâno');
    }

}
