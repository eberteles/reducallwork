<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores017COREPesquisarFornecedorPorMeioDoCampoRazaoSocialNomeTamanhoMaximoComOsRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');

        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma que cotêm linhas|registros na tabela
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
        });
    }

    public function PesquisarPorRazaoSocialComExatamente220Caracteres(AcceptanceTester $I)
    {
        $nome = 'Renan Ferreira Rodrigues Isabella Ribeiro Souza Luis Barbosa Alves';
        $nome .= ' Luis Costa Gomes Thaís Melo Cunha Letícia Ferreira Rocha Julia Sousa';
        $nome .= ' Martins Luis Costa Gomes Thaís Melo Cunha Letícia Ferreira Rocha';
        $nome .= ' Julia Sousa Martins';
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', $nome);
    }

    public function PesquisarPorNomeExatamente220Caracteres(AcceptanceTester $I)
    {
        $nome = 'Fornecedor Com Nome De Empresa Legal Com Duzentos E Vinte';
        $nome .= ' Caracteres Para Ser Pesquisado No Caso De Pesquisar Fornecedores';
        $nome .= ' Com Exatos Duzentos E Vinte Caracteres No Nome E Aparecer Resultado';
        $nome .= ' Na Grid Nao Sei Mais O Que Es';
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', $nome);
    }

}
