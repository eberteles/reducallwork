<?php

class MercurioFornecedores018COREPesquisarFornecedorPorMeioDoCampoRazaoSocialNomeSemOsRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');

        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma que exibe a mensagem de que não encontrou nenhum registro
        $I->see('Nenhum registro encontrado');
    }

    public function PesquisarPorRazaoSocialQueNaoExisteNoBanco(AcceptanceTester $I)
    {
        $nome = 'Razao Social Que Sei Que Nao Existe No Banco De Dados';
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', $nome);
    }

    public function PesquisarPorNomeQueNaoExisteNoBanco(AcceptanceTester $I)
    {
        $nome = 'Nome Que Sei Que Não Existe No Banco';
        # pesquisar por um nome de um fornecedor válido cadastrado no sistema
        $I->fillField('data[Fornecedor][no_razao_social]', $nome);
    }

}
