<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores020COREPesquisarFornecedorPorMeioDoCampoAreaSemOsRegistrosPesquisadosEstaremCadastradosCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        # deixar o campo nome de um fornecedor
        $I->fillField('data[Fornecedor][no_razao_social]', '');
    }

    public function _after(AcceptanceTester $I)
    {
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
        # confirma que têm exatamente quatro linhas|registros na tabela
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertEmpty($rows);
        });
    }

    public function PesquisarPorArea(AcceptanceTester $I)
    {
        # Seleciona o valor 'ÁREA 3' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[4]');
        $I->assertEquals('ÁREA 3', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
    }

}
