<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores021CORETelaDadosDoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        # deixar o campo nome de um fornecedor
        $I->fillField('data[Fornecedor][no_razao_social]', '');
        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');

        # clica no botão 'Mostrar' do primeiro registro que encontrar
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
            $I->click('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr[1]/td[6]/div/a[1]');
        });
    }

    public function VerificaCamposDaTela(AcceptanceTester $I)
    {
        $xPathDaTabela = '/html/body/div[4]/div/div[2]/div/div/div/div[3]/table[%d]/tbody/tr[%d]/th[%d]';
        $I->assertEquals('FORNECEDOR', $I->grabTextFrom(sprintf($xPathDaTabela, 1, 1, 1)));
        $I->assertEquals('Tipo de Fornecedor', $I->grabTextFrom(sprintf($xPathDaTabela, 1, 2, 1)));
        $I->assertEquals('CNPJ / CPF', $I->grabTextFrom(sprintf($xPathDaTabela, 1, 2, 2)));
        $I->assertEquals('Razão Social / Nome', $I->grabTextFrom(sprintf($xPathDaTabela, 1, 2, 3)));

        $I->assertEquals('CONTATO', $I->grabTextFrom(sprintf($xPathDaTabela, 2, 1, 1)));
        $I->assertEquals('E-mail', $I->grabTextFrom(sprintf($xPathDaTabela, 2, 2, 1)));
        $I->assertEquals('Telefone', $I->grabTextFrom(sprintf($xPathDaTabela, 2, 2, 2)));
        $I->assertEquals('Fax', $I->grabTextFrom(sprintf($xPathDaTabela, 2, 2, 3)));

        $I->assertEquals('GRUPO RESPONSÁVEL', $I->grabTextFrom(sprintf($xPathDaTabela, 3, 1, 1)));
        $I->assertEquals('Responsável', $I->grabTextFrom(sprintf($xPathDaTabela, 3, 2, 1)));
        $I->assertEquals('CPF', $I->grabTextFrom(sprintf($xPathDaTabela, 3, 2, 2)));

        $I->assertEquals('ENDEREÇO', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 1, 1)));
        $I->assertEquals('Município/UF', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 2, 1)));
        $I->assertEquals('Endereço', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 2, 2)));
        $I->assertEquals('Bairro', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 2, 3)));
        $I->assertEquals('Número', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 2, 4)));
        $I->assertEquals('CEP', $I->grabTextFrom(sprintf($xPathDaTabela, 4, 2, 5)));

        $I->assertEquals('DADOS BANCÁRIOS', $I->grabTextFrom(sprintf($xPathDaTabela, 5, 1, 1)));
        $I->assertEquals('Banco', $I->grabTextFrom(sprintf($xPathDaTabela, 5, 2, 1)));
        $I->assertEquals('Agência', $I->grabTextFrom(sprintf($xPathDaTabela, 5, 2, 2)));
        $I->assertEquals('Conta', $I->grabTextFrom(sprintf($xPathDaTabela, 5, 2, 3)));

        $I->assertEquals('OBSERVAÇÕES', $I->grabTextFrom(sprintf($xPathDaTabela, 6, 1, 1)));
        $I->assertEquals('Área', $I->grabTextFrom(sprintf($xPathDaTabela, 6, 2, 1)));
        $I->assertEquals('Observação', $I->grabTextFrom(sprintf($xPathDaTabela, 6, 2, 2)));
    }

}
