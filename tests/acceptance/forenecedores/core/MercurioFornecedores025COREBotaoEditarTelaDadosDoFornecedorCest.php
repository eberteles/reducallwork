<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores025COREBotaoEditarTelaDadosDoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        # deixar o campo nome de um fornecedor
        $I->fillField('data[Fornecedor][no_razao_social]', '');
        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');

        # clica no botão 'Mostrar' do primeiro registro que encontrar
        # e depois no botão de editar na tela de detalhamento
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
            $I->click('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr[1]/td[6]/div/a[1]');
            $I->click('Editar');
        });
    }

    public function VerificaLabelsDaTela(AcceptanceTester $I)
    {
        $I->see('Fornecedor');
        $I->see('Tipo de Fornecedor');
        $I->see('CNPJ / CPF');
        $I->see('Razão Social / Nome');
        $I->see('É Fornecedor do Órgão?');

        $I->see('Contato');
        $I->see('E-mail');
        $I->see('Telefone');
        $I->see('Fax');

        $I->see('Grupo Responsável');
        $I->see('Responsável');
        $I->see('CPF');

        $I->see('Endereço');
        $I->assertEquals('Endereço', $I->grabTextFrom('//*[@id="FornecedorEditForm"]/div[3]/div[2]/div[2]/div/div/div[1]/div[1]/label'));
        $I->see('Número');
        $I->see('Cep');
        $I->see('UF');
        $I->see('Município');
        $I->see('Bairro');

        $I->see('Dados Bancários');
        $I->see('Banco');
        $I->see('Agência');
        $I->see('Conta');

        $I->see('Observações');
        $I->see('Área');
        $I->see('Observação');
    }

    public function VerificaCamposDaTela(AcceptanceTester $I)
    {
        $I->seeElement('select', ['name' => 'data[Fornecedor][tp_fornecedor]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cnpj]']);
        $I->seeElement('textarea', ['name' => 'data[Fornecedor][no_razao_social]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][is_fornecedor_orgao]']);

        $I->seeElement('input', ['name' => 'data[Fornecedor][ds_email]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_telefone]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_fax]']);

        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cpf_responsavel]']);
        
        $I->seeElement('input', ['name' => 'data[Fornecedor][ds_endereco]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cep]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][sg_uf]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][co_municipio]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][bairro]']);

        $I->seeElementInDOM('select', ['name' => 'data[Fornecedor][co_banco]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_agencia]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_conta]']);

        $I->seeElementInDOM('select', ['name' => 'data[Fornecedor][co_area]']);
        $I->seeElement('textarea', ['name' => 'data[Fornecedor][ds_observacao]']);
    }

}
