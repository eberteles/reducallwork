<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;

class MercurioFornecedores026COREBotaoExcluirTelaFornecedoresCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');

        # deixar o campo tipo do fornecedor sem selecionar
        $I->selectOption('#FornecedorTpFornecedor', '');
        # deixar o campo cpf cnpj em branco
        $I->fillField('data[Fornecedor][nu_cnpj]', '');
        # deixar o campo nome de um fornecedor
        $I->fillField('data[Fornecedor][no_razao_social]', '');
        # Seleciona o valor 'Selecione...' no combobox de Área do twitter bootstrap
        $I->click('//*[@id="FornecedorCoArea_chosen"]/a/span');
        $I->click('//*[@id="FornecedorCoArea_chosen"]/div/ul/li[1]');
        $I->assertEquals('Selecione...', $I->grabTextFrom('//*[@id="FornecedorCoArea_chosen"]/a/span'));
        # botão pesquisar
        $I->click('//*[@id="FornecedorIndexForm"]/div[3]/div/button[1]');
    }

    public function VerificaMensagemDoBotaoExcluir(AcceptanceTester $I)
    {
        # clica no botão 'Excluir' do primeiro registro que encontrar
        $I->executeInSelenium(function(RemoteWebDriver $webDriver) use($I) {
            $rows = $webDriver->findElements(WebDriverBy::xpath('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr'));
            $I->assertNotEmpty($rows);
            $I->click('/html/body/div[4]/div/div[2]/div/div/div/table/tbody/tr[1]/td[6]/div/a[3]/i');
            $mensagemDeExclusao = 'Tem certeza de que deseja excluir ? Essa exclusão é lógica';
            $mensagemDeExclusao .= ' e será registrada nos logs. Os registros que já possuem';
            $mensagemDeExclusao .= ' esse item atrelado não sofrerão alterações e os novos';
            $mensagemDeExclusao .= ' registros não conseguirão mais associar este item';
            $I->seeInPopup($mensagemDeExclusao);
            $I->cancelPopup();
        });
    }

}
