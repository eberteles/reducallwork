<?php

class MercurioFornecedores028COREApresentacaoDaTelaParaCadastroDeNovoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
        $I->click('Novo Fornecedor');
        $I->seeCurrentUrlEquals('/fornecedores/add');
    }

    public function VerificaLabelsDaTela(AcceptanceTester $I)
    {
        $I->see('Fornecedor');
        $I->see('Tipo de Fornecedor');
        $I->see('CNPJ / CPF');
        $I->see('Razão Social / Nome');
        $I->see('É Fornecedor do Órgão?');

        $I->see('Contato');
        $I->see('E-mail');
        $I->see('Telefone');
        $I->see('Fax');

        $I->see('Grupo Responsável');
        $I->see('Responsável');
        $I->see('CPF');

        $I->see('Endereço');
        $I->assertEquals('Endereço', $I->grabTextFrom('//*[@id="FornecedorAddForm"]/div[3]/div[2]/div[2]/div/div/div[1]/div[2]/label'));
        $I->see('Número');
        $I->see('Cep');
        $I->see('UF');
        $I->see('Município');
        $I->see('Bairro');

        $I->see('Dados Bancários');
        $I->see('Banco');
        $I->see('Agência');
        $I->see('Conta');

        $I->see('Observações');
        $I->see('Área');
        $I->see('Observação');
    }

    public function VerificaCamposDaTela(AcceptanceTester $I)
    {
        $I->seeElement('select', ['name' => 'data[Fornecedor][tp_fornecedor]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cnpj]']);
        $I->seeElement('textarea', ['name' => 'data[Fornecedor][no_razao_social]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][is_fornecedor_orgao]']);

        $I->seeElement('input', ['name' => 'data[Fornecedor][ds_email]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_telefone]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_fax]']);

        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cpf_responsavel]']);

        $I->seeElement('input', ['name' => 'data[Fornecedor][ds_endereco]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_cep]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][sg_uf]']);
        $I->seeElement('select', ['name' => 'data[Fornecedor][co_municipio]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][bairro]']);

        $I->seeElementInDOM('select', ['name' => 'data[Fornecedor][co_banco]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_agencia]']);
        $I->seeElement('input', ['name' => 'data[Fornecedor][nu_conta]']);

        $I->seeElementInDOM('select', ['name' => 'data[Fornecedor][co_area]']);
        $I->seeElement('textarea', ['name' => 'data[Fornecedor][ds_observacao]']);
    }

}
