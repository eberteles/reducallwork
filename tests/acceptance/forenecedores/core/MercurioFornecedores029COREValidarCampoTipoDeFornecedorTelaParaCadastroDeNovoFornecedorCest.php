<?php

class MercurioFornecedores029COREValidarCampoTipoDeFornecedorTelaParaCadastroDeNovoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores');
        $I->click('Novo Fornecedor');
        $I->seeCurrentUrlEquals('/fornecedores/add');
    }

    public function VerificarValoresDoCampoTipoDeFornecedor(AcceptanceTester $I)
    {
        $I->seeElement('select', ['id' => 'FornecedorTpFornecedor']);

        # Garante que as opções do combobox do tipo de fornecedor são Selecione.., Pessoa Física e PessoaJurídica
        $I->assertEquals('Pessoa Jurídica', $I->grabTextFrom('form select[id=FornecedorTpFornecedor] option:nth-child(1)'));
        $I->assertEquals('Pessoa Física', $I->grabTextFrom('form select[id=FornecedorTpFornecedor] option:nth-child(2)'));
    }

}
