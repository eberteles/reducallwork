<?php

class MercurioFornecedores030COREValidarCampoCNPJCPFDeAcordoComOCampoTipoDeFornecedorTelaParaCadastroDeNovoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificaMascaraDeCpfCnpjNaTelaPesquisa(AcceptanceTester $I)
    {
        # seleciona a opção Pessoa Jurídica, tem que aplicar máscara de CNPJ
        $I->selectOption('#FornecedorTpFornecedor', 'J');
        $campoMascaraCpfCnpj = 'data[Fornecedor][nu_cnpj]';
        $valorCnpj = '11.222.333/4444-55';
        $I->fillField($campoMascaraCpfCnpj, $valorCnpj);
        $I->assertEquals($valorCnpj, $I->grabValueFrom($campoMascaraCpfCnpj));

        # seleciona a opção Pessoa Física, tem que aplicar máscara de CPF
        $I->selectOption('#FornecedorTpFornecedor', 'F');
        $valorCpf = '123.456.789-00';
        $I->fillField($campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals($valorCpf, $I->grabValueFrom($campoMascaraCpfCnpj));
    }

}
