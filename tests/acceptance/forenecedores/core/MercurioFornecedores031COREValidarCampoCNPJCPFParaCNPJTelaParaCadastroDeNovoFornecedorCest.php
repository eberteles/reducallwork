<?php

class MercurioFornecedores031COREValidarCampoCNPJCPFParaCNPJTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do combobox do tipo de fornecedor
     */
    private $campoMascaraCpfCnpj = 'data[Fornecedor][nu_cnpj]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
         # seleciona a opção Pessoa Jurídica, tem que aplicar máscara de CNPJ
        $I->selectOption('#FornecedorTpFornecedor', 'J');
    }

    public function InserirNoCampoCaracteresAlfabeticos(AcceptanceTester $I)
    {
        $valorCnpj = 'abcdefg';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        $I->assertEquals('__.___.___/____-__', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirNoCampoCaracteresEspaçoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $valorCnpj = '  &[]{}  *¨%';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        $I->assertEquals('__.___.___/____-__', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirNumerosNoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '12345678901234';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        $I->assertEquals('12.345.678/9012-34', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirMenosQue14DigitosERetirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '12345';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->assertEquals('', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirMaisQue14DigitosNoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '12345678901234576';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        $I->assertEquals('12.345.678/9012-34', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirUmCNPJInvalidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '22.222.222/2222-22';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('O CNPJ/CPF digitado é inválido! Por favor insira outro');
    }

    public function InserirUmCNPJValidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '62.384.444/0001-25';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCnpj);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('CNPJ válido!');
    }


}
