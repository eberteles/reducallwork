<?php

class MercurioFornecedores032COREValidarCampoCNPJCPFParaCPFTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do combobox do tipo de fornecedor
     */
    private $campoMascaraCpfCnpj = 'data[Fornecedor][nu_cnpj]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
        # seleciona a opção Pessoa Física, tem que aplicar máscara de CPF
        $I->selectOption('#FornecedorTpFornecedor', 'F');
    }

    public function InserirNoCampoCaracteresAlfabeticos(AcceptanceTester $I)
    {
        $valorCpf = 'abcdefg';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirNoCampoCaracteresEspaçoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $valorCpf = '  &[]{}  *¨%';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirNumerosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345678901';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirMenosQue11DigitosERetirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->assertEquals('', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirMaisQue11DigitosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345678901123';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoMascaraCpfCnpj));
    }

    public function InserirUmCpfInvalidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '222.222.222-22';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('O CNPJ/CPF digitado é inválido! Por favor insira outro');
    }

    public function InserirUmCpfValidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '641.734.774-57';
        $I->fillField($this->campoMascaraCpfCnpj, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('CPF válido!');
    }

}
