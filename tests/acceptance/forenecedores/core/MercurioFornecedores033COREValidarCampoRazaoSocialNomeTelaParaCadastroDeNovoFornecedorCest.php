<?php

class MercurioFornecedores033COREValidarCampoRazaoSocialNomeTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do textarea do nome, razão social do fornecedor
     */
    private $campoNomeRazaoSocial = 'data[Fornecedor][no_razao_social]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function InformarCaracteresAlfabeticosNoCampo(AcceptanceTester $I)
    {
        $nome = 'Nome João Da Silva';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function InformarEspacoEmBrancoECaracteresEspeciaisNoCampo(AcceptanceTester $I)
    {
        $nome = '      ^]}[{&  ** #@! `';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function InformarNumerosNoCampo(AcceptanceTester $I)
    {
        $nome = '12312321312357567569789';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function Informar220Digitos(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes Julian';
        $nome .= ' Almeida Pereira Erick Castro Azevedo Rafael Martins Pereira Laura Souza';
        $nome .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function InformarMaisQue220DigitosNoCampo(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes Julian';
        $nome .= ' Almeida Pereira Erick Castro Azevedo Rafael Martins Pereira Laura Souza';
        $nome .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $nome .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals(substr($nome, 0, 221), $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

    public function InformarCaixaAltaCaixaBaixaEAcentuacao(AcceptanceTester $I)
    {
        $nome = 'JOÃO mario CoNsTâNtInO Da SiLvÁ';
        $I->fillField($this->campoNomeRazaoSocial, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->campoNomeRazaoSocial));
    }

}
