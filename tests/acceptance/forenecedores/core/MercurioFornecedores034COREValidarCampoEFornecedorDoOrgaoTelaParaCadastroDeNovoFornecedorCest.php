<?php

class MercurioFornecedores034COREValidarCampoEFornecedorDoOrgaoTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do textarea do nome, razão social do fornecedor
     */
    private $campoNomeRazaoSocial = 'data[Fornecedor][no_razao_social]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarOCampoEFornecedorDoOrgao(AcceptanceTester $I)
    {
        $I->assertEquals('Sim', $I->grabTextFrom('//*[@id="FornecedorIsFornecedorOrgao"]/option[1]'));
        $I->assertEquals('Não', $I->grabTextFrom('//*[@id="FornecedorIsFornecedorOrgao"]/option[2]'));
    }

}
