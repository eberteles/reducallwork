<?php

class MercurioFornecedores035COREValidarCampoEmailTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do email
     */
    private $campoEmail = '//*[@id="FornecedorDsEmail"]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function InserirNoCampoCaracteresAlfabeticosEspacoEmBrancoCaracteresEspeciaisENumeros(AcceptanceTester $I)
    {
        $email = 'asd $%&* {[]} 349';
        $I->fillField($this->campoEmail, $email);
        $I->assertEquals($email, $I->grabValueFrom($this->campoEmail));
    }

    public function Inserir50DigitosCaracteresNoCampo(AcceptanceTester $I)
    {
        $email = 'diojasidjasiodjasiojdioasjdioasjdiojaioj@gmail.com';
        $I->fillField($this->campoEmail, $email);
        $I->assertEquals($email, $I->grabValueFrom($this->campoEmail));
    }

    public function InserirMaisQue50DigitosCaracteresNoCampo(AcceptanceTester $I)
    {
        $email = 'diojasidjasiodjasiojdioasjdioasjdiojaiojIUEHodgoidfjguihIuheuihiush@gmail.com';
        $I->fillField($this->campoEmail, $email);
        $I->assertEquals(substr($email, 0, 50), $I->grabValueFrom($this->campoEmail));
    }

    public function InformarUmEmailInvalidoPreencherTodosOsCamposObrigatoriosComDadosValidosEClicarNoBotaoSalvar(AcceptanceTester $I)
    {
        $I->fillField($this->campoEmail, 'jaca');
        $I->click('Salvar');
        $I->see('Informe um e-mail válido!');
    }

    public function InformarUmEmailValidoEJaCadastradoPreencherTodosOsCamposObrigatoriosComDadosValidosEClicarNoBotãoSalvar(AcceptanceTester $I)
    {
        $I->fillField($this->campoEmail, 'uniao@gmail.com');
        $I->click('Salvar');
        $I->see('E-mail já cadastrado.');
    }

}
