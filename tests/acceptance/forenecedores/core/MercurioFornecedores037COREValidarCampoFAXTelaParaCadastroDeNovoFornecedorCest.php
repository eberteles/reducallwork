<?php

class MercurioFornecedores037COREValidarCampoFAXTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do fax
     */
    private $campoFax = 'data[Fornecedor][nu_fax]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarSeOCampoApresentaMascara(AcceptanceTester $I)
    {
        $I->fillField($this->campoFax, 'a');
        $I->assertEquals('(__) ____-_____', $I->grabValueFrom($this->campoFax));
    }

    public function InserirNoCampoCaracteresAlfabeticosEspacoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $fax = 'asd $%&* {[]} +-';
        $I->fillField($this->campoFax, $fax);
        $I->assertEquals('(__) ____-_____', $I->grabValueFrom($this->campoFax));
    }

    public function Inserir11NumerosNoCampo(AcceptanceTester $I)
    {
        $fax = '(12) 3456-78901';
        $I->fillField($this->campoFax, $fax);
        $I->assertEquals($fax, $I->grabValueFrom($this->campoFax));
    }

    public function InserirMaisQue11NumerosNoCampo(AcceptanceTester $I)
    {
        $fax = '1234567898653456';
        $I->fillField($this->campoFax, $fax);
        $I->assertEquals('(12) 3456-78986', $I->grabValueFrom($this->campoFax));
    }

}
