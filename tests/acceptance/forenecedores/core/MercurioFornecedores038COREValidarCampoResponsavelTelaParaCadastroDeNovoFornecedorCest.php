<?php

class MercurioFornecedores038COREValidarCampoResponsavelTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do responsável
     */
    private $nomeDoResponsavel = 'data[Fornecedor][no_responsavel]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function InformarCaracteresAlfabeticosNoCampo(AcceptanceTester $I)
    {
        $nome = 'Nome João Da Silva';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->nomeDoResponsavel));
    }

    public function InformarEspacoEmBrancoECaracteresEspeciaisNoCampo(AcceptanceTester $I)
    {
        $nome = '      ^]}[{&  ** #@! `';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->nomeDoResponsavel));
    }

    public function InformarNumerosNoCampo(AcceptanceTester $I)
    {
        $nome = '12312321312357567569789';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->nomeDoResponsavel));
    }

    public function Informar100Digitos(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes';
        $nome .= ' Julian Almeida Pereir';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->nomeDoResponsavel));
    }

    public function InformarMaisQue100DigitosNoCampo(AcceptanceTester $I)
    {
        $nome = 'Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes Julian';
        $nome .= ' Almeida Pereira Erick Castro Azevedo Rafael Martins Pereira Laura Souza';
        $nome .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $nome .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals(substr($nome, 0, 100), $I->grabValueFrom($this->nomeDoResponsavel));
    }

    public function InformarCaixaAltaCaixaBaixaEAcentuacao(AcceptanceTester $I)
    {
        $nome = 'JOÃO mario CoNsTâNtInO Da SiLvÁ';
        $I->fillField($this->nomeDoResponsavel, $nome);
        $I->assertEquals($nome, $I->grabValueFrom($this->nomeDoResponsavel));
    }

}
