<?php

class MercurioFornecedores039COREValidarCampoCPFTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do responsável
     */
    private $campoCpf = 'data[Fornecedor][nu_cpf_responsavel]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarSeOCampoExibeMascara(AcceptanceTester $I)
    {
        $I->fillField($this->campoCpf, 'a');
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirNoCampoCaracteresAlfabeticos(AcceptanceTester $I)
    {
        $valorCpf = 'abcdefg';
        $I->fillField($this->campoCpf, $valorCpf);
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirNoCampoCaracteresEspaçoEmBrancoECaracteresEspeciais(AcceptanceTester $I)
    {
        $valorCpf = '  &[]{}  *¨%';
        $I->fillField($this->campoCpf, $valorCpf);
        $I->assertEquals('___.___.___-__', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirNumerosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345678901';
        $I->fillField($this->campoCpf, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirMenosQue11DigitosERetirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345';
        $I->fillField($this->campoCpf, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->assertEquals('', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirMaisQue11DigitosNoCampo(AcceptanceTester $I)
    {
        $valorCpf = '12345678901123';
        $I->fillField($this->campoCpf, $valorCpf);
        $I->assertEquals('123.456.789-01', $I->grabValueFrom($this->campoCpf));
    }

    public function InserirUmCpfInvalidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '222.222.222-22';
        $I->fillField($this->campoCpf, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('O CNPJ/CPF digitado é inválido! Por favor insira outro');
    }

    public function InserirUmCpfValidoETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCpf = '641.734.774-57';
        $I->fillField($this->campoCpf, $valorCpf);
        # Clica em tab para retirar o foco do campo e o JS exibir mensagem
        $I->pressKey('#nuCpf', WebDriverKeys::TAB);
        $I->waitForText('CPF válido!');
    }

}
