<?php

class MercurioFornecedores040COREValidarCampoCEPTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do cep
     */
    private $campoCep = 'data[Fornecedor][nu_cep]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarSeOCampoExibeMascara(AcceptanceTester $I)
    {
        $I->fillField($this->campoCep, 'a');
        $I->assertEquals('_____-___', $I->grabValueFrom($this->campoCep));
    }

    public function InserirNoCampoCaracteresAlfabeticos(AcceptanceTester $I)
    {
        $I->fillField($this->campoCep, 'abcdefg');
        $I->assertEquals('_____-___', $I->grabValueFrom($this->campoCep));
    }

    public function InformarEspacoEmBrancoECaracteresEspeciaisNoCampo(AcceptanceTester $I)
    {
        $I->fillField($this->campoCep, '      ^]}[{&  ** #@! `');
        $I->assertEquals('_____-___', $I->grabValueFrom($this->campoCep));
    }

    public function InformarNumerosNoCampo(AcceptanceTester $I)
    {
        $cep = '12345678';
        $I->fillField($this->campoCep, $cep);
        $I->assertEquals('12345-678', $I->grabValueFrom($this->campoCep));
    }

    public function InformarMenosQue8DigitosETirarOFocoDoCampo(AcceptanceTester $I)
    {
        $valorCnpj = '12345';
        $I->fillField($this->campoCep, $valorCnpj);
        # Clica em tab para retirar o foco do campo e o JS limpar o campo
        $I->pressKey('#FornecedorNuCep', WebDriverKeys::TAB);
        $I->assertEquals('', $I->grabValueFrom($this->campoCep));
    }

    public function InformarMaisQue8DigitosNoCampo(AcceptanceTester $I)
    {
        $cep = '1234567890123';
        $I->fillField($this->campoCep, $cep);
        $I->assertEquals('12345-678', $I->grabValueFrom($this->campoCep));
    }

}
