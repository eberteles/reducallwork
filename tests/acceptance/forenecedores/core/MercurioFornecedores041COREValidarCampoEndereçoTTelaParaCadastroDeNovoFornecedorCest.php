<?php

class MercurioFornecedores041COREValidarCampoEndereçoTTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do endereco
     */
    private $campoEndereco = 'data[Fornecedor][ds_endereco]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function InformarCaracteresAlfabeticosNoCampo(AcceptanceTester $I)
    {
        $endereco = 'Endereço legal';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals($endereco, $I->grabValueFrom($this->campoEndereco));
    }

    public function InformarEspacoEmBrancoECaracteresEspeciaisNoCampo(AcceptanceTester $I)
    {
        $endereco = '      ^]}[{&  ** #@! `';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals($endereco, $I->grabValueFrom($this->campoEndereco));
    }

    public function InformarNumerosNoCampo(AcceptanceTester $I)
    {
        $endereco = '12312321312357567569789';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals($endereco, $I->grabValueFrom($this->campoEndereco));
    }

    public function Informar100DigitosCaracteres(AcceptanceTester $I)
    {
        $endereco = '1234 Mario Contantino Da Silva Moura Daniel Pereira Fernandes';
        $endereco .= ' Julian Almeida Pereira Erick Castro Ab';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals($endereco, $I->grabValueFrom($this->campoEndereco));
    }

    public function InformarMaisQue100DigitosNoCampo(AcceptanceTester $I)
    {
        $endereco = 'Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes Julian';
        $endereco .= ' Almeida Pereira Erick Castro Azevedo Rafael Martins Pereira Laura Souza';
        $endereco .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $endereco .= ' Correia Lucas Carvalho Rocha Eduardo Pereira Azevedo Diogo Fernandes Gomes Júli';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals(substr($endereco, 0, 100), $I->grabValueFrom($this->campoEndereco));
    }

    public function InformarCaixaAltaCaixaBaixaEAcentuacao(AcceptanceTester $I)
    {
        $endereco = 'EnDeReÇo LeGal Do JOÃO mario CoNsTâNtInO Da SiLvÁ';
        $I->fillField($this->campoEndereco, $endereco);
        $I->assertEquals($endereco, $I->grabValueFrom($this->campoEndereco));
    }

}
