<?php

class MercurioFornecedores042COREValidarCampoNumeroTelaParaCadastroDeNovoFornecedorCest
{

    /**
     *
     * @var string Xpath do input do numero
     */
    private $campoNumero = 'data[Fornecedor][nu_endereco]';

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function InformarCaracteresAlfabeticosNoCampo(AcceptanceTester $I)
    {
        $numero = 'Numero do Ender';
        $I->fillField($this->campoNumero, $numero);
        $I->assertEquals($numero, $I->grabValueFrom($this->campoNumero));
    }

    public function InformarEspacoEmBrancoECaracteresEspeciaisNoCampo(AcceptanceTester $I)
    {
        $numero = ' ^]}[{& * #@! `';
        $I->fillField($this->campoNumero, $numero);
        $I->assertEquals($numero, $I->grabValueFrom($this->campoNumero));
    }

    public function InformarNumerosNoCampo(AcceptanceTester $I)
    {
        $numero = '123123213123575';
        $I->fillField($this->campoNumero, $numero);
        $I->assertEquals($numero, $I->grabValueFrom($this->campoNumero));
    }

    public function InformarMaisQue15DigitosCaracteresNoCampo(AcceptanceTester $I)
    {
        $numero = 'Numero Do endereco Joao Mario Contantino Da Silva Moura Daniel Pereira Fernandes Julian';
        $I->fillField($this->campoNumero, $numero);
        $I->assertEquals(substr($numero, 0, 15), $I->grabValueFrom($this->campoNumero));
    }

    public function InformarCaixaAltaCaixaBaixaEAcentuacao(AcceptanceTester $I)
    {
        $numero = 'NúMeRo EnDeReÇo';
        $I->fillField($this->campoNumero, $numero);
        $I->assertEquals($numero, $I->grabValueFrom($this->campoNumero));
    }

}
