<?php

class MercurioFornecedores043COREValidarCampoUFTelaParaCadastroDeNovoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarQuaisDadosSaoApresentadosNoCampoUF(AcceptanceTester $I)
    {
        $ufs = array(
            '',
            'AC',
            'AL',
            'AM',
            'AP',
            'BA',
            'CE',
            'DF',
            'ES',
            'GO',
            'MA',
            'MG',
            'MS',
            'MT',
            'PA',
            'PB',
            'PE',
            'PI',
            'PR',
            'RJ',
            'RN',
            'RO',
            'RR',
            'RS',
            'SC',
            'SE',
            'SP',
            'TO',
        );
        foreach ($ufs as $index => $uf) {
            $indiceDoOption = $index + 1;
            $xpath = "//*[@id='FornecedorSgUf']/option[{$indiceDoOption}]";
            $I->assertEquals($uf, $I->grabValueFrom($xpath));
        }
    }

}
