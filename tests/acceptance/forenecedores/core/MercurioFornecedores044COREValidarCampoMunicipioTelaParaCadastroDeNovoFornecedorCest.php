<?php

use Facebook\WebDriver\Remote\RemoteWebElement;

class MercurioFornecedores044COREValidarCampoMunicipioTelaParaCadastroDeNovoFornecedorCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->login('usr_fiscal', 'pwd_fiscal');
        $I->amOnPage('/fornecedores/add');
    }

    public function VerificarUfEVerificarOsDadosQueSaoApresentadosNoCampoMunicipio(AcceptanceTester $I)
    {
        $ufs = array(
            'AC' => array(
                'SELECIONE..',
                'ACRELANDIA',
                'ASSIS BRASIL',
                'BRASILEIA',
                'BUJARI',
                'CAPIXABA',
                'CRUZEIRO DO SUL',
                'EPITACIOLANDIA',
                'FEIJO',
                'JORDAO',
                'MANCIO LIMA',
                'MANOEL URBANO',
                'MARECHAL THAUMATURGO',
                'PLACIDO DE CASTRO',
                'PORTO WALTER',
                'RIO BRANCO',
                'RODRIGUES ALVES',
                'SANTA ROSA DO PURUS',
                'SENADOR GUIOMARD',
                'SENA MADUREIRA',
                'TARAUACA',
                'XAPURI',
                'PORTO ACRE'
            ),
            'AL' => ARRAY(
                'SELECIONE..',
                'AGUA BRANCA',
                'ANADIA',
                'ARAPIRACA',
                'ATALAIA',
                'BARRA DE SANTO ANTONIO',
                'BARRA DE SAO MIGUEL',
                'BATALHA',
                'BELEM',
                'BELO MONTE',
                'BOCA DA MATA',
                'BRANQUINHA',
                'CACIMBINHAS',
                'CAJUEIRO',
                'CAMPESTRE',
                'CAMPO ALEGRE',
                'CAMPO GRANDE',
                'CANAPI',
                'CAPELA',
                'CARNEIROS',
            ),
        );
        foreach ($ufs as $uf => $cidades) {
            $I->selectOption('#FornecedorSgUf', $uf);
            $I->wait(2);
            $indiceDoOption = 0;
            foreach ($cidades as $cidade) {
                $indiceDoOption++;
                $xpath = "//*[@id='FornecedorCoMunicipio']/option[{$indiceDoOption}]";
                $I->assertEquals($cidade, $I->grabTextFrom($xpath));
            }
        }
    }

}
