<?php

class MercurioTelaLoginCertificadoDigital003IconesCamposUsuárioSenhaBotoesCest
{

    protected function iconeProvider()
    {
        return [
            ['class' => 'icon-user', 'mensagem' => 'Não está aparecendo o ícone de usuário no input de login'],
            ['class' => 'icon-lock', 'mensagem' => 'Não está aparecendo o ícone do cadeado no input de senha'],
            ['class' => 'icon-arrow-right icon-white', 'mensagem' => 'Não está aparecendo o ícone de seta no botão "Entrar"'],
            ['class' => 'icon-remove', 'mensagem' => 'Não está aparecendo o ícone de X no botão "Fechar"'],
            ['class' => 'icon-refresh icon-white', 'mensagem' => 'Não está aparecendo o ícone de refresh no botão de "Esqueci a minha Senha"'],
        ];
    }

    /**
     * @dataprovider iconeProvider
     */
    public function IconesCamposUsuárioSenhaBotoesCest(AcceptanceTester $I, \Codeception\Example $icone)
    {
        $I->amOnPage('/usuarios/autenticar');
        $I->seeElement('i', ['class' => $icone['class']], $icone['mensagem']);
    }

}
