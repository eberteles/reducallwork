<?php

class MercurioTelaLoginCertificadoDigital004ElementosTelaLoginAcessoCest
{

    public function SimboloObrigatoriedadeCampos(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        $I->seeElement('input', ['name' => 'data[Usuario][no_usuario]']);
        $I->seeElement('input', ['name' => 'data[Usuario][ds_senha]']);

        $I->seeElement('input', ['id' => 'UsuarioCertificado', 'type' => 'checkbox']);
        $I->see('Acessar com Certificado Digital', '//span[@class="lbl"]');

        $I->see('Entrar', '//button[@id="signInButton"]');
        $I->see('Atualizar Certificados', '//button[@id="refreshButton"]');
        $I->see('Esqueci a minha Senha', '//a[@class="btn btn-small btn-primary"]');
        $I->see('Fechar', '//a[@class="btn btn-small"]');
    }

}
