<?php

class MercurioTelaLoginCertificadoDigital005FecharTelaUtilizandoElementoXCest
{

    public function FecharTelaUtilizandoElementoX(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        $I->waitForElementVisible('div#modal-login', 1);
        $I->click('×');
        $I->waitForElementNotVisible('div#modal-login', 1);
    }

}
