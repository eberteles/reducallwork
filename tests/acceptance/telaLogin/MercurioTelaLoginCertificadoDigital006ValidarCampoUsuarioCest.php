<?php

class MercurioTelaLoginCertificadoDigital006ValidarCampoUsuarioCest
{

    /**
     * @see https://n2o-ti.atlassian.net/wiki/spaces/GES/pages/13860886/006+-+Validar+o+campo+Usu+rio
     * @param AcceptanceTester $I
     */
    public function ValidarCampoUsuarioComCaracteresEspeciaisCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir letras, números, espaço em branco e caracteres especiais no campo "Usuário".
        $I->fillField('data[Usuario][no_usuario]', 'abcdef  12345*&¬\}][{¬¢£³²¹%$');
        # Confirma foi possível inserir a  informação anterior
        $I->assertSame('abcdef  12345*&¬\}][{¬¢£³²¹%$', $I->grabValueFrom('data[Usuario][no_usuario]'));
    }

    /**
     * @see https://n2o-ti.atlassian.net/wiki/spaces/GES/pages/13860886/006+-+Validar+o+campo+Usu+rio
     * @param AcceptanceTester $I
     */
    public function ValidarUsuarioValidoMenorQue16CaracteresCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir usuário válido menor que 16 caracteres.
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', 'pwd_fiscal');
        $I->click('Entrar');
        $I->see('Dashboard');
    }

    /**
     * @see https://n2o-ti.atlassian.net/wiki/spaces/GES/pages/13860886/006+-+Validar+o+campo+Usu+rio
     * @param AcceptanceTester $I
     */
    public function ValidarUsuarioValidoIgualA16CaracteresCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir usuário válido igual a 16 caracteres.
        $I->fillField('data[Usuario][no_usuario]', 'usr_com_16_carar');
        $I->fillField('data[Usuario][ds_senha]', 'qaz123');
        $I->click('Entrar');
        $I->see('Dashboard');
    }

    /**
     * @see https://n2o-ti.atlassian.net/wiki/spaces/GES/pages/13860886/006+-+Validar+o+campo+Usu+rio
     * @param AcceptanceTester $I
     */
    public function ValidarUsuarioValidoMaiorQue16CaracteresCest(AcceptanceTester $I)
    {
        $I->fail('Verificar este item depois porque parece que vai poder ter usuários com mais de 16 caracteres.');
    }

}
