<?php

class MercurioTelaLoginCertificadoDigital007ValidarcampoSenhaCest
{

    public function ValidarCampoSenhaComCaracteresEspeciaisCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir números, letras, espaço em branco e caracteres especiais no campo "Senha".
        $I->fillField('data[Usuario][ds_senha]', 'ab 123*&¬\£³²¹');
        # Confirma foi possível inserir a  informação anterior
        $I->assertSame('ab 123*&¬\£³²¹', $I->grabValueFrom('data[Usuario][ds_senha]'));
    }

    public function ValidarCampoSenhaComMenosDe15CaracteresCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir um usuário correspondente a uma senhaválida com menos de 15 caracteres.
        $I->fillField('data[Usuario][no_usuario]', 'usr_com_16_carar');
        # Inserir senha válida com menos de 15 caracteres
        $I->fillField('data[Usuario][ds_senha]', 'qaz123');
    }

    public function ValidarCampoSenhaComExatamente15CaracteresCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir um usuário correspondente a uma senhaválida com menos de 15 caracteres.
        $I->fillField('data[Usuario][no_usuario]', 'usr_senha_de_15');
        # Inserir senha válida com menos de 15 caracteres
        $I->fillField('data[Usuario][ds_senha]', 'senhacom15letra');
        $I->see('Dashboard');
    }

    public function ValidarCampoSenhaMaiorQue15CaracteresCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # Inserir um usuário correspondente a uma senhaválida com menos de 15 caracteres.
        $I->fillField('data[Usuario][no_usuario]', 'usr_senha_de_15');
        # Inserir senha válida com menos de 15 caracteres
        $I->fillField('data[Usuario][ds_senha]', 'senhaErradaMaiorQue15Caracteres');
        $I->see('Senha inválida.');
    }

}
