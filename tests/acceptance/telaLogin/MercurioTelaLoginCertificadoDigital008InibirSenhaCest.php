<?php

class MercurioTelaLoginCertificadoDigital008InibirSenhaCest
{

    public function InibirSenhaCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # valida se o elemento de informar a senha é do tipo password
        $I->seeElement(
                'input', [
                    'name' => 'data[Usuario][ds_senha]',
                    'type' => 'password'
                ]
        );
    }

}
