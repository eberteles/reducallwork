<?php

class MercurioTelaLoginCertificadoDigital009BotaoFecharCest
{

    public function BotaoFecharCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');
        $I->click('Fechar');
        $I->waitForElementNotVisible('div#modal-login', 1);
    }

}
