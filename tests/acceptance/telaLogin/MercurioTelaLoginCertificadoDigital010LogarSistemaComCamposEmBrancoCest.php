<?php

class MercurioTelaLoginCertificadoDigital010LogarSistemaComCamposEmBrancoCest
{

    public function ValidarCampoUsuarioESenhaVaziosCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # o campo usuário e senha será preenchido com string vazio
        $I->fillField('data[Usuario][no_usuario]', '');
        $I->fillField('data[Usuario][ds_senha]', '');

        $I->dontSee('Campo Usuário e/ou Senha inválido.');

        # será clicado em enviar e o texto "Campo Usuário e/ou Senha inválido." passará
        # a ser exibido
        $I->click('//button[@id="signInButton"]');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }

}
