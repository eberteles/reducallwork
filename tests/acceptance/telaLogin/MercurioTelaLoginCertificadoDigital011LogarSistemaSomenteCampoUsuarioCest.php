<?php
class MercurioTelaLoginCertificadoDigital011LogarSistemaSomenteCampoUsuarioCest
{
    public function ValidarSomenteCampoUsuarioCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # apenas o campo usuário é preenchido
        $I->fillField('data[Usuario][no_usuario]', 'usuario qualquer');

        $I->dontSee('Campo Usuário e/ou Senha inválido.');

        # será clicado em enviar e o texto "Campo Usuário e/ou Senha inválido." passará
        # a ser exibido
        $I->click('//button[@id="signInButton"]');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }
}
