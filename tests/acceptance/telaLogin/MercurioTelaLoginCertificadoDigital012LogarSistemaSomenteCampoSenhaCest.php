<?php
class MercurioTelaLoginCertificadoDigital012LogarSistemaSomenteCampoSenhaCest
{
    public function ValidarSomenteCampoSenhaCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # apenas o campo senha é preenchido
        $I->fillField('data[Usuario][ds_senha]', 'senha qualquer');

        $I->dontSee('Campo Usuário e/ou Senha inválido.');

        # será clicado em enviar e o texto "Campo Usuário e/ou Senha inválido." passará
        # a ser exibido
        $I->click('//button[@id="signInButton"]');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }
}
