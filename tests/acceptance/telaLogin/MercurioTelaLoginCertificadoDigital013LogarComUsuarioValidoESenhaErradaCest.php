<?php

class MercurioTelaLoginCertificadoDigital013LogarComUsuarioValidoESenhaErradaCest
{
    public function ValidarUsuarioValidoESenhaInvalidaCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # preencher o campo usuário com um usuário válido e a senha inválida
        # o usuário 'usr_fiscal' tem que estar previamente cadastrado no sistema
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', 'senha qualquer');

        $I->dontSee('Campo Usuário e/ou Senha inválido.');

        # será clicado em enviar e o será levada para outra tela com
        # a ME020
        $I->click('//button[@id="signInButton"]');
        $I->see('Senha inválida');

        #garante que tem os campos a seguir
        $I->see('Usuário');
        $I->see('Senha');
        $I->see('Entrar');
        $I->see('Esqueci a minha Senha');
        $I->see('Voltar');
    }
}
