<?php

use Codeception\Util\Locator;

class MercurioTelaLoginCertificadoDigital014LogarComUsuarioInvalidoEInformarSenhaErradaCest
{
    public function ValidarUsuarioESenhaInvalidosCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        # preencher o campo usuário com um usuário inválido e a senha inválida
        $I->fillField('data[Usuario][no_usuario]', 'usuario qualquer');
        $I->fillField('data[Usuario][ds_senha]', 'senha qualquer');

        $I->dontSee('Campo Usuário e/ou Senha inválido.');

        # será clicado em enviar e o será levada para outra tela com
        $I->click('//button[@id="signInButton"]');
        $I->see('Usuário não encontrado');

        #garante que tem os campos a seguir
        $I->seeElement('//img[@src="/img/logo_no2.png"]');
        $I->see('Usuário');
        $I->see('Senha');
        $I->see('Entrar');
        $I->see('Esqueci a minha Senha');
        $I->see('Voltar');
        $I->see('Sistema melhor visualizado em 1024x768');
    }

    public function _after(AcceptanceTester $I)
    {
        $I->assertRegExp('/© 2017 GESCON - Versão: [\s\S]+/', $I->grabTextFrom(Locator::lastElement('.sistema')));
    }
}
