<?php

class MercurioTelaLoginCertificadoDigital015BotaoVoltarDaTelaDeFalhaDeLoginCest
{
    public function ValidarBotaoDeVoltarLevaParaTelaDeLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('usuarios/autenticar/falha');

        # clica no botão voltar
        $I->click('Voltar');
        # confirma que foi redirecionado para a tela de autenticar
        $I->seeCurrentURLEquals('/usuarios/autenticar');
    }
}
