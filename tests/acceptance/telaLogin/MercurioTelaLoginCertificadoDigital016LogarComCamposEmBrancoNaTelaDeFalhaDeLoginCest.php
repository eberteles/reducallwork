<?php

class MercurioTelaLoginCertificadoDigital016LogarComCamposEmBrancoNaTelaDeFalhaDeLoginCest
{
    public function ValidarCampoDeUsuarioESenhaEmBrancoCest(AcceptanceTester $I)
    {
        $I->amOnPage('usuarios/autenticar/falha');

        # o campo usuário e senha será preenchido com string vazio
        $I->fillField('data[Usuario][no_usuario]', '');
        $I->fillField('data[Usuario][ds_senha]', '');

        # será clicado em enviar e o texto "Campo Usuário e/ou Senha inválido." passará
        # a ser exibido
        $I->click('Entrar');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }
}
