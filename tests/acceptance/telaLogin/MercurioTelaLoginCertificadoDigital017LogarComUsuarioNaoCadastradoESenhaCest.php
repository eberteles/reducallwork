<?php

class MercurioTelaLoginCertificadoDigital017LogarComUsuarioNaoCadastradoESenhaCest
{
    public function ValidarCampoDeUsuarioCadastradoESenhaCest(AcceptanceTester $I)
    {
        $I->amOnPage('usuarios/autenticar/falha');

        $I->fillField('data[Usuario][no_usuario]', 'usuario inexistente');
        $I->fillField('data[Usuario][ds_senha]', 'senha qualquer');

        $I->click('Entrar');
        $I->see('Usuário não encontrado.');
    }
}
