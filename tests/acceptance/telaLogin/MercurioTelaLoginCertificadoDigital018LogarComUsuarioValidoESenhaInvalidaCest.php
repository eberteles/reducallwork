<?php

class MercurioTelaLoginCertificadoDigital018LogarComUsuarioValidoESenhaInvalidaCest
{
    public function ValidarCampoDeUsuarioValidoESenhaInvalidaCest(AcceptanceTester $I)
    {
        $I->amOnPage('usuarios/autenticar/falha');

        # o usuario usr_fiscal tem que estar previamente cadastrado no banco
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', 'senha qualquer');

        $I->click('Entrar');
        $I->see('Senha inválida.');
    }
}
