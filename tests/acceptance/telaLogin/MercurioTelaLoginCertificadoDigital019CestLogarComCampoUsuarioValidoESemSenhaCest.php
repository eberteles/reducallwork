<?php

class MercurioTelaLoginCertificadoDigital019CestLogarComCampoUsuarioValidoESemSenhaCest
{
    public function ValidarCampoDeUsuarioValidoESenhaInvalidaCest(AcceptanceTester $I)
    {
        $I->amOnPage('usuarios/autenticar/falha');

        # o usuario usr_fiscal tem que estar previamente cadastrado no banco
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', '');

        $I->click('Entrar');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }
}
