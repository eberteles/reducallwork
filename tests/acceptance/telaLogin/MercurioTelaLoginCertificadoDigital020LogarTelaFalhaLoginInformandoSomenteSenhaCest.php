<?php

class MercurioTelaLoginCertificadoDigital020LogarTelaFalhaLoginInformandoSomenteSenhaCest
{

    public function LogarTelaFalhaLoginInformandoSomenteSenhaCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar/falha');
        $I->dontSee('Campo Usuário e/ou Senha inválido.');
        $I->fillField('data[Usuario][no_usuario]', '');
        $I->fillField('data[Usuario][ds_senha]', 'pwd_fake');
        $I->click('Entrar');
        $I->see('Campo Usuário e/ou Senha inválido.');
    }

}
