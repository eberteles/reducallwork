<?php
class MercurioTelaLoginCertificadoDigital021IconeBotoesCamposTelaFalhaLoginCest
{
    public function IconeBotoesCamposTelaFalhaLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar/falha');

        $I->seeElement('i', ['class' => 'icon-user']);
        $I->seeElement('i', ['class' => 'icon-lock']);
        $I->seeElement('i', ['class' => 'icon-arrow-right icon-white']);
        $I->seeElement('i', ['class' => 'icon-plus icon-white']);
        $I->seeElement('i', ['class' => 'icon-arrow-left icon-envelope']);
    }
}
