<?php

class MercurioTelaLoginCertificadoDigital022LogarTelaFalhaLoginUsuarioSenhaValidosCest
{

    public function LogarTelaFalhaLoginUsuarioSenhaValidosCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar/falha');
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', 'pwd_fiscal');
        $I->click('Entrar');
        $I->see('Dashboard');
    }

}
