<?php

class MercurioTelaLoginCertificadoDigital023LogarSistemaUsuarioSenhaValidosCest
{

    public function LogarSistemaUsuarioSenhaValidosCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');
        $I->fillField('data[Usuario][no_usuario]', 'usr_fiscal');
        $I->fillField('data[Usuario][ds_senha]', 'pwd_fiscal');
        $I->click('Entrar');
        $I->see('Dashboard');
    }

}
