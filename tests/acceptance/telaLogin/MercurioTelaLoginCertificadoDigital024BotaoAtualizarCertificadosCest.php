<?php

class MercurioTelaLoginCertificadoDigital024BotaoAtualizarCertificadosCest
{

    public function BotaoAtualizarCertificadosCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');
        $I->fillField('data[Usuario][no_usuario]', '');
        $I->fillField('data[Usuario][ds_senha]', '');
        $I->click('Atualizar Certificados');
        $I->see('Aguarde! Carregando...');
    }

}
