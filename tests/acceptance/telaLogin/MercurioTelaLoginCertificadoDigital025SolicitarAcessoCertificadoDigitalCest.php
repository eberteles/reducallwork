<?php

class MercurioTelaLoginCertificadoDigital025SolicitarAcessoCertificadoDigitalCest
{

    public function SolicitarAcessoCertificadoDigitalCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');
        $I->fillField('data[Usuario][no_usuario]', '');
        $I->fillField('data[Usuario][ds_senha]', '');
        $I->dontSeeCheckboxIsChecked('Acessar com Certificado Digital');
        $I->checkOption('#UsuarioCertificado');
        $I->see('Escolha um Certificado');

        #garante que tem os campos a seguir
        $I->see('Login de acesso');
        $I->see('Acessar com Certificado Digital');
        $I->see('Escolha um Certificado');
        $I->see('Entrar');
        $I->see('Atualizar Certificados');
        $I->see('Fechar');
    }

}
