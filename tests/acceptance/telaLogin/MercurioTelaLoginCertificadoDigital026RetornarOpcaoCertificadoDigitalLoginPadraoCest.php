<?php

class MercurioTelaLoginCertificadoDigital026RetornarOpcaoCertificadoDigitalLoginPadraoCest
{

    public function RetornarOpcaoCertificadoDigitalLoginPadraoCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        $I->checkOption('#UsuarioCertificado');
        $I->seeElement('select', ['id' => 'certificateSelect']);

        $I->see('Escolha um Certificado');

        $I->dontSeeElement('input', ['name' => 'data[Usuario][no_usuario]']);
        $I->dontSeeElement('input', ['name' => 'data[Usuario][ds_senha]']);

        $I->see('Acessar com Certificado Digital', '//span[@class="lbl"]');

        $I->see('Entrar', '//button[@id="signInButton"]');
        $I->see('Atualizar Certificados', '//button[@id="refreshButton"]');
        $I->see('Esqueci a minha Senha', '//a[@class="btn btn-small btn-primary"]');
        $I->see('Fechar', '//a[@class="btn btn-small"]');

        $I->click('#UsuarioCertificado');

        #garante que tem os campos a seguir
        $I->seeElement('input', ['name' => 'data[Usuario][no_usuario]']);
        $I->seeElement('input', ['name' => 'data[Usuario][ds_senha]']);
        $I->see('Acessar com Certificado Digital');
        $I->see('Entrar');
        $I->see('Atualizar Certificados');
        $I->see('Esqueci a minha Senha');
        $I->see('Fechar');
    }

}
