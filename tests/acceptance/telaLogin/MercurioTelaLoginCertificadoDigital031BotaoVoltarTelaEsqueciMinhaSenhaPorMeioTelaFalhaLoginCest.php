<?php

class MercurioTelaLoginCertificadoDigital031BotaoVoltarTelaEsqueciMinhaSenhaPorMeioTelaFalhaLoginCest
{

    public function BotaoVoltarTelaEsqueciMinhaSenhaPorMeioTelaFalhaLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');

        $I->click('Voltar');

        $I->seeCurrentURLEquals('/usuarioss/autenticar');
    }

}
