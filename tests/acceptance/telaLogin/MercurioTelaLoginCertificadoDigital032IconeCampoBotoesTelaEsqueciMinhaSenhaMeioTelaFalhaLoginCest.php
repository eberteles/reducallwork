<?php
class MercurioTelaLoginCertificadoDigital032IconeCampoBotoesTelaEsqueciMinhaSenhaMeioTelaFalhaLoginCest
{
    public function IconeCampoBotoesTelaEsqueciMinhaSenhaMeioTelaFalhaLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');

        $I->seeElement('i', ['class' => 'icon-user']);
        $I->seeElement('i', ['class' => 'icon-arrow-right icon-white']);
        $I->seeElement('i', ['class' => 'icon-arrow-left icon-envelope']);
    }
}
