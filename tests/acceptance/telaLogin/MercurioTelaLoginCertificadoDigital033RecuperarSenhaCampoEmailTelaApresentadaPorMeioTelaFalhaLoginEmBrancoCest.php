<?php

class MercurioTelaLoginCertificadoDigital033RecuperarSenhaCampoEmailTelaApresentadaPorMeioTelaFalhaLoginEmBrancoCest
{

    public function RecuperarSenhaCampoEmailTelaApresentadaPorMeioTelaFalhaLoginEmBrancoCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->fillField('data[Usuario][ds_email]', '');
        $I->see('Favor informar o seu e-mail cadastrado para a recuperação de sua senha de acesso.');
    }

}
