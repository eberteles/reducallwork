<?php

class MercurioTelaLoginCertificadoDigital034RecuperarSenhaInserindoEmailInvalidoCampoTelaApresentadaPorMeioTelaFalhaLoginCest
{

    public function RecuperarSenhaInserindoEmailInvalidoCampoTelaApresentadaPorMeioTelaFalhaLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->fillField('data[Usuario][ds_email]', 'e-mail');
        $I->click('Enviar');
        $I->see('O E-mail informado é inválido ou não está cadastrado no sistema.');
    }

}
