<?php

class MercurioTelaLoginCertificadoDigital035RecuperarSenhaInformandoEmailValidoTelaApresentadaPorMeioTelaFalhaLoginCest
{

    public function RecuperarSenhaInformandoEmailValidoTelaApresentadaPorMeioTelaFalhaLoginCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->fillField('data[Usuario][ds_email]', 'cadastrousuario@teste.com');
        $I->click('Enviar');
        $I->see('A sua nova senha de acesso foi encaminhada para o seu E-mail cadastrado!');
    }

}
