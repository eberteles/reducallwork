<?php

use Codeception\Util\Locator;

class MercurioTelaLoginCertificadoDigital036TelaEsqueciMinhaSenhaPorMeioTelaLoginCest
{

    public function TelaEsqueciMinhaSenhaPorMeioTelaLogin(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/autenticar');

        $I->click('Esqueci a minha Senha');

        #garante que tem os campos a seguir
        $I->seeElement('//img[@src="/img/logo_no2.png"]');
        $I->see('Favor informar o seu e-mail cadastrado para a recuperação de sua senha de acesso.');
        $I->see('E-mail');
        $I->see('Entrar');
        $I->see('Voltar');
        $I->see('Sistema melhor visualizado em 1024x768');
        $I->see(' GESCON - Versão: ');
    }

    public function _after(AcceptanceTester $I)
    {
        $I->assertRegExp('/© 2017 GESCON - Versão: [\s\S]+/', $I->grabTextFrom(Locator::lastElement('.sistema')));
    }

}
