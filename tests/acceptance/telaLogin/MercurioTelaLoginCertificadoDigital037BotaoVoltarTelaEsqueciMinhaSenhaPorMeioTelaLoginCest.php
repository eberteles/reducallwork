<?php

class MercurioTelaLoginCertificadoDigital037BotaoVoltarTelaEsqueciMinhaSenhaPorMeioTelaLoginCest
{

    public function BotaoVoltarTelaEsqueciMinhaSenhaPorMeioTelaLogin(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->click('Voltar');
        $I->seeCurrentURLEquals('/usuarios/autenticar');
        $I->see('Login de acesso');
    }

}
