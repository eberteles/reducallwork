<?php

class MercurioTelaLoginCertificadoDigital038IconeDoCampoEBotoesDaTelaEsqueciMinhaSenhaPorMeioDaTelaDeLoginCest
{

    /**
     */
    public function IconesTelaLoginCertificadoDigitalCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->seeElement('i', ['class' => 'icon-user']);
        $I->seeElement('i', ['class' => 'icon-arrow-right icon-white']);
        $I->seeElement('i', ['class' => 'icon-arrow-left icon-envelope']);
    }

}
