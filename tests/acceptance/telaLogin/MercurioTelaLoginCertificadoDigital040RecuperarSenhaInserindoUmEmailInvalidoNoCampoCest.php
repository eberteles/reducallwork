<?php

class MercurioTelaLoginCertificadoDigital040RecuperarSenhaInserindoUmEmailInvalidoNoCampoCest
{

    /**
     */
    public function RecuperarSenhaInserindoUmEmailInvalidoNoCampoCest(AcceptanceTester $I)
    {
        $I->amOnPage('/usuarios/esqueci');
        $I->fillField('data[Usuario][ds_email]', 'email');
        $I->click('Enviar');
        $I->see('O E-mail informado é inválido ou não está cadastrado no sistema.');
    }

}
