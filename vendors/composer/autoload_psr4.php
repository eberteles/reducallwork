<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'N2oti\\Integras\\' => array($vendorDir . '/n2oti/integras-sei/src/N2oti/Integras'),
    'N2oti\\Components\\Excel\\' => array($vendorDir . '/n2oti/excel/src/Excel'),
);
